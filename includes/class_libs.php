﻿<?php

/*================================================================================*\
|| 							Name code : class_libs.php 		 		 											  			# ||
||  				Copyright © 2009 by Thai Son - CMS vnTRUST                					# ||
\*================================================================================*/

/**
 * @version : 6.0
 * @date upgrade : 5/5/2018 by Thai Son
 **/
class Lib
{
  function getMenus(){
    global $DB, $input, $vnT, $conf;
    //$param_cache = array() ;
    //$cache = $vnT->Cache->read_cache("menu_horizontal",$param_cache);
    //if ($cache != _NOC) return $cache;
    //$act = $vnT->setting['menu_active'];
    $act = $input['mod'];
    $out = array();
    $arr_menu = array();
    $arr_menu_pos = array();
    $result= $vnT->DB->query("SELECT * FROM menu n ,menu_desc nd
                              WHERE n.menu_id=nd.menu_id AND display=1 AND lang='$vnT->lang_name'
                              ORDER BY pos ASC, parentid ASC , menu_order ASC , n.menu_id ASC ");
    while ($row = $vnT->DB->fetch_row($result)) {
      if ($row['parentid'] == 0) {
        $arr_menu_pos[$row['pos']][$row['menu_id']] = $row;
      } else {
        $arr_menu[$row['parentid']][$row['menu_id']] = $row;
      }
    }
    // menutop
    $num = count($arr_menu_pos['horizontal']);
    if ($num > 0) {
      $out['menutop'] = '<ul class="navbar-nav ml-auto mt-2 mt-lg-0">';
      $menu_group = '';
      $i = 0;
      foreach ($arr_menu_pos['horizontal'] as $menu_id => $row) {
        $i++;
        $menu_id = $row['menu_id'];
        $menu_link = $vnT->func->HTML($row['menu_link']);
        if ($menu_link == '') {
          $link = "javascript:void(0)";
        } else {
          $link = (!strstr($menu_link, "http://") && !strstr($menu_link, "https://")) ? $vnT->link_root . $menu_link : $menu_link;
        }
        $title = $vnT->func->HTML($row['title']);
        $target = $row["target"];
        $current = (($act == $row['name']) && ($row['name'])) ? " active " : "";
        $text = "<li  class='nav-item ".$current."'><a href=\"{$link}\" target=\"{$target}\">".$title."</a>";
        $text .= $this->add_submenus($menu_id, $arr_menu);
        $text .= '</li>';
        $out['menutop'] .= $text;
      }
      $out['menutop'] .= '</ul>';
      $vnT->setting['menutop'] = $out['menutop'];
      $out['group'] = $menu_group;
    }
    // mobile
    $num = count($arr_menu_pos['mobile']);
    if ($num > 0) {
      $out['mobile'] = '<ul class="mmMain">';
      $i = 0;
      foreach ($arr_menu_pos['mobile'] as $menu_id => $row) {
        $i++;
        $menu_id = $row['menu_id'];
        $menu_link = $vnT->func->HTML($row['menu_link']);
        if ($menu_link == '') {
          $link = "javascript:void(0)";
        } else {
          $link = (!strstr($menu_link, "http://") && !strstr($menu_link, "https://")) ? $vnT->link_root.$menu_link : $menu_link;
        }
        $title = $vnT->func->HTML($row['title']);
        $target = $row["target"];
        $current = (($act == $row['name']) && ($row['name'])) ? " class='current'" : "";
        $text = "<li ".$current."><a href=\"{$link}\" target=\"{$target}\">{$title}</a>";
        $text .= $this->add_submenus($menu_id, $arr_menu);
        $text .= '</li>';
        $out['mobile'] .= $text;
      }
      $out['mobile'] .= '</ul>';
    }

    $num = count($arr_menu_pos['footer']);
    if ($num > 0) {
      $out['footer'] = '<ul class="mmMain">';
      $i = 0;
      foreach ($arr_menu_pos['footer'] as $menu_id => $row) {
        $i++;
        $menu_id = $row['menu_id'];
        $menu_link = $vnT->func->HTML($row['menu_link']);
        if ($menu_link == '') {
          $link = "javascript:void(0)";
        } else {
          $link = (!strstr($menu_link, "http://") && !strstr($menu_link, "https://")) ? $vnT->link_root.$menu_link : $menu_link;
        }
        $title = $vnT->func->HTML($row['title']);
        $target = $row["target"];
        $current = (($act == $row['name']) && ($row['name'])) ? " class='current'" : "";
        $text = "<li ".$current."><a href=\"{$link}\" target=\"{$target}\" style=\"color:white\">{$title}</a>";
        $text .= $this->add_submenus($menu_id, $arr_menu);
        $text .= '</li>';
        $out['footer'] .= $text;
      }
      $out['footer'] .= '</ul>';
    }
   
    // menutop
    $num = count($arr_menu_pos['maps']);
    if ($num > 0) {
      $i = 0;
      foreach ($arr_menu_pos['maps'] as $menu_id => $row) {
        $i++;
        $menu_link = $vnT->func->HTML($row['menu_link']);
        if ($menu_link == '') {
          $link = "javascript:void(0)";
        } else {
          $link = (!strstr($menu_link, "http://") && !strstr($menu_link, "https://")) ? $vnT->link_root . $menu_link : $menu_link;
        }
        if( $i==1 ){
          $out['maps'] = '<div class="mapOnTools"><a href="'.$link.'"><i class="fa fa-map-marker"></i></a></div>';
        }
      }
      $vnT->setting['mapsmenu'] = $out['maps'];
    }
    // menutop
    $num = count($arr_menu_pos['survey']);
    if ($num > 0) {
      $i = 0;
      foreach ($arr_menu_pos['survey'] as $menu_id => $row) {
        $i++;
        $title = $vnT->func->HTML($row['title']);
        $menu_link = $vnT->func->HTML($row['menu_link']);
        if ($menu_link == '') {
          $link = "javascript:void(0)";
        } else {
          $link = (!strstr($menu_link, "http://") && !strstr($menu_link, "https://")) ? $vnT->link_root . $menu_link : $menu_link;
        }
        if( $i==1 ){
          $out['survey'] = '<div class="khaosatOnTools"><a href="'.$link.'"><span>'.$title.'</span></a></div>';
        }
      }
      $vnT->setting['surveymenu'] = $out['survey'];
    }
    //$vnT->Cache->save_cache("menu_horizontal", $text,$param_cache);
    return $out;
  }
  function add_submenus($cid, $arr_menu = array()){
    global $DB, $conf, $func, $vnT, $input;
    $text = "";
    $num = count($arr_menu[$cid]);
    if ($num > 0) {
      $text = '<ul>';
      foreach ($arr_menu[$cid] as $menu_id => $row) {
        $title = $vnT->func->HTML($row['title']);
        $menu_link = $vnT->func->HTML($row['menu_link']);
        $link = (!strstr($menu_link, "http://") && !strstr($menu_link, "https://")) ? $vnT->link_root . $menu_link : $menu_link;
        $text .= "<li><a href=\"{$link}\" >{$title}</a>";
        $text .= $this->add_submenus($row['menu_id'], $arr_menu);
        $text .= "</li>" . "\n";
      } //end while
      $text .= '</ul>';
    }
    return $text;
  }
  
  function box_lang () {
    global $vnT, $DB, $input, $vnTRUST;
    $output="";
    $result = $DB->query("SELECT * FROM language ");
    if ($num = $DB->num_rows($result)) {
      $i = 0;
      while ($row = $DB->fetch_row($result)){
        $i ++;
        if($vnT->link_lang[$row['name']]){
          $link = $vnT->link_lang[$row['name']]; 
        }else{
          if($input['mod'] == "" || $input['mod']=="main") {
            $link = ROOT_URI.$row['name']."/".$vnT->setting['seo_name'][$row['name']][$input['mod']].".html" ;
          }else{
            $link_old = str_replace($vnT->conf['rooturl'].$vnT->lang_name."/".$vnT->setting['seo_name'][$vnT->lang_name][$input['mod']],"",$vnT->seo_url);           
            $link = ROOT_URI.$row['name']."/".$vnT->setting['seo_name'][$row['name']][$input['mod']].$link_old; 
          }
        }
        $src = ROOT_URL . "vnt_upload/lang/" .$row['picture'];
        $class = ($row['name'] == $vnT->lang_name) ? "class='current'" : "";
        if($vnT->lang_name != $row['name'])
          $cur = '<img src="'.$src.'" alt="'.$row['title'].'" /><span>'.$row['title'].'</span>';
        $output .= '<a class="language" href="'.$link.'" title="'.$row['title'].'"><img src="'.$src.'" alt="'.$row['name'].'" /></a>';
      }
      $textout = '<div class="text-center">'.$output.'</div>';
    }
    return $textout;
  }
  function box_statistics(){
    global $vnT, $DB, $input;
    $output = $vnT->lang['global']['vistited'] . " : <b id='stats_totals' >&nbsp;</b> - " . $vnT->lang['global']['online'] . " : <b id='stats_online' >&nbsp;</b>";
    if ($vnT->conf['cache']) {
      $output = "<!-- Start_box_statistics -->" . $output . "<!-- End_box_statistics -->";
    }
    return $output;
  }
  function getBanners($pos, $banner_num = 0, $tpl_skin = 'advertise'){
    global $func, $DB, $conf, $vnT, $input;
    $text = '';
    $module = ($input['mod']) ? $input['mod'] : $vnT->conf['module'];
    $order_by = ($banner_num == 1) ? " RAND()" : " l_order  ASC";
    $limit = ($banner_num) ? " LIMIT 0," . $banner_num : "";
    $sql = "select * from advertise where pos='$pos' and display=1 and lang='$vnT->lang_name' AND (FIND_IN_SET('$module',module_show) or (module_show='')) ORDER BY " . $order_by . "  " . $limit;
    $result = $vnT->DB->query($sql);
    if ($num = $vnT->DB->num_rows($result)) {
      $data = array();
      $i = 0;
      while ($row = $vnT->DB->fetch_row($result)) {
        $i++;
        $src = ROOT_URL . "vnt_upload/weblink/" . $vnT->func->HTML($row['img']);
        $l_link = (!strstr($row['link'], "http")) ? $vnT->link_root . $row['link'] : $row['link'];
        $target = ($row['target']) ? $row['target'] : "_blank";

        $row['link'] = $l_link;
        $row['title'] = $vnT->func->HTML($row['title']);
        $row['target'] = $target;
        $row['src'] = $src;

        $vnT->skin_box->assign("row", $row);
        $vnT->skin_box->parse($tpl_skin . ".html_item");
      }

      $data['class'] = ($num > 1) ? " muti" : "";
      $vnT->skin_box->reset($tpl_skin);
      $vnT->skin_box->assign("data", $data);
      $vnT->skin_box->parse($tpl_skin);
      $textout = $vnT->skin_box->text($tpl_skin);
    }
    return $textout;
  }
  function get_advertise($pos){
    global $DB, $input, $vnT;
    $output = "";
    $marquee = 0;
    $res_ck = $DB->query("select * from ad_pos where name='$pos'");
    if ($row_ck = $DB->fetch_row($res_ck)) {
      $width = $row_ck['width'];
      if ($row_ck['type_show'] == 0 || $row_ck['type_show'] == 2 || $row_ck['type_show'] == 3) {
        $type_show = "horizontal";
        if ($row_ck['type_show'] != 0) {
          $marquee = 1;
          $direction = ($row_ck['type_show'] == 3) ? "down" : $direction = "up";
        }
      }
      if ($row_ck['type_show'] == 1 || $row_ck['type_show'] == 4 || $row_ck['type_show'] == 5) {
        $type_show = "vertical";
        if ($row_ck['type_show'] != 1) {
          $marquee = 1;
          $direction = ($row_ck['type_show'] == 5) ? "right" : $direction = "left";
        }
      }
    }
    $module = ($input['mod']) ? $input['mod'] : $vnT->conf['module'];
    $result = $vnT->DB->query("select * from advertise where  display=1  and lang='$vnT->lang_name' and  pos='$pos'  and  (FIND_IN_SET('$module',module_show) or (module_show='') )  ORDER BY type_ad DESC,l_order   ");
    $html_row = "";
    $selectbox = 0;
    $html_option = "";
    if ($DB->num_rows($result)) {
      if ($type_show == 'vertical')
        $html_row .= '<table width="100%" border="0" cellspacing="1" cellpadding="1"><tr align=center>';
      while ($row = $vnT->DB->fetch_row($result)) {
        $title = $vnT->func->HTML($row['title']);
        $src = ROOT_URL . "vnt_upload/weblink/" . $vnT->func->HTML($row['img']);
        $l_link = (!strstr($row['link'], "http://")) ? $vnT->link_root . $row['link'] : $row['link'];
        $target = ($row['target']) ? $row['target'] : "_blank";

        switch ($row["type_ad"]) {
          case 0:
            $html_row .= ($type_show == 'vertical') ? '<td class=advertise >' : '<p class=advertise >';
            if ($row["type"] == "swf") {
              $html_row .= '<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=7,0,19,0" width="' . $row["width"] . '" height="' . $row["height"] . '">
							<param name="movie" value="' . $src . '" />
							<param name="quality" value="high" />
							<param name="wmode" value="transparent" />
							<embed src="' . $src . '" quality="high" pluginspage="http://www.macromedia.com/go/getflashplayer" type="application/x-shockwave-flash" width="' . $row["width"] . '" height="' . $row["height"] . '" wmode="transparent" ></embed>
							</object>';
            } else {
              $html_row .= "<a onmousedown=\"return rwt(this,'advertise'," . $row['l_id'] . ")\" href='{$l_link}' target='{$target}' title='" . $title . "'  ><img border=0 src=\"{$src}\"  width='" . $row['width'] . "'  alt='{$title}' /></a>";
            }
            $html_row .= ($type_show == 'vertical') ? '</td>' : '</p>';
            break;
          case 1:
            $html_row .= ($type_show == 'vertical') ? '<td class=advertise >' : '<p class=advertise >';
            $html_row .= "<a href='" . $l_link . "' target='{$target}' >" . $title . "</a>";
            $html_row .= ($type_show == 'vertical') ? '</td>' : '</p>';
            break;
          case 2:
            $op_link = ROOT_URL . $vnT->cmd . "=mod:advertise|type:advertise|lid:" . $row['l_id'];
            $html_option .= "<option value=\"{$op_link}\" >" . $title . "</option>";
            $selectbox = 1;
            break;
          case 3:
            $html_row .= '<div class=advertise >' . $vnT->func->txt_unHTML($row['img']) . '</div>';
            break;
        } //end switch
      }
      if ($selectbox)
        $html_row = '<p align="center" ><form action="" name="f_link" method="post"><select name="site"  onChange="if (f_link[\'site\'].selectedIndex != 0){window1=window.open(f_link[\'site\'].value)}" style="width:' . $width . 'px"><option>---- Web link ---</option>' . $html_option . '</select></form></p>' . $html_row;
      if ($marquee) {
        $output = "<marquee behavior=\"scroll\" direction=\"{$direction}\" scrollamount=\"2\" scrolldelay=\"2\" onmouseover=\"this.stop()\" onmouseout=\"this.start();\" >" . $html_row . "</marquee>";
      } else {
        $output = $html_row;
      }
      return $output;
    } else {
      return '';
    }
  }
  function get_logos($pos = "logo"){
    global $DB, $input, $vnT,$conf;
    $lang = ($vnT->lang_name) ? $vnT->lang_name : 'vn';
    $arr_out = array();
    $result = $DB->query("SELECT * FROM advertise WHERE display=1 AND pos='$pos' AND lang='$lang'
                          ORDER BY l_order LIMIT 0,1");
    while ($row = $DB->fetch_row($result)) {
      $title = $row['title'];
      $src = $conf['rooturl']."vnt_upload/weblink/" . $row['img'];
      $l_link = (!strstr($row['link'], "http://")) ? $vnT->link_root . $row['link'] : $row['link'];
      $target = ($row['target']) ? $row['target'] : "_blank";
      $header = "<a href='{$l_link}' target='{$target}' title='".$title."'><img src='{$src}' alt='{$title}' width='115px'/></a>";
      $footer = '<img src="'.$src.'" alt="'.$title.'" width="195px"/>';
      if ($input['mod'] == "main"){
        $header = '<h1>'.$header.'</h1>';
      }
    }
    $arr_out['header'] = $header;
    $arr_out['footer'] = $footer;
    return $arr_out;
  }
  function List_Country($did = "", $ext = ""){
    global $vnT, $conf, $DB, $func;
    $text = "<select name=\"country\" id=\"country\" class='select form-control'  {$ext}   >";
    $sql = "SELECT * FROM iso_countries where display=1 ORDER BY name ASC ";
    $result = $vnT->DB->query($sql);
    while ($row = $vnT->DB->fetch_row($result)) {
      $selected = ($row['iso'] == $did) ? " selected" : "";
      $text .= "<option value=\"{$row['iso']}\" " . $selected . ">" . $func->HTML($row['name']) . "</option>";
    }
    $text .= "</select>";
    return $text;
  }
  function get_country_name($code){
    global $func, $DB, $conf, $vnT;
    $text = $code;
    $result = $vnT->DB->query("SELECT name FROM iso_countries WHERE iso='$code' ");
    if ($row = $vnT->DB->fetch_row($result)) {
      $text = $vnT->func->HTML($row['name']);
    }
    return $text;
  }
  function List_City($country = "VN", $did = "", $default = "", $type_show = "list", $ext = ""){
    global $vnT, $conf, $DB, $func;
    $text = "<option value=\"\" >" . $default . "</option>";
    $sql = "SELECT * FROM iso_cities where country='" . $country . "' AND display=1  ORDER BY c_order ASC , name ASC  ";
    $result = $vnT->DB->query($sql);
    while ($row = $vnT->DB->fetch_row($result)) {
      $selected = ($row['id'] == $did) ? "selected" : "";
      $text .= "<option value=\"{$row['id']}\" {$selected} >" . $vnT->func->HTML($row['name']) . "</option>";
    }
    if ($type_show == "option") {
      $textout = $text;
    } else {
      $textout = "<select name=\"city\" id=\"city\" class='form-control'  {$ext}   >";
      $textout .= $text;
      $textout .= "</select>";
    }
    return $textout;
  }
  function get_city_name($id){
    global $func, $DB, $conf, $vnT;
    $text = $id;
    $result = $vnT->DB->query("SELECT name FROM iso_cities WHERE id=" . $id);
    if ($row = $vnT->DB->fetch_row($result)) {
      $text = $vnT->func->HTML($row['name']);
    }
    return $text;
  }
  function List_State($city, $did = "", $default = "", $type_show = "list", $ext = ""){
    global $vnT, $conf, $DB, $func;
    $text = "<option value=\"\" >" . $default . "</option>";
    $sql = "SELECT * FROM iso_states where display=1 and city='$city'  ORDER BY s_order ASC , name ASC  ";
    $result = $vnT->DB->query($sql);
    while ($row = $vnT->DB->fetch_row($result)) {
      $selected = ($row['id'] == $did) ? "selected" : "";
      $text .= "<option value=\"{$row['id']}\" {$selected} >" . $vnT->func->HTML($row['name']) . "</option>";
    }
    if ($type_show == "option") {
      $textout = $text;
    } else {
      $textout = "<select name=\"state\" id=\"state\" class='form-control'  {$ext}   >";
      $textout .= $text;
      $textout .= "</select>";
    }
    return $textout;
  }
  function get_state_name($id){
    global $func, $DB, $conf, $vnT;
    $text = $id;
    $result = $vnT->DB->query("SELECT name FROM iso_states WHERE id=" . $id);
    if ($row = $vnT->DB->fetch_row($result)) {
      $text = $vnT->func->HTML($row['name']);
    }
    return $text;
  }
  function List_Ward($state, $did = "", $default = "", $type_show = "list", $ext = ""){
    global $vnT, $conf, $DB, $func;
    $text = "<option value=\"\" selected>" . $default . "</option>";
    $sql = "SELECT * FROM iso_wards where display=1 and state='$state'  ORDER BY   w_order ASC , name ASC  ";
    $result = $vnT->DB->query($sql);
    while ($row = $vnT->DB->fetch_row($result)) {
      $selected = ($row['code'] == $did) ? "selected" : "";
      $text .= "<option value=\"{$row['id']}\" {$selected} >" . $vnT->func->HTML($row['name']) . "</option>";
    }

    if ($type_show == "option") {
      $textout = $text;
    } else {
      $textout = "<select name=\"ward\" id=\"ward\" class='form-control'  {$ext}   >";
      $textout .= $text;
      $textout .= "</select>";
    }
    return $textout;
  }
  function get_ward_name($id){
    global $func, $DB, $conf, $vnT;
    $text = $id;
    $result = $vnT->DB->query("SELECT name FROM iso_wards WHERE id=" . $id);
    if ($row = $vnT->DB->fetch_row($result)) {
      $text = $vnT->func->HTML($row['name']);
    }
    return $text;
  }
  function box_search (){
    global $vnT, $input;
    $arr_out = array();
    $data['keyword'] = ($input['keyword']) ? $input['keyword'] : '';
    $data['link_search'] = $vnT->link_root .$vnT->setting['seo_name'][$vnT->lang_name]['search'] .".html";
    $arr_out['pc'] = $vnT->skin_box->parse_box("box_search", $data);
    $arr_out['mb'] = $vnT->skin_box->parse_box("box_search_mb", $data);
    $vnT->setting['fixed_search'] = $vnT->skin_box->parse_box("box_search_side", $data);
    return $arr_out;
  }
  function get_social_network(){
    global $vnT, $input;
    $arr_out = array();
    $res_icon= $vnT->DB->query("SELECT * FROM social_network_icon 
                                WHERE display=1 ORDER BY display_order ASC , id ASC ");
    $arr_out['icon'] = '<ul>';
    while ($row_icon = $vnT->DB->fetch_row($res_icon)) {
      $arr_out['icon'].= '<li><a href="'.$row_icon['link'].'" target="'.$row_icon['target'].'"><i class="fa '.$row_icon['fonts'].'"></i></a></li>';
    }
    $arr_out['icon'] .= '</ul>';
    $vnT->setting['social_icon'] = $arr_out['icon'];
    $res_share = $vnT->DB->query("SELECT * FROM social_network_share
                                  WHERE display=1 ORDER BY display_order ASC, id ASC ");
    while ($row_share = $vnT->DB->fetch_row($res_share)) {
      if ($row_share['type'] == 2) {
        $arr_out['share'] .= $vnT->func->txt_unHTML($row_share['picture']) . ' &nbsp; ';
      } else {
        $arr_out['share'] .= "<a href='" . $row_share['link'] . "' target='_blank' ><img src='" . ROOT_URI . "vnt_upload/weblink/" . $row_share['picture'] . "' /></a> &nbsp; ";
      }
    }
    $res_like = $vnT->DB->query("SELECT * FROM social_network_like WHERE display=1 ORDER BY display_order ASC , id ASC ");
    while ($row_like = $vnT->DB->fetch_row($res_like)) {
      $arr_out['like'] .= $vnT->func->txt_unHTML($row_like['html_code']) . ' &nbsp; ';
    }

    return $arr_out;
  }
  function get_icon_share($type, $link_share){
    global $vnT, $input;
    $textout = '';
    switch ($type) {
      case "google"   :
        $textout = '<a title="Chia sẻ qua Google Plus." href="https://plus.google.com/share?url=' . $link_share . '" rel="nofollow"  target="_blank"><img src="' . $vnT->dir_images . '/icon_google_share.png" /></a>';
        break;
      case "facebook"   :
        $textout = '<a title="Chia sẻ qua Facebook." href="https://www.facebook.com/sharer.php?u=' . $link_share . '" rel="nofollow"   target="_blank"><img src="' . $vnT->dir_images . '/icon_facebook_share.png" /></a>';
        break;
      case "twitter"   :
        $textout = '<a href="https://twitter.com/share" class="twitter-share-button">Tweet</a>';
        break;
    }
    return $textout;
  }
  function get_icon_like($type, $link_share){
    global $vnT, $input;
    $textout = '';
    switch ($type) {
      case "facebook"   :
        $textout = '<iframe src="https://www.facebook.com/plugins/like.php?href=' . $link_share . '&width=70&height=28&layout=button&action=like&size=large&show_faces=false&share=false&appId=' . $vnT->setting['social_network_setting']['facebook_appId'] . '"  style="border:none;overflow:hidden"  scrolling="no" frameborder="0" allowTransparency="true" width="70" height="28" ></iframe>';
        break;
      case "google"   :
        $textout = '<g:plusone href="' . $link_share . '" size="medium" ></g:plusone>';
        break;
    }
    return $textout;
  }
  function loadSiteDoc(){
    global $vnT, $input;
    $out = array();
    $result = $vnT->DB->query("SELECT * FROM sitedoc WHERE lang='" . $vnT->lang_name . "'");
    while ($row = $vnT->DB->fetch_row($result)) {
      $out[$row['doc_name']] = $row['doc_content'];
    }
    return $out;
  }
  function get_navation($arr_items){
    global $vnT, $input;
    $textout = '<ul>';
    $textout .= '<li><a href="' . $vnT->link_root . '" >' . $vnT->lang['global']['homepage'] . '</a></li>';
    foreach ($arr_items as $item) {
      if ($item['link']) {
        $textout .= '<li><a href="' . $item['link'] . '">' . $item['title'] . "</a></li>";
      } else {
        $textout .= '<li>' . $item['title'] . "</li>";
      }
    }
    $textout .= '</ul>';
    return $textout;
  }
  function loadDataGlobal(){
    global $vnT, $input;
    $data = array();
    $data['sitedoc'] = $this->loadSiteDoc();
    $data['link_search'] = $vnT->link_root.$vnT->setting['seo_name'][$vnT->lang_name]['search'] . ".html";
    $data['social_network'] = $this->get_social_network();
    //$vnT->setting['social_icon'] = $data['social_network']['icon'];
    $data['link_fanpage'] = $vnT->setting['social_network_setting']['facebook_page'];
    $data['facebook_appId'] = $vnT->setting['social_network_setting']['facebook_appId'];
    $data['google_appId'] = $vnT->setting['social_network_setting']['google_apikey'];
    $data['box_member'] = $this->box_member();
    $data['box_lang'] = $this->box_lang();
    $data['box_contact'] = $this->box_contact();
    $data['bct'] = $this->get_img_banner('bct');
    $data['box_search'] = $this->box_search();
    return $data;
  }
  function get_child_slide($pos='child',$ext=''){
    global $DB, $func, $input, $vnT;
    $textout = "";
    
    return $textout;
  }
  function box_navation ($text=''){
    global $DB, $input, $vnT;
    $textout  ='<ol class="breadcrumb">';
    $textout .= $text;
    $textout .= '</ol>';
    return $textout;
  }
  function get_src_banner ($pos='top'){
    global $DB, $vnT;
    $html_banner = "";
    $module = ($input['mod']) ? $input['mod'] : $vnT->conf['module']; 
    if($module=="main"){ $html_banner .= '<h1>'; }
    $result = $DB->query("select * from advertise where pos='$pos' and display=1   and lang='$vnT->lang_name' and  (FIND_IN_SET('$module',module_show) or (module_show='') )  ORDER BY l_order LIMIT 0,1");
    if ($row = $DB->fetch_row($result)){
      // $title = $vnT->func->HTML($row['title']);
      // $l_link = (! strstr($row['link'], "http://")) ? $vnT->link_root .$row['link'] : $row['link'] ;
      $src = ROOT_URL . "vnt_upload/weblink/" . $row['img'];
    }
    return $src;
  }
  function get_img_banner ($pos='top'){
    global $DB, $vnT;
    $img = "";
    $module = ($input['mod']) ? $input['mod'] : $vnT->conf['module']; 
    if($module=="main"){ $html_banner .= '<h1>'; }
    $result = $DB->query("SELECT * FROM advertise
                          WHERE pos='$pos' AND display=1 AND lang='$vnT->lang_name' 
                          AND (FIND_IN_SET('$module',module_show) or (module_show='') ) ORDER BY l_order LIMIT 0,1");
    if ($row = $DB->fetch_row($result)){
      $src = ROOT_URL . "vnt_upload/weblink/" . $row['img'];
      if($row['link']){
        if ($row['link'] == 'http://') {
          $link = "javascript:void(0)";
        } else {
          $link = (!strstr($row['link'], "http://") && !strstr($row['link'], "https://")) ? $vnT->link_root . $row['link'] : $row['link'];
        }
        $img = '<a href="'.$link.'" target="'.$row['target'].'"><img src="'.$src.'"/></a>';
      }else{
        $img = '<a href=""><img src="'.$src.'"/>';
      }
    }
    return $img;
  }
  function box_contact(){
    global $vnT, $input;
    $text  = '';
    $result= $vnT->DB->query("SELECT * FROM contact_config WHERE lang='$vnT->lang_name' AND display=1
                              ORDER BY display_order ASC LIMIT 0,2");
    while($row = $vnT->DB->fetch_row($result)){
      $text .= '<div class="boxBot">';
      $text .= '<div class="title">'.$vnT->func->HTML($row['title']).'</div>';
      $text .= '<div class="content"><div class="addressFoot">';
      $text .= '<div class="be fa-home">'.$vnT->func->HTML($row['address']).'</div>';
      if($row['phone']){
        $text .= '<div class="be fa-phone"><a href="tel:'.$row['phone'].'">'.$row['phone'].'</a></div>';
      }
      if($row['hotline']){
        $text .= '<div class="be fa-phone"><a href="tel:'.$row['hotline'].'">'.$row['hotline'].'</a></div>';
      }
      if($row['email'])
        $text .= '<div class="be fa-envelope-o"><a href="mailto:'.$row['email'].'">'.$row['email'].'</a></div>';
      $text .= '</div></div></div>';
    }
    return $text;
  }
  function logo_footer ($pos='logo_footer'){
    global $DB, $input, $vnT;
    $text = "";
    $module = ($input['mod']) ? $input['mod'] : $vnT->conf['module'];
    $result = $DB->query("SELECT * FROM advertise 
                          WHERE pos='$pos' AND display=1 AND lang='$vnT->lang_name' 
                          AND (FIND_IN_SET('$module',module_show) OR (module_show='') ) ORDER BY l_order LIMIT 0,1");
    if ($row = $DB->fetch_row($result)){
      $title = $vnT->func->HTML($row['title']);
      $src = ROOT_URL . "vnt_upload/weblink/" . $row['img'];
      $l_link = (! strstr($row['link'], "http://")) ? $vnT->link_root .$row['link'] : $row['link'];
      $text= '<div class="logo_ft"><a href="'.$l_link.'" title="'.$title.'">
                <img src="'.$src.'" alt="'.$title.'"></a>
              </div>';
    }
    return $text;
  }
  function box_member(){
    global $DB, $input, $vnT;
    $link_mod = $vnT->link_root . $vnT->setting['seo_name'][$vnT->lang_name]['member'];
    if($vnT->user['mem_id']>0){
      $data['logout_link'] = $link_mod.'/logout.html';
      $data['manager_link'] = $link_mod.'/update_account.html';
      $data['order_link'] = $link_mod.'/your_order.html';
      $data['changepass_link'] = $link_mod.'/changepass.html';
      if ($vnT->user['avatar']) {
        $picture = "member/avatar/".$vnT->user['avatar'];
        $src = $vnT->func->get_src_modules($picture, 150 ,'',1,'1:1');
      } else {
        $src = ROOT_URL."vnt_upload/member/avatar/no_avatar.jpg";
      }
      $data['pic'] = "<img src=\"{$src}\" alt='".$vnT->user['full_name']."'>";
      $data['full_name'] = $vnT->user['full_name'];
      $arr_out['mb'] = $vnT->skin_box->parse_box("is_member_mb", $data);
      $arr_out['pc'] = $vnT->skin_box->parse_box("is_member_pc", $data);
    }else{
      $data['login_link'] = $link_mod.'/login.html';
      $data['register_link'] = $link_mod.'/register.html';
      $arr_out['mb'] = $vnT->skin_box->parse_box("not_member_mb", $data);
      $arr_out['pc'] = $vnT->skin_box->parse_box("not_member_pc", $data);
    } 
    return $arr_out;
  }
  function get_cart_info(){
    global $vnT, $DB, $input,$func ;
    $textout = '';
    if(empty($_SESSION['session_cart'])){
      $session_cart = md5(uniqid(rand()));
      $_SESSION['session_cart'] = $session_cart;
    }
    $session = $_SESSION['session_cart'];
    $link_cart = $vnT->link_root.$vnT->setting['seo_name'][$vnT->lang_name]['product'].'/cart.html';
    $result = $DB->query("SELECT * FROM order_shopping WHERE session='$session'");
    if($num = $DB->num_rows($result)){
      $num_items = $num;
    }else{
      $num_items = 0;
    }
    $textout = '<a href="'.$link_cart.'"><i class="fa fa-shopping-basket"></i><span class="number">'.$num_items.'</span></a>';
    return $textout;
  }
  function get_list_tags ($module,$tags) {
    global $vnT, $func, $DB, $conf;
    $text = "";
    $sql = "SELECT * FROM {$module}_tags
            WHERE display=1 AND FIND_IN_SET(tag_id,'".$tags."')>0 ORDER BY name ASC, date_post DESC";
    $query = $vnT->DB->query($sql);
    if(($num = $vnT->DB->num_rows($query))){
      $i=1;
      $text = "";
      while($row = $vnT->DB->fetch_row($query)){
        $text .= '<li><a href="'.$vnT->link_root.$vnT->setting['seo_name'][$vnT->lang_name][$module].'/tags/'.$row['tag_id']."/".$vnT->func->make_url ($row['name']).'.html">'.$row["name"].'</a></li>';
        $i++;
      }
    }
    return $text;
  }
  function get_tag_name ($module,$tagID) {
    global $vnT, $func, $DB, $conf;
    $text = "";
    $query = $vnT->DB->query("SELECT name FROM {$module}_tags WHERE tag_id=".$tagID);
    if($row = $vnT->DB->fetch_row($query)){
      $text =  $vnT->func->HTML($row['name']);
    }
    return $text;
  }
  function get_member_avatar($mem_id){
    global $DB, $conf, $func, $vnT, $input;
    $sql ="SELECT avatar FROM members WHERE mem_id =".$mem_id;
    die($sql);
    $query = $DB->query($sql);
    if($row = $DB->fetch_row($query)){
      $avatar = $row['avatar'];
    }else
      $avatar ='';
    return $avatar;
  }
  function get_banner_sidebar ($pos='sidebar'){
    global $DB, $input, $vnT;
    $textout = "";
    $where = "";
    $module = ($input['mod']) ? $input['mod'] : $vnT->conf['module'];
    $result = $DB->query("SELECT * FROM advertise 
                          WHERE pos='$pos' AND display=1 AND lang='$vnT->lang_name' 
                          AND (FIND_IN_SET('$module',module_show) OR (module_show='') ) ORDER BY l_order");
    if($num =$DB->num_rows($result)){
      $textout .= '<div class="advertise">';
      while ($row = $DB->fetch_row($result)){
        $l_link = (! strstr($row['link'], "http://")) ? $vnT->link_root .$row['link'] : $row['link'];
        $title = $vnT->func->HTML($row['title']);
        $src = ROOT_URL . "vnt_upload/weblink/" . $row['img'];
        $textout.= '<a class="style-adv" href="'.$l_link.'" target="'.$row['target'].'"><img src="'.$src.'" alt="'.$title.'"/></a>';
      }
      $textout .= '</div>';
    }
    return $textout;
  }
  function fixed_sidebar(){
    global $vnT,$DB;
    $data['social_icon'] = $vnT->setting['social_icon'];
    $data['menutop'] = $vnT->setting['menutop'];
    $data['link_map'] = $vnT->setting['mapsmenu'];
    $data['surveymenu'] = $vnT->setting['surveymenu'];
    $data['fixed_search'] = $vnT->setting['fixed_search'];
    $textout = $vnT->skin_box->parse_box("box_fixed", $data);
    return $textout;
  }
  function regex_url_youtube($url){
    preg_match("/^(?:http(?:s)?:\/\/)?(?:www\.)?(?:m\.)?(?:youtu\.be\/|youtube\.com\/(?:(?:watch)?\?(?:.*&)?v(?:i)?=|(?:embed|v|vi|user)\/))([^\?&\"'>]+)/", $url, $matches);
    return $matches[1];
  }


  function getSql($table, $field_id, $where="") {
        global $DB, $input, $vnT, $conf;
        $sql = "SELECT * 
        FROM $table n, {$table}_desc nd
        WHERE n.$field_id=nd.$field_id
        AND display=1
        AND lang='$vnT->lang_name'
        $where";
        return $sql;
    }
}
?>