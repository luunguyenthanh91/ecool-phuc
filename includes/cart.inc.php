<?php
if(empty($_SESSION['session_cart']))  //make sure this hasn't already been established 
{
  $session_cart = md5(uniqid(rand()));   //creates a random session value 
	$_SESSION['session_cart'] = $session_cart;
}
// start Cart class
class Cart1
{ 
	var $session ="" ;
	//xoa cat item cua
	function Cart1(){
		global $conf,$func,$input,$DB,$vnT; 
		$this->session = $_SESSION['session_cart'];
		$expired_time = time()-(2*24*60*60);
		$DB->query("DELETE FROM order_shopping WHERE timeadd<='$expired_time' ");		
	}
	// add item to shopping database
	function add_item($session,$product,$quantity,$data=array()) 	{
		global $conf,$func,$input,$DB,$vnT; 
	   $err="";
			 // see if item is already in shopping list 
			 $in_list = "SELECT * FROM order_shopping WHERE session='$session' AND product_id='$product'";
			 $result = $DB->query($in_list);
			 if ($row =$DB->fetch_row($result)){
					$quantity = $quantity + $row["quantity"];
					$color = $row["color"];
					$size = $row["size"];
					$dup['quantity']= $quantity;
					$dup['color']= $color;
					$dup['size']= $size;
					$dup['timeadd']= time();
					$DB->do_update("order_shopping",$dup," session='$session' AND product_id='$product' ");
			 }else{
					$cot['session'] = $session;
					$cot['product_id'] = $product;
					$cot['quantity'] = $quantity;
					$cot['color'] = $input['color'];
					$cot['size'] = $input['size'];
					$cot['date'] = date("Ymd");
					$cot['timeadd'] = time();
					$DB->do_insert("order_shopping",$cot);
			 } 
			 return $err;
		}
		 // delete a specific item 
		function delete_item($session,$id)
		{
			 global $conf,$func,$input,$DB,$vnT; 
			 $DB->query( "DELETE FROM order_shopping WHERE session='$session' AND product_id=$id  ");
		}
		 // modifies a quantity of an item 
		function modify_quantity($session, $id, $quantity,$data=array())
		{		   
			 global $conf,$func,$input,$DB,$vnT; 
			  $dup ['quantity'] = $quantity;
			  $ok = $DB->do_update("order_shopping",$dup," session='$session' AND product_id=$id ");
		}
		 // clear all content in their cart 
		function clear_cart($session)
		{
			 global $conf,$func,$input,$DB,$vnT; 
			 $DB->query( "DELETE FROM order_shopping WHERE session='$session'");
		}
		 //add up the shopping cart total 
		function cart_total($session)
		{
			 global $conf,$func,$input,$DB,$lang; 
			 $total=0;
			 $result = $DB->query( "SELECT * FROM order_shopping WHERE session='$session'");
			 if($DB->num_rows($result) >0) 
			 {
					while($row = $DB->fetch_row($result))	
					{
						 $sql_price =  "SELECT  * FROM products WHERE p_id = ".$row["product_id"];
						 $result_price = $DB->query($sql_price);
						 $row_price = $DB->fetch_row($result_price);
						 $price =$row_price['price'] ;
						 
			 			 // add total
					   $total = $total + ($price*$row["quantity"]);
				 } //while
			}// if
	
		 return $total;
	}
		 
	 // function to display contents
	function display_contents($session) {
	  global $conf,$func,$input,$DB,$vnT; 
		$count = 0;
		$total = 0;
		
		$sql = "SELECT *
						FROM order_shopping o , products p , products_desc pd
						WHERE o.product_id = p.p_id
						AND p.p_id=pd.p_id
						AND pd.lang='$vnT->lang_name' 
						AND o.session='$session' order by o.timeadd ASC";
		$result = $DB->query($sql);
		while($row = $DB->fetch_row($result))		
		{
			
			$contents["id"][$count] = $row["id"];			  
			$contents["cat_id"][$count] = $row["cat_id"];
			$contents["p_id"][$count] = $row["p_id"];
			$contents["maso"][$count] = $row['maso'];
			$contents["picture"][$count] = $row["picture"];
			$contents["p_name"][$count] = $row['p_name'];
			$contents["friendly_url"][$count] = $row['friendly_url'];			 
			$contents["status"][$count] = $row['status'];
			$contents["eligible_for_sale"][$count] = ($row['eligible_for_sale']* $row["quantity"]);			  
			
			$contents["price"][$count] = $row['price'];
			$contents["quantity"][$count] = $row["quantity"];
			$contents["color"][$count] = $row["color"];
			$contents["size"][$count] = $row["size"];
			$contents["total"][$count] = ($contents[ "price"][$count] * $row["quantity"]);
			$total += $contents["total"][$count];
			
			$count ++;
		}	
		$contents[ "final"] = $total;
		return $contents;
	}
		
 // count no items
 function num_items($session)	 {
			global $conf,$func,$input,$DB,$vnT;
			$result = $DB->query( "SELECT * FROM order_shopping WHERE session='$session'");
			
			$num_rows = $DB->num_rows($result);
			return $num_rows;
 }
 
 // count quantity item
 function num_quantity($session)	 {
		global $conf,$func,$input,$DB,$vnT;
		$num = 0;
		$result = $DB->query( "SELECT * FROM order_shopping WHERE session='$session'");
				while ($row = $DB->fetch_row($result) ){
				$num+= $row['quantity'];
		}
		return $num;
 }
//end class
}
?>
