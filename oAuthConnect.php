<?php
session_start();
define('IN_vnT', 1);
define('PATH_ROOT', dirname(__FILE__));
define('DS', DIRECTORY_SEPARATOR);
require_once("_config.php"); 
require_once (PATH_ROOT . DS . 'includes' . DS . 'defines.php');
require_once("includes/class_db.php"); 
require_once("includes/class_functions.php"); 	
require_once ('includes/class_global.php');
// initialize the data registry
$vnT = & new vnT_Registry();
$DB = $vnT->DB ;
$func = $vnT->func ; 
  
$vnT->conf = $vnT->func->fetchDbConfig($conf); 
$vnT->lang_name = (isset($_POST['lang'])) ? $_POST['lang']  : "vn" ;
$func->load_language('global');

$vnT->user = array();
$vnT->user['mem_id'] =0;
$arr_cookie_member = explode("|",$_COOKIE[MEMBER_COOKIE]);
$res_mem = $vnT->DB->query("SELECT * FROM members WHERE username='".$arr_cookie_member[0]."' ");
if($row_mem = $vnT->DB->fetch_row($res_mem))
{ 
	$mem_hash = md5($row_mem['mem_id'] . '|' . $row_mem['password']);  	
	if($arr_cookie_member[1] ==$mem_hash )	{
		$vnT->user  = $row_mem  ; 
	}		
}	 
  
	function getIp() {
		$ip = $_SERVER['REMOTE_ADDR'];
		if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
				$ip = $_SERVER['HTTP_CLIENT_IP'];
		} elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
				$ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
		}
		return $ip;	
	}
	
	$result = $DB->query("select * from member_setting WHERE lang='vn' ");
  $setting = $DB->fetch_row($result);
	
	$api_type = ($_GET['provider']) ? $_GET['provider'] : "Facebook";	
	$returnUrl = ($_GET['returnUrl']) ? $_GET['returnUrl'] : "" ;
	
	
	$vnT->setting = array();
	$res_sn = $vnT->DB->query("SELECT * FROM social_network_setting WHERE id=1");
	$vnT->setting = $vnT->DB->fetch_row($res_sn) ;	
	$vnT->setting['base_url'] = $conf['rooturl']."libraries/hybridauth/";	
 
	
	$api_type = ($_GET['provider']) ? $_GET['provider'] : "Facebook";	
	
	include('libraries/hybridauth/config.php');
  include('libraries/hybridauth/Hybrid/Auth.php');
  	 
	try{
 		
		$hybridauth = new Hybrid_Auth( $config );

		$adapter = $hybridauth->authenticate( $api_type );

		$user_profile = $adapter->getUserProfile();
		/*echo "<pre>";
		print_r($user_profile);
		echo "</pre>";
		*/ 
		
		$email = trim($user_profile->email);
		
		$res_ck = $vnT->DB->query("SELECT * FROM members WHERE email='".$email."' ") 		;
		if($row = $vnT->DB->fetch_row($res_ck)) //login
		{
			$vnT->func->vnt_set_member_cookie($row['mem_id'],0);
			$vnT->func->User_Login($row); 
			$vnT->DB->query("Update members set last_login=" . time() . " ,last_ip='".getIp()."' , num_login=num_login+1 where mem_id=" . $row['mem_id'] . ""); 
			
			if($row['api_pass']){
					$link_ref = ($returnUrl) ? "http://".$_SERVER['HTTP_HOST'].$returnUrl : $vnT->conf['rooturl']."member.html";
			}else{
					$link_ref = $vnT->conf['rooturl']."member/api_changepass.html";
			}	
			
		}else{ 
			$link_ref = $vnT->conf['rooturl']."member/api_changepass.html";
			$activate_code = md5(uniqid(microtime()));
			$m_status =  1 ;
			$ip = $_SERVER['REMOTE_ADDR'];
			$pass = $vnT->func->m_random_str(6);
			$password =  $vnT->func->md10($pass);
			$gender = ($user_profile->gender =="male" ) ? 1 : 2 ;
			$birthday = $user_profile->birthDay."/".$user_profile->birthMonth."/".$user_profile->birthYear;
			
			$img = file_get_contents('https://graph.facebook.com/'.$user_profile->identifier.'/picture?type=large');
			$avatar = $user_profile->identifier.'.jpg';
			$file = $vnT->conf['rootpath'].'vnt_upload/member/avatar/'.$avatar;
			@file_put_contents($file, $img);
			
			$cot = array(
			'mem_group' => 1 ,  
			'email' => $email , 
			'password' => $password ,
			'phone' => $user_profile->phone ,  
			'avatar' => $avatar ,
			'full_name' => $user_profile->displayName , 
			'gender' => $gender ,   
			'birthday' => $birthday,
			'address' => $user_profile->address , 
								
			'date_join' => $vnT->func->getTimestamp() , 
			'activate_code' => $activate_code , 
			'newsletter' => 1 , 
			'last_login' => $vnT->func->getTimestamp() , 
			'm_status' => $m_status,
			'api_type' => $api_type,
			'api_user' => $user_profile->identifier,
			'api_pass' => 0
			);

 
			$ok = $vnT->DB->do_insert("members", $cot);
			//echo $DB->debug();
			if ($ok) {
				$mem_id = $vnT->DB->insertid();
				//update  username					
				$so_next = $mem_id;				 
				$str_so ="";
				for ($n=strlen(strval($so_next));$n<4;$n++){
					$str_so .="0";
				}
				$str_so.=$so_next;
				$username = "KH".$str_so;
 				$vnT->DB->query("Update members set username='" .$username. "' where mem_id=" . $mem_id . " ");
 					
					
				//login
				$info['mem_id'] = $mem_id;
				$info['username'] = $username;
				$kq = $func->User_Login($info);
				if ($kq) {
					$vnT->func->vnt_set_member_cookie($info['mem_id'],0);	
					$vnT->DB->query("Update members set last_login=" . time() . " ,last_ip='".getIp()."' , num_login=num_login+1 where mem_id=" . $mem_id . " ");
				} 						
			}							
		}  
		
		@header("Location: " . $link_ref . " ");
    echo "<meta http-equiv='refresh' content='0; url=" . $link_ref . "' />";
	 

}
	catch( Exception $e ){
	die( "<b>got an error!</b> " . $e->getMessage() ); 
}
$vnT->DB->close();							
?>