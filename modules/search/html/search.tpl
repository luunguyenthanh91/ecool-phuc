<!-- BEGIN: modules -->
<div class="container">
  <nav class="fs-10 font-weight-light" aria-label="breadcrumb">
        {data.navation}
  </nav>
  <div class="wrapping">
  <div class="wrapCont">
    <div class="wrapper">
      {data.main}
    </div>
  </div>
  <div id="flagEnd"></div>
</div>
</div>
<!-- END: modules -->
 
<!-- BEGIN: html_list_search -->
<p>{data.note_keyword}</p>
<p class="mess_result">{data.note_result}</p>

{data.list_search}
{data.nav}
<!-- END: html_list_search -->