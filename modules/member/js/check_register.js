﻿// JavaScript Document
var aErr = new Array('#rUserName_err','#rPassWord_err','#rConfirm_PassWord_err','#rEmail_err','#rVerifyCode_err','#full_name_err');
var aNote = new Array('#rUserName_note','#rPassWord_note','#rConfirm_PassWord_note','#rEmail_note','#rVerifyCode_note','#full_name_note');
var aTrNote = new Array('#rUserName_tr_note','#rPassWord_tr_note','#rConfirm_PassWord_tr_note','#rEmail_tr_note','#rVerifyCode_tr_note','#full_name_tr_note');		

function setValue(obj){
	rId = "#"+$(obj).attr('id')+"_tr";
	labelId = "#"+$(obj).attr('id')+"_label";
	trNoteId = "#"+$(obj).attr('id')+"_tr_note";
	noteId = "#"+$(obj).attr('id')+"_note";
	errId = "#"+$(obj).attr('id')+"_err";
}
function hideNote(cur){	
	for(var i=0;i<aNote.length;i++){
		if(aNote[i]!=cur){
			$(aNote[i]).hide();
			if($(aErr[i]).css('display') == 'none')
				$(aTrNote[i]).removeClass('td_note');		
		}	
	}
}

function onSubmit(){	

	var flag_submit = true;
	if(!checkRequire() || $('#rUserName_err').css('display') != 'none' || $('#rPassWord_err').css('display') != 'none' || $('#rConfirm_PassWord_err').css('display') != 'none' || $('#rEmail_err').css('display') != 'none' || $('#rVerifyCode_err').css('display') != 'none' || $('#full_name_err').css('display') != 'none' ){
		flag_submit = false;
	}	
	
		
	cc = $('#r_agree').attr('checked') ;
	if(!cc) {
		alert(Argee_Error) ;
		return false;
	}	
	
	if(flag_submit){
		$('#rUserName').val($('#rUserName_').val());		
		return true;	
	}else{
		return false;
	}	
	
	
}

function checkRequire(){	
	var count = 0;
	var aInput = new Array('#rUserName','#rPassWord','#rConfirm_PassWord','#rEmail','#rVerifyCode','#full_name');
	for(var i=0;i<aInput.length;i++){			
		if($(aInput[i]).val() == ''){			
			showMessRequire(aInput[i]);
			count++;
		}
	}	
	return true;		
}

function showMessRequire(obj){
	setValue(obj);		
	showErrorHTML();
	$(errId).html(Require_Error);	
}
function showErrorHTML(){
	$(noteId).hide();
	$(errId).show();
	$(trNoteId).addClass("td_note");
	$(rId).addClass("tr_error");
}
function onFocus(obj){

	setValue(obj);
	hideNote(noteId);
	//fixed password	
	if(trNoteId == '#rPassWord_tr_note'){
		if($(obj).val() == "" && $('#rUserName_input').css('display') == "none"){
			//$('#rPassWord_note').css('top','60px');
		}else{
			//$('#rPassWord_note').css('top','73px');
		}
	}
	//
	
	if(trNoteId == '#rConfirm_PassWord_tr_note'){					
		if($(obj).val() == ''){
			chkConfirmPassErr == 1;	
			$(labelId).removeClass();
			$(errId).hide();
			$(rId).removeClass();	
			$(trNoteId).removeClass('td_note');
		}
		if(chkConfirmPassErr == 1){
			return false;
		}else{
			$(errId).hide();
			$(trNoteId).removeClass('td_note');
		}
		return false;		
		return false;
	}
	
	var rEmail = $('#rEmail').val();
	

	if(trNoteId == '#dName_tr_note'){
		$(rId).removeClass();
		$(labelId).removeClass();
		$(trNoteId).removeClass('td_note');
	}	
	else{
		$(rId).removeClass();
		$(labelId).removeClass();		
		$(trNoteId).addClass('td_note');	
	}		
	$(errId).hide();	
	$(noteId).show();		
	
}
//####################################### Password ##################################################
function checkCharLow(c){
	if(c > 96 && c < 123)
		return true;
	return false;
}
function checkCharUp(c){
	if(c > 64 && c < 91)
		return true;
	return false;
}
function checkNum(c){
	if(c > 47 && c < 58)
		return true;
	return false;
}
function checkStringIsNum(str){
	for(i=0;i<str.length;i++){
		if(!checkNum(str.charCodeAt(i)))
			return false;
	}
	return true;
}
function checkPersonalId(str){
	if(str.length < 8 || str.length > 15)
		return false;
	if(!checkStringIsNum(str))
		return false;
	return true;
}
function password_level(str){
	if(str.length < 6)
		return false;
	var level = new Array(null,null,null,null);
	for(i=0;i<str.length;i++){		
		// Ky tu thuong
		if(checkCharLow(str.charCodeAt(i)))
			level[0] = 1;
		// Ky tu hoa
		if(checkCharUp(str.charCodeAt(i)))
			level[1] = 1;	
		// Ky tu so
		if(checkNum(str.charCodeAt(i)))
			level[2] = 1;		
	}
	//co ky tu dat biet
	if(!checkCharacter(str)){
		level[3] = 1;
	}
	return level;
}

//####################################### Account Name ##################################################
/*************** Kiem tra ky tu (.,_) o dau va cuoi *****************/
function acc_checkCharBegin(obj){
	if(obj.charAt(0) == '.' || obj.charAt(0) == '_'){
		return false;	
	}	
	return true;
}
/*************** Kiem tra ky tu (.,_) va cuoi *****************/
function acc_checkCharEnd(obj){
	if(obj.charAt(obj.length-1) == '.' || obj.charAt(obj.length-1) == '_'){
		return false;	
	}
	return true;
}
/*************** Kiem tra ky tu gach duoi va dau cham lien tiep *****/
function acc_checkCharContinuous(obj){
	if(obj.indexOf('._') != -1 || obj.indexOf('_.') != -1 || obj.indexOf('__') != -1 || obj.indexOf('..') != -1){
		return false;
	}	
	return true;
}
/*************** Kiem tra khong cho nhap ky tu dat biet *****/
function acc_checkSpecialChars(obj){
	return checkCharacter(obj);
}
function checkCharacter(str){
	for(i=0;i<str.length;i++){
		if((str.charCodeAt(i) < 48 && str.charCodeAt(i) != 46)|| str.charCodeAt(i) > 122)
			return false;
		if(str.charCodeAt(i) > 57 && str.charCodeAt(i) < 65)
			return false;
		if(str.charCodeAt(i) > 90 && str.charCodeAt(i) < 97 && str.charCodeAt(i) != 95)
			return false;		
	}
	return true;
}
function checkVietNam(str){
	for(i=0;i<str.length;i++){
		if(str.charCodeAt(i) < 32 || str.charCodeAt(i) > 126)
			return false;			
	}
	return true;
}
/*********** check len **************/
function acc_checkLen(obj){
	if((obj.length != 0 & obj.length < 4) || obj.length > 32)
		return false;
	return true;
}
/*********** check len **************/
function pass_checkLen(obj){
	if((obj.length != 0 & obj.length < 6) || obj.length > 32)
		return false;
	return true;
}
/******** call suggest account  *******/
function randomstring(word){
	var index = (Math.floor ( Math.random ( ) * arrSuggest.length) );
	return word+arrSuggest[index-1];
}
var cur_acc = '';
var account_list = '';
var cur_w1 = '';
var cur_w2 = '';
var cur_w3 = '';

function getAccountName(){
	var flag = true;	

	if(chkAccErr == 2 && cur_acc == $('#rUserName').val()){			 
		return false;
	}
	if(chkAccErr == 3 && cur_acc == $('#rUserName').val()){
		return false;
	}

	var word1 = '';
	var word2 = '';
	var word3 = '';	
	
	word1 = $('#rUserName').val();		
	if(word1 == '' || !acc_checkLen(word1))
		return false;
	
	$('#rUserName_wait').show();
	
	////////////////Check word da nhap truoc do
	var rUserName = $('#rUserName').val();	
	var sessionRegisterPage = $('#sessionRegisterPage').val();
	$.get( ROOT + "modules/member/ajax/check_register.php", { 
		   'do': 'check_username', 
			 'ACC': rUserName, 
		   'sessionRegisterPage': sessionRegisterPage
		 }, 
		function(json){ 			
			cur_acc = rUserName;
			if(json['flag'] == 0){
				chkAccErr = 1;		
				$('#rUserName_wait').hide();
				showError('#rUserName',UserName_Error[7]);
				return false;
			}
			if(json['flag'] == 1){
				chkAccErr = 0;
				selectAcc(rUserName);
				$('#rUserName_wait').hide();
			}
			if(json['flag'] == 2){
				chkAccErr = 2;
				$('#rUserName_wait').hide();
				showError('#rUserName',UserName_Error[6]);
				return false;
			}	
			
		},'json'
	);	
}
 

function selectAcc(acc){
	chkAccErr = 0;
	$('#rUserName').val(acc);
	$('#user_name').val(acc);
	var str = acc;
	var strCheck = "<a href='#"+acc+"' onclick=changetext('"+acc+"')>Thay đổi</a>";
	curAcc = str;
	$("#rUserName_readonly").show();	
	$("#rUserName_readonly").html(str);	
	$("#Check_label").html(strCheck);	
	$("#rUserName_input").hide();
	
	
	hideError('#rUserName');
	$("#rPassWord").focus();
	return checkValid("#rUserName_icon");	
}

function changetext(acc){
	$("#rUserName_readonly").html('');
	$("#Check_label").html('');	
	$("#rUserName_input").show();
	$("#rUserName").val(acc);
	$("#rUserName").focus();
}


/***** EMAIL *****/
function getEmail(){
	var flag = true;	
	
	var email_check = $('#rEmail').val() ;	
	var sessionRegisterPage = $('#sessionRegisterPage').val();
	$.get( ROOT + "modules/member/ajax/check_register.php", { 
		   'do': 'check_email', 
			 'email': email_check, 
		   'sessionRegisterPage': sessionRegisterPage
		 }, 
		function(json){ 			
			if(json['flag'] == 0){
				chkEmailErr = 1;		
				$('#rEmail_wait').hide();
				showError('#rEmail',Email_Error[1]);
				return false;
			}
			if(json['flag'] == 1){
				chkEmailErr = 0;
				$('#rEmail_wait').hide();
				hideError('#rEmail');
				return checkValid("#rEmail_icon");	
			}
			
			
		},'json'
	);	
}

//#################################################### general
function checkValid(obj){
	$(obj).show();
	$(obj).fadeOut(2000);
	return true;
	
}
/************ End ************/
// show error
function showError(obj,mess){
	setValue(obj);
	$(errId).html(mess);
	showErrorHTML()
	return true;
}
//Hide Error
function hideError(obj){
	setValue(obj);		
	$(rId).removeClass();
	$(labelId).removeClass();
	$(trNoteId).removeClass();
	//$(errId).fadeOut("slow");	
	$(errId).hide();	
}


///////////////////// General ////////////////////////
function checkDate(dd,mm,yyyy){     
    var mydate=new Date();
	var year=mydate.getFullYear();
	var month=mydate.getMonth() + 1;
	var daym=mydate.getDate(); 
	//alert(dd+' --- '+ daym);
	//alert(mm+' --- '+ month);
	//alert(yyyy+' --- '+ year);
   //Kiem tra thoi gian hien tai
   if(!checkStringIsNum(dd) || !checkStringIsNum(mm) || !checkStringIsNum(yyyy))   
	   	return false;
   if(dd < 1 || dd > 31 || yyyy < 1900)
	   	return false;
   if(yyyy > year)
   		return false;  
   if(yyyy == year){
   		if(mm > month)
			return false;  
		if(mm == month)
			if(dd > daym || dd == daym)
				return false;   
   }   
   //Kiem tra hop le
   if(!isNaN(yyyy)&&(yyyy!="")&&(yyyy<10000)){     
		if ((mm==4 || mm==6 || mm==9 || mm==11) && dd==31)
			return false; 
		if (mm == 2) { // check for february 29th
			var isleap = (yyyy % 4 == 0 && (yyyy % 100 != 0 || yyyy % 400 == 0));
			if (dd>29 || (dd==29 && !isleap))
				return false; 
		}
   }   
   return true;
}
//##############################################################################################
function CheckEmail(mailName,minlenMailName,minlen,maxlen)
{
	var sMailName = mailName.toLowerCase();
	if(!sMailName )
		return false;
	if(sMailName.length < minlenMailName || sMailName.charAt(0) == '_' || sMailName.charAt(sMailName.length-1) == '_' || sMailName.indexOf('._') != -1 || sMailName.indexOf('_.') != -1 || sMailName.indexOf('__') != -1 || sMailName.indexOf('-') != -1)
	{ 
		return false;
	}

	//		
	var stringIn = sMailName  ;
	//
	if(stringIn.length < minlen || stringIn.length > maxlen)
	{		
		return false;
	}
	//
	if (stringIn.indexOf('..') != -1 || stringIn.indexOf('.@') != -1 || stringIn.indexOf('@.') != -1 || stringIn.indexOf(':') != -1  )
	{
		
		return false;
	}
	var re = /^([A-Za-z0-9\_\-]+\.)*[A-Za-z0-9\_\-]+@[A-Za-z0-9\_\-]+(\.[A-Za-z0-9\_\-]+)+$/;
	if (stringIn.search(re) == -1)
	{
		
		return false;
	}		
	return true;	
}


