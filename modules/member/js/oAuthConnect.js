function oAuthConnect (provider,info) {
	var ok_load = 1 ;
	if(ok_load){
		var mydata =  "provider="+provider+'&identifier='+info.id+'&email='+info.email+'&name='+info.name+'&gender='+info.gender+'&birthday='+info.birthday+'&avatar='+info.avatar;
		$.ajax({
			async: true,
			dataType: 'json',
			url: ROOT_MEM+"/ajax/oAuthConnect.html",
			type: 'POST',
			data: mydata ,
			success: function (data) {
				if(data.ok) {
					if(data.link_ref){
						top.location = data.link_ref;
					}else{
						top.location.reload(true);
					}
				}else{
					jAlert(data.mess,js_lang['error']);
				}
			}
		})
	}

	return false ;
}

function initGoogle () {
	gapi.load('auth2', function(){
		// Retrieve the singleton for the GoogleAuth library and set up the client.
		auth2 = gapi.auth2.init({
			client_id: google_appId,
			cookiepolicy: 'single_host_origin',
			// Request scopes in addition to 'profile' and 'email'
			scope: 'profile email'
		});
		googleSignin(document.getElementById('gpLogin'));
	});
}

function googleSignin(element) {
	var googleUser = {};
	console.log(element.id);
	auth2.attachClickHandler(element, {},
		function(googleUser) {
			var profile = googleUser.getBasicProfile();
			var user_email = profile.getEmail(); //get user email
			var info =	{"id": profile.getId() ,"email":  profile.getEmail() ,"name": profile.getName(),"gender": "","birthday":"","avatar": profile.getImageUrl()};

			if(vnTRUST.is_email(user_email)){
				oAuthConnect('google',info);
			}else{
				alert("Quá trình kết nối Google không lấy được thông tin Email của bạn. Vui lòng thử lại","Báo lỗi");
				return false ;
			}

		}, function(error) {
			alert(JSON.stringify(error, undefined, 2));
		});
}

function initFacebook(a) {
	window.fbAsyncInit = function () {
		FB.init({
			appId: facebook_appId,
			status: false,
			cookie: true,
			xfbml: true
		});
		if ($.isFunction(a)) {
			a()
		}
		function b() {
			FB.login(function(response) {
				if (response.authResponse) {
					//console.log('Welcome!  Fetching your information.... ');
					//console.log(response); // dump complete info
					access_token = response.authResponse.accessToken; //get access token
					user_id = response.authResponse.userID; //get FB UID
					FB.api('/me?fields=id,email,name,gender,birthday', function(response) {
						user_email = response.email; //get user email
						if(vnTRUST.is_email(user_email)){
							oAuthConnect('facebook',response);
						}else{
							alert("Quá trình kết nối Facebook không lấy được thông tin Email của bạn. Vui lòng thử lại","Báo lỗi");
							return false ;
						}
					});
				} else {
					//user hit cancel button
					alert("User cancelled login or did not fully authorize.","Báo lỗi");
					console.log('User cancelled login or did not fully authorize.');
				}
			}, {
				scope: 'email, user_birthday, user_about_me, public_profile'
			});
		}
		$("#fbLogin").click(function () {
			b();
			$("#loading").show()
		})
	};
	(function () {
		var b = document.createElement("script");
		b.src = document.location.protocol + "//connect.facebook.net/vi_VN/all.js";
		b.async = true;
		document.getElementById("fb-root").appendChild(b)
	} ())
}
function doFacebookLogin(){
	FB.login(function(response) {
		if (response.authResponse) {
			//console.log('Welcome!  Fetching your information.... ');
			//console.log(response); // dump complete info
			access_token = response.authResponse.accessToken; //get access token
			user_id = response.authResponse.userID; //get FB UID
			FB.api('/me?fields=id,email,name,gender,birthday', function(response) {
				user_email = response.email; //get user email
				if(vnTRUST.is_email(user_email)){
					oAuthConnect('facebook',response);
				}else{
					jAlert("Quá trình kết nối Facebook không lấy được thông tin Email của bạn. Vui lòng thử lại","Báo lỗi");
					return false ;
				}
			});
		} else {
			//user hit cancel button
			jAlert("User cancelled login or did not fully authorize.","Báo lỗi");
			console.log('User cancelled login or did not fully authorize.');
		}
	}, {
		scope: 'email, user_birthday, user_about_me, public_profile'
	});
}

function initLogin() {
	initFacebook();
	initGoogle();
}