<?php
/*================================================================================*\
|| 							Name code : register.php 		 		 																	  # ||
||  				Copyright © 2007 by Thai Son - CMS vnTRUST                					# ||
\*================================================================================*/
/**
 * @version : 1.0
 * @date upgrade : 11/12/2007 by Thai Son
 **/
if (! defined('IN_vnT')) {
  die('Access denied');
}
$nts = new sMain();

class sMain
{
  var $output = "";
  var $skin = "";
  var $linkUrl = "";
  var $module = "member";
  var $action = "logout";

  /**
   * function sMain ()
   * Khoi tao 
   **/
  function sMain ()
  {
    global $input, $vnT, $conf;
	  include ("function_" . $this->module . ".php");
    loadSetting();
    
    if (isset($input['url']) && $input['url'] != "") {
      $url = $input['url'];
    } else
      $url = $vnT->func->base64url_encode($vnT->link_root);
      // check member 
    if ($vnT->user['mem_id'] == 0) {
      $link_ref = $vnT->link_root;
			$vnT->func->header_redirect($link_ref) ;  
    } else {
      $vnT->func->vnt_clear_member_cookie();
      $s_id = $vnT->user['session_id'];
      $do_update = $vnT->DB->query("UPDATE sessions SET time='{$time}',mem_id='0' WHERE s_id='{$s_id}'"); 
			
      $mess = $vnT->lang['member']['logout_success'];
      $url = $vnT->func->base64url_decode($url);
      $vnT->func->html_redirect($url, $mess);
    }
  }
  // end class
}
?>