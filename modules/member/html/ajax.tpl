<!-- BEGIN: html_item --><div class="result-item clearfix">
  <div class="item-info">
    <div class="img">{data.pic}</div>
    <div class="name">{data.p_name}</div>
    <div class="maso">#{data.maso}</div>
    <div class="price">{data.text_price}</div>
  </div>
  <div class="box-form-order">

    <div class="box-list-add">
      <div class="row-list" id="row_{data.p_id}">
        <input type="hidden" id="product_{data.p_id}" name="product[{data.p_id}]" value="{data.p_id}" />
        <div class="div-color">
          <div class="input-wrapper clearfix"><div class="item-label">{LANG.member.color_code}</div><div class="item-input"><select class="form-control" name="color[{data.p_id}]" id="color_{data.p_id}" >
                <option value="{data.color}">{data.color_code}</option>
              </select></div></div>
        </div>
        <div class="div-size-quantity ">
          <div id="ext_size_quantity_{data.p_id}">
            <div id="size-quantity1" class="row-size-quantity clearfix">
            <div class="div-size">
              <div class="input-wrapper clearfix"><div class="item-label">Size</div><div class="item-input">

                  <select class="form-control size" name="size_{data.p_id}[1]" id="size_{data.p_id}_1" >
                    {data.option_size}
                  </select>

                </div></div>

            </div>
            <div class="div-quantity">
              <div class="input-wrapper clearfix"><div class="item-label">{LANG.member.quantity}</div><div class="item-input"><input type="text" name="quantity_{data.p_id}[1]" id="quantity_{data.p_id}_1" class="form-control quantity" value="1"/></div></div>

            </div>
            <div class="div-btn-del"><a href="javascript:void(0)" onclick="vnTOrder.delSize({data.p_id},1)" class="btn-add-del" ><span class="del">{LANG.member.del}</span></a></div>
          </div>
          </div>

          <div class="div-btn-add">
            <input type="hidden" name="num_size_quantity_{data.p_id}" id="num_size_quantity_{data.p_id}" value="1" />
            <a href="javascript:void(0) " class="btn-add-del" onclick="vnTOrder.addMoreSize({data.p_id})"><span class="add">{LANG.member.add_size}</span></a> </div>
        </div>
      </div>
    </div>

    <div class="div-button-order">
      <div class="row">
        <div class="col-xs-6">{data.btn_add_more_color}</div>
        <div class="col-xs-6" style="text-align: right">
          <input type="hidden" name="do_sunmit" id="do_sunmit" value="1" />
          <button id="btnAddCart" name="btnAddCart" type="button" class="btn btn-default" value="Add Cart" onclick="vnTOrder.doAddToCart()"><span>{LANG.member.add_to_cart}</span></button>
        </div>
      </div>
    </div>
  </div>
</div>

<div style="display: none">
  <div id="size-quantity-tmp" class="row-size-quantity clearfix"  >
  <div class="div-size">
    <div class="input-wrapper clearfix"><div class="item-label">Size</div><div class="item-input">

        <select class="form-control size" name="size_vitri[new]" id="size_vitri_new" >
          {data.option_size}
        </select>

      </div></div>

  </div>
  <div class="div-quantity">
    <div class="input-wrapper clearfix"><div class="item-label">{LANG.member.quantity}</div><div class="item-input"><input type="text" name="quantity_vitri[new]" id="quantity_vitri_new" class="form-control quantity" value="1"/></div></div>

  </div>
  <div class="div-btn-del"><a href="javascript:void(0) " class="btn-add-del"><span class="del">{LANG.member.del}</span></a></div>
</div>

 </div>
</div>
<!-- END: html_item -->

<!-- BEGIN: html_chose_color -->
<div class="box-chose-color">
  <div class="chose-title">{LANG.member.title_chose_color} : <b class="maso">#{data.maso}<b></div>
  <table class="table table-bordered">
    <tr>
      <th colspan="2" >{LANG.member.product}</th>
      <th>{LANG.member.color_code}</th>
      <th>{LANG.member.chose}</th>
    </tr>
    {data.list_item}
  </table>
</div>
<!-- END: html_chose_color -->

<!-- BEGIN: html_color -->
<div class="row-list" id="row_{data.p_id}">
  <input type="hidden" id="product_{data.p_id}" name="product[{data.p_id}]" value="{data.p_id}" />
  <div class="div-color">
    <div class="input-wrapper clearfix"><div class="item-label">{LANG.member.color_code}</div><div class="item-input"><select class="form-control" name="color[{data.p_id}]" id="color_{data.p_id}" >
          <option value="{data.color}">{data.color_code}</option>
        </select></div></div>
  </div>
  <div class="div-size-quantity ">
    <div id="ext_size_quantity_{data.p_id}">
      <div id="size-quantity1" class="row-size-quantity clearfix">
        <div class="div-size">
          <div class="input-wrapper clearfix"><div class="item-label">Size</div><div class="item-input">

              <select class="form-control size" name="size_{data.p_id}[1]" id="size_{data.p_id}_1" >
                {data.option_size}
              </select>

            </div></div>

        </div>
        <div class="div-quantity">
          <div class="input-wrapper clearfix"><div class="item-label">{LANG.member.quantity}</div><div class="item-input"><input type="text" name="quantity_{data.p_id}[1]" id="quantity_{data.p_id}_1" class="form-control quantity" value="1"/></div></div>

        </div>
        <div class="div-btn-del"><a href="javascript:void(0)" onclick="vnTOrder.delSize({data.p_id},1)" class="btn-add-del" ><span class="del">{LANG.member.del}</span></a></div>
      </div>
    </div>

    <div class="div-btn-add">
      <input type="hidden" name="num_size_quantity_{data.p_id}" id="num_size_quantity_{data.p_id}" value="1" />
      <a href="javascript:void(0) " class="btn-add-del" onclick="vnTOrder.addMoreSize({data.p_id})"><span class="add">{LANG.member.add_size}</span></a> </div>
  </div>
</div>
<!-- END: html_color -->
