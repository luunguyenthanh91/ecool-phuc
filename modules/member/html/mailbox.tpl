<!-- BEGIN: modules -->

  <div id="vnt-navation" class="breadcrumb" itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb"><div class="navation">{data.navation}</div></div>
  <div class="mod-content">

    <div class="row">
      <div class="col-xs-9">{data.main}</div>
      <aside  class="col-xs-3">{data.box_sidebar}</aside>
    </div>
  </div>

<!-- END: modules -->

<!-- BEGIN: html_list_email -->
<script type="text/javascript">
 	 js_lang['please_chose_item']   	= "{LANG.member.please_chose_item}";
	 js_lang['error_only_member']   	= "{LANG.member.error_only_member}";
	 js_lang['are_you_sure_del']   	= "{LANG.member.are_you_sure_del}"; 	 
</script> 
 
{data.err} 



      <div class="box-search">
        <form  action="{data.fsearch}" method="post" name="f_Search" id="f_Search">
        	<div class="box-search-content row">
          	<div class="col-xs-10">
            	<div class="s-item row">
            
            <div class="col-xs-12 col-sm-3 item"><input name="key" id="key" type="text" class="textfiled form-control" placeholder="{LANG.member.mail_subject}" value="{data.key}"   /></div>
            
            <div class="col-xs-12 col-sm-3 item"><input name="date_from" id="date_from" type="text" class="textfiled form-control dates" placeholder="{LANG.member.date_from}" value="{INPUT.date_from}"   /></div>
            <div class="col-xs-12 col-sm-3 item"><input name="date_to" id="date_to" type="text" class="textfiled form-control dates" placeholder="{LANG.member.date_to}" value="{INPUT.date_to}"   /></div>
            
            <div class="col-xs-12 col-sm-3 item"><button id="btnSearch" name="btnSearch" type="submit" class="btn btnGay" value="Tìm ngay"><span>{LANG.member.btn_search}</span></button></div> 
            
             
            
          </div>
           	</div>
            
            <div class="col-xs-2">
            	<div class="nav-action"><a href="{data.link_add}" class="write"><i class="fa fa-plus-square"></i> <span>{LANG.member.compose_mail}</span></a></div>
           	</div>
            
          
              
           
        </div>
       	</form>
      </div>
      
      
    <div class="list-mailbox">  
        {data.table_list} 
        <div class="footer-tool clearfix " {data.style_nav}>
        	<div class="div-action fl"><span class="arr_up">&nbsp;</span><a href="javascript:;" onclick="vnTpost.del_selected('{data.action_del}');" class="btnS">{LANG.member.delete}</a></div>
          <div class="nav fr">{data.nav}</div>
        </div>
      

			</div>
      
      
<br>
<!-- END: html_list_email -->


 
<!-- BEGIN: html_write -->
<script type="text/javascript"> 
	
	 js_lang['err_mail_to_empty']   	= "{LANG.member.err_mail_to_empty}";
	 js_lang['err_mail_subject_empty']   	= "{LANG.member.err_mail_subject_empty}";
	 js_lang['err_mail_content_empty']   	= "{LANG.member.err_mail_content_empty}"; 	 
  
</script> 
 

<div id="Member" class="buying-request">
	
	<div class="boxForm">
  	  
     
      {data.err}
       
       <form action="{data.link_action}" name="frmMember" id="frmMember" method="post"  class="form-horizontal validate " >
      	
               
            
            <div class="form-group">
              <label class="control-label col-xs-4" for="mail_to">Người nhận <span class="font_err">*</span></label>
              <div class="col-xs-8 "><input name="mail_to" id="mail_to" class="required form-control" style="width:100%" type="text" value="{data.mail_to}" {data.readonly}  title="{LANG.member.err_cannot_empty}"  /></div>
               
            </div>
            
             <div class="form-group">
              <label class="control-label col-xs-4" for="product_name">{LANG.member.mail_subject} <span class="font_err">*</span></label>
              <div class="col-xs-8 "><input name="mail_subject" id="mail_subject" class="required form-control" style="width:100%" type="text" value="{data.mail_subject}"    title="{LANG.member.err_cannot_empty}"  /></div>
               
            </div> 
            
            <div class="form-group">
              <label class="control-label col-xs-4" for="mail_content">{LANG.member.mail_content} <span class="font_err">*</span></label>
              <div class="col-xs-8 "><textarea id="mail_content" name="mail_content" class="form-control" rows="10"  >{data.mail_content}</textarea></div>
            </div>
  

				
                  
        <div class="div-button">
        	<input type="hidden" name="do_submit" id="do_submit"  value="1" /> 
        	<button  id="btnSubmit" name="btnSubmit" type="submit" class="btn btn-default" value="{LANG.member.btn_send}" ><span >{LANG.member.btn_send}</span></button> &nbsp;
          
          
          <button  id="btnReset" name="btnReset" type="reset" class="btn btn-default" value="{LANG.member.btn_reset}" ><span >{LANG.member.btn_reset}</span></button>
          
        </div>
        
       </form>
         
       
    
	</div>      
</div> 
<script language="javascript">
  $(document).ready(function() {	
    // validate signup form on keyup and submit
    var validator = $("#frmMember").validate({
      rules: { 
				mail_content : {required: true	, minlength: 20}
			},
      messages: {	 
				mail_content : { required: "{LANG.member.err_cannot_empty}" , number: "{LANG.member.err_minlength_20}" }
			}         
    });	  
  });  
   
</script>

<!-- END: html_write -->



<!-- BEGIN: html_view -->
<script type="text/javascript">
 	 js_lang['please_chose_item']   	= "{LANG.member.please_chose_item}";
	 js_lang['error_only_member']   	= "{LANG.member.error_only_member}";
	 js_lang['are_you_sure_del']   	= "{LANG.member.are_you_sure_del}"; 	 
</script> 
<div class="box-nav-menu row">
	<div class="col-xs-6">
  	{data.nav_control}
  </div>
  <div class="col-xs-6">
  	 <div class="nav-action"><a href="{data.link_add}" class="write"><i class="fa fa-plus-square"></i> <span>{LANG.member.compose_mail}</span></a></div>
  </div> 
</div>


<div id="boxMessage">
	<div class="message_title"><div class="row">
  	
  	<div class="col-xs-10">
    	<h2>{data.mail_subject}</h2>
      <div class="sender">{LANG.member.sender} : <span class="fUsername">{data.from_name}</span></div>
      <div class="receiver">{LANG.member.receiver} : <span class="fUsername">{data.to_name}</span></div>
    </div>
    <div class="col-xs-2">
    	{data.mail_date}
    </div>
  </div></div>
  
  <div class="message_content">
  	{data.mail_content}
  </div> 
</div> 
<br />

<!-- END: html_view -->
