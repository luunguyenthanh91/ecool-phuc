<?php
/*================================================================================*\
|| 							Name code : function_module.php 		 		 											  # ||
||  				Copyright © 2007 by Thai Son - CMS vnTRUST                					# ||
\*================================================================================*/
/**
* @version : 1.0
* @date upgrade : 17/12/2007 by Thai Son
**/

if ( !defined('IN_vnT') )	{ die('Access denied');	}
define("LINK_ACTION", LINK_MOD . '/mailbox'); 
 


/*----- get_mail_to -----*/
function get_mail_to ($text){
	global $func,$DB,$conf;
	$textout = array();
	 
	$res = $DB->query("SELECT mem_id,username,full_name FROM members WHERE m_status=1 AND (email='".$text."' OR username='".$text."' )");
	if($row = $DB->fetch_row($res))
	{
		$textout['mem_id'] = (int)$row['mem_id'];
		$textout['username'] = $row['username'];
		$textout['full_name'] = $row['full_name'];
	}else{
		$textout['mem_id'] = 0;
		$textout['username'] = '';
		$textout['full_name'] = '';
	}
	
	return $textout;
} 

/*----- List_User -----*/
function List_User($did=-1,$submit){
global $func,$DB,$conf;

	if ($submit)
		$text= "<select size=1  name=\"mem_id\" onChange=\"submit();\" style='width:50%' >";
	else
		$text= "<select size=1  name=\"mem_id\" style='width:50%' >";
	
	$query = $DB->query("SELECT * FROM members order by username ");
	while ($row=$DB->fetch_row($query)) {
		$user_group = getGroupName($row['mem_group']);
		if ($row['mem_id']==$did)
			$text.="<option value=\"{$row['mem_id']}\" selected>{$row['username']} [$user_group]</option>";
		else
			$text.="<option value=\"{$row['mem_id']}\" >{$row['username']} [$user_group]</option>";
	}
	$text.="</select>";
	return $text;
}


/*----- get_username -----*/
function get_username ($mem_id){
	global $func,$DB,$conf;
	$query = $DB->query("SELECT * FROM members WHERE mem_id={$mem_id}");
	if ($row=$DB->fetch_row($query)) {
		$output.=$row["username"];
	}else $output.="Unknown";
	
	return $output;
}
 
function render_row($row_info) {
	global $func,$DB,$vnT;
	$row=$row_info;
// Xu ly tung ROW
	$id = $row['mail_id'] ;
	$row_id = "row_".$row['mail_id'];
	
	$link_view = LINK_ACTION.".html/?do=view&id=".$id;
	$link_reply =  LINK_ACTION.".html/?do=reply&id=".$id; 
	
	if($row['sendbox']){
 		$link_del	=  LINK_ACTION.".html/?do=sendbox&do_action=del&id=".$id. '&ext=' . $row['ext_page'];
	}else{
		$link_del	=  LINK_ACTION.".html/?do_action=del&id=".$id. '&ext=' . $row['ext_page'];
	}
	
	$output['check_box'] = "<input type=\"checkbox\" name=\"del_id[]\" value=\"{$id}\" class=\"checkbox\" onclick=\"vnTpost.select_row('{$row_id}')\"  >"; 
	
	$output['mail_from'] = $row['mail_from']; 
	if ($row['mail_from']) {
		//$output['sender'] = "<b>".$row['sender_name']."</b>";		
		$output['sender'] = "<b>".$row['from_name']."</b>";
	  $output['reply'] ="<a href=\"{$link_reply}\"><img src=\"".MOD_DIR_IMAGE."/ads_edit.gif\" align=\"absmiddle\" /></a>";
	}else{
		$output['sender'] = "Guest";
	 	$output['reply'] ="---";	 
	}  	
	
	$output['mail_to']= $row['mail_to'];
	//$output['receiver'] = ($row['receiver_name']) ? $row['receiver_name'] : " --- ";
	$output['receiver'] = ($row['to_name']) ? $row['to_name'] : " --- ";
	
	
	
	
	if ($row['mail_status']==1){
		$output['mail_subject'] = "<a href=\"{$link_view}\">".$row['mail_subject']."</a>";
		$output['mail_status']= '<span class="mail_status1">'.$vnT->lang['member']['read'].'</span>';
	}else{
		$output['mail_subject'] = "<a href=\"{$link_view}\"><strong>".$row['mail_subject']."</strong></a>";
		$output['mail_status']="<b class='mail_status2'>".$vnT->lang['member']['unread']."</b>";
	}		
	$output['mail_date'] = @date ("H:i ,d/m/Y",$row['mail_date']);
 	 	
	$output['delete'] = '<a href="javascript:void(0);" onClick="vnTpost.del_item(\''.$link_del.'\')" ><img src="' . MOD_DIR_IMAGE. '/delete.gif"  alt="Delete "></a>';
		 
	return $output;
} 
  

?>