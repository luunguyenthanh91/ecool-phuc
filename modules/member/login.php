<?php
/*================================================================================*\
|| 							Name code : main.php 		 		 																	  # ||
||  				Copyright © 2007 by Thai Son - CMS vnTRUST                					# ||
\*================================================================================*/
/**
 * @version : 1.0
 * @date upgrade : 11/12/2007 by Thai Son
 **/
if (! defined('IN_vnT')) {
  die('Access denied');
}
$nts = new sMain();

class sMain
{
  var $output = "";
  var $skin = "";
  var $linkUrl = "";
  var $module = "member";
  var $action = "login";

  function sMain (){
    global $vnT, $input;
    include ("function_" . $this->module . ".php");
    loadSetting();
    $this->skin = new XiTemplate(DIR_MODULE . "/" . $this->module . "/html/" . $this->action . ".tpl");
    $this->skin->assign('DIR_MOD', DIR_MOD);
    $this->skin->assign('LANG', $vnT->lang);
    $this->skin->assign('INPUT', $input);
    $this->skin->assign('CONF', $vnT->conf);
    $this->skin->assign('DIR_IMAGE', $vnT->dir_images);
    $vnT->html->addStyleSheet(DIR_MOD . "/css/" . $this->module . ".css");
    $vnT->html->addScript(DIR_MOD . "/js/" . $this->module . ".js");
    // extra title
    $vnT->conf['indextitle'] = $vnT->lang['member']['title_login'];
	  if ($vnT->user['mem_id'] > 0) {
      $linkref = LINK_MOD . ".html";
			$vnT->func->header_redirect($linkref) ;  
    }
    $data['main'] = $this->do_Login();
		$navation = get_navation($vnT->lang['member']['title_login']);
    $data['navation'] = $vnT->lib->box_navation($navation);
    //$vnT->setting['banner'] = $vnT->lib->get_child_slide('child');
    $data['fixed_sidebar'] = $vnT->lib->fixed_sidebar();
    $this->skin->assign("data", $data);
    $this->skin->parse("modules");
    $vnT->output .= $this->skin->text("modules");
  }
  function do_Login (){
    global $vnT, $input, $func, $DB, $conf;
    $err = "";
		$ref = $_SERVER['HTTP_REFERER'];
		if (isset($input['ref']) && $input['ref'] != "") {
      $ref = $input['ref'];
    } else
      $ref = $vnT->func->base64url_encode(LINK_MOD.'.html');
    if ($input['do_login']){
      $data = $input;
			$ref = $input['ref'];
      $username = $input['username'];
      $password = $vnT->func->md10($input['password']);
      $check_qr = $DB->query("SELECT * FROM members WHERE username='{$username}' AND password='{$password}' ");
      if ($info = $DB->fetch_row($check_qr)){
        if ($info['m_status'] == 0) {
          if ($vnT->setting['active_method'] == 2) {
            $mess = $vnT->lang['member']['account_not_active_BQT'];
          } else {
            $link = LINK_MOD . '/send_code.html';
            $mess = str_replace("{link}", $link, $vnT->lang['member']['account_not_active']);
          }
          $err .= $vnT->func->html_err($mess);
        }
        if ($info['m_status'] == 2) {
          $err .= $vnT->func->html_err($vnT->lang['member']['account_ban']);
        }
      } else
        $err .= $vnT->func->html_err($vnT->lang['member']['mess_login_failt']);
      if (empty($err)){
				$vnT->func->vnt_set_member_cookie($info['mem_id'],$_POST['ch_remember']);
        $kq = $vnT->func->User_Login($info);
        if ($kq) {
          $vnT->DB->query("UPDATE members SET last_login=".time().", num_login=num_login+1 
                          WHERE mem_id=" . $info['mem_id'] . " ");
          $link_ref = $vnT->func->base64url_decode($ref);
          $vnT->func->header_redirect($link_ref);
        } else {
          $err = $vnT->func->html_err($vnT->lang['member']['mess_login_failt']);
				}
      }
    }
		$data['box_mess_join'] = $vnT->setting["mess_join"];
		$data['ref'] = $ref;
    $data['err'] = $err;
    if(isset($_SESSION['mess']) && $_SESSION['mess']!=''){
      $data['err'] = 	$_SESSION['mess'];
      unset($_SESSION['mess']);
    }
    $data['link_lostpass'] = LINK_MOD . "/forget_pass.html";
    $data['link_register'] = LINK_MOD . "/register.html";
    $data['facebook_appId'] = $vnT->setting['social_network_setting']['facebook_appId'];
		$data['google_appId'] = $vnT->setting['social_network_setting']['google_apikey'];
    $data['link_action'] = LINK_MOD . "/login.html"  ;
    $this->skin->assign("data", $data);
    $this->skin->parse("login");
    $nd['content'] = $this->skin->text("login");
    $nd['f_title'] = '<h1>'.$vnT->lang['member']['title_login'].'</h1>';
    return $vnT->skin_box->parse_box("box_middle2", $nd);
  }
}
?>