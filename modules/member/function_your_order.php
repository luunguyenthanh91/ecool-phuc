<?php
/*================================================================================*\
|| 							Name code : function_rental.php 		 		 											  # ||
||  				Copyright � 2007 by Thai Son - CMS vnTRUST                					# ||
\*================================================================================*/
/**
 * @version : 1.0
 * @date upgrade : 17/12/2007 by Thai Son
 **/

define("UPLOAD_PRO", PATH_ROOT . '/vnt_upload/product/');
define("DIR_UPLOAD_PRO", ROOT_URL . 'vnt_upload/product');
define("LINK_PRO", $vnT->link_root . $vnT->setting['seo_name'][$vnT->lang_name]['product']);
define("LINK_ACTION", LINK_MOD . '/your_order');

/*-------------- loadSetting_Pro --------------------*/
function loadSetting_Pro ()
{
	global $vnT, $func, $DB, $conf;
	$setting = array();
	$result = $DB->query("select * from product_setting WHERE id=1");
	$setting = $DB->fetch_row($result);
	if (is_array($setting)) {
		foreach ($setting as $k => $v) {
			$vnT->setting_pro[$k] = stripslashes($v);
		}
	}
	unset($setting);

	$res_s = $DB->query("SELECT * FROM order_status WHERE display=1 ORDER BY s_order ASC");
	while($row_s = $DB->fetch_row($res_s))
	{
		if($row_s['is_default']==1)	 {
			$vnT->setting_pro['status_default']	= $row_s['status_id'];
		}
		if($row_s['is_complete']==1)	 {
			$vnT->setting_pro['status_complete']	= $row_s['status_id'];
		}
		if($row_s['is_payment']==1)	 {
			$vnT->setting_pro['status_payment']	= $row_s['status_id'];
		}
		if($row_s['is_cancel']==1)	 {
			$vnT->setting_pro['status_cancel']	= $row_s['status_id'];
		}
		if($row_s['is_customer']==1)	 {
			$vnT->setting_pro['status_customer']	= $row_s['status_id'];
		}

		$vnT->setting_pro['status'][$row_s['status_id']] =  "<span class='status' style='color:#".$row_s['color']."'>" . $func->fetch_array($row_s['title']) . "</span>";
		$vnT->setting_pro['arr_status'][$row_s['status_id']]['id'] = $row_s['status_id'] ;
		$vnT->setting_pro['arr_status'][$row_s['status_id']]['title'] =  $func->fetch_array($row_s['title']) ;
		$vnT->setting_pro['arr_status'][$row_s['status_id']]['color'] =  $row_s['color'] ;

	}


}




//----- get_info_address
function get_info_address ($data, $type = "")
{
	global $DB, $input, $func, $vnT, $conf;
	if ($type == "shipping") {

		$text = '<div class="row-info clearfix"><div class="attr-name">'.$vnT->lang['member']['full_name'].'</div><div class="attr-value">'.$data['c_name'].'</div></div>';
		if ($data['c_company']){
			$text .= '<div class="row-info clearfix"><div class="attr-name">'.$vnT->lang['member']['company'].'</div><div class="attr-value">'.$data['c_company'].'</div></div>';
		}


		$text .= '<div class="row-info clearfix"><div class="attr-name">'.$vnT->lang['member']['address'].'</div><div class="attr-value">';
		$text .= $data['c_address'];
		if ($data['c_city'])    $text .= "," . $vnT->lib->get_city_name($data['c_city']);
		if ($data['c_state'])    $text .= ", " . $vnT->lib->get_state_name($data['c_state']);
		if ($data['c_zipcode'])   $text .= "," . $data['c_zipcode'];
		if ($data['c_country']) $text .= ", " .  $vnT->lib->get_country_name($data['c_country']);
		$text .= '</div></div>';

		if ($data['c_phone']){
			$text .= '<div class="row-info clearfix"><div class="attr-name">'.$vnT->lang['member']['phone'].'</div><div class="attr-value">'.$data['c_phone'].'</div></div>';
		}

	} else {

		$text = '<div class="row-info clearfix"><div class="attr-name">'.$vnT->lang['member']['full_name'].'</div><div class="attr-value">'.$data['d_name'].'</div></div>';
		if ($data['d_company']){
			$text .= '<div class="row-info clearfix"><div class="attr-name">'.$vnT->lang['member']['company'].'</div><div class="attr-value">'.$data['d_company'].'</div></div>';
		}

		$text .= '<div class="row-info clearfix"><div class="attr-name">'.$vnT->lang['member']['address'].'</div><div class="attr-value">';
		$text .= $data['d_address'];
		if ($data['d_city'])    $text .= "," . $vnT->lib->get_city_name($data['d_city']);
		if ($data['d_state'])    $text .= ", " . $vnT->lib->get_state_name($data['d_state']);
		if ($data['d_zipcode'])   $text .= "," . $data['d_zipcode'];
		if ($data['d_country']) $text .= ", " .  $vnT->lib->get_country_name($data['d_country']);
		$text .= '</div></div>';

		if ($data['d_phone']){
			$text .= '<div class="row-info clearfix"><div class="attr-name">'.$vnT->lang['member']['phone'].'</div><div class="attr-value">'.$data['d_phone'].'</div></div>';
		}

		if ($data['d_email']){
			$text .= '<div class="row-info clearfix"><div class="attr-name">Email </div><div class="attr-value">'.$data['d_email'].'</div></div>';
		}

	}
	return $text;
}

//----- get_method_name
function get_method_name ($table, $name)
{
	global $DB, $input, $func, $vnT, $conf;
	$text = "";
	$reuslt = $DB->query("select title from {$table} where name='{$name}' ");
	if ($row = $DB->fetch_row($result)) {
		$text = $func->fetch_array($row['title']);
	}
	return $text;
}


/*-------------- get_price_product --------------------*/
function get_price_product ($price,$default="0 VND"){
	global $func,$DB,$conf,$vnT;
	if ($price){
		$price = $vnT->func->format_number($price). " VND";
	}else{
		$price = $default;
	}
	return $price;
}

function List_Status_Order ($did)
{
  global $func, $DB, $conf, $vnT;
  $text = "<select name=\"status\" id=\"status\"  class='form-control' >";
	$text .= "<option value=\"0\" >" . $vnT->lang['member']['select_status_order'] . "</option>";
  foreach ($vnT->setting_pro['arr_status'] as $key => $value )
	{
    $selected =  ( $key == $did) ?  " selected" : ""  ;
    $text .= "<option value=\"{$key}\" {$selected} >" . $value['title'] . "</option>";
  }
  $text .= "</select>";
  return $text;
}
function get_option_size($info,$did=0){
	global $vnT,$input;
	$out="";
	$sql = "SELECT * FROM product_size where size_id IN (".$info['size'].") order by display_order ASC, size_id ASC ";
	$result = $vnT->DB->query($sql);
	while($row = $vnT->DB->fetch_row($result)){
		$selected = ($did == $row['size_id']) ? " selected" : "" ;
		$text .=  '<option value="'.$row['size_id'].'" '.$selected.' >'.$row['title'].'</option>';
	}
	return $text;
}
function get_list_name($order_id){
	global $DB,$vnT;
	$text = '';
	$order_id = (int) $order_id;
	$result = $DB->query("SELECT item_title FROM order_detail WHERE order_id = $order_id");
	if($num = $DB->num_rows($result)){
		$i = 1;
		while ($row = $DB->fetch_row($result)) {
			if($i==$num)
				$text .= $vnT->func->HTML($row['item_title']).', '.$vnT->func->HTML($row['item_title']);
			else
				$text .= $vnT->func->HTML($row['item_title']).', ';
		}
	}
	return $text;
}

?>