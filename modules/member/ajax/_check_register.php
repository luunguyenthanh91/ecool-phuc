<?
include ("../../../_config.php");
include ("../../../includes/class_db.php");
$DB = new DB();
$nts = new sMain();

class sMain
{
  var $output = "";
  var $baseurl = "";
  var $html = array();
  var $lang = array();

  // Start func
  function sMain ()
  {
    global $DB, $conf;
    $jsout = "";
    switch ($_GET["do"]) {
      case 'email':
        {
          $email = $_GET['email'];
          $check_qr = $DB->query("SELECT email FROM members WHERE email='{$email}' ");
          if ($exist = $DB->fetch_row($check_qr)) {
            $this->return_string('found');
            break;
          } else {
            $this->return_string('notfound');
            break;
          }
        }
        ;
      break;
      case 'username':
        {
          $username = $_GET['username'];
          $check_qr = $DB->query("SELECT username FROM members WHERE username='{$username}' ");
          if ($exist = $DB->fetch_row($check_qr)) {
            $this->return_string('found');
            break;
          } else {
            $this->return_string('notfound');
            break;
          }
        }
        ;
      break;
    }
  }

  /*-------------------------------------------------------------------------*/
  // Return string
  /*-------------------------------------------------------------------------*/
  function return_string ($string)
  {
    @header("Content-type: text/plain");
    $this->print_nocache_headers();
    echo $string;
    exit();
  }

  /*-------------------------------------------------------------------------*/
  // Print no cache headers
  /*-------------------------------------------------------------------------*/
  function print_nocache_headers ()
  {
    header("HTTP/1.0 200 OK");
    header("HTTP/1.1 200 OK");
    header("Cache-Control: no-cache, must-revalidate, max-age=0");
    header("Expires: 0");
    header("Pragma: no-cache");
  }
  // end class
}
?>