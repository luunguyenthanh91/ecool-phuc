<?php
@ini_set("display_errors", "0");
session_start();
define('IN_vnT', 1);
define('PATH_ROOT', dirname(__FILE__));
define('DS', DIRECTORY_SEPARATOR);

require_once("../../../_config.php");
require_once ($conf['rootpath'] . 'includes' . DS . 'defines.php');
require_once($conf['rootpath']."includes/class_db.php");
$vnT->DB= $DB = new DB;
require_once ($conf['rootpath']."includes/class_functions.php");
$vnT->func = $func = new Func_Global();
$vnT->conf = $conf = $func->fetchDbConfig($conf);
require "../../../includes/JSON.php";

$vnT->lang_name = ($_GET['lang']) ? $_GET['lang'] : "vn";

switch ($_GET['do']) {
  case "check_username":    $jsout = check_username();  break;
  case "check_email":    $jsout = check_email();  break;
  default:    $jsout = "Error";  break;
}

//-----------
function check_username ()
{
  global $DB, $func, $conf, $vnT;

  $username = $_GET['rUserName'];

  if (!$username) {
    return  'false';
  }

  $arr_folder = array("index","admin","blocks","cache","db_backup","includes","js","language","libraries","modules","plugins","rss","skins","vnt_upload","about","page","sitemap","contact","main","member","business","product","estore","news","faqs","trangchu", "administrator","bigmark","hinhanh","photo","amnhac","music","phim","movie","game","play","mp3");

  if(in_array($username,$arr_folder))
  {
    return  'false';
  }

  if (!preg_match('/^[a-z0-9-_]+$/', $username)) {
    return  'false';
  }
  if (strstr($username,"/")) {
    return  'false';
  }

  $check_qr = $DB->query("SELECT username FROM members WHERE username='{$username}' ");
  if ($exist = $DB->fetch_row($check_qr)) {
    return  'false';
  }

  $check_qr = $DB->query("SELECT name_url FROM estore WHERE name_url='{$username}' ");
  if ($exist = $DB->fetch_row($check_qr)) {
    return  'false';
  }

  return 'true';
}

//-----------
function check_email ()
{
  global $DB, $func, $conf, $vnT;

  $email = $_GET['rEmail'];
  $check_qr = $DB->query("SELECT email FROM members WHERE email='{$email}' ");
  if ($exist = $DB->fetch_row($check_qr)) {
    return  'false';
  } else {
    return 'true';
  }
}

echo $jsout;
$vnT->DB->close();
?>