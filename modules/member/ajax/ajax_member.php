<?php
define('IN_vnT', 1);
require_once ("../../../_config.php");
require_once ("../../../includes/class_db.php");
require_once ("../../../includes/class_functions.php");
$DB = new DB();
$func = new Func_Global();
$conf = $func->fetchDbConfig($conf);
$vnT->lang_name = ($_GET['lang']) ? $_GET['lang'] : "en";
$func->load_language('member');
switch ($_GET['do']) {
  case "list_city":
    $jsout = get_list_city();
  break;
  case "list_state":
    $jsout = get_list_state();
  break;
  case "check_username":
    $jsout = check_username();
  break;
  case "check_email":
    $jsout = check_email();
  break;
  default:
    $jsout = "Error";
  break;
}

/**
 * function get_list_city 
 * 
 **/
function get_list_city ()
{
  global $DB, $func, $conf, $vnT;
  $textout = "";
  $country = $_GET['country'];
  $textout = "<select name=\"city\" id=\"city\" class='select'      >";
  $textout .= "<option value=\"\" selected>-- " . $vnT->lang['member']['select_city'] . " --</option>";
  $sql = "SELECT * FROM iso_cities where display=1 and country='$country'  order by c_order ASC , id DESC  ";
  //	echo $sql;
  $result = $DB->query($sql);
  while ($row = $DB->fetch_row($result)) {
    if ($row['code'] == $did) {
      $textout .= "<option value=\"{$row['code']}\" selected>" . $func->HTML($row['name']) . "</option>";
    } else {
      $textout .= "<option value=\"{$row['code']}\">" . $func->HTML($row['name']) . "</option>";
    }
  }
  $textout .= "</select>";
  return $textout;
}

//get_list_state
function get_list_state ()
{
  global $DB, $func, $conf, $vnT;
  $textout = "";
  $city = $_GET['city'];
  $sql = "SELECT * FROM iso_states where display=1 and city='$city'   order by s_order ASC ,id DESC ";
  //	echo $sql;
  $result = $DB->query($sql);
  if ($num = $DB->num_rows($result)) {
    $textout = "<select name=\"state\" id=\"state\" class='select'   >";
    $textout .= "<option value=\"\" selected>-- " . $vnT->lang['member']['select_state'] . " --</option>";
    while ($row = $DB->fetch_row($result)) {
      if ($row['code'] == $did) {
        $textout .= "<option value=\"{$row['code']}\" selected>" . $func->HTML($row['name']) . "</option>";
      } else {
        $textout .= "<option value=\"{$row['code']}\">" . $func->HTML($row['name']) . "</option>";
      }
    }
    $textout .= "</select>";
  } else {
    $textout = '<input type="text"   class="textfiled" value="" name="state" id="state" />';
  }
  return $textout;
}

//check_username
function check_username ()
{
  global $DB, $func, $conf, $vnT;
  $username = $_POST['user_name'];
  if ($username) {
    $check_qr = $DB->query("SELECT username FROM members WHERE username='{$username}' ");
    if ($exist = $DB->fetch_row($check_qr)) {
      $textout = "exist";
    } else {
      $textout = "yes";
    }
  } else {
    $textout = "invalid";
  }
  return $textout;
}

//check_email
function check_email ()
{
  global $DB, $func, $conf, $vnT;
  $email = $_POST['email_address'];
  $chars = "/^([a-z0-9+_]|\\-|\\.)+@(([a-z0-9_]|\\-)+\\.)+[a-z]{2,6}\$/i";
  if (strpos($email, '@') !== false && strpos($email, '.') !== false) {
    if (preg_match($chars, $email)) {
      $check_qr = $DB->query("SELECT email FROM members WHERE email='{$email}' ");
      if ($exist = $DB->fetch_row($check_qr)) {
        $textout = "exist";
      } else {
        $textout = "yes";
      }
    } else {
      $textout = "invalid";
    }
  } else {
    $textout = "invalid";
  }
  return $textout;
}
echo $jsout;
?>