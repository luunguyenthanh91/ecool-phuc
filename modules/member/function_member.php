<?php
/*================================================================================*\
|| 							Name code : function_member.php 		 		 											  # ||
||  				Copyright © 2007 by Thai Son - CMS vnTRUST                					# ||
\*================================================================================*/
/**
 * @version : 1.0
 * @date upgrade : 17/12/2007 by Thai Son
 **/
define("DIR_MOD", ROOT_URI . "modules/member");
define('MOD_DIR_UPLOAD', ROOT_URI . 'vnt_upload/member'); 
define('PATH_UPLOAD', PATH_ROOT . '/vnt_upload/member');
define("MOD_DIR_IMAGE", ROOT_URI . 'modules/member/images');
define("LINK_MOD", $vnT->link_root . "member");

function loadSetting (){
  global $vnT, $func, $DB, $conf;
  $setting = array();
  $result = $DB->query("select * from member_setting WHERE lang='$vnT->lang_name' ");
  $setting = $DB->fetch_row($result);
  foreach ($setting as $k => $v) {
    $vnT->setting[$k] = stripslashes($v);
  }
	$vnT->setting['arr_gender'] = array("1"=> $vnT->lang['member']['male'],"2"=>$vnT->lang['member']['female'] );
  $res_group = $vnT->DB->query("SELECT * FROM mem_group WHERE display=1 ORDER BY g_order ASC");
  while($row_group = $vnT->DB->fetch_row($res_group))	{
    $vnT->setting['arr_mem_group'][$row_group['g_id']] = $row_group;
  }
  unset($setting);
}
function load_html ($file, $data){
  global $vnT, $input;
  $html = new XiTemplate(DIR_MODULE . "/member/html/" . $file . ".tpl");
  $html->assign('DIR_MOD', DIR_MOD);
  $html->assign('LANG', $vnT->lang);
  $html->assign('INPUT', $input);
  $html->assign('CONF', $vnT->conf);
  $html->assign('DIR_IMAGE', $vnT->dir_images);
  $html->assign("data", $data);
  $html->parse($file);
  return $html->text($file);
}
function nav_login($act='login'){
  global $vnT,$input,$conf;
  $text = '';
  $arr[0]['act'] = 'login';
  $arr[0]['title'] = $vnT->lang['member']['title_login'];

  $arr[1]['act'] = 'register';
  $arr[1]['title'] = $vnT->lang['member']['title_register'];

  $text = '<ul>';
  for($i=0;$i<sizeof($arr);$i++){
    $active = ($act==$arr[$i]['act']) ? 'class="current"' : '';
    $link = LINK_MOD.'/'.$arr[$i]['act'].'.html';
    $text.='<li '.$active.'><a href="'.$link.'">'.$arr[$i]['title'].'</a></li>';
  }
  $text .= '</ul>';
  $nd['f_title'] = $vnT->lang['member']['select_menu'];
  $nd['content'] = $text;
  // $textout .= $vnT->skin_box->parse_box("box_cat",$nd);
  return $textout;
}
function nav_member($act='update_account'){
  global $vnT,$input,$conf;
  $arr_out = array();
  $arr[0]['act'] = 'update_account';
  $arr[0]['fonts'] = 'fa-user';
  $arr[0]['title'] = $vnT->lang['member']['update_account'];

  $arr[1]['act'] = 'changepass';
  $arr[1]['fonts'] = 'fa-key';
  $arr[1]['title'] = $vnT->lang['member']['f_changepass'];

  $arr[2]['act'] = 'your_order';
  $arr[2]['fonts'] = 'fa-clone';
  $arr[2]['title'] = $vnT->lang['member']['f_your_order'];

  $arr[3]['act'] = 'logout';
  $arr[3]['fonts'] = 'fa-clone';
  $arr[3]['title'] = $vnT->lang['member']['f_logout'];

  $text = '<ul>';
  for($i=0;$i<sizeof($arr);$i++){
    $active = ($act==$arr[$i]['act']) ? 'class="active"' : '';
    $link = LINK_MOD.'/'.$arr[$i]['act'].'.html';
    $text.='<li '.$active.'><a href="'.$link.'">'.$arr[$i]['title'].'</a></li> ';
  }
  $text .= '</ul>';
  $nd['f_title'] = $vnT->lang['member']['select_menu'];
  $nd['content'] = $text;
  $arr_out['pc'] = '<div class="menuMember">'.$text.'</div>';
  // $arr_out['mb'] = $vnT->skin_box->parse_box("box_cat",$nd);
  return $arr_out;
}
function get_navation ($text=""){
  global $DB,$conf,$func,$vnT,$input;
  $output= '<ul><li><a itemprop="url" href="'.$vnT->link_root.'"><span itemprop="title"><i class="fa fa-home"></i></span></a></li> ';
  $output .= "<li><a href='".LINK_MOD.".html'>".$vnT->lang['member']['member']."</a></li>" ;
  $output .= '<li>'.$text.'</li>';
  $output .= '</ul>';
  return $output;
}
function box_usercp (){
  global $vnT,$input;
  $data['avatar'] =  get_avatar($vnT->user['avatar'], 170);
  $src_photocover = ($vnT->user['photocover']) ?   ROOT_URL . 'vnt_upload/member/cover/' . $vnT->user['photocover']  : MOD_DIR_IMAGE."/nocover.jpg";
  $data['photocover'] = '<img src="' . $src_photocover. '"  id="idphotocover" alt="">';
  $data['avatar'] .= '<div class="editavatar"><a href="javascript:void(0)" onClick="vnTMember.do_EditAvatar()" >Sửa</a></div>';
  $data['photocover'] .= '<div class="editcover"><a href="javascript:void(0)" onClick="vnTMember.do_EditCover()" >Sửa</a></div>';
  $data['header_info'] = '<b class="username">' . $vnT->user['full_name'] . '</b>';
  $res_pm = $vnT->DB->query ("SELECT mail_to FROM mailbox WHERE mail_to = ".$vnT->user['mem_id']."AND mail_status=0 ");
  $num_pm = $vnT->DB->num_rows($res_pm);
  $num_email = ($num_pm) ? "<span class='num'>".$num_pm."</span>" : "";

  $link_post1 = LINK_MOD."/dang-tin.html";
  $link_post2 = LINK_MOD."/dang-tin.html/?tab=2";
  $link_mailbox = LINK_MOD."/mailbox.html";

  $header_tool = '<ul>';
  $header_tool .= '<li><a href="'.$link_post1.'" class="fa-edit">Đăng tin</a></li>';
  $header_tool .= '<li><a href="'.$link_mailbox.'" class="fa-envelope-o">Hộp thư'.$num_email.'</a></li>';
  $header_tool .= '</ul>';
  $data['header_tool'] = $header_tool;

  return load_html("box_usercp",$data);
}
function check_member_login (){
	global $vnT, $conf ;
	if($vnT->user['mem_id']==0) {
		$link_login = LINK_MOD."/login.html/?ref=".$vnT->seo_url;
		$vnT->func->header_redirect($link_login);  	
	}	
}
function get_group_default (){
  global $vnT, $func, $DB, $conf;
  $text = 1;
  $sql = "select * from mem_group where is_default=1 ";
  $result = $DB->query($sql);
  if ($row = $DB->fetch_row($result)) {
    $text = (int) $row['g_id'];
  }
  return $text;
}
function List_Gender ($did ,$show="radio", $ext=""){
	global $vnT,$func, $DB, $conf;
	$text = "";
  if($show=="select"){
    $text = '<select name="gender" id="gender" class="form-control" >';
    $text .= "<option value=\"\" >Giới tính</option>";
    foreach ($vnT->setting['arr_gender'] as $key => $value)
    {
      $selected = ($did==$key) ? " selected " : "";
      $text .= "<option value=\"{$key}\" ".$selected." >" . $value . "</option>";
    }
    $text .= '</select>';
  }else{
    foreach ($vnT->setting['arr_gender'] as $key => $value)
    {
      $checked = ($did==$key) ? " checked " : "";
      $text .= '<input name="gender" id="gender" type="radio" value="'.$key.'" '.$checked.' align="absmiddle" /> '.$value.' &nbsp; '	 ;
    }
  }
	return  $text ;
}
function ListDay ($did){
  global $input, $conf, $vnT;
  $text = '<select name="day"  style="width:100px; text-align:center;" class="select form-control" >';
  $text .= "<option value=\"0\" >" . $vnT->lang['member']['day'] . "</option>";
  for ($i = 1; $i <= 31; $i ++) {
    if ($i < 10)
      $value = "0" . $i;
    else
      $value = $i;
    if ($value == $did) {
      $text .= "<option value=\"{$value}\" selected >" . $value . "</option>";
    } else {
      $text .= "<option value=\"{$value}\" >" . $value . "</option>";
    }
  }
  $text .= '</select>';
  return $text;
}
function ListMonth ($did){
  global $input, $conf, $vnT;
  $text = '<select name="month"  style="width:100px; text-align:center;"  class="select form-control" >';
  $text .= "<option value=\"0\" >" . $vnT->lang['member']['month'] . "</option>";
  for ($i = 1; $i <= 12; $i ++) {
    if ($i < 10)
      $value = "0" . $i;
    else
      $value = $i;
    if ($value == $did) {
      $text .= "<option value=\"{$value}\" selected >" . $value . "</option>";
    } else {
      $text .= "<option value=\"{$value}\" >" . $value . "</option>";
    }
  }
  $text .= '</select>';
  return $text;
}
function ListYear ($did){
  global $input, $conf, $vnT;
  $text = '<select name="year"  style="width:100px; text-align:center;"  class="select form-control" >';
  $text .= "<option value=\"0\" >" . $vnT->lang['member']['year'] . "</option>";
  for ($i = 1900; $i <= date("Y"); $i ++) {
    if ($i < 10)
      $value = "0" . $i;
    else
      $value = $i;
    if ($value == $did) {
      $text .= "<option value=\"{$value}\" selected >" . $value . "</option>";
    } else {
      $text .= "<option value=\"{$value}\" >" . $value . "</option>";
    }
  }
  $text .= '</select>';
  return $text;
}
function get_avatar($picture, $w = ""){
  global $vnT, $DB, $func;
  $out = "";
  if ($picture) {
    $src = ROOT_URI . "vnt_upload/member/avatar/" . $picture;
  } else {
    $src = ROOT_URI . "vnt_upload/member/avatar/noavatar.gif";
  }
  $out = "<img  src=\"{$src}\" width=\"{$w}\" alt='Avatar'  />";
  return $out;
}
function ShowTable ($table){
  global $Template, $vnT, $func;
  $list = "";
  $numcol = count($table['title']);
  $rowfield = array();
  $rowalign = array();
  $rowextra = array();
  $data['f_title'] = $table['f_title'];
  $data['link_action'] = $table['link_action'];
  $data['link_del'] = $table['link_del'];
  $data['link_edit'] = $table['link_edit'];
  $data['link_display'] = $table['link_display'];
  $data['link_hidden'] = $table['link_hidden'];
  $data['numcol'] = $numcol;
  $data['button'] = $table['button'];
  // Title
  $list .= "<thead><tr>";
  while (list ($k, $v) = each($table['title'])) {
    $rowfield[] = $k;
    $tittle_arr = explode("|", $v);
    $title = $tittle_arr[0];
    $width = ($tittle_arr[1]) ? "width=\"".$tittle_arr[1]."\" " : "" ;
    $align = $tittle_arr[2];
    $extra = $tittle_arr[3];
    if (! empty($align))
      $align = "align=\"{$align}\"";
    $rowalign[] = $align;
    $rowextra[] = $extra;
    $list .= "<th  {$align} {$width} {$extra}>{$title}</th>";
  }
  $list .= "</tr></thead>\n";
  // End
  $list .= "<tbody>\n";
  // Row
  foreach ($table['row'] as $row) 
	{
    $list .= '<tr id="' . $row['row_id'] . '" ' . $row['extra'] . ' > ';
    for ($i = 0; $i < $numcol; $i ++) {
      $value = $row[$rowfield[$i]];
      $align = $rowalign[$i];
      $extra = $rowextra[$i];
      if ((empty($value)) && ($value != 0))   $value = "&nbsp;";        
      $list .= "<td  {$align} {$extra} >{$value}</td>";
    }
    $list .= "</tr>\n";
  }
  if ($table['extra'])
    $list .= '<tr><td colspan="' . $numcol . '" class="extra">' . $table['extra'] . '</td></tr>';
  $list .= "</tbody>\n";
  //end
  $data['list'] = $list; 
	 
  return load_html("box_manage",$data);
}
function box_sidebar (){
  global $vnT, $con, $input, $DB, $func;
  $output = '';
  if ($_SERVER['REQUEST_URI']) {
    $url = "/" . $vnT->func->base64url_encode($vnT->seo_url);
  } else {
    $url = "";
  }
  $link_login = LINK_MOD . "/login.html" . $url;
  $link_logout = LINK_MOD . "/logout.html" . $url;
  $num_email = (int) $vnT->setting['num_email'] ;

  if($vnT->user['mem_id']>0) {
    $arr_box['member'] = array(
      'act' => array("update_account" ,"changepass","logout"),
      'title' => array($vnT->lang['member']['update_account'] ,$vnT->lang['member']['f_changepass'],$vnT->lang['member']['logout_account']),
      'link' => array(LINK_MOD ."/update_account.html"  ,LINK_MOD ."/changepass.html","onClick=\"doLogout()\""),
    ) ;


    $arr_box['your_order'] = array(
      'act' => array( "your_order|manage"    ),
      'title' => array( "Danh sách đơn hàng"  ),
      'link' => array( LINK_MOD ."/your_order.html"  ),
    ) ;




  }else{
    $arr_box['member'] = array(
      'act' => array("regsiter","login","forget_pass","activate"),
      'title' => array($vnT->lang['member']['register_member'],$vnT->lang['member']['title_login'],$vnT->lang['member']['forget_password'],$vnT->lang['member']['title_active_member']),
      'link' => array("onClick=\"popupRegister()\"","onClick=\"popupLogin('')\"",LINK_MOD."/forget_pass.html",LINK_MOD."/activate.html"),
    ) ;
  }

  foreach ($arr_box as $box_name => $box)
  {
    $arr_act = $box['act'];
    $arr_title = $box['title'];
    $arr_link = $box['link'];

    $style = "";
    if($input['act']==$box_name){
      $style = "display:" ;
    }

    $list = '';

    for ($i=0;$i<count($arr_title);$i++) {

      $active = (($input['act']==$box_name) && ( $input['do'] == $arr_act[$i]) ) ? " class='current' " : "";

      if(strstr($arr_link[$i],"onClick=")) {
        $list.="<li {$active} ><a href=\"javascript:void(0)\" ".$arr_link[$i]."   ><span>".$arr_title[$i]."</span></a></li>";
      }else{
        $list.="<li {$active} ><a href=\"".$arr_link[$i]."\"   ><span>".$arr_title[$i]."</span></a></li>";
      }
    }

    $img = ($style == "display:none") ? MOD_DIR_IMAGE."/but_cong.png"  :  MOD_DIR_IMAGE."/but_tru.png" ;

    $list_menu .= '<li class="menu-item">
    <div class="menu-top">          	
      <div class="menu-title"><h2>'.$vnT->lang['member']["menu_".$box_name].'</h2></div>
      <div class="menu-toggle"><i class="icon">&nbsp;</i></div>
    </div>
    <div class="clear"></div>
    <div class="sub-menu" id="'.$box_name.'" style="'.$style.'" >         
    <ul >
      '.$list.'
    </ul>
    </div>
  </li>';

  }

  $output ='<div class="box-usercp"><ul id="user-menu">'.$list_menu.'</ul></div>';

  return $output;
}
?>