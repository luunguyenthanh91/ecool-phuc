<?php
/*================================================================================*\
|| 							Name code : product.php 		 		            	  ||
||  				Copyright @2008 by Thai Son - CMS vnTRUST                     ||
\*================================================================================*/
/**
 * @version : 2.0
 * @date upgrade : 09/01/2009 by Thai Son
 **/
if (!defined('IN_vnT')) {
  die('Access denied');
}

$nts = new sMain();

class sMain
{
  var $output = "";
  var $skin = "";
  var $linkUrl = "";
  var $module = "member";
  var $action = "your_order";

  function sMain(){
    global $vnT, $input;
    include("function_" . $this->module . ".php");
    loadSetting();
    include("function_" . $this->action . ".php");
    loadSetting_Pro();
    $this->skin = new XiTemplate(DIR_MODULE . "/" . $this->module . "/html/" . $this->action . ".tpl");
    $this->skin->assign('DIR_MOD', DIR_MOD);
    $this->skin->assign('MOD_DIR_IMAGE', MOD_DIR_IMAGE);
    $this->skin->assign('LANG', $vnT->lang);
    $this->skin->assign('DIR_JS', $vnT->dir_js);
    $this->skin->assign('INPUT', $input);
    $this->skin->assign('CONF', $vnT->conf);
    $this->skin->assign('DIR_IMAGE', $vnT->dir_images);
    $vnT->html->addStyleSheet(DIR_MOD . "/css/" . $this->module . ".css");
    $vnT->html->addScript(DIR_MOD . "/js/" . $this->module . ".js");
    $vnT->html->addStyleSheet($vnT->dir_js . "/jquery_ui/themes/base/ui.all.css");
    $vnT->html->addScript($vnT->dir_js . "/jquery_ui/ui.datepicker.js");
    $vnT->html->addScriptDeclaration("
			$(function() {
				$('.dates').datepicker({
					changeMonth: true,
					changeYear: true 
				});
			});
		");
    //check login
    check_member_login();
    switch ($input['do']) {
      case "edit" :
        $navation =  $vnT->lang['member']['edit_order'];
        $data['main'] = $this->do_Edit();
        break;

      case "detail" :
        $navation = $vnT->lang['member']['info_order'];
        $data['main'] = $this->do_Detail();
        break;
      case "del" :
        $data['main'] = $this->do_Del();
        break;
      default :
        $navation = $vnT->lang['member']['manage_order'];
        $data['main'] = $this->do_Manage();
        break;
    }
    $data['nav_member'] = nav_member('your_order','mb');
    $navation = get_navation($navation);
    $data['navation'] = $vnT->lib->box_navation($navation);
    $vnT->setting['banner'] = $vnT->lib->get_child_slide('child',$nav_member['mb']);
    $data['fixed_sidebar'] = $vnT->lib->fixed_sidebar();
    $this->skin->assign("data", $data);
    $this->skin->parse("modules");
    $vnT->output .= $this->skin->text("modules");
  }
  function do_Edit(){
    global $input, $vnT, $conf, $DB, $func;
    $id = (int)$input['id'];
    $mem_id = $vnT->user['mem_id'];
    if ($_POST['btnUpdate']) {
      if (is_array($_POST['quantity'])){
        foreach ($_POST['quantity'] as $key => $quantity){
          if($quantity==0){
            $vnT->DB->query("DELETE FROM order_detail WHERE order_id=".$id." AND id=".$key);
          }else{
            $vnT->DB->query("UPDATE order_detail SET quantity=".$quantity."  WHERE order_id=".$id." AND id=".$key);
          }
        }
      }
      $total_cart =0 ; $total_price =0 ;
      $res_d = $vnT->DB->query("SELECT * FROM order_detail WHERE order_id=".$id );
      while ($row_d = $vnT->DB->fetch_row($res_d)) {
        $total_cart +=  ($row_d['quantity'] * $row_d['price']);
      }
      $total_price = $total_cart;
      $dup['status'] = $vnT->setting_pro['status_default'] ;
      $dup['total_cart'] = $total_cart;
      $dup['total_price'] = $total_price;
      $dup['date_update'] = time();
      $dup['num_update'] = (int)$_POST['num_update'] +1 ;
      $ok = $vnT->DB->do_update("order_sum", $dup, "mem_id=" . $vnT->user['mem_id'] . " AND order_id=" . $id);
      if ($ok) {
        $mess = $vnT->func->html_mess($vnT->lang['member']['update_order_success'], " alert-autohide");
        //$vnT->func->header_redirect(LINK_ACTION . ".html/?do=detail&id=" . $id, $mess);
        $vnT->func->header_redirect(LINK_ACTION . ".html/?do=edit&id=" . $id , $mess);
      } else {
        $err = $vnT->func->html_err($vnT->lang['member']['err_failt']);
      }
    }
    $sql = "SELECT * FROM order_sum WHERE mem_id=$mem_id AND display=1 AND order_id =" . $id;
    $result = $DB->query($sql);
    if ($data = $DB->fetch_row($result)){
      if($data['status'] <> $vnT->setting_pro['status_default']){
        $vnT->func->header_redirect(LINK_ACTION . ".html");
      }
      // chi tiet gio hang
      $html_row_cart = "";
      $sql_cart = "select * from order_detail where order_id = " . $id;
      $result_cart = $vnT->DB->query($sql_cart);
      $x = 0;
      $w = 65;
      while ($row_cart = $vnT->DB->fetch_row($result_cart)) {
        $x++;
        $row_cart['stt'] = $x;
        $id_cart = $row_cart["id"];
        $item_type = $row_cart["item_type"];
        $item_id = $row_cart["item_id"];
        $price = $row_cart["price"];
        $total = ($price * $row_cart['quantity']);

        $link .= $vnT->link_root . $row_cart["item_link"];

        $src_pic = ($row_cart["item_picture"]) ? $vnT->func->get_src_modules($row_cart["item_picture"], $w, 0, 0) : ROOT_URL . "modules/product/images/nophoto.gif";

        $pic = "<a href='{$link}' target='_blank'><img src=\"{$src_pic}\" width=\"{$w}\" alt='" . $row_cart['item_title'] . "' /></a>";

        $item_title = "<table border='0' ><tr><td width='70'>" . $pic . "</td><td><a href='" . $link . "' target='_blank' >" . $row_cart['item_title'] . "</a></td></tr></table>";

        $row_cart['pic'] = $pic;
        $row_cart['item_title'] = $item_title;

        $row_cart['price'] = ($row_cart['item_price']) ? get_price_product($row_cart['item_price']) : get_price_product($row_cart['price']);

        $class_qty = ""; $note_quantity = '';
        if( $row_cart['note_quantity'] )
        {
          if($row_cart['note_quantity']<$row_cart['quantity'])
          {
            $class_qty = " have-note";
            $note_quantity = '<div class="note-quantity"><div class="arrow_box">Còn '.(int) $row_cart['note_quantity'] .' sản phẩm<span class="close">x</span></div></div>';
          }

        }
        $text_quantity = "<div class='div-quantity'><input type='text' class='form-control quantity".$class_qty."' id='quantity_".$id_cart."' name='quantity[" . $id_cart . "]'  value='" . $row_cart['quantity']. "' maxlength='4' onChange='vnTOrder.updateQuantity(" . $id_cart . ")' /> ".$note_quantity.'</div>';

        $row_cart['quantity'] = $text_quantity;

        $row_cart['total'] = get_price_product($total);
        $row_cart['class'] = "row_item_cart";

        $this->skin->assign('row', $row_cart);
        $this->skin->parse("html_edit.row_cart");


      }
      //====


      $data['total_cart'] = get_price_product($data['total_cart']);
      $data['s_price'] = get_price_product($data['s_price']);
      $data['total_price'] = get_price_product($data['total_price']);


      $data['link_action'] = LINK_ACTION . ".html/?do=edit&id=".$id;
    } else { //end if order
      $linkref = LINK_ACTION . ".html";
      @header("Location: {$linkref}");
      echo "<meta http-equiv='refresh' content='0; url={$linkref}' />";
    }
    $data['err'] = $err;
    if ($_SESSION['mess'] != '') {
      $data['err'] = $_SESSION['mess'];
      $_SESSION['mess'] = '';
    }
    $this->skin->assign("data", $data);
    $this->skin->parse("html_edit");
    $nd['content'] = $this->skin->text("html_edit");
    $nd['f_title'] = $vnT->lang['member']['edit_order'];
    $textout = $vnT->skin_box->parse_box("box_middle", $nd);
    return $textout;
  }
  function do_Detail(){
    global $input, $vnT, $conf, $DB, $func;
    $vnT->html->addStyleSheet(ROOT_URL . "modules/product/css/cart.css");
    // $vnT->html->addScript(DIR_MOD . "/js/" . $this->module . ".js");
    // $vnT->html->addScript(DIR_MOD . "/js/" . $this->action . ".js");
    // $vnT->html->addStyleSheet(DIR_MOD . "/css/" . $this->action . ".css");
    $id = (int)$input['id'];
    $mem_id = $vnT->user['mem_id'];
    $sql = "SELECT * FROM order_sum WHERE mem_id=$mem_id AND display=1 AND order_id =" . $id;
    $result = $DB->query($sql);
    if ($data = $DB->fetch_row($result)) {
      $payment_address = get_info_address($data);
      $shipping_address = get_info_address($data, "shipping");
      $html_row_cart = "";
      $sql_cart = "SELECT * FROM order_detail WHERE order_id = " . $id;
      $result_cart = $vnT->DB->query($sql_cart);
      $x = 0;
      $w = 65;
      while ($row_cart = $vnT->DB->fetch_row($result_cart)) {
        $x++;
        $row_cart['stt'] = $x;
        $item_type = $row_cart["item_type"];
        $item_id = $row_cart["item_id"];
        $price = $row_cart["price"];
        $total = ($price * $row_cart['quantity']);
        $link = $vnT->link_root.$row_cart["item_link"];
        $src_pic = ($row_cart["item_picture"]) ? $vnT->func->get_src_modules($row_cart["item_picture"], $w, 0, 0) : ROOT_URL . "modules/product/images/nophoto.gif";
        $item_title = '<div class="img"><a href="'.$link.'"><img src="'.$src_pic.'" alt="'.$row_cart['item_title'].'"></a></div>';
        $item_title.= '<div class="i-title">'.$row_cart['item_title'].'</div>';
        $item_title.= '<div class="clear"></div>';
        $row_cart['item_title'] = $item_title;
        $row_cart['price'] = ($row_cart['item_price']) ? get_price_product($row_cart['item_price']) : get_price_product($row_cart['price']);
        $row_cart['quantity'] = $row_cart['quantity'];
        $row_cart['total'] = get_price_product($total);
        $row_cart['class'] = "row_item_cart";
        $this->skin->assign('row', $row_cart);
        $this->skin->parse("html_detail.row_cart");
      }
      $data['payment_address'] = $payment_address;
      $data['shipping_address'] = $shipping_address;
      $res_pay = $vnT->DB->query("SELECT * FROM payment_method WHERE name='" . $data['payment_method'] . "' ");
      if ($row_pay = $vnT->DB->fetch_row($res_pay)) {
        $data['info_payment'] = '<p><strong>' . $func->fetch_array($row_pay['title']) . '</strong></p>';
        $data['info_payment'] .= $vnT->func->fetch_array($row_pay['description']);
      }
      if( $data['shipping_method']){
        $res_ship = $vnT->DB->query("SELECT * FROM shipping_method WHERE name='" . $data['shipping_method'] . "' ");
        if ($row_ship = $vnT->DB->fetch_row($res_ship)) {
          $data['info_shipping'] = '<p><strong>' . $func->fetch_array($row_ship['title']) . '</strong></p>';
          $data['info_shipping'] .= $vnT->func->fetch_array($row_ship['description']);
        }
      }
      $data['other_price'] ='';
      $discount = $data['promotion_price'];
      if ($discount) {
        $data['other_price'] = '<li>
                                  <div class="p">Giảm giá</div>
                                  <div class="l">- '.get_price_product($discount, "0").'</div>
                                  <div class="clear"></div>
                                </li>';
      }
      $data['total_cart'] = get_price_product($data['total_cart']);
      $data['s_price'] = get_price_product($data['s_price']);
      $data['total_price'] = get_price_product($data['total_price']);
      $data['eligible_for_sale'] = str_replace("{point}", "<b class=font_err>" . $data['mem_point_earned'] . "</b>", $vnT->lang['mess_eligible_for_sale']);
      $data['text_date_order'] = @date("d-m-Y, H:i:s", $data['date_order']);
      if ($data['comment'])
        $data['comment'] = "<p class='mess_comment'>" . $func->HTML($data['comment']) . "</p>";
      else
        $data['comment'] = "";
      $data['text_status'] = $vnT->setting_pro['status'][$data['status']];
      $button = '';
      $data['button'] = $button;
    } else { //end if order
      $linkref = LINK_ACTION . ".html";
      @header("Location: {$linkref}");
      echo "<meta http-equiv='refresh' content='0; url={$linkref}' />";
    }
    $data['link_print'] = LINK_MOD . "/your_order.html/?do=print&order_id=" . $id;
    $this->skin->assign("data", $data);
    $this->skin->parse("html_detail");
    return $this->skin->text("html_detail");
    $nd['f_title'] = $vnT->lang['member']['info_order'];
    $textout = $vnT->skin_box->parse_box("box_middle", $nd);
    return $textout;
  }
  function do_Del() {
    global $input, $func, $DB, $conf, $vnT;
    $mem_id = (int)$vnT->user['mem_id'];
    $id = (int)$input['id'];
    $ok_del = 0;
    $cancel_status = $vnT->setting_pro['status_customer'];
    $result = $DB->query("SELECT order_id,status FROM order_sum 
                          WHERE order_id = {$id} AND mem_id = ".$vnT->user['mem_id']);
    if($row = $DB->fetch_row($result)){
      if($row['status'] == $vnT->setting_pro['status_default']){
        $ok = $DB->query("UPDATE order_sum SET status = {$cancel_status} WHERE order_id = {$id}");
        if ($ok) {
          $mess = $vnT->func->html_mess($vnT->lang['member']["del_success"], " alert-autohide");
        } else {
          $mess = $vnT->func->html_err($vnT->lang['member']["del_failt"], " alert-autohide");
        }
      }else{
        $mess = $vnT->func->html_err($vnT->lang['member']["not_default"], " alert-autohide");
      }
    }else{
      $mess = $vnT->func->html_err($vnT->lang['member']["err_memid"], " alert-autohide");
    }
    $ok = $vnT->DB->query("DELETE FROM orders WHERE buyer_id=$mem_id AND order_id IN (" . $ids . ") ");
    $ext_page = str_replace("|", "&", $ext);
    $url = LINK_ACTION.".html";
    $vnT->func->header_redirect($url, $mess);
  }
  function render_row($row_info) {
    global $func, $conf, $DB, $vnT;
    $row = $row_info;
    // Xu ly tung ROW
    $id = $row['order_id'];
    $row_id = "row_" . $id;
    $link_edit = LINK_ACTION . ".html/?do=edit&id={$id}&ext=" . $row['ext_link'];
    $link_detail = LINK_ACTION . ".html/?do=detail&id=".$id;
    $item_link = $vnT->link_root . $vnT->setting['seo_name'][$vnT->lang_name]['product'] . "/" . $row['item_id'] . "/" . $row['item_link'] . ".html";
    $output['check_box'] = "<input type=\"checkbox\" name=\"del_id[]\" value=\"{$id}\" class=\"checkbox\" onclick=\"vnTpost.select_row('{$row_id}')\"  >";
    $output['order_code'] = "<a  href='" . $link_detail. "' ><b class='order-code'>" . $row['order_code'] . "</b></a>";
    $output['total_price'] = '<span class="price">'.get_price_product($row['total_price']).'</span>';
    $output['status'] = $vnT->setting_pro['status'][$row['status']];
    $output['date_order'] = @date("d/m/Y, H:i", $row['date_order']);
    $list_action = "";
    if ($row['status'] == $vnT->setting_pro['status_default']) {
      $list_action .= '<a href="javascript:void(0);" onClick="vnTOrder.removeOrder(' . $id . ')" class="btn-acttion" title="'.$vnT->lang['member']['btn_cancel'].'"  >' . $vnT->lang['member']['btn_cancel'] . '</a>';
    }
    $list_action .= '<a href="' . $link_detail . '" class="btn-acttion detail" title="Xem chi tiết" >Xem chi tiết</a>';
    $output['action'] = '<input name=h_id[]" type="hidden" value="' . $id . '" />';
    $output['action'] .= $list_action;
    return $output;
  }
  function do_Manage(){
    global $vnT, $func, $DB, $conf, $input;
    $p = ((int)$input['p']) ? (int)$input['p'] : 1;
    $keyword = (isset($input['key'])) ? $input['key'] : "";
    $status = (isset($input['status'])) ? $input['status'] : "0";
    $date_from = (isset($input['date_from'])) ? $input['date_from'] : "";
    $date_to = (isset($input['date_to'])) ? $input['date_to'] : "";
    $root_link = LINK_ACTION . ".html";
    $html_row = "";
    $where = "";
    $ext_link = "p=$p";
    $ext_pag = "";
    if ($status) {
      $where .= " AND status=" . $status;
    }
    if ($date_from || $date_to) {
      $tmp1 = @explode("/", $date_from);
      $time_begin = @mktime(0, 0, 0, $tmp1[1], $tmp1[0], $tmp1[2]);

      $tmp2 = @explode("/", $date_to);
      $time_end = @mktime(23, 59, 59, $tmp2[1], $tmp2[0], $tmp2[2]);

      $where .= " AND (date_order BETWEEN {$time_begin} AND {$time_end} ) ";
      $ext_pag .= "&date_from=" . $date_from . "&date_to=" . $date_to;
    }
    if ($keyword) {
      $where .= " AND (order_code like '%{$keyword}%' )";
      $ext_pag .= "&key=" . rawurlencode($keyword);
      $ext_link .= "|key=" . $keyword;
    }
    $sql_num = "SELECT order_id FROM order_sum WHERE mem_id=" . $vnT->user['mem_id'] . " $where ";
    $res_num = $vnT->DB->query($sql_num);
    $totals = $vnT->DB->num_rows($res_num);
    $n = 20;
    $num_pages = ceil($totals / $n);
    if ($p > $num_pages) $p = $num_pages;
    if ($p < 1) $p = 1;
    $start = ($p - 1) * $n;
    if ($num_pages > 1) {
      $nav = "<div class=\"vnt-pagination\">".$vnT->func->paginate_search($root_link,$totals,$n,$ext_pag, $p)."</div>";
    }
    $sql = "SELECT * FROM order_sum WHERE mem_id=" . $vnT->user['mem_id'] . " {$where} 
            ORDER BY date_order DESC LIMIT $start,$n ";
    $result = $vnT->DB->query($sql);
    $table['link_action'] = LINK_ACTION . ".html";
    if ($vnT->DB->num_rows($result)) {
      while ($row = $vnT->DB->fetch_row($result)) {
        $id = $row['order_id'];
        $link_detail = LINK_ACTION.".html/?do=detail&id=".$id;
        $link_del = LINK_ACTION.'.html/?do=del&id='.$id;
        $table_list .= '<tr>';
        $table_list .= '<td data-cont="'.$vnT->lang['member']['order_code'].'"><a class="code" href="'.$link_detail.'">'.$row['order_code'].'</a></td>';
        $table_list .= '<td data-cont="'.$vnT->lang['member']['date_order'].'">'.@date('d/m/Y, H:i',$row['date_order']).'</td>';
        $list_name = $vnT->func->cut_string($vnT->func->check_html(get_list_name($id),'nohtml'),40,1);
        $table_list .= '<td data-cont="'.$vnT->lang['member']['product'].'">'.$list_name.'</td>';
        $table_list .= '<td data-cont="'.$vnT->lang['member']['total_price'].'"><div class="stt">'.get_price_product($row['total_price']).'</div></td>';
        $table_list .= '<td data-cont="Tình trạng" class="text_align">';
        $table_list .= '<div class="red"><a href="'.$link_detail.'">'.$vnT->setting_pro['status'][$row['status']].'</a></div>';
        $table_list .= ($row['status'] == $vnT->setting_pro['status_default']) ? '<button type="button" class="cancel_item button_hidden" onClick="location.href=\''.$link_del.'\'">'.$vnT->lang['member']['cancel'].'</button>' : '';
        $table_list .= '<div class="clear"></div></td>';
        $table_list .= '</tr>';
      }
    } else {
      $table_list = '<tr><td colspan="6" align="center"><span class="font_err">'.$vnT->lang['member']['no_order'].'</span></td></tr>';
    }
    $data['table_list'] = $table_list;
    $data['nav'] = $nav;
    $data['totals'] = $totals; 
    $data['keyword'] = $keyword;
    $data['list_status_order'] = List_Status_Order($status);
    $data['err'] = $err;
    if ($_SESSION['mess'] != '') {
      $data['err'] = $_SESSION['mess'];
      $_SESSION['mess'] = '';
    }
    $data['link_add'] = LINK_ACTION . ".html/?do=add";
    $data['action_del'] = LINK_ACTION . ".html/?do=del";
    $data['link_action'] = LINK_ACTION . ".html";
    $data['nav_member'] = nav_member('your_order');
    $this->skin->assign("data", $data);
    $this->skin->parse("html_manage");
    return $this->skin->text("html_manage");
    // $nd['f_title'] = $vnT->lang['member']['manage_order'];
    // return $vnT->skin_box->parse_box("box_middle", $nd);
  }
}// class
?>
