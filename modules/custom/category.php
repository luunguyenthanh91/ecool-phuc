<?php
/*================================================================================*\
|| 							Name code : main.php 		 		 																	  # ||
||  				Copyright © 2007 by Thai Son - CMS vnTRUST                					# ||
\*================================================================================*/
/**
 * @version : 1.0
 * @date upgrade : 11/12/2007 by Thai Son
 **/
if (! defined('IN_vnT'))
{
  die('Access denied');
}
$nts = new sMain();
class sMain
{
  var $output = "";
  var $skin = "";
  var $linkUrl = "";
  var $module = "custom";
  var $act = "category";

  function sMain (){
    global $vnT, $input;
    include ("function_" . $this->module . ".php");
    loadSetting();
    $this->skin = new XiTemplate( DIR_MODULE ."/". $this->module . "/html/". $this->module . ".tpl");
    $this->skin->assign('DIR_MOD', DIR_MOD);
    $this->skin->assign('LANG', $vnT->lang);
    $this->skin->assign('INPUT', $input);
    $this->skin->assign('CONF', $vnT->conf);
    $this->skin->assign('DIR_IMAGE', $vnT->dir_images);
    $vnT->html->addStyleSheet(DIR_MOD . "/css/" . $this->module . ".css");
    $vnT->html->addScript(DIR_MOD . "/js/" . $this->module . ".js");
		$vnT->setting['menu_active'] = $this->module;
    //check category
    $cat_id = (int) $input['catID'];
    $res_ck= $vnT->DB->query("SELECT * FROM custom_category n, custom_category_desc nd
															WHERE n.cat_id=nd.cat_id AND display=1 AND lang='$vnT->lang_name' 
															AND n.cat_id=$cat_id");
    if ($row = $vnT->DB->fetch_row($res_ck)){
			if ($row['metadesc'])	$vnT->conf['meta_description'] = $row['metadesc'];
			if ($row['metakey']) $vnT->conf['meta_keyword'] = $row['metakey'];
			if ($row['friendly_title']) $vnT->conf['indextitle'] = $row['friendly_title'];
			if($vnT->muti_lang>0){
				$res_lang= $vnT->DB->query("SELECT friendly_url,lang FROM custom_category_desc 
																		WHERE cat_id=".$row['cat_id']." AND display=1 ");
				while ($row_lang = $vnT->DB->fetch_row($res_lang)) {
					$link_lang = create_link ("category",$row['cat_id'],$row_lang['friendly_url']);
					$vnT->link_lang[$row_lang['lang']]=str_replace($vnT->link_root,ROOT_URI.$row_lang['lang']."/",$link_lang);
				}
			}
		  $data['main'] .= $this->list_custom($row);  
			$navation = get_navation($input['catID']);
      $data['navation'] = $vnT->lib->box_navation($navation);
      $vnT->setting['banner'] = $vnT->lib->get_child_slide('child');
      $data['fixed_sidebar'] = $vnT->lib->fixed_sidebar();
    } else {
			$linkref = LINK_MOD.".html";
      @header("Location: ".$linkref."");
      echo "<meta http-equiv='refresh' content='0; url=".$linkref."' />";
    }
    $this->skin->assign("data", $data);
    $this->skin->parse("modules");
    $vnT->output .= $this->skin->text("modules");
  }
  function list_custom ($info){
    global $DB, $func, $input, $vnT;
    $p = ((int) $input['p']) ? (int) $input['p'] : 1;
		$root_link = create_link("category",$cat_id,$info['cat_name']);
    $cat_id = (int) $info['cat_id'];
    $num_row=3;
    $view = 1;
		$where = '';
    if ($cat_id)   {
      $where .= get_where_cat($cat_id);
    }
		$sID = (int) $input['sID'];
		if($sID){
			$where .= "AND status=".$sID;
			$ext_pag .= "&sID=".$sID;
		}
    $sql_num = "SELECT p.p_id FROM custom p, custom_desc pd
								WHERE p.p_id=pd.p_id AND display=1 AND lang='$vnT->lang_name' $where ";
    $res_num = $vnT->DB->query($sql_num);
    $totals = $vnT->DB->num_rows($res_num);
		if($input['display']){
      $n = ($input['display']);
      $ext_pag .= "&display=".$input['display'];
    }else{
      $n = ($vnT->setting['n_grid']) ? $vnT->setting['n_grid'] : 9;
    }
    $num_pages = ceil($totals / $n);
    if ($p > $num_pages) $p = $num_pages;
    if ($p < 1) $p = 1;
    $start = ($p - 1) * $n;
		if($num_pages>1){			
			$nav = "<div class='pagination'>".$vnT->func->htaccess_paginate($root_link,$totals,$n,$ext_pag,$p)."</div>" ;
		}
    switch ($input['sort']) {
      case 'new':
        $where .= " ORDER BY date_post DESC ";
        break;
      case 'old':
        $where .= " ORDER BY date_post ASC ";
        break;
      case 'low':
        $where .= " ORDER BY price ASC ";
        break;
      case 'hight':
        $where .= " ORDER BY price DESC ";
        break;
      default:
        $where .= " ORDER BY date_post DESC, p_order DESC ";
        break;
    }
		$data['description'] = $info['description'];
    $data['row_custom'] = row_custom($where, $start, $n, $num_row, $view);
    $data['nav'] = $nav;
    //$data['box_filter'] = box_filter();
    $this->skin->assign("data", $data);
    $this->skin->parse("html_list");
		$nd['content'] = $this->skin->text("html_list");
 		$nd['f_title'] = '<h1>'.$vnT->func->HTML($info['cat_name']).'</h1>';
    $nd['more'] = $info['slogan'];
		$textout = $vnT->skin_box->parse_box("box_middle",$nd);
		return $textout;
  }
}
?>