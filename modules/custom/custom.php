<?php
/*================================================================================*\
|| 							Name code : main.php 		 		 																	  # ||
||  				Copyright © 2007 by Thai Son - CMS vnTRUST                					# ||
\*================================================================================*/
/**
 * @version : 1.0
 * @date upgrade : 11/12/2007 by Thai Son
 **/
if (! defined('IN_vnT'))
{
  die('Access denied');
}
$nts = new sMain();
class sMain
{
  var $output = "";
  var $skin = "";
  var $linkUrl = "";
  var $module = "custom";

  function sMain (){
    global $vnT, $input;
    include ("function_" . $this->module . ".php");
    loadSetting();
    $this->skin = new XiTemplate( DIR_MODULE ."/". $this->module . "/html/". $this->module . ".tpl");
    $this->skin->assign('DIR_MOD', DIR_MOD);
    $this->skin->assign('LANG', $vnT->lang);
    $this->skin->assign('INPUT', $input);
    $this->skin->assign('CONF', $vnT->conf);
    $this->skin->assign('DIR_IMAGE', $vnT->dir_images);
    $vnT->html->addStyleSheet(DIR_MOD . "/css/" . $this->module . ".css");
		//active menu
		$vnT->setting['menu_active'] = $this->module;
		//SEO
		if ($vnT->setting['metakey'])	$vnT->conf['meta_keyword'] = $vnT->setting['metakey'];
		if ($vnT->setting['metadesc'])	$vnT->conf['meta_description'] = $vnT->setting['metadesc'];		
		if ($vnT->setting['friendly_title']){	 
			$vnT->conf['indextitle'] = $vnT->setting['friendly_title'];	
		}
    $err = '';
    if (isset($_POST['btnConfirm'])){
      if ($vnT->user['sec_code'] == $input['security_code']){
        if(empty($err)){
          $cot['project'] = (int) $input['project'];
          $cot['product'] = (int) $input['product'];
          $cot['style'] = (int) $input['style'];
          $cot['material'] = (int) $input['material'];
          $cot['color'] = $input['color'];
          $cot['area'] = $input['area'];
          $cot['note'] = $vnT->func->txt_HTML($_POST['note']);

          $cot['name'] = $input['name'];
          $cot['phone'] = $input['phone'];
          $cot['email'] = $input['email'];
          $cot['address'] = $input['address'];
          $cot['status'] = 1;
          $cot['subject'] = $input['subject'];
          $cot['comment'] = $vnT->func->txt_HTML($_POST['comment']);
          $cot['date_post'] = time();
          $ok = $vnT->DB->do_insert("custom_booking",$cot);
          if($ok){
            load_attr_name();
            $content_email = $vnT->func->load_MailTemp("servey");
            $qu_find = array(
              '{domain}' , 
              '{name}' , 
              '{email}' , 
              '{phone}' , 
              '{address}' , 
              '{project}' , 
              '{product}' , 
              '{style}' , 
              '{material}' , 
              '{color}' , 
              '{area}' , 
              '{note}' , 
              '{content}',
              '{date}'
            );

            $qu_replace = array(
              $_SERVER['HTTP_HOST'] , 
              $input['name'] , 
              $input['email'] ,
              $input['phone'] , 
              $input['address'],
              $vnT->setting['attr_name'][$input['project']],
              $vnT->setting['attr_name'][$input['product']],
              $vnT->setting['attr_name'][$input['style']],
              $vnT->setting['attr_name'][$input['material']],
              $input['color'],
              $input['area'],
              $vnT->func->HTML($_POST["note"]) , 
              $vnT->func->HTML($_POST["comment"]) , 
              date("H:i, d/m/Y")
            );
            $message = str_replace($qu_find, $qu_replace, $content_email);
            $subject = str_replace("{host}",$_SERVER['HTTP_HOST'],$vnT->lang['custom']['subject_booking']);
            $sent = $vnT->func->doSendMail($vnT->conf['email'], $subject, $message, $input["email"], $file_attach);
            $link_ref = LINK_MOD.'/booking.html/?do=success';
            $vnT->func->header_redirect($link_ref);
          }else{
            $err = $func->html_err($DB->debug());
          }
        }else{
          $err = $vnT->func->html_err($err);
        }
      }else{
        $err = $vnT->func->html_err($vnT->lang['custom']['security_code_invalid']);
      }
    }
    $row['err'] = $err;
		$data['main'] = $this->list_custom($row);
		$navation = get_navation($input['catID']);
    $data['navation'] = $vnT->lib->box_navation($navation);
    $vnT->setting['banner'] = $vnT->lib->get_child_slide('child');
    $data['fixed_sidebar'] = $vnT->lib->fixed_sidebar();
    $this->skin->assign("data", $data);
    $this->skin->parse("modules");
    $vnT->output .= $this->skin->text("modules");
  }
  function list_custom ($info){
    global $DB, $func, $input, $vnT;
    $p = ((int) $input['p']) ? (int) $input['p'] : 1;
    $root_link = LINK_MOD.".html";
    $view = 1;
		$num_row = 3;
		$sID = (int) $input['sID'];
		$where= '';
    $sql_num = "SELECT p.p_id FROM custom p, custom_desc pd
								WHERE p.p_id=pd.p_id AND lang='$vnT->lang_name' AND display=1 $where ";
    $res_num = $vnT->DB->query($sql_num);
    $totals = $vnT->DB->num_rows($res_num);
		$n = ($vnT->setting['n_grid']) ? $vnT->setting['n_grid'] : 9;
    $num_pages = ceil($totals / $n);
    if ($p > $num_pages) $p = $num_pages;
    if ($p < 1) $p = 1;
    $start = ($p - 1) * $n;
		if($num_pages>1){ 
			$nav = "<div class='pagination'>".$vnT->func->htaccess_paginate($root_link,$totals,$n,$ext_pag,$p)."</div>";
		}
    $where .= " ORDER BY date_post DESC, p_order DESC ";
    $data['row_custom'] = row_custom($where, $start, $n, $num_row, $view);
    $data['nav'] = $nav;
    $data['description'] = $vnT->setting['description'];
    $data['option'] = get_attr_option();
    $vnT->user['sec_code'] = $vnT->func->get_security_code();
    $scode = $vnT->func->NDK_encode($vnT->user['sec_code']);
    $data['ver_img'] = ROOT_URL."includes/sec_image.php?code=$scode&h=40";
    $data['err'] = $info['err'];
    $this->skin->assign("data", $data);
    $this->skin->parse("html_list");
		$nd['content'] = $this->skin->text("html_list");
 		$nd['f_title'] = '<h1>'.$vnT->lang['custom']['custom'].'</h1>';
    $nd['more'] = $vnT->setting['slogan'];
		$textout = $vnT->skin_box->parse_box("box_middle",$nd);
		return $textout;
  }
}
?>