<!-- BEGIN: box_filter -->
<div class="boxFilter">
  <div class="fPart">
    {data.box_category}
  </div>
  <div class="fPart">
    <div class="grid">
      {data.Sort_Filter}
      {data.Display_Filter}
    </div>
  </div>
</div>
<!-- END: box_filter -->