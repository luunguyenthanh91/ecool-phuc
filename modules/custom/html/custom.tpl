<!-- BEGIN: modules -->
<div id="vnt-content">
    <div class="vnt-main-top">
      <div class="vnt-slide">
          <div id="vnt-slide" class="slick-init">
              <div class="item"><div class="img">
                  <img src="{DIR_IMAGE}/about/slide.jpg" alt="">
              </div></div>
          </div>
          <div id="vnt-navation" class="breadcrumb hidden-sm hidden-xs" itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb">
              <div class="wrapper">
                  <div class="navation">
                      {data.navation}
                      <div class="clear"></div>
                  </div>
              </div>
          </div>
      </div>
<div class="wrapping">
  <div class="wrapCont">
    <div class="wrapper">
      <br/>
      {data.main}
    </div>
  </div>
  <div id="flagEnd"></div>
</div></div>
<!-- END: modules -->

<!-- BEGIN: html_list -->
<div class="customDes">
  {data.description}
</div>
<div class="custommiItem">
  <div class="title text-center">{LANG.custom.all_project}</div>
  <div class="row">
    {data.row_custom}
  </div>
  {data.nav}
</div>
<div class="formCustom">
  {data.err}
  <form name="f_booking" id="f_booking" action="" method="POST">
    <div class="titleBig">{LANG.custom.note_booking}</div>
    <div class="box">
      <div class="wrap">
        <div class="title">{LANG.custom.servey_information}</div>
        <div class="row">
          <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
            <div class="form-group">
              <label for="project">{LANG.custom.project_type} *</label>
              {data.option.project}
            </div>
            <div class="form-group">
              <label for="product">{LANG.custom.product_type} *</label>
              {data.option.product}
            </div>
            <div class="form-group">
              <label for="style">{LANG.custom.style_type} *</label>
              {data.option.style}
            </div>
            <div class="form-group">
              <label for="material">{LANG.custom.material_type} *</label>
              {data.option.material}
            </div>
          </div>
          <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
            <div class="form-group">
              <label for="color">{LANG.custom.color} *</label>
              <input type="text" id="color" name="color"/>
            </div>
            <div class="form-group">
              <label for="area">{LANG.custom.area} *</label>
             <input type="text" id="area" name="area"/>
            </div>
            <div class="form-group">
              <label for="note">{LANG.custom.request}</label>
              <textarea name="note" id="note"></textarea>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="box">
      <div class="wrap">
        <div class="title">{LANG.custom.custom_information}</div>
        <div class="row">
          <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
            <div class="form-group">
              <label for="name">{LANG.custom.full_name} *</label>
              <input type="text" name="name" id="name" />
            </div>
            <div class="form-group">
              <label for="phone">{LANG.custom.phone} *</label>
              <input type="text" id="phone" name="phone"/>
            </div>
            <div class="form-group">
              <label for="email">{LANG.custom.email} *</label>
              <input type="text" id="email" name="email"/>
            </div>
            <div class="form-group">
              <label for="address">{LANG.custom.address}</label>
              <input type="text" id="address" name="address"/>
            </div>
          </div>
          <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
            <div class="form-group">
              <label for="subject">{LANG.custom.subject}</label>
              <input type="text" id="subject" name="subject"/>
            </div>
            <div class="form-group">
              <label for="comment">{LANG.custom.note} *</label>
              <textarea name="comment" id="comment"></textarea>
            </div>
            <div class="form-group">
              <label for="security_code">{LANG.custom.security_code} *</label>
              <div class="input-group">
                <input type="text" name="security_code" id="security_code" class="required">
                <span class="input-group-img"><img class="security_ver" src="{data.ver_img}"></span>
              </div>
            </div>
          </div>
        </div>
        <div class="form-group text-center">
          <button name="btnConfirm" id="btnConfirm" value="1">{LANG.custom.btnSend}</button>
        </div>
      </div>
    </div>
  </form>
</div>
<script language="javascript">
  function checkform(f) {
    if (f.project.value == '' || f.project.value == 0 ) {
      alert("{LANG.custom.err_project_empty}");
      f.project.focus();
      return false;
    }
    if (f.product.value == '' || f.product.value == 0 ) {
      alert("{LANG.custom.err_product_empty}");
      f.product.focus();
      return false;
    }
    if (f.style.value == '' || f.style.value == 0 ) {
      alert("{LANG.custom.err_style_empty}");
      f.style.focus();
      return false;
    }
    if (f.material.value == '' || f.material.value == 0 ) {
      alert("{LANG.custom.err_material_empty}");
      f.material.focus();
      return false;
    }
    if (f.color.value == '') {
      alert("{LANG.custom.err_color_empty}");
      f.color.focus();
      return false;
    }
    if (f.area.value == '') {
      alert("{LANG.custom.err_area_empty}");
      f.area.focus();
      return false;
    }
    if (f.name.value == '' ) {
      alert("{LANG.custom.err_name_empty}");
      f.name.focus();
      return false;
    }
    if (f.phone.value == '' ) {
      alert("{LANG.custom.err_phone_empty}");
      f.phone.focus();
      return false;
    }
    if(!vnTRUST.is_phone(f.phone.value)){
      alert("{LANG.custom.err_phone_invalid}");
      f.phone.focus();
      return false;
    }
    var re =/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,5})$/gi;
    var email = f.email.value;
    if (email == '') {
      alert("{LANG.custom.err_email_empty}");
      f.email.focus();
      return false;
    }
    if (email != '' && email.match(re)==null)   {
      alert("{LANG.custom.err_email_invalid}");
      f.email.focus();
      return false;
    }
    if (f.security_code.value == '' ) {
      alert("{LANG.custom.err_security_code_empty}");
      f.security_code.focus();
      return false;
    }
    return true;
  }
  $(function(){
    $("#f_booking").submit(function(){
      if( checkform( $(this)[0] ) ){
        $(".formCustom").addClass("loading");
        return true;
      }else{
        return false;
      }
    });
  });
</script>
<!-- END: html_list -->

<!-- BEGIN: html_360 -->
<div class="iframe360">
  <iframe src="{data.link360}" frameborder="0" gesture="media" allow="encrypted-media" allowfullscreen onload="this.width=screen.width;this.height=screen.height;"></iframe>
</div>
<!-- END: html_360 -->