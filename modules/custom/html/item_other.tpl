<!-- BEGIN: item_other -->
<div class="item">
  <div class="collect">
    <div class="img">
    	<a href="{data.link}" title="{data.p_name}"><img src="{data.src}" alt="{data.p_name}" /></a>
    </div>
    <div class="tend"><h3><a href="{data.link}" title="{data.p_name}">{data.p_name}</a></h3></div>
  </div>
</div>
<!-- END: item_other -->