<!-- BEGIN: item_custom -->
<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
  <div class="item">
    <div class="img">
      <a href="{data.link}" title="{data.p_name}" target="_blank">
        <img src="{data.src}" alt="{data.p_name}"/>
      </a>
    </div>
    <div class="tend"><h3>{data.p_name}</h3></div>
  </div>
</div>
<!-- END: item_custom -->