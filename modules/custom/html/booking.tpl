<!-- BEGIN: modules -->
{data.fixed_sidebar}
<div class="wrapping">
  <div class="wrapCont">
    <div class="wrapper">
      {data.main}
    </div>
  </div>
  <div id="flagEnd"></div>
</div>
<!-- END: modules -->

<!-- BEGIN: html_booking -->
<form name="f_booking" id="f_booking" action="" method="POST">
  <div class="title">{LANG.custom.f_booking}</div>
  <div class="content">
    <div class="formPopup">
      <div class="row">
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
          <div class="form-group">
            <label for="name">{LANG.custom.full_name} *</label>
            <div class="formFa fa-user">
              <input type="text" class="required" id="name" name="name"/>
            </div>
          </div>
        </div>
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
          <div class="form-group">
            <label for="email">{LANG.custom.email} *</label>
            <div class="formFa fa-envelope-o">
              <input type="text" class="required" id="email" name="email"/>
            </div>
          </div>
        </div>
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
          <div class="form-group">
            <label for="phone">{LANG.custom.phone} *</label>
            <div class="formFa fa-phone">
              <input type="text" class="required" id="phone" name="phone"/>
            </div>
          </div>
        </div>
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
          <div class="form-group">
            <label for="address">{LANG.custom.address}</label>
            <div class="formFa fa-home">
              <input type="text" id="address" name="address"/>
            </div>
          </div>
        </div>
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
          <div class="form-group">
            <label for="p_id">{LANG.custom.product_name} </label>
            {data.list_custom}
          </div>
        </div>
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
          <div class="form-group">
            <label for="comment">{LANG.custom.note}</label>
            <div class="formFa fa-edit">
              <textarea name="comment" id="comment"></textarea>
            </div>
          </div>
        </div>
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
          <div class="form-group">
            <label for="security_code">{LANG.custom.security_code} *</label>
            <div class="input-group">
              <div class="formFa fa-shield">
                <input type="text" name="security_code" id="security_code" class="required">
              </div>
              <span class="input-group-img">
                <img class="security_ver" src="{data.ver_img}">
              </span>
            </div>
          </div>
        </div>
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
          <div class="form-group">
            <label for="btnConfirm"></label>
            <button name="btnConfirm" id="btnConfirm" value="1"><span>{LANG.custom.btnSend}</span></button>
          </div>
        </div>
      </div>
    </div>
  </div>
</form>
<script language="javascript">
  function checkform(f) {
    if (f.name.value == '' ) {
      alert("{LANG.custom.err_name_empty}");
      f.name.focus();
      return false;
    }
    if (f.phone.value == '' ) {
      alert("{LANG.custom.err_phone_empty}");
      f.phone.focus();
      return false;
    }
    var re =/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,5})$/gi;
    var email = f.email.value;
    if (email == '') {
      alert("{LANG.custom.err_email_empty}");
      f.email.focus();
      return false;
    }
    if (email != '' && email.match(re)==null)   {
      alert("{LANG.custom.err_email_invalid}");
      f.email.focus();
      return false;
    }
    if (f.security_code.value == '' ) {
      alert("{LANG.custom.err_security_code_empty}");
      f.security_code.focus();
      return false;
    }
    return true;
  }
  $(function(){
    $("#f_booking").submit(function(){
      return checkform( $(this)[0] );
    });
  });
</script>
<!-- END: html_booking -->

<!-- BEGIN: html_booking_success --> 
<div class="content">
  <div class="Booking_Success" style="text-align: center;">
    {LANG.custom.note_success}
  </div>
</div>
<!-- END: html_booking_success -->