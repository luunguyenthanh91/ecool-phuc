<!-- BEGIN: modules -->
<div class="ecool-video">
      <div class="position-relative"><img class="w-100" src="{DIR_IMAGE}/banner-news.png" alt="Sơn Hà">
        <div class="container">
          <div class="row menu-sub-news text-center">
            {data.box_category}
            <div class="col-6 col-lg-3 p-0"><a href="#">
                <p class="fs-16 font-weight-medium menu-sub-news-title active"> Video</p></a></div>
          </div>
        </div>
      </div>
      <div class="container container--860">

          <div class="video-title">Có thể bạn sẽ quan tâm</div>
        <div class="row video-items">
         {data.main}

        </div>

      </div>
    </div>
<div class="modal-popup-video">
    <div class="video-box">
        <img class="icon-close-popup " src="{DIR_IMAGE}/close.png" alt="Sơn Hà">
        <iframe width="100%" height="400" src="">
        </iframe>
    </div>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script>

    $(".open_modal_video").on("click",function () {
        $('.modal-popup-video iframe').attr("src","http://www.youtube.com/embed/"+$(this).attr("alt")+"?autoplay=1");

        $('.modal-popup-video').attr("style","display:flex");
    });
    $(".icon-close-popup").on("click",function () {
        $('.modal-popup-video').attr("style","display:none")
    });

</script>

<!-- END: modules -->

<!-- BEGIN: html_list -->
{data.list_dealer}
{data.nav}

<!-- END: html_list -->


<!-- BEGIN: html_detail -->
<div class="news-detail">
      <div class="container">
        <nav class="fs-10 font-weight-light" aria-label="breadcrumb">
          <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="/">Home</a></li>
            <li class="breadcrumb-item"><a href="/home.html">Video</a></li>

          </ol>
        </nav>
      </div>
      <div class="container container--790 pb-5">
        <div class="row">
          <div class="col-12 text-center fs-16 fs-lg-31 font-weight-bold">
            <h1>{data.title}</h1>
          </div>

          <div class="col-12 fs-10 news-content text-justify">
            {data.description}
            <br/><br/>
            {data.iframe_youtube}

          </div>
        </div>
      </div>

    </div>
<!-- END: html_detail -->

<!-- BEGIN: item_other -->
<div class="item">
  <div class="i-img">
    <a href="{data.link}" title="{data.title}">
      <img src="{data.src}" alt="{data.title}" />
    </a>
  </div>
  <div class="i-desc">
    <div class="i-name">
      <h3><a href="{data.link}" title="{data.title}">{data.title}</a></h3>
    </div>
    {data.info}
  </div>
</div>
<!-- END: item_other -->
