<?php
/*================================================================================*\
|| 							Name code : function_dealer.php 		 		 												  # ||
||  				Copyright © 2007 by Thai Son - CMS vnTRUST                					# ||
\*================================================================================*/
/**
 * @version : 1.0
 * @date upgrade : 17/12/2007 by Thai Son
 **/
if (! defined('IN_vnT')) {
  die('Access denied');
}
define("DIR_MOD", ROOT_URI . "modules/dealer");
define('MOD_DIR_UPLOAD', ROOT_URI . 'vnt_upload/dealer');
define("MOD_DIR_IMAGE", ROOT_URI . "modules/dealer/images");
define("LINK_MOD", $vnT->link_root . $vnT->setting['seo_name'][$vnT->lang_name]['dealer']);

function loadSetting (){
  global $vnT, $func, $DB, $conf;
  $setting = array();
  $result = $DB->query("SELECT * FROM dealer_setting WHERE lang='$vnT->lang_name' ");
  $setting = $DB->fetch_row($result);
  foreach ($setting as $k => $v) {
    $vnT->setting[$k] = stripslashes($v);
  }
  unset($setting);
}
function create_link ($act, $id, $title, $extra = ""){
  global $vnT, $func, $DB, $conf;
	switch ($act){
		case "category" : $text = $vnT->link_root.$title.".html";	 break ;
		case "detail" :  $text = $vnT->link_root.$title.".html";	 break ;
		default : $text = LINK_MOD."/".$act."/".$id."/".$func->make_url($title).".html";	 break ;
	}
  $text .= ($extra) ? "/" . $extra : "";
  return $text;
}
function load_html ($file, $data){
  global $vnT, $input;
  $html = new XiTemplate(DIR_MODULE . "/dealer/html/" . $file . ".tpl");
  $html->assign('DIR_MOD', DIR_MOD);
  $html->assign('LANG', $vnT->lang);
  $html->assign('INPUT', $input);
  $html->assign('CONF', $vnT->conf);
  $html->assign('DIR_IMAGE', $vnT->dir_images);
  $html->assign("data", $data);
  $html->parse($file);
  return $html->text($file);
}
function get_navation ($cat_id){
  global $DB,$conf,$func,$vnT,$input;
  $output = '<ul itemscope="" itemtype="http://schema.org/BreadcrumbList">';
  $output.= '<li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem" class="home"><a href="'.$vnT->link_root.'" itemscope="" itemtype="http://schema.org/Thing" itemprop="item"><span itemprop="name"><i class="fa fa-home"></i></span></a></li> ';
	$output.= '<li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem"><a itemscope="" itemtype="http://schema.org/Thing" itemprop="item" href="'.LINK_MOD.'.html"><span itemprop="name">'.$vnT->lang['dealer']['dealer'].'</span></a></li> ';
	if($cat_id) {
		$res = $DB->query("SELECT cat_code,cat_id FROM dealer_category where cat_id in (".$cat_id.")  ");
		if ($cat=$DB->fetch_row($res)) {
			$cat_code = $cat['cat_code'];
		}
		$tmp = explode("_",$cat_code);
		for ($i=0;$i<count($tmp);$i++){
			$res= $DB->query("SELECT cat_name FROM dealer_category_desc 
												WHERE cat_id=".$tmp[$i]." and lang='{$vnT->lang_name}' ");
			if ($r = $DB->fetch_row($res)){		
				$link = create_link("category",$tmp[$i],$r['friendly_url']);	
				$output.='<li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem"><a itemscope="" itemtype="http://schema.org/Thing" itemprop="item" href="'.$link.'"><span itemprop="name">'.$func->HTML($r['cat_name']).'</span></a></li>';
			}
		}
	}
	return '<ul>'.$output.'</ul>';
}
function get_where_cat ($cat_id){
  global $input, $conf, $vnT;
  $where = "";
  $where .= " and cat_id=$cat_id ";
  return $where;
}
function Get_Cat($did=-1,$ext=""){
	global $vnT,$func,$DB,$conf;
	$text= "<select size=1 id=\"catID\" name=\"catID\" class='select' {$ext} >";
	$text.="<option value=\"0\">".$vnT->lang['dealer']['select_category']."</option>";
	$query = $DB->query("SELECT n.*,nd.cat_name FROM dealer_category n, dealer_category_desc nd
				where n.cat_id=nd.cat_id
				AND nd.lang='$vnT->lang_name' 
				AND display=1
				AND parentid=0 
				ORDER BY n.cat_order ASC, n.cat_id DESC ");

	while ($cat=$DB->fetch_row($query)) {
		$cat_name = $func->HTML($cat['cat_name']);
		if ($cat['cat_id']==$did)
			$text.="<option value=\"{$cat['cat_id']}\" selected  >{$cat_name}</option>";
		else
			$text.="<option value=\"{$cat['cat_id']}\"  >{$cat_name}</option>";
		$n=1;
		$text.=Get_Sub($did,$cat['cat_id'],$n,$lang);
	}
	$text.="</select>";
	return $text;
}
function Get_Sub($did,$cid,$n){
	global $func,$DB,$conf ,$vnT;

	$output="";
	$k=$n;
	$query = $DB->query("SELECT n.*,nd.cat_name FROM dealer_category n, dealer_category_desc nd
				where n.cat_id=nd.cat_id
				AND nd.lang='$vnT->lang_name' 
				AND display=1
				AND parentid=$cid
				ORDER BY n.cat_order ASC, n.cat_id DESC");
	while ($cat=$DB->fetch_row($query)) {
		$cat_name = $func->HTML($cat['cat_name']);
		
		if ($cat['cat_id']==$did)	{
			$output.="<option value=\"{$cat['cat_id']}\" selected>";
			for ($i=0;$i<$k;$i++) $output.= "|-- ";
			$output.="{$cat_name}</option>";
		}else{	
			$output.="<option value=\"{$cat['cat_id']}\" >";
			for ($i=0;$i<$k;$i++) $output.= "|-- ";
			$output.="{$cat_name}</option>";
		}
		$n=$k+1;
		$output.=Get_Sub($did,$cat['cat_id'],$n);
	}
	return $output;
}
function List_SubCat ($cat_id){
  global $func, $DB, $vnT;
  $output = "";
  $query = $DB->query("SELECT * FROM dealer_category WHERE parentid={$cat_id}  ");
  while ($cat = $DB->fetch_row($query)) {
    $output .= $cat["cat_id"] . ",";
    $output .= List_SubCat($cat['cat_id']);
  }
  return $output;
}

//-----------------  get_picture
function get_picture ($picture, $w = "", $ext = "")
{
  global $vnT, $func;
  $out = "";
  $linkhinh = "vnt_upload/dealer/" . $picture;
	$linkhinh = str_replace("//", "/", $linkhinh);
	$dir = substr($linkhinh, 0, strrpos($linkhinh, "/"));
	$pic_name = substr($linkhinh, strrpos($linkhinh, "/") + 1);
	
	if ($w) {
		
		$file_thumbs = $dir . "/thumbs/{$w}_" . substr($linkhinh, strrpos($linkhinh, "/") + 1);
		$linkhinhthumbs = $vnT->conf['rootpath'] . $file_thumbs;
		if (! file_exists($linkhinhthumbs)) {
			if (@is_dir($vnT->conf['rootpath'] . $dir . "/thumbs")) {
				@chmod($vnT->conf['rootpath'] . $dir . "/thumbs", 0777);
			} else {
				@mkdir($vnT->conf['rootpath'] . $dir . "/thumbs", 0777);
				@chmod($vnT->conf['rootpath'] . $dir . "/thumbs", 0777);
			}
			// thum hinh
			$vnT->func->thum($vnT->conf['rootpath'] . $linkhinh, $linkhinhthumbs, $w);
		}
		$src = ROOT_URI . $file_thumbs;
	} else {
		$src = MOD_DIR_UPLOAD . "/" . $picture;
	}
 
  $out = "<img  src=\"{$src}\"  alt=\"{$pic_name}\"  {$ext}  >";
  return $out;
}

/*-------------- get_city_name --------------------*/
function get_city_name ($code)
{
  global $func, $DB, $conf, $vnT;
  $text = $code;
  $result = $DB->query("SELECT name FROM iso_cities WHERE id='$code' ");
  if ($row = $DB->fetch_row($result)) {
    $text = $func->HTML($row['name']);
  }
  return $text;
}

/*-------------- get_state_name --------------------*/
function get_state_name ($code)
{
  global $func, $DB, $conf, $vnT;
  $text = $code;
  $result = $DB->query("SELECT name FROM iso_states WHERE id='$code' ");
  if ($row = $DB->fetch_row($result)) {
    $text = $func->HTML($row['name']);
  }
  return $text;
}

//-------- get_text_address
function get_text_address ($info)
{
  global $vnT, $input;
  $text = $info['address'];
  if ($info['state'])
    $text .= ", " . get_state_name($info['state']);
  if ($info['city'])
    $text .= ", " . get_city_name($info['city']);
  return $text;
}
function row_dealer ($where, $start, $n, $num_row=4,$view){
  global $vnT, $input, $DB, $func, $conf;
  $text = "";
  $sql = "SELECT * FROM dealer n, dealer_desc nd		
					WHERE n.did = nd.did AND display=1 AND lang='$vnT->lang_name' $where 
					ORDER BY d_order ASC, date_post DESC  LIMIT $start,$n";
  $result = $vnT->DB->query($sql);
  if ($num = $vnT->DB->num_rows($result)) {
    if ($view == 2) {
      while ($row = $DB->fetch_row($result)) {
				$text .= html_col($row);
      }
    } else {
      while ($row = $DB->fetch_row($result)) {
        $text .= html_row($row);
      }
    }
  } else {
    $text = "<div class='noItem'>{$vnT->lang['partner']['no_have_dealer']}</div>";
  }
  return $text;
}
function html_col ($data){
  global $input, $conf, $vnT ,$func;
	$link = create_link("detail", $data['did'], $data['friendly_url']);
	$link_map = ROOT_URI.'modules/dealer/popup/maps.php?id='.$data['did'].'&lang='.$vnT->lang_name.'&TB_iframe=true&width=700&height=590';
	$w = ($vnT->setting['imgthumb_width']) ? $vnT->setting['imgthumb_width'] : 100;
	if ($data['picture']) {
		$pic = get_picture($data['picture'], $w);
		$data['pic'] = '<div class="img_left" ><a href="' . $link . '" >' . $pic . '</a></div>';
	} else {
		$data['pic'] = "<div class='img_left' ><img src=\"" . MOD_DIR_IMAGE . "/nophoto.gif\" width='{$w}' ></div>";
	}
	$title =  $vnT->func->HTML($data['title']);
	$data['title'] = '<a href="' . $link . '">' . $title . '</a> ';
	 
	
	$data['info'] = '<ul class="listInfo">';
	$data['info'] .= '<li><strong>'.$vnT->lang['dealer']['address'].' : </strong> '. get_text_address($data).'</li>';		
	if($data['contact_name'])
		$data['info'] .= '<li><strong>'.$vnT->lang['dealer']['contact_name'].' : </strong> '. $data['contact_name'].'</li>';			
	if($data['phone'])
		$data['info'] .= '<li><strong>'.$vnT->lang['dealer']['phone'].' : </strong> '. $data['phone'].'</li>';		
	if($data['email'])
		$data['info'] .= '<li><strong>Email : </strong> '. $data['email'].'</li>';		
	
	
	
	if ($data['map_lat'] && $data['map_lng'])
	{
		$data['info'] .= '<li style="text-align:right"><a href="'.$link_map.'" class="thickbox" title="'.$title.'" ><img src="' . MOD_DIR_IMAGE . '/icon_map.gif" align="absmiddle"  > &nbsp; '.$vnT->lang['dealer']['view_map'].'</a></li>';				
	}	
	$data['info'] .= '</ul>';
  $textout = '<div class="info">'.$data['pic'].'<h3>'.$data['title'].'</h3>'.$data['info'].' </div>'; 
	return $textout;
}
function html_row ($row){
  global $input, $conf, $vnT, $func;
  $did = (int) $row['did'];
	$data['link_map'] = ROOT_URI.'modules/dealer/popup/maps.php?id='.$row['did'].'&lang='.$vnT->lang_name.'&TB_iframe=true';
	if($row['link360']){
		$data['link'] = create_link("detail", $row['did'], $row['friendly_url']);
		$data['btn360'] = '<a target="_blank" href="'.$data['link'].'" class="_360"><img src="'.$conf['icon360'].'" alt="360 images"/></a>';
	}else{
		$data['btn360'] = '';
		$data['link'] = 'javascript:;';
	}
	$data['link360'] = $row['link360'];
	$w = ($vnT->setting['imgthumb_width']) ? $vnT->setting['imgthumb_width'] : 370;
	$picture = ($row['picture']) ? "dealer/".$row['picture'] : "dealer/".$vnT->setting['pic_nophoto'];
  $data['src'] = $vnT->func->get_src_modules($picture, $w ,'',1,'1.5:1');
	$data['title'] = $row['title'];
	$data['short'] = $func->HTML($row['short']);
	$info = '<div class="be fa-home">'.$row['address'].'</div>';
	if($row['phone'])
		$info .= '<div class="be fa-phone">'.$row['phone'].'</div>';
	if($row['email'])
		$info .= '<div class="be fa-envelope">'.$row['email'].'</div>';
	if($row['work_time'])
		$info .= '<div class="be fa-clock-o">'.$row['work_time'].'</div>';
	$data['info'] = $info;
	$data['gallery'] = get_dealer_gallery($did,$data['title']);
	$output = load_html('item_dealer',$data);
  return $output;
}
function get_dealer_gallery($did,$title=''){
	global $vnT,$DB;
	$text = '';
	$result = $DB->query("SELECT * FROM dealer_picture 
												WHERE did=$did ORDER BY pic_order ASC, id DESC LIMIT 0,6 ");
  if ($num = $DB->num_rows($result)) {
  	$text .= '<div class="gridG">';
    while ($row = $DB->fetch_row($result)) {
      $title = ($row['pic_name']) ? $row['pic_name'] : $title;
      $src = $vnT->func->get_src_modules("dealer/".$row['picture'], 150 ,'',1,'1.5:1');
      $src_pic = MOD_DIR_UPLOAD.'/'.$row['picture'];
      $text .= '<div class="col">
      						<a href="'.$src_pic.'" rel="fancy1"><img src="'.$src.'" alt="'.$title.'" /></a>
    						</div>';
    }
    $text .= '</div>';
  }
  return $text;
}
function box_sidebar (){
	global $vnT, $input;
	$output = '';
 	return $output;
}
function get_list_category(){
	global $vnT, $input,$DB;
	$query= $DB->query("SELECT d.cat_id, cat_name FROM dealer_category d, dealer_category_desc dg
											WHERE d.cat_id = dg.cat_id AND lang = '$vnT->lang_name' AND display = 1 
											ORDER BY cat_order ASC, date_post DESC");
	if($num = $DB->num_rows($query)){
		$text .= '<select class="form-control" name="cat_id" id="cat_id">';
		$text .= '<option value="0">'.$vnT->lang['dealer']['select_category'].'</option>';
		while ($row = $DB->fetch_row($query)) {
			$text .= '<option value="'.$row['cat_id'].'">'.$row['cat_name'].'</option>';
		}
		$text .= '</select>';
	}else{
		$text = '';
	}
	return $text;
}

?>