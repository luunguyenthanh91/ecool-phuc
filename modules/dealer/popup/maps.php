<?php
define('IN_vnT', 1);
define('DS', DIRECTORY_SEPARATOR);
$input['mod'] = 'dealer';
require_once ("../../../_config.php");
require_once ($conf['rootpath'] . "includes/class_db.php");
$DB = new DB();
require_once ($conf['rootpath'] . "includes/class_functions.php");
$func = new Func_Global();
$conf = $func->fetchDbConfig($conf);
require_once ($conf['rootpath'] . "includes/class_libs.php");
$vnT->lib = new Lib();
$logo = $vnT->lib->get_logos('logo');
$vnT->lang_name = (isset($_GET['lang'])) ? $_GET['lang'] : "vn";
$func->load_language('dealer');
$id = (int) $_GET['id'];
$w = ($_GET['w']) ? $_GET['w'] : 700;
$h = ($_GET['h']) ? $_GET['h'] : 500;
//load
$sql = "SELECT * FROM dealer n, dealer_desc nd 
		WHERE n.did=nd.did AND display=1 AND lang='$vnT->lang_name' AND  n.did=".$id;
$result = $DB->query($sql);
if ($data = $DB->fetch_row($result)) {
  if ($data['picture']) {
    $src = $conf['rooturl'] . "vnt_upload/dealer/" . $data['picture'];
    $picture = '<img hspace="4" height="80" width="100" border="0" align="left" alt="" style="border: 1px solid rgb(172, 172, 172);" src="'.$src.'"/>';
  } else {
    $picture = '';
  }
}
$short = '<b>' . $func->txt_tooltip($func->HTML($data['title'])).'</b><br>';
$short .= "<br>" . str_replace("\r\n", "<br>", $data['map_desc']);
$description = '<div style="width: 350px; padding-right: 5px; font-family: Arial; font-size: 12px;">';
$description .= $picture;
$description .= '<span>'.$short.'</span>';
$description .= '</div>';
?>
<html>
<head>
<title>GoogleMap</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=1" />
<link href="<?php echo $conf['rooturl'];?>js/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="<?php echo $conf['rooturl'];?>skins/default/fontawesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
<link href="<?php echo $conf['rooturl'];?>skins/default/style/screen.css" rel="stylesheet" type="text/css" />
<link href="<?php echo $conf['rooturl'];?>modules/dealer/css/dealer.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="http://maps.google.com/maps/api/js?key=<?php echo $conf['GoogleMapsAPIKey']?>&language=vi
"></script>
<script type="text/javascript">
var map;
var infowindow;
function initialize(){
contentString = '<?php echo $description; ?>';
var defaultLatLng = new google.maps.LatLng(<?php
echo $data['map_lat'];
?>, <?php
echo $data['map_lng'];
?>);
var myOptions= {zoom: 16,
center: defaultLatLng,
scrollwheel : false,
mapTypeId: google.maps.MapTypeId.ROADMAP};
map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);
map.setCenter(defaultLatLng);

clickmarker = new google.maps.Marker({
position: defaultLatLng,
clickable: true,
cursor: "pointer",
map: map
});
infowindow= new google.maps.InfoWindow({content: contentString});
infowindow.open(map, clickmarker);

google.maps.event.addListener(clickmarker, 'click', function(){
infowindow.open(map, clickmarker);
});
}
</script>
<body onLoad="initialize();">
	<div id="vnt-showroom-popup" style="max-width: 100%; overflow: hidden;">
	  <div class="wrapLogo">
	    <div class="img"><?php echo $logo['header'];?></div>
	  </div>
	  <div class="wrapper">
	    <div id="map_canvas" style="width:100%; height:80%"></div>
	  </div>
	</div>
</body>
</html>