<!-- BEGIN: modules -->
<div id="vnt-content">
    <div class="vnt-main-top">
      <div class="vnt-slide">
          <div id="vnt-slide" class="slick-init">
              <div class="item"><div class="img">
                  <img src="{DIR_IMAGE}/about/slide.jpg" alt="">
              </div></div>
          </div>
          <div id="vnt-navation" class="breadcrumb hidden-sm hidden-xs" itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb">
              <div class="wrapper">
                  <div class="navation">
                      {data.navation}
                      <div class="clear"></div>
                  </div>
              </div>
          </div>
      </div>
      
  </div>
<div class="wrapping">
  <div class="wrapper">
    {data.navation}
  </div>
</div>
<div class="wrapping">
  <div class="wrapCont">
    <div class="wrapper">
      {data.main}
    </div>
  </div>
  <div id="flagEnd"></div>
</div></div>
<!-- END: modules -->
 
<!-- BEGIN: html_popup --> 
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>{CONF.indextitle} {CONF.extra_title}</title>
<meta name="robots" content="index, follow"/>
<meta name="author" content="{CONF.indextitle}"/>
<meta name="description" CONTENT="{CONF.meta_description}" />
<meta name="keywords" CONTENT="{CONF.meta_keyword}" />
<link rel="SHORTCUT ICON" href="{CONF.rooturl}favicon.ico" type="image/x-icon" />
<link href="{DIR_MOD}/css/popup.css" rel="stylesheet" type="text/css" />
</head>
<body>
<div style="padding:10px;">
{data.content}
</div>
</body>
</html>
<!-- END: html_popup --> 

<!-- BEGIN: html_sitemap --> 
{data.content}
<!-- END: html_sitemap --> 