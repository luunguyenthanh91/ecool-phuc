<?php
/*================================================================================*\
|| 							Name code : function_project.php 		 		 											  # ||
||  				Copyright © 2007 by Thai Son - CMS vnTRUST                					# ||
\*================================================================================*/
/**
* @version : 1.0
* @date upgrade : 17/12/2007 by Thai Son
**/

if ( !defined('IN_vnT') )	{ die('Access denied');	}

define("DIR_MOD", ROOT_URI . "modules/project");
define('MOD_DIR_UPLOAD', ROOT_URI . 'vnt_upload/project');
define("MOD_DIR_IMAGE", ROOT_URI . "modules/project/images");
define("LINK_MOD",  $vnT->link_root . $vnT->setting['seo_name'][$vnT->lang_name]['project']);	

function create_link ($act,$id,$title,$extra=""){
	global $vnT,$func,$DB,$conf;
	switch ($act){
		case "category" : $text = $vnT->link_root.$title.".html";	break;
		case "detail" : $text = $vnT->link_root.$title.".html";	break;
		default : $text = LINK_MOD."/".$act."/".$id."/".$func->make_url($title).".html"; break;
	}
	$text .= ($extra) ? "/".$extra : "";
	return $text;
}
function loadSetting (){
	global $vnT,$func,$DB,$conf;
	$setting = array();
	$result = $DB->query("select * from project_setting WHERE lang='$vnT->lang_name' ");
	$setting = $DB->fetch_row($result);
	foreach ($setting as $k => $v)	{
		$vnT->setting[$k] = stripslashes($v);
	}
	$res_s= $DB->query("SELECT n.cat_id,cat_code, cat_name FROM project_category n, project_category_desc nd
                      WHERE n.cat_id = nd.cat_id AND display = 1 AND lang='$vnT->lang_name' ");
  while ($row_s = $DB->fetch_row($res_s)) {
    $vnT->setting['cat_code'][$row_s['cat_id']] = $vnT->func->HTML($row_s['cat_code']);
  }
	unset($setting);
}
function load_html ($file, $data){
  global $vnT, $input;
  $html = new XiTemplate( DIR_MODULE . "/project/html/" . $file . ".tpl");
  $html->assign('DIR_MOD', DIR_MOD);
  $html->assign('LANG', $vnT->lang);
  $html->assign('INPUT', $input);
  $html->assign('CONF', $vnT->conf);
  $html->assign('DIR_IMAGE', $vnT->dir_images);
  $html->assign("data", $data);
  $html->parse($file);
  return $html->text($file);
}
function get_where_cat ($cat_id){
  global $input, $conf, $vnT;	
  $text = ""; 	
	$subcat = List_SubCat($cat_id);
	$subcat = substr($subcat, 0, - 1);
	if (empty($subcat)) $text .= " and FIND_IN_SET('$cat_id',cat_id)<>0 ";
	else{
		$tmp = explode(",", $subcat);
		$str_ = " FIND_IN_SET('$cat_id',cat_id)<>0 ";
		for ($i = 0; $i < count($tmp); $i ++)		{
			$str_ .= " or FIND_IN_SET('$tmp[$i]',cat_id)<>0 ";
		}
		$text .= " and (" . $str_ . ") ";
	}
  return $text;
}
function get_cat_name ($cat_id){
  global $func, $DB, $conf, $vnT;
  $out = $vnT->lang['project']['category'];
  $sql = "SELECT cat_name FROM project_category_desc WHERE cat_id={$cat_id} and lang='$vnT->lang_name'";
  $result = $DB->query($sql);
  if ($row = $DB->fetch_row($result)){
    $out = $func->HTML($row['cat_name']);
  }
  return $out;
}
function get_friendly_url ($table,$where=""){
  global $func, $DB, $conf, $vnT;
  $out = "";
  $sql = "SELECT friendly_url FROM {$table}_desc WHERE lang='$vnT->lang_name' {$where} ";
  $result = $DB->query($sql);
  if ($row = $DB->fetch_row($result)) {
    $out = $row['friendly_url'];
  }
  return $out;
}
function get_navation ($cat_id,$ext=''){
  global $DB,$conf,$func,$vnT,$input;
  $output = '<ul itemscope="" itemtype="http://schema.org/BreadcrumbList">';
  $output.= '<li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem" class="home"><a href="'.$vnT->link_root.'" itemscope="" itemtype="http://schema.org/Thing" itemprop="item"><span itemprop="name"><i class="fa fa-home"></i></span></a></li> ';
  $output.= '<li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem"><a itemscope="" itemtype="http://schema.org/Thing" itemprop="item" href="'.LINK_MOD.'.html"><span itemprop="name">'.$vnT->lang['project']['project'].'</span></a></li> ';
	if($cat_id){
		$cat_code = $vnT->setting['cat_code'][$cat_id];
		$tmp = explode("_",$cat_code);
		for ($i=0;$i<count($tmp);$i++){
			$res= $DB->query("SELECT cat_name,friendly_url FROM project_category_desc 
												WHERE cat_id=".$tmp[$i]." and lang='{$vnT->lang_name}' ");
			if ($r = $DB->fetch_row($res)){		
				$link = create_link("category",$tmp[$i],$r['friendly_url']);
				$output.='<li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem"><a itemscope="" itemtype="http://schema.org/Thing" itemprop="item" href="'.$link.'"><span itemprop="name">'.$func->HTML($r['cat_name']).'</span></a></li>';
			}
		}
	}
	if($ext)
		$output .= '<li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem"><span itemprop="name">'.$ext.'</span></li>';
	$output .= '</ul>';
	return $output;
}
function get_pic_thumb ($picture, $w=100, $ext="" , $is_email=0){
  global $vnT;
  $out = "";
	$w_thumb = ($vnT->setting['img_width_grid']) ? $vnT->setting['img_width_grid'] : 100 ;
	$link_url = ($is_email) ? ROOT_URL : ROOT_URI;
	$linkhinh = "vnt_upload/project/".$picture;
	$linkhinh = str_replace("//","/",$linkhinh);
	$dir = substr($linkhinh,0,strrpos($linkhinh,"/"));
	$pic_name = substr($linkhinh,strrpos($linkhinh,"/")+1) ;
	if($vnT->setting['thum_size']){
		$src = $link_url.$dir."/thumbs/{$w_thumb}_".$pic_name;				
	}else{
		$src =  $link_url.$dir."/thumbs/".$pic_name;	
	}
	if($w<$w_thumb) $ext .= " width='$w' ";
  $out = "<img src=\"{$src}\" {$ext} >";
  return $out;
}
function get_src_pic_project ($picture, $w = "" ,$h=0,$crop=0){
	global $vnT,$func;	
  $out = "";	
	$w_thumb = ($vnT->setting['img_width_grid']) ? $vnT->setting['img_width_grid'] : 100 ;	
  if ($w){
		$linkhinh = "vnt_upload/project/".$picture;
 		$linkhinh = str_replace("//","/",$linkhinh);
		$dir = substr($linkhinh,0,strrpos($linkhinh,"/"));
		$pic_name = substr($linkhinh,strrpos($linkhinh,"/")+1);
		$file_thumbs = $dir."/thumbs/{$w}_".substr($linkhinh,strrpos($linkhinh,"/")+1);
		$linkhinhthumbs = $vnT->conf['rootpath'].$file_thumbs;
		if (!file_exists($linkhinhthumbs)) {
			if (@is_dir($vnT->conf['rootpath'].$dir."/thumbs")) {
				@chmod($vnT->conf['rootpath'].$dir."/thumbs",0777);
			} else {
				@mkdir($vnT->conf['rootpath'].$dir."/thumbs",0777);
				@chmod($vnT->conf['rootpath'].$dir."/thumbs",0777);
			}
			$vnT->func->thum($vnT->conf['rootpath'].$linkhinh, $linkhinhthumbs, $w ,$h ,$crop);
		}
		$src = ROOT_URI . $file_thumbs;
  } else {
    $src = MOD_DIR_UPLOAD."/".$picture;    
  }
  return $src;
}
function get_pic_project ($picture, $w = "", $ext=""){
	global $vnT,$func;	
  $out = "";
	$w_thumb = ($vnT->setting['img_width_grid']) ? $vnT->setting['img_width_grid'] : 100 ;	
	$linkhinh = "vnt_upload/project/".$picture;
	$linkhinh = str_replace("//","/",$linkhinh);
	$dir = substr($linkhinh,0,strrpos($linkhinh,"/"));
	$pic_name = substr($linkhinh,strrpos($linkhinh,"/")+1);
  if ($w){
		$file_thumbs = $dir."/thumbs/{$w}_".substr($linkhinh,strrpos($linkhinh,"/")+1);
		$linkhinhthumbs = $vnT->conf['rootpath'].$file_thumbs;
		if (!file_exists($linkhinhthumbs)) {
			if (@is_dir($vnT->conf['rootpath'].$dir."/thumbs")) {
				@chmod($vnT->conf['rootpath'].$dir."/thumbs",0777);
			} else {
				@mkdir($vnT->conf['rootpath'].$dir."/thumbs",0777);
				@chmod($vnT->conf['rootpath'].$dir."/thumbs",0777);
			}
			$vnT->func->thum($vnT->conf['rootpath'].$linkhinh, $linkhinhthumbs, $w,$w ,"1:1");
		} 
		$src = ROOT_URI . $file_thumbs;
  } else {
    $src = MOD_DIR_UPLOAD . "/" . $picture;    
  }
	$alt = substr($pic_name, 0, strrpos($pic_name, "."));
  $out = "<img src=\"{$src}\" {$ext}>";
  return $out;
}
function List_Status_Pro ($selname, $did, $ext = ""){
  global $func, $DB, $conf, $vnT;
  $text = "<select name=\"{$selname}\" class='select'  {$ext}   >";
  $text .= "<option value=\"0\" selected>".$vnT->lang['project']['select_status']."</option>";
  $sql = "SELECT n.*, nd.title FROM project_status n, project_status_desc nd 
					WHERE n.status_id=nd.status_id AND nd.lang='$vnT->lang_name' AND  display=1 
					ORDER BY s_order ASC, n.status_id DESC ";
  $result = $DB->query($sql);
  while ($row = $DB->fetch_row($result)){
	 	$selected = ($row['status_id'] == $did) ? " selected " : "";
		$text .= "<option value=\"{$row['status_id']}\" {$selected} >".$vnT->func->HTML($row['title'])."</option>";		
  }
  $text .= "</select>";
  return $text;
}
function Get_Cat($did=-1,$ext){
	global $func,$DB,$conf,$vnT;
	$text= "<select size=1  name=\"cat_id\"  class=\"select\" $ext >";
	$text.="<option value=\"0\">-- Tất cả danh mục --</option>";
	$query= $DB->query("SELECT c.cat_id,cd.cat_name,cd.description 
											FROM project_category c , project_category_desc cd
											WHERE c.cat_id=cd.cat_id AND cd.lang='$vnT->lang_name' AND c.parentid=0 
											ORDER BY cat_order ASC ,c.cat_id DESC");
	while ($cat=$DB->fetch_row($query)) {
		$cat_name = $vnT->func->HTML($cat['cat_name']);
		if ($cat['cat_id']==$did)
			$text.="<option value=\"{$cat['cat_id']}\" selected>{$cat_name}</option>";
		else
			$text.="<option value=\"{$cat['cat_id']}\" >{$cat_name}</option>";
		$n=1;
		$text.=Get_Sub($cat['cat_id'],$n,$did);
	}
	$text.="</select>";
	return $text;
}
function Get_Sub($cid,$n,$did=-1){
	global $vnT,$func,$DB,$conf;
	$output="";
	$k=$n;
	$query= $DB->query("SELECT c.cat_id,cd.cat_name,cd.description 
											FROM project_category c , project_category_desc cd
											WHERE c.cat_id=cd.cat_id AND cd.lang='$vnT->lang_name' AND c.parentid={$cid}
											ORDER BY cat_order ASC ,c.cat_id DESC ");
	while ($cat=$DB->fetch_row($query)) {
	$cat_name = $vnT->func->HTML($cat['cat_name']);
		if ($cat['cat_id']==$did){
			$output.="<option value=\"{$cat['cat_id']}\" selected>";
			for ($i=0;$i<$k;$i++) $output.= "--";
			$output.=" {$cat_name}</option>";
		} else {	
			$output.="<option value=\"{$cat['cat_id']}\" >";
			for ($i=0;$i<$k;$i++) $output.= "--";
			$output.=" {$cat_name}</option>";
		}
		$n=$k+1;
		$output.=Get_Sub($cat['cat_id'],$n,$did);
	}
	return $output;
}
function List_Search($did){
	global $func,$DB,$conf,$vnT;
	$text= "<select size=1 name=\"search\" >";
	if ($did =="maso")
		$text.="<option value=\"maso\" selected> {$vnT->lang['project']['code_project']} </option>";
	else
		$text.="<option value=\"maso\"> {$vnT->lang['project']['code_project']} </option>";
	if ($did =="name")
		$text.="<option value=\"name\" selected> {$vnT->lang['project']['p_name']} </option>";
	else
		$text.="<option value=\"name\"> {$vnT->lang['project']['p_name']} </option>";
	$text.="</select>";
	return $text;
}
function get_option ($info){
	global $vnT,$func,$DB;
	if ($info['options']){
		$textout='<table width="100%" border="0" cellspacing="1" cellpadding="1" class="table">';
		$arr_op = unserialize($info['options']);
		$res_op = $DB->query("SELECT * FROM project_option n, project_option_desc nd  
													WHERE n.op_id=nd.op_id AND lang ='$vnT->lang_name' AND n.display=1 
													ORDER BY n.op_order, n.op_id DESC");
		while ($r_op = $DB->fetch_row($res_op)){
			if($arr_op[$r_op['op_id']]){
				$textout .='<tr>
											<td class="colInfo1" width="80" nowrap>'.$func->HTML($r_op['op_name']).' : </td>
											<td class="colInfo2">'.$arr_op[$r_op['op_id']].'</td>
										</tr>';
			}
		}
		$textout.="</table>";
	}
	return $textout;
}
function get_imgvote ($votes){
  global $func, $DB, $conf;
  $voteimg = "";
  $votenum = floor($votes);
  $votemod = $votes - $votenum;
  for ($n = 0; $n < $votenum; $n ++)
    $voteimg .= "<img src=\"" . MOD_DIR_IMAGE . "/star2.gif\" />";
  if ($votemod > 0.3){
    if ($votemod > 0.7)
      $voteimg .= "<img src=\"" . MOD_DIR_IMAGE . "/star2.gif\" />";
    else
      $voteimg .= "<img src=\"" . MOD_DIR_IMAGE . "/star1.gif\"  />";
    for ($n = 0; $n < (4 - $votenum); $n ++)
      $voteimg .= "<img src=\"" . MOD_DIR_IMAGE . "/star0.gif\"  />";
  } else {
    for ($n = 0; $n < (5 - $votenum); $n ++)
      $voteimg .= "<img src=\"" . MOD_DIR_IMAGE . "/star0.gif\"  />";
  }
  return $voteimg;
}
function get_p_name ($id){
	global $func,$DB,$conf,$vnT;
	$p_name="";
	$result = $DB->query("SELECT p_name FROM project_desc WHERE lang='$vnT->lang_name' AND p_id=$id ");
	if ($row= $DB->fetch_row($result)){
		$p_name = $func->HTML($row['p_name']);
	}
	return $p_name ;
}
function List_SubCat ($cat_id){
  global $func, $DB, $vnT;
  $output = "";
  $query = $DB->query("SELECT * FROM project_category WHERE parentid={$cat_id}  ");
  while ($cat = $DB->fetch_row($query)){
    $output .= $cat["cat_id"] . ",";
    $output .= List_SubCat($cat['cat_id']);
  }
  return $output;
}
function get_tooltip ($data){
  global $func, $DB, $vnT, $conf;
	// cache
 	$param_cache = array($data['p_id']);
	$cache = $vnT->Cache->read_cache("pro_tooltip",$param_cache);
	if ($cache != _NOC) return $cache;
  $output = "";	
  $output .= '<div class=fTooltip>' . $vnT->func->HTML($data['p_name']) . ' </div>';	
	$output.='<table width=100%  border=0 cellspacing=0 cellpadding=0>';
	$output.='<tr >
            <td  class=tInfo1 nowrap width=50  >'.$vnT->lang['project']['maso'].' </td>
            <td width=10  class=tInfo1 align=center> : </td>
            <td class=tInfo2 ><b >'.$data['maso'].'</b></td>
        </tr>';
	$output.='<tr >
            <td valign="top" class=tInfo1 nowrap  >'.$vnT->lang['project']['price'].' </td>
            <td width=10 valign="top" class=tInfo1 align=center> : </td>
            <td class=tInfo2 ><b class=tPrice>'.get_price_project($data['price']).'</b></td>
        </tr>';
	//option
	if ($data['options']){
		 
		$arr_op = unserialize($data['options']);		 
		$res_op = $DB->query("select * from project_option n, project_option_desc nd  
													where  n.op_id=nd.op_id
													AND nd.lang='$vnT->lang_name'
													AND n.display=1  
													AND n.focus=1
													ORDER BY n.op_order ASC ,  n.op_id DESC");
		while ($r_op = $DB->fetch_row($res_op)){
			if($arr_op[$r_op['op_id']]){
				$output .= '<tr>
											<td class=tInfo1  nowrap >'.$func->HTML($r_op['op_name']).' </td>
											<td width=10 valign="top" class=tInfo1 align=center> : </td>
											<td class=tInfo2> <span class="color">'.$arr_op[$r_op['op_id']].'</span></td>
										</tr>';
			}
		}
	}
				
	$output.="</table>";
	$output .= '<table width=100% border=0 cellspacing=2 cellpadding=2><tr><td >';  
	if($data['desc_status']) {
		$output .= '<h3 class=th3 >'.$vnT->lang['project']['f_promotion'].'</h3>'; 
  	$output .= '<div align=justify >'.$vnT->func->HTML($data['desc_status']) . '</div>';
	}
	$output .='</tr></table>';
	$vnT->Cache->save_cache("pro_tooltip", $output, $param_cache);
  return $output;
}
function html_row ($row){
  global $input, $conf, $vnT, $func;
  $pID = (int) $row['p_id'];
	$w = ($vnT->setting['img_width_grid']) ? $vnT->setting['img_width_grid'] : 370;
  $data['link'] = create_link("detail",$pID,$row['friendly_url']);
	$data['p_name'] = $vnT->func->HTML($row['p_name']);
	$picture = ($row['picture']) ? 'project/'.$row['picture'] : 'project/'.$vnT->setting['pic_nophoto'];
	$data['pic'] = $vnT->func->get_pic_modules($picture, $w, 0, " alt='".$row['p_name']."' title='".$row['p_name']."' ",1,0,array("fix_width"=>1)); 
	$data['short'] = $vnT->func->cut_string($vnT->func->check_html($row['description'],'nohtml'),240,1);
	$data['address'] = ($row['address']) ? '<div class="map">'.$vnT->func->HTML($row['address']).'</div>' : '';
	$data['cat_name'] = get_cat_name($row['cat_id']);
  $output = load_html("item_project", $data);
  return $output;
}
function html_col ($data){
  global $input, $vnT, $func, $DB;
  $pID = (int) $data['p_id'];
  $link = create_link("detail",$pID,$data['friendly_url']);
  $data['checkbox'] = "<input name=\"ch_id[]\" id=\"ch_id\" type=\"checkbox\" value='" . $pID . "'  class=\"checkbox\" onClick=\"javascript:select_list('item{$pID}')\"  />";
	$w = ($vnT->setting['img_width_list']) ? $vnT->setting['img_width_list'] : 100;
	$pic = ($data['picture']) ?  get_pic_project($data['picture'],$w," alt='".$data['p_name']."' title='".$data['p_name']."' ") : "<img  src=\"" . MOD_DIR_IMAGE . "/nophoto.gif\"  >";
  $data['pic'] = "<a href=\"{$link}\" title='".$data['p_name']."'>{$pic}</a>";
	$p_name = $vnT->func->HTML($data['p_name']);	
  $data['name'] = "<a href=\"{$link}\" title='".$data['p_name']."' >" . $p_name . "</a>";
  $data['list_option']  = get_option($data) ; 
  return load_html("item_view2", $data);
}
function row_project ($where, $start, $n, $num_row=4, $view=1){
  global $vnT, $input, $DB, $func, $conf;
  $text = "";
  $sql = "SELECT * FROM project p, project_desc pd
					WHERE p.p_id=pd.p_id AND display=1 AND lang='$vnT->lang_name' $where LIMIT $start,$n";
  $result = $vnT->DB->query($sql);
  if ($num = $vnT->DB->num_rows($result)){
    if ($view == 2){
      $i = 1;
      while ($row = $DB->fetch_row($result)){
        $text .= html_col($row);
        $i ++;
      }
    } else {
			while ($row = $DB->fetch_row($result)){
				$text .= html_row($row);
			}
    }
  } else {
    $text = "<div class='noItem'>{$vnT->lang['project']['no_have_project']}</div>";
  }
  return $text;
}
function scroll_project ($scroll_name, $where,$start=0,$n=10){
	global $DB, $func, $input, $vnT;  
	//$param_cache = array($scroll_name,$input['pID']) ;	
	//$cache = $vnT->Cache->read_cache("scroll_project",$param_cache);
	//if ($cache != _NOC) return $cache;   
	$sql = "SELECT * FROM project p, project_desc pd
					WHERE p.p_id=pd.p_id AND display=1 AND pd.lang='$vnT->lang_name' $where LIMIT $start,$n";
	$result = $vnT->DB->query($sql);
	if ($num = $vnT->DB->num_rows($result)){
		$w = ($vnT->setting['img_width_grid']) ? $vnT->setting['img_width_grid'] : 270;
		while ($row = $vnT->DB->fetch_row($result)){
		  $data['link'] = create_link("detail",$pID,$row['friendly_url']);
			$data['p_name'] = $vnT->func->HTML($row['p_name']);
			$picture = ($row['picture']) ? "project/".$row['picture'] : "project/".$vnT->setting['pic_nophoto'];
		  $data['src'] = $vnT->func->get_src_modules($picture, $w ,'',1,'1.5:1');
		  $text .= load_html("item_view2", $data);
		}
	} else{
		$text .='<li class=noItem>'.$vnT->lang['project']['no_have_project'].'</li>';
	}
	$text_out = $text;
	//$vnT->Cache->save_cache("scroll_project", $text_out,$param_cache);	
	return $text_out;
}
function box_sidebar (){
	global $vnT, $input;
	$textout = '';
	$textout .= $vnT->lib->get_banner_sidebar('sidebar');;
 	return $textout;
}
function box_filter(){
	global $vnT,$BD,$input;
	$data = array();
	$data['box_category'] = box_category();
	$data['Display_Filter'] = Display_Filter($input['display']);
	$data['Sort_Filter'] = Sort_Filter($input['sort']);
	return load_html('box_filter',$data);
}
function Display_Filter($key){
	global $vnT;
	$n = ($vnT->setting['n_grid']) ? $vnT->setting['n_grid'] : 8;
	$arr[1]['key'] = $n;
	$arr[1]['value'] = $n;
	$arr[2]['key'] = $n + $n;
	$arr[2]['value'] = $n + $n;
	$arr[3]['key'] = $n + $n*2;
	$arr[3]['value'] = $n + $n*2;
	$arr[4]['key'] = $n + $n*3;
	$arr[4]['value'] = $n + $n*3;
	$cur_link = $vnT->seo_url;
	$cur_title = $n;
	$text .= '<select name="display" id="display" onchange="location=this.value;">';
	for($i=1;$i<=count($arr);$i++){
		$selected = ($key==$arr[$i]['key']) ? 'selected' : '';
		$link = build_links($cur_link,"display",$arr[$i]['key']);
		if($key==$arr[$i]['key']) $cur_title = $arr[$i]['key'];
		$text .= '<option value="'.$link.'" '.$selected.'>'.$arr[$i]['value'].'</option>';
	}
	$text .= '</select>';
	$data['f_title'] = $vnT->lang['project']['display_filter'];
	$data['content'] = $text;
	return $vnT->skin_box->parse_box("box_project_filter", $data);
}
function Sort_Filter($key){
	global $vnT;
	$arr[1]['key'] = 'new';
	$arr[1]['value'] = $vnT->lang['project']['sort_new'];

	$arr[2]['key'] = 'old';
	$arr[2]['value'] = $vnT->lang['project']['sort_old'];

	// $arr[3]['key'] = 'low';
	// $arr[3]['value'] = $vnT->lang['product']['price_low'];
	
	// $arr[4]['key'] = 'hight';
	// $arr[4]['value'] = $vnT->lang['product']['price_hight'];
	$cur_link = $vnT->seo_url;
	$text ='<select name="sort" id="sort" onchange="location=this.value;">';
	$link_default = build_links($cur_link,"sort",'default');
	$text.= '<option value="'.$link_default.'">'.$vnT->lang['project']['sort_default'].'</option>';
	for($i=1;$i<=count($arr);$i++){
		$selected = ($key==$arr[$i]['key']) ? 'selected' : '';
		$link = build_links($cur_link,"sort",$arr[$i]['key']);
		$text .= '<option value="'.$link.'" '.$selected.'>'.$arr[$i]['value'].'</option>';
	}
	$text .= '</select>';
	$data['f_title'] = $vnT->lang['project']['sort_filter'];
	$data['content'] = $text;
	return $vnT->skin_box->parse_box("box_project_filter", $data);
}
function build_links ($cur_link ,$key,$value, $act="add")	{
	global $vnT ,$input;
	$new_link = "";
	$p = ((int) $input['p']) ? (int) $input['p'] : 1;
	$check = @explode("?",$cur_link);
	if($check[1]){
		$arr_new_param = @explode("&", $check[1]);	
		$is_new = 1;
		$new_link = $check[0]."?" ;
		foreach ($arr_new_param as $param){
			$tmp = @explode("=",$param);
			if ( $tmp[0] == $key ) {
				$new_link .=  $tmp[0] . "=" .$value ."&" ;
				$is_new=0;
			}else{
				$new_link .=  $param ."&" ;	
			}
		}
		if($is_new==1) {
			$new_link .= $key."=".$value; 
		}else{
			$new_link = substr($new_link,0,-1); 	
		}
	}else{
		$new_link = $cur_link ."/?".$key."=".$value; 	
	}
	return $new_link ;
}
function box_category (){
    global $vnT, $func, $DB, $conf, $input;
    $text = '';
    $cat_id = (int) $input['catID'];
    $parentid = get_parent_id($cat_id);
    $query= $DB->query("SELECT n.*,nd.cat_name,nd.friendly_url FROM project_category n, project_category_desc nd
                        WHERE n.cat_id=nd.cat_id AND lang='{$vnT->lang_name}' AND display=1 AND parentid={$parentid}
                        ORDER BY cat_order ASC, nd.cat_id DESC, date_post DESC");
    if ($num = $DB->num_rows($query)) {
      $f_title = $vnT->lang['project']['cat_filter'];
      while ($row = $DB->fetch_row($query)) {
        $link = create_link('category',$row['cat_id'], $row['friendly_url']);
        $class = ($cat_id == $row['cat_id']) ? 'active' : '';
        if($cat_id == $row['cat_id'])
          $f_title = $vnT->func->HTML($row['cat_name']);
        //$text.= '<div class="myCheckbox '.$class.'"><a href="'.$link.'">'.$vnT->func->HTML($row['cat_name']).'</a></div>';
        $text.= '<option value="'.$link.'">'.$vnT->func->HTML($row['cat_name']).'</option>';
      }
    //   $textout = '<div class="filter"><div class="txt">'.$vnT->lang['project']['cat_filter'].'</div>
      //              <div class="icon">'.$f_title.'</div><div class="popup">';
      // $textout.= $text;
    //   $textout.= '</div></div>';
    }
    return $text;
  }
function get_parent_id($cat_id){
  global $vnT,$DB;
  $parentid = 0;
  $result = $DB->query("SELECT n.cat_id, parentid FROM project_category n, project_category_desc nd
                        WHERE n.cat_id = nd.cat_id AND display = 1
                        AND lang='$vnT->lang_name' AND parentid = {$cat_id}");
  if($num = $DB->num_rows($result)){
    $parentid = $cat_id;
  } else{
    $result = $DB->query("SELECT parentid FROM project_category WHERE cat_id = {$cat_id}");
    if($row = $DB->fetch_row($result))
      $parentid = $row['parentid'];
  }
  return $parentid;
}
function get_price_project ($price,$unit ='đ',$default=""){
	global $func,$DB,$conf,$vnT;
	if ($price){
		$price = $func->format_number($price).$unit;
	}else{
		$price = ($default) ? $default : $vnT->lang['project']['price_default'];
	}
	return $price;
}
?>