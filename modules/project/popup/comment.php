<?php
	session_start();
	define('IN_vnT',1);
	define('DS', DIRECTORY_SEPARATOR);
	require_once("../../../_config.php"); 
	require_once($conf['rootpath']."includes/class_db.php"); 
	$DB = new DB;
	//load mailer
	require_once($conf['rootpath']."libraries/phpmailer/phpmailer.php"); 	
	$vnT->mailer = new PHPMailer();	
	
	require_once($conf['rootpath']."includes/class_functions.php"); 	
	$func = new Func_Global;
	$conf=$func->fetchDbConfig($conf);
	
	$linkMod = "?".$conf['cmd']."=mod:project";
	$vnT->lang_name = (isset($_GET['lang'])) ? $_GET['lang']  : "vn" ;
	$func->load_language('project');
	
	$id= (int)$_GET['id'];


	$ok_send =0;
	$dirUpload = "vnt_upload/project";
	
	$setting = array();
  $result = $DB->query("select * from project_setting");
  $setting = $DB->fetch_row($result);
  foreach ($setting as $k => $v)
  {
    $vnT->setting[$k] = stripslashes($v);
  }
  unset($setting);
	
	$result = $DB->query("select * from project p, project_desc pd 
											 where p.p_id=pd.p_id and pd.lang='{$vnT->lang_name}' AND p.p_id=$id ");
	
	$listSP = '<table width="100%" border="0" cellspacing="1" cellpadding="1" bgcolor="#CCCCCC">';
	if ($row = $DB->fetch_row($result)){
	
		$w_thumb = ($vnT->setting['img_width_grid']) ? $vnT->setting['img_width_grid'] : 100 ;
		$linkhinh = $dirUpload."/".$row['picture'];
		$linkhinh = str_replace("//","/",$linkhinh);
		$dir = substr($linkhinh,0,strrpos($linkhinh,"/"));
		$src = $conf['rooturl'].$dir."/thumbs/{$w_thumb}_".substr($linkhinh,strrpos($linkhinh,"/")+1);	
		
		$link = $conf['rooturl']."project/detail/".$row['id']."/".$func->make_url($row['p_name']).".html";
		
		$name ="<a href='".$link."'><strong>".$func->HTML($row['p_name'])."</strong></a>";
		$price = $func->format_number($row['price'])." VNĐ";
		$res_cat = $DB->query("select cat_name from project_category_desc where cat_id=".$row['catRoot']." and lang='".$vnT->lang_name."'");
		if($row_cat = $DB->fetch_row($res_cat));
		
		$cat_name = $row_cat['cat_name'];
		$listSP.= "<tr bgcolor='#ffffff'>
					<td width=160 align=center><img src='".$src."' height=100 /></td>
					<td style='padding-left:5px;'>
					<p>".$vnT->lang['project']['category']." : <strong>".$cat_name."</strong> </p>
					<p>".$vnT->lang['project']['project']." : ".$name."</p>
					<p>".$vnT->lang['project']['price']." : <b style='color:#CC0000'>".$price."</b></p></td>
				 </tr>";
	}
	$listSP .= "</table>";
	$text ='<p><strong>'.$vnT->lang['project']['introduce_project'].'</strong> :</p>';
	$text .= $listSP;
	
	if (isset($_POST['btnSend']) ) {
	  if(empty($_SESSION['issend']) || $_SESSION['issend']!=$id ) 
		{
			$data = $_POST;
			$vote = (int) $_POST['mark'];
			$cot['username'] = $func->txt_HTML($_POST['username']);
			$cot['id'] = $id;
			$cot['content'] = $func->txt_HTML($_POST['content']);
			$cot['mark'] = $vote;
			$cot['date_post'] = time();
			$ok = $DB->do_insert ("project_comment",$cot);
			if ($ok){
				$_SESSION['issend']=$id;
				//cap nhat vote
				$res = $DB->query("select numvote,votes from project where p_id=$id");
				if ($row=$DB->fetch_row($res)){
					$votes=(($row['votes']*$row['numvote'])+$vote)/($row['numvote']+1);
					$dup['votes'] = $votes;
					$dup['numvote'] = $row['numvote']+1;
					$DB->do_update("project",$dup,"p_id=$id");			
				}
				
				$mess= $func->html_mess('Bạn đã viết nhận xét thành công. Nhận xét này đang được chờ duyệt. Xin cảm ơn bạn');
				$ok_send =1;
			}else{
				$mess= $func->html_mess('Viết nhận xét thất bại . Thử lại lần nữa');
			}
	  }else{
	  	 $mess= $func->html_mess("Bạn đã viết nhận xét cho dự án này rồi");
			 $ok_send =1;
	  }		
	}

	mt_srand ((double) microtime() * 1000000);
	$num  = mt_rand(100000,999999);
	$scode = $func->NDK_encode($num);
	$img_code = "../../../includes/sec_image.php?code=$scode";
?>
<html>
<head>
<title>.: DANH GIA SAN PHAM :.</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<link href="../css/popup.css" rel="stylesheet" type="text/css" />
<script language=javascript>
 
	function checkform(f) {			
			
		username = f.username.value;
		if (username == '') {
			alert('Vui lòng nhập tên bạn');
			f.username.focus();
			return false;
		}

		var content = f.content.value;
		if (content == '') {
			alert('Vui lòng nhập nội dung');
			f.content.focus();
			return false;
		}
		
		if (f.h_code.value != f.security_code.value ) {
			alert('Mã bảo vệ không chính xác');
			f.security_code.focus();
			return false;
		}
		
		return true;
	}
</script>
 
</head>

<body  >

<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td  height="30" class="font_f_title" ><img src="../images/i_comment.gif"  align="absmiddle"/>&nbsp;VIẾT NHẬN XÉT CHO SẢN PHẨM </td>
	</tr>
	<tr>
		<td  bgcolor="#FF0000" height="2" ></td>
	</tr>
</table>
<table width="100%"  border="0" align="center" cellpadding="0" cellspacing="0" >
	<tr>
			<td height="10" ></td>
		</tr>
		<tr>
	<td align="center" height="30" ><?php echo $text; ?></td>
</tr>
	<tr><td height="10" ></td></tr>
		<tr>
			<td ><?php echo $mess ; ?></td>
		</tr>
		<tr>
			<td align="left">
			<?php
				if ($ok_send==0){
?>
			<form action="<?php echo $link_action; ?>" method="post" name="myform" id="myform" onSubmit="return checkform(this);">
					<table width="100%"  border="0" align="center" cellpadding="2" cellspacing="2">

						<tr>
							<td width="30%"><strong>Họ tên</strong> <font color="red">(*)</font> : </td>
							<td><input id="username" name="username" size="40"  class="textfiled"/></td>
						</tr>
						<tr>
							<td valign="top"><strong>Nội dung</strong>: <font color="red">(*)</font></td>
							<td><textarea class="textarea" name="content" cols="35" rows="7"  id="content"></textarea></td>
						</tr>

						<tr>
							<td ><strong>Điểm : </strong></td>
							<td><select name="mark">
							<option value="1"> 1 </option>
							<option value="2"> 2 </option>
							<option value="3" selected="selected"> 3 </option>
							<option value="4"> 4 </option>
							<option value="5" > 5 </option>
							</select></td>
						</tr>
						<tr>
							<td ><strong>Mã bảo vệ :</strong> <font color="red">(*)</font></td>
							<td><input id="security_code" name="security_code" size="15" maxlength="6" class="textfiled"/>&nbsp;<img src="<?php echo $img_code ;?>" align="absmiddle" /></td>
						</tr>
					 
						<tr align="center" height="30">
							<td colspan="2">
	<input type="hidden" name="h_code" value="<?php echo $num ; ?>">
	<input type="submit" name="btnSend" value=" Gửi " class="button" /> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	<input type="reset" name="Reset" value="Nhập lại" class="button" /></td>
						</tr>
					</table>
			</form>
			<?php
				}else{
echo '<table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr>
			<td height="150"><div align="center"><a href = "javascript:self.parent.tb_remove();"><font color="#990000" size="2" face="Verdana, Arial, Helvetica, sans-serif"><br>
Close Window</font></a> </div>
<div align="center"></div></td>
			</tr>
		</table>';
}
?>
			</td>
		</tr>
</table>

</body>

</html>