<?php
/*================================================================================*\
|| 							Name code : main.php 		 		 																	  # ||
||  				Copyright © 2007 by Thai Son - CMS vnTRUST                					# ||
\*================================================================================*/
/**
 * @version : 1.0
 * @date upgrade : 11/12/2007 by Thai Son
 **/
if (! defined('IN_vnT'))
{
  die('Access denied');
}
$nts = new sMain();
class sMain
{
  var $output = "";
  var $skin = "";
  var $linkUrl = "";
  var $module = "project";
  var $act = "search";
  
  /**
   * function sMain ()
   * Khoi tao 
   **/
  function sMain ()
  {
    global $vnT, $input;
    include ("function_" . $this->module . ".php");
    loadSetting();
    $this->skin = new XiTemplate( DIR_MODULE ."/". $this->module . "/html/". $this->module . ".tpl");
    $this->skin->assign('DIR_MOD', DIR_MOD);
    $this->skin->assign('LANG', $vnT->lang);
    $this->skin->assign('INPUT', $input);
    $this->skin->assign('CONF', $vnT->conf);
    $this->skin->assign('DIR_IMAGE', $vnT->dir_images);    
    $vnT->html->addStyleSheet(DIR_MOD . "/css/" . $this->module . ".css");
    $vnT->html->addScript(DIR_MOD . "/js/" . $this->module . ".js");   
		
		$vnT->setting['menu_active'] = $this->module; 
		//SEO		
		if ($vnT->setting['metakey'])	$vnT->conf['meta_keyword'] = $vnT->setting['metakey'];
		if ($vnT->setting['metadesc'])	$vnT->conf['meta_description'] = $vnT->setting['metadesc'];		
		if ($vnT->setting['friendly_title']){	 
			$vnT->conf['indextitle'] = $vnT->setting['friendly_title'] ;	
		}
     
		 
		$data['main'] = $this->do_Search();	  
		$data['box_sidebar'] = box_sidebar(); 
		 
    $this->skin->assign("data", $data);
    $this->skin->parse("modules");
    $vnT->output .= $this->skin->text("modules");
  
  }
   
  /**
   * function list_project 
   * 
   **/
  function do_Search ()
  {
    global $DB, $func, $input, $vnT;
    $p = ((int) $input['p']) ? (int) $input['p'] : 1;
		
		$where="";		
		$ext_pag = "";		
		
		$cat_id = $input['cat_id'];
		
		if($cat_id)
		{
			$where .=  get_where_cat($cat_id);
			$ext_pag .= "&cat_id=".$input['cat_id'];
		}
  		
		//keyword
		if (isset($input['keyword'])) $keyword=$input['keyword'];
		if ($keyword){
			$key =  $vnT->func->get_keyword($keyword) ;
			$where .= " and (p_name like '%".$key."%' or LOWER(p_name) like '%".$key."%' ) ";
			$ext_pag .= "&keyword=" . rawurlencode($key);
		}
  		
		$sql_num = "SELECT p.p_id
								FROM project p, project_desc pd
								WHERE p.p_id=pd.p_id 
								AND lang='$vnT->lang_name'
								AND display=1 								
								$where ";
    //echo $sql_num;
    $res_num = $vnT->DB->query($sql_num);
    $totals = $vnT->DB->num_rows($res_num);
		
		$view = 1;
		$num_row=3;
		$n_list = ($vnT->setting['n_list']) ? $vnT->setting['n_list'] : 10;
		$n_grid = ($vnT->setting['n_grid']) ? $vnT->setting['n_grid'] : 20;
		$n = ($view == 2) ? $n_list : $n_grid;
			
    $num_pages = ceil($totals / $n);
    if ($p > $num_pages) $p = $num_pages;
    if ($p < 1) $p = 1;
    $start = ($p - 1) * $n;
		
		if($num_pages>1) {			
			$root_link = LINK_MOD."/search" ;	
			$nav = "<div class=\"pagination\">".$vnT->func->htaccess_paginate($root_link,$totals,$n,$ext_pag,$p)."</div>" ;
		}
		
		$where .= " ORDER BY date_post DESC" ; 		
		$data['list_search'] = row_project($where, $start, $n, $num_row, $view);
		$data['nav'] = $nav ;
 		
		$note_result = str_replace("{totals}","<b class=font_err>".$totals."</b>",$vnT->lang['project']['note_result']); 
		$data['note_result'] = str_replace("{num_pages}","<b>".$num_pages."</b>",$note_result);
		
		$data['list_cat'] = Get_Cat($cat_id);
		$data['link_search'] = LINK_MOD."/search" ;	
		
		$data['f_title'] = $vnT->lang['project']['f_search'];
		$this->skin->reset("html_search");
		$this->skin->assign("data", $data);
    $this->skin->parse("html_search");
    $textout = $this->skin->text("html_search");
		
    return $textout;
		
  }
  

  
// end class
}
?>