$(document).ready(function(){
	// FILTER
    // $(".myCheckbox").click(function(){
    //     if(!$(this).hasClass("active")){
    //         $(this).addClass("active");
    //         $(this).find("input").prop('checked', true);
    //     }
    //     else{
    //         $(this).removeClass("active");
    //         $(this).find("input").prop('checked', false);
    //     }
    // });
    $(".filter .icon").click(function(){
        if(!$(this).parents(".filter").hasClass("active")){
            $(this).parents(".filter").addClass("active");
        }
        else{
            $(this).parents(".filter").removeClass("active");
        }
    });
    // MANSORY
    $(window).on("load",function(){
        $('.gridMansory').masonry({
            itemSelector: '.gridMansory .col',
            columnWidth: '.gridMansory .colC',
            percentPosition: true
        });
    });
    // THUMNAIL
    $("#slideThumbnail").slick({
        arrows:false,
        autoplay:true,
        speed:500,
    });
    // SLIDE IMG
    $("#slideImg").slick({
        autoplay:true,
        speed:500,
    });
    $("#slideImg .item a").fancybox({
        padding:0,
        minWidth: '100%',
        minHeight : '100%',
        margin: 0,
        type:'iframe',
        // closeBtn: false,
        wrapCSS: 'designGal',
    });
    $(".projectContent .link a").fancybox({
        padding     : 0,
        maxWidth    : 740,
        width       : '99%',
        height      : '99%',
        autoSize    : true,
        fitToView   : false,
        autoHeight  : true,
        autoWidth   : true,
        type        : 'iframe',
        wrapCSS     : 'myDesignPopup',
        margin      : [80,20,20,20],
    });
});