<?php
define('IN_vnT', 1);
require_once ("../../_config.php");
require_once ("../../includes/class_db.php");
require_once ("../../includes/class_functions.php");
$DB = new DB();
$func = new Func_Global();
$conf = $func->fetchDbConfig($conf);
$vnT->lang_name = ($_GET['lang']) ? $_GET['lang'] : "vn";
$func->load_language('project');
$func->load_language('project');
$dirUpload = '../../vnt_upload/project';
$result = $DB->query("select * from project_setting");
$setting = $DB->fetch_row($result);
foreach ($setting as $k => $v) {
  $vnT->setting[$k] = stripslashes($v);
}
$id = (int) $_GET['p_id'];
$pic_order = (int) $_GET['pic'];

/*-------------- get_pic_thumb --------------------*/
function get_pic_thumb ($picture, $w = 100, $ext = "")
{
  global $vnT, $conf;
  $out = "";
  $w_thumb = ($vnT->setting['img_width_grid']) ? $vnT->setting['img_width_grid'] : 100;
  if ($w) {
    $linkhinh = "vnt_upload/project/" . $picture;
    $linkhinh = str_replace("//", "/", $linkhinh);
    $dir = substr($linkhinh, 0, strrpos($linkhinh, "/"));
    $pic_name = substr($linkhinh, strrpos($linkhinh, "/") + 1);
    if ($vnT->setting['thum_size']) {
      $file_thumbs = $dir . "/thumbs/{$w_thumb}_" . substr($linkhinh, strrpos($linkhinh, "/") + 1);
      $src = $conf['rooturl'] . $file_thumbs;
    } else {
      if ($w < $w_thumb)
        $ext .= " width='$w' ";
      $src = $conf['rooturl'] . $dir . "/thumbs/" . $pic_name;
    }
  } else {
    $src = $conf['rooturl'] . "vnt_upload/project/" . $picture;
  }
  $out = "<img  src=\"{$src}\" {$ext} >";
  return $out;
}
$text_out = "";
$row_pic .= "<table border=\"0\" cellspacing=\"2\" cellpadding=\"2\" > 
											<tr>";
$sql = "SELECT p.*, pd.p_name, pd.description
					FROM project p, project_desc pd
					WHERE p.p_id=pd.p_id 
					AND p.display=1 
					AND pd.lang='" . $vnT->lang_name . "'
					AND p.p_id =$id ";
$result = $DB->query($sql);
if ($data = $DB->fetch_row($result)) {
  $src = $dirUpload . "/" . $data['picture'];
  $data['pic'] = "<img src=\"{$src}\" />";
  $row_pic .= "<td  valign=top><a href=\"#\" onclick=\"showPreview('{$src }','0');return false\">" . get_pic_thumb($data['picture'], 60) . "</a</td>";
}
$res = $DB->query("select * from project_picture where p_id=" . $id);
if ($num = $DB->num_rows($res)) {
  $i_pic = 0;
  while ($row = $DB->fetch_row($res)) {
    $i_pic ++;
    $src = $dirUpload . "/" . $row['picture'];
    $row_pic .= "<td  valign=top><a href=\"#\" onclick=\"showPreview('{$src }','{$i_pic}');return false\">" . get_pic_thumb($row['picture'], 60) . "</a</td>";
    if ($i_pic == 10) {
      $row_pic .= "</tr><tr>";
      $i_pic = 0;
    }
  }
}
$row_pic .= "</tr></table>";
$data['row_pic'] = $row_pic;
echo html_view_pic($data);

function html_view_pic ($data)
{
  global $conf, $vnT;
  return <<<EOF
<script language="JavaScript" >
var displayWaitMessage=true;
function showPreview(imagePath,imageIndex){
	var subImages = document.getElementById('previewPane').getElementsByTagName('IMG');
	
	if(subImages.length==0){
		var img = document.createElement('IMG');
		document.getElementById('previewPane').appendChild(img);			
	}else img = subImages[0];
	
	
	if(displayWaitMessage){
		document.getElementById('waitMessage').style.display='inline';
	}
	img.onload = function() { hideWaitMessageAndShowCaption(imageIndex-1); };
	img.src = imagePath;
}
function hideWaitMessageAndShowCaption(imageIndex){
	document.getElementById('waitMessage').style.display='none';	
}

//document.oncontextmenu=new Function("alert('Welcome AnCuong.com');return false");
</script>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>PIC PRODUCT</title>
<style type="text/css">
html {
	SCROLLBAR-BASE-COLOR: #CCCCCCC;
	SCROLLBAR-ARROW-COLOR: #000000;
}
body {
	background:#fff;
	margin-left: 5px;
	margin-top: 5px;
	margin-right: 5px;
	margin-bottom: 5px;
}
td,th {
	FONT-FAMILY: verdana, arial, helvetica, sans-serif;
	font-size: 11px;
	color: #000000;
}
a:active {  text-decoration: none; color:#FF0707}
a:link {  text-decoration: none ; color:#FF0707}
a:visited {  text-decoration: none ; color:#FF0707}
a:hover {  color:#1074EF; text-decoration: underline; }

.button {
	cursor:pointer;
	border:outset 1px #b76500;
	color:#000;
	font-size:11px;
	font-weight:bold;
	padding: 2px 4px;
	background:url(images/bg_button.gif) repeat-x left top;
}

img { border : 0px; }
.img_border {
	border:1px solid #ccc;
	padding:1px;
}
.font_err{ color:#FF0000; }
	#previewPane img{
		line-height:400px;
	}
	#previewPane #largeImageCaption{	/* CSS styling of image caption below large image */
		font-style:italic;
		text-align:center;
		font-family: Trebuchet MS, Lucida Sans Unicode, Arial, sans-serif;	/* Font to use */
		font-size:0.9em;
	}	
	#waitMessage{
		display:none;
		position:absolute;
		left:150px;
		top:100px;
		background-color:#fff;
		border:3px double #000;
		padding:4px;
		color:#555;
		font-size:11pt;
		font-family:arial;	
		width:400px;
	}
</style>
<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
  <tr>
    <td style="background:#efefef" align="center">{$data['row_pic']}</td>
  </tr>
</table>
<br />

<table align="center" border="0" cellspacing="0" cellpadding="0" >
 
  <tr>
    <td id="previewPane" style="border:1px solid #000000; background:#000000; text-align:center;">{$data['pic']}</td>
  </tr>  
</table>
<br />
<table width="100%" border="0" cellspacing="2" cellpadding="0" >
  <tr>
    <td id="waitMessage">{$vnT->lang['project']['wait_load_image']}</td>
  </tr>
</table>		
EOF;
}
?>
