<?php
	define('IN_vnT',1);
	require_once("../../../_config.php"); 
	require_once("../../../includes/class_db.php"); 
	require_once("../../../includes/class_functions.php"); 	
	$DB = new DB;
	$func = new Func_Global;
	$conf=$func->fetchDbConfig($conf);
	
	$vnT->lang_name = (isset($_GET['lang'])) ? $_GET['lang']  : "vn" ;
	$func->load_language('project');

	$vnT->imagesdir= $conf['rooturl']."modules/project/images/";
	$vnT->dirMod = $conf['rooturl']."modules/project/";
	
	$id= (int)$_GET['id'] ;
	$p = ((int)$_GET['p']) ? $_GET['p'] : 1 ;
	
	$res = $DB->query("select n_comment from project_setting where id=1");
	$r = $DB->fetch_row($res);
	$n = ($r['n_comment']) ? $r['n_comment'] : 5 ;

	$sql_num = "select * from project_comment where id=$id and display=1 ";
	$result_num = $DB->query($sql_num); 
	$totals = $DB->num_rows($result_num);
	
	
	$num_pages = ceil($totals/$n) ;
	if ($p > $num_pages) $p=$num_pages;
	if ($p < 1 ) $p=1;
	$start = ($p-1) * $n ; 
	$object = "show_comment({$id},'{$vnT->lang_name}',";
	$nav = "<div class=\"pagination\">".$func->paginate_js($totals, $n, $p, $object)."</div>" ;
	
	$sql ="select * from project_comment where id=$id and display=1 order by cid DESC LIMIT $start,$n";
	$result=$DB->query ($sql);
	if ($num=$DB->num_rows($result))
	{
		$text ="";
		$i=0;
		while ($row=$DB->fetch_row($result))
		{
			$date_post = date("H:i - d/m/Y",$row['date_post']);

			$email = ($row['hidden_email']==0) ? '&nbsp;<span class="femail">('.$row['email'].')</span>' : "";

			$class  = ($i==$num) ? "class='last'" : "" ;
			$text.='<div class="list_comment">
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
					  <tr>
							<td class="hr_comment"  ><img src="'.$vnT->imagesdir.'comment.gif"  align="absmiddle" />&nbsp;<strong class="fname">'.$func->HTML($row['name']).'</strong>'.$email.'&nbsp;<span clas="fdate">'.$vnT->lang['project']['post_at'].' '.$date_post.'</span></td>					  
					  <tr>
							<td>'.$row['content'].'</td>
					  </tr>
						</table>
						</div>';
			
		}
	}
	
	$jsout = $text ;
	$jsout .= $nav ;
	
header("Cache-Control: no-cache, must-revalidate");
echo $jsout;	
	
 
?>