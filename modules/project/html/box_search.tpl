<!-- BEGIN: box_search -->
<div  class="toolSearch" >
<script type="text/javascript">

function check_search(f){
	var len = f.keyword.value.length;
	var key_default = "{LANG.project.keyword_default}" ;
	if(f.keyword.value==key_default)
	{
		f.keyword.value="";
	}
	return true;
}

</script>
<form action="{data.link_action}" method="post"  onsubmit="return check_search(this);" name="fSearch" id="fSearch" >
<input type="hidden" name="do_search" value="1" />
<table width="100%" border="0" cellspacing="2" cellpadding="2">
  <tr>
   <td ><label><strong>{LANG.project.category}</strong></label>{data.list_category}</td> 
  </tr>  
  <tr> 
    <td  ><label><strong>{LANG.project.keyword}</strong></label><input type="text" class="textfiled" name="keyword" id="keyword" onfocus="if(this.value=='{LANG.project.keyword_default}') this.value='';" onblur="if(this.value=='') this.value='{LANG.project.keyword_default}';"  value="{data.keyword}"   style="width:100%"  /> </td>    
  </tr>
  <tr> 
  <td align="center"  ><button  id="btnSearch" name="btnSearch" type="submit" class="btn" value="{LANG.project.btn_search}" ><span >{LANG.project.btn_search}</span></button> </td> 
  </tr>  
</table> 
</form>
</div>
<!-- END: box_search -->