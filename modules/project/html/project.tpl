<!-- BEGIN: modules -->
  <style>
     /* Set the size of the div element that contains the map */
    #map {
      height: 400px;  /* The height is 400 pixels */
      width: 100%;  /* The width is the width of the web page */
     }
  </style>

  <div class="page-system-cool">
      <div class="container">
        <nav aria-label="breadcrumb">
          <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="/">Home</a></li>
            <li class="breadcrumb-item active" aria-current="page"><a href="{data.link_action}">{LANG.project.daily}</a></li>
          </ol>
        </nav>
      </div>

      <div class="container container--790">
        <h1 class="text-center mt-5 mb-5">{LANG.project.daily}</h1>
        <p class="mt-5 mb-5">{data.slogan}</p>
        <form class="form-search-system text-center" action="{data.link_action}/" >
          <div class="form-group">
            <input class="input-search-system form-group border-form-system form-control" name="keyword" type="text" placeholder="{LANG.project.timkiem}">
            <button class="btn-search-system" type="submit"><img class="img-btn-search" src="{DIR_IMAGE}/search.png"></button>
          </div>
          <div class="form-group"><span>Sử dụng vị trí của bạn</span></div>
        </form>
        <div class="mt-5 mb-5 map-contact" id="map">

        </div>
        <!-- <iframe class="mt-5 mb-5 map-contact" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d29786.60214010603!2d105.72967043335746!3d21.059667202056662!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x313454e33407ecc7%3A0x6b9d843be64f7f06!2zTWluaCBLaGFpLCBC4bqvYyBU4burIExpw6ptLCBIw6AgTuG7mWksIFZp4buHdCBOYW0!5e0!3m2!1svi!2s!4v1577599643198!5m2!1svi!2s" frameborder="0" style="border:0;" allowfullscreen=""></iframe> -->
    <script>
        // Initialize and add the map
        function initMap() {
          // The location of Uluru
          var uluru = {lat: -25.344, lng: 131.036};
          // The map, centered at Uluru
          var map = new google.maps.Map(
              document.getElementById('map'), {zoom: 4, center: uluru});
          // The marker, positioned at Uluru
          var marker = new google.maps.Marker({position: uluru, map: map});
        }
    </script>
    <!--Load the API from the specified URL
    * The async attribute allows the browser to render the page while the API loads
    * The key parameter will contain your own API key (which is not needed for this tutorial)
    * The callback parameter executes the initMap() function
    -->

    <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBhiPT-4aUbBschW3uLu7BmyJ85AlhHSko&callback=initMap"
  type="text/javascript"></script>
        <table class="mt-5 mb-4 table table-sm">
          <thead>

            <tr>
              <th scope="col" width="15%">{LANG.project.khoangcach}</th>
              <th scope="col" width="20%">{LANG.project.dl}</th>
              <th scope="col">{LANG.project.diachi}</th>
              <th>
                <form name="menuform">
                <select class="form-control" name="menu2"
onChange="top.location.href = this.form.menu2.options[this.form.menu2.selectedIndex].value;
return false;">
                  <option value="1">Tất cả</option>
                  {data.box_category}

                </select>
              </form>
              </th>
            </tr>
          </thead>
          <tbody>
            <div class="tr-system-ecool mt-3 mb-3">

              {data.main}
            </div>
          </tbody>
        </table>
          <!-- <p class="see-more-system-ecool text-center">Xem thêm</p></a> -->
      </div>

    </div>
<!-- END: modules -->

<!-- BEGIN: html_list -->
 {data.row_project}

<!-- END: html_list -->

<!-- BEGIN: html_search -->
<div class="formSearch">
  <form name='modSearch' id="modSearch" method='get' action="{data.link_action}">
    <table width="90%" border="0" cellspacing="2" cellpadding="2" align="center">
      <tr>
        <td class="col1" width="20%" nowrap="nowrap"><strong>{LANG.project.keyword}</strong></td>
        <td><input type="text" class="textfiled" name="keyword" id="keyword" value="{INPUT.keyword}" size="30"/></td>
      </tr>
      <tr>
        <td class="col1" nowrap="nowrap" ><strong>{LANG.project.category}</strong></td>
        <td>{data.list_cat}</td>
      </tr>
      <tr>
        <td class="col1" nowrap="nowrap">&nbsp;</td>
        <td>
          <button id="btnSearch" name="btnSearch" type="submit" class="btn" value="{LANG.project.btn_search}">
            <span>{LANG.project.btn_search}</span>
          </button>
        </td>
      </tr>
    </table>
  </form>
</div>
<p class="mess_result">{data.note_keyword} <br>{data.note_result}</p>
<div id="List_View">
  {data.list_search}
  <br class="clear">
</div>
{data.nav}
<!-- END: html_search -->

<!-- BEGIN: detail -->
<div class="projectWrap">
  <div class="row">
    <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
      <div id="slideThumbnail" class="slick-init">
        <div class="item"><img src="{data.src}" alt="{data.p_name}" /></div>
      </div>
    </div>
    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
      <div class="dfTitle">
        <div class="title"><h1>{data.p_name}</h1></div>
        {data.btn360}
      </div>
      <div class="projectAttr">
        <ul>
          {data.row_option}
          <li>
            <div class="at">{LANG.project.address}</div>
            <div class="as">{data.address}</div>
            <div class="clear"></div>
          </li>
        </ul>
      </div>
      {data.price_text}
      <div class="projectContact">
        <div class="t1">{LANG.project.consultant}: </div>
        <div class="t2">{CONF.hotline} - {CONF.hotline2}</div>
      </div>
      <div class="projectSocial">
        <ul>
          <li><a href="https://www.facebook.com/sharer.php?u={data.link_share}" title="Chia sẻ lên Facebook" target="_blank"><i class="fa fa-facebook"></i></a></li>
          <li><a href="https://plus.google.com/share?url={data.link_share}" title="Chia sẻ lên Google+" target="_blank"><i class="fa fa-google"></i></a></li>
          <li><a href="https://twitter.com/intent/tweet?url={data.link_share}" title="Chia sẻ lên Twitter" target="_blank"><i class="fa fa-twitter"></i></a></li>
          <li><a href="http://pinterest.com/pin/create/link/?url={data.link_share}" title="Chia sẻ lên Pinterest" target="_blank"><i class="fa fa-pinterest"></i></a></li>
        </ul>
      </div>
    </div>
  </div>
</div>
<div class="projectContent">
  <div class="grid">
    <div class="col">
      <div class="caption">
        <div class="tend">{data.p_name}</div>
        <div class="des">{data.text_short}</div>
        <div class="link"><a href="{data.link_desc}"><span>{LANG.project.view_more}</span></a></div>
      </div>
    </div>
    <div class="col">
      <div class="img"><img src="{data.src}" alt="{data.p_name}" /></div>
    </div>
  </div>
</div>
<div id="slideImg" class="slick-init">
  {data.list_pic}
</div>
<div class="templateDesign">
  {data.description2}
</div>
<!-- END: detail -->

<!-- BEGIN: html_other_project -->
<div class="box-project-other">
  <div class="mid-title">
    <h1 class="titleL">{data.f_title}</h1>
    <div class="titleR">{data.more}</div>
    <div class="clear"></div>
  </div>
  <div class="box-content-other">
      {data.list}
      <div class="clear"></div>
  </div>
</div>
<!-- END: html_other_project -->

<!-- BEGIN: html_360 -->
<div class="iframe360">
  <iframe src="{data.link360}" frameborder="0" gesture="media" allow="encrypted-media" allowfullscreen></iframe>
</div>
<!-- END: html_360 -->

<!-- BEGIN: html_desc -->
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:fb="http://www.facebook.com/2008/fbml">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1" />
  <title>Trang chủ</title>
  <!-- DÙNG CHUNG CHO TÒAN SITE -->
  <link href="{DIR_JS}/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
  <link href="{DIR_STYLE}/font/fontawesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
  <link href="{DIR_STYLE}/screen.css" rel="stylesheet" type="text/css" />
  <script type="text/javascript" src="{DIR_JS}/jquery.min.js"></script>
  <link href="{DIR_MOD}/css/project.css" rel="stylesheet" type="text/css" />
  <link rel="stylesheet" href="{DIR_JS}/mscrollbar/jquery.mCustomScrollbar.min.css" type="text/css" />
  <script type="text/javascript" src="{DIR_JS}/mscrollbar/jquery.mCustomScrollbar.js"></script>
  <script type="text/javascript">
    $(document).ready(function(){
      if(typeof $(".the-info-content").offset() =='object'){
        $(".the-info-content").mCustomScrollbar();
      }
    });
  </script>
</head>
<body>
  <div class="designPopup tra-about" style="max-width: 740px;">
    <div class="content">
      <div class="the-info-wrap">
        <div class="the-info-content desc">
          <div class="t1">{data.p_name}</div>
          {data.description}
        </div>
      </div>
    </div>
  </div>
</body>
</html>
<!-- END: html_desc -->

<!-- BEGIN: html_slide -->
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:fb="http://www.facebook.com/2008/fbml">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1" />
  <title>Trang chủ</title>
  <link href="{DIR_JS}/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
  <link href="{DIR_JS}/slick/slick.css" rel="stylesheet" type="text/css" />
  <link href="{DIR_JS}/menumobile/menumobile.css" rel="stylesheet" type="text/css" />
  <link href="{DIR_JS}/fancybox/jquery.fancybox.css" rel="stylesheet" type="text/css" media="screen"/>
  <link href="{DIR_STYLE}/font/fontawesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
  <link href="{DIR_STYLE}/screen.css" rel="stylesheet" type="text/css" />
  <script type="text/javascript" src="{DIR_JS}/jquery.min.js"></script>
  <script type="text/javascript" src="{DIR_JS}/jquery-migrate.min.js"></script>
  <script type="text/javascript" src="{DIR_JS}/bootstrap/js/bootstrap.min.js"></script>
  <script type="text/javascript" src="{DIR_JS}/slick/slick.min.js"></script>
  <script type="text/javascript" src="{DIR_JS}/menumobile/menumobile.js"></script>
  <script type="text/javascript" src="{DIR_JS}/fancybox/jquery.fancybox.js"></script>
  <script type="text/javascript" src="{DIR_JS}/core.js"></script>
  <link href="{ROOT_URL}modules/collection/css/collection.css" rel="stylesheet" type="text/css" />
  <script type="text/javascript" src="{ROOT_URL}modules/collection/js/collection.js"></script>
</head>
<body>
  <div id="slideGalleryWrap">
    <div id="slideGallery" class="slick-init">
      {data.list_for}
    </div>
    <div id="slideNavGallery">
      {data.list_nav}
    </div>
  </div>
</body>
</html>
<!-- END: html_slide -->
