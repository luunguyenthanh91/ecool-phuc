<?php
/*================================================================================*\
|| 							Name code : main.php 		 		 																	  # ||
||  				Copyright © 2007 by Thai Son - CMS vnTRUST                					# ||
\*================================================================================*/
/**
 * @version : 1.0
 * @date upgrade : 11/12/2007 by Thai Son
 **/
if (! defined('IN_vnT'))
{
  die('Access denied');
}
$nts = new sMain();
class sMain
{
  var $output = "";
  var $skin = "";
  var $linkUrl = "";
  var $module = "project";
  var $act = "status";
  
  /**
   * function sMain ()
   * Khoi tao 
   **/
  function sMain ()
  {
    global $vnT, $input;
    include ("function_" . $this->module . ".php");
    loadSetting();
    $this->skin = new XiTemplate( DIR_MODULE ."/". $this->module . "/html/". $this->module . ".tpl");
    $this->skin->assign('DIR_MOD', DIR_MOD);
    $this->skin->assign('LANG', $vnT->lang);
    $this->skin->assign('INPUT', $input);
    $this->skin->assign('CONF', $vnT->conf);
    $this->skin->assign('DIR_IMAGE', $vnT->dir_images);
    
    $vnT->html->addStyleSheet(DIR_MOD . "/css/" . $this->module . ".css");
    $vnT->html->addScript(DIR_MOD . "/js/" . $this->module . ".js");    
    
 		
    //check category
    $id = (int) $input['sID'];
    $res_ck = $vnT->DB->query("SELECT *
							FROM project_status n, project_status_desc nd
							WHERE n.status_id=nd.status_id
							AND lang='$vnT->lang_name' 
							AND display=1							
							AND n.status_id=$id");
    if ($row = $vnT->DB->fetch_row($res_ck))
    {
			//SEO
			$vnT->conf['indextitle'] =  $row['title'] ; 
 			
		  $data['main'] .= $this->list_project($row);      
    } else    {
			$linkref = LINK_MOD.".html";
      @header("Location: " . $linkref . "");
      echo "<meta http-equiv='refresh' content='0; url=" . $linkref . "' />";
    }
		$data['box_sidebar'] = box_sidebar();
		
    $this->skin->assign("data", $data);
    $this->skin->parse("modules");
    $vnT->output .= $this->skin->text("modules");
  
  }
 
  
  /**
   * function list_project 
   * 
   **/
  function list_project ($info)
  {
    global $DB, $func, $input, $vnT;
    $p = ((int) $input['p']) ? (int) $input['p'] : 1;
   
	  $sID = (int) $info['status_id'];
    $num_row=3;	
    $view = 1;
		
		$where = " AND status=$sID ";
     
    $cat_id =(int)$input['catID'] ;
    //lay dieu kien
    if ($cat_id)   {
       $where .= get_where_cat($cat_id);
    }     
		
    
    
    //lay tong so
    $sql_num = "SELECT  p.p_id
								FROM project p, project_desc pd
								WHERE p.p_id=pd.p_id 
								AND lang='$vnT->lang_name'
								AND display=1 								
								$where ";
    //echo $sql_num;
    $res_num = $vnT->DB->query($sql_num);
    $totals = $vnT->DB->num_rows($res_num);
    
	  $n_list = ($vnT->setting['n_list']) ? $vnT->setting['n_list'] : 10;
		$n_grid = ($vnT->setting['n_grid']) ? $vnT->setting['n_grid'] : 20;
		$n = ($view == 2) ? $n_list : $n_grid;
    
    $num_pages = ceil($totals / $n);
    if ($p > $num_pages) $p = $num_pages;
    if ($p < 1) $p = 1;
    $start = ($p - 1) * $n;
		
		if($num_pages>1)
		{
			$root_link = create_link("status",$sID,$info['title']);
			$nav = "<div class=\"pagination\">".$vnT->func->htaccess_paginate($root_link,$totals,$n,$ext_pag,$p)."</div>" ;
		}
      
    $where .= " ORDER BY date_post DESC " ;    
    
 
		$data['description'] = $info['description'];
    //show ket qua
    $data['row_project'] = row_project($where, $start, $n, $num_row, $view);
    $data['nav'] = $nav;		
    $this->skin->assign("data", $data);
    $this->skin->parse("html_list");
		
		$nd['content'] = $this->skin->text("html_list");
 		$nd['f_title']= $vnT->func->HTML($info['title']);
		$textout = $vnT->skin_box->parse_box("box_middle",$nd);
			
		return $textout;
		
  }
  

  
// end class
}
?>