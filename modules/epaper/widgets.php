<?php
/*================================================================================*\
|| 							Name code : function_epaper.php 		 		 												  # ||
||  				Copyright © 2007 by Thai Son - CMS vnTRUST                					# ||
\*================================================================================*/
/**
 * @version : 1.0
 * @date upgrade : 17/12/2007 by Thai Son
 **/
 
 
if (! defined('IN_vnT')) {
  die('Access denied');
}

  
 /**
 * @function : box_search  
 * @param 		:  null
 * @return		:  text
 */

function box_search ()
{
	global $vnT, $input ,$DB, $func;
	$text_out = "";
	
	$data['keyword'] = ($input['keyword']) ? $input['keyword'] : $vnT->lang['epaper']['keyword_default'];
	$data['link_search']  = LINK_MOD."/search/";
	$text_out = load_html("box_search",$data);
	return $text_out ;
}


/*-------------- box_category --------------------*/
function box_category ()
{
  global $func, $DB, $conf, $vnT, $input;
  // lay ROOT cat
  $sql = "select n.cat_id,nd.cat_name,nd.friendly_url 
					from epaper_category n , epaper_category_desc nd
					where n.cat_id=nd.cat_id
					and lang='$vnT->lang_name' 
					and parentid=0 
					and display=1
					order by cat_order ASC, n.cat_id ASC";
  $result = $DB->query($sql);
  if ($num = $DB->num_rows($result)) {
    $cat_cur =  $input["catID"];
    $text = '<div class="box_category"><ul >';
    $i = 0;
    while ($row = $DB->fetch_row($result)) {
      $i ++;
      $cat_id = $row['cat_id'];
      $show_sub = 0; // mac dinh ban dau
      $arr_child = Get_Child($cat_id);
      $arr_child[] = $cat_id;
      $active = "";
      if (in_array($cat_cur, $arr_child)) {
        $show_sub = 1;
        $active = "class='current'";
      } else {
        $active = "";
      }
      $class = ($i == $num) ? " class ='last' " : "";
      $link = create_link("category", $cat_id, $row['friendly_url']);
      $text .= "<li {$active} ><a href='{$link}'   ><span  >" . $vnT->func->HTML($row['cat_name']) . "</span></a>";
      //check sub
      $sql_sub = "select n.cat_id,nd.cat_name,nd.friendly_url 
					from epaper_category n , epaper_category_desc nd
					where n.cat_id=nd.cat_id
					and lang='$vnT->lang_name' 
					and parentid=$cat_id 
					and display=1
					order by cat_order ASC, n.cat_id ASC";
      $res_sub = $vnT->DB->query($sql_sub);
      if ($num_sub = $vnT->DB->num_rows($res_sub)) {
        $text_sub = "<ul>";
        while ($row_sub = $vnT->DB->fetch_row($res_sub)) {
          $active_sub = ($cat_cur == $row_sub['cat_id']) ? " class ='current' " : "";
          $link_sub = create_link("category", $row_sub['cat_id'], $row_sub['friendly_url']);
          $text_sub .= "<li ><a href='{$link_sub}' {$active_sub} > " . $vnT->func->HTML($row_sub['cat_name']) . "</a></li>";
        }
        $text_sub .= "</ul>";
        if ($show_sub) {
          $text .= $text_sub;
        }
      }
      $text .= "</li>";
    }
    $text .= '</ul></div>';
		
    
		$nd['content'] = $text;
    $nd['f_title'] = $vnT->lang['epaper']['category'];
    $textout = $vnT->skin_box->parse_box("box", $nd);
    return $textout ;
		
  } else {
    return '';
  }
}

 
//========Check Sub===============
	function Check_Sub($cid){
	global $DB,$vnT;
		$query = $DB->query("SELECT * FROM epaper_category WHERE parentid ={$cid}");
		if ($scat=$DB->fetch_row($query)) return 1;
		else return 0;
	}
	
	//=========Get Child ============
	function Get_Child($cid) {
	global $DB,$func,$input,$conf,$vnT;
		$sql ="select * from epaper_category where parentid = '{$cid}'";
		$query = $DB->query($sql);
		while ($row=$DB->fetch_row($query)) {			
			$text[]=$row['cat_id'];
			$res_sub = $DB->query("select cat_id,parentid from epaper_category where parentid=".$row['cat_id']);
			while ($row_sub=$DB->fetch_row($res_sub)) 
			{		
				$text[]=$row_sub['cat_id'];
			}
		}	
	return $text;	
	}

 
  
?>