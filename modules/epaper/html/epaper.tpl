<!-- BEGIN: modules -->
<div class="container">
  <div class="row">
    <div class="col-12">
      <nav class="fs-10 font-weight-light" aria-label="breadcrumb">
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="/">Trang chủ</a></li>
          <li class="breadcrumb-item"><a href="">Tài liệu</a></li>

        </ol>
      </nav>
    </div>
  </div>
</div>
    <div class="epaper-box">
      <div class="row">
        {data.main}
      </div>
    </div>
<!-- END: modules -->

<!-- BEGIN: html_list -->


      {data.row_epaper}


<!-- END: html_list -->
