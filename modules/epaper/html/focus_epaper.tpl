<!-- BEGIN: focus_epaper -->
<div class="box-epaper">
  <div class="box-title">
    <h1 class="titleL">{data.f_title}</h1>
    <div class="titleR">{data.more}</div>
    <div class="clear"></div>
  </div>
  <div class="box-content">
  	<div id="List_epaper">
    {data.list}
    <div class="clear"></div>
    </div>
  </div>          
</div>
<!-- END: focus_epaper -->