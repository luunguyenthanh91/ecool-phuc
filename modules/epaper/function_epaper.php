<?php
/*================================================================================*\
|| 							Name code : function_epaper.php 		 		 											  # ||
||  				Copyright © 2007 by Thai Son - CMS vnTRUST                					# ||
\*================================================================================*/
/**
* @version : 1.0
* @date upgrade : 17/12/2007 by Thai Son
**/

if ( !defined('IN_vnT') )	{ die('Access denied');	}

define("DIR_MOD", ROOT_URI . "modules/epaper");
define('MOD_DIR_UPLOAD', ROOT_URI . 'vnt_upload/epaper/book_image');
define("MOD_DIR_IMAGE", ROOT_URI . "modules/epaper/images");
define("LINK_MOD", $vnT->link_root . $vnT->setting['seo_name'][$vnT->lang_name]['epaper']);	
 
/*-------------- create_link --------------------*/
function create_link ($act,$id,$title,$extra=""){
	global $vnT,$func,$DB,$conf;

	switch ($act)
	{
		case "category" : $text = LINK_MOD."/".$title."-".$id.".html";	 break ;
		case "detail" : $text = LINK_MOD."/".$act."/".$title."-".$id.".html";	 break ;
		default : $text = LINK_MOD."/".$act."/".$id."/".$func->make_url($title).".html";	 break ;
	}
	$text .= ($extra) ? "/".$extra : "" ;
	
	return $text;
}
	
/*-------------- loadSetting --------------------*/
function loadSetting (){
	global $vnT,$func,$DB,$conf;
	$setting = array();
	$result = $DB->query("select * from epaper_setting WHERE lang='$vnT->lang_name' ");
	$setting = $DB->fetch_row($result);
	foreach ($setting as $k => $v)	{
		$vnT->setting[$k] = stripslashes($v);
	}
  
	unset($setting);
}

/**
 * function load_html ()
 * 
 **/
function load_html ($file, $data)
{
  global $vnT, $input;
  $html = new XiTemplate( DIR_MODULE . "/epaper/html/" . $file . ".tpl");
  
  $html->assign('DIR_MOD', DIR_MOD);
  $html->assign('LANG', $vnT->lang);
  $html->assign('INPUT', $input);
  $html->assign('CONF', $vnT->conf);
  $html->assign('DIR_IMAGE', $vnT->dir_images);
  $html->assign("data", $data);
  
  $html->parse($file);
  return $html->text($file);
}
 
 /*--------------------------- get_cat_name ---------------------*/
function get_friendly_url ($table,$where="")
{
  global $func, $DB, $conf, $vnT;
  $out = "";
  $sql = "SELECT friendly_url FROM {$table}_desc WHERE lang='$vnT->lang_name' {$where} ";
  $result = $DB->query($sql);
  if ($row = $DB->fetch_row($result)) {
    $out = $row['friendly_url'];
  }
  return $out;
}
 /*-------------- get_pic_thumb --------------------*/
function get_pic_thumb ($picture, $w=100, $ext="" , $is_email=0)
{
  global $vnT;
  $out = "";
	$w_thumb = ($vnT->setting['img_width_grid']) ? $vnT->setting['img_width_grid'] : 100 ;
	
	$link_url = ($is_email) ? ROOT_URL : ROOT_URI ;	
 
	$linkhinh = "vnt_upload/epaper/".$picture;
	$linkhinh = str_replace("//","/",$linkhinh);
	$dir = substr($linkhinh,0,strrpos($linkhinh,"/"));
	$pic_name = substr($linkhinh,strrpos($linkhinh,"/")+1) ;
	
	if($vnT->setting['thum_size'])
	{
		$src = $link_url.$dir."/thumbs/{$w_thumb}_".$pic_name;				
	}else{
		$src =  $link_url.$dir."/thumbs/".$pic_name;	
	}	 

	if($w<$w_thumb) $ext .= " width='$w' ";
	
  $out = "<img  src=\"{$src}\" {$ext} >";
	
  return $out;
}


/*-------------- get_src_pic_epaper --------------------*/
function get_src_pic_epaper ($picture, $w = "")
{
	global $vnT,$func;	
  $out = "";	
	$w_thumb = 212;	
  if ($w)  
	{
		$linkhinh = "vnt_upload/epaper/book_image/".$picture;
 		$linkhinh = str_replace("//","/",$linkhinh);
		$dir = substr($linkhinh,0,strrpos($linkhinh,"/"));
		$pic_name = substr($linkhinh,strrpos($linkhinh,"/")+1) ;
		
		 
			$file_thumbs = $dir."/thumbs/{$w}_".substr($linkhinh,strrpos($linkhinh,"/")+1);
			$linkhinhthumbs = $vnT->conf['rootpath'].$file_thumbs;
			
			if (!file_exists($linkhinhthumbs)) {
				if (@is_dir($vnT->conf['rootpath'].$dir."/thumbs")) {
					@chmod($vnT->conf['rootpath'].$dir."/thumbs",0777);
				} else {
					@mkdir($vnT->conf['rootpath'].$dir."/thumbs",0777);
					@chmod($vnT->conf['rootpath'].$dir."/thumbs",0777);
				}		
				// thum hinh
				$vnT->func->thum($vnT->conf['rootpath'].$linkhinh, $linkhinhthumbs, $w);
			} 
			$src = ROOT_URI . $file_thumbs;
		 	
		    
  } else {
    $src = MOD_DIR_UPLOAD . "/" . $picture;    
  }

  return $src;
}

/*-------------- get_pic_epaper --------------------*/
function get_pic_epaper ($picture, $w = "", $ext="")
{
	global $vnT,$func;	
  $out = "";	
	
	$w_thumb = ($vnT->setting['img_width_grid']) ? $vnT->setting['img_width_grid'] : 100 ;	
	$linkhinh = "vnt_upload/epaper/book_image/".$picture;
	$linkhinh = str_replace("//","/",$linkhinh);
	$dir = substr($linkhinh,0,strrpos($linkhinh,"/"));
	$pic_name = substr($linkhinh,strrpos($linkhinh,"/")+1) ;
		
  if ($w)  
	{		 
			$file_thumbs = $dir."/thumbs/{$w}_".substr($linkhinh,strrpos($linkhinh,"/")+1);
			$linkhinhthumbs = $vnT->conf['rootpath'].$file_thumbs;
			
			if (!file_exists($linkhinhthumbs)) {
				if (@is_dir($vnT->conf['rootpath'].$dir."/thumbs")) {
					@chmod($vnT->conf['rootpath'].$dir."/thumbs",0777);
				} else {
					@mkdir($vnT->conf['rootpath'].$dir."/thumbs",0777);
					@chmod($vnT->conf['rootpath'].$dir."/thumbs",0777);
				}		
				// thum hinh
				$h = 295;
				$vnT->func->thum($vnT->conf['rootpath'].$linkhinh, $linkhinhthumbs, $w, $w, '1:1');
			} 
			$src = ROOT_URI . $file_thumbs;
 		    
  } else {
    $src = MOD_DIR_UPLOAD . "/" . $picture;    
  }
	
	$alt = substr($pic_name, 0, strrpos($pic_name, "."));
  $out = "<img  src=\"{$src}\"  {$ext} >";
  return $out;
}


  
  

/*-------------- get_imgvote --------------------*/
function get_imgvote ($votes)
{
  global $func, $DB, $conf;
  $voteimg = "";
  $votenum = floor($votes);
  $votemod = $votes - $votenum;
  for ($n = 0; $n < $votenum; $n ++)
    $voteimg .= "<img src=\"" . MOD_DIR_IMAGE . "/star2.gif\" />";
  
  if ($votemod > 0.3)
  {
    if ($votemod > 0.7)
      $voteimg .= "<img src=\"" . MOD_DIR_IMAGE . "/star2.gif\" />";
    else
      $voteimg .= "<img src=\"" . MOD_DIR_IMAGE . "/star1.gif\"  />";
    for ($n = 0; $n < (4 - $votenum); $n ++)
      $voteimg .= "<img src=\"" . MOD_DIR_IMAGE . "/star0.gif\"  />";
  }
  else
  {
    for ($n = 0; $n < (5 - $votenum); $n ++)
      $voteimg .= "<img src=\"" . MOD_DIR_IMAGE . "/star0.gif\"  />";
  }
  
  return $voteimg;
}

  
  
/*-------------- get_pic_other --------------------*/
function get_pic_other ($info, $w = 50,$n=3)
{
  global $func, $DB, $conf, $vnT;
  $out = array();
	$e_id = (int)$info['e_id'];
	$pic_w = ($vnT->setting['imgdetail_width']) ? $vnT->setting['imgdetail_width'] : 250;
  
	$result = $DB->query("Select * from epaper_picture where e_id=$e_id");
  if ($num = $DB->num_rows($result))
  {
		
		$list_pic = '<table border="0" cellspacing="1" cellpadding="1"><tr>'; 		
		
		$src_big = get_src_pic_epaper($info['picture'],$pic_w);		
		$list_pic .= "<td align='center' class='other_pic'><table width='{$w}' border=0 cellspacing=0 cellpadding=0 align=center  ><tr><td height='{$w}' class='img' ><a href='#Pic0' onClick=\"load_image('".$src_big."')\" >".get_pic_epaper($info['picture'],$w," class='img_border' ")."</a></td></tr></table></td>";

    $i = 0;
 		$dem = 1;
		$list_src .= '';
    while ($row = $DB->fetch_row($result))
    {
  		$i ++;    
			$dem ++;  
      $src_big = get_src_pic_epaper($row['picture'],$pic_w);
			
      $list_pic .= "<td align='center' class='other_pic'><table width='{$w}' border=0 cellspacing=0 cellpadding=0><tr><td height='{$w}' align=center class='img' ><a  href='#Pic" . $i . "' onClick=\"load_image('".$src_big."')\" >".get_pic_epaper($row['picture'],$w," class='img_border' ")."</a></td></tr></table></td>";
			
			$list_src .= '<a href="'.MOD_DIR_UPLOAD.'/'.$row['picture'].'" title="'.$row['pic_name'].'" ><img src="'.$vnT->dir_images.'/blank.gif" /></a> ';
			if ($dem == $n)
      {
        $list_pic .= "</tr><tr>";
        $dem = 0;
      }
       
    }
    $list_pic .= '</tr></table>';		 
   
  }else{
		$list_pic="";
	}	
	
	$out['list_pic'] = $list_pic;
	$out['list_src'] = $list_src;
  return $out;
}
function html_row ($data){
  global $input, $conf, $vnT, $func;
  $eid = (int) $data['e_id'];
	$w = ($vnT->setting['img_width_grid']) ? $vnT->setting['img_width_grid'] : 140;
  $data['link'] = ROOT_URL."modules/epaper/detail.php?eID={$eid}&lang={$vnT->lang_name}";
  $picture = ($data['picture']) ? "epaper/book_image/".$data['picture'] : "epaper/".$vnT->setting['pic_nophoto'];
  $data['src'] = $vnT->func->get_src_modules($picture, $w ,'',1,'0.75:1');
	$data['title'] = $vnT->func->HTML($data['title']);
	$data['link_download'] = $data['link_down'];
  $output .= load_html("table_pro", $data);
  return $output;
}
function html_col ($data){
  global $input, $vnT, $func, $DB;
  $eid = (int) $data['e_id'];
  $link = ROOT_URL."modules/epaper/flash/index.php?eID={$eid}&lang={$vnT->lang_name}" ;
  $data['checkbox'] = "<input name=\"ch_id[]\" id=\"ch_id\" type=\"checkbox\" value='" . $eid . "'  class=\"checkbox\" onClick=\"javascript:select_list('item{$eid}')\"  />";
  
	$w = ($vnT->setting['img_width_list']) ? $vnT->setting['img_width_list'] : 100;
	$pic = ($data['picture']) ?  get_pic_epaper($data['picture'],$w," alt='".$data['title']."' title='".$data['title']."' ") : "<img  src=\"" . MOD_DIR_IMAGE . "/nophoto.gif\"  >";  
 
  $data['pic'] = "<a href=\"javascript:void(0)\" onclick=\"fullScreen('".$link."')\"  title='".$data['title']."'>{$pic}</a>";
  
	$title = $vnT->func->HTML($data['title']);	
	if(isset($input['keyword']))
	{
		$qu_find = array('[#]' , '[/#]');
		$qu_replace = array('<font class="font_keyword">' , '</font>');
		
		$key = trim($input['keyword']);
		$arr_key = array($key,strtolower($key),strtoupper($key));
		$arr_keyRe = array("[#]".$key."[/#]","[#]".strtolower($key)."[/#]","[#]".strtoupper($key)."[/#]");
		$title = str_replace($arr_key,$arr_keyRe,$title); 
		$title = str_replace($qu_find, $qu_replace, $title); 
	} 	
	
  $data['name'] = "<a href=\"javascript:void(0)\" onclick=\"fullScreen('".$link."')\"  title='".$data['title']."' >" . $title . "</a>";
	$data['view_detail'] = "<a href=\"javascript:void(0)\" onclick=\"fullScreen('".$link."')\"  title='".$data['title']."' >View detail</a>";
    
  return load_html("table_col", $data);
}
function row_epaper ($where, $start, $n, $num_row=4, $view=1){
  global $vnT, $input, $DB, $func, $conf;
  $text = "";
  $sql = "SELECT * FROM epaper p, epaper_desc pd
					WHERE p.e_id=pd.e_id AND display=1 AND pd.lang='$vnT->lang_name'
					$where LIMIT $start,$n";
  $result = $vnT->DB->query($sql);
  if ($num = $vnT->DB->num_rows($result)){
    if ($view == 2){
      while ($row = $DB->fetch_row($result)){
        $text .= html_col($row);
      }
    } else {
      while ($row = $DB->fetch_row($result)){
        $text .= html_row($row);
      }
    }
  } else {
    $text = "<div class='noepaper'>{$vnT->lang['epaper']['no_have_epaper']}</div>";
  }
  return $text;
}
function box_sidebar ()
{
	global $vnT, $input;
	$output = '';
	include ("widgets.php");	
	
	$output .= box_search();
	$output .= box_category();

 	return $output;
}

 
 


?>