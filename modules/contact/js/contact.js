$(document).ready(function(){
    $(".view-map-contact a").fancybox({
        width     : '100%',
        height    : '100%',
        margin : 0,
        modal: false,
        padding:20,
        wrapCSS     : 'myPopupDesign',
    });
});