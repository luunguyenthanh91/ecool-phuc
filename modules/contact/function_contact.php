<?php
/*================================================================================*\
|| 							Name code : function_contact.php 		 		 											  # ||
||  				Copyright © 2007 by Thai Son - CMS vnTRUST                					# ||
\*================================================================================*/
/**
 * @version : 1.0
 * @date upgrade : 11/12/2007 by Thai Son
 **/
define("DIR_IMAGE", ROOT_URI . "modules/contact/images");
define("DIR_MOD", ROOT_URI . "modules/contact");
define("LINK_MOD", $vnT->link_root . $vnT->setting['seo_name'][$vnT->lang_name]['contact']);

function getDepartment ($did = -1, $ext = "")
{
  global $func, $DB, $conf, $vnT;
  $text = "<select name=\"staff\" id=\"staff\" class='form-control' {$ext} >";
  $query = $DB->query("select * from contact_staff WHERE display=1  order by staff_order");
  while ($row = $DB->fetch_row($query)) {
    $title = $func->fetch_array($row['title']);
    if ($row['email'] == $did)
      $text .= "<option value=\"{$row['email']}\" selected>" . $title . "</option>";
    else
      $text .= "<option value=\"{$row['email']}\">" . $title . "</option>";
  }
  $text .= "</select>";
  return $text;
}

/**
 * function box_right
 * 
 **/
function box_right ()
{
  global $vnT, $con, $input, $DB, $func;
  $output = '';
  $output = $vnT->block->_print_one_block("support");
  $output .= $vnT->block->_print_one_block("maillist");
  return $output;
}
?>