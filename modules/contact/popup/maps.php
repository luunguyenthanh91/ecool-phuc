<?php
define('IN_vnT', 1);
define('DS', DIRECTORY_SEPARATOR);
require_once ("../../../_config.php");
require_once ($conf['rootpath'] . "includes/class_db.php");
$DB = new DB();
//maps
$id = (int) $_GET['id'];
$w = ($_GET['w']) ? $_GET['w'] : 500;
$h = ($_GET['h']) ? $_GET['h'] : 300;
$query = $DB->query("SELECT * FROM contact_config WHERE id=$id");
if ($row_m = $DB->fetch_row($query)) {
  $data['description'] = str_replace("\r\n", "<br>", $row_m['map_desc']);
  $data['map_lat'] = ($row_m['map_lat']) ? $row_m['map_lat'] : "10.804866895605";
  $data['map_lng'] = ($row_m['map_lng']) ? $row_m['map_lng'] : "106.64199984239";
} else {
  $data['map_lat'] = "10.804866895605";
  $data['map_lng'] = "106.64199984239";
}
//		echo $data['description'] ;
$description = '<div style="width: 300px; padding-right: 5px; font-family: Arial; font-size: 12px;">';
$description .= $data['description'];
$description .= '</div>';
?>
<html>
<head>
<title>GoogleMap</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
 <link rel="stylesheet" href="<?php echo $conf['rooturl']?>js/google/gmaps.css" type="text/css" />
  <script type="text/javascript" src="<?php echo $conf['rooturl']?>js/jquery"></script>
	<script type="text/javascript" src="http://www.google.com/jsapi"></script>
  <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false&language=vi"></script>
  <script type="text/javascript" src="<?php echo $conf['rooturl']?>js/google/gmaps.js"></script>
  
<script type="text/javascript">
jQuery(document).ready( function($) {
	ceMap.lat	= <?php echo $data['map_lat']; ?>;	
	ceMap.lng	= <?php echo $data['map_lng']; ?>;	
	ceMap.zoom	= 16;	
	ceMap.useDirections	= true;	
	ceMap.showCoordinates	= true;	
	ceMap.mapTitle	= '';	
	ceMap.infoWindowDisplay	= 'alwaysOn';	
	ceMap.infoWindowContent	= '<?php echo $description; ?>';	
	ceMap.scrollwheel	= true;	
	ceMap.mapContainer	= 'ce_map_container';	
	ceMap.mapCanvas	= 'ce_map_canvas';	
	ceMap.jsObjName	= 'ceMap';	
	ceMap.markerImage	= false;	
	ceMap.markerShadow	= false;	
	ceMap.companyMarkerDraggable	= false;	
	ceMap.typeControl	= '1';	
	ceMap.typeId	= 'ROADMAP';	
	ceMap.navigationControl	= '1';	
	ceMap.travelMode	= 'DRIVING';	
	ceMap.input.lat	= false;	
	ceMap.input.lng	= false;	
	ceMap.input.zoom	= false;	
	ceMap.input.address	= 'dir_address';	
	ceMap.input.highways	= 'dir_highways';	
	ceMap.input.tolls	= 'dir_tolls';	
	ceMap.lang.showIPBasedLocation	= 'Showing IP-based location';	
	ceMap.lang.directionsFailed	= 'Directions failed';	
	ceMap.lang.geocodeError	= 'Geocode was not successful for the following reason';	
	ceMap.typeId=google.maps.MapTypeId.ROADMAP ;
	ceMap.getMarkerImage(ceMap.markerImage,ceMap.markerShadow);
	ceMap.init();
	
	jQuery("#ce-map-cpanel-container").hide();
	jQuery('.ce-route').click(function() {
		jQuery("#ce-map-cpanel-container").slideToggle(150);
		jQuery('#dir_address').focus();
	});	
	
});
 

</script>


<body onLoad="initialize();" style="margin:0px;" >
 
<div id="ce_map_container">
  <div id="ce_map_canvas"></div>
  <div id="ce-map-coordinates">
    <div class="ce-map-lat"><span class="ce-map-coord-label">Latitude: </span><span class="ce-map-coord-value">10.3978266</span></div>
    <div class="ce-map-lng"><span class="ce-map-coord-label">Longitude: </span><span class="ce-map-coord-value">107.1152133</span></div>
  </div>
  <div id="ce-map-cpanel-switch"><a href="javascript:void(0);" class="ce-route ce-boxed" >Get directions...</a></div>
  <div id="ce-map-cpanel-container">
    <div id="ce-map-cpanel" class="ce-map-cpanel">
      <form action="http://dhe.vn/lien-he.html" onsubmit="return false;">
        <fieldset>
          <legend>Route options</legend>
          <div>
            <label for="address2">From address</label>
            <input type="text" class="inputbox" id="dir_address" name="address" value="" />
          </div>
          <div >
            <label for="highways" class="labelCheckbox">
              <input type="checkbox" class="inputbox" id="dir_highways" name="highways" />
              Avoid highways</label>
          </div>
          <div >
            <label for="tolls" class="labelCheckbox">
              <input type="checkbox" class="inputbox" id="dir_tolls" name="tolls" />
              Avoid tolls</label>
          </div>
          <div class="submit">
            <div>
              <button type="button" class="button" id="ce-map-submit"
								onclick="ceMap.getDirections();"	> Go!</button>
              <button type="reset"  class="button" id="ce-map-submit" 
								onclick="ceMap.reset();"	> Reset</button>
            </div>
          </div>
        </fieldset>
      </form>
    </div>
  </div>
  <div id="ce-directionsPanel"></div>
</div>
</body>
</html>