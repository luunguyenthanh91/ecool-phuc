<?php
define('IN_vnT', 1);
define('DS', DIRECTORY_SEPARATOR);
require_once ("../../../_config.php");
require_once ($conf['rootpath'] . "includes/class_db.php");
$DB = new DB();
require_once ($conf['rootpath'] . "includes/class_functions.php");
$func = new Func_Global();
$conf = $func->fetchDbConfig($conf);
require_once ($conf['rootpath'] . "includes/class_libs.php");
$vnT->lib = new Lib();
//maps
$id = (int) $_GET['id'];
$w = ($_GET['w']) ? $_GET['w'] : '1200';
$h = ($_GET['h']) ? $_GET['h'] : '500';
$query = $DB->query("SELECT * FROM contact_config WHERE id=$id");
if ($row_m = $DB->fetch_row($query)) {
  $data['description'] = str_replace("\r\n", "<br>", $row_m['title']);
  $data['map_lat'] = ($row_m['map_lat']) ? $row_m['map_lat'] : "10.804866895605";
  $data['map_lng'] = ($row_m['map_lng']) ? $row_m['map_lng'] : "106.64199984239";
} else {
  $data['map_lat'] = "10.804866895605";
  $data['map_lng'] = "106.64199984239";
}
//echo $data['description'] ;
$description = '<div style="width: 300px; padding-right: 5px; font-family: Arial; font-size: 12px;">';
$description .= $row_m['map_desc'];
$description .= '</div>';
?>
<html>
<head>
<title>GoogleMap</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<script type="text/javascript" src="http://maps.google.com/maps/api/js?key=<?php echo $conf['GoogleMapsAPIKey']?>&language=vi
"></script>
<script type="text/javascript">
var map;
var infowindow;

function initialize(){
contentString = '<?php
echo $description;
?>';

var defaultLatLng = new google.maps.LatLng(<?php echo $data['map_lat']; ?>, <?php echo $data['map_lng']; ?>);
var myOptions= {zoom: 15,
center: defaultLatLng,
scrollwheel : false,
mapTypeId: google.maps.MapTypeId.ROADMAP};
map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);
map.setCenter(defaultLatLng);

clickmarker = new google.maps.Marker({
position: defaultLatLng,
clickable: true,
cursor: "pointer",
map: map
});

infowindow= new google.maps.InfoWindow({content: contentString});
infowindow.open(map, clickmarker);

google.maps.event.addListener(clickmarker, 'click', function(){
infowindow.open(map, clickmarker);
});
}
</script>
<body onLoad="initialize();" style="margin:0px;" >
<div id="map_canvas" style="width:100%; height:100%"></div>
</body>
</html>