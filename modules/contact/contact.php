<?php
/*================================================================================*\
|| 							Name code : main.php 		 		 																	  # ||
||  				Copyright © 2007 by Thai Son - CMS vnTRUST                					# ||
\*================================================================================*/
/**
 * @version : 1.0
 * @date upgrade : 11/12/2007 by Thai Son
 **/
if (! defined('IN_vnT')) {
  die('Access denied');
}
$nts = new sMain();

class sMain
{
  var $output = "";
  var $skin = "";
  var $linkUrl = "";
  var $module = "contact";

  function sMain (){
    global $vnT, $input;
    include ("function_" . $this->module . ".php");
    $this->skin = new XiTemplate(DIR_MODULE . "/" . $this->module . "/html/" . $this->module . ".tpl");
    $this->skin->assign('DIR_MOD', DIR_MOD);
    $this->skin->assign('LANG', $vnT->lang);
    $this->skin->assign('INPUT', $input);
    $this->skin->assign('CONF', $vnT->conf);
    $this->skin->assign('DIR_JS', $vnT->dir_js);
		$this->skin->assign('DIR_IMAGE', $vnT->dir_images);
    $vnT->html->addStyleSheet(DIR_MOD . "/css/" . $this->module . ".css");
    $vnT->html->addScript(DIR_MOD . "/js/" . $this->module . ".js");
		$vnT->html->addScript($vnT->dir_js . "/jquery_plugins/jquery.validate.js");
		$vnT->setting['menu_active'] = $this->module; 
    $vnT->conf['indextitle'] =  $vnT->lang['contact']['contact'];
    $data['main'] = $this->do_Contact();
    $navation ='<ul itemscope="" itemtype="http://schema.org/BreadcrumbList"><li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem" class="home"><a itemscope="" itemtype="http://schema.org/Thing" itemprop="item" href="'.$vnT->link_root.'"><span itemprop="name"><i class="fa fa-home"></i></span></a></li> <li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem"><a itemscope="" itemtype="http://schema.org/Thing" itemprop="item" href="'.$vnT->link_root.$vnT->setting['seo_name'][$vnT->lang_name]['about'].'.html"><span itemprop="name">'.$vnT->lang['contact']['contact'].'</span></a></li> </ul>';
    $data['fixed_sidebar'] = $vnT->lib->fixed_sidebar();
		$data['navation'] = $vnT->lib->box_navation($navation);
		$vnT->setting['banner'] = $vnT->lib->get_child_slide('child');
    $this->skin->assign("data", $data);
    $this->skin->parse("modules");
    $vnT->output .= $this->skin->text("modules");
  }
  function do_Contact (){
    global $input, $vnT, $conf, $DB, $func;
    $mess = "";
    $ext = "";
		$func->include_libraries('qrcode.qrcode');
		$vnT->qrcode = new QrCodes;
    if ($input['do_submit']){
      $data = $input; 
      if ($vnT->user['sec_code'] == $input['security_code']){
				//reset sec_code
				$vnT->func->get_security_code();
				$staff = ($input["staff"]) ? $input["staff"] :  $vnT->conf['email'] ;
        $row['name'] = $input["name"];
        $row['email'] = $input["email"];
        $row['company'] = $input["company"];
        $row['address'] = $input["address"];
        $row['phone'] = $input["phone"];
        $row['staff'] = $staff;
        $row['file_attach'] = $file_attach;
        $row['subject'] = $input['subject'];
        $row['content'] = $vnT->func->txt_HTML($_POST['content']);
        $row['datesubmit'] = time();
        $ok = $vnT->DB->do_insert("contact", $row);
        if ($ok) {
					$content_email = $vnT->func->load_MailTemp("contact");
					$qu_find = array(
						'{domain}' , 
						'{name}' , 
						'{email}' , 
						'{company}' , 
						'{address}' , 
						'{phone}' , 
						'{content}' , 
						'{date}');
					$qu_replace = array(
						$_SERVER['HTTP_HOST'] , 
						$input['name'] , 
						$input['email'] , 
						$input['company'] , 
						$input['address'] , 
						$input['phone'] , 
						$vnT->func->HTML($_POST["content"]) , 
						date("d/m/Y"));
					$message = str_replace($qu_find, $qu_replace, $content_email);
					$subject = str_replace("{host}",$_SERVER['HTTP_HOST'],$vnT->lang['contact']['subject_email']);
					$sent = $vnT->func->doSendMail($staff, $subject, $message, $input["email"], $file_attach);
          $mess = $vnT->lang['contact']['send_contact_success'];
          if($input['ref'])
          	$url = $vnT->func->base64url_decode($input['ref']);
          else
          	$url = LINK_MOD . ".html";
          $vnT->func->html_redirect($url, $mess);      
        } else {
          $err = $vnT->func->html_err($vnT->lang['contact']['send_contact_failt']);
        }
      } else {
        $err = $vnT->func->html_err($vnT->lang['contact']['security_code_invalid']);
      }
    }
		$info_contact ='';
		$list_tab= '';
		$w_map = 450;
		$h_map = 450;
		$show_map = 0;
    $result= $vnT->DB->query("SELECT * FROM contact_config
    													WHERE display=1 AND lang='$vnT->lang_name'
    													ORDER BY display_order ASC , id DESC ");
   	if ($num = $vnT->DB->num_rows($result)){
     	$i=0;
			$list_tab = '<ul class="list_maps">';
			while($row = $vnT->DB->fetch_row($result)){
				$class = "" ;
				if($row['map_type']!=0) $show_map = 1;
 				if( $i==0 ) {
					$class = " class='active' ";
					switch ($row['map_type']){
						case 1 : 
							$data['maps'] = '<script language=javascript>load_maps('.$row['id'].','.$w_map.','.$h_map.'); </script>';
						break;
						case 2 :
							$data['maps'] = '<div id="Map" class="maps" ><img src="'.$row['map_picture'].'" alt="map_picture" width="'.$w_map.'" /></div>';		
						break;
					}
				}
				$info_contact .= '<div class="info-contact">';
				$qrcode = $vnT->qrcode->GetVcard($row['full_name'],$row['company'],"",$row['phone'],$row['fax'],$row['email'],$row['website'],$row['address']);
				// $picture = ($row['picture']) ? "weblink/".$row['picture'] : "weblink/location.png";
				$info_contact .= '<div class="img hidden-sm hidden-xs">'.$qrcode.'</div>';
				$info_contact .= '<div class="over">';
				if($row['company']){
					$info_contact .= '<div class="name">'.$vnT->func->HTML($row['title']).'</div>';
					$info_contact .= '<ul>';
					$info_contact .= '<li class="fa-home"><span>'.$vnT->lang['contact']['address'].':</span> '.$vnT->func->HTML($row['address']).'</li>';
					if($row['phone']){
						$info_contact .= '<li class="fa-phone"><span>'.$vnT->lang['contact']['phone'].':</span><a href="tel:'.$row['phone'].'">'.$row['phone'].'</a>';
						if($row['hotline'])
							$info_contact .= ' - <a href="tel:'.$row['hotline'].'">'.$row['hotline'].'</a>';
						$info_contact .= '</li>';
					}
					if($row['fax'])
						$info_contact .= '<li class="fa-fax"><span>Fax:</span>'.$row['fax'].'</li>';
					if($row['email'])
						$info_contact .= '<li class="fa-envelope-o"><span>Email:</span><a href="mailto:'.$row['email'].'">'.$row['email'].'</a></li>';
					if($row['work_time'])
						$info_contact .= '<li class="fa-clock-o"><span>'.$vnT->lang['contact']['work_time'].': </span>'.$row['work_time'].'</li>';
					$info_contact .= '</ul>';
					$info_contact .= '<div class="view-map-contact"><a href="'.ROOT_URI.'modules/contact/popup/maps_odl.php?id='.$row['id'].'&lang='.$vnT->lang_name.'" class="fancybox.iframe" rel="nofollow"><span>'.$vnT->lang['contact']['view_map'].'</span></a></div>';
				}
				$info_contact .= '</div><div class="clear"></div></div>';
				$list_tab .= "<li {$class} id='maps".$row['id']."'><a href='javascript:;' onclick=\"load_maps(".$row['id'].",".$w_map.",".$h_map.");\" ><span>".$vnT->func->HTML($row['title'])."</span></a></li>";	
				$i++;
			}
			$list_tab .= '</ul><div class="clear"></div>';
    }
    $data['info_contact'] = $info_contact;
		$data['list_tab'] = ($num>1) ? $list_tab : "";
    //$data['list_department'] = getDepartment($input['staff'],"style='width:100%' ");
 		//if($show_map){
		// 	$data['td_maps'] = $data['list_tab'].' <div id="ext_maps">'.$data['maps'].'</div>';
		// }
		$data["err"] = $err;
		$vnT->user['sec_code'] = $vnT->func->get_security_code();
    $scode = $vnT->func->NDK_encode($vnT->user['sec_code']);
    $data['ver_img'] = ROOT_URL . "includes/sec_image.php?code=$scode&h=40";
    $data['link_action'] = LINK_MOD . ".html";
    $data['note_contact'] = $vnT->func->load_Sitedoc('note_contact');
     $data['f_title'] = $vnT->lang['contact']['contact'];
    $data['box_right'] = $vnT->func->load_SiteDoc("right_contact");
    $this->skin->assign("data", $data);
    $this->skin->parse("html_contact");		 		
		$nd['content'] = $this->skin->text("html_contact");	
 		// $nd['f_title'] = '<h1>'.$vnT->lang['contact']['contact'].'</h1>';
 		$nd['more'] = $vnT->lang['contact']['glogan'];
    return $vnT->skin_box->parse_box("box_middle", $nd);
  }
}
?>