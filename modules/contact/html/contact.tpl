<!-- BEGIN: modules -->
<div class="contact">
      <div class="container">
        <nav class="fs-10 font-weight-light" aria-label="breadcrumb">
          <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="/">{LANG.global.homepage}</a></li>
            <li class="breadcrumb-item active" aria-current="page"><a href="#">{LANG.contact.contact}</a></li>
          </ol>
        </nav>
        {data.main}
      </div>
      <iframe class="map-contact" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d29786.60214010603!2d105.72967043335746!3d21.059667202056662!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x313454e33407ecc7%3A0x6b9d843be64f7f06!2zTWluaCBLaGFpLCBC4bqvYyBU4burIExpw6ptLCBIw6AgTuG7mWksIFZp4buHdCBOYW0!5e0!3m2!1svi!2s!4v1577599643198!5m2!1svi!2s" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
    </div>
<!-- END: modules -->

<!-- BEGIN: html_contact -->

<h1 class="text-center fs-20 fs-lg-55 font-weight-bold mt-5 mb-4">{data.f_title}</h1>
        <div class="row">
          <div class="col-12 company-info text-center">
            
            {data.note_contact}
          </div>
          <div class="col-12 pt-4">
            <p class="text-center">__________________</p>

          </div>
           <div style="text-align: center;margin: auto;"><center>{data.err}</center></div>
        </div>
          <form id="formContact" name="formContact" method="post" action="{data.link_action}" class="form-contact text-center validate">
          <div class="row">
            <div class="col-lg-8 col-12 form_left">
              <input class="form-group border-form-contact form-control " value="{data.name}" id="name" name="name" type="text" placeholder="{LANG.contact.full_name}" required>
              <input class="form-group border-form-contact form-control required"  value="{data.phone}" id="phone" name="phone" type="phone" placeholder="{LANG.contact.phone}" required>
              <input class="form-group border-form-contact form-control" type="email" placeholder="{LANG.contact.email}" required>
              <textarea class="form-group border-textarea-contact form-control" row="28" placeholder="{LANG.contact.mess_empty_email}"></textarea>
              <div class="flexLeft">
                  <div class="colRight">
                    <div class="input-group">
                      <div class="faForm fa-shield">
                        <input type="text" class="form-group border-form-contact form-control" value="{data.security_code}" id="security_code" name="security_code" title="{LANG.contact.mess_empty_security_code}" placeholder="{LANG.contact.security_code} *" required="" />
                      </div>
                      <span class="input-group-img">
                        <img class="security_ver" src="{data.ver_img}" align="absmiddle">
                      </span>
                    </div>
                  </div>
                  <div class="clear"></div>
                </div>
              <button class="btn btn-contact" id="do_submit" name="do_submit" type="submit" value="1">{LANG.contact.btn_send}</button>
            </div>
            {data.box_right}
          </div>
        </form>
<!-- END: html_contact -->