<?php
define('IN_vnT', 1);
define('DS', DIRECTORY_SEPARATOR);
require_once ("../../../_config.php");
require_once ($conf['rootpath'] . "includes/class_db.php");
$DB = new DB();
include ($conf['rootpath'] . "includes/class_functions.php");
$func = new Func_Global();
$conf = $func->fetchDbConfig($conf);
$vnT->lang_name = (isset($_GET['lang'])) ? $_GET['lang'] : "vn"; 
$func->load_language('contact');

$id = (int) $_GET['id']; 
$cat_id = (int) $_GET['cat_id']; 
$city = (int) $_GET['city']; 
$state = (int) $_GET['state']; 
$key = $_GET['key']; 

$w = ($_GET['w']) ? $_GET['w'] : 420;
$h = ($_GET['h']) ? $_GET['h'] : 500;


/*-------------- get_city_name --------------------*/
function get_city_name ($id)
{
	global $func, $DB, $conf, $vnT;
	$text = $id;
	$result = $DB->query("SELECT name FROM iso_cities WHERE id=".$id);
	if ($row = $DB->fetch_row($result)) {
		$text = $func->HTML($row['name']);
	}
	return $text;
}	
/*-------------- get_state_name --------------------*/
function get_state_name ($id)
{
	global $func, $DB, $conf, $vnT;
	$text = $id;
	$result = $DB->query("SELECT name FROM iso_states WHERE id=".$id);
	if ($row = $DB->fetch_row($result)) {
		$text = $func->HTML($row['name']);
	}
	return $text;
}

if($id) {
	$where = " AND n.did=".$id; 	
}else{
	
	$where = '';
	if($city){
		$where  .= " AND city='".$city."'";
	}
	if($state){
		$where  .= " AND state='".$state."'";
	}
	if($catPro){
		$where .= " AND (catPro ='' OR  FIND_IN_SET(".$catPro.", catPro) ) "; 
	}
	if($brand){
		$where .= " AND (brand ='' OR  FIND_IN_SET(".$brand.", brand) ) "; 
	}
	
	if($key){	 
		$where .= " and (title  like '%" . $key . "%'  OR address like '%" . $key . "%' )";	
	}	
}

$map_lat_default = '14.177834';
$map_lng_default = '108.4414900000';
$zoom_default = 5 ;

if($cat_id){
	switch ($cat_id)
	{
		case 1 : //hn
			$map_lat_default = '20.99721';
			$map_lng_default = '105.8930520000';
			$zoom_default = 10 ;	
		break ;		
		case 2 : 
			$map_lat_default = '15.919074';
			$map_lng_default = '107.730102';
			$zoom_default = 8 ;	
		break ;	
		case 3 : 
			$map_lat_default = '10.730757';
			$map_lng_default = '106.7823450000';
			$zoom_default = 10 ;	
		break ;	
			 		
	}	
}
if($city){
	switch ($city)
	{
		case 1 : //hn
			$map_lat_default = '20.99721';
			$map_lng_default = '105.8930520000';
			$zoom_default = 10 ;	
		break ;		
		case 2 : 
			$map_lat_default = '10.730757';
			$map_lng_default = '106.7823450000';
			$zoom_default = 10 ;	
		break ;	
		case 3 : 
			$map_lat_default = '20.847522';
			$map_lng_default = '106.7750360000';
			$zoom_default = 10 ;	
		break ;		
		case 4 :  
			$map_lat_default = '16.067269';
			$map_lng_default = '108.2156400000';
			$zoom_default = 10 ;	
		break ;		
	}	
}
$result = $DB->query("SELECT * FROM dealer n , dealer_desc nd WHERE n.did=nd.did AND lang='$vnT->lang_name' AND display=1 {$where} ORDER BY d_order ASC , date_post DESC ");
if ($num = $DB->num_rows($result)) 
{
	$arr_beaches = '';
	$arr_message = '';
  while($row = $DB->fetch_row($result)) 
	{
		$title = $func->HTML($row['title']);
		$address = $func->HTML($row['full_address']);				
		$link_city = $vnT->conf['rooturl']."san-pham/search/?city=".$row['city'];
		
		$pic = ($row['picture']) ? '<img src='.$conf['rooturl'].'vnt_upload/dealer/'.$row['picture'].' />' : '<img src='.$conf['rooturl'].'modules/dealer/images/nophoto.gif />';
		$map_lat = $row['map_lat'];
		$map_lng = $row['map_lng'];
		
		$info = $row['full_address'];
		if($row['phone'])
			$info .= "<br>".$row['phone'];
		if($row['email'])
			$info .= "<br>".$row['email'];
		if($row['website'])
			$info .= "<br>".$row['website'];
		
		$info .= '<div class=view_all><a href="'.$link_city.'" title="'.$title.'" >Xem các điểm ưu đãi tại '.get_city_name($row['city']).'</a></div>';
							
		$arr_beaches .= "['".$address."', ".$map_lat.",".$map_lng.", 1],"	;
		$arr_message .= "'<div class=dealer_info><div class=img>".$pic."</div><div class=info><h3>".$title."</h3>".$info."</div><div class=clear></div></div>',";
		
	}	 
}  

if($id){
	$map_lat_default = $map_lat ;
	$map_lng_default = $map_lng;	
	$zoom_default = 18;
} 

?>
<style>
	
</style>
<script>
var infowindow;
   function initialize() {
   
   var mapOptions = {
    zoom: <?php echo $zoom_default; ?>,
    center: new google.maps.LatLng(<?php echo $map_lat_default; ?>,<?php echo $map_lng_default; ?>),
    mapTypeId: google.maps.MapTypeId.ROADMAP
  }
  var map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);

  setMarkers(map, beaches, message);
  
}  var beaches = [<?php echo $arr_beaches; ?>] 
 
var message = [<?php echo $arr_message; ?>];
function setMarkers(map, locations,message) {
 
  var image = {
    url: '<?php echo $conf['rooturl']; ?>modules/dealer/images/icon_place.png',  
    size: new google.maps.Size(26, 37),  
    origin: new google.maps.Point(0,0),  
    anchor: new google.maps.Point(0, 32)
  };
  
  var shadow = {
    url: '<?php echo $conf['rooturl']; ?>modules/dealer/images/icon_place.png',  
    size: new google.maps.Size(26, 38),
    origin: new google.maps.Point(0,0),
    anchor: new google.maps.Point(0, 32)
  };
 
  var shape = {
      coord: [1, 1, 1, 20, 18, 20, 18 , 1],
      type: 'poly'
  };
  
   infowindow = new google.maps.InfoWindow();
  
  for (var i = 0; i < locations.length; i++) {
   
    var beach = locations[i];
    var myLatLng = new google.maps.LatLng(beach[1], beach[2]);
    var marker = new google.maps.Marker({
        position: myLatLng,
        map: map, 
        title: beach[0],
        zIndex: beach[3]		
    });
	
	
	infowindow = new google.maps.InfoWindow();
	attachSecretMessage(marker, i,message);

  }
  
}

function attachSecretMessage(marker, num,message) {   
  google.maps.event.addListener(marker, 'click', function() {
   infowindow.setContent(message[num]);

    infowindow.open(marker.get('map'), marker);
  });
} 
 
initialize();
</script>
   
<?php
	$DB->close();
?>