<?php
/*================================================================================*\
|| 							Name code : function_news.php 		 		 												  # ||
||  				Copyright © 2007 by Thai Son - CMS vnTRUST                					# ||
\*================================================================================*/
/**
 * @version : 1.0
 * @date upgrade : 17/12/2007 by Thai Son
 **/
 
 
if (! defined('IN_vnT')) {
  die('Access denied');
}


//-----------------  List_City
function List_City ($country, $did = "", $ext = "")
{
  global $vnT, $conf, $DB, $func;
  $text = "<select name=\"city\" id=\"city\" class='select'  {$ext}   >";
  $text .= "<option value=\"\" selected>" . $vnT->lang['dealer']['select_city'] . "</option>";
  $sql = "SELECT * FROM iso_cities where display=1 and country='$country'  order by c_order ASC , id DESC  ";
  $result = $DB->query($sql);
  while ($row = $DB->fetch_row($result)) {
    if ($row['code'] == $did) {
      $text .= "<option value=\"{$row['code']}\" selected>" . $func->HTML($row['name']) . "</option>";
    } else {
      $text .= "<option value=\"{$row['code']}\">" . $func->HTML($row['name']) . "</option>";
    }
  }
  $text .= "</select>";
  return $text;
}

//-----------------  List_State
function List_State ($city, $did = "", $ext = "")
{
  global $vnT, $conf, $DB, $func;
  $sql = "SELECT * FROM iso_states where display=1 and city='$city'  order by s_order ASC , id DESC ";
  $result = $DB->query($sql);
  if ($num = $DB->num_rows($result)) {
    $text = "<select name=\"state\" id=\"state\" class='select'  {$ext}   >";
    $text .= "<option value=\"\" selected>" . $vnT->lang['dealer']['select_state'] . "</option>";
    while ($row = $DB->fetch_row($result)) {
      if ($row['code'] == $did) {
        $text .= "<option value=\"{$row['code']}\" selected>" . $func->HTML($row['name']) . "</option>";
      } else {
        $text .= "<option value=\"{$row['code']}\">" . $func->HTML($row['name']) . "</option>";
      }
    }
    $text .= "</select>";
  } else {
    $text = '<input type="text"  class="textfiled" value="' . $did . '" name="state" id="state" />';
  }
  return $text;
}

 
//Function - box_search
function box_search ()
{
  global $vnT, $func, $DB, $input;
 	
  $list_city = List_City("VN", $input['city'], "   ");
 // $list_state = List_State($input['city'], $input['state'], "  ");
  $content = '<form action="' . LINK_MOD . '.html" method="post" name="fDealer" id="fDealer">
				<div id="box_search">
				<table width="100%"  border="0" cellspacing="2" cellpadding="2">
				<tr>
					<td > <strong>' . $vnT->lang['dealer']['keyword'] . ' :</strong> <br>
					<input name="key" type="text"  value="' . $input['key'] . '" class="textfiled"  /></td>
					</tr>					
					 <tr>
						<td><strong>' . $vnT->lang['dealer']['city'] . ' :</strong><br>' . $list_city . ' </td>    
					</tr>					  		
					<tr>
					<td align="center" ><button  id="do_submit" name="do_submit" type="submit" class="btn" value="' . $vnT->lang['dealer']['btn_search'] . '" ><span >' . $vnT->lang['dealer']['btn_search'] . '</span></button> </td>
					</tr>
				</table>
				</div>
			</form>';
  $data['f_title'] = $vnT->lang['dealer']['box_search'];
  $data['content'] = $content;
  return $vnT->skin_box->parse_box("box", $data);
}
 

/*-------------- box_category --------------------*/
function box_category ()
{
  global $func, $DB, $conf, $vnT, $input;
  // lay ROOT cat
  $sql = "select *
					from dealer_category  
					where lang='$vnT->lang_name' 
					and parentid=0 
					and display=1
					order by cat_order ASC , cat_id DESC";
  $result = $DB->query($sql);
  if ($num = $DB->num_rows($result)) {
    $cat_cur =   $input["catID"];
    $text = '<div class="box_category"><ul >';
    $i = 0;
    while ($row = $DB->fetch_row($result)) {
      $i ++;
      $cat_id = $row['cat_id'];
      $show_sub = 0; // mac dinh ban dau
      $arr_child = Get_Child($cat_id);
      $arr_child[] = $cat_id;
      $active = "";
      if (in_array($cat_cur, $arr_child)) {
        $show_sub = 1;
        $active = "class='current'";
      } else {
        $active = "";
      }
      $last = ($i == $num) ? " class ='last' " : "";
      $link = create_link("category", $cat_id, $row['friendly_url']);
      $text .= "<li {$last} ><a href='{$link}' {$active}  ><span  >" . $vnT->func->HTML($row['cat_name']) . "</span></a>";
      //check sub
      $sql_sub = "select *
					from dealer_category  
					where lang='$vnT->lang_name' 
					and parentid=$cat_id 
					and display=1
					order by cat_order ASC , cat_id DESC";
      $res_sub = $vnT->DB->query($sql_sub);
      if ($num_sub = $vnT->DB->num_rows($res_sub)) {
        $text_sub = "<ul>";
        while ($row_sub = $vnT->DB->fetch_row($res_sub)) {
          $active_sub = ($cat_cur == $row_sub['cat_id']) ? " class ='current' " : "";
          $link_sub = create_link("category", $row_sub['cat_id'], $row_sub['friendly_url']);
          $text_sub .= "<li ><a href='{$link_sub}'  {$active_sub} > " . $vnT->func->HTML($row_sub['cat_name']) . "</a></li>";
        }
        $text_sub .= "</ul>";
        if ($show_sub) {
          $text .= $text_sub;
        }
      }
      $text .= "</li>";
    }
    $text .= '</ul></div>';
		
    
		$nd['content'] = $text;
    $nd['f_title'] = $vnT->lang['dealer']['box_category'];
    $textout = $vnT->skin_box->parse_box("box", $nd);
    return $textout ;
		
  } else {
    return '';
  }
}

//======== get_catid ==============
function get_catid ($id)
{
  global $DB, $vnT;
  $res = $DB->query("SELECT cat_id FROM dealer WHERE fid ={$id}");
  if ($row = $DB->fetch_row($res)) {
    return (int) $row['cat_id'];
  } else {
    return 0;
  }
}

//========Check Sub===============
function Check_Sub ($cid)
{
  global $DB, $vnT;
  $query = $DB->query("SELECT * FROM dealer_category WHERE parentid ={$cid}");
  if ($scat = $DB->fetch_row($query))
    return 1;
  else
    return 0;
}

//=========Get Child ============
function Get_Child ($cid)
{
  global $DB, $func, $input, $conf, $vnT;
  $sql = "select * from dealer_category where parentid = '{$cid}'";
  $query = $DB->query($sql);
  while ($row = $DB->fetch_row($query)) {
    $text[] = $row['cat_id'];
    $res_sub = $DB->query("select cat_id,parentid from dealer_category where parentid=" . $row['cat_id']);
    while ($row_sub = $DB->fetch_row($res_sub)) {
      $text[] = $row_sub['cat_id'];
    }
  }
  return $text;
} 

?>