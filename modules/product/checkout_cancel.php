<?php
/*================================================================================*\
|| 							Name code : confirmation.php 		 		 													  # ||
||  				Copyright  2007 by Thai Son - CMS vnTRUST                					# ||
\*================================================================================*/
/**
 * @version : 1.0
 * @date upgrade : 11/12/2007 by Thai Son
 **/
if (! defined('IN_vnT')) {
  die('Access denied');
}
$nts = new sMain();

class sMain
{
  var $output = "";
  var $skin = "";

  // Start func
  function sMain ()
  {
    global $vnT, $input, $cart, $session, $ShoppingCart, $conf;
    include ("function_product.php");
    loadSetting();
    include ("function_shopping.php");
		
    // tro ve cat cua san pham 
		$link_ref = ROOT_URL."main.html";
		$res_cat = $vnT->DB->query("SELECT cat_id from products where p_id=".(int)$_SESSION['last_pid']);
		if ($r_cat = $vnT->DB->fetch_row($res_cat))
		{
			$cat_id = (int)$r_cat['cat_id'] ;
			$friendly_url = get_friendly_url ("product_category"," AND cat_id=$cat_id ");			
 			$link_ref = create_link("category",$cat_id,$friendly_url) ;
		} 
		
    // xoa hoa don
    $order_id = trim($vnT->func->NDK_decode($input['order_code']));
    //	echo "order_id = ".$order_id;
    if ($order_id) {
      $ok = $vnT->DB->query("DELETE FROM order_sum WHERE order_code='" . $order_id . "' ");
      if ($ok) {
        $vnT->DB->query("DELETE FROM order_detail WHERE order_code='" . $order_id . "' ");
      }
      // xoa cu
      $vnT->DB->query("DELETE FROM order_shopping  WHERE session='" . $cart->session . "' ");
      $vnT->DB->query("DELETE FROM order_address  WHERE address_id='" . $_SESSION['checkout_address'] . "' ");
      //del session
      $_SESSION['checkout_address'] = "";
      $_SESSION['shipping'] = "";
      $_SESSION['payment'] = "";
      $_SESSION['dis_price'] = "";
      $_SESSION['s_price'] = "";
      $_SESSION['promotion_id'] = "";
      $_SESSION['promotion'] = "";
      $_SESSION['promotion_value'] = "";
      $_SESSION['total_price'] = "";
      $_SESSION['do_finished'] = 1;
    } else {
      @header("Location: {$link_ref}");
      echo "<meta http-equiv='refresh' content='0; url={$link_ref}' />";
    }
    $data['content'] = $vnT->lang['product']['mess_checkout_cancel'];
    $data['back_product'] = "<b><a href='" . $link_ref . "'>[" . $vnT->lang['product']['back_page_product'] . "]</a></b>";
    $nd['content'] = $this->html_main($data);
    $nd['f_title'] = $vnT->lang['product']['f_checkout_cancel'];
    $vnT->output .= $vnT->skin_box->parse_box("box_middle", $nd);
  }

  // end func
  //---------------------------------
  function html_main ($data)
  {
    global $vnT, $input, $cart, $session, $ShoppingCart;
    return <<<EOF
<div id="BoxShopping">
<table width="100%" border="0" cellspacing="0" cellpadding="0">

		<tr>
			<td height=100  >{$data['content']}</td>
		</tr>
	</table>
	<p align="center">{$data['back_product']}</p>
	<br />

</div>
EOF;
  }
  // end class
}
?>