<?php
/*================================================================================*\
|| 							Name code : cart.php 		 		 																	  # ||
||  				Copyright © 2007 by Thai Son - CMS vnTRUST                					# ||
\*================================================================================*/
/**
 * @version : 1.0
 * @date upgrade : 11/12/2007 by Thai Son
 **/
if (! defined('IN_vnT'))
{
  die('Access denied');
}
$nts = new sMain();
class sMain
{
  var $output = "";
  var $skin = "";
  var $linkUrl = "";
	var $module = "product";
	var $action = "checkout_method";

	function sMain(){
		global $vnT,$input,$func,$cart,$DB,$conf;
		include ("function_".$this->module.".php");
		loadSetting();
		include ("function_shopping.php");
		$this->linkMod = ROOT_URL.$vnT->cmd . "=mod:".$this->module;
		$this->skin = new XiTemplate( DIR_MODULE ."/". $this->module . "/html/". $this->action . ".tpl");
    $this->skin->assign('DIR_MOD', DIR_MOD);
		$this->skin->assign('LANG', $vnT->lang);
		$this->skin->assign('INPUT', $input);
		$this->skin->assign('CONF', $vnT->conf);
		$this->skin->assign('DIR_IMAGE', $vnT->dir_images);
		$vnT->html->addScript(DIR_MOD."/js/cart.js");
		$vnT->html->addStyleSheet( DIR_MOD."/css/cart.css");
		//active menu
		$vnT->setting['menu_active'] = $this->module; 
		$vnT->conf['indextitle'] = $vnT->lang['product']['f_info_order'];
		if ($cart->num_items($cart->session)==0)	{			 
		 	$mess = $vnT->lang['product']['empty_cart'];
		 	$url = create_link_shopping("cart");
	   	$vnT->func->html_redirect($url,$mess);
		}
		$res = $vnT->DB->query("SELECT * FROM order_address WHERE session='".$cart->session."' ");
		$info = $vnT->DB->fetch_row($res);
		if(empty($info['address_id'])){
			$link_ref= create_link_shopping("checkout_address");    
			$vnT->func->header_redirect($link_ref);    
		}
		$err="";
		if (isset($_POST['btnConfirm'])){
		 	$cart_total = $cart->cart_total($cart->session);
			$s_price = 0;
			// $res_s = $vnT->DB->query("select * from shipping_method where name='".$input['rShipping']."' ");
			// if($row_s = $vnT->DB->fetch_row($res_s)){
			// 	if($row_s['s_type']==1){
			// 		$res_price = $vnT->DB->query("SELECT s_price FROM shipping_price where country='".$info['c_country']."' and city='".$info['c_city']."' ");
			// 		if($row_price = $vnT->DB->fetch_row($res_price)){
			// 			$s_price = $row_price['s_price'];
			// 		}	
			// 	}else{
			// 		$s_price = $row_s['price'];
			// 	}
			// 	$cot['s_price'] = $s_price;					
			// }
			if(empty($err)){
				$cot['d_name'] = $input['d_name'];
				$cot['d_address'] = $input['d_address'];
				$cot['d_country'] = 'VN';
				$cot['d_city'] = $input['d_city'];
				$cot['d_state'] = $input['d_state'];
				$cot['d_phone'] = $input['d_phone'];
				$cot['d_email'] = $input['d_email'];

				$cot['c_country'] = 'VN';
				$cot['c_name'] = ($input['notSame']) ? $input['c_name'] : $input['d_name'];
				$cot['c_address'] = ($input['notSame']) ? $input['c_address'] : $input['d_address'];
				$cot['c_city'] = ($input['notSame']) ? $input['c_city'] : $input['d_city'];
				$cot['c_state'] = ($input['notSame']) ? $input['c_state'] : $input['d_state'];
				$cot['c_phone'] = ($input['notSame']) ? $input['c_phone'] : $input['d_phone'];

		 		$cot['bill'] = ($input['bill']) ? 1 : 0;
		 		$cot['bill_company'] = ($input['bill']) ? $input['bill_company'] : '';
		 		$cot['bill_address'] = ($input['bill']) ? $input['bill_address'] : '';
		 		$cot['bill_mst'] = ($input['bill']) ? $input['bill_mst'] : '';

		 		$cot['s_price'] = $s_price;
				$cot['shipping_method'] = $input['rShipping'];
				$cot['shipping_name'] = get_method_name ("shipping_method",$input['rShipping']);
				$cot['payment_method'] = $input['rPayment'];
				$cot['payment_name'] = get_method_name ("payment_method",$input['rPayment']);
				$ok = $vnT->DB->do_update("order_address",$cot," session='".$cart->session."' ");
				if($ok){
					$link_ref = create_link_shopping("checkout_confirmation") ;
					$vnT->func->header_redirect($link_ref);
				}else{
					$err = $func->html_err($DB->debug());
				}
			}else{
				$err =  $vnT->func->html_err($err);
			}
		}
		$data['delivery_address'] = $this->delivery_address($info); 
		$data['shipping_method'] = $this->shipping_method($info);
		$data['payment_method'] = $this->payment_method($info);
		$data['info_cart'] = do_ShowCart($info);
		$data['comment'] = $info['comment'];
		$data['nav_shopping']= nav_shopping('checkout_method');
		$data['link_action'] = create_link_shopping("checkout_method");
		$data['link_back'] = create_link_shopping("cart");
		$data['err'] = $err;
		$navation = get_navation (0,$vnT->lang['product']['f_info_order']);
		$data['navation'] = $vnT->lib->box_navation($navation);
    //$vnT->setting['banner'] = $vnT->lib->get_child_slide('child');
    $data['fixed_sidebar'] = $vnT->lib->fixed_sidebar();
		$this->skin->assign("data", $data);		
    $this->skin->parse("modules");
		$vnT->output .= $this->skin->text("modules");
	}
	function delivery_address($info){
		global $vnT,$input,$cart,$conf,$DB,$func;
		$data = $info;
		if(empty($data['d_country'])) $data['d_country'] = "VN";
		if($data['d_country']=='VN'){
			if(empty($data['d_city'])) $data['d_city'] = "2";
			$onChange = " onChange=\"LoadAjax('list_state','&act=d_state&act2=d_ward&city='+this.value+'&lang=".$vnT->lang_name."','ext_d_state')\" ";
			$stateChange = " onChange=\"LoadAjax('list_ward','&act=d_ward&state='+this.value,'ext_d_ward')\"";
			$data['list_d_city'] = List_City_Shopping ("d_city",$data['d_country'],$data['d_city'], $onChange);
			$data['list_d_state'] = List_State_Shopping('d_state', $data['d_city'], $data['d_state'],$stateChange);
			$data['list_d_ward'] = List_Ward_Shopping('d_ward', $data['d_state'], $data['d_ward'] );
		}else{
			$data['list_d_city'] = '<input type="text" class="form-control" value="'.$data['d_city'].'" name="d_city" />';
		}
		if(empty($data['c_country'])) $data['c_country'] = "VN";
		if($data['c_country']=='VN'){
			$onChange = " onChange=\"LoadAjax('list_state','&act=c_state&act2=c_ward&city='+this.value+'&lang=".$vnT->lang_name."','ext_c_state')\" ";
			$stateChange = " onChange=\"LoadAjax('list_ward','&act=c_ward&state='+this.value,'ext_c_ward')\"";
			$data['list_c_city'] = List_City_Shopping ("c_city",$data['c_country'],$data['c_city'],$onChange);
			$data['list_c_state'] = List_State_Shopping('c_state', $data['c_city'], $data['c_state'],$stateChange);
			$data['list_c_ward'] = List_Ward_Shopping('c_ward', $data['c_state'], $data['c_ward'] );
		}else{
			$data['list_c_city'] = '<input type="text" class="form-control" value="'.$data['c_city'].'" name="c_city" />';
		}
		$data['d_email'] = ($data['d_email']) ? $data['d_email'] : $vnT->user['email'];
		$data['d_name'] = ($data['d_name']) ? $data['d_name'] : $vnT->user['full_name'];
		$data['d_phone'] = ($data['d_phone']) ? $data['d_phone'] : $vnT->user['phone'];
		$data['d_address'] = ($data['d_address']) ? $data['d_address'] : $vnT->user['address'];
		$gender = ($data['gender']) ? $data['gender'] : $vnT->user['gender'];
		$data['list_gender'] = List_Gender($gender);
		$this->skin->assign("data", $data);
		$this->skin->parse("delivery_address");
		return $this->skin->text("delivery_address");	 
	}
	function shipping_method($info){
		global $vnT,$input,$cart,$conf,$DB,$func;
		if ($info['shipping_method']){
			$is_shipping = $info['shipping_method'];
		}else{
			$res = $vnT->DB->query("SELECT name FROM shipping_method ORDER BY s_order LIMIT 0,1 ");
			$r_ = $vnT->DB->fetch_row($res);
			$is_shipping = $r_['name'];
		}
		$result = $vnT->DB->query("SELECT * FROM shipping_method WHERE display=1 ORDER BY s_order");
		$i=0;
		while ($row = $vnT->DB->fetch_row($result)){
			$title = $vnT->func->fetch_array($row['title']);
			$description = $vnT->func->fetch_array($row['description']);
			//$price = get_price_pro ($row['price'],0);
			$checked = ($is_shipping==$row['name']) ? 'checked' : '';
			$active = ($is_shipping==$row['name']) ? 'active' : '';
			$id = $row['id'];
			$text.='<div class="boxRadio '.$active.'">
					      <input id="rShipping_'.$id.'" type="radio" name="rShipping" value="'.$row['name'].'" '.$checked.'>
					      <label for="rShipping_'.$id.'">'.$title.'</label>'.$description.'
					    </div>';
			$i++;
		}
		$data['list_shipping_method'] = $text;
		$this->skin->assign("data", $data);
		$this->skin->parse("shipping_method");
		return $this->skin->text("shipping_method");	
	}
	function payment_method($info){
		global $vnT,$input,$cart,$conf,$DB,$func;
		if ($info['payment_method']){
			$is_payment = $info['payment_method'];
		}else{
		 	$res = $vnT->DB->query("SELECT name FROM payment_method ORDER BY p_order LIMIT 0,1 ");
			$r_ = $vnT->DB->fetch_row($res);
			$is_payment = $r_['name'];
		}
		$result = $vnT->DB->query("SELECT * FROM payment_method WHERE display=1 ORDER BY p_order");
		$i=0;
		while ($row = $vnT->DB->fetch_row($result)){
			$title = $vnT->func->fetch_array($row['title']);
			$description = $func->fetch_array($row['description']);
			$checked = ($is_payment==$row['name']) ? 'checked' : '';
			$active = ($is_payment==$row['name']) ? 'active' : '';
			$id = $row['id'];
			$text.='<div class="boxRadio '.$active.'">
					      <input id="rPayment_'.$id.'" type="radio" name="rPayment" '.$checked.' value="'.$row['name'].'" '.$checked.'>
					      <label for="rPayment_'.$id.'">'.$title.'</label>'.$description.'
					    </div>';
			$i++;
		}
		$data['list_payment_method'] = $text;
		$this->skin->assign("data", $data);
		$this->skin->parse("payment_method");
		return $this->skin->text("payment_method");
	}
}
?>