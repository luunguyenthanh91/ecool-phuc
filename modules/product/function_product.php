<?php
/*================================================================================*\
|| 							Name code : function_product.php 		 		 											  # ||
||  				Copyright © 2007 by Thai Son - CMS vnTRUST                					# ||
\*================================================================================*/
/**
* @version : 1.0
* @date upgrade : 17/12/2007 by Thai Son
**/
 
if ( !defined('IN_vnT') )	{ die('Access denied');	}

define("DIR_MOD", ROOT_URI . "modules/product");
define('MOD_DIR_UPLOAD', ROOT_URI . 'vnt_upload/product');
define("MOD_DIR_IMAGE", ROOT_URI . "modules/product/images");
define("LINK_MOD", $vnT->link_root . $vnT->setting['seo_name'][$vnT->lang_name]['product']);
 
/*-------------- create_link --------------------*/
function create_link ($act,$id,$title,$extra=""){
	global $vnT,$func,$DB,$conf;
	switch ($act){
		case "category" : $text = $vnT->link_root.$title.".html";	break;
		case "detail" : $text = $vnT->link_root.$title.".html";	break;
		default : $text = LINK_MOD."/".$act."/".$id."/".$func->make_url($title).".html"; break;
	}
	$text .= ($extra) ? "/".$extra : "";
	return $text;
}
function loadSetting (){
	global $vnT,$func,$DB,$conf;
	$setting = array();
	$result = $DB->query("SELECT * FROM product_setting WHERE lang='$vnT->lang_name' ");
	$setting = $DB->fetch_row($result);
	foreach ($setting as $k => $v)	{
		$vnT->setting[$k] = stripslashes($v);
	}
	$res_s = $DB->query("SELECT status_id, picture FROM product_status  WHERE display = 1 ");
  while ($row_s = $DB->fetch_row($res_s)) {
    $vnT->setting['pic_status'][$row_s['status_id']] = ($row_s['picture']) ? "<img src=\"".MOD_DIR_UPLOAD."/".$row_s['picture']."\"/>" : "";
    //$vnT->setting['src_status'][$row_s['status_id']] = MOD_DIR_UPLOAD
  }
  $res_s= $DB->query("SELECT p.cat_id,cat_code,list_price,cat_name
											FROM product_category p, product_category_desc pd
											WHERE p.cat_id = pd.cat_id AND lang='$vnT->lang_name' AND display = 1 ");
  while ($row_s = $DB->fetch_row($res_s)){
  	$vnT->setting['cat_name'][$row_s['cat_id']] = $vnT->func->HTML($row_s['cat_name']);
    $vnT->setting['cat_code'][$row_s['cat_id']] = $row_s['cat_code'];
    $arr = explode('_', $row_s['cat_code']);
    $vnT->setting['cat_root'][$row_s['cat_id']] = $arr[0];
    $vnT->setting['cat_list_price'][$row_s['cat_id']] = $row_s['list_price'];
  }
  unset($row_s);
	unset($setting);
}
function load_html ($file, $data){
  global $vnT, $input;
  $html = new XiTemplate( DIR_MODULE . "/product/html/" . $file . ".tpl");
  $html->assign('DIR_MOD', DIR_MOD);
  $html->assign('LANG', $vnT->lang);
  $html->assign('INPUT', $input);
  $html->assign('CONF', $vnT->conf);
  $html->assign('DIR_IMAGE', $vnT->dir_images);
  $html->assign("data", $data);
  $html->parse($file);
  return $html->text($file);
}
function get_parentid ($cat_id){
	global $func,$DB,$conf;
	$cid = list_parentid ($cat_id);
	$cid = substr ($cid,strrpos($cid,",")+1);
	if (empty($cid)) $cid =$cat_id;
	return $cid;	
}
function list_parentid ($cat_id){
	global $func,$DB,$conf;
	$res = $DB->query("SELECT parentid,cat_id FROM product_category where cat_id in (".$cat_id.") and parentid>0  ");
	while ($cat=$DB->fetch_row($res)) {
		$output .=",".$cat["parentid"];
		$output .=list_parentid($cat['parentid']);
	}
	return $output;	
}
function get_cat_end ($cat_id){
	global $func,$DB,$conf;
	if (strstr($cat_id,",")){
		$out = substr($cat_id,strrpos($cat_id,",")+1);
	}else $out = $cat_id;
	
	return $out;
}
function get_where_cat ($cat_id){
  global $input, $conf, $vnT;	
  $text = ""; 	
 	$text .= " AND FIND_IN_SET('$cat_id',cat_list)<>0 ";
  return $text;
}
function get_cat_name ($cat_id){
  global $func, $DB, $conf, $vnT;
  $out = $vnT->lang['product']['category'];
  $sql = "SELECT cat_name FROM product_category_desc WHERE cat_id={$cat_id} and lang='$vnT->lang_name'";
  $result = $DB->query($sql);
  if ($row = $DB->fetch_row($result)){
    $out = $func->HTML($row['cat_name']);
  } 
  return $out;
}
function get_friendly_url ($table,$where=""){
  global $func, $DB, $conf, $vnT;
  $out = "";
  $sql = "SELECT friendly_url FROM {$table}_desc WHERE lang='$vnT->lang_name' {$where} ";
  $result = $DB->query($sql);
  if ($row = $DB->fetch_row($result)) {
    $out = $row['friendly_url'];
  }
  return $out;
}
function get_navation ($cat_id,$ext=''){
  global $DB,$conf,$func,$vnT,$input;
  $output= '<li class="breadcrumb-item" itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem" class="home"><a href="'.$vnT->link_root.'" itemscope="" itemtype="http://schema.org/Thing" itemprop="item"><span itemprop="name">'.$vnT->lang['global']['homepage'].'</span></a></li> ';
	$output.= '<li class="breadcrumb-item" itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem" ><a itemscope="" itemtype="http://schema.org/Thing" itemprop="item" href="'.LINK_MOD.'.html"><span itemprop="name">'.$vnT->lang['product']['product'].'</span></a></li>';
	if($cat_id){
		$cat_code = $vnT->setting['cat_code'][$cat_id];
		$tmp = explode("_",$cat_code);
		for ($i=0;$i<count($tmp);$i++){
			$res= $DB->query("SELECT cat_name,friendly_url FROM product_category_desc 
												WHERE cat_id=".$tmp[$i]." and lang='{$vnT->lang_name}' ");
			if ($r = $DB->fetch_row($res)){		
				$link = create_link("category",$tmp[$i],$r['friendly_url']);	
				$output.='<li class="breadcrumb-item" itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem"><a itemscope="" itemtype="http://schema.org/Thing" itemprop="item" href="'.$link.'"><span itemprop="name">'.$func->HTML($r['cat_name']).'</span></a></li>';
			}
		}
	}
	if($ext)
		$output .= '<li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem"><span itemprop="name">'.$ext.'</span></li>';
	return $output;
}
function get_pic_thumb ($picture, $w=100, $ext="" , $is_email=0){
  global $vnT;
  $out = "";
	$w_thumb = ($vnT->setting['img_width_grid']) ? $vnT->setting['img_width_grid'] : 100 ;
	$link_url = ($is_email) ? ROOT_URL : ROOT_URI ;	
	$linkhinh = "vnt_upload/product/".$picture;
	$linkhinh = str_replace("//","/",$linkhinh);
	$dir = substr($linkhinh,0,strrpos($linkhinh,"/"));
	$pic_name = substr($linkhinh,strrpos($linkhinh,"/")+1) ;
	$src =  $link_url.$dir."/thumbs/".$pic_name;		
	if($w){
		$ext .= " width=\"{$w}\"  ";
	}	
  $out = "<img  src=\"{$src}\" {$ext} >";
  return $out;
}
function get_src_pic_product ($picture, $w = ""){
	global $vnT,$func;	
  $out = "";	
	$w_thumb = ($vnT->setting['img_width_grid']) ? $vnT->setting['img_width_grid'] : 100 ;	
  if ($w){
		$linkhinh = "vnt_upload/product/".$picture;
 		$linkhinh = str_replace("//","/",$linkhinh);
		$dir = substr($linkhinh,0,strrpos($linkhinh,"/"));
		$pic_name = substr($linkhinh,strrpos($linkhinh,"/")+1) ;
		$file_thumbs = $dir."/thumbs/{$w}_".substr($linkhinh,strrpos($linkhinh,"/")+1);
		$linkhinhthumbs = $vnT->conf['rootpath'].$file_thumbs;
		if (!file_exists($linkhinhthumbs)) {
			if (@is_dir($vnT->conf['rootpath'].$dir."/thumbs")) {
				@chmod($vnT->conf['rootpath'].$dir."/thumbs",0777);
			} else {
				@mkdir($vnT->conf['rootpath'].$dir."/thumbs",0777);
				@chmod($vnT->conf['rootpath'].$dir."/thumbs",0777);
			}		
			// thum hinh
			$vnT->func->thum($vnT->conf['rootpath'].$linkhinh, $linkhinhthumbs, $w);
		} 
		$src = ROOT_URI . $file_thumbs;
  } else {
    $src = MOD_DIR_UPLOAD . "/" . $picture;    
  }
  return $src;
}
function get_pic_product ($picture, $w = "", $ext="",$thumb=0){
	global $vnT,$func;	
  $out = "";	
	
	$w_thumb = ($vnT->setting['img_width_grid']) ? $vnT->setting['img_width_grid'] : 100 ;	
	$linkhinh = "vnt_upload/product/".$picture;
	$linkhinh = str_replace("//","/",$linkhinh);
	$dir = substr($linkhinh,0,strrpos($linkhinh,"/"));
	$pic_name = substr($linkhinh,strrpos($linkhinh,"/")+1) ;
		
  if ($w)  
	{		 
		if($thumb){
			$file_thumbs = $dir . "/thumbs/{$w}_" . substr($linkhinh, strrpos($linkhinh, "/") + 1);
			$linkhinhthumbs = $vnT->conf['rootpath'] . $file_thumbs;
			if (! file_exists($linkhinhthumbs)) {
				if (@is_dir($vnT->conf['rootpath'] . $dir . "/thumbs")) {
					@chmod($vnT->conf['rootpath'] . $dir . "/thumbs", 0777);
				} else {
					@mkdir($vnT->conf['rootpath'] . $dir . "/thumbs", 0777);
					@chmod($vnT->conf['rootpath'] . $dir . "/thumbs", 0777);
				}
				// thum hinh
				$vnT->func->thum($vnT->conf['rootpath'] . $linkhinh, $linkhinhthumbs, $w);
			}
			$src = ROOT_URI . $file_thumbs;
		}else{
			$src = ROOT_URI . $dir ."/thumbs/".$pic_name;	
		}
 		    
  } else {
    $src = MOD_DIR_UPLOAD . "/" . $picture;    
  }
	
	$alt = substr($pic_name, 0, strrpos($pic_name, "."));
  $out = "<img  src=\"{$src}\"  {$ext} >";
  return $out;
}
function Get_Cat($did=-1,$ext=""){
	global $func,$DB,$conf,$vnT;
	$text= "<select size=1  name=\"cat_id\"  class=\"select\" $ext >";
	$text.="<option value=\"0\">-- Tất cả danh mục --</option>";
	$query = $DB->query("select c.cat_id,cd.cat_name,cd.description 
					from product_category c , product_category_desc cd
					where c.cat_id=cd.cat_id
					and cd.lang='$vnT->lang_name' 
					and c.parentid=0 
					order by cat_order DESC ,date_post DESC");
	while ($cat=$DB->fetch_row($query)) {
		$cat_name = $vnT->func->HTML($cat['cat_name']);
		if ($cat['cat_id']==$did)
			$text.="<option value=\"{$cat['cat_id']}\" selected>{$cat_name}</option>";
		else
			$text.="<option value=\"{$cat['cat_id']}\" >{$cat_name}</option>";
		$n=1;
		$text.=Get_Sub($cat['cat_id'],$n,$did);
	}
	$text.="</select>";
	return $text;
}
function Get_Sub($cid,$n,$did=-1){
	global $vnT,$func,$DB,$conf;
	$output="";
	$k=$n;
	$query = $DB->query("select c.cat_id,cd.cat_name,cd.description 
					from product_category c , product_category_desc cd
					where c.cat_id=cd.cat_id
					and cd.lang='$vnT->lang_name' 
					and c.parentid={$cid}
					order by cat_order DESC , date_post DESC ");
	while ($cat=$DB->fetch_row($query)) {
	$cat_name = $vnT->func->HTML($cat['cat_name']);
		if ($cat['cat_id']==$did)
		{
			$output.="<option value=\"{$cat['cat_id']}\" selected>";
			for ($i=0;$i<$k;$i++) $output.= "--";
			$output.=" {$cat_name}</option>";
		}
		else
		{	
			$output.="<option value=\"{$cat['cat_id']}\" >";
			for ($i=0;$i<$k;$i++) $output.= "--";
			$output.=" {$cat_name}</option>";
		}
		$n=$k+1;
		$output.=Get_Sub($cat['cat_id'],$n,$did);
	}
	return $output;
}
function List_Search($did){
	global $func,$DB,$conf,$vnT;
	$text= "<select size=1 name=\"search\" >";
	if ($did =="maso")
		$text.="<option value=\"maso\" selected> {$vnT->lang['product']['code_product']} </option>";
	else
		$text.="<option value=\"maso\"> {$vnT->lang['product']['code_product']} </option>";
	
	if ($did =="name")
		$text.="<option value=\"name\" selected> {$vnT->lang['product']['p_name']} </option>";
	else
		$text.="<option value=\"name\"> {$vnT->lang['product']['p_name']} </option>";
	$text.="</select>";
	return $text;
}
function get_price_product ($price,$unit ='đ',$default=""){
	global $func,$DB,$conf,$vnT;
	if ($price){
		$price = $func->format_number($price).$unit;
	}else{
		$price = ($default) ? $default : $vnT->lang['product']['price_default'];
	}
	return $price;
}
function get_option ($info){
	global $vnT,$func,$DB;

	//option
	if ($info['options']){
		
		$textout.='';
		$arr_op = unserialize($info['options']);
		
		$res_op = $DB->query("select * from product_option n, product_option_desc nd  
												where  n.op_id=nd.op_id
												AND nd.lang='$vnT->lang_name'
												AND n.display=1  
												order by n.op_order DESC ,  n.op_id DESC");
		while ($r_op = $DB->fetch_row($res_op)){
			if($arr_op[$r_op['op_id']]){
				$textout .='	&#8226; '.$func->HTML($r_op['op_name']).' : <span class="color">'.$arr_op[$r_op['op_id']].'</span><br>';
			}
		}
	}
	return $textout;
}
function get_imgvote ($votes){
  global $func, $DB, $conf;
  $voteimg = "";
  $votenum = floor($votes);
  $votemod = $votes - $votenum;
  for ($n = 0; $n < $votenum; $n ++)
    $voteimg .= "<img src=\"" . MOD_DIR_IMAGE . "/star2.gif\" />";
  if ($votemod > 0.3){
    if ($votemod > 0.7)
      $voteimg .= "<img src=\"" . MOD_DIR_IMAGE . "/star2.gif\" />";
    else
      $voteimg .= "<img src=\"" . MOD_DIR_IMAGE . "/star1.gif\"  />";
    for ($n = 0; $n < (4 - $votenum); $n ++)
      $voteimg .= "<img src=\"" . MOD_DIR_IMAGE . "/star0.gif\"  />";
  } else {
    for ($n = 0; $n < (5 - $votenum); $n ++)
      $voteimg .= "<img src=\"" . MOD_DIR_IMAGE . "/star0.gif\"  />";
  }
  return $voteimg;
}
function get_p_name ($id){
	global $func,$DB,$conf,$vnT;
	$p_name="";
	$result = $DB->query("SELECT p_name FROM products_desc WHERE lang='$vnT->lang_name' AND p_id=$id ");
	if ($row= $DB->fetch_row($result)){
		$p_name = $func->HTML($row['p_name']);
	}
	return $p_name ;
}
function List_SubCat ($cat_id){
  global $func, $DB, $vnT;
  $output = "";
  $query = $DB->query("SELECT * FROM product_category WHERE parentid={$cat_id}  ");
  while ($cat = $DB->fetch_row($query)){
    $output .= $cat["cat_id"] . ",";
    $output .= List_SubCat($cat['cat_id']);
  }
  return $output;
}
function get_banner_product ($cat_id, $n = ""){
  global $DB, $input, $vnT;
  $html_advertise = "";
  $sql = "select * from product_advertise where cat_id=0 or cat_id=$cat_id order by displayorder, id DESC ";
  if ($n) $sql .= " LIMIT 0,$n ";
  $result = $DB->query($sql);
  while ($row = $DB->fetch_row($result)){
		$title = $vnT->func->HTML($row['title']);
    $src = MOD_DIR_UPLOAD . "/advertise/" . $row['picture'];
    $l_link = (! strstr($row['link'], "http://")) ? $vnT->link_root .$row['link'] : $row['link'] ;
    if ($row["type"] == "swf"){
      $html_advertise .= "<p class='advertise'><object classid='clsid:D27CDB6E-AE6D-11cf-96B8-444553540000' codebase='http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=7,0,19,0' width=" . $row["width"] . " height=" . $row["height"] . "><param name='movie' value=" . $src . " /><param name='quality' value='high' /><param value='transparent' name='wmode'/><embed src=" . $src . " quality='high' pluginspage='http://www.macromedia.com/go/getflashplayer' type='application/x-shockwave-flash' width=" . $row["width"] . " height=" . $row["height"] . " wmode='transparent' ></embed></object><p>";
    } else {
			$target = ($row['target']) ? $row['target'] : "_blank";
      $html_advertise .= "<p class='advertise'><a onmousedown=\"return rwt(this,'advertise',".$row['l_id'].")\" href='{$l_link}'   target='{$target}'> <img  src='{$src}' width='" . $row['width'] . "' alt='{$title}' /></a></p>";
    }
  }
  return $html_advertise;
}
function get_pic_other ($info, $w = 50,$n=4){
  global $func, $DB, $conf, $vnT;
  $out = array();
	$p_id = (int)$info['p_id'];
	$pic_w = ($vnT->setting['imgdetail_width']) ? $vnT->setting['imgdetail_width'] : 250;
	$result = $DB->query("Select * from product_picture where p_id=$p_id");
  if ($num = $DB->num_rows($result)){
		$list_pic = '<table border="0" cellspacing="2" cellpadding="2"><tr>'; 		
		$src = ($info['picture']) ? MOD_DIR_UPLOAD.'/'.$info['picture'] : MOD_DIR_UPLOAD . "/".$vnT->setting['pic_nophoto'] ;
		$src_big = get_src_pic_product($info['picture'],$pic_w);		
		$list_pic .= "<td align='center' class='other_pic'><table width='{$w}' border=0 cellspacing=0 cellpadding=0 align=center  ><tr><td height='{$w}' class='img' ><a href='#Pic0' onClick=\"vnTProduct.load_image('".$src_big."','".$src."')\" >".get_pic_product($info['picture'],$w,"",1 )."</a></td></tr></table></td>";
    $i = 0;
 		$dem = 1;
		$list_src .= '';
    while ($row = $DB->fetch_row($result)){
  		$i ++;    
			$dem ++;  
			$src = ($row['picture']) ? MOD_DIR_UPLOAD.'/'.$row['picture'] : MOD_DIR_UPLOAD . "/".$vnT->setting['pic_nophoto']  ;
      $src_big = get_src_pic_product($row['picture'],$pic_w);
      $list_pic .= "<td align='center' class='other_pic'><table width='{$w}' border=0 cellspacing=0 cellpadding=0><tr><td height='{$w}' align=center class='img' ><a  href='#Pic" . $i . "' onClick=\"vnTProduct.load_image('".$src_big."','".$src."')\" >".get_pic_product($row['picture'],$w,"",1 )."</a></td></tr></table></td>";
			
			$list_src .= '<a href="'.MOD_DIR_UPLOAD.'/'.$row['picture'].'" title="'.$row['pic_name'].'" ><img src="'.$vnT->dir_images.'/blank.gif" /></a> ';
			if ($dem == $n){
        $list_pic .= "</tr><tr>";
        $dem = 0;
      }
    }
    $list_pic .= '</tr></table>';		 
   
  }else{
		$list_pic="";
	}
	$out['list_pic'] = $list_pic;
	$out['list_src'] = $list_src;
  return $out;
}
function get_tooltip ($data){
  global $func, $DB, $vnT, $conf;
	// cache
 	$param_cache = array($data['p_id']);
	$cache = $vnT->Cache->read_cache("pro_tooltip",$param_cache);
	if ($cache != _NOC) return $cache;
  $output = "";	
  $output .= '<div class=fTooltip>' . $vnT->func->HTML($data['p_name']) . ' </div>';	
	$output.='<table width=100%  border=0 cellspacing=0 cellpadding=0>';
	$output.='<tr >
            <td  class=tInfo1 nowrap width=50  >'.$vnT->lang['product']['maso'].' </td>
            <td width=10  class=tInfo1 align=center> : </td>
            <td class=tInfo2 ><b >'.$data['maso'].'</b></td>
        </tr>';
	$output.='<tr >
            <td valign="top" class=tInfo1 nowrap  >'.$vnT->lang['product']['price'].' </td>
            <td width=10 valign="top" class=tInfo1 align=center> : </td>
            <td class=tInfo2 ><b class=tPrice>'.get_price_product($data['price']).'</b></td>
        </tr>';
	if ($data['options']){
		$arr_op = unserialize($data['options']);		 
		$res_op = $DB->query("select * from product_option n, product_option_desc nd  
												where  n.op_id=nd.op_id
												AND nd.lang='$vnT->lang_name'
												AND n.display=1  
												AND n.focus=1
												order by n.op_order DESC ,  n.op_id DESC");
		while ($r_op = $DB->fetch_row($res_op)){			 
			if($arr_op[$r_op['op_id']]){
				$output .='	<tr>
					<td class=tInfo1  nowrap >'.$func->HTML($r_op['op_name']).' </td>
					<td width=10 valign="top" class=tInfo1 align=center> : </td>
					<td class=tInfo2> <span class="color">'.$arr_op[$r_op['op_id']].'</span></td>
				</tr>';
			}
		}
	}	
	$output.="</table>";	
	$output .= '<table width=100% border=0 cellspacing=2 cellpadding=2><tr><td >';  
	if($data['desc_status']) {
		$output .= '<h3 class=th3 >'.$vnT->lang['product']['f_promotion'].'</h3>'; 
  	$output .= '<div align=justify >'.$vnT->func->HTML($data['desc_status']) . '</div>';
	}
	$output .='</tr></table>';
	// cache
	$vnT->Cache->save_cache("pro_tooltip", $output, $param_cache);
  return $output;
}
function box_status($list_status){
	global $vnT, $DB;
	$text = '';
	$arr = explode(',', $list_status);
	if(is_array($arr)){
		foreach ($arr as $key => $value) {
			$text .= '<div class="rib">'.$vnT->setting['pic_status'][$value].'</div>';
		}
	}
	return $text;
}
function html_row ($row){
  global $input, $conf, $vnT, $func;
  $pID = (int) $row['p_id'];
	$w = ($vnT->setting['img_width_grid']) ? $vnT->setting['img_width_grid'] : 240;
  $data['link'] = create_link("detail",$pID,$row['friendly_url']);
	$picture = ($row['picture']) ? "product/".$row['picture'] : "product/".$vnT->setting['pic_nophoto'];
  //$data['src'] = $vnT->func->get_src_modules($picture, $w ,'',1,'1:1');
  $data['pic'] = $vnT->func->get_pic_modules($picture, $w, 0, " alt='".$row['p_name']."' title='".$row['p_name']."' ",1,0,array("fix_width"=>1)); 
	$data['p_name'] = $vnT->func->HTML($row['p_name']);
	$data['price'] = $row['price'];
	$data['short'] = $row['short'];
	$data['price_text'] = get_price_product($row['price']);
	$data['price_old_text'] = $data['discount'] = '';
	if($row['price_old'] && $row['price_old'] > $row['price']){
		$data['price_old_text'] = '<div class="nor">'.get_price_product($row['price_old']).'</div>';
		$dis = $row['price_old'] - $row['price'];
		$discount = ROUND(($dis * 100)/$row['price_old']);
		// $data['discount'] = ($discount > 1) ? '<div class="rib pro">-'.$discount.'%</div>' : '';
	}
	$data['maso'] = $row['maso'];
	$data['box_status'] = box_status($row['status']);
	$data['out_stock'] = ($row['is_parent']==0 && $row['onhand'] < 1) ? '<div class="hethang">'.$vnT->lang['product']['out_stock'].'</div>' : '';
  $output = load_html("table_pro", $data);
  return $output;
}
function html_col ($data){
  global $input, $vnT, $func, $DB;
  $pID = (int) $data['p_id'];
  $link = create_link("detail",$pID,$data['friendly_url']);
  $data['checkbox'] = "<input name=\"ch_id[]\" id=\"ch_id\" type=\"checkbox\" value='" . $pID . "'  class=\"checkbox\" onClick=\"javascript:select_list('item{$pID}')\"  />";
	$w = ($vnT->setting['img_width_list']) ? $vnT->setting['img_width_list'] : 100;
	$picture = ($data['picture']) ? "product/".$data['picture'] : "product/".$vnT->setting['pic_nophoto'];
	$pic = $vnT->func->get_pic_modules($picture, $w, 0, " alt='".$data['p_name']."' title='".$data['p_name']."' "); 
  $data['pic'] = "<a href=\"{$link}\" title='".$data['p_name']."'>{$pic}</a>";
	$p_name = $vnT->func->HTML($data['p_name']);	
	if(isset($input['keyword'])){
		$qu_find = array('[#]' , '[/#]');
		$qu_replace = array('<font class="font_keyword">' , '</font>');
		$key = trim($input['keyword']);
		$arr_key = array($key,strtolower($key),strtoupper($key));
		$arr_keyRe = array("[#]".$key."[/#]","[#]".strtolower($key)."[/#]","[#]".strtoupper($key)."[/#]");
		$p_name = str_replace($arr_key,$arr_keyRe,$p_name); 
		$p_name = str_replace($qu_find, $qu_replace, $p_name); 
	}
  $data['name'] = "<a href=\"{$link}\" title='".$data['p_name']."' >" . $p_name . "</a>";
  $status_pro = ($vnT->setting['pic_status'][$data['status']]) ? $vnT->setting['pic_status'][$data['status']] : "";
  $data['status_pro'] = $status_pro;
  $data['price'] = $vnT->lang['product']['price'] . " : <span class='price'>" . get_price_product($data['price']) . "</span>";
	if ($data['price_old']){
			$data['price'].= "<br><span class='price_old'>".get_price_product($data['price_old'])."</span>";
	}
  $data['vote'] = get_imgvote($data['votes']);
  $data['views'] = $vnT->lang['product']['views'] . ': <span class="font_err">' . $data['views'] . '</span>';
  
  $link_cart = LINK_MOD."/cart.html/?do=add&pID=" . $pID;
  $data['add_cart'] = "<a href=\"{$link_cart}\"><img src=\"" . MOD_DIR_IMAGE . "/btn_addcart.gif\"  align=\"absmiddle\"/></a>";
  $data['view_more'] = "<a href='{$link}'>" . $vnT->lang['product']['view_more'] . "&nbsp;<img src=\"" . MOD_DIR_IMAGE . "/icon_readmore.gif\" align='absmiddle' /></a>";
  return load_html("table_col", $data);
}
function row_product ($where, $start, $n, $num_row=4, $view=1){
  global $vnT, $input, $DB, $func, $conf;
  $text = "";
  $sql = "SELECT * FROM products p, products_desc pd
					WHERE p.p_id=pd.p_id AND display=1 AND lang='$vnT->lang_name'
					$where LIMIT $start,$n";
  $result = $vnT->DB->query($sql);
  if ($num = $vnT->DB->num_rows($result)){
    if ($view == 2){
      $i = 1;
      while ($row = $DB->fetch_row($result)){
        $text .= html_col($row);
        $i ++;
      }
    } else {
			while ($row = $DB->fetch_row($result)){
				$text .= html_row($row);
				$i ++;
			}
    }
  } else {
    $text = "<div class='noItem'>{$vnT->lang['product']['no_have_product']}</div>";
  }
  return $text;
}
function scroll_product ($scroll_name, $where,$start=0,$n=10){
	global $DB, $func, $input, $vnT;
	//$param_cache = array($scroll_name,$input['pID']);
	//$cache = $vnT->Cache->read_cache("scroll_product",$param_cache);
	//if ($cache != _NOC) return $cache;
	$sql = "SELECT * FROM products p, products_desc pd
					WHERE p.p_id=pd.p_id AND display=1 AND pd.lang='$vnT->lang_name' $where LIMIT $start,$n";
	$result = $vnT->DB->query($sql);
	if ($num = $vnT->DB->num_rows($result)){
		$i = 1;
		while ($row = $vnT->DB->fetch_row($result)){
			$pID = (int) $row['p_id'];
			$w = ($vnT->setting['img_width_grid']) ? $vnT->setting['img_width_grid'] : 240;
		  $data['link'] = create_link("detail",$pID,$row['friendly_url']);
			$picture = ($row['picture']) ? "product/".$row['picture'] : "product/".$vnT->setting['pic_nophoto'];
		  $data['src'] = $vnT->func->get_src_modules($picture, $w ,'',1,'1:1');
			$data['p_name'] = $vnT->func->HTML($row['p_name']);
			$data['price'] = $row['price'];

			$data['price_text'] = get_price_product($row['price']);
			$data['price_old_text'] = '';
			if($row['price_old']){
				$data['price_old_text'] = '<div class="nor">'.get_price_product($row['price_old']).'</div>';
			}
			$data['link_cart'] = LINK_MOD.'/cart.html/?do=add&pID='.$pID;
			$text .= load_html("table_col", $data);
			$i ++;
		}
	} else {
		$text .='<div class="item">'.$vnT->lang['product']['no_have_product'].'</div>';
	}
	$text_out = $text;
	//$vnT->Cache->save_cache("scroll_product", $text_out,$param_cache);
	return $text_out; 
}
function focus_product ($cat_id=0){
	global $vnT, $input;
	$textout="";
	$param_cache = array() ;		
	$cache = $vnT->Cache->read_cache("focus_product",$param_cache);
	if ($cache != _NOC) return $cache;	
	$sql = "SELECT * FROM product_status 
					WHERE lang='$vnT->lang_name' AND display=1 AND focus=1
					ORDER BY s_order DESC , status_id DESC  ";
	$result = $vnT->DB->query($sql);
	if($num = $vnT->DB->num_rows($result)){
		$i=0;
		$start=0;
		$num_row=5;
		$n= ($vnT->setting['n_list']) ? $vnT->setting['n_list'] : 10;		
		while ($row = $vnT->DB->fetch_row($result)){
			$i++;
			$status_id = $row['status_id'];
			$link_more = LINK_MOD."/status/".$status_id."/".$vnT->func->make_url($row['title']).".html";
			$f_title = "<a href='".$link_more."'>".$vnT->func->HTML($row['title'])."</a>";
			$where = " AND status=".$status_id;
			if($cat_id)	{
				$where .= get_where_cat($cat_id);
			}
			$where .= "  ORDER BY p_order DESC,  date_post DESC ";
			$row['list'] = row_product($where,$start,$n,$num_row);
			$row['f_title'] = $f_title;
			$row['more'] = "<a href='".$link_more."' class='more' >".$vnT->lang['product']['view_all']."</a>";
			$textout .=  load_html ("focus_product", $row);
		}
	}
  $vnT->Cache->save_cache("focus_product", $textout,$param_cache);
	return $textout;
}
function box_sidebar (){
	global $vnT, $input;
	$output = $vnT->lib->get_banner_sidebar('sidebar');
 	return $output;
}
function box_category (){
  global $vnT, $func, $DB, $conf, $input;
  $text = '';
  $cat_id = (int) $input['catID'];
  $parentid = get_parent_id($cat_id);
  $query= $DB->query("SELECT n.*,nd.cat_name,nd.friendly_url FROM product_category n, product_category_desc nd
                      WHERE n.cat_id=nd.cat_id AND lang='{$vnT->lang_name}' AND display=1 AND parentid={$parentid}
                      ORDER BY cat_order ASC, nd.cat_id DESC, date_post DESC");
  if ($num = $DB->num_rows($query)) {
    $cur_title = $vnT->lang['product']['cat_filter'];
    while ($row = $DB->fetch_row($query)) {
      $link = create_link('category',$row['cat_id'], $row['friendly_url']);
      $class = ($cat_id == $row['cat_id']) ? 'active' : '';
      if($cat_id == $row['cat_id'])
      	$cur_title = $vnT->func->HTML($row['cat_name']);
      $text.= '<div class="myCheckbox '.$class.'"><a href="'.$link.'">'.$vnT->func->HTML($row['cat_name']).'</a></div>';
    }
    $nd['f_title'] = $vnT->lang['product']['cat_filter'];
	  $nd['cur_title'] = $cur_title;
	  $nd['content'] = $text;
	  $textout = $vnT->skin_box->parse_box("box_select_filter", $nd);
  }
  return $textout;
}
function box_filter(){
	global $vnT,$DB, $input;
	$data = array();
	$data['Display_Filter'] = Display_Filter($input['display']);
	$data['Price_filter'] = Price_filter();
	$data['box_category'] = box_category();
	//$data['Sort_Filter'] = Sort_Filter($input['sort']);
 	return load_html('box_filter',$data);
}
function build_links ($cur_link ,$key,$value, $act="add")	{
	global $vnT ,$input;
	$new_link = "";
	$p = ((int) $input['p']) ? (int) $input['p'] : 1;
	$check = @explode("?",$cur_link);
	if($check[1]){
		$arr_new_param = @explode("&", $check[1]);	
		$is_new = 1;
		$new_link = $check[0]."?" ;
		foreach ($arr_new_param as $param){
			$tmp = @explode("=",$param);
			if ( $tmp[0] == $key ) {
				$new_link .=  $tmp[0] . "=" .$value ."&" ;
				$is_new=0;
			}else{
				$new_link .=  $param ."&" ;	
			}
		}
		if($is_new==1) {
			$new_link .= $key."=".$value; 
		}else{
			$new_link = substr($new_link,0,-1); 	
		}
	}else{
		$new_link = $cur_link ."/?".$key."=".$value; 	
	}
	return $new_link ;
}
function Price_filter () {
  global $func, $DB, $conf, $vnT, $input;
  $cur_link = $vnT->seo_url;
  $arr_price = explode(',', $vnT->setting['list_price']);
  //echo $vnT->setting['list_price']; die();
  if(is_array($arr_price)){
    $k=0;
    $count = count($arr_price);
    $price["0-".$arr_price[0]] = "0đ ".$vnT->lang['product']['price_to'].get_price_product($arr_price[0],'đ');
    for($i=0;$i<$count-1;$i++) {
      $key = trim($arr_price[$i])."-".trim($arr_price[$i+1]);
      $value = get_price_product($arr_price[$i],'đ').$vnT->lang['product']['price_to'].get_price_product($arr_price[$i+1],'đ');
      $price[$key] = $value;
    }
    $price[trim($arr_price[$count-1])] = $vnT->lang['product']['price_over'].get_price_product($arr_price[$count-1],'đ');
  }
  $cur_title = $vnT->lang['product']['select_price'];
  // echo "<pre/>";print_r($key);die;
  $link_price1 = build_links($cur_link,"price","0-99999999");
  $text =  '<li ><label class="select-box__option" for="0" aria-hidden=""><a href="'.$link_price1.'">All</a></label></li>';
  foreach ($price as $key => $value) {
    $link_price = build_links($cur_link,"price",$key);
    $selected = ($input['price'] == $key) ? 'active' : '';
    if($input['price'] == $key)
    	$cur_title = $vnT->func->cut_string($vnT->func->check_html($value,'nohtml'),22,1);
    //$text .= '<div class="myCheckbox '.$selected.'"><a href="'.$link_price.'">'.$value.'</a></div>';
    $text .= '<li ><label class="select-box__option" for="0" aria-hidden=""><a href="'.$link_price.'">'.$value.'</a></label></li>';
  }
  // $nd['f_title'] = $vnT->lang['product']['price_filter'];
  // $nd['cur_title'] = $cur_title;
  // $nd['content'] = $text;
  return $text;
}
function Display_Filter($key){
	global $vnT;
	$n = ($vnT->setting['n_grid']) ? $vnT->setting['n_grid'] : 8;
	$arr[1]['key'] = $n;
	$arr[1]['value'] = $n;
	$arr[2]['key'] = $n + $n;
	$arr[2]['value'] = $n + $n;
	$arr[3]['key'] = $n + $n*2;
	$arr[3]['value'] = $n + $n*2;
	$arr[4]['key'] = $n + $n*3;
	$arr[4]['value'] = $n + $n*3;
	$cur_link = $vnT->seo_url;
	$cur_title = $vnT->lang['product']['select_filter'];
	$text .= '<select name="page" id="page" onchange="location = this.value;">';
	$text .= '<option value="">'.$vnT->lang['product']['display_filter'].'</option>';
	for($i=1;$i<=count($arr);$i++){
		$selected = ($key==$arr[$i]['key']) ? 'selected' : '';
		$link = build_links($cur_link,"display",$arr[$i]['key']);
		if($key==$arr[$i]['key']) $cur_title = $arr[$i]['key'];
		// $text .= '<div class="myCheckbox '.$selected.'"><a href="'.$link.'">'.$arr[$i]['value'].'</a></div>';
		$text .= '<option value="'.$link.'" '.$selected.'>'.$arr[$i]['value'].' page</option>';
	}
	$text .= '</select>';
  // $nd['f_title'] = $vnT->lang['product']['display_filter'];
  // $nd['cur_title'] = $cur_title;
  // $nd['content'] = $text;
  return $text;
}
function Sort_Filter($key){
	global $vnT,$input,$DB;

	// $arr[1]['key'] = 'new';
	// $arr[1]['value'] = $vnT->lang['product']['sort_new'];

	// $arr[2]['key'] = 'old';
	// $arr[2]['value'] = $vnT->lang['product']['sort_old'];

	$arr[1]['key'] = 'low';
	$arr[1]['value'] = $vnT->lang['product']['price_low'];
	
	$arr[2]['key'] = 'hight';
	$arr[2]['value'] = $vnT->lang['product']['price_hight'];

	$cur_link = $vnT->seo_url;
	$cur_title = $vnT->lang['product']['sort_filter'];
	// $text .= '<select name="sort" id="sort" onchange="location = this.value;" class="select-box__current">';
	// $text .= '<option value="">Sort by</option>';
	for($i=1;$i<=count($arr);$i++){
		$selected = ($key==$arr[$i]['key']) ? 'selected' : '';
		if($key == $arr[$i]['key'])
			$cur_title = $arr[$i]['value'];
		$link = build_links($cur_link,"sort",$arr[$i]['key']);
		// $text .= '<li><div class="myCheckbox '.$selected.'"><a href="'.$link.'">'.$arr[$i]['value'].'</a></div></li>';
		// $text .= '<option value="'.$link.'" '.$selected.'>'.$arr[$i]['value'].'</option>';
		$text .= '<li ><label class="select-box__option" for="0" aria-hidden=""><a href="'.$link.'">'.$arr[$i]['value'].'</a></label></li>';
	}
	
	// $text .= '</select>';
	return $text;
	// $nd['f_title'] = $vnT->lang['product']['sort_filter'];
 	// $nd['cur_title'] = $cur_title;
 	// $nd['content'] = $text;
 	// return $vnT->skin_box->parse_box("box_select_filter", $nd);
}
function status_filter(){
	global $vnT,$DB,$input;
	$sql = "SELECT p.status_id, title FROM product_status p, product_status_desc pd
					WHERE p.status_id = pd.status_id AND display = 1 AND lang ='$vnT->lang_name'
					ORDER BY s_order ASC, p.status_id DESC";
	$result = $DB->query($sql);
	$cur_link = $vnT->seo_url;
	$arr_all = explode('?', $cur_link);
  if(count($arr_all)>1){
		$pattern = '/&status=[0-9]+/';
		if (preg_match($pattern, $arr_all[1],$mar)){
			$cur_link = str_replace($mar[0], '', $cur_link);
		} 
		$pattern = '/status=[0-9]+&/';
		if (preg_match($pattern, $arr_all[1],$mar)){
			$cur_link = str_replace($mar[0], '', $cur_link);
		}
		$pattern = '/status=[0-9]+/';
		if (preg_match($pattern, $arr_all[1],$mar)){
			$cur_link = str_replace($mar[0], '', $cur_link);
		}
		$arr_cur = explode('?', $cur_link);
		if(count($arr_cur)>1 && $arr_cur[1]==''){
			$cur_link = str_replace('/?', '', $cur_link);
		}
	}
	if($num = $DB->num_rows($result)){
		$cur_active = (!$input['status']) ? 'class="active"' : '';
		// $text .= '<li '.$cur_active.'><a href="'.$cur_link.'">'.$vnT->lang['product']['all'].'</a></li>';
		while ($row = $DB->fetch_row($result)) {
			$link = build_links($cur_link,"status",$row['status_id']);
			$title = $vnT->func->HTML($row['title']);
			$selected = ($input['status'] == $row['status_id']) ? 'active' : '';
			if($input['status'] == $row['status_id'])
				$cur_title = $title;
			$text .= '<li ><label class="select-box__option" for="0" aria-hidden=""><a href="'.$link.'">'.$title.'</a></label></li>';
			//$text .= '<div class="myCheckbox '.$selected.'"><a href="'.$link.'">'.$title.'</a></div>';
		}
	}
  return $text;
}
function attr_filter(){
	global $vnT,$DB,$input;
	$sql = "SELECT p.attr_id, title FROM product_attr p, product_attr_desc pd
					WHERE p.attr_id = pd.attr_id AND display = 1 AND lang ='$vnT->lang_name'
					ORDER BY display_order ASC, p.attr_id DESC";
	$result = $DB->query($sql);
	$cur_link = $vnT->seo_url;
	$cur_title = $vnT->lang['product']['select_attr'];
	if($num = $DB->num_rows($result)){
		while ($row = $DB->fetch_row($result)) {
			$link = build_links($cur_link,"attr",$row['attr_id']);
			$title = $vnT->func->HTML($row['title']);
			$selected = ($input['attr'] == $row['attr_id']) ? 'active' : '';
			if($input['attr'] == $row['attr_id'])
				$cur_title = $title;
			$text .= '<div class="myCheckbox '.$selected.'"><a href="'.$link.'">'.$title.'</a></div>';
		}
	}
	$text .= '</select>';
	$data['f_title'] = $vnT->lang['product']['attr_filter'];
	$data['cur_title'] = $cur_title;
  $data['content'] = $text;
  return $vnT->skin_box->parse_box("box_filter", $data);
}
function List_category (){
  global $vnT, $func, $DB, $conf, $input;
  $text = '';
  $arr_out = array();
  $cat_id = (int) $input['catID'];
  $parentid = get_parent_id($cat_id);
  $query= $DB->query("SELECT n.*,nd.cat_name,nd.friendly_url FROM product_category n, product_category_desc nd
                      WHERE n.cat_id=nd.cat_id AND lang='{$vnT->lang_name}' AND display=1 AND parentid={$parentid}
                      ORDER BY cat_order ASC, nd.cat_id DESC, date_post DESC");
  if ($num = $DB->num_rows($query)) {
    $f_title = $vnT->lang['product']['select_category'];
    while ($row = $DB->fetch_row($query)) {
      $link = create_link('category',$row['cat_id'], $row['friendly_url']);
      if($cat_id == $row['cat_id']){
        $class = ' class="current"';
        $f_title = $func->HTML($row['cat_name']);
      }else
        $class = '';
      $text .= '<li '.$class.'><a href="'.$link.'">'.$func->HTML($row['cat_name']).'</a></li>';
    }
    $nd['f_title'] = $vnT->lang['product']['product'];
    $textout = '<ul>'.$text.'</ul>';
    // $arr_out['mb'] = $vnT->skin_box->parse_box("box_cat_mobile",$nd);
    // $arr_out['side'] = $vnT->skin_box->parse_box("box_cat_side",$nd);
  }
  return $textout;
}
function get_parent_id($cat_id){
  global $vnT,$DB;
  $parentid = 0;
  $result = $DB->query("SELECT n.cat_id, parentid FROM product_category n, product_category_desc nd
                        WHERE n.cat_id = nd.cat_id AND display = 1
                        AND lang='$vnT->lang_name' AND parentid = {$cat_id}");
  if($num = $DB->num_rows($result)){
    $parentid = $cat_id;
  } else{
    $result = $DB->query("SELECT parentid FROM product_category WHERE cat_id = {$cat_id}");
    if($row = $DB->fetch_row($result))
      $parentid = $row['parentid'];
  }
  return $parentid;
}









/*-------------- box_category --------------------*/
  function box_category1 ($catid=0) {
    global $vnT, $func, $DB, $conf, $input;
    $where = " and parentid=0 order by cat_order ASC , date_post DESC";
    $sql = $vnT->lib->getSql("product_category", "cat_id", $where);
    $result = $DB->query($sql);
    if ($num = $DB->num_rows($result)) {
      $i=0;
      $text = "<ul>"; 
      while ($row = $DB->fetch_row($result)) {
        $i++;
        $id = $row['cat_id'];
        $src = ROOT_URL . "vnt_upload/product/" . $row['picture'];
        $title = str_replace("<br/>", "", $row['cat_name']);
        $link = $vnT->link_root.$row['friendly_url'].".html";
        // $class = ($input['catID'] == $row['cat_id']) ? 'class=active' : "";
        //check sub
        $check = check_sub_menu($id);
        $class = ($check > 0) ? 'class=hasSub' : "";
       	$text .= '<li '.$class.'><a href="'.$link.'"><span class="img"><img src="'.$src.'" alt="" /></span><span>'.$title.'</span></a>';
        $text .= sub_menu($id);
        $text .= "</li>";
      }
      $text .= "</ul>";
    }
    return $text;
  }

  function check_sub_menu ($parent) {
    global $vnT, $func, $DB, $conf, $input;
    $where2 = " and parentid={$parent} order by cat_order ASC , date_post DESC";
    $sql2 = $vnT->lib->getSql("product_category", "cat_id", $where2);
    $result2 = $DB->query($sql2);
    $num = $DB->num_rows($result);
    return $num;
  }

  function sub_menu ($parent) {
    global $vnT, $func, $DB, $conf, $input;
    $where = " and parentid={$parent} order by cat_order ASC , date_post DESC";
    $sql = $vnT->lib->getSql("product_category", "cat_id", $where);
    $result = $DB->query($sql);
    if ($num = $DB->num_rows($result)) {
      $i=0;
      $text = "<ul>";
      while ($row = $DB->fetch_row($result)) {
        $i++;
        $title = $row['cat_name'];
        $link = $vnT->link_root.$row['friendly_url'].".html";
        $class = ($input['catID'] == $row['cat_id']) ? 'class=current' : "";
        $text .= "<li {$class}><a href=\"{$link}\" title=\"{$title}\">{$title}</a>";
        $text .= sub_menu($id);
        $text .= "</li>";
      }
      $text .= "</ul>";
    }
    return $text;
  }

?>