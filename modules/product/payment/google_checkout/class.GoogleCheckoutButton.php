<?php

/* vim: set expandtab tabstop=4 shiftwidth=4 softtabstop=4: */

/**
 * The main include file for GoogleCheckoutButton class
 *
 * PHP version 4 and 5
 *
 * GoogleCheckoutButton class for creating and submitting requests to Google Checkout
 * Copyright (c) 2007 Vagharshak Tozalakyan
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU  Lesser General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * @version  0.1.1
 * @author   Vagharshak Tozalakyan <vagh@tozalakyan.com>
 * @license  http://www.gnu.org/licenses/lgpl.html  GNU Lesser General Public License
 * @link     http://www.tozalakyan.com/xhtml/php_classes/foo.tz
 */

/**
 * Class for creating and submitting requests to Google using Google Checkout HTML API
 *
 * This class can be used to detect and protect a Web site from attempts to
 * flood it with too many requests. It also allows to protect the site from
 * automatic downloading many pages or files from the same IP address. The
 * detection of flood is determine according to a set of parameters indicating
 * the maximal allowed number of requests for the certain time interval. It is
 * possible to set several parameters at once in order to perform more
 * effective protection.
 *
 * @author   Vagharshak Tozalakyan <vagh@tozalakyan.com>
 * @license  http://www.gnu.org/licenses/lgpl.html  GNU Lesser General Public License
 * @link     http://www.tozalakyan.com/xhtml/php_classes/foo.tz
 */
class GoogleCheckoutButton
{

    // {{{ properties

    /**
     * Merchant ID
     *
     * @access protected
     * @var string
     */
    var $_merchantId = '';

    /**
     * Server type - 'sandbox' or 'checkout'
     *
     * @access protected
     * @var string
     */
    var $_serverType = 'sandbox';

    /**
     * Currency
     *
     * @access protected
     * @var string
     */
    var $_currency = 'USD';

    /**
     * Shopping cart items
     *
     * @access protected
     * @var array
     */
    var $_items = array();

    /**
     * Shipping methods
     *
     * @access protected
     * @var array
     */
    var $_shipMethods = array();

    /**
     * Tax rate and US state
     *
     * @access protected
     * @var array
     */
    var $_taxRate = array();

    // }}}
    /// {{{ GoogleCheckoutButton()

    /**
     * Class constructor
     *
     * @access public
     * @param string $merchantId Merchant ID.
     * @param string $serverType Server type - 'sandbox' or 'checkout'.
     * @param string $currency Currency.
     * @return object
     */
    function GoogleCheckoutButton($merchantId, $serverType, $currency = 'USD')
    {
        $this->_merchantId = $merchantId;
        $this->_serverType = ($serverType == 'checkout' ? 'checkout' : 'sandbox');
        $this->_currency = $currency;
    }

    // }}}
    /// {{{ addItem()

    /**
     * Add a shopping cart item
     *
     * @access public
     * @param string $name Item name.
     * @param string $description Item description.
     * @param int $quantity Item quantity.
     * @param float $price Item unit price.
     * @return void
     */
    function addItem($name, $description, $quantity, $price)
    {
        $this->_items[] = array(
            'name' => $name,
            'description' => $description,
            'quantity' => abs(intval($quantity)),
            'price' => floatval($price)
        );
    }

    // }}}
    /// {{{ addDiscount()

    /**
     * Add a discount amount
     *
     * @access public
     * @param string $name Discount item name.
     * @param string $description Discount item description.
     * @param float $amount Discount amount.
     * @return void
     */
    function addDiscount($name, $description, $amount)
    {
        if ($amount > 0) {
            $amount = 0 - $amount;
        }
        $this->_items[] = array(
            'name' => $name,
            'description' => $description,
            'quantity' => 1,
            'price' => floatval($amount)
        );
    }

    // }}}
    /// {{{ addShippingMethod()

    /**
     * Add a shipping method
     *
     * @access public
     * @param string $name Name of shipping method.
     * @param float $price Shipping price.
     * @param string $area Region of the United States where items may be shipped:
     *                     'CONTINENTAL_48' - All U.S. states except Alaska and Hawaii,
     *                     'FULL_50_STATES' - All U.S. states,
     *                     'ALL' - All U.S. postal service addresses.
     *
     * @return void
     */
    function addShippingMethod($name, $price, $area = '')
    {
        $this->_shipMethods[] = array(
            'name' => $name,
            'price' => abs(floatval($price)),
            'area' => $area
        );
    }

    // }}}
    /// {{{ setTaxRate()

    /**
     * Define a tax rate for specific US state
     *
     * @access public
     * @param float $taxRate Tax rate.
     * @param string $taxState US state where particular tax rule is applied.
     * @return void
     */
    function setTaxRate($taxRate, $taxState)
    {
        $this->_taxRate = array(
            'rate' => $taxRate,
            'state' => $taxState
        );
    }

    // }}}
    /// {{{ showButton()

    /**
     * Display the button
     *
     * @access public
     * @param string $type The type of button - 'large', 'medium' or 'small'.
     * @param string $style The button style - 'white' or 'trans'.
     * @param bool $disabled Button state.
     * @param bool $asString Return form as a string if true.
     * @return string
     */
    function showButton($type = 'large', $style = 'white', $disabled = false, $asString = false)
    {
        if ($this->_serverType == 'sandbox') {
            $submitUrl = 'https://sandbox.google.com/checkout/cws/v2/Merchant/' . $this->_merchantId . '/checkoutForm';
            $buttonUrl = 'http://sandbox.google.com/checkout/buttons/checkout.gif';
        } else {
            $submitUrl = 'https://checkout.google.com/cws/v2/Merchant/' . $this->_merchantId . '/checkoutForm';
            $buttonUrl = 'http://checkout.google.com/buttons/checkout.gif';
        }
        switch ($type) {
        case 'small':
            $w = 160;
            $h = 43;
            break;
        case 'medium':
            $w = 168;
            $h = 44;
            break;
        default:
            $w = 180;
            $h = 46;
        }
        $variant = ($disabled ? 'disabled' : 'text');
        $formHtml = '' ;
        foreach ($this->_items as $i => $item) {
			$stt = $i+1;
            $formHtml .= '<input type="hidden" name="item_name_' . $stt . '" value="' . $item['name'] . '" />' . "\n";
            $formHtml .= '<input type="hidden" name="item_description_' . $stt . '" value="' . $item['description'] . '" />' . "\n";
            $formHtml .= '<input type="hidden" name="item_quantity_' . $stt . '" value="' . $item['quantity'] . '" />' . "\n";
            $formHtml .= '<input type="hidden" name="item_price_' . $stt . '" value="' . $item['price'] . '" />' . "\n";
            $formHtml .= '<input type="hidden" name="item_currency_' . $stt . '" value="' . $this->_currency . '" />' . "\n";
        }
        foreach ($this->_shipMethods as $i => $shipMethod) {
			$stt=$i+1;
            $formHtml .= '<input type="hidden" name="ship_method_name_' . $stt . '" value="' . $shipMethod['name'] . '" />' . "\n";
            $formHtml .= '<input type="hidden" name="ship_method_price_' . $stt . '" value="' . $shipMethod['price'] . '" />' . "\n";
            $formHtml .= '<input type="hidden" name="ship_method_currency_' . $stt . '" value="' . $this->_currency . '" />' . "\n";
            if (!empty($shippingMethod['area'])) {
                $formHtml .= '<input type="hidden" name="ship_method_us_area_' . $stt . '" value="' . $shipMethod['area'] . '" />' . "\n";
            }
        }
        if ($this->_taxRate['rate'] > 0 && !empty($this->_taxRate['state'])) {
            $formHtml .= '<input type="hidden" name="tax_rate" value="' . $this->_taxRate['rate'] . '" />' . "\n";
            $formHtml .= '<input type="hidden" name="tax_us_state" value="' . $this->_taxRate['state'] . '" />' . "\n";
        }
		$formHtml .= '<input type="hidden" name="_charset_" />' . "\n";
        return $formHtml;
    }

}

?>