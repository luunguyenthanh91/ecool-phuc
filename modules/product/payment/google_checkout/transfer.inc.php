<?php
require_once('class.GoogleCheckoutButton.php');

//========
function fixedVars($info_google){
	global $vnT,$DB,$conf,$input,$session,$cart;
	$cart_order_id = $vnT->module['order_id'];
	$gch = new GoogleCheckoutButton($vnT->module['merchant_id'],$vnT->module['server'],$vnT->module['currency']);
	$gch->addShippingMethod($info_google['ship_title'],$info_google['ship_price'], 'ALL');
	
	
  //Items
  $contents = $cart->display_contents($session);
	if($cart->num_items($session))  
	{ 
		$x = 0;
		while($x < $cart->num_items($session))
		{
		   if ($contents["price"][$x]){ 
		   	$price= $contents["price"][$x];
		   }else $price=0.00;
		   $description = "Product ID ".$contents["p_id"][$x];
		   $gch->addItem($contents["p_name"][$x],$description, $contents["quantity"][$x],$price);
		   $x++;
		}// ends loop for each product
	}
	$gch->addDiscount('Discount', '', $info_google['dis_price'] );
  //Output				
  	$hiddenVars = $gch->showButton();
 
	return $hiddenVars;
	
}

if($vnT->module['server']=="sandbox"){
	$formAction = "https://sandbox.google.com/checkout/cws/v2/Merchant/".$vnT->module['merchant_id']."/checkoutForm";
} else {
	$formAction = 'https://checkout.google.com/cws/v2/Merchant/' . $vnT->module['merchant_id'] . '/checkoutForm';
}
///////////////////////////
// Other Vars
////////

$is_google = 1;
$is_online = 1 ;
$is_insert = "yes";
?>