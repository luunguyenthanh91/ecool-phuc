﻿<?php
$file_payoo_conf = $conf['rootpath']."modules/product/payment/payoo/lib/Config.php";
if (! file_exists($file_payoo_conf))
  die("Không tìm thấy File cấu hình của payoo.com.vn ");

function fixedVars ($info)
{
  global $vnT, $conf, $input, $cart;
  //echo "<pre>";
	//print_r($vnT->module);
	//echo "</pre>";	
  include_once ($conf['rootpath']."modules/product/payment/payoo/lib/Config.php");
  include_once ($conf['rootpath'].'modules/product/payment/payoo/lib/PaymentXMLFactory.php');
  try {
    $pay = new PaymentXMLFactory(TRUE); //FALSE
    $pay->Session = $vnT->user['seesion_id'];
    $pay->BusinessUsername = _BusinessUsername;
    $pay->ShopID = _ShopID;
    $pay->ShopTitle = _ShopTitle;
    $pay->ShopDomain = _ShopDomain;
    $pay->ShopBackUrl = _ShopBackUrl;
    $pay->NotifyUrl = _NotifyUrl;
    $pay->PayooPaymentUrl = _PayooPaymentUrl;
    $pay->OrderNo = $vnT->module['order_code'];
    $pay->CashAmount = $vnT->module['total_price'];
    $pay->StartShippingDate = date('d/m/Y');
    $pay->ShippingDays = $vnT->module['ShippingDays'];
    $pay->OrderDescription = email_ShowCart();
    $hiddenVars .= "<input type='hidden' name='OrdersForPayoo' value='" . $pay->GetPaymentXml() . "' />";
    //$pay->PayNow();
  } catch (Exception $ex) {
    echo "Message: " . $ex;
  }
  return $hiddenVars;
}

function formAction ($info)
{
  global $vnT, $conf, $input;
	
	$link_action = $vnT->module['PayooPaymentUrl']; 
  return $link_action ;
}
 
$is_payoo = 1;
$is_online = 1;
$is_insert = "yes";
?>