<?php

abstract class ResponseType
{
  public $DigitalSignature;
  public $Ack;
  public $Error;
}

class ErrorType
{
  public $SeverityCode;
  public $ShortMessage;
  public $LongMessage;
}

class GetOrderInformationResponseType extends ResponseType
{
  public $OrderStatus;
  public $OrderCash;
  public $OrderFee;
  public $PaymentDate;
  public $ShippingDate;
  public $DeliveryDate;

  function LoadData ($stdObject)
  {
    $this->DigitalSignature = $stdObject->DigitalSignature;
    $this->Ack = $stdObject->Ack;
    $this->OrderStatus = $stdObject->OrderStatus;
    $this->OrderCash = $stdObject->OrderCash;
    $this->OrderFee = $stdObject->OrderFee;
    if (! ($stdObject->Ack == 'Success')) {
      $this->Error = new ErrorType();
      $this->Error->SeverityCode = $stdObject->Error->SeverityCode;
      $this->Error->ShortMessage = $stdObject->Error->ShortMessage;
      $this->Error->LongMessage = $stdObject->Error->LongMessage;
      return $this;
    }
    $this->PaymentDate = $stdObject->PaymentDate;
    $this->ShippingDate = $stdObject->ShippingDate;
    $this->DeliveryDate = $stdObject->DeliveryDate;
    return $this;
  }

  function GetXml ()
  {
    $xml = '';
    if ($this->Ack == 'Success') {
      $xml = '<?xml version="1.0"?>
<GetOrderInformationResponseType xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
  <Ack xmlns="BusinessAPI">' . $this->Ack . '</Ack>
  <OrderStatus xmlns="BusinessAPI">' . $this->OrderStatus . '</OrderStatus>
  <OrderCash xmlns="BusinessAPI">' . $this->OrderCash . '</OrderCash>
  <OrderFee xmlns="BusinessAPI">' . $this->OrderFee . '</OrderFee>
  <PaymentDate xmlns="BusinessAPI">' . $this->PaymentDate . '</PaymentDate>
  <ShippingDate xmlns="BusinessAPI">' . $this->ShippingDate . '</ShippingDate>
  <DeliveryDate xmlns="BusinessAPI">' . $this->DeliveryDate . '</DeliveryDate>
</GetOrderInformationResponseType>';
    } else 
      if ($this->Ack == 'Failure') {
        $xml = '<?xml version="1.0"?>
<GetOrderInformationResponseType xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
  <Ack xmlns="BusinessAPI">' . $this->Ack . '</Ack>
  <Error xmlns="BusinessAPI">
    <SeverityCode>' . $this->Error->SeverityCode . '</SeverityCode>
    <ShortMessage>' . $this->Error->ShortMessage . '</ShortMessage>
    <LongMessage>' . $this->Error->LongMessage . '</LongMessage>
  </Error>
  <OrderStatus xmlns="BusinessAPI">' . $this->OrderStatus . '</OrderStatus>
  <OrderCash xmlns="BusinessAPI">' . $this->OrderCash . '</OrderCash>
  <OrderFee xmlns="BusinessAPI">' . $this->OrderFee . '</OrderFee>
</GetOrderInformationResponseType>';
      } else {
        $xml = '<?xml version="1.0"?>
<GetOrderInformationResponseType xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
  <Ack xmlns="BusinessAPI">' . $this->Ack . '</Ack>
  <Error xmlns="BusinessAPI">
    <SeverityCode>' . $this->Error->SeverityCode . '</SeverityCode>
    <ShortMessage>' . $this->Error->ShortMessage . '</ShortMessage>
    <LongMessage>' . $this->Error->LongMessage . '</LongMessage>
  </Error>
  <OrderStatus xmlns="BusinessAPI">' . $this->OrderStatus . '</OrderStatus>
  <OrderCash xmlns="BusinessAPI">' . $this->OrderCash . '</OrderCash>
  <OrderFee xmlns="BusinessAPI">' . $this->OrderFee . '</OrderFee>
  <PaymentDate xmlns="BusinessAPI">' . $this->PaymentDate . '</PaymentDate>
  <ShippingDate xmlns="BusinessAPI">' . $this->ShippingDate . '</ShippingDate>
  <DeliveryDate xmlns="BusinessAPI">' . $this->DeliveryDate . '</DeliveryDate>
</GetOrderInformationResponseType>';
      }
    return $xml;
  }
}

class SendOrderResponseType extends ResponseType
{
  public $PayooOrderID;

  function SendOrderResponseType ()
  {}

  function LoadData ($stdObject)
  {
    $this->DigitalSignature = $stdObject->DigitalSignature;
    $this->Ack = $stdObject->Ack;
    $this->PayooOrderID = $stdObject->PayooOrderID;
    if (! ($stdObject->Ack == 'Success')) {
      $this->Error = new ErrorType();
      $this->Error->SeverityCode = $stdObject->Error->SeverityCode;
      $this->Error->ShortMessage = $stdObject->Error->ShortMessage;
      $this->Error->LongMessage = $stdObject->Error->LongMessage;
      return $this;
    }
    return $this;
  }

  function GetXml ()
  {
    $xml = '';
    if ($this->Ack == 'Success') {
      $xml = '<?xml version="1.0"?>
<SendOrderResponseType xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
  <Ack xmlns="BusinessAPI">' . $this->Ack . '</Ack>
  <PayooOrderID xmlns="BusinessAPI">' . $this->PayooOrderID . '</PayooOrderID>
</SendOrderResponseType>';
    } else {
      $xml = '<?xml version="1.0"?>
<SendOrderResponseType xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
  <Ack xmlns="BusinessAPI">' . $this->Ack . '</Ack>
  <Error xmlns="BusinessAPI">
    <SeverityCode>' . $this->Error->SeverityCode . '</SeverityCode>
    <ShortMessage>' . $this->Error->ShortMessage . '</ShortMessage>
    <LongMessage>' . $this->Error->LongMessage . '</LongMessage>
  </Error>
  <PayooOrderID xmlns="BusinessAPI">' . $this->PayooOrderID . '</PayooOrderID>
</SendOrderResponseType>';
    }
    return $xml;
  }
}

class UpdateOrderStatusResponseType extends ResponseType
{

  function LoadData ($stdObject)
  {
    $this->DigitalSignature = $stdObject->DigitalSignature;
    $this->Ack = $stdObject->Ack;
    if (! ($stdObject->Ack == 'Success')) {
      $this->Error = new ErrorType();
      $this->Error->SeverityCode = $stdObject->Error->SeverityCode;
      $this->Error->ShortMessage = $stdObject->Error->ShortMessage;
      $this->Error->LongMessage = $stdObject->Error->LongMessage;
    }
    return $this;
  }

  function GetXml ()
  {
    $xml = '';
    if ($this->Ack == 'Success') {
      $xml = '<?xml version="1.0"?>
<UpdateOrderStatusResponseType xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
  <Ack xmlns="BusinessAPI">' . $this->Ack . '</Ack>
</UpdateOrderStatusResponseType>';
    } else {
      $xml = '<?xml version="1.0"?>
<UpdateOrderStatusResponseType xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
  <Ack xmlns="BusinessAPI">' . $this->Ack . '</Ack>
  <Error xmlns="BusinessAPI">
    <SeverityCode>' . $this->Error->SeverityCode . '</SeverityCode>
    <ShortMessage>' . $this->Error->ShortMessage . '</ShortMessage>
    <LongMessage>' . $this->Error->LongMessage . '</LongMessage>
  </Error>
</UpdateOrderStatusResponseType>';
    }
    return $xml;
  }
}

class ConfirmNotifyInformationResponseType extends ResponseType
{
  public $ConfirmResult;

  function LoadData ($stdObject)
  {
    $this->DigitalSignature = $stdObject->DigitalSignature;
    $this->Ack = $stdObject->Ack;
    if (! ($stdObject->Ack == 'Success')) {
      $this->Error = new ErrorType();
      $this->Error->SeverityCode = $stdObject->Error->SeverityCode;
      $this->Error->ShortMessage = $stdObject->Error->ShortMessage;
      $this->Error->LongMessage = $stdObject->Error->LongMessage;
    }
    $this->ConfirmResult = $stdObject->ConfirmResult;
    return $this;
  }

  function GetXml ()
  {
    $xml = '';
    if ($this->Ack == 'Success') {
      $xml = '<?xml version="1.0"?>
<ConfirmNotifyInformationResponseType xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
  <Ack xmlns="BusinessAPI">' . $this->Ack . '</Ack>
  <ConfirmResult xmlns="BusinessAPI">' . $this->ConfirmResult . '</ConfirmResult>
</ConfirmNotifyInformationResponseType>';
    } else {
      $xml = '<?xml version="1.0"?>
<ConfirmNotifyInformationResponseType xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
  <Ack xmlns="BusinessAPI">' . $this->Ack . '</Ack>
  <Error xmlns="BusinessAPI">
    <SeverityCode>' . $this->Error->SeverityCode . '</SeverityCode>
    <ShortMessage>' . $this->Error->ShortMessage . '</ShortMessage>
    <LongMessage>' . $this->Error->LongMessage . '</LongMessage>
  </Error>
</ConfirmNotifyInformationResponseType>';
    }
    return $xml;
  }
}
?>