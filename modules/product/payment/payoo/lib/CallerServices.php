<?php
include_once ('SOAPCaller.php');
include_once ('RequestType.php');
include_once ('ResponseType.php');

class CallerServices
{
  private $sign = NULL;

  function CallerServices ()
  {
    try {
      $this->sign = new DigitalSignature();
      $this->sign->LoadPrivateKey(_PrivateKey);
      $this->sign->LoadPublicCertificate(_PublicCertificate);
      $this->sign->SetTempDir(_TempDirectory);
    } catch (Exception $ex) {
      throw $ex;
    }
  }

  function Call ($APIName, $APIParam)
  {
    try {
      $Caller = new SOAPCaller();
      $APIParam->DigitalSignature = $this->sign->sign($APIParam->GetXml());
      $response = $Caller->Call($APIName, $APIParam);
      $response = $this->GetResponseObject($APIName, $response->CallResult);
      if ($this->VerifySignature($response->GetXml(), $response->DigitalSignature) != TRUE) {
        throw new Exception('Invalid digital signature.');
      }
      return $response;
    } catch (Exception $ex) {
      throw $ex;
    }
  }

  private function GetResponseObject ($APIName, $stdObject)
  {
    try {
      $APIName = $APIName . 'ResponseType';
      $return = new $APIName();
      $return->LoadData($stdObject);
      return $return;
    } catch (Exception $ex) {
      throw $ex;
    }
  }

  private function FormatDigitalSignature ($DigitalSignature)
  {
    if (strlen($DigitalSignature) <= 0) {
      throw new Exception('Invalid digital signature formating.');
    } else {
      $str = '';
      $n = ceil(strlen($DigitalSignature) / 64);
      for ($j = 1; $j <= $n; $j ++) {
        $str = $str . substr($DigitalSignature, ($j - 1) * 64, 64) . chr(10);
      }
    }
    return $str;
  }

  private function VerifySignature ($signedData, $DigitalSignature)
  {
    try {
      $this->sign->SetPartnerPubCertPath(_PayooPublicCertificate);
      if ($this->sign->Verify($signedData, $this->FormatDigitalSignature($DigitalSignature)) != 1) {
        return FALSE;
      }
      return TRUE;
    } catch (Exception $ex) {
      throw $ex;
    }
  }
}
?>