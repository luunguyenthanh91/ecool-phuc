<?php 
	define('IN_vnT',1);
	define('DS', DIRECTORY_SEPARATOR);
	require_once("../../../../_config.php");  
	require_once($conf['rootpath']."includes/class_db.php"); 
	$DB = new DB; 	
	require_once($conf['rootpath']."includes/class_functions.php"); 	
	$func = new Func_Global;
	$conf=$func->fetchDbConfig($conf); 
	
	//Lay thong tin tu Baokim POST sang
	$req = '';
	foreach ( $_POST as $key => $value ) {
		$value = urlencode ( stripslashes ( $value ) );
		$req .= "&$key=$value";
	}
	
	$myFile = "logs.txt";
	$fh = fopen($myFile, 'a');
	fwrite($fh, $req);

	$ch = curl_init();

	//Dia chi chay BPN test
	//curl_setopt($ch, CURLOPT_URL,'http://sandbox.baokim.vn/bpn/verify');
	
	//Dia chi chay BPN that
	curl_setopt($ch, CURLOPT_URL,'https://www.baokim.vn/bpn/verify');
	curl_setopt($ch, CURLOPT_VERBOSE, 1);
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
	curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch, CURLOPT_POST, 1);
	curl_setopt($ch, CURLOPT_POSTFIELDS, $req);
	$result = curl_exec($ch);
	$status = curl_getinfo($ch, CURLINFO_HTTP_CODE); 
	$error = curl_error($ch);
	
	fwrite($fh, " | result = ".$result." | status =".$status." | error =".$error);
	
	if($result != '' && strstr($result,'VERIFIED') && $status==200){
		//thuc hien update hoa don
		fwrite($fh, ' => VERIFIED');
		
		$order_id = $_POST['order_id'];
		$transaction_id = $_POST['transaction_id'];
		$transaction_status = $_POST['transaction_status'];
		$payment_type = $_POST['transaction_status'];
		//Mot so thong tin khach hang khac
		$customer_name = $_POST['customer_name'];
		$customer_address = $_POST['customer_address'];
		//...
		
		//neu giao dich thanh cong
		if ($transaction_status == 4){
			//kiem tra ma so 
			$result = $DB->query("SELECT * FROM order_sum WHERE order_code='".$order_id.'"  AND status=0');
			if($row = $DB->fetch_row($result))
			{
				// neu co thi update trang thai da thanh toan				
				$dup['status']	= 3; // da thanh toan 
				$DB->do_update("order_sum",$dup,"order_code='".$order_id."' "); 				
			} 			
		}
		
		/**
		 * Neu khong thi bo qua
		 */
	}else{
		fwrite($fh, ' => INVALID');
	}
	
	if ($error){
		fwrite($fh, " | ERROR: $error");
	}
	
	fwrite($fh, "\r\n");
	
	fclose($fh);
	
	$DB->close();
?>