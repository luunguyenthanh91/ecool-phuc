<?php
require_once ('class.authorizenet.php');

//========
function fixedVars ($info)
{
  global $vnT, $DB, $func, $conf, $input,  $cart;
  
	  $order_desc =  $vnT->lang['product']['order_desc']." (#".$vnT->module['order_code'].")";
		$d_state_code  =  $vnT->lib->get_state_code($info['d_state']) ;
		$c_state_code  =  $vnT->lib->get_state_code($info['c_state']) ;
		
		$sequence	= rand(1, 1000); 
		$timeStamp	= time(); 
		
		if( phpversion() >= '5.1.2' )
		{	$fingerprint = hash_hmac("md5", $vnT->module['merchantid'] . "^" . $sequence . "^" . $timeStamp . "^" . $vnT->module['total_price'] . "^", $vnT->module['transactionkey']); }
		else 
		{ $fingerprint = bin2hex(mhash(MHASH_MD5, $vnT->module['merchantid'] . "^" . $sequence . "^" . $timeStamp . "^" .  $vnT->module['total_price'] . "^", $vnT->module['transactionkey'])); }


		$ccnum = $input['ccnum'];
		$cc_code = $input['cc_cvv2'];
		$ccexp = $input['ccexpm'].$input['ccexpy'];
		
	// Arrange the data into name/value pairs ready to send
		$an_values = array (
			"x_login"				=> $vnT->module['merchantid'],
			//"x_tran_key"			=> $vnT->module['transactionkey'],
			"x_type"				=> $vnT->module['transactiontype'],
			"x_version"				=> "3.1",
			//"x_delim_char"			=> "|",
			//"x_delim_data"			=> "true",
			//"x_url"					=> "false",
			//"x_duplicate_window"	=> "28800",
			"x_fp_sequence" => $sequence,
			"x_fp_timestamp" => $timeStamp,
			"x_fp_hash" => $fingerprint,
			"x_show_form" => 'PAYMENT_FORM',
			
			"x_method"				=> "CC",			
			"x_relay_response"		=> "false",
			"x_card_num"			=> $ccnum,
			"x_exp_date"			=> $ccexp,
			"x_card_code"	=>	$cc_code,
			'x_invoice_num'			=> $vnT->module['order_code'],
			"x_description"			=> $order_desc,
			"x_amount"				=> $vnT->module['total_price'],
			"x_phone"				=> $info['d_phone'],
			"x_first_name"			=> $info['d_fname'],
			"x_last_name"			=> $info['d_lname'],
			"x_address"				=> $info['d_address'],
			"x_email"				=> $info['d_email'],
			"x_city"				=> $info['d_city'],
			"x_state"				=> $d_state_code,
			"x_zip"					=> $info['d_zipcode'],
			"x_country"				=> $info['d_country'],
			"x_company"				=>  $info['d_company'],

			//shipping info
			"x_ship_to_first_name"	=> $info['c_fname'],
			"x_ship_to_last_name"	=> $info['c_lname'],
			"x_ship_to_address"		=> $info['c_address'],
			"x_ship_to_city"		=> $info['c_city'],
			"x_ship_to_state"		=> $c_state_code,
			"x_ship_to_zip"			=> $info['c_zipcode'],
			"x_ship_to_country"		=> $info['c_country'],
			"x_ship_to_company"		=> $info['c_company'],
			"x_ship_to_phone"		=> $info['c_phone'],

			"shop_order_token"		=> $cart->session
		);
		
		$hiddenVars ='';
		foreach($an_values as $k=>$v) {
			
			//$an_data .= sprintf("%s=%s&", $k, urlencode($v));
			$hiddenVars .="<input type='hidden' name='".$k."' value='" . $v . "' />"."\n";
		}
		//$an_data = rtrim($an_data, '&');
				 
  	return $hiddenVars;
}

//========
function ProcessPaymentForm($info)
{
	global $vnT, $DB, $func, $conf, $input,  $cart;
	$Authorizenet = new Authorizenet();
	
	$arr_return = array();
	$arr_return['ok'] = 0;
	$arr_return['error'] = '';
	
	$bill_firstname = "";
	$bill_lastname = "";
	$result = "";
	$an_data = "";
	$an_uri = "/gateway/transact.dll";
	$error = false;

	$requiredFields = array(
		"ccname",
		"ccnum",
		"cc_cvv2",
		"ccexpm",
		"ccexpy" 
	);
 	 

	$missingFields = false;
	foreach($requiredFields as $field) {
		if(!isset($_POST[$field]) || !$_POST[$field]) {
			$missingFields = true;
		}
	}
	
	$d_state_code  =  $vnT->lib->get_state_code($info['d_state']) ;
	$c_state_code  =  $vnT->lib->get_state_code($info['c_state']) ;
	
	//check cart va Fields yeu cau
	if(isset($cart->session) && $missingFields == false) 
	{
		$ccname = $_POST['ccname'];
		$ccnum = $_POST['ccnum'];
		$cccode = $_POST['cc_cvv2'];
		$ccexpm = $_POST['ccexpm'];
		$ccexpy = $_POST['ccexpy'];
		$ccexp = sprintf("%s%s", $ccexpm, $ccexpy);
		
		$ccaddress = $info['d_address'];
		$cccompany = $info['d_company'];
		$cccity = $info['d_city'];
		$ccstate = $d_state_code;
		
		$cczip = $info['d_zipcode'];
		$cccountry =  $info['d_country'];
 		
		// Split the billing name up into firstname and last name
		$bill_details = explode(" ", $ccname);

		for($i = 0; $i < count($bill_details)-1; $i++) {
			$bill_firstname .= $bill_details[$i] . " ";
		}

		$bill_firstname = trim($bill_firstname);
		$bill_lastname = $bill_details[count($bill_details)-1];
		 
		$order_desc =  $vnT->lang['product']['order_desc']." (#".$vnT->module['order_code'].")";
		
		// Load the Authorize.net merchant ID
		$merchant_id = $vnT->module['merchantid'];

		// Load the tranaction key
		$transaction_key = $vnT->module['transactionkey'] ;

		// Is Authorize.net setup in test or live mode?
		$test_mode = $vnT->module['testmode'];

		// Load the Authorize.net transaction Type
		$transactionType = $vnT->module['transactiontype'];

		if($vnT->module['testmode'] == 1) {
			$an_url = "https://test.authorize.net/gateway/transact.dll";
			$an_pp_url = "test.authorize.net";
		}
		else {
			$an_url = "https://secure.authorize.net/gateway/transact.dll";
			$an_pp_url = "secure.authorize.net";
		}
 		
		

		// Arrange the data into name/value pairs ready to send
		$an_values = array (
			"x_login"				=> $merchant_id,
			"x_version"				=> "3.1",
			"x_delim_char"			=> "|",
			"x_delim_data"			=> "true",
			"x_url"					=> "false",
			"x_duplicate_window"	=> "28800",
			"x_type"				=> $transactionType,
			"x_method"				=> "CC",
			"x_tran_key"			=> $transaction_key,
			"x_relay_response"		=> "false",
			"x_card_num"			=> $ccnum,
			"x_card_code"			=>	$cccode,
			"x_exp_date"			=> $ccexp,
			'x_invoice_num'			=> $vnT->module['order_code'],
			"x_description"			=> $order_desc,
			"x_amount"				=> $vnT->module['total_price'],
			"x_phone"				=> $info['d_phone'],
			"x_first_name"			=> $info['d_fname'],
			"x_last_name"			=> $info['d_lname'],
			"x_address"				=> $info['d_address'],
			"x_email"				=> $info['d_email'],
			"x_city"				=> $info['d_city'],
			"x_state"				=> $d_state_code,
			"x_zip"					=> $info['d_zipcode'],
			"x_country"				=> $info['d_country'],
			"x_company"				=>  $info['d_company'],
 
			//shipping info
			"x_ship_to_first_name"	=> $info['c_fname'],
			"x_ship_to_last_name"	=> $info['c_lname'],
			"x_ship_to_address"		=> $info['c_address'],
			"x_ship_to_city"		=> $info['c_city'],
			"x_ship_to_state"		=> $c_state_code,
			"x_ship_to_zip"			=> $info['c_zipcode'],
			"x_ship_to_country"		=> $info['c_country'],
			"x_ship_to_company"		=> $info['c_company'],
			"x_ship_to_phone"		=> $info['c_phone'],

			"shop_order_token"		=> $cart->session
		);
		
 		
		//echo "<pre>" ;
		//print_r($an_values);
		//echo "</pre>" ;
		
		// Merge the name/value pairs into a string
		foreach($an_values as $k=>$v) {
			$an_data .= sprintf("%s=%s&", $k, urlencode($v));
		}

		$an_data = rtrim($an_data, '&');

		$an_response = $Authorizenet->ConnectToProvider($an_url, $an_pp_url, $an_data);
		
		//echo "<pre>" ;
		//print_r($an_response);
		//echo "</pre>" ;
		
		if(!$an_response || empty($an_response)) {
			$arr_return['ok'] =0;
			$arr_return['error'] = " ConnectToProvider Error ";
		}
		
		if($an_response[0] == 1) { // 1 is a success, 2 is declined and 3 is an error
			
			
			$dup['status'] = $vnT->module['status_payment'] ;			 
 			$vnT->DB->do_update("order_sum",$dup," order_code='".$vnT->module['order_code']."' ") ;
		 	
			$arr_return['ok'] =1;
			$arr_return['error'] = '';
		}
		else {
			$arr_return['ok'] =0;
			// Status was declined or error, show the response message as an error
			if($an_response[2] == 11) {				
				$arr_return['error'] = ' Status was declined or error ';
			}
			else {
				$arr_return['error'] = $an_response[3] ;
			}

			if($an_response[0] == 2) {
 				// Authorize.net has declined 
				$arr_return['error'] = $an_response[3] ;
   			 
			}
			else {
				// Authorize.net returned an invalid
				$arr_return['error']  = $an_response[3] ;
			}
			 
		}
		 
	}
	else {		 
		$arr_return['error'] = " session Cart not found";
	} 
 	
	return $arr_return ;
	
}


function formAction ()
{
  global $vnT, $conf, $input;
 	 
	if ($vnT->module['testmode'] == 1) {
		$link_action = "https://test.authorize.net/gateway/transact.dll";		 
	} else {
		$link_action = "https://secure.authorize.net/gateway/transact.dll"; 
	} 
  return $link_action ;
}

function formPayment ($info)
{
  global $vnT, $conf, $input;
 	
	$data['ccname'] = ($input['ccname']) ? $input['ccname'] : $info['d_name'];
	
	$list_ccexpm = '<select id="ccexpm" name="ccexpm" size=1 class="select">' ;
	$list_ccexpm .= '<option  value="0" >&nbsp;</option>' ;
	
	for($i = 1; $i <= 12; $i++) {
		$stamp = mktime(0, 0, 0, $i, 15, date("Y"));

		$i = str_pad($i, 2, "0", STR_PAD_LEFT);

		if (@$_POST['ccexpm'] == $i) {
			$sel = 'selected="selected"';
		} else {
			$sel = "";
		}

		$list_ccexpm .= sprintf("<option %s value='%s'>%s</option>", $sel, $i, date("M", $stamp));
	}
	$list_ccexpm .= '</select>' ;
	$data['list_ccexpm'] = $list_ccexpm;
	
	$list_ccexpy = '<select id="ccexpy" name="ccexpy" size=1 class="select">' ;
	$list_ccexpy .= '<option  value="0" >&nbsp;</option>' ;
	for($i = date("Y"); $i < date("Y")+10; $i++) {
		if (@$_POST['ccexpy'] == substr($i, 2, 2)) {
			$sel = 'selected="selected"';
		} else {
			$sel = "";
		}
		$list_ccexpy .= sprintf("<option %s value='%s'>%s</option>", $sel, substr($i, 2, 2), $i);
	}
	$list_ccexpy .= '</select>' ;
	
	$data['list_ccexpy'] = $list_ccexpy;
	$text = load_html("formPayment",$data);
  return $text ;
}

///////////////////////////
// Other Vars
////////
 
$is_online = 1;
$is_insert = "yes"

?>