<?php
 
class Authorizenet
{
  // URL chheckout 
  var $merchantid;   
  var $transactionkey;    
  var $transactiontype;  

  function Authorizenet ()
  {
    //set up some defaults
    $this->merchantid = $vnT->module['merchantid'];
    $this->transactionkey = $vnT->module['transactionkey'];
    $this->transactiontype = $vnT->module['transactiontype'];
  }
	
	
  function ConnectToProvider($an_url, $an_pp_url, $an_data)
	{
		global $vnT, $func, $DB, $input, $cart,  $conf;
		$an_response = array();
 			
		// Use Authorize.net's API to charge the credit card
		if(function_exists("curl_exec")) {
			// Use CURL if it's available
			$ch = @curl_init($an_url);
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $an_data);
			curl_setopt($ch, CURLOPT_TIMEOUT, 60);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	
			// Setup the proxy settings if there are any
			if ($vnT->conf['HTTPProxyServer']) {
				curl_setopt($ch, CURLOPT_PROXY, $vnT->conf['HTTPProxyServer']);
				if ($vnT->conf['HTTPProxyPort']) {
					curl_setopt($ch, CURLOPT_PROXYPORT, $vnT->conf['HTTPProxyPort']);
				}
			}
	
			//if ($vnT->conf['HTTPSSLVerifyPeer'] == 0) {
				curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			//}
	
			$result = curl_exec($ch);
	
			if(curl_error($ch) != '') {
				$vnT->setting['error'] = "Bao loi : ".curl_errno($ch) ; 
				die($vnT->setting['error']);
				return false;
			}
		}
		else if(function_exists("fsockopen")) {
			$header = "";
			$header .= "POST " . $an_url . " HTTP/1.0\r\n";
			$header .= "Content-Type: application/x-www-form-urlencoded\r\n";
			$header .= "Content-Length: " . strlen($an_data) . "\r\n\r\n";
	
			if($fp = @fsockopen("ssl://" . $an_pp_url, 443, $errno, $errstr, 30)) {
				@fputs($fp, $header . $an_data);
	
				// Read the body data
				$result = "";
				$headerdone = false;
	
				while(!@feof($fp)) {
					$line = @fgets($fp, 1024);
	
					if(@strcmp($line, "\r\n") == 0) {
						// Read the header
						$headerdone = true;
					}
					else if($headerdone) {
						// Header has been read, read the contents
						$result .= $line;
					}
				}
			}
			else {
				$vnT->setting['error'] = "fsockopen error";
				die($vnT->setting['error']);
				return false;
			}
		}
		else {
			$vnT->setting['error'] = "AuthorizeNetNotSupported error";
			die($vnT->setting['error']);
			return false; 
		}
	
		// Check to see the we got a response
		if ($result == "") {
			$vnT->setting['error'] = "AuthorizeNetNoResult error";
			die($vnT->setting['error']);
			 
			return false;
		}
	
		$an_response = explode("|", $result);
		for ($i=0; $i<count($an_response); $i++) {
			$an_response[$i] = trim($an_response[$i], '"');
		} 
		return $an_response;
	}
}
?>