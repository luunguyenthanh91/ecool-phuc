<?php
	
	$file_smartlink = "modules/product/payment/smartlink/Payment.php";
	if(!file_exists($file_smartlink)) die("Không tìm thấy File cấu hình của Smart Link ");
	include ($file_smartlink);
	
	function fixedVars ($info)
	{
		global $vnT, $conf, $input;
		$hiddenVars = "";
		return $hiddenVars;
	}
	
	function formAction($info)
	{
		global $vnT, $conf,$input;
		
		$payment = new Payment();
		$payment->setSecureSecret($info['vpc_SecureHash']);
		$payment->setVirtualPaymentUrl($info['PaymentUrl']);
		/*echo "<pre>";
		print_r($info);
		echo "</pre>";		*/
		$vpc_Amount = $info['vpc_Amount']*100 ;
		$_params['vpc_Version'] = $info['vpc_Version'] ;
		$_params['vpc_Command'] = $info['vpc_Command'] ;
		$_params['vpc_AccessCode'] = $info['vpc_AccessCode'] ;
		$_params['vpc_MerchTxnRef'] = $info['vpc_MerchTxnRef'] ;
		$_params['vpc_Merchant'] = $info['vpc_Merchant'] ;
		$_params['vpc_OrderInfo'] = $info['vpc_OrderInfo'] ;
		$_params['vpc_Amount'] = $vpc_Amount ;
		$_params['vpc_Locale'] = $info['vpc_Locale'] ;
		$_params['vpc_Currency'] = $info['vpc_Currency'] ;		
		$_params['vpc_ReturnURL'] = $info['vpc_ReturnURL'] ;
		$_params['vpc_BackURL'] = $info['vpc_BackURL'] ; 
		$payment->redirect($_params);
 	 	die();
	}
	
	function formPayment ($info)
	{
		global $vnT, $conf, $input;
		$type_pay =($input['type_pay']) ? $input['type_pay'] : 0;
		$list_type_pay = vnT_HTML::list_radio("type_pay", array('0' => 'Thanh toán thẻ Nội Địa' , '1' => 'Thanh toán thẻ Quốc Tế' ), $type_pay); 
		$text = '<table width="100%" cellspacing="2" cellpadding="2" border="0"  >		
    	 <tr>
        <td  ><strong>Chọn loại thanh toán :</strong></td>
        <td >'.$list_type_pay.'</td>
      </tr> </table><br>';
			
		return $text ;
	}
	//========
	function ProcessPaymentForm($info)
	{
		global $vnT, $DB, $func, $conf, $input,  $cart;
 		 
 		$cart_order_id =  base64_encode($vnT->module['order_code']); 		
		$vpc_ReturnURL = $conf['rooturl']."san-pham/checkout_finished.html/?order_code=" . $cart_order_id ; 
		$vpc_BackURL =  $conf['rooturl']."san-pham/checkout_cancel.html/?order_code=". $cart_order_id ; 
		$vpc_OrderInfo =  $vnT->module['order_code'] ;
		$price = $vnT->module['total_price'] ;
		
		if($input['type_pay']==1){
			$info_payment['vpc_Version'] = $vnT->module['vpc_VersionQT'] ;
			$info_payment['vpc_Command'] = $vnT->module['vpc_CommandQT'] ;
			$info_payment['vpc_AccessCode'] = $vnT->module['vpc_AccessCodeQT'] ;
			$info_payment['vpc_MerchTxnRef'] = $vnT->module['order_code'] ;
			$info_payment['vpc_Merchant'] = $vnT->module['vpc_MerchantQT'] ;
			$info_payment['vpc_OrderInfo'] = $vpc_OrderInfo;
			$info_payment['vpc_Amount'] = $price ;
			$info_payment['vpc_Locale'] = $vnT->module['vpc_LocaleQT'] ;
			$info_payment['vpc_Currency'] = $vnT->module['vpc_CurrencyQT'] ;
			$info_payment['vpc_ReturnURL'] = $vpc_ReturnURL ;
			$info_payment['vpc_BackURL'] = $vpc_BackURL ; 	
			
			$info_payment['vpc_SecureHash'] = $vnT->module['vpc_SecureHashQT'] ;
			$info_payment['PaymentUrl'] = $vnT->module['PaymentUrlQT'] ;
			
		}else{
			$info_payment['vpc_Version'] = $vnT->module['vpc_Version'] ;
			$info_payment['vpc_Command'] = $vnT->module['vpc_Command'] ;
			$info_payment['vpc_AccessCode'] = $vnT->module['vpc_AccessCode'] ;
			$info_payment['vpc_MerchTxnRef'] = $vnT->module['order_code'] ;
			$info_payment['vpc_Merchant'] = $vnT->module['vpc_Merchant'] ;
			$info_payment['vpc_OrderInfo'] = $vpc_OrderInfo;
			$info_payment['vpc_Amount'] = $price ;
			$info_payment['vpc_Locale'] = $vnT->module['vpc_Locale'] ;
			$info_payment['vpc_Currency'] = $vnT->module['vpc_Currency'] ;							
			$info_payment['vpc_ReturnURL'] = $vpc_ReturnURL ;
			$info_payment['vpc_BackURL'] = $vpc_BackURL ; 	
			
			$info_payment['vpc_SecureHash'] = $vnT->module['vpc_SecureHash'] ;
			$info_payment['PaymentUrl'] = $vnT->module['PaymentUrl'] ;
		} 
		
		return $info_payment ;
		
	}
	 

?>