<?php
/*
 * This page is called from Mobivi to notify transaction's status changes.
 * In your website, it is used to process invoice update. In here, we do:
 *
 * 1. Import Mobivi Checkout class
 * 2. Create Mobivi Notification object and read Mobivi public key
 * 3. Parse the notification from Mobivi
 * 4. Process invoice update based on notification (transaction status changes)
 *
 * You can not print out debug directly on this page, because it is called by
 * Mobivi and you will not see what you print. But you can print debug to a
 * local file, in this sample, we will print it to php error log.
 *
 */

// 1. Import Mobivi Checkout class
include('clsMobiviCheckout.php');

// 2. Create Mobivi Notification object and read Mobivi public key
$mbvNotify = new MobiviNotification();
$mbvNotify->read_verify_cert("keys/mobivi.crt");

// 3. Parse the notification from Mobivi
$mbvMessage = $mbvNotify->parse($_POST);
$mbvNotify->dispose();

// 4. Process invoice update based on notification (transaction status changes)
if ($mbvMessage == NULL) {
	// Null message?
	error_log("The message was invalid.");

} elseif ($mbvMessage instanceof MobiviNewOrder) {
	// A new transaction is created on Mobivi system for your invoice.
	// Get the transaction information and attach it with your invoice.
	// In this sample, we just print out the messeage.
	$mbvRequest = $mbvMessage->CheckoutRequest;
	error_log("NewOrderNotification:");
	error_log("    SerialID: " . $mbvMessage->SerialID);
	error_log("    TransactionID: " . $mbvMessage->TransactionID);
	error_log("    State: " . $mbvMessage->State);
	error_log("    Invoice:");
	error_log("        InvoiceID: " . $mbvRequest->InvoiceID);
	error_log("        From: " . $mbvRequest->InvoiceFrom);
	error_log("        To: " . $mbvRequest->InvoiceTo);
	error_log("        Description: " . $mbvRequest->InvoiceDescription);
	error_log("        Amount: " . $mbvRequest->InvoiceAmount);
	error_log("        Tax: " . $mbvRequest->InvoiceTax);
	error_log("        Items: " . count($mbvRequest->InvoiceItems) . " item(s)");
	foreach ($mbvRequest->InvoiceItems as $item) {
		error_log("            " . $item["ItemID"] . ". " . $item["Name"] . " <" . $item["Description"] . ">: " . $item["UnitPrice"] . "x" . $item["Quantity"] . " Taxable: " . $item["Taxable"]);
	}

} elseif ($mbvMessage instanceof MobiviTransactionStateChange) {
	// Transaction status changed.
	// Get the change information and update your invoice.
	// In this sample, we just print out the messeage.
	error_log("TransactionStateChangeNotification:");
	error_log("    SerialID: " . $mbvMessage->SerialID);
	error_log("    TransactionID: " . $mbvMessage->TransactionID);
	error_log("    State: " . $mbvMessage->State);

} else {
	// Something wrong?
	error_log("Unexpected type, the bug is somewhere.");
	error_log($mbvMessage);

}
?>
