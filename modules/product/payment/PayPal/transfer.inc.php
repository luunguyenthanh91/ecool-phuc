<?php

	
function fixedVars ($info)
{
	global $vnT, $conf, $input;
	$hiddenVars = "";
	return $hiddenVars;
}

function formAction($info)
{
	global $vnT, $DB, $func, $conf, $input,  $cart , $API_Endpoint,$version,$API_UserName,$API_Password,$API_Signature,$nvp_Header, $subject, $AUTH_token,$AUTH_signature,$AUTH_timestamp;
	
	$file_paypal = $vnT->conf['rootpath']."modules/product/payment/paypal/CallerService.php";
	if (! file_exists($file_paypal)) die("Not found File Paypal ");
	include ($file_paypal);
	
	$cart_order_id = base64_encode($vnT->module['order_code']);
	
	/* The servername and serverport tells PayPal where the buyer
	 should be directed back to after authorizing payment.
	 In this case, its the local webserver that is running this script
	 Using the servername and serverport, the return URL is the first
	 portion of the URL that buyers will return to after authorizing payment
	 */
 		
 	 $currencyCode="USD"; 
	 $paymentType = ($vnT->module['paymentType']) ? $vnT->module['paymentType'] : "Sale";
	 $total_price = ($vnT->setting['currency']=="VND") ? get_price_pro_en ($vnT->module['total_price']) : sprintf( "%.2f", $vnT->module['total_price'] ) ;
 		
	 $personName        = $info['c_name'];
	 $SHIPTOSTREET      = $info['c_address'];
	 $SHIPTOCITY        = $info['c_city'];
	 $SHIPTOSTATE	      = $info['c_state'];
	 $SHIPTOCOUNTRYCODE = $info['c_country'];
	 $SHIPTOZIP         = $info['c_zip'];
	 
	 $L_NAME0           = " Order #".$vnT->module['order_code']." ";
	 $L_AMT0            =  $total_price ;  
	 $L_QTY0            =	1;
	  



 /* The returnURL is the location where buyers return when a
	payment has been succesfully authorized.
	The cancelURL is the location buyers are sent to when they hit the
	cancel button during authorization of payment during the PayPal flow
	*/
	if ($vnT->func->is_muti_lang()) {
		 $root_link = $conf['rooturl'] .$vnT->lang_name."/";
	}else{
		 $root_link = $conf['rooturl'] ;
	}
	
	 $returnURL = $root_link.$vnT->setting['seo_name'][$vnT->lang_name]['product']."/checkout_finished.html/?order_code=" . $cart_order_id;
   $cancelURL = $root_link.$vnT->setting['seo_name'][$vnT->lang_name]['product']."/checkout_cancel.html/?order_code=" . $cart_order_id;
 
 
 /* Construct the parameter string that describes the PayPal payment
	the varialbes were set in the web form, and the resulting string
	is stored in $nvpstr
	*/
	 
 	 
	 $itemamt = 0.00;
	 $itemamt = $L_QTY0*$L_AMT0;
	 $amt = $itemamt;
	 
	 $nvpstr="";
	 /*
		* Setting up the Shipping address details
		*/
	 $shiptoAddress = "&SHIPTONAME=$personName&SHIPTOSTREET=$SHIPTOSTREET&SHIPTOCITY=$SHIPTOCITY&SHIPTOSTATE=$SHIPTOSTATE&SHIPTOCOUNTRYCODE=$SHIPTOCOUNTRYCODE&SHIPTOZIP=$SHIPTOZIP";
	 
	 
	 $nvpstr="&ADDRESSOVERRIDE=1".$shiptoAddress."&L_NAME0=".$L_NAME0."&L_AMT0=".$L_AMT0."&L_QTY0=".$L_QTY0."&AMT=".(string)$amt."&ReturnUrl=".$returnURL."&CANCELURL=".$cancelURL ."&CURRENCYCODE=".$currencyCodeType."&PAYMENTACTION=".$paymentType;
 			 
	/* Make the call to PayPal to set the Express Checkout token
	If the API call succeded, then redirect the buyer to PayPal
	to begin to authorize payment.  If an error occured, show the
	resulting errors
	*/
	
	//echo "nvpstr = ".$nvpstr;
	 $resArray=hash_call("SetExpressCheckout",$nvpstr);
	 $_SESSION['reshash']=$resArray;
	
	 /*echo "<pre>"	;
	 print_r($resArray);
	 echo "</pre>"	;
	 die();
	 */
	 $ack = strtoupper($resArray["ACK"]);

	 if($ack=="SUCCESS"){
			// Redirect to paypal.com here
	$token = urldecode($resArray["TOKEN"]);
	$payPalURL = PAYPAL_URL.$token;
	header("Location: ".$payPalURL);
	} else  {
	 //Redirecting to APIError.php to display errors.
		$location = $vnT->conf['rooturl']."modules/product/payment/paypal/APIError.php";
		header("Location: $location");
	} 

	die();
	//return $formAction;			
}
	 

 
$is_online = 1;
$is_insert = "yes"

?>