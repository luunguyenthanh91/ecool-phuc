<?php
/*
 * This is the start page of this sample. In your website, it maybe your
 * shopping cart checkout page. In here, we do:
 *
 * 1. Import Mobivi Checkout class
 * 2. Create Mobivi Checkout object and read merchant private key
 * 3. Fill the request with your cart information (the amount customer needs to
 *    pay, cart items, ReturnURL, DoneURL, etc.) then send request
 * 4. Redirect customer to Mobivi Checkout page (URL in request's response)
 *
 * Then, customer will login to his/her ewallet and confirm the transaction.
 * He/she will be redirected back to merchant's site (the DoneURL in request).
 * DoneURL is only a 'Thank You for Purchase' page. Do not process any invoice
 * update on DoneURL page, we will do that in listener.php.
 *
 */

// 1. Import Mobivi Checkout class
include('clsMobiviCheckout.php');

// 2. Create Mobivi Checkout object and read merchant private key
$mbv = new MobiviCheckout("https://sandbox.mobivi.com/checkout");
$read_key_result = $mbv->read_private_key("keys/merchant_pri.pem");

// 3. Fill the request with your cart information (the amount customer needs to
// pay, cart items, etc.) then send request
$unitprice = 1000;
$quantity = 1;
$tax = 10;
$mbvRequest = new MobiviCheckoutRequest();
$mbvRequest->SerialID = uniqid();
$mbvRequest->From = "Beesnext";
$mbvRequest->To = "vnTRUST";
$mbvRequest->Description = "Your invoice...";
$mbvRequest->Amount = $unitprice * $quantity * (($tax + 100) / 100);
$mbvRequest->Tax = $tax;
$mbvRequest->ReturnURL = "http://localhost/bee_next_v2/";
$mbvRequest->DoneURL = "http://localhost/bee_next_v2/MobiviCheckout/listener.php";
$mbvRequest->add_invoice_item(html_entity_decode("iPhone 3G[S]", ENT_NOQUOTES, "UTF-8"), $unitprice, $quantity, true, 1, html_entity_decode("", ENT_NOQUOTES, "UTF-8"));
$send = $mbv->send("beesnext", $mbvRequest, $redirect_url);
$mbv->dispose();

 
// 4. Redirect customer to Mobivi Checkout page (URL in request's response)
if ($send) {
	header("Location: $redirect_url");
	exit();
} else {
	echo "aaaaaaaaa";
	echo $redirect_url;
}
?>
