<?php
if (empty($_SESSION['session_cart']))  //make sure this hasn't already been established
{
  $session_cart = md5(uniqid(rand()));   //creates a random session value
  $_SESSION['session_cart'] = $session_cart;
}

// start Cart class
class Cart
{
  var $session = "";

  //xoa cat item cua
  function Cart(){
    global $conf, $func, $input, $DB, $vnT;
    $this->session = $_SESSION['session_cart'];
    $expired_time = time() - (2 * 24 * 60 * 60);
    $DB->query("DELETE FROM order_shopping WHERE timeadd<='$expired_time' ");
  }
  // add item to shopping database
  function add_item($session, $item_type = "product", $item_id, $quantity, $data = array()){
    global $conf, $func, $input, $DB, $vnT;
    $err = "";
    $color = (int)$data['color'];
    $size = (int)$data['size'];
    $where_ck = "";
    if ($color) {
      $where_ck .= " AND color=$color";
    }
    if ($size) {
      $where_ck .= " AND size=$size";
    }
    $in_list = "SELECT * FROM order_shopping 
                WHERE session='$session' AND item_type='{$item_type}' AND item_id='$item_id' {$where_ck} ";
    $result = $DB->query($in_list);
    if ($row = $DB->fetch_row($result)){
      $quantity = $quantity + $row["quantity"];
      $dup['quantity'] = $quantity;
      $dup['timeadd'] = time();
      $DB->do_update("order_shopping", $dup, " session='$session' AND item_type='{$item_type}' AND item_id='$item_id' {$where_ck} ");
    } else {
      $cot['session'] = $session;
      $cot['item_type'] = $item_type;
      $cot['item_id'] = $item_id;
      $cot['quantity'] = $quantity;
      $cot['price'] = get_price_item(0,$item_id);
      $cot['date'] = date("Ymd");
      $cot['color'] = $color;
      $cot['size'] = $size;
      $cot['timeadd'] = time();
      $DB->do_insert("order_shopping", $cot);
    }
    return $err;
  }
  function add_item_sub($session,$item_type = "product",$item_id,$subid, $quantity, $data = array()){
    global $conf, $func, $input, $DB, $vnT;
    $err = "";
    $color = (int) $data['color'];
    $size = (int) $data['size'];
    $where_ck = "";
    if ($color) {
      $where_ck .= " AND color=$color";
    }
    if ($size) {
      $where_ck .= " AND size=$size";
    }
    $in_list = "SELECT * FROM order_shopping 
                WHERE session='$session' AND item_type='{$item_type}' 
                AND item_id='$item_id' AND subid = {$subid} {$where_ck} ";
    $result = $DB->query($in_list);
    if ($row = $DB->fetch_row($result)) {
      $quantity = $quantity + $row["quantity"];
      $dup['quantity'] = $quantity;
      $dup['timeadd'] = time();
      $whereup = " session='$session' AND subid='{$subid}' AND item_id='$item_id' {$where_ck} ";
      $DB->do_update("order_shopping", $dup, $whereup);
    } else {
      $cot['session'] = $session;
      $cot['item_type'] = $item_type;
      $cot['item_id'] = $item_id;
      $cot['subid'] = $subid;
      $cot['quantity'] = $quantity;
      $cot['price'] = get_price_item(1,$item_id,$subid);
      $cot['date'] = date("Ymd");
      //$cot['color'] = $color;
      //$cot['size'] = $size;
      $cot['timeadd'] = time();
      $DB->do_insert("order_shopping", $cot);
    }
    return $err;
  }
  // delete a specific item
  function delete_item($session, $id){
    global $conf, $func, $input, $DB, $vnT;
    $DB->query("DELETE FROM order_shopping WHERE session='$session' AND id=$id  ");
  }
  // modifies a quantity of an item
  function modify_quantity($session, $id, $quantity, $data = array()){
    global $conf, $func, $input, $DB, $vnT;
    $dup ['quantity'] = $quantity;
    if ($data['color']) {
      $dup ['color'] = $data['color'];
    }
    if ($data['size']) {
      $dup ['size'] = $data['size'];
    }
    $ok = $DB->do_update("order_shopping", $dup, " session='$session' AND id=$id ");
  }
  // clear all content in their cart
  function clear_cart($session) {
    global $conf, $func, $input, $DB, $vnT;
    $DB->query("DELETE FROM order_shopping WHERE session='$session'");
  }
  //add up the shopping cart total
  function cart_total($session){
    global $conf, $func, $input, $DB, $vnT;
    $total = 0;
    $result = $DB->query("SELECT * FROM order_shopping  WHERE session='$session'");
    if ($DB->num_rows($result) > 0) {
      while ($row = $DB->fetch_row($result)) {
        $total = $total + ($row['price'] * $row["quantity"]);
      } //while
    }// if
    return $total;
  }
  // function to display contents
  function display_contents($session){
    global $conf, $func, $input, $DB, $vnT;
    $arr_size = array();
    $res_size = $vnT->DB->query("SELECT size_id,title FROM product_size ");
    while ($row_size = $vnT->DB->fetch_row($res_size)) {
      $arr_size[$row_size['size_id']] = $row_size;
    }
    $count = 0;
    $total = 0;
    $sql = "SELECT * FROM order_shopping 
						WHERE session='$session' ORDER BY id DESC ";
    $result = $DB->query($sql);
    while ($row = $DB->fetch_row($result)) {
      $sql_item= "SELECT p.* , pd.p_name  , pd.friendly_url FROM  products p, products_desc pd
        					WHERE p.p_id=pd.p_id AND display=1 AND lang='$vnT->lang_name' AND p.p_id=" . $row['item_id'];
      $res_item = $DB->query($sql_item);
      $row_item = $DB->fetch_row($res_item);
      $subid = (int) $row['subid'];
      if($subid){
        $rs_sub = $DB->query("SELECT * FROM products_sub p, products_sub_desc pd
                              WHERE p.subid = pd.subid AND lang='$vnT->lang_name' AND display = 1
                              AND p.subid = $subid");
        $r_sub = $DB->fetch_row($rs_sub);
      }
      $price = ($subid) ? $r_sub['price'] : $row_item['price'];
      $contents["item_maso"][$count] = ($subid) ? $r_sub["maso"] : $row_item['maso'];
      $contents["item_picture"][$count] = ($subid) ? "product/".$r_sub["picture"] : "product/".$row_item["picture"];
      $contents["item_title"][$count] = $row_item['p_name'];
      $contents["item_link"][$count] = $row_item['friendly_url'].".html";
      $contents["item_price"][$count] = $price;
      //$contents["price_old"][$count] = $row_item['price_old'];
      //more
      //$contents["catRoot"][$count] = $row_item["catRoot"];
      //$contents["cat_id"][$count] = $row_item["cat_id"];
      $contents["p_id"][$count] = $row_item["p_id"];

      $contents["id"][$count] = $row["id"];
      $contents["item_type"][$count] = $row["item_type"];
      $contents["item_id"][$count] = $row["item_id"];
      $contents["subid"][$count] = $row["subid"];
      $contents["color"][$count] = $row['color'];
      $contents["size"][$count] = $row['size'];
      $contents["price"][$count] = $price;
      $contents["quantity"][$count] = $row["quantity"];
      $contents["total"][$count] = ($price * $row["quantity"]);
      $total += $contents["total"][$count];
      $count++;

    }
    $contents["final"] = $total;
    return $contents;
  }
  // count no items
  function num_items($session){
    global $conf, $func, $input, $DB, $vnT;
    $result = $DB->query("SELECT id FROM order_shopping WHERE session='$session'");
    $num_rows = $DB->num_rows($result);
    return $num_rows;
  }
  // count quantity item
  function num_quantity($session){
    global $conf, $func, $input, $DB, $vnT;
    $num = 0;
    $result = $DB->query("SELECT quantity FROM order_shopping WHERE session='$session'");
    while ($row = $DB->fetch_row($result)) {
      $num += $row['quantity'];
    }
    return $num;
  }

  function check_cart($session){
    global $conf, $func, $input, $DB, $vnT;
    $arr_out = array();
    $result = $DB->query("SELECT * FROM order_shopping WHERE session='$session'");
    $has_err = 0;
    if ($DB->num_rows($result) > 0) {
      while ($row = $DB->fetch_row($result)) {
        $onHand = check_onhand($row['item_id'],$row['subid']);
        if($onHand == 0){
          $has_err++;
          $p_name = get_p_name($row['item_id']);
          $err .= '<p>'.str_replace('{p_name}',$p_name,$vnT->lang['product']['mess_out_stock']).'</p>';
          $this->delete_item($session,$row['id']);
        }else{
          if($onHand < $row['quantity']){
            $has_err++;
            $p_name = get_p_name($row['item_id']);
            $err .= '<p>'.str_replace(array('{p_name}','{onhand}'),array($p_name,$onHand),$vnT->lang['product']['mess_not_enough']).'</p>';
            $this->modify_quantity($session,$row['id'],$onHand);
          }
        }
      }
    }
    $arr_out['has_err'] = $has_err;
    $arr_out['err'] = $err;
    return $arr_out;
  }
}
?>