$(document).ready(function(){
  $(".boxRadio label").click(function(){
    $(this).parents(".boxRadio").siblings().removeClass("active");
    $(this).parents(".boxRadio").addClass("active");
  });
	$(".choose-quantity .quantity").keydown(function(e){
    checknuber(e);
    if (e.keyCode == 38){
        var $value = $(this).val();
        if( $value == ""){
            $(this).val(1);
        }
        $value = parseInt($value) + 1;
        $(this).val($value);
    }
    if (e.keyCode == 40){
        var $value = parseInt($(this).val());
        if( $value == ""){
            $(this).val(1);
        }
        if(parseInt($value) > 1)
        {
            $value = parseInt($value) - 1;
            $(this).val($value);
        }
    }
  });
  $(".choose-quantity .q-prev").click(function(){
    var $value = parseInt($(this).parents(".choose-quantity").find(".quantity").val());
    if(parseInt($value) > 1){
      $value = parseInt($value) - 1;
      $(this).parents(".choose-quantity").find(".quantity").val($value);
    }
    vnTProduct.updateQuantity($(this).attr('data-qid'));
  });
  $(".choose-quantity .q-next").click(function(){
      var $value = parseInt($(this).parents(".choose-quantity").find(".quantity").val());
      $value = parseInt($value) + 1;
      $(this).parents(".choose-quantity").find(".quantity").val($value);
      vnTProduct.updateQuantity($(this).attr('data-qid'));
  });
  $(".choose-quantity .quantity").blur(function(){
    var $value = $(this).val();
    if( $value == ""){
      $(this).val(1);
    }
  });
  function checknuber(e){
    if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
      // Allow: Ctrl+A, Command+A
      (e.keyCode == 65 && ( e.ctrlKey === true || e.metaKey === true ) ) ||
      // Allow: home, end, left, right, down, up
      (e.keyCode >= 35 && e.keyCode < 40)) {
      // let it happen, don't do anything
      return;
    }
    // Ensure that it is a number and stop the keypress
    if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
      e.preventDefault();
    }
  };
  $(".cartField .optionCart").click(function(){
    if(!$(this).hasClass("active")){
      $(this).addClass("active");
      $(this).find("input").prop('checked', true);
    }
    else{
      $(this).removeClass("active");
      $(this).find("input").prop('checked', false);
    }
  });
  // CHECKBOX

  $("#dcGiaoOption").click(function(){
    if($(this).hasClass("active")){
      $("#dcGiao").stop().slideDown(300);
    }
    else{
      $("#dcGiao").stop().slideUp(300);
    }
  });
  $("#dcBillOption").click(function(){
    if($(this).hasClass("active")){
      $("#dcBill").stop().slideDown(300);
    }
    else{
      $("#dcBill").stop().slideUp(300);
    }
  });
});
vnTProduct = {
  changeQuantity :function (obj,type,id_cart){
    var cur_quantity = parseInt( $("#"+obj).val() );
    if( type=="decrease" ) {
      if(cur_quantity>1){
        $("#"+obj).val(cur_quantity-1);
      }
    }else{
      $("#"+obj).val(cur_quantity+1);
    }
    if(id_cart>0){
      vnTProduct.updateQuantity(id_cart);
    }
  },
  updateQuantity :function (id){
    var quantity = parseInt($("#quantity_"+id).val());
    var city = $('#d_city').val();
    var state = $('#d_state').val();
    if( $.isNumeric( quantity ) ) {
      var mydata = 'id='+id+'&quantity='+quantity+'&city='+city+'&state='+state;
      $.ajax({
        async: true,
        dataType: 'json',
        url:  ROOT_MOD+"/ajax/update_quantity.html" ,
        type: 'POST',
        data: mydata,
        success: function (data) {
          $('#quantity_'+id).val(data.quantity);
          $("#ext_total_"+id).html(data.total);
          $("#total_price").html(data.total_price);
        }
      });
    }
  },
  updateSize :function (id){
    var quantity = parseInt( $("#size"+id).val() );
    if( $.isNumeric( quantity ) ) {
      var mydata = 'id='+id+'&quantity='+quantity ;
      $.ajax({
        async: true,
        dataType: 'json',
        url:  ROOT_MOD+"/ajax/update_quantity.html" ,
        type: 'POST',
        data: mydata ,
        success: function (data) {
          $("#ext_total_"+id).html(data.total);
          $("#cart_total").html(data.total_price);
        }
      });
    }
  },
  CaculateShipPrice:function (state=-1){
    var shipping_method = $('input[name=rShipping]:checked').val();
    var payment_method  = $('input[name=rPayment]:checked').val();
    if($("#ck-notSame").attr("checked")){
        var city = $('#c_city').val();
        var state = $('#c_state').val();
    }else{
        var city = $('#d_city').val();
        var state = $('#d_state').val();
    }
    //alert(state);
    var mydata = 'shipping_method='+shipping_method+'&payment_method='+payment_method+'&city='+city+"&state="+state;
    $.ajax({
      async: true,
      dataType: 'json',
      url:  ROOT_MOD+"/ajax/shipping_price.html" ,
      type: 'POST',
      data: mydata ,
      success: function (data) {
        $("#ext_s_price").html(data.s_price);
        $("#ext_total_price").html(data.total_price);
      }
    });
  },
};
function City_Change(mydata) {
  $.ajax({
    async: true,
    url: ROOT+'load_ajax.php?do=member_state',
    type: 'POST',
    data: mydata ,
    success: function (data) {
      $("#ext_state").html(data)
    }
  });
  $.ajax({
    async: true,
    url: ROOT+'load_ajax.php?do=member_ward',
    type: 'POST',
    data: mydata ,
    success: function (data) {
      $("#ext_ward").html(data)
    }
  });
}
function State_Change(mydata) {
  $.ajax({
    async: true,
    url: ROOT+'load_ajax.php?do=member_ward',
    type: 'POST',
    data: mydata ,
    success: function (data) {
      $("#ext_ward").html(data)
    }
  });
}