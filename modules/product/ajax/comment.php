<?php
if(empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) != 'xmlhttprequest') {
	die('Hacking attempt!');
}
@ini_set("display_errors", "0");

	session_start();
	define('IN_vnT', 1);
	define('PATH_ROOT', dirname(__FILE__));
	define('DS', DIRECTORY_SEPARATOR); 
	
	require_once("../../../_config.php"); 
	require_once ($conf['rootpath'] . 'includes' . DS . 'defines.php');
	require_once($conf['rootpath']."includes/class_db.php"); 
	$vnT->DB= $DB = new DB;
	require_once ($conf['rootpath']."includes/class_functions.php"); 
	$vnT->func = $func = new Func_Global();
	$vnT->conf = $conf = $func->fetchDbConfig($conf);
	require "../../../includes/JSON.php";
	
	$vnT->user = array();
	$vnT->user['mem_id'] =0;
	$arr_cookie_member = explode("|",$_COOKIE[MEMBER_COOKIE]);
	$res_mem = $DB->query("SELECT mem_id,password,username,avatar,email,full_name,phone,address FROM members WHERE username='".$arr_cookie_member[0]."' ");
	if($row_mem = $DB->fetch_row($res_mem)){ 
		$mem_hash = md5($row_mem['mem_id'] . '|' . $row_mem['password']);  	
		if($arr_cookie_member[1] ==$mem_hash )	{
			$vnT->user  = $row_mem  ; 
		}		
	}
	$vnT->lang_name = (isset($_POST['lang'])) ? $_POST['lang']  : "vn" ;
	$func->load_language('product');
	switch ($_GET['do']){
 		case "votes" : $jsout = do_votes() ;break; 
		case "post" : $jsout = post_comment() ;break; 
		case "del" : $jsout = del_comment() ;break; 
		case "list" : $jsout = list_comment() ;break; 
		case "show_post_reply" : $jsout = show_post_reply() ; break; 
		case "post_reply" : $jsout = post_reply () ; break; 
		
		default : $jsout ="Error"; break;
	}
	function list_comment(){
		global $DB,$func,$conf,$vnT;
		$p = ((int)$_POST['p']) ? $_POST['p'] : 1 ;
		$id= (int)$_POST['id'];
		$res = $DB->query("select n_comment from product_setting where lang='{$vnT->lang_name}' ");
		$r = $DB->fetch_row($res);
		$n = ($r['n_comment']) ? $r['n_comment'] : 5 ;
		$sql_num = "select * from product_comment where id=$id and display=1 and parentid=0 ";
		$result_num = $DB->query($sql_num); 
		$totals = $DB->num_rows($result_num);
		$num_pages = ceil($totals/$n);
		if ($p > $num_pages) $p=$num_pages;
		if ($p < 1 ) $p=1;
		$start = ($p-1) * $n ; 
		$object = "vnTcomment.show_comment({$id},'{$vnT->lang_name}',";
	  	if($num_pages>1)
			$nav = "<div class=\"pagination\">".$func->paginate_js($totals, $n, $p, $object)."</div>" ;
		$sql="SELECT * FROM product_comment WHERE id=$id AND display=1 AND parentid=0 
					ORDER BY cid DESC LIMIT $start,$n";
		$result=$DB->query ($sql);
		if ($num=$DB->num_rows($result)){ 
			$i=0;
			$text = '';
			while ($row=$DB->fetch_row($result)){
				$cid = $row['cid'];
				//load reply
 				$text_reply = '';
 				$res_sub = $vnT->DB->query("SELECT * FROM product_comment WHERE parentid=".$cid." AND display=1 ORDER BY date_post DESC ");
 				if($num_sub = $vnT->DB->num_rows($res_sub) ){
 					while ($row_sub = $vnT->DB->fetch_row($res_sub)){
 						//$sub_avatar = $vnT->lib->get_member_avatar($row_sub['mem_id']);
 						$sub_src_avatar = ($sub_avatar) ? $vnT->conf['rooturl'].'vnt_upload/member/avatar/'.$sub_avatar : $vnT->conf['rooturl'].'modules/product/images/avatar.jpg';
						$sub_date_post = $vnT->lang['product']['post_at'].' '.@date("H:i - d/m/Y",$row_sub['date_post']);
						$sub_email = ($row_sub['hidden_email']==0) ? '&nbsp;<span class="email">('.$row_sub['email'].')</span>' : "";
						$sub_name = $vnT->func->HTML($row_sub['name']);
						$sub_content =  $vnT->func->HTML($row_sub['content']);
						$text_reply .= '<div class="nodeanswer">';
						$text_reply .= '<div class="avatar"><img src="'.$sub_src_avatar.'" alt="'.$row_sub['name'].'" /></div>';
						$text_reply .= '<div class="info-comment">';
						$text_reply .= '<div class="info-preson"><span class="name">'.$sub_name.'</span>'.$sub_email.' - <span class="time">'.$sub_date_post.'</span></div>';
						$text_reply .= '<div class="ccomment">'.$sub_content.'</div>';
						$text_reply .= '</div>';
						$text_reply .= '</div>';
 					}
 				}
 				//$c_avatar = $vnT->lib->get_member_avatar($row['mem_id']);
 				$src_avatar = ($c_avatar) ? $vnT->conf['rooturl'].'vnt_upload/member/avatar/'.$c_avatar : $vnT->conf['rooturl'].'modules/product/images/avatar.jpg';
				$date_post = $vnT->lang['product']['post_at'].' '.@date("H:i - d/m/Y",$row['date_post']) ;
				$email = ($row['hidden_email']==0) ? '&nbsp;<span class="email">('.$row['email'].')</span>' : "";
				$name = $vnT->func->HTML($row['name']);
				$title = '<span class="ctitle">'.	$vnT->func->HTML($row['title']).'</span>' ;
				$content =  $vnT->func->HTML($row['content']) ;
				$text .='<div  id="com'.$cid.'" class="node-commnet">
									<div class="avatar"><img src="'.$src_avatar.'" /></div>
									<div class="info-comment">
										<div class="info-preson">
											<span class="name">'.$name.'</span>'.$email.' -  <span class="time">'.$date_post .'</span>
										</div>
										<div class="ccomment">'.$content.'</div>';
				$text .= "<div class='linkanswer'><a href='javascript:void(0);' onclick=\"vnTcomment.show_post_reply(".$row['cid'].",'".$vnT->lang_name."')\" >".$vnT->lang['product']['reply']."</a></div>";
				$text .= '<div id="com_reply'.$cid.'" class="listanswer">';
				$text .= $text_reply ;
				$text .= '</div>' ;
				$text .='</div></div><div class="clear"></div></div>';
			}
		} 
		$textout = $text ;
		$textout .= $nav ;	
		return $textout;
	}
	function do_votes()
	{
		global $DB,$func,$conf,$vnT; 
		
		$arr_json = array();
		$id = (int)$_POST['id']; 
		$rating = (int)$_POST['rating']; 
		
		//cap nhat vote
		$res = $DB->query("select numvote,votes from products where p_id=$id");
		if ($row=$DB->fetch_row($res)){
			$votes=(($row['votes']*$row['numvote'])+$rating)/($row['numvote']+1);
			$dup['votes'] = round($votes,1);
			$dup['numvote'] = $row['numvote']+1;
			$DB->do_update("products",$dup,"p_id=$id");
			
			$arr_json['ok'] = 1;
		}else{
			$arr_json['ok'] = 0;		 	
		}	 
		
		$json = new Services_JSON( );
	  $textout = $json->encode($arr_json);
		
		return $textout;
	}
	function post_comment(){
		global $DB,$func,$conf,$vnT;
		$arr_json = array();
		$id = (int)$_POST['id']; 
		$display = $vnT->setting['comment_display'];
		$ok_post = 1;
		//xu ly post lien tiep
		// $arr_json['ok'] = 0;
		// $arr_json['mess'] = $_SESSION['sec_code'].' : '.$_POST['security_code'];
		// $json = new Services_JSON( );
	 	// $textout = $json->encode($arr_json);
		// return $textout; 
		if ($_SESSION['sec_code'] == $_POST['security_code']){
			$sec_limit = 5;
			$time_post = time() - $sec_limit ;
			if(isset($_SESSION['last_post'])){
				$sec = $_SESSION['last_post'] - $time_post ;
				if($sec>0) {
					$ok_post = 0 ; 
					$arr_json['ok'] = 0;	
					$arr_json['mess'] = str_replace("{sec}", $sec, $vnT->lang['product']['err_time_post_comment'])  ;	
				}
			}
			if($ok_post){
				$cot['id'] = $id;
				$cot['mem_id'] = $vnT->user['mem_id'];
				//$cot['mark'] = $_POST['mark'];
				$cot['name'] = $func->txt_HTML($_POST['name']);
				$cot['email'] = $_POST['email'];
				//$cot['hidden_email'] = $_POST['h_email']; 
				$cot['content'] = $func->txt_HTML($_POST['content']); 
				$cot['display'] = $display;
				$cot['date_post'] = time();
				$ok = $DB->do_insert("product_comment",$cot);
				if($ok){
					// if($display==1){
					// 	$res = $vnT->DB->query("select numvote,votes from products where p_id=".$id);
					// 	if ($row=$vnT->DB->fetch_row($res)){
					// 		$votes=(($row['votes']*$row['numvote'])+$cot['mark'])/($row['numvote']+1);
					// 		$dup['votes'] = round($votes,1);
					// 		$dup['numvote'] = $row['numvote']+1;
					// 		$vnT->DB->do_update("products",$dup,"p_id=".$id);
					// 	}
					// }
					$_SESSION['last_post'] = time();
		 			$arr_json['ok'] = 1;
					$arr_json['display']= $display;
				}else{
					$arr_json['ok'] = 0;		 	
				}
			}
		}else{
			$arr_json['ok'] = 0;
			$arr_json['mess'] = $vnT->lang['product']['security_code_invalid']; 
		}
		$json = new Services_JSON( );
  	$textout = $json->encode($arr_json);
		return $textout;
	}
	function del_comment()
	{
		global $DB,$func,$conf,$vnT;
		$arr_json = array();
		$id = (int)$_POST['id'];
		$mem_id = (int)$_POST['mem_id'];
		
		//check 
		$res_ck = $DB->query("SELECT * FROM product_comment WHERE mem_id=$mem_id AND cid=$id ");
		if($row_ck = $DB->fetch_row($res_ck))
		{
			$DB->query("DELETE FROM  product_comment WHERE cid=$id ");
			$arr_json['ok'] = 1;		
 		
		}else{
			$arr_json['ok'] = 0;			
		}
		$json = new Services_JSON( );
	  	$textout = $json->encode($arr_json);
		return $textout;
	}
	function show_post_reply(){
		global $DB,$func,$conf,$vnT;
		$arr_json = array(); 
		$cid = (int)$_POST['cid'];
		$reply_sec_code = rand(100000, 999999);
	    $_SESSION['reply_sec_code'] = $reply_sec_code;
	    $data['code_num'] = $reply_sec_code;
	    $scode = $vnT->func->NDK_encode($reply_sec_code);
	    $ver_img = ROOT_URI . "includes/sec_image.php?code=$scode";
		$ok = 1 ;
		$html = ''; 
		$html .= '<form method="post" id="form_reply_'.$cid.'" method="post" onsubmit="return vnTcomment.post_reply('.$cid.',\''.$vnT->lang_name.'\');">
			      <div class="r_content">
			        <textarea id="reply_content'.$cid.'" name="reply_content'.$cid.'" class="form-control" placeholder="'.$vnT->lang['product']['reply_default'].'" style="height: 90px;"></textarea>
			        <div class="content-info" id="reply_info">
			          <div class="info-title">'.$vnT->lang['product']['enter_info_comment'].'</div>
			          <input type="text" name="reply_email'.$cid.'" id="reply_email'.$cid.'" class="form-control" placeholder="Email"/>
			          <input type="text" name="reply_name'.$cid.'" id="reply_name'.$cid.'" class="form-control" placeholder="'.$vnT->lang['product']['your_name'].'"/>
			          <div class="input-group">
			            <input type="text" name="reply_code" id="reply_code" class="form-control" placeholder="'.$vnT->lang['product']['enter_code'].'">
			            <div class="input-group-img">
			                <img class="security_ver" src="'.$ver_img.'" alt="">
			            </div>
			          </div>
			          <button id="btn-send" name="btn-send" class="btn" type="submit">'.$vnT->lang['product']['btn_send_comment'].'</button>
			          </div>
			      </div>
			    </form>';
		$arr_json['ok'] = $ok;
		$arr_json['html'] = $html;
		$json = new Services_JSON( );
	  	$textout = $json->encode($arr_json);
		return $textout;
	}
	function post_reply(){
		global $DB,$func,$conf,$vnT;
		$arr_json = array();
		$cid = (int)$_POST['cid'];
		$id = (int)$_POST['id'];
		$display = $vnT->setting['comment_display'];
		$ok_post = 1;
		//xu ly post lien tiep
		if($_POST['code'] == $_SESSION['reply_sec_code']){
			$sec_limit = 5 ;
			$time_post = time() - $sec_limit ;
			if(isset($_SESSION['last_post'])){
				$sec = $_SESSION['last_post'] - $time_post ;
				if($sec>0) {
					$ok_post = 0 ; 
					$arr_json['ok'] = 0;	
					$arr_json['mess'] = str_replace("{sec}", $sec, $vnT->lang['product']['err_time_post_comment'])  ;	
				}
			}
			if($ok_post){
				$cot['id'] = $id;
				$cot['parentid'] = $cid;
				$cot['mem_id'] = $vnT->user['mem_id'];
				$cot['name'] = $func->txt_HTML($_POST['name']);
				$cot['email'] = trim($_POST['email']);
				$cot['content'] = $func->txt_HTML($_POST['content']); 
				$cot['display'] = $display;
				$cot['date_post'] = time();
				$ok = $DB->do_insert("product_comment",$cot);
				if($ok){
					$_SESSION['last_post'] = time();
		 			$arr_json['ok'] = 1;
					$arr_json['display']= $display;
					if($display==1){
						$src_avatar = ($vnT->user['avatar']) ? $vnT->conf['rooturl'].'vnt_upload/member/avatar/'.$vnT->user['avatar'] : $vnT->conf['rooturl'].'vnt_upload/member/avatar/avatar.jpg';
						$date_post = $vnT->lang['product']['post_at'].' '.date("H:i - d/m/Y") ; 
						$name = $vnT->func->HTML($cot['name']); 
						$content =  $vnT->func->HTML($cot['content']) ;
						$html = '<div class="nodeanswer">';
						$html .= '<div class="avatar"><img src="'.$src_avatar.'" alt="'.$cot['name'].'" /></div>';
						$html .= '<div class="info-comment">';
						$html .= '<div class="info-preson"><span class="name">'.$name.'</span>  -  <span class="time">'.$date_post.'</span></div>';
						$html .= '<div class="ccomment">'.$content.'</div>';
						$html .= '</div>';
						$html .= '</div>';
						$arr_json['html']  = $html ;
					}
				}else{
					$arr_json['ok'] = 0;		 	
				}  
			}
		}else{
			$arr_json['ok'] = 0;
			$arr_json['mess'] = $vnT->lang['product']['security_code_invalid']; 
		}
		$json = new Services_JSON( );
	  	$textout = $json->encode($arr_json);
	  	return $textout;
	}
  

	
echo $jsout;
$vnT->DB->close();
?>