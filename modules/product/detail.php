<?php
/*================================================================================*\
|| 							Name code : main.php 		 		 																	  # ||
||  				Copyright © 2007 by Thai Son - CMS vnTRUST                					# ||
\*================================================================================*/
/**
 * @version : 1.0
 * @date upgrade : 11/12/2007 by Thai Son
 **/
if (!defined('IN_vnT')) {
  die('Access denied');
}
$nts = new sMain();

class sMain
{
  var $output = "";
  var $skin = "";
  var $linkUrl = "";
  var $module = "product";
  var $action = "detail";

  function sMain(){
    global $vnT, $input;
    include("function_" . $this->module . ".php");
    loadSetting();
    $this->skin = new XiTemplate(DIR_MODULE."/".$this->module."/html/".$this->action.".tpl");
    $this->skin->assign('DIR_MOD', DIR_MOD);
    $this->skin->assign('LANG', $vnT->lang);
    $this->skin->assign('INPUT', $input);
    $this->skin->assign('CONF', $vnT->conf);
    $this->skin->assign('DIR_IMAGE', $vnT->dir_images);
    $this->skin->assign('DIR_JS', $vnT->dir_js);
    $vnT->html->addStyleSheet(DIR_MOD . "/css/" . $this->module . ".css");
    // $vnT->html->addScript(DIR_MOD . "/js/" . $this->module . ".js");
    $vnT->setting['menu_active'] = $this->module;
    $id = (int)$input['itemID'];
    if ($_GET['preview'] == 1 && isset($_SESSION['admin_session'])) {
      $where = " ";
    } else {
      $vnT->DB->query("UPDATE products SET views=views+1 WHERE p_id=$id");
      $where = " AND display=1 ";
    }
    $result= $vnT->DB->query("SELECT * FROM products p, products_desc pd
                              WHERE p.p_id=pd.p_id AND lang='{$vnT->lang_name}' AND p.p_id=$id {$where} ");
    if ($row = $vnT->DB->fetch_row($result)) {
      if ($row['metadesc']) $vnT->conf['meta_description'] = $row['metadesc'];
      if ($row['metakey']) $vnT->conf['meta_keyword'] = $row['metakey'];
      if ($row['friendly_title']) $vnT->conf['indextitle'] = $row['friendly_title'];
      $link_seo = ($vnT->muti_lang) ? ROOT_URL . $vnT->lang_name . "/" : ROOT_URL;
      $link_seo .= $row['friendly_url'] . ".html";
      $vnT->conf['meta_extra'] .= "\n".'<link rel="canonical" href="' . $link_seo . '" />';
      $vnT->conf['meta_extra'] .= "\n".'<link rel="alternate" media="handheld" href="' . $link_seo . '"/>';
      $vnT->setting["meta_social_network"] .= "\n".'<meta itemprop="headline" content="'.$vnT->conf['indextitle'].'"/>';
      $vnT->setting["meta_social_network"].="\n".'<meta property="og:url" itemprop="url" content="'.$link_seo.'"/>';
      $vnT->setting["meta_social_network"] .= "\n".'<meta property="og:type" content="article" />';
      $vnT->setting["meta_social_network"] .= "\n".'<meta property="og:title" name="title" itemprop="name" content="'.$vnT->conf['indextitle'].'" />';
      $vnT->setting['meta_social_network'] .= "\n".'<meta property="og:description" itemprop="description" name="description" content="'.$vnT->conf['meta_description'].'" />';
      if ($vnT->muti_lang > 0) {
        $res_lang= $vnT->DB->query("SELECT friendly_url,lang FROM products_desc WHERE p_id=".$row['p_id']." AND display=1 ");
        while ($row_lang = $vnT->DB->fetch_row($res_lang)) {
          $link_lang = create_link("detail", $row['p_id'], $row_lang['friendly_url']);
          $vnT->link_lang[$row_lang['lang']] = str_replace($vnT->link_root, ROOT_URI . $row_lang['lang'] . "/", $link_lang);
        }
      }
      $input['catID'] = $row['cat_id'];
      $row['link_share'] = $link_seo;
      $data['main'] = $this->do_Detail($row);
    } else {
      $link_ref = LINK_MOD . ".html";
      $vnT->func->header_redirect($link_ref);
    }
    $navation = get_navation($input['catID']);
    $data['navation'] = $vnT->lib->box_navation($navation);
    //$vnT->setting['banner'] = $vnT->lib->get_child_slide('child');
    $data['fixed_sidebar'] = $vnT->lib->fixed_sidebar();
    $this->skin->assign("data", $data);
    $this->skin->parse("modules");
    $vnT->output .= $this->skin->text("modules");
  }
  function do_Detail($info){
    global $DB, $func, $input, $vnT;
    $data = $info;
    $cat_id = (int)$info['cat_id'];
    $pic_w = ($vnT->setting['imgdetail_width']) ? $vnT->setting['imgdetail_width'] : 300;
    $pic_wthum = 65;
    if ($info['picture']) {
      $social_network_picture = ROOT_URL . "vnt_upload/product/" . $info['picture'];
      $vnT->setting['meta_social_network'] .= "\n".'<link rel="image_src" href="'.$social_network_picture.'"/>';
      $vnT->setting['meta_social_network'] .= "\n" . '<meta property="og:image" itemprop="thumbnailUrl" content="' . $social_network_picture . '"  />';
      $vnT->setting['meta_social_network'] .= "\n".'<meta itemprop="image" content="'.$social_network_picture.'">';
    }
    if (isset($_SERVER['HTTP_CLIENT_IP']))
        $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
    else if(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
        $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
    else if(isset($_SERVER['HTTP_X_FORWARDED']))
        $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
    else if(isset($_SERVER['HTTP_FORWARDED_FOR']))
        $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
    else if(isset($_SERVER['HTTP_FORWARDED']))
        $ipaddress = $_SERVER['HTTP_FORWARDED'];
    else if(isset($_SERVER['REMOTE_ADDR']))
        $ipaddress = $_SERVER['REMOTE_ADDR'];
    else
        $ipaddress = 'UNKNOWN';
    $cot = [];
    $cot['ip'] = $ipaddress;
    $cot['p_id'] = $info['p_id'];
    $flag_his = $vnT->DB->query("SELECT * FROM history WHERE p_id = ".$info['p_id']." AND ip = '".$ipaddress."'" );
    if (!$flag_hi = $vnT->DB->num_rows($flag_his)) {
        $DB->do_insert("history", $cot);
    }


    $p_name = $vnT->func->HTML($info['p_name']);
    $res_pic = $vnT->DB->query("SELECT * FROM product_picture WHERE p_id=".$info['p_id']);
    $list_slide_img = '';
    if ($num_pic = $vnT->DB->num_rows($res_pic)) {

      if($info['picture']){

        $picture = $this->module . "/" . $info['picture'];
        $src = MOD_DIR_UPLOAD.'/'.$info['picture'];

        $pic_for = $vnT->func->get_pic_modules($picture, $pic_w, 0, " alt='".$p_name."' title='".$p_name."' ");
        $pic_nav = $vnT->func->get_pic_modules($picture, $pic_w, 0, " alt='".$p_name."' title='".$p_name."' ");

        $list_for= '<a  class="text-center">'.$pic_nav.'</a>';
        $list_nav = '<div class="slide-item">
                        <div class="img">'.$pic_for.'</div>
                    </div>';
      }

      $list_slide_img .= '<div class="slide pt-5">
              <div class="slide-featured-product">';
      $slide_add = "";
      while ($row_pic = $vnT->DB->fetch_row($res_pic)) {
        $picture = $this->module."/".$row_pic['picture'];
        $src = MOD_DIR_UPLOAD.'/'.$row_pic['picture'];
        $pic_name = ($row_pic['pic_name']) ? $row_pic['pic_name'] : $p_name;
        $pic_for = $vnT->func->get_pic_modules($picture, $pic_w, 0, " alt='".$pic_name."' title='".$pic_name."' ");
        $pic_nav = $vnT->func->get_pic_modules($picture, $pic_w, 0, " alt='".$pic_name."' title='".$pic_name."' ");
        $list_for .= '<div class="item">
                        <div class="img"><a href="'.$src.'" class="fancy_media" data-fancybox-group="thumb">'.$pic_for.'</a></div>
                    </div>';
        $list_nav .= '<div class="item">
                        <div class="img">'.$pic_nav.'</div>
                    </div>';

        $list_slide_img .=  '<div class="slide-item"><img src="'.$src.'"></div>';
        $slide_add .=  '<div class="slide-item"><img src="'.$src.'"></div>';
      }

      $list_slide_img .='</div>
      <div class="slide-featured-product-thumbnails mt-3">';
      $list_slide_img .= $slide_add;


      $list_slide_img .='</div>
        </div>';


    } else {
      $picture = ($info['picture']) ? $this->module."/".$data['picture'] : $this->module."/".$vnT->setting['pic_nophoto'];
      $src = ($info['picture']) ? MOD_DIR_UPLOAD.'/'.$info['picture'] : MOD_DIR_UPLOAD.'/'.$vnT->setting['pic_nophoto'];
      $pic_for = $vnT->func->get_pic_modules($picture, $pic_w, 0, " alt='".$pic_name."' title='".$pic_name."' ");
      $pic_nav = $vnT->func->get_pic_modules($picture, $pic_w, 0, " alt='".$pic_name."' title='".$pic_name."' ");
      $list_for= '<div class="item"><div class="img">
                    <a href="'.$src.'" class="fancy_media" data-fancybox-group="thumb">'.$pic_for.'</a>
                  </div></div>';
      $list_nav= '<div class="item"><div class="img">'.$pic_nav.'</div></div>';
    }
    $flag = 0;
    $big = '';
    $small= '';
    if($data['image_1'] != ''){
        $data['image_1'] = $this->get_picture($data['image_1']);
        $flag++;
        $big .= '<img class="mySlides center-cropped" src="'.$data['image_1'].'" style="width:100%;">';
        $small .= '<div style="width : 20%;float:left;">
          <img class="demo w3-opacity w3-hover-opacity-off center-cropped-small" src="'.$data['image_1'].'" style="width:100%;cursor:pointer" onclick="currentDiv(1)">
        </div>';
    }

    if($data['image_2'] != ''){
        $data['image_2'] = $this->get_picture($data['image_2']);
        $flag++;
        $big .= '<img class="mySlides center-cropped" src="'.$data['image_2'].'" style="width:100%;display:none">';
        $small .= '<div style="width : 20%;float:left;">
          <img class="demo w3-opacity w3-hover-opacity-off center-cropped-small" src="'.$data['image_2'].'" style="width:100%;cursor:pointer" onclick="currentDiv(2)">
        </div>';
    }

    if($data['image_3'] != ''){
        $data['image_3'] = $this->get_picture($data['image_3']);
        $flag++;
        $big .= '<img class="mySlides center-cropped" src="'.$data['image_3'].'" style="width:100%;display:none">';
        $small .= '<div style="width : 20%;float:left;">
          <img class="demo w3-opacity w3-hover-opacity-off center-cropped-small" src="'.$data['image_3'].'" style="width:100%;cursor:pointer" onclick="currentDiv(3)">
        </div>';
    }

    if($data['image_4'] != ''){
        $data['image_4'] = $this->get_picture($data['image_4']);
        $flag++;
        $big .= '<img class="mySlides center-cropped" src="'.$data['image_4'].'" style="width:100%;display:none">';
        $small .= '<div style="width : 20%;float:left;">
          <img class="demo w3-opacity w3-hover-opacity-off center-cropped-small" src="'.$data['image_4'].'" style="width:100%;cursor:pointer" onclick="currentDiv(4)">
        </div>';
    }

    if($data['image_5'] != ''){
        $data['image_5'] = $this->get_picture($data['image_5']);
        $flag++;
        $big .= '<img class="mySlides center-cropped" src="'.$data['image_5'].'" style="width:100%;display:none">';
        $small .= '<div style="width : 20%;float:left;" >
          <img class="demo w3-opacity w3-hover-opacity-off center-cropped-small" src="'.$data['image_5'].'" style="width:100%;cursor:pointer" onclick="currentDiv(5)">
        </div>';
    }


    $data['big'] = $big;
    $data['small'] = $small;
    $data['flag'] = $flag;
    $data['list_slide_img'] = $list_slide_img;
    $data['list_for'] = $list_for;
    $data['list_nav'] = $list_nav;
    $data['maso'] = $func->HTML($info['maso']);
    $data['p_name'] = $func->HTML($info['p_name']);
    $data['date_post'] = @date("d/m/Y", $info['date_post']);
    $data['date_update'] = @date("d/m/Y", $info['date_update']);
    $data['box_status'] = box_status($info['status']);
    $data['short'] = ($info['short']) ? '<div class="productDes">'.$info['short'].'</div>' : '';
    if ($info['price_old'] && $info['price_old'] > $info['price']) {
      $price = '<div class="p1"><span>'.get_price_product($info['price_old'],'₫').'</span></div>';
      $price.= '<div class="p2"><span>'.get_price_product($info['price'],'₫').'</span></div>';
      $dis = $info['price_old'] - $info['price'];
      $discount = ROUND(($dis * 100)/$info['price_old']);
      $data['discount_text'] = '<div class="rib pro">-'.$discount.'%</div>';
      $price.= '<div class="p3">Tiết kiệm : <strong>'.$discount.'%</strong> ('.get_price_product($dis,'₫').')</div>';
    }else{
      $price = '<div class="p2"><span>'.get_price_product($info['price'],'₫').'</span></div>';
    }
    $data['price_text'] = $price;
    $link_cart = LINK_MOD."/cart.html/?do=add&pID=".$info['p_id'];
    if($info['is_parent'] == 0){
      if($info['in_stock']==2){
        $stock = '<div class="stt dangve">'.$vnT->lang['product']['stock_coming'].'</div>';
      }else{
        $stock = ($info['onhand'] > 0) ? '<div class="stt">'.$vnT->lang['product']['in_stock'].'</div>' : '<div class="stt hethang">'.$vnT->lang['product']['out_stock'].'</div>';
      }
      $data['text_stock']= '<li><div class="at">'.$vnT->lang['product']['stock_title'].'</div>
                            <div class="as">'.$stock.'</div>
                            <div class="clear"></div></li>';
      $nor['link_cart'] = $link_cart;
      $nor['btn_type'] = ($info['onhand'] > 0) ? 'submit' : 'button';
      $nor['onclick'] = ($info['onhand'] > 0) ? '' : ' onclick="OutStock();"';
      $nor['btn_text']=($info['onhand'] > 0) ? $vnT->lang['product']['buy_now'] : $vnT->lang['product']['not_buy'];
      $this->skin->assign('row', $nor);
      // $this->skin->parse("detail.form_normal");
    }else{
      $data['product_sub'] = $this->get_product_sub($info['p_id']);
      $this->skin->assign('row', $nor);
      $this->skin->parse("detail.form_parent");
    }
    if($info['video']){
      $data['btnvideo'] = '<div class="productVideo"><a href="https://www.youtube.com/embed/'.$info['video'].'?autoplay=1&rel=0&loop=1&showinfo=0" class="fancybox.iframe"><span>'.$vnT->lang['product']['video'].'</span></a></div>';
    }
    if($info['element']){
      $data['ele']['title'] = '<li class=""><a href="#tab2" role="tab" data-toggle="tab" aria-expanded="false">'.$vnT->lang['product']['element'].'</a></li>';
      $data['ele']['content'] = '<div class="tab-pane" id="tab2"><div class="the-content desc">'.$info['element'].'</div></div>';
    }
    if ($info['options']) {
      $row_option = '';
      $arr_op = unserialize($info['options']);
      $res_op = $DB->query("SELECT * FROM product_option n, product_option_desc nd
    												WHERE n.op_id=nd.op_id AND nd.lang='$vnT->lang_name' AND display=1
    												ORDER BY op_order ASC, n.op_id DESC");
      while ($r_op = $DB->fetch_row($res_op)) {
        if ($arr_op[$r_op['op_id']]) {
          $row_option .= '<li>
                            <div class="at">'.$func->HTML($r_op['op_name']).'</div>
                            <div class="as">'.$arr_op[$r_op['op_id']].'</div>
                            <div class="clear"></div>
                          </li>';
        }
      }
    }
    $data['row_option'] = $row_option;
    $data['link'] = $info['link'];
    $data['list_tags'] = $vnT->func->get_list_tags($this->module, $info['tags']);
    $link_share = $info['link_share'];
    //plugin
    $list_social_network = '<ul>';
    if ($vnT->setting['social_network_share']) {
      $arr_share = @explode(",", $vnT->setting['social_network_share']);
      foreach ($arr_share as $val) {
        $list_social_network .= '<li>' . $vnT->lib->get_icon_share($val, $link_share) . '</li>';
      }
    }
    if ($vnT->setting['social_network_like']) {
      $arr_share = @explode(",", $vnT->setting['social_network_like']);
      foreach ($arr_share as $val) {
        $list_social_network .= '<li>'.$vnT->lib->get_icon_like($val, $link_share).'</li>';
      }
    }
    $list_social_network .= '</ul>';

    $his_pro = $vnT->DB->query("SELECT * FROM history WHERE ip = '".$ipaddress."' limit 3" );
    $div_his = '';
    if ($his_pro_count = $vnT->DB->num_rows($his_pro)) {
        while ($his_pro_1 = $vnT->DB->fetch_row($his_pro)) {
            $result = $DB->query("SELECT * FROM products p, products_desc pd
                                  WHERE p.p_id = pd.p_id AND lang ='$vnT->lang_name' AND display = 1
                                  AND p.p_id = ". $his_pro_1['p_id']);
            if($num = $DB->num_rows($result)){

              while ($row =$DB->fetch_row($result)) {
                $pID = (int) $row['p_id'];
                $a['link'] = create_link("detail",$pID,$row['friendly_url']);
                $picture = ($row['picture']) ? "product/".$row['picture'] : "product/".$vnT->setting['pic_nophoto'];
                $a['pic'] = $vnT->func->get_pic_modules($picture, $w, 0, " class='img_his' alt='".$row['p_name']."' title='".$row['p_name']."' ",1,0,array("fix_width"=>1));
                $a['p_name'] = $vnT->func->HTML($row['p_name']);
                $a['price_text'] = get_price_product($row['price']);
                $a['price'] = $row['price'];
                $div_his .= '<a class="col-12 col-lg-4 product product_item_style_1" href="'.$a['link'].'">
                  <div class="item__wrap" style="background:#fff;">
                    <div class="item__image">'.$a['pic'].'</div>
                    <p class="item__title">'.$a['p_name'].'</p>
                    <p class="item__price">'.$a['price_text'].'đ</p>
                  </div>
                </a>';
              }
            }

        }
    }

    $data['his'] = $div_his;
    $data['list_social_network'] = $list_social_network;
    $data['link_print'] = "javascript:print();";
    $data['link_tellfriend'] = DIR_MOD."/popup/send_email.php?id=".$info['p_id']."&lang={$vnT->lang_name}&TB_iframe=true&width=650&height=550";
    $data['link_share'] = $link_share;
    $data['support'] = $vnT->func->load_Sitedoc('support');
    if($info['belong_product']){
      $data['belong_product'] = $this->belong_product($info['belong_product']);
    }else{
      $data['belong_product'] = $this->other_product($info);
    }
    if($vnT->setting['comment']){
      $data['c_title'] = '<li class=""><a class="scrollFlag" href="#flag1">'.$vnT->lang['product']['f_comment'].'</a></li>';
      $data['comment'] = $this->do_Comment($info);
      if ($vnT->setting['facebook_comment']) {
        $data['facebook_comment'] = '<div class="facebook-comment" style="padding-top:10px;"><div class="fb-comments" data-href="'.$link_share.'" data-width="100%" data-num-posts="5" data-colorscheme="light"></div><div id="fb-root"></div><script>(function(d, s, id) {  var js, fjs = d.getElementsByTagName(s)[0];  if (d.getElementById(id)) return;  js = d.createElement(s); js.id = id;  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1&appId=' . $vnT->setting['social_network_setting']['facebook_appId'] . '";  fjs.parentNode.insertBefore(js, fjs);}(document, "script", "facebook-jssdk"));</script></div>';
      }
    }
    if($vnT->lang_name == 'vn'){
      $data['link_map'] = '/vn/dai-ly.html';
    }else{
        $data['link_map'] = '/en/systems.html';
    }

    $this->skin->assign("data", $data);
    $this->skin->parse("detail");
    return $this->skin->text("detail");
  }
  function get_picture($picture, $w = ""){
    global $vnT, $func, $DB, $conf;
    $out = "";
    $ext = "";
    $w_thumb = ($vnT->setting['imgthumb_width']) ? $vnT->setting['imgthumb_width'] : 100;
    if ($picture) {
      $linkhinh = "../vnt_upload/product/" . $picture;
      $linkhinh = str_replace("//", "/", $linkhinh);
      $dir = substr($linkhinh, 0, strrpos($linkhinh, "/"));
      $pic_name = substr($linkhinh, strrpos($linkhinh, "/") + 1);
      $src = $dir . "/" . $pic_name;
    }
    if ($w < $w_thumb) $ext = " width='$w' ";
    $out = "<img  src=\"{$src}\" {$ext} >";
    return $src;
  }
  function get_product_sub($p_id){
    global $DB, $func, $input, $vnT;
    $textout = '';
    $where = " AND parentid = {$p_id} ";
    $where .= " ORDER BY sub_order ASC, date_post DESC ";
    $result = $DB->query("SELECT * FROM products_sub p, products_sub_desc pd
                          WHERE p.subid = pd.subid AND lang ='$vnT->lang_name' AND display = 1 $where");
    if($num = $DB->num_rows($result)){
      $w = 65;
      while ($row =$DB->fetch_row($result)) {
        $subid = (int) $row['subid'];
        $picture = ($row['picture']) ? $row['picture'] : $vnT->setting['pic_nophoto'];
        $data['pic'] = $vnT->func->get_pic_modules("product/".$picture,$w,0," alt='".$row['title']."' ",1,0,array("fix_width"=>1));
        $data['src'] = MOD_DIR_UPLOAD.'/'.$picture;
        $data['cat_name'] = $vnT->setting['cat_name'][(int)$row['cat_id']];
        $data['color'] = $vnT->func->HTML($row['color']);
        $data['param'] = $vnT->func->HTML($row['param']);
        $data['price_text'] = get_price_product($row['price']);
        $data['maso'] = $row['maso'];
        $data['short'] = $vnT->func->cut_string($vnT->func->check_html($row['description'],'nohtml'),200,1);
        $data['subid'] = $row['subid'];
        if($row['onhand'] > 0){
          $data['onclick'] = ' onclick="AddCart_SubPro('.$p_id.','.$subid.');"';
          $data['btn_class'] = '';
          $data['disabled'] = '';
        }else{
          $data['onclick'] = ' onclick="OutStock();"';
          $data['btn_class'] = 'out_stock';
          $data['disabled'] = 'disabled="disabled"';
        }
        $this->skin->assign('row', $data);
        $this->skin->parse("html_product_sub.has_item");
      }
      $nd['num_sub'] = str_replace('{num}', $num, $vnT->lang['product']['num_sub'] );
    }else{
      $this->skin->assign('row', $data);
      $this->skin->parse("html_product_sub.no_item");
    }
    $nd['f_title'] = $vnT->lang['product']['belong_product'];
    $nd['list'] = $text;
    $this->skin->reset("html_product_sub");
    $this->skin->assign("data", $nd);
    $this->skin->parse("html_product_sub");
    $textout = $this->skin->text("html_product_sub");
    return $textout;
  }
  function belong_product($belong){
    global $DB, $func, $input, $vnT;
    $arr_out = array();
    $text = '';
    $where = " AND p.p_id IN ($belong) ";
    $where .= " ORDER BY p_order DESC, date_post DESC ";
    $result = $DB->query("SELECT * FROM products p, products_desc pd
                          WHERE p.p_id = pd.p_id AND lang ='$vnT->lang_name' AND display = 1 $where");
    if($num = $DB->num_rows($result)){
      $arr_out['title'] = '<li class=""><a class="scrollFlag" href="#flag2">'.$vnT->lang['product']['belong_product'].'</a></li>';
      $w = ($vnT->setting['img_width_grid']) ? $vnT->setting['img_width_grid'] : 240;
      while ($row =$DB->fetch_row($result)) {
        $pID = (int) $row['p_id'];
        $data['link'] = create_link("detail",$pID,$row['friendly_url']);
        $picture = ($row['picture']) ? "product/".$row['picture'] : "product/".$vnT->setting['pic_nophoto'];
        $data['pic'] = $vnT->func->get_pic_modules($picture, $w, 0, " alt='".$row['p_name']."' title='".$row['p_name']."' ",1,0,array("fix_width"=>1));
        $data['p_name'] = $vnT->func->HTML($row['p_name']);
        $data['price'] = $row['price'];
        $data['price_text'] = get_price_product($row['price']);
        $data['price_old_text'] = $data['discount'] = '';
        if($row['price_old'] && $row['price_old'] > $row['price']){
          $data['price_old_text'] = '<div class="nor">'.get_price_product($row['price_old']).'</div>';
          $dis = $row['price_old'] - $row['price'];
          $discount = ROUND(($dis * 100)/$row['price_old']);
          $data['discount'] = ($discount > 1) ? '<div class="rib pro">-'.$discount.'%</div>' : '';
        }
        $data['maso'] = $row['maso'];
        $data['box_status'] = box_status($row['status']);
        $data['out_stock'] = ($row['is_parent']==0 && $row['onhand'] < 1) ? '<div class="hethang">'.$vnT->lang['product']['out_stock'].'</div>' : '';
        $this->skin->reset("item_belong");
        $this->skin->assign("data", $data);
        $this->skin->parse("item_belong");
        $text .=  $this->skin->text("item_belong");
      }
      $nd['f_title'] = $vnT->lang['product']['belong_product'];
      $nd['list'] = $text;
      $this->skin->reset("html_belong_product");
      $this->skin->assign("data", $nd);
      $this->skin->parse("html_belong_product");
      $arr_out['list'] = $this->skin->text("html_belong_product");
    }
    return $arr_out;
  }
  function other_product($info){
    global $DB, $func, $input, $vnT;
    $arr_out = array();
    $text = '';
    $p_id = (int)$info['p_id'];
    $cat_id = (int)$info['cat_id'];
    $where = get_where_cat($cat_id);
    $where .= " AND p.p_id<>$p_id  ";
    $where .= " ORDER BY p_order DESC, date_post DESC ";
    $result = $DB->query("SELECT * FROM products p, products_desc pd
                          WHERE p.p_id = pd.p_id AND lang ='$vnT->lang_name' AND display = 1 $where");
    if($num = $DB->num_rows($result)){
      $arr_out['title'] = '<li class=""><a class="scrollFlag" href="#flag2">'.$vnT->lang['product']['belong_product'].'</a></li>';
      $w = ($vnT->setting['img_width_grid']) ? $vnT->setting['img_width_grid'] : 240;
      while ($row =$DB->fetch_row($result)) {
        $pID = (int) $row['p_id'];
        $data['link'] = create_link("detail",$pID,$row['friendly_url']);
        $picture = ($row['picture']) ? "product/".$row['picture'] : "product/".$vnT->setting['pic_nophoto'];
        $data['pic'] = $vnT->func->get_pic_modules($picture, $w, 0, " alt='".$row['p_name']."' title='".$row['p_name']."' ",1,0,array("fix_width"=>1));
        $data['p_name'] = $vnT->func->HTML($row['p_name']);
        $data['price'] = $row['price'];
        $data['price_text'] = get_price_product($row['price']);
        $data['price_old_text'] = $data['discount'] = '';
        if($row['price_old'] && $row['price_old'] > $row['price']){
          $data['price_old_text'] = '<div class="nor">'.get_price_product($row['price_old']).'</div>';
          $dis = $row['price_old'] - $row['price'];
          $discount = ROUND(($dis * 100)/$row['price_old']);
          $data['discount'] = ($discount > 1) ? '<div class="rib pro">-'.$discount.'%</div>' : '';
        }
        $data['maso'] = $row['maso'];
        $data['box_status'] = box_status($row['status']);
        $data['out_stock'] = ($row['is_parent']==0 && $row['onhand'] < 1) ? '<div class="hethang">'.$vnT->lang['product']['out_stock'].'</div>' : '';
        $this->skin->reset("item_belong");
        $this->skin->assign("data", $data);
        $this->skin->parse("item_belong");
        $text .=  $this->skin->text("item_belong");
      }
      $nd['f_title'] = $vnT->lang['product']['other_product'];
      $nd['list'] = $text;
      $this->skin->reset("html_belong_product");
      $this->skin->assign("data", $nd);
      $this->skin->parse("html_belong_product");
      $arr_out['list'] = $this->skin->text("html_belong_product");
    }
    return $arr_out;
  }
  function do_Comment($info){
    global $DB, $func, $input, $vnT;
    $vnT->html->addScript($vnT->dir_js . "/jquery_alerts/jquery.alerts.js");
    $vnT->html->addStyleSheet($vnT->dir_js . "/jquery_alerts/jquery.alerts.css");
    $p_id = (int)$info['p_id'];
    //check comment
    $data['list_comment'] = ' <div id="ext_comment" class="grid-comment">';
    $data['list_comment'] .= "<script language=\"JavaScript\"> vnTcomment.show_comment('" . $p_id . "','" . $vnT->lang_name . "','0'); </script>";
    $data['list_comment'] .= '</div>';
    $data['mem_id'] = (int)$vnT->user['mem_id'];
    $data['com_name'] = '';
    $data['com_email'] = '';
    if ($vnT->user['mem_id'] > 0) {
      $data['com_name'] = $vnT->user['full_name'];
      $data['com_email'] = $vnT->user['email'];
    }
    $sec_code = rand(100000, 999999);
    $_SESSION['sec_code'] = $sec_code;
    $data['code_num'] = $sec_code;
    $scode = $vnT->func->NDK_encode($sec_code);
    $data['ver_img'] = ROOT_URI . "includes/sec_image.php?code=$scode&h=40";
    $data['p_id'] = $p_id;
    $data['lang'] = $vnT->lang_name;
    $data['mem_id'] = $vnT->user['mem_id'];
    $data['link_login'] = ROOT_URI . "member/login.html/" . $vnT->func->NDK_encode($vnT->seo_url);
    $data['err'] = $err;
    $data['link_action'] = $link;
    $this->skin->assign("data", $data);
    $this->skin->parse("html_comment");
    $textout = $this->skin->text("html_comment");
    return $textout;
  }
}
?>
