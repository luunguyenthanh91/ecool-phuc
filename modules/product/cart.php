<?php
/*================================================================================*\
|| 							Name code : cart.php 		 		 																	  # ||
||  				Copyright © 2007 by Thai Son - CMS vnTRUST                					# ||
\*================================================================================*/
/**
 * @version : 1.0
 * @date upgrade : 11/12/2007 by Thai Son
 **/
if (!defined('IN_vnT')) {
  die('Access denied');
}
$nts = new sMain();

class sMain
{
  var $output = "";
  var $skin = "";
  var $linkUrl = "";
  var $module = "product";
  var $action = "cart";

  function sMain(){
    global $vnT, $input, $func, $cart, $DB, $conf;
    include("function_" . $this->module . ".php");
    loadSetting();
    include("function_shopping.php");
    $this->skin = new XiTemplate(DIR_MODULE . "/" . $this->module . "/html/" . $this->action . ".tpl");
    $this->skin->assign('DIR_MOD', DIR_MOD);
    $this->skin->assign('LANG', $vnT->lang);
    $this->skin->assign('INPUT', $input);
    $this->skin->assign('CONF', $vnT->conf);
    $this->skin->assign('DIR_IMAGE', $vnT->dir_images);
    $vnT->html->addStyleSheet(DIR_MOD . "/css/cart.css");
    $vnT->html->addScript(DIR_MOD . "/js/cart.js");
    //active menu
    $vnT->setting['menu_active'] = $this->module;
    $vnT->conf['indextitle'] = $vnT->lang['product']['f_cart'];
    //============= them san pham =========================
    $date = date("l j, F Y");
    // if product is to be added
    if (isset($input["do"]) && ($input["do"] == "buy_now")) {
      $cart->add_item($cart->session, "product", (int) $input["pID"], 1);
      $link_ref = create_link_shopping("checkout_address");
      $vnT->func->header_redirect($link_ref);
    }
    if (isset($input["do"]) && ($input["do"] == "add")) {
      $arr_id = array();
      $_SESSION['last_pid'] = (int) $input['pID'];
      if ($input['pID']) {
        $arr_id = explode(",", $input["pID"]);
      }
      if($input['subID']){
        $arrsub_id = explode(",",$input["subID"]);
      }
      $quantity = intval($input['quantity']);
      if ($quantity == 0) $quantity = 1;
      for ($i = 0; $i < count($arr_id); $i++) {
        $check = check_product_order($arr_id[$i]);
        if ($check['ok'] == 0) {
          $ok_insert = 0;
          $mess = $check['mess']."<br>".$check['mess'];
          $url = $_SERVER['HTTP_REFERER'];
          $vnT->func->html_redirect($url, $mess);
        } else {
          if(isset($input["do_act"]) && ($input["do_act"] =="add_sub")){
            $cart->add_item_sub($cart->session,"product",$arr_id[$i],$arrsub_id[$i],$quantity, $input);
          }else{
            $cart->add_item($cart->session, "product", $arr_id[$i], $quantity, $input);
          }
        }
      }
      if($input['btnBuynow'])
        $link_ref = create_link_shopping("checkout_address");
      else
        $link_ref = create_link_shopping("cart");
      $vnT->func->header_redirect($link_ref);
    }
    // Xoa san pham ra khoi cart
    if (isset ($input["remove"])) {
      if ($input["remove"] != "all") {
        $id = $input["remove"];
        $cart->delete_item($cart->session, $id);
      }
      if ($input["remove"] == "all") {
        $sql = "DELETE FROM order_shopping WHERE session='" . $cart->session . "' ";
        $vnT->DB->query($sql);
      }
      $link_ref = create_link_shopping("cart");
      $vnT->func->header_redirect($link_ref);
    }
    // if contents is to be modified
    if (isset($input["modify"])) {
      $contents = $cart->display_contents($cart->session);
      $quantity = $input["quantity"];
      for ($i = 0; $i < count($quantity); $i++) {
        $id = $contents["id"][$i];
        $newquan = $quantity[$id];
        if ($newquan == 0) {
          // $vnT->DB->query( "DELETE FROM order_shopping  WHERE session='".$cart->session."' AND id=$id");
        } else {
          $data['color'] = $input['color'][$id];
          $data['size'] = $input['size'][$id];
          $cart->modify_quantity($cart->session, $id, $newquan, $data);
        }
      }
    }
    // khoi tao
    $res = $vnT->DB->query("SELECT * FROM order_address WHERE session='" . $cart->session . "' ");
    $info = $vnT->DB->fetch_row($res);
    if (isset($_POST['btnCheckout'])) {
      $err = "";
      $total_cart = $cart->cart_total($cart->session);
      $check_onhand = $cart->check_cart($cart->session);
      if($check_onhand['has_err'] > 0){
        $err = $check_onhand['err'];
        $mess = $vnT->func->html_mess($check_onhand['err']);
      }
      if (empty($err)) {
        $cot['mem_id'] = $vnT->user['mem_id'];
        $cot['username'] = $vnT->user['username'];
        $res_ck = $DB->query("SELECT * FROM order_address WHERE session='".$cart->session."' ");
        if ($row_ck = $DB->fetch_row($res_ck)) {
          $cot['d_name'] = ($row_ck['d_name']) ? $row_ck['d_name'] : $vnT->user['full_name'];
          $cot['d_email'] = ($row_ck['d_email']) ? $row_ck['d_email'] : $vnT->user['email'];
          $cot['d_address'] = ($row_ck['d_address']) ? $row_ck['d_address'] : $vnT->user['address'];
          $cot['d_city'] = ($row_ck['d_city']) ? $row_ck['d_city'] : $vnT->user['city'];
          $cot['d_state'] = ($row_ck['d_state']) ? $row_ck['d_state'] : $vnT->user['state'];
          $cot['d_ward'] = ($row_ck['d_ward']) ? $row_ck['d_ward'] : $vnT->user['ward'];
          $vnT->DB->do_update("order_address", $cot, "session='" . $cart->session . "' ");
        } else {
          $cot['session'] = $cart->session;
          $cot['d_name'] = $vnT->user['full_name'];
          $cot['d_email'] = $vnT->user['email'];
          $cot['d_address'] = $vnT->user['address'];
          $cot['d_city'] = $vnT->user['city'];
          $cot['d_state'] = $vnT->user['state'];
          $cot['d_ward'] = $vnT->user['ward'];
          $cot['date_post'] = time();
          $vnT->DB->do_insert("order_address", $cot);
        }
        $link_ref = create_link_shopping("checkout_address");
        $vnT->func->header_redirect($link_ref);
      }//end fid err
    }//end if btnCheckout
    $info['mess'] = $mess;
    if ($cart->num_items($cart->session)) {
      $data['main'] = $this->do_ShowCart($info);
    } else {
      $data['main'] = $this->do_EmptyCart();
    }
    $data['link_action'] = create_link_shopping("cart");
    $navation = get_navation(0, $vnT->lang['product']['f_cart']);
    $data['navation'] = $vnT->lib->box_navation($navation);
    //$vnT->setting['banner'] = $vnT->lib->get_child_slide('child');
    $data['fixed_sidebar'] = $vnT->lib->fixed_sidebar();
    $data['mess'] = $mess;
    if (isset($_SESSION['mess']) && $_SESSION['mess'] != '') {
      $data['mess'] = $_SESSION['mess'];
      unset($_SESSION['mess']);
    }
    $this->skin->assign("data", $data);
    $this->skin->parse("modules");
    $vnT->output .= $this->skin->text("modules");
  }
  function do_ShowCart($info){
    global $vnT, $input, $conf, $cart;
    $row_cart = "";
    $contents = $cart->display_contents($cart->session);
    $row['stt'] = $x + 1;
    $x = 0;
    $w = 80;
    while ($x < $cart->num_items($cart->session)) {
      $id = $contents["id"][$x];
      $item_type = $contents["item_type"][$x];
      $item_id = $contents["item_id"][$x];
      $link = $vnT->link_root . $contents["item_link"][$x];
      $maso = $contents['item_maso'][$x];
      $picture = ($contents["item_picture"][$x]) ? $contents["item_picture"][$x] : "product/" . $vnT->setting['pic_nophoto'];
      $src_pic = $vnT->func->get_src_modules($picture, $w, 0, 0);
      $item_title = '<div class="img"><a href="'.$link.'"><img src="'.$src_pic.'"></a></div>';
      $item_title.= '<div class="i-title"><a href="'.$link.'">'.$contents['item_title'][$x].'</a></div>';
      $item_title.= '<div class="i-code">#'.$maso.'</div>';
      $item_title.= '<div class="clear"></div>';
      $price = $contents["price"][$x];
      $total = $price * $contents['quantity'][$x];
      $row['price_old'] = '';
      // if($contents["price_old"][$x]){
      //   $row['price_old'] = '<div class="normal">'.get_price_pro($contents['price_old'][$x],'đ').'</div>';
      // }
      $row['id'] = $id;
      $row['item_title'] = $item_title;
      $row['maso'] = $contents['item_maso'][$x];
      $row['price'] = get_price_pro($contents['item_price'][$x],'đ');
      $row['quantity'] = "<input type='text' class='form-control quantity' id='quantity_".$id."' name='quantity[".$id."]' value='".$contents['quantity'][$x]."' onChange=\"vnTProduct.updateQuantity(".$id.")\"/> ";
      $row['total'] = get_price_pro($contents['total'][$x]);
      $row['link_remove'] = create_link_shopping("cart", "?remove=" . $id);
      $row_cart .= load_html("html_row_cart", $row);
      $x++;
    }
    $data['cart_total'] = get_price_pro($cart->cart_total($cart->session));
    $data['row_cart'] = $row_cart;
    $link_ref = $_SERVER['HTTP_REFERER'];
    $res_cat = $vnT->DB->query("SELECT cat_id FROM products WHERE p_id=" . (int)$_SESSION['last_pid']);
    if ($r_cat = $vnT->DB->fetch_row($res_cat)) {
      $cat_id = (int)$r_cat['cat_id'];
      $friendly_url = $vnT->setting['arr_category'][$cat_id]['friendly_url'];
      $link_ref = create_link("category", $cat_id, $friendly_url);
    }
    $data['link_ref'] = $link_ref;
    $data['link_remove'] = create_link_shopping("cart", "?remove=all");
    $data['link_continue_shopping'] = $vnT->link_root . "member/your_order.html/?do=add";
    $data['mess_buy_cart'] = $vnT->func->load_Sitedoc("mess_buy_cart");
    $data['link_action'] = create_link_shopping("cart");
    $data['note_cart'] = $vnT->func->load_Sitedoc('note_cart');
    $data['note_payment'] = $vnT->func->load_Sitedoc('note_payment');
    $data['nav_shopping'] = nav_shopping('cart');
    $data['mess'] = $info['mess'];
    $this->skin->assign("data", $data);
    $this->skin->parse("html_cart");
    return $this->skin->text("html_cart");
    $nd['f_title'] = '<h1>'.$vnT->lang['product']['f_cart'].'</h1>';
    return $vnT->skin_box->parse_box("box_middle2",$nd);
  }
  function do_EmptyCart(){
    global $vnT, $input, $cart;
    $data['nav_shopping'] = nav_shopping('cart');
    $data['link_back'] = $vnT->link_root;
    $this->skin->assign("data", $data);
    $this->skin->parse("html_empty_cart");
    return $this->skin->text("html_empty_cart");
    $nd['f_title'] = '<h1>'.$vnT->lang['product']['f_cart'].'</h1>';
    return $vnT->skin_box->parse_box("box_middle",$nd);
  }
}

?>