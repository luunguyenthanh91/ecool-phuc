<?php
/*================================================================================*\
|| 							Name code : cart.php 		 		 																	  # ||
||  				Copyright © 2007 by Thai Son - CMS vnTRUST                					# ||
\*================================================================================*/
/**
 * @version : 1.0
 * @date upgrade : 11/12/2007 by Thai Son
 **/
if (! defined('IN_vnT'))
{
  die('Access denied');
}
$nts = new sMain();
class sMain
{
  var $output = "";
  var $skin = "";
  var $linkUrl = "";
	var $module = "product";
	var $action = "checkout_confirmation";

	function sMain(){
		global $vnT,$input,$func,$cart,$DB,$conf;
		include ("function_".$this->module.".php");
		loadSetting();
		include ("function_shopping.php");
		$this->linkMod = $vnT->cmd . "=mod:".$this->module;
		$this->skin = new XiTemplate( DIR_MODULE ."/". $this->module . "/html/". $this->action . ".tpl");
    $this->skin->assign('DIR_MOD', DIR_MOD);
		$this->skin->assign('LANG', $vnT->lang);
		$this->skin->assign('INPUT', $input);
		$this->skin->assign('CONF', $vnT->conf);
		$this->skin->assign('DIR_IMAGE', $vnT->dir_images);		
		
		$vnT->html->addScript(DIR_MOD."/js/cart.js");
		$vnT->html->addStyleSheet( DIR_MOD."/css/cart.css");
		//active menu
		$vnT->setting['menu_active'] = $this->module; 
		$vnT->conf['indextitle'] = $vnT->lang['product']['f_checkout_confirmation'];
		if ($cart->num_items($cart->session) == 0){
			$mess = $vnT->lang['product']['empty_cart'];
			$url = $this->linkMod;
			$vnT->func->html_redirect($url,$mess);
		}
		$res = $vnT->DB->query("SELECT * FROM order_address WHERE session='".$cart->session."' ");
		$info = $vnT->DB->fetch_row($res);
		//check neu chua co address  
		if(empty($info['address_id'])){
			$link_ref= create_link_shopping("checkout_address");    
			$vnT->func->header_redirect($link_ref);    
		}
		//empty sipping
		if (empty($info['shipping_method'])){
			$link_ref=  create_link_shopping("checkout_method");
			$vnT->func->header_redirect($link_ref); 						
		}
		//empty payment
		if (empty($info['payment_method'])){
			$link_ref=  create_link_shopping("checkout_method");
			$vnT->func->header_redirect($link_ref);
		}
		$data['nav_shopping'] = nav_shopping('checkout_confirmation');
		$data['checkout_address'] = $this->checkout_address($info);
		$data['info_shipping'] = $this->info_shipping($info);
		$data['info_payment'] = $this->info_payment($info);
		$data['info_cart'] = do_ShowCart($info);

		$data['link_action'] = create_link_shopping("checkout_process");
		$data['link_back'] = create_link_shopping("checkout_method");
		$navation = get_navation (0,$vnT->lang['product']['f_checkout_confirmation']);
		$data['navation'] = $vnT->lib->box_navation($navation);
    //$vnT->setting['banner'] = $vnT->lib->get_child_slide('child');
    $data['fixed_sidebar'] = $vnT->lib->fixed_sidebar();
		$this->skin->assign("data", $data);		
    $this->skin->parse("modules");
		$vnT->output .= $this->skin->text("modules");
	}
	function checkout_address($info){
		global $vnT,$input,$cart ,$conf;
		$info_address='';
		$total =0;
		$s_price=0;
		$total = $cart->cart_total($cart->session);
		$data['payment_address'] = get_cart_address ($info);
		$data['shipping_address'] = get_cart_address ($info,"shipping");
		if($info['bill']){
			$bill = '<div class="boxCart"><div class="title">'.$vnT->lang['product']['invoice_info'].'</div>
						  	<div class="content">';
			$bill.= '<p>'.$vnT->lang['product']['company'].' : '.$info['bill_company'].'</p>';
			$bill.= '<p>'.$vnT->lang['product']['address'].' : '.$info['bill_address'].'</p>';
			$bill.= '<p>'.$vnT->lang['product']['mst'].' : '.$info['bill_mst'].'</p>';
			$bill .= '</div></div>';
			$data['invoice_info'] = $bill;
		}
		$this->skin->assign("data", $data);
		$this->skin->parse("checkout_address");
		return $this->skin->text("checkout_address");	 
	}
	function info_shipping($info){
		global $vnT,$input,$cart,$conf;
		$res_p = $vnT->DB->query("SELECT * FROM shipping_method WHERE name='".$info['shipping_method']."' ");
		if($row_p = $vnT->DB->fetch_row($res_p)){
			$data['title'] = $vnT->func->fetch_array($row_p['title']);
			$data['description'] =  $vnT->func->fetch_array($row_p['description']);
		}
		$data['f_title'] = $vnT->lang['product']['shipping_method'];
		$this->skin->reset("html_info_method");
		$this->skin->assign("data", $data);
		$this->skin->parse("html_info_method");
		return $this->skin->text("html_info_method");
	}
	function info_payment($info){
		global $vnT,$input,$cart,$conf;
		$res_p = $vnT->DB->query("SELECT * FROM payment_method WHERE name='".$info['payment_method']."' ");
		if($row_p = $vnT->DB->fetch_row($res_p)){
			$data['title'] = $vnT->func->fetch_array($row_p['title']);
			$description =  $vnT->func->fetch_array($row_p['description']);
			$payment_address = ($info['payment_address'] == 1) ? "<b>Địa chỉ người mua</b>" : "<b>Địa chỉ giao hàng</b>";
			$data['description'] = str_replace("{list_select}",$payment_address,$description);
		}
		if($row_p['payment']=="bank_transfer"){
			$module = unserialize($row_p['config']);
			$data['description'] = $module['description_'.$vnT->lang_name.''];
		}
		//load payment
		$name_payment = trim($info['payment_method']);
		$vnT->module = fetch_module_payment($name_payment);		
    $vnT->module['order_code'] = $order_code;
    $vnT->module['s_price'] = $s_price;
    $vnT->module['total_price'] = $total_price;
		$path = PATH_ROOT . "/modules/product/payment/".$name_payment."/transfer.inc.php";
    $path_payment = (file_exists($path)) ? $path : "payment/transfer.inc.php";
    include ($path_payment);
		if(function_exists("formPayment")) {
			$data['form_payment'] = formPayment($info);
		}
		$data['f_title'] = $vnT->lang['product']['payment_method'];
		$this->skin->reset("html_info_method");
		$this->skin->assign("data", $data);
		$this->skin->parse("html_info_method");
		return $this->skin->text("html_info_method");
	}
}
?>