<?php
	session_start();
	define('IN_vnT',1);
	define('DS', DIRECTORY_SEPARATOR);
	require_once("../../../_config.php"); 
	require_once($conf['rootpath']."includes/class_db.php"); 
	$DB = new DB;
	//load mailer
	require_once($conf['rootpath']."libraries/phpmailer/phpmailer.php"); 	
	$vnT->mailer = new PHPMailer();	
	
	require_once($conf['rootpath']."includes/class_functions.php"); 	
	$func = new Func_Global;
	$conf=$func->fetchDbConfig($conf);
	$vnT->conf = $conf ;		
	
	$linkMod = "?".$conf['cmd']."=mod:product";
	$vnT->lang_name = (isset($_GET['lang'])) ? $_GET['lang']  : "vn" ;
	$func->load_language('product');
	
	$id= (int)$_GET['id'];
	$cat_id= (int)$_GET['cat_id'];
	
	$result = $DB->query("select * from product_setting");
  $setting = $DB->fetch_row($result);
  foreach ($setting as $k => $v)
  {
    $vnT->setting[$k] = stripslashes($v);
  }
	
	/*-------------- get_pic_thumb --------------------*/
	function get_pic_thumb ($picture, $w=100, $ext="")
	{
		global $vnT,$conf;
		$out = "";
		$w_thumb = ($vnT->setting['img_width_grid']) ? $vnT->setting['img_width_grid'] : 100 ;
		
		if ($w)  
		{
			$linkhinh = "vnt_upload/product/".$picture;
			$linkhinh = str_replace("//","/",$linkhinh);
			$dir = substr($linkhinh,0,strrpos($linkhinh,"/"));
			$pic_name = substr($linkhinh,strrpos($linkhinh,"/")+1) ;
			$src = $conf['rooturl'] . $dir."/thumbs/".$pic_name;						
		} else {
			$src = $conf['rooturl'] . "vnt_upload/product/" . $picture;    
		}
		
		
		if($w<$w_thumb) $ext .= " width='$w' ";
		
		$out = "<img  src=\"{$src}\" {$ext} >";
		
		return $out;
	}
	
	
	$ok_send =0;
	$mess="";
	$dirUpload = "vnt_upload/product";
 	$result = $DB->query("SELECT * 
						FROM products p, products_desc pd
						WHERE p.p_id=pd.p_id 
						AND lang='$vnT->lang_name'
						AND display=1 						
						AND p.p_id=$id ");	
	$listSP = '<table width="100%" border="0" cellspacing="1" cellpadding="1" bgcolor="#CCCCCC">';
	if ($row = $DB->fetch_row($result)){
		$pic = get_pic_thumb($row['picture'],100);
		
		if ($func->is_muti_lang()) {
			 $link = $conf['rooturl'] .$vnT->lang_name."/";
		}else{
			 $link = $conf['rooturl'] ;
		}
		$link .= "product/".$row['p_id']."/".$row['friendly_url'].".html";
		
		$name ="<a href='".$link."' ><strong>".$func->HTML($row['p_name'])."</strong></a>";
		$price = $func->format_number($row['price'])." VNĐ";
		$listSP.= "<tr bgcolor='#ffffff'>
					<td width=160 align=center>".$pic."</td>
					<td style='padding-left:5px;'>
					<p>".$vnT->lang['product']['maso']." : <strong>".$row['maso']."</strong> </p>
					<p>".$vnT->lang['product']['product']." : ".$name."</p>
					<p>".$vnT->lang['product']['price']." : <b style='color:#CC0000'>".$price."</b></p></td>
				 </tr>";
	}
	$listSP .= "</table>"; 
	$text = $listSP;
	
	
	if (isset($_POST['btnSend'])) {

			//send mail	
			$content_email = $func->load_MailTemp("tell_friend");
			$qu_find = array(
				'{host}',
				'{name}',
				'{sender}',
				'{product}',	
				'{content}'
			);
			$qu_replace = array(
				$_SERVER['HTTP_HOST'],
				$_POST['name_to'],
			  $_POST['name_from']." (".$_POST['email_from'].") " , 
				$listSP,
				$_POST['content']
			);
			$message = str_replace($qu_find, $qu_replace, $content_email);
			$subject = $vnT->lang['product']['subject_email_tellfriend'] ;

			if ($_SESSION['send_email']!=$_POST['email_to'])	
			{
				$sent = $func->doSendMail($_POST['email_to'],$subject,$message,$vnT->conf['email']);
				$_SESSION['send_email'] = $_POST['email_to'];
				$ok_send =1;
				$mess= $func->html_mess($vnT->lang['product']['send_email_success']);
			}else{
				$mess= $func->html_mess($vnT->lang['product']['err_email_sended']);
			}
			
	}
	
	 
	$data['mess_info'] = $text;

	mt_srand ((double) microtime() * 1000000);
	$num  = mt_rand(100000,999999);
	$scode = $func->NDK_encode($num);
	$img_code = "../../../includes/do_image.php?code=$scode";

	
?>
<html>
<head>
<title>.: TELL FRIEND :.</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<link href="../css/popup.css" rel="stylesheet" type="text/css" />
<script language=javascript>
	function checkform(f) {			
		var re =/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,5})$/gi;
		
		var name_from = f.name_from.value;
		if (name_from == '') {
			alert('<?=$vnT->lang['product']['err_name_from_empty']?>');
			f.name_from.focus();
			return false;
		}
			 
		email_from = f.email_from.value;
		if (email_from == '') {
			alert('<?=$vnT->lang['product']['err_email_from_empty']?>');
			f.email_from.focus();
			return false;
		}
		if (email_from != '' && email_from.match(re)==null) {
			alert('<?=$vnT->lang['product']['err_email_invalid']?>');
			f.email_from.focus();
			return false;
		}
		
		var name_to = f.name_to.value;
		if (name_to == '') {
			alert('<?=$vnT->lang['product']['err_name_to_empty']?>');
			f.name_to.focus();
			return false;
		}
			 
		email_to = f.email_to.value;
		if (email_to == '') {
			alert('<?=$vnT->lang['product']['err_email_to_empty']?>');
			f.email_to.focus();
			return false;
		}
		if (email_to != '' && email_to.match(re)==null) {
			alert('<?=$vnT->lang['product']['err_email_invalid']?>');
			f.email_to.focus();
			return false;
		}
		
		var content = f.content.value;
		if (content == '') {
			alert('<?=$vnT->lang['product']['err_content_empty']?>');
			f.content.focus();
			return false;
		}
		
		if (f.h_code.value != f.security_code.value ) {
			alert('<?=$vnT->lang['product']['err_security_code_invalid']?>');
			f.security_code.focus();
			return false;
		}

		return true;
	}
</script>

</head>
<body >

<div   style="margin:5px" >
<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td  height="30" class="font_f_title" ><img src="../images/mail_send.gif"  align="absmiddle"/>&nbsp;<?=$vnT->lang['product']['f_intro_to_friend']?></td>
	</tr>
	<tr>
		<td  bgcolor="#FF0000" height="2" ></td>
	</tr>
</table>
<br />

<?php
	 
	 echo $mess;
  if ($ok_send==0)
	{
     echo $data['mess_info'] ;
 ?>
  
<form action="<?=$link_action?>" method="post" name="myFrom" id="myFrom" onSubmit="return checkform(this);">

<table width="100%" border="0" cellspacing="3" cellpadding="3">
  <tr>
    <td><label for="name"><strong><?php echo $vnT->lang['product']['name_from']; ?></strong> <font color="red">(*)</font> :</label>
    <input class="textfiled" name="name_from" type="text" id="name_from"  style="width:100%" maxlength="250" value="<?=$_POST['name_from']?>" />
    </td>
    <td><label for="email"><strong><?php echo $vnT->lang['product']['email_from']; ?></strong> <font color="red">(*)</font> :</label>
    <input class="textfiled" name="email_from" type="text" id="email_from"  style="width:100%" maxlength="250" value="<?=$_POST['email_from']?>" /></td>
  </tr>
  
   <tr>
    <td><label for="name"><strong><?php echo $vnT->lang['product']['name_to']; ?></strong> <font color="red">(*)</font> :</label>
    <input class="textfiled" name="name_to" type="text" id="name_to"  style="width:100%" maxlength="250" value="<?=$_POST['name_to']?>" />
    </td>
    <td><label for="email"><strong><?php echo $vnT->lang['product']['email_to']; ?></strong> <font color="red">(*)</font> :</label>
    <input class="textfiled" name="email_to" type="text" id="email_to"  style="width:100%" maxlength="250" value="<?=$_POST['email_to']?>" /></td>
  </tr>
  
  <tr>
    <td colspan="2" ><label for="message"><strong><?php echo $vnT->lang['product']['message']; ?></strong> <font color="red">(*)</font> :</label>
     <textarea class="textarea" name="content"  style="width:100%" rows="5"  id="content"><?=$_POST['content']?></textarea>
     </td>
  </tr>
  
  <tr>
    <td align="left" nowrap>  
    	<strong><?php echo $vnT->lang['product']['security_code']; ?> :</strong>  <font color="red">(*)</font> <input id="security_code" name="security_code" size="15" maxlength="6" class="textfiled"/>&nbsp;<img src="<?=$img_code?>" align="absmiddle" />
        
    </td>
    <td align="right">  
    		<input type="hidden" name="h_code" value="<?=$num?>">
        <button  id="btnSend" name="btnSend" type="submit" class="btn" value="<?php echo $vnT->lang['product']['btn_send']; ?>" ><span ><?php echo $vnT->lang['product']['btn_send']; ?></span></button>&nbsp; 
        <button  id="btnReset" name="btnReset" type="reset" class="btn" value="<?php echo $vnT->lang['product']['btn_reset']; ?>" onClick="self.parent.tb_remove();" ><span ><?php echo $vnT->lang['product']['btn_reset']; ?></span></button>   </td>
  </tr>
  
</table>
 
  
  </form>
<?php
	}else{
?>
	 <table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
	<td height="150"><div align="center" class="font_err"><a href = "javascript:self.parent.tb_remove();">Close Window</a> </div> </td>
	</tr>
	</table> 
<?php  
	}
?>
		 
</div>
</body>

</html>