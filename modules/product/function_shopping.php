<?php
/*================================================================================*\
|| 							Name code : function_shopping.php		 		 											  # ||
||  				Copyright © 2007 by Thai Son - CMS vnTRUST                					# ||
\*================================================================================*/
/**
* @version : 1.0
* @date upgrade : 17/12/2007 by Thai Son
**/
if ( !defined('IN_vnT') )	{ die('Access denied');	}

if(!class_exists('Cart')){
	// shopping
	include("cart.inc.php");
	$vnT->cart = $cart = new Cart; // khoi tao $seesion va cookie $ShoppingCart
}

/*-------------- create_link_shopping --------------------*/
function create_link_shopping ($act,$extra=""){
	global $vnT,$func,$DB,$conf;
	$text = LINK_MOD."/".$act.".html";
	$text .= ($extra) ? "/".$extra : "" ;
	return $text;
}
function nav_shopping($act='cart'){
	global $vnT,$input,$conf;
	$Nav[0]['act'] = array("cart"); 
	$Nav[0]['title'] = $vnT->lang['product']['f_cart'];
	$Nav[0]['link'] = create_link_shopping("cart"); 
	
	$Nav[1]['act'] =  array("checkout_address","checkout_method"); 
	$Nav[1]['title'] = $vnT->lang['product']['f_checkout_method'];
	$Nav[1]['link'] = create_link_shopping("checkout_method"); 
	
	$Nav[2]['act'] = array("checkout_confirmation"); 
	$Nav[2]['title'] = $vnT->lang['product']['f_checkout_confirmation'];
	$Nav[2]['link'] = create_link_shopping("checkout_confirmation");
	
	$Nav[3]['act'] = array("checkout_finished"); 
	$Nav[3]['title'] = $vnT->lang['product']['finished'];
	$Nav[3]['link'] = "";
	
	for ($n=0;$n<count($Nav);$n++) {
		if (in_array($act,$Nav[$n]['act'])){
			$current = $n;
		}
	}
	$text = '<ul class="list-inline text-center">';
	for ($i=0;$i<count($Nav);$i++) {
		$navItem = $Nav[$i];
		if ( in_array($act,$navItem['act'])){
			$text .= '<li class="active">
			            <span class="number">'.($i+1).'</span>
			            <span class="text">'.$navItem['title'].'</span>
				        </li>';
		}else{
			if($i<$current){
				$text .= '<li class="active">
		                <span class="number">'.($i+1).'</span>
		                <span class="text">'.$navItem['title'].'</span>
		            	</li>';
			}else{
				$text .= '<li>
		                <span class="number">'.($i+1).'</span>
		                <span class="text">'.$navItem['title'].'</span>
		            	</li>';
			}
		}
	}
	$text .= "</ul>";
	$textout = '<div class="style_step">'.$text.'<div class="clear"></div></div>';
	return $textout;
}
function check_product_order ($p_id){
	global $DB, $input, $func, $vnT;
	$text = array();
	$text['ok'] = 1;
	$res_ck= $vnT->DB->query("SELECT p.p_id, pd.p_name FROM products p, products_desc pd
														WHERE p.p_id=pd.p_id AND display=1 AND lang='$vnT->lang_name' AND p.p_id=".$p_id);
	if($row_ck = $vnT->DB->fetch_row($res_ck)) {

	}else{
		$text['ok']= 0;
		$text['mess']= str_replace("{pID}",$p_id,$vnT->lang['product']['product_not_found']);
	}
	return $text;
}
function get_check_promotion_code ($list_promotion_code , $total_cart){
	global $DB, $input, $func, $vnT;
	$text = array();
	$text['ok']=1;
	$mess = '';
	$promotion_price =0; $code_ids ='';
	$arr_promotion_code = @explode(",",$list_promotion_code);
	$err_text = '';
	//kiem tra code
	foreach ($arr_promotion_code as $promotion_code) {
		$res_ck = $vnT->DB->query("SELECT * FROM promotion_coupon WHERE display=1 AND  status=0  AND code='" . trim($promotion_code) . "' ") ;
		if($row_ck = $vnT->DB->fetch_row($res_ck)){
			if ($row_ck['quantity_now'] >= $row_ck['quantity'] && $row_ck['quantity'] > 0){
				$err_text .= '<div>'.str_replace("{code}",$promotion_code,$vnT->lang['product']['err_promotion_quantity']).'</div>';
			}
			$currday = date("Ymd");
			$day_end = ($row_ck['day_end']) ? date("Ymd", $row_ck['day_end']) : $currday;
			if (($day_end - $currday) < 0 && $row_ck['day_end']>0) {
				$err_text .= '<div>'.str_replace("{code}",$promotion_code,$vnT->lang['product']['err_promotion_date']).'</div>';
			}
			if ($row_ck['price_type'] == "%") {
				$prom_price = $total_cart * ($row_ck['price'] / 100);
			} else {
				$prom_price = $row_ck['price'];
			}
			$promotion_price += round($prom_price, 2);
			$code_ids .= $row_ck['id'].",";

		}else{
			$err_text .= '<div>'. str_replace("{code}",$promotion_code, $vnT->lang['product']['err_promotion_code_invalid']).'</div>';
		}
	}//end foreach
	if($err_text){
		$text['ok'] = 0;
		$mess = $err_text;
	}
	$text['code_ids'] = substr($code_ids,0,-1) ;
	$text['promotion_code'] = $list_promotion_code;
	$text['promotion_price'] = $promotion_price;
	$text['mess'] = $mess;
	return $text;
}
function get_status_instok ($status_id){
  global $func, $DB, $conf, $vnT;
  $in_stock = 1;
  $result = $vnT->DB->query("SELECT * FROM product_status where status_id in (" . $status_id . ") ");
  while ($row = $vnT->DB->fetch_row($result))
  {
    if ($row['is_order'] == 0) $in_stock = 0;
  }
  return $in_stock;
}
function get_status_outstok (){
  global $func, $DB, $conf, $vnT;
  $out=5;
  $result = $vnT->DB->query("SELECT * FROM product_status where  name='empty' ");
  if ($row = $vnT->DB->fetch_row($result)){
    $out = $row['status_id'] ;
  }
  return $out;
}
function get_price_pro ($price,$default="0 đ"){
	global $func,$DB,$conf,$vnT;
	if ($price){
		$price = $func->format_number($price)." đ" ;
	}else{
		$price = $default;
	}
	return $price;
}
function get_price_pro_en ($price,$default="0"){
	global $DB,$input,$func,$vnT,$conf;
	$rate = ($vnT->setting['rate']) ? ($vnT->setting['rate']) : 1 ;
	if ($price){
		$price = floatval(round($price/$rate,0));				
		$price = sprintf( "%.2f",$price );					
	}else{
		$price = $default;
	}
	return $price;
}
function get_price_pro_vn ($price,$default="Call"){
	global $func,$DB,$conf,$vnT;
	if ($price){
		$price = $price*$vnT->conf['rate'];
		$price = round($price,-3);
	}else{
		$price =$default;
	}
	return $price;
}
function get_shopping_pname ($p_name,$link,$tooltip=""){
	global $DB,$input,$func,$vnT;
	$p_name = $func->HTML($p_name);
	if ($tooltip) $extra =  "onmouseover=\"return ddrivetip('{$tooltip}',150)\" onmouseout=\"hideddrivetip();\" ";
	$text ="<a href=\"{$link}\" {$extra}  >".$p_name."</a>";
	return $text ;
}
function get_info_address ($data,$type=""){
	global$DB,$input,$func,$vnT,$conf;
	if($type=="shipping"){
		$text = '<div>'.$vnT->lang['product']['full_name'].': '.$data['c_name'].'</div>';
		$text.= '<div>'.$vnT->lang['product']['address'].' : '.$data['c_address'];
		if($data['c_ward'])
			$text .= ", ".$vnT->lib->get_ward_name($data['c_ward']);
		if($data['c_state'])
			$text .= ", ".$vnT->lib->get_state_name($data['c_state']);
		if($data['c_city'])
			$text .= ", ".$vnT->lib->get_city_name($data['c_city']);
		$text .= '</div>';
		$text .= '<div>'.$vnT->lang['product']['phone'].' : '.$data['c_phone'].'</div>';
	}else{
		$text = '<div>'.$vnT->lang['product']['full_name'].': '.$data['d_name'].'</div>';
		$text.= '<div>'.$vnT->lang['product']['address'].' : '.$data['d_address'];
		if($data['d_ward'])
			$text .= ", ".$vnT->lib->get_ward_name($data['d_ward']);
		if($data['d_state'])
			$text .= ", ".$vnT->lib->get_state_name($data['d_state']);
		if($data['d_city'])
			$text .= ", ".$vnT->lib->get_city_name($data['d_city']);
		$text .= '</div>';
		$text .= '<div>'.$vnT->lang['product']['phone'].' : '.$data['d_phone'].'</div>';
		if($data['d_email'])
			$text .= '<div>E-mail : '.$data['d_email'].'</div>';
	}
	return $text;
}
function get_cart_address ($data,$type=""){
	global$DB,$input,$func,$vnT,$conf;
	if($type=="shipping"){
		$text = '<p>'.$vnT->lang['product']['full_name'].': '.$data['c_name'].'</p>';
		$text.= '<p>'.$vnT->lang['product']['address'].' : '.$data['c_address'];
		if($data['c_ward'])
			$text .= ", ".$vnT->lib->get_ward_name($data['c_ward']);
		if($data['c_state'])
			$text .= ", ".$vnT->lib->get_state_name($data['c_state']);
		if($data['c_city'])
			$text .= ", ".$vnT->lib->get_city_name($data['c_city']);
		$text .= '</p>';
		$text .= '<p>'.$vnT->lang['product']['phone'].' : '.$data['c_phone'].'</p>';
	}else{
		$text = '<p>'.$vnT->lang['product']['full_name'].': '.$data['d_name'].'</p>';
		$text.= '<p>'.$vnT->lang['product']['address'].' : '.$data['d_address'];
		if($data['d_ward'])
			$text .= ", ".$vnT->lib->get_ward_name($data['d_ward']);
		if($data['d_state'])
			$text .= ", ".$vnT->lib->get_state_name($data['d_state']);
		if($data['d_city'])
			$text .= ", ".$vnT->lib->get_city_name($data['d_city']);
		$text .= '</p>';
		$text .= '<p>'.$vnT->lang['product']['phone'].' : '.$data['c_phone'].'</p>';
		$text .= '<p>E-mail : '.$data['d_email'].'</p>';
	}
	return $text;
}
function fetch_module_payment($name){
	global $vnT,$DB,$input,$conf;
	$array= array();
	$reuslt = $DB->query ("select config from payment_method where name='{$name}' ") ;
	if ($row = $DB->fetch_row($result)){
		if ($row['config'])
			$array = unserialize($row['config']);
	}
	return $array;
}
function get_array_address(){
	global $vnT,$DB,$func,$input,$conf;
	$array= array();
	$res = $DB->query ("select * from order_address where address_id=".(int)$_SESSION['checkout_address']) ;
	if($row = $DB->fetch_row($res)){
		$array['mem_id'] =$row['mem_id'];
		$array['d_company'] =$row['d_company'];
		$array['d_name'] = $row['d_name'];
		$array['d_email'] = $row['d_email'];
		$array['d_phone'] = $row['d_phone'];
		$array['d_mobile'] =$row['d_mobile'];
		$array['d_fax'] = $row['d_fax'];
		$array['d_address'] = $row['d_address'];
		$array['d_city'] = $row['d_city'];
		$array['d_zipcode'] = $row['d_zipcode'];
		$array['d_country'] = $row['d_country'];
		
		$array['c_name'] = $row['c_name'];
		$array['c_phone'] = $row['c_phone'];
		$array['c_mobile'] =$row['c_mobile'];
		$array['c_address'] = $row['c_address'];
		$array['c_city'] = $row['c_city'];
		$array['c_zipcode'] =$row['c_zipcode'];
		$array['c_country'] =$row['c_country'];
	}
	return $array;
}
function get_info_paypal(){
	global $vnT,$DB,$func,$input,$conf,$cart,$session;
	$array= array();
	$res = $DB->query ("select * from order_address where address_id=".(int)$_SESSION['checkout_address']) ;
	if ($row = $DB->fetch_row($res)){
		$arr_name = explode(" ",$row['d_name']);
		$array['fname'] = $arr_name[0];
		$array['lname'] = $arr_name[1];
		$array['phone'] = $row['d_phone'];
		$array['gender'] = $row['d_gender'];
		$array['address'] = $row['d_address'];
		$array['city'] = $row['d_city'];
		$array['state'] = $row['d_state'];
		$array['postcode'] = $row['d_postcode'];
		$array['country'] = $row['d_country'];
	}
	$result = $vnT->DB->query("SELECT * FROM shipping_method WHERE name= '".$_SESSION['shipping']."' ");
	if ($row = $DB->fetch_row($result)){
		$title = $vnT->func->HTML($row['title']);
		$description = $vnT->func->HTML($row['description']);
		if($row['s_type']==1){
			$ship_price = $vnT->arr_shipping[$array['city']];
		}else{
			$ship_price = $row['price'];
		}
		$dis_price=0;
		$a_discount = unserialize($row['discount']);
		if(is_array($a_discount))
		{
			ksort($a_discount);
			foreach ($a_discount as $key => $value){
				if ($cart->cart_total($session)>=$key)
					$dis_price = $value;
			}
		}

		if ($dis_price)
			$ship_price -= $dis_price;

		$array['ship_price'] = $ship_price;
	}
	
	return $array;
}
function get_info_google(){
	global $vnT,$DB,$func,$input,$conf,$cart,$session;
	$array= array();
	$result = $vnT->DB->query("select * from shipping_method where name= '".$_SESSION['shipping']."' ");
	if ($row = $DB->fetch_row($result)){
		$title = $vnT->func->HTML($row['title']);
		$description = $vnT->func->HTML($row['description']);
		$array['ship_title'] = $title;
		$array['ship_desc'] = $description;
		$array['ship_price'] = $row['price'];
		$a_discount = unserialize($row['discount']);
		ksort($a_discount);
		$dis_price=0;
		foreach ($a_discount as $key => $value){
			if ($cart->cart_total($session)>=$key)
				$dis_price = $value;
		}
		$array['dis_price'] = $dis_price;
	}
	
	return $array;
}
function get_discount($ship_name){
	global $vnT,$DB,$func,$input,$conf,$cart,$session;
	$dis_price= 0;
	$result = $vnT->DB->query("select * from shipping_method where name= '".$ship_name."' ");
	if ($row = $DB->fetch_row($result)){
		$a_discount = unserialize($row['discount']);
		ksort($a_discount);
		foreach ($a_discount as $key => $value){
			if ($cart->cart_total($cart->session)>=$key)
				$dis_price = $value;
		}
	}
	return $dis_price;
}
function get_method_name ($table,$name){
	global $DB,$input,$func,$vnT,$conf;
	$text="";
	$reuslt = $DB->query ("select title from {$table} where name='{$name}' ") ;
	if ($row = $DB->fetch_row($result)){
		$text = $func->HTML($func->fetch_array($row['title']));
	}
	return $text;
}
function list_type_price($selname,$list_price,$did,$ext=""){
	global $func,$DB,$conf,$vnT;
	$arr_price = unserialize($list_price);
	$out = "<select size=1  name=\"{$selname}\"  id=\"type_price\" class=\"select\" align=absmiddle {$ext} >";
	$res= $DB->query("SELECT * FROM product_price WHERE display=1 
										ORDER BY displayorder ASC, id DESC ");
	if($DB->num_rows($res)){
		$list_option="";
		while ($row = $DB->fetch_row($res)){
			if($arr_price[$row['id']]){
				if ($did == $row['id'] ){
					$list_option.="<option value=\"{$row['id']}\" selected>".$func->HTML($row['name'])."</option>";
				}else{
					$list_option.="<option value=\"{$row['id']}\">".$func->HTML($row['name'])."</option>";
				}
			}
		}
	}
	if($list_option){
		$out .= $list_option ;
	}else{
		$out.="<option value=\"0\"> ".$vnT->lang['product']['no_have_type_price']." </option>";
	}
	$out .='</select>';
	return $out;
}
function List_Country ($selname="country",$did="",$ext=""){
	global $vnT,$conf,$DB,$func;
	$text= "<select name=\"{$selname}\" id=\"{$selname}\" class='select'  {$ext}   >";
	$sql="SELECT * FROM iso_countries where display=1 ORDER BY name ASC ";
	$text.="<option value=\"0\" selected>".$vnT->lang['product']['select_country']."</option>";
	$result = $DB->query ($sql);
	while ($row = $DB->fetch_row($result)){
		if ($row['iso']==$did){
			$text .= "<option value=\"{$row['iso']}\" selected>".$func->HTML($row['name'])."</option>";
		} else{
			$text .= "<option value=\"{$row['iso']}\">".$func->HTML($row['name'])."</option>";
		}
	}
	$text.="</select>";
	return $text;
}
function List_City ($selname="city",$country,$did="",$ext=""){
	global $vnT,$conf,$DB,$func;
	$text= "<select name=\"{$selname}\" id=\"{$selname}\" class='select'  {$ext}   >";
	$text.="<option value=\"0\" selected>-- ".$vnT->lang['product']['select_city']." --</option>";
	$sql="SELECT * FROM iso_cities where display=1 and country='$country'  ORDER BY name ASC  ";
	$result = $DB->query ($sql);
	while ($row = $DB->fetch_row($result)){
		if ($row['code']==$did){
			$text .= "<option value=\"{$row['id']}\" selected>".$func->HTML($row['name'])."</option>";
		} else{
			$text .= "<option value=\"{$row['id']}\">".$func->HTML($row['name'])."</option>";
		}
	}
	$text.="</select>";
	return $text;
}
function get_group_default(){
	global $vnT,$func,$DB,$conf;
	$setting = array();
	$result = $DB->query("select * from member_setting");
	$row = $DB->fetch_row($result);	
	foreach ($row as $k => $v)	{
		$setting[$k] = stripslashes($v);
	}
	return (int)$setting['group_default'];
}
function do_ShowCart ($info){
	global $vnT,$input,$cart,$DB,$func,$conf;
	$cart_total = $cart->cart_total($cart->session);
	$row_cart="";
	$contents = $cart->display_contents($cart->session);
	$x = 0; $w = 80;
	while($x < $cart->num_items($cart->session)){
		$id = $contents["id"][$x] ;		
		$item_type = $contents["item_type"][$x];
		$item_id = $contents["item_id"][$x];
		$link = $contents["item_link"][$x];
		$src_pic = $vnT->func->get_src_modules ($contents["item_picture"][$x], $w, 0,0);
		$p_name = $vnT->func->HTML($contents['item_title'][$x]);
		$item_title = '<div class="img"><a href=""><img src="'.$src_pic.'" alt="'.$p_name.'"></a></div>';
		$item_title.= '<div class="i-title"><a href="'.$link.'">'.$p_name.'</a></div>';
		$item_title.= '<div class="i-code">#'.$contents["item_maso"][$x].'</div>';
		$item_title.= '<div class="clear"></div>';
 		$row['stt'] = $x+1;
 		$price_old = '';
 		if($contents["price_old"][$x])
 			$price_old = '<span class="normal">'.get_price_pro($contents["price_old"][$x]).'</span>';
		$price = get_price_pro($contents["price"][$x]);																
		$quantity = $contents['quantity'][$x];
		$total = get_price_pro($contents['total'][$x]);
		$row_cart .= '<tr>
                    <td>'.$item_title.'</td>
                    <td>'.$quantity.'</td>
                    <td><div class="price_simple">'.$price.'</div></td>
                    <td><div class="price_total">'.$total.'</div></td>
                  </tr>';
		$x++;
	}
	$total_price = $cart_total + $info['s_price'];
	$textout = '<table class="addtocart2 res2"><thead>';
	$textout.= '<tr>
                <td>'.$vnT->lang['product']['product'].'</td>
                <td>SL</td>
                <td>'.$vnT->lang['product']['cart_price'].'</td>
                <td>'.$vnT->lang['product']['total'].'</td>
              </tr>';
  $textout.= '</thead><tbody>'.$row_cart.'</tbody></table>';
  $textout.= '<div class="coupon"><div class="totalPrice"><ul><li>
                <div class="p">'.$vnT->lang['product']['total_price'].'</div>
                <div class="l v">'.get_price_pro($total_price).'</div>
                <div class="clear"></div>
              </li></ul></div><div class="clear"></div></div>';
	return $textout;
}
function email_ShowCart($info){
	global $vnT,$input,$cart,$func,$conf;
	$html_row_cart  ="";
	$order_id = $info['order_id'];
	$sql_cart = "select * from order_detail where order_id =".$order_id;
	$result_cart = $vnT->DB->query($sql_cart);
	$x = 0; $w =65 ;
	while ($row_cart = $vnT->DB->fetch_row($result_cart)){
		$row_cart['stt'] = ($x+1);
		$item_type = $row_cart["item_type"] ;
		$item_id = $row_cart["item_id"] ;
		$price = $row_cart["price"] ;
		$total = ($price * $row_cart['quantity']);
		$link = ($vnT->muti_lang) ? $vnT->conf['rooturl'].$vnT->lang_name."/" : $vnT->conf['rooturl'] ;
		$link .= $row_cart["item_link"];
		$src_pic = ($row_cart["item_picture"]) ? $vnT->func->get_src_modules ($row_cart["item_picture"], $w, 0,0) : ROOT_URL."modules/product/images/nophoto.gif" ;
		$pic = "<a href='{$link}' target='_blank'><img src=\"{$src_pic}\" width=\"{$w}\" alt='".$row_cart['item_title']."' /></a>";
		$item_title =	"<table border='0' ><tr><td width='70'>".$pic."</td><td><a href='".$link."' target='_blank' ><strong >".$row_cart['item_title']."</strong></a></td></tr></table>";
		$row_cart['pic'] =  $pic ;
		$row_cart['item_title'] = $item_title;
		$row_cart['price'] = ($row_cart['item_price']) ? get_price_pro($row_cart['item_price']) :  get_price_pro($row_cart['price']) ;
		$row_cart['quantity'] = $row_cart['quantity'];
		$row_cart['total'] = get_price_pro($total, 0);
		$html_row_cart .='<tr bgcolor="f7f7f7" height="20">
											 	<td style="padding-left:5px" align="left">'.$row_cart['item_title'].'</td>
												<td align="center">'.$row_cart['price'].'</td>
												<td align="center">'.$row_cart['quantity'].'</td>
												<td align="center">'.$row_cart['total'].'</span></td>
											</tr>';
		$x ++;
	}
	$data['html_row_cart'] = $html_row_cart;
	$discount = $info['promotion_price'] + $info['ctv_price'] ;
	if($discount){
		$data['price_other'] = '<tr bgcolor="#EEEEEE">
															<td height="20" colspan="3" align="right"><b>Giảm thêm :</b> </td>
															<td  height="20" align="center"  >- '.get_price_pro($discount).'</td>
														</tr>';
	}
	$data['total_cart'] = get_price_pro($info['total_cart'] );
	$data['s_price'] =  get_price_pro($info['s_price']) ;
	$data['total_price'] = get_price_pro($info['total_price'] );

	return load_html("email_show_cart",$data);
}
function radio_yesno ($name = "radiobutton", $yes = 0,$ext=""){
  global $vnT, $conf;
  $txt = '';
  for ($i = 1; $i >= 0; $i --){
    $txt .= '<input type="radio" name="' . $name . '" id="' . $name . '" value="' . $i . '" ' . (($i == $yes) ? "checked" : "") . ' '.$ext.' align="absmiddle" >&nbsp;' . (($i) ? $vnT->lang['product']["yes"] : $vnT->lang['product']["no"]) . '&nbsp;&nbsp;&nbsp;';
  }
  return $txt;
}
function List_Gender ($did = 1){
  global $vnT, $conf;
  $arr[1]['id'] = 'male';
  $arr[1]['title'] = $vnT->lang['product']['male'];
  $arr[2]['id'] = 'female';
  $arr[2]['title'] = $vnT->lang['product']['female'];
  foreach ($arr as $key => $value) {
  	$checked = ($did == $key) ? 'checked' : '';
  	$txt .= '<label for="'.$value['id'].'"><input name="gender" type="radio" id="'.$value['id'].'" value="'.$key.'" '.$checked.'>'.$value['title'].'</label>';
  }
  return $txt;
}
function get_status_default (){
	global $vnT,$conf,$DB,$func;
	$text=1;
	$result = $DB->query ("SELECT status_id FROM order_status where display=1 and is_default=1 ");
	if ($row = $DB->fetch_row($result)){
		$text = $row['status_id'];
	}
	return $text;
}
function get_status_order($id){
	global $func,$DB,$conf,$vnT;
	$text = "UnKnow";
	$result = $DB->query ("SELECT * FROM order_status where status_id=$id");
	if ($row = $DB->fetch_row($result)){
		$text = $func->fetch_array($row['title']);
		if($row['is_default']) {
			$text = "<b class=red>".$text."</b>";
		}
		if($row['is_complete']) {
			$text = "<b class=blue>".$text."</b>";
		}

	}
	return $text;
}
function List_Country_Shopping ($selname="country",$did="",$ext=""){
	global $vnT ,$input;
	$text= "<select name=\"{$selname}\" id=\"{$selname}\" class='select form-control'  {$ext}   >";
	$text.="<option value=\"0\" selected>".$vnT->lang['product']['select_country']."</option>";
	$sql="SELECT * FROM iso_countries where display=1 ORDER BY name ASC ";
	$result = $vnT->DB->query ($sql);
	while ($row = $DB->fetch_row($result)){
		if ($row['iso']==$did){
			$text .= "<option value=\"{$row['iso']}\" selected>".$func->HTML($row['name'])."</option>";
		} else{
			$text .= "<option value=\"{$row['iso']}\">".$func->HTML($row['name'])."</option>";
		}
	}
	$text.="</select>";
	return $text;
}
function List_City_Shopping ($selname="city",$country,$did="",$ext=""){
	global $vnT ,$input;
	$text= "<select name=\"{$selname}\" id=\"{$selname}\" class='form-control' ".$ext."   >";
	$text.="<option value=\"0\" selected>-- ".$vnT->lang['product']['select_city']." --</option>";
	$sql = "SELECT * FROM iso_cities WHERE country='".$country."' AND display=1  ORDER BY c_order ASC , name ASC  ";
	$result = $vnT->DB->query($sql);
	while ($row = $vnT->DB->fetch_row($result)) {
		$selected =  ($row['id'] == $did)  ? "selected" : "";
		$text .= "<option value=\"{$row['id']}\" {$selected} >" . $vnT->func->HTML($row['name']) . "</option>";
	}
	$text.="</select>";
	return $text;
}
function List_State_Shopping ($selname = 'states', $city = '', $did = '' ,$ext=""){
	global $vnT ,$input;
	$text= "<select name=\"{$selname}\" id=\"{$selname}\" ".$ext."  >";
	$text.="<option value=\"0\" selected>-- ".$vnT->lang['product']['select_state']." --</option>";
	$sql = "SELECT * FROM iso_states where display=1 and city=".$city."  ORDER BY s_order ASC , name ASC  ";
	$result = $vnT->DB->query($sql);
	while ($row = $vnT->DB->fetch_row($result)) {
		$selected =  ($row['id'] == $did)  ? "selected" : "";
		$text .= "<option value=\"{$row['id']}\" {$selected} >" . $vnT->func->HTML($row['name']) . "</option>";
	}
	$text .= '</select>';
	return $text;
}
function List_Ward_Shopping ($selname = 'wards', $state = '', $did = '' ,$ext=""){
	global $vnT ,$input;
	$text= "<select name=\"{$selname}\" id=\"{$selname}\" class='form-control' ".$ext.">";
	$text.="<option value=\"0\" selected>-- ".$vnT->lang['product']['select_ward']." --</option>";
	$sql = "SELECT * FROM iso_wards where display=1 AND state=$state ORDER BY w_order ASC, name ASC ";
	$result = $vnT->DB->query($sql);
	while ($row = $vnT->DB->fetch_row($result)) {
		$selected =  ($row['id'] == $did)  ? "selected" : "";
		$text .= "<option value=\"{$row['id']}\" {$selected}>".$vnT->func->HTML($row['name'])."</option>";
	}
	$text .= '</select>';
	return $text;
}
function get_price_item($type = 0, $p_id, $subid = 0){
	global $vnT,$DB;
	$price = 0;
	if($type==1){
		$result = $DB->query("SELECT price FROM products_sub WHERE parentid = {$p_id} AND subid=$subid");
		if($row = $DB->fetch_row($result))
			$price =  $row['price'];
	}else{
		$result = $DB->query("SELECT price FROM products WHERE p_id=$p_id");
		if($row = $DB->fetch_row($result))
			$price = $row['price'];
	}
	return $price;
}
function check_onhand($pID,$subid =0){
	global $DB, $input, $func, $vnT;
	if($subid)
		$res_ck= $vnT->DB->query("SELECT onhand FROM products_sub WHERE parentid=$pID AND subid = {$subid}");
	else	
		$res_ck= $vnT->DB->query("SELECT p_id,onhand FROM products WHERE p_id=".$pID);
	if($row = $DB->fetch_row($res_ck)){
		return $row['onhand'];
	}else{
		return 0;
	}
}
function get_item_cart($id){
	global $vnT,$DB;
	$result = $DB->query("SELECT item_id,subid FROM order_shopping WHERE id = {$id}");
	if($row = $DB->fetch_row($result)){
		return $row;
	}
}
?>