<?php
/*================================================================================*\
|| 							Name code : main.php 		 		 																	  # ||
||  				Copyright © 2007 by Thai Son - CMS vnTRUST                					# ||
\*================================================================================*/
/**
 * @version : 1.0
 * @date upgrade : 11/12/2007 by Thai Son
 **/
if (! defined('IN_vnT'))
{
  die('Access denied');
}
$nts = new sMain();
class sMain
{
  var $output = "";
  var $skin = "";
  var $linkUrl = "";
  var $module = "product";
  var $act = "category";

  function sMain (){
    global $vnT, $input;
    include ("function_" . $this->module . ".php");
    loadSetting();
    $this->skin = new XiTemplate( DIR_MODULE ."/". $this->module . "/html/category.tpl");
    $this->skin->assign('DIR_MOD', DIR_MOD);
    $this->skin->assign('LANG', $vnT->lang);
    $this->skin->assign('INPUT', $input);
    $this->skin->assign('CONF', $vnT->conf);
    $this->skin->assign('DIR_IMAGE', $vnT->dir_images);
    $vnT->html->addStyleSheet(DIR_MOD . "/css/" . $this->module . ".css");
    $vnT->html->addScript(DIR_MOD . "/js/" . $this->module . ".js");
		$vnT->setting['menu_active'] = $this->module;
    //check category
    $cat_id = (int) $input['catID'];
    $res_ck= $vnT->DB->query("SELECT * FROM product_category n,  product_category_desc nd
															WHERE n.cat_id=nd.cat_id AND display=1 AND lang='$vnT->lang_name'
															AND n.cat_id=$cat_id");
    if ($row = $vnT->DB->fetch_row($res_ck)){
			if ($row['metadesc'])	$vnT->conf['meta_description'] = $row['metadesc'];
			if ($row['metakey'])	$vnT->conf['meta_keyword'] = $row['metakey'];
			if ($row['friendly_title']) $vnT->conf['indextitle'] =  $row['friendly_title'];
			$link_seo = ($vnT->muti_lang) ? ROOT_URL.$vnT->lang_name."/" : ROOT_URL ;
			$link_seo .= $row['friendly_url']. ".html";
			$vnT->conf['meta_extra'] .= "\n".'<link rel="canonical" href="'.$link_seo.'" />';
			$vnT->conf['meta_extra'] .= "\n". '<link rel="alternate" media="handheld" href="'.$link_seo.'"/>';
			if($vnT->muti_lang>0){
				$res_lang= $vnT->DB->query("SELECT friendly_url,lang FROM product_category_desc
																		WHERE cat_id=".$row['cat_id']." AND display=1 ");
				while ($row_lang = $vnT->DB->fetch_row($res_lang)) {
					$link_lang = create_link ("category",$row['cat_id'],$row_lang['friendly_url']);
					$vnT->link_lang[$row_lang['lang']]=str_replace($vnT->link_root,ROOT_URI.$row_lang['lang']."/",$link_lang);
				}
			}
      $data['cat_name'] = $row['cat_name'];
      $data['slogan'] = $row['slogan'];
      $data['src'] = MOD_DIR_UPLOAD.'/'.$row['picture'];
      $data['src_b'] = MOD_DIR_UPLOAD.'/'.$row['picture_b'];
      $data['description'] = $row['description'];
		  $data['main'] = $this->list_product($row);
      $data['news_focus'] = $this->get_news_focus();
    } else {
			$linkref = LINK_MOD.".html";
      @header("Location: ".$linkref."");
      echo "<meta http-equiv='refresh' content='0; url=".$linkref."'/>";
    }
		$navation = get_navation($input['catID']);
    $data['navation'] = $vnT->lib->box_navation($navation);
    $vnT->setting['banner'] = $this->get_category_banner($row);
    $data['fixed_sidebar'] = $vnT->lib->fixed_sidebar();
    $this->skin->assign("data", $data);
    $this->skin->parse("modules");
    $vnT->output .= $this->skin->text("modules");
  }
	function list_category () {
		global $vnT,$func,$DB,$input;
		$catID = (int) $input['catID'];
		$textout="";
		$start = 0; $n= 4;
	  $view =1; $num_row=3;
		$sql_cat="SELECT * FROM product_category n, product_category_desc nd
							WHERE n.cat_id=nd.cat_id AND display=1 AND lang='$vnT->lang_name' AND parentid=$catID
							ORDER BY cat_order DESC , date_post DESC ";
		$result_cat = $vnT->DB->query($sql_cat);
		if ($vnT->DB->num_rows($result_cat)){
			$text="";
			while ($row_cat = $vnT->DB->fetch_row($result_cat)){
				$cat_id = (int)$row_cat['cat_id'];
				$link_cat = create_link("category",$cat_id,$row_cat['cat_name']);
				$where="";
				if ($cat_id){
					$where .= get_where_cat($cat_id);
				}
				$row_cat['row_product'] = row_product($where, $start, $n, $num_row, $view);
				$row_cat['cat_name']  = "<a href='".$link_cat."'>".$vnT->func->HTML($row_cat['cat_name'])."</a>";
				$row_cat['more']  = "<a href='".$link_cat."'>".$vnT->lang['product']['view_all']."</a>";
				$this->skin->reset("html_list_category");
				$this->skin->assign("data", $row_cat);
				$this->skin->parse("html_list_category");
				$text .= $this->skin->text("html_list_category");
			}
		}
		$nd['content'] = $text ;
 		$nd['f_title']= get_navation ($catID);
		$textout = $vnT->skin_box->parse_box("box_middle",$nd);
		return $textout;
	}
  function list_product ($info){
    global $DB, $func, $input, $vnT;
    $p = ((int) $input['p']) ? (int) $input['p'] : 1;
    $cat_id = (int) $info['cat_id'];
    $num_row=3; $view = 1;
    if ($cat_id) {
      $where .= get_where_cat($cat_id);
    }
    $status = $input['status'];
    if($status){
    	$where .= " AND FIND_IN_SET('$status',status)<>0 ";
    	$ext_pag .= "&status=".$status;
    }
    $attr = $input['attr'];
    if($attr){
    	$where .= " AND FIND_IN_SET('$attr',attr_id)<>0 ";
    	$ext_pag .= "&attr=".$attr;
    }
    //keyword
    if (isset($input['keyword'])) $keyword=$input['keyword'];
    if (!empty($keyword))
    {
      $arr_keyword = array();
      if (stristr($keyword, ' AND ') !== false)
      {
          $arr_keyword        = @explode('AND', $keyword);
          $operator   = " AND ";
      }
      elseif (stristr($keyword, ' OR ') !== false)
      {
          $arr_keyword        = @explode('OR', $keyword);
          $operator   = " OR ";
      }
      elseif (stristr($keyword, ' + ') !== false)
      {
          $arr_keyword        = @explode('+', $keyword);
          $operator   = " OR ";
      }
      else
      {
          $arr_keyword        = @explode(' ', $keyword);
          $operator   = " AND ";
      }

      $where .= 'AND (';
      foreach ($arr_keyword AS $key => $val)
      {
        $val_utf8 =  strtolower($vnT->func->utf8_to_ascii($val)) ;
        if ($key > 0 && $key < count($arr_keyword) && count($arr_keyword) > 1)
        {
            $where .= $operator;
        }
        $val        = trim($val);
        $where  .= "(p_name LIKE '%$val%' OR key_search LIKE '%$val_utf8%' )";
      }
      $where .= ')';
      $ext_pag .= "&keyword=" . rawurlencode($keyword);
    }

    if($input['price']){
      $tmp_price = @explode("-",$input['price']);
      $min = (int)$tmp_price[0];
      $max = (int)$tmp_price[1];
      if ($tmp_price[0]== 0 ){
        $where .= " AND price < $max ";
      }else if (!$tmp_price[1]){
        $where .= " AND price > $min ";
      }else{
        $where .= " AND ( price BETWEEN $min AND $max )";
      }
      $ext_pag .= "&price=".$input['price'];
    }
    $sql_num = "SELECT p.p_id FROM products p, products_desc pd
								WHERE p.p_id=pd.p_id AND display=1 AND lang='$vnT->lang_name' $where ";
    $res_num = $vnT->DB->query($sql_num);
    $totals = $vnT->DB->num_rows($res_num);
		if($input['display']){
      $n = ($input['display']);
      $ext_pag .= "&display=".$input['display'];
    }else{
      $n = ($vnT->setting['n_grid']) ? $vnT->setting['n_grid'] : 9;
    }
    $num_pages = ceil($totals / $n);
    if ($p > $num_pages) $p = $num_pages;
    if ($p < 1) $p = 1;
    $start = ($p - 1) * $n;
		if($num_pages>1){
			$root_link = create_link("category",$cat_id,$info['friendly_url']);
			$nav= "<div class='pagination'>".$vnT->func->htaccess_paginate($root_link,$totals,$n,$ext_pag,$p)."</div>";
		}
		switch ($input['sort']) {
      case 'new':
        $where .= " ORDER BY date_post DESC ";
        break;
      case 'old':
        $where .= " ORDER BY date_post ASC ";
        break;
      case 'low':
        $where .= " ORDER BY price ASC ";
        break;
      case 'hight':
        $where .= " ORDER BY price DESC ";
        break;
      default:
        $where .= " ORDER BY p_order DESC, date_post DESC ";
        break;
    }
		$data['description'] = $info['description'];
    $data['row_product'] = row_product($where, $start, $n, $num_row, $view);
    $data['nav'] = $nav;
    // $data['box_filter'] = box_filter();
    $data['Display_Filter'] = Display_Filter($input['display']);
    $data['Price_filter'] = Price_filter();
    $data['Sort_Filter'] = Sort_Filter($input['sort']);
    $data['Display_Filter'] = Display_Filter($input['page']);
    $data['list_cat_mobile'] = List_category();
    $data['status_filter'] = status_filter();
    $data['box_category1'] = box_category1($info['cat_id']);
    $data['count'] = $totals;
    $this->skin->assign("data", $data);
    $this->skin->parse("html_list");
    return $this->skin->text("html_list");
  //   $this->skin->assign("data", $data);
  //   $this->skin->parse("html_list");
  //   $this->skin->assign("data", $row_cat);
  //   $this->skin->parse("html_list");
		// // $nd['content'] = $this->skin->text("html_list");
 	// // 	$nd['f_title'] = '<h1>'.$info['cat_name'].'</h1>';
  // //   $nd['more'] = $vnT->func->HTML($info['slogan']);
		// // $textout = $vnT->skin_box->parse_box("box_middle",$nd);

  }

  function get_news_focus(){
    global $DB, $func, $input, $vnT;
    $textout = '';
    $sql = "SELECT * FROM news n, news_desc nd
            WHERE n.newsid = nd.newsid AND focus_main = 1 AND display = 1 AND lang = '{$vnT->lang_name}'
            ORDER BY focus_order ASC, n.newsid DESC LIMIT 0,3";
    $result = $DB->query($sql);
    if($num = $DB->num_rows($result)){
      while ($row = $DB->fetch_row($result)) {
        $data['link'] = create_link("detail",$row['p_id'],$row['friendly_url']);
        $picture = ($row['picture']) ? "news/".$row['picture'] : "news/no_photo.jpg";
        $data['date_post'] = @date('d/m/Y',$row['date_post']);
        $data['date_post_meta'] = @date('Y-m-d',$row['date_post']);
        $data['date_update_meta'] = @date('Y-m-d',$row['date_update']);
        $data['title'] = $vnT->func->HTML($row['title']);
        //$data['cat_name'] = $vnT->setting['news_cat'][$row['cat_id']];
        $data['short']=$vnT->func->cut_string($vnT->func->check_html($row['short'],'nohtml'),300,1);
        $data['src'] = $vnT->func->get_src_modules($picture, 632 ,'',1,'1.8:1');
        $this->skin->reset("item_news");
        $this->skin->assign("data", $data);
        $this->skin->parse("item_news");
        $nd['list_news'] .= $this->skin->text("item_news");
      }
      $nd['link_news'] = $vnT->link_root.$vnT->setting['seo_name'][$vnT->lang_name]['news'].'.html';
      $this->skin->reset("html_news_focus");
      $this->skin->assign("data", $nd);
      $this->skin->parse("html_news_focus");
      $textout = $this->skin->text("html_news_focus");
    }
    return $textout;
  }
  function get_category_banner($info){
    global $vnT,$DB;
    $html = '';
    if($info['picture_b']){
      $title = $vnT->func->HTML($info['cat_name']);
      $src = MOD_DIR_UPLOAD.'/'.$info['picture_b'];
      $html.= '<div class="vnt-main-top" style="min-height: 60px;"><div id="vnt-slide" class="slick-init">';
      $html.= '<div class="item"><div class="img"><img src="'.$src.'" alt="'.$title.'"/></div></div>';
      $html.= '</div></div>';
    }
    return $html;
  }
}
?>
