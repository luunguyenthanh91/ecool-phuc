<?php
/*================================================================================*\
|| 							Name code : cart.php 		 		 																	  # ||
||  				Copyright © 2007 by Thai Son - CMS vnTRUST                					# ||
\*================================================================================*/
/**
 * @version : 1.0
 * @date upgrade : 11/12/2007 by Thai Son
 **/
if (! defined('IN_vnT'))
{
  die('Access denied');
}
$nts = new sMain();
class sMain
{
  var $output = "";
  var $skin = "";
  var $linkUrl = "";
	var $module = "product";
	var $action = "checkout_process";

	function sMain(){
		global $vnT,$input,$func,$cart,$DB,$conf;
		include ("function_".$this->module.".php");
		loadSetting();
		include ("function_shopping.php");
		$this->skin = new XiTemplate( DIR_MODULE."/".$this->module."/html/".$this->action.".tpl");
    $this->skin->assign('DIR_MOD', DIR_MOD);
		$this->skin->assign('LANG', $vnT->lang);
		$this->skin->assign('INPUT', $input);
		$this->skin->assign('CONF', $vnT->conf);
		$this->skin->assign('DIR_IMAGE', $vnT->dir_images);		
		
		$vnT->html->addStyleSheet( DIR_MOD."/css/cart.css");
		$vnT->html->addScript(DIR_MOD."/js/cart.js");
		//active menu
		$vnT->setting['menu_active'] = $this->module;
		$vnT->conf['indextitle'] = $vnT->lang['product']['f_checkout_process'];
		$ok_process=1;		
		if ($cart->num_items($cart->session)==0){
			$mess = $vnT->lang['product']['empty_cart'];
			$$url = create_link_shopping("cart");
      $vnT->func->html_redirect($url, $mess);
			$ok_process=0;
		}
		$res = $vnT->DB->query("SELECT * FROM order_address WHERE session='".$cart->session."' ");
		$info = $vnT->DB->fetch_row($res);
		//check neu chua co address 
		if(empty($info['address_id'])){
			$link_ref= create_link_shopping("checkout_address");    
			$vnT->func->header_redirect($link_ref);
			$ok_process=0;
		}
		//empty shipping
		if (empty($info['shipping_method'])){ 
			$link_ref = create_link_shopping("checkout_method");
			$vnT->func->header_redirect($link_ref); 						
			$ok_process = 0;
		}
		//empty payment
		if (empty($info['payment_method'])){
			$link_ref = create_link_shopping("checkout_method");
			$vnT->func->header_redirect($link_ref);
			$ok_process=0;
		}
		//check sunmit
		if (empty($input['do_process'])){
			$link_ref = create_link_shopping("checkout_confirmation") ;
			$vnT->func->header_redirect($link_ref); 
			$ok_process=0;
		}
		if($ok_process){
  	  $_SESSION['do_finished'] = "";
			if($input['check_form_payment']){
				$data['content'] = $this->do_ProcessPaymentForm($info);
			}else{
				$data['content'] = $this->do_Transfer($info); 	
			}
		}
		$navation = get_navation (0,$vnT->lang['product']['f_checkout_process']);
		$data['navation'] = $vnT->lib->box_navation($navation);
    //$vnT->setting['banner'] = $vnT->lib->get_child_slide('child');
    $data['fixed_sidebar'] = $vnT->lib->fixed_sidebar();
		$this->skin->assign("data", $data);		
    $this->skin->parse("modules");
    $vnT->output .= $this->skin->text("modules");
	}
	function do_Transfer($info){
		global $vnT,$func,$DB,$input,$cart,$conf;
		$err = "";
		$today = date("Ymd");
		//tinh lai gia 
		$total_cart = $cart->cart_total($cart->session);
		$total_price = $total_cart;
		//phi van chuyen
		$s_price = $info['s_price'];
		$total_price += $s_price;
 		if(empty($err)){
			// cot address shipping
			$cot['mem_id'] = $vnT->user['mem_id'];
			$cot['d_name'] = $info['d_name'];
			$cot['d_email'] = $info['d_email'];
			$cot['d_phone'] = $info['d_phone'];
			$cot['d_address'] = $info['d_address'];
			$cot['d_city'] = $info['d_city'];
			$cot['d_country'] = $info['d_country'];
			$cot['d_state'] = $info['d_state'];

			$cot['c_name'] = $info['c_name'];
			$cot['c_phone'] = $info['c_phone'];
			$cot['c_address'] = $info['c_address'];
			$cot['c_city'] = $info['c_city'];
			$cot['c_country'] = $info['c_country'];
			$cot['c_state'] = $info['c_state'];

			$cot['shipping_method'] = $info['shipping_method'];
			$cot['shipping_name'] = $info['shipping_name'];
			$cot['payment_method'] = $info['payment_method'];
			$cot['payment_name'] = $info['payment_name'];

			$cot['bill'] = ($info['bill']) ? $info['bill'] : 0;
	 		$cot['bill_company'] = ($info['bill']) ? $info['bill_company'] : '';
	 		$cot['bill_address'] = ($info['bill']) ? $info['bill_address'] : '';
	 		$cot['bill_mst'] = ($info['bill']) ? $info['bill_mst'] : '';

			$cot['comment'] = $info['comment'];
			$cot['total_cart'] = $total_cart;
			$cot['s_price'] = $s_price;
			$cot['total_price'] = $total_price;

			$cot['status'] = get_status_default();
			$cot['date_order'] = time();
			$cot['lang'] = $vnT->lang_name;

			$ok = $vnT->DB->do_insert("order_sum", $cot);
			if ($ok) {
				$order_id = $vnT->DB->insertid();
				// update order code
				$str_so ="";
				for ($n=strlen(strval($order_id));$n<4;$n++){
					$str_so .="0";
				}
				$str_so.=$order_id;
				$order_code = $str_so."-".$today;
				$vnT->DB->query("UPDATE order_sum SET order_code='".$order_code."' WHERE order_id=".$order_id);
				// insert detail
				$contents = $cart->display_contents($cart->session);
				if ($cart->num_items($cart->session)) {
					$x = 0;
					while ($x < $cart->num_items($cart->session)) {
						$cot_d['order_id'] = $order_id;
						$cot_d['item_type'] = $contents["item_type"][$x];
						$cot_d['item_id'] = $contents["item_id"][$x];
						$cot_d['subid'] = $contents["subid"][$x];
						$cot_d['quantity'] = $contents["quantity"][$x];
						$cot_d['price'] = $contents["price"][$x];

						$cot_d['color'] = $contents["text_color"][$x];
						$cot_d['size'] = $contents["text_size"][$x];

						$cot_d['item_maso'] = $contents["item_maso"][$x];
						$cot_d['item_title'] = $contents["item_title"][$x];
						$cot_d['item_price'] = $contents["item_price"][$x];
						$cot_d['item_picture'] = $contents["item_picture"][$x];
						$cot_d['item_link'] = $contents["item_link"][$x];
						$vnT->DB->do_insert("order_detail", $cot_d);
						$vnT->DB->query( "UPDATE products SET num_buy=num_buy+".$contents["price"][$x]." 
															WHERE p_id='".$contents["item_id"][$x]."' ");
						$x ++;
					}
				}
				//update promotion
				// if ($info['promotion_code']) {
				// 	$vnT->DB->query("UPDATE promotion_coupon SET 	status=1 , date_used=".time()." WHERE id in(" . $check_promotion['code_ids'] . ") ");
				// }
			}//end if ok
		}else{
			$link_ref = create_link_shopping("cart");
			$vnT->func->header_redirect($link_ref,$vnT->func->html_err($err));
		}
		//load payment
		$name_payment = trim($info['payment_method']);    
		$vnT->module = fetch_module_payment($name_payment);		
    $vnT->module['order_id'] = $order_id;
		$vnT->module['order_code'] = $order_code;
    $vnT->module['s_price'] = $s_price;
    $vnT->module['total_price'] = $total_price;
		$path = PATH_ROOT . "/modules/product/payment/" . $name_payment . "/transfer.inc.php";
    $path_payment = (file_exists($path)) ? $path : "payment/transfer.inc.php";
    include ($path_payment);
		if(function_exists("ProcessPaymentForm")) {
			$arr_response = ProcessPaymentForm($info);
			if(is_array($arr_response)){
				foreach ($arr_response as $key => $val){
					$info[$key]	= $val ;
				}
			}
		}
		$data['hidden_value'] = fixedVars($info);
		$data['link_action'] = formAction($info);
    $data['link_cancel'] = LINK_MOD."/checkout_cancel.html/?order_code=".base64_encode($order_code);
    $data['nav_shopping'] = nav_shopping('checkout_confirmation');
		$this->skin->assign("data", $data);
		$this->skin->parse("checkout_process");
		$nd['content'] = $this->skin->text("checkout_process");		
		$nd['f_title'] = '<h1>'.$vnT->lang['product']['f_checkout_process'].'</h1>';
		return $vnT->skin_box->parse_box("box_middle",$nd);
	}
  function do_ProcessPaymentForm ($info){
    global $vnT, $func, $DB, $input, $cart, $session, $conf;
    $data = $input;
		$today = date("Ymd");
		if(empty($_SESSION['order_code'])){
			$total_cart = $cart->cart_total($cart->session);
			$total_price = $total_cart ;
			$s_price = $info['s_price'];
			$total_price += $s_price;
			// cot address shipping
			$cot['mem_id'] = $vnT->user['mem_id'];
			$cot['gender'] = $info['gender'];
			$cot['d_name'] = $info['d_name'];
			$cot['d_email'] = $info['d_email'];
			$cot['d_phone'] = $info['d_phone'];
			$cot['d_address'] = $info['d_address'];
			$cot['d_city'] = $info['d_city'];
			$cot['d_country'] = $info['d_country'];
			$cot['d_state'] = $info['d_state'];
			$cot['d_ward'] = $info['d_ward'];

			$cot['c_name'] = $info['c_name'];
			$cot['c_phone'] = $info['c_phone'];
			$cot['c_address'] = $info['c_address'];
			$cot['c_city'] = $info['c_city'];
			$cot['c_country'] = $info['c_country'];
			$cot['c_state'] = $info['c_state'];
			$cot['c_ward'] = $info['c_ward'];

			$cot['shipping_method'] = $info['shipping_method'];
			$cot['shipping_name'] = $info['shipping_name'];
			$cot['payment_method'] = $info['payment_method'];
			$cot['payment_name'] = $info['payment_name'];

			$cot['bill'] = ($info['bill']) ? $info['bill'] : 0;
	 		$cot['bill_company'] = ($info['bill']) ? $info['bill_company'] : '';
	 		$cot['bill_address'] = ($info['bill']) ? $info['bill_address'] : '';
	 		$cot['bill_mst'] = ($info['bill']) ? $info['bill_mst'] : '';

			$cot['comment'] = $info['comment'];
			$cot['total_cart'] = $total_cart;
			$cot['s_price'] = $s_price;
			$cot['total_price'] = $total_price;

			$cot['status'] = get_status_default();
			$cot['date_order'] = time();
			$cot['lang'] = $vnT->lang_name;

			$ok = $vnT->DB->do_insert("order_sum", $cot);
			if ($ok) {
				$order_id = $vnT->DB->insertid();
				$str_so ="";
				for ($n=strlen(strval($order_id));$n<4;$n++){
					$str_so .="0";
				}
				$str_so.=$order_id; 			
				$order_code = $str_so."-".$today;
				$vnT->DB->query("UPDATE order_sum SET order_code='" . $order_code . "' where order_id=".$order_id);
				// insert detail
				$contents = $cart->display_contents($cart->session);
				if ($cart->num_items($cart->session)) {
					$x = 0;
					while ($x < $cart->num_items($cart->session)) {
						$cot_d['order_id'] = $order_id;
						$cot_d['item_type'] = $contents["item_type"][$x];
						$cot_d['item_id'] = $contents["item_id"][$x];
						$cot_d['quantity'] = $contents["quantity"][$x];
						$cot_d['price'] = $contents["price"][$x];
						$cot_d['color'] = $contents["text_color"][$x];
						$cot_d['size'] = $contents["text_size"][$x];
						
						$cot_d['item_maso'] = $contents["item_maso"][$x];
						$cot_d['item_title'] = $contents["item_title"][$x];
						$cot_d['item_price'] = $contents["price"][$x];
						$cot_d['item_picture'] = $contents["item_picture"][$x];
						$cot_d['item_link'] = $contents["item_link"][$x];
						$vnT->DB->do_insert("order_detail", $cot_d);
						$x ++;
					} // ends loop for each product
				}
				$_SESSION['order_code'] = $order_code;
				$_SESSION['s_price'] = $s_price;
				$_SESSION['total_price'] = $total_price;
			}
		} else{
			$order_code	= $_SESSION['order_code'];
			$s_price = $_SESSION['s_price'];
			$total_price = $_SESSION['total_price'];
		}
		//load payment
		$name_payment = trim($info['payment_method']);
		$vnT->module = fetch_module_payment($name_payment);		
    $vnT->module['order_code'] = $order_code;
    $vnT->module['s_price'] = $s_price;
    $vnT->module['total_price'] = $total_price;
		$vnT->module['status_payment'] = get_status_payment();
		$path = PATH_ROOT . "/modules/product/payment/" . $name_payment . "/transfer.inc.php";
    $path_payment = (file_exists($path)) ? $path : "payment/transfer.inc.php";
    include ($path_payment);
		$an_response = ProcessPaymentForm ($info);
		if($an_response['ok']==1){
			$link_ref=create_link_shopping("checkout_finished")."/?order_code=".base64_encode($vnT->module['order_code']);
      @header("Location: {$link_ref}");
      echo "<meta http-equiv='refresh' content='0; url={$link_ref}' />";
			exit();
		}else{
			$err = $vnT->func->html_err($an_response['error']);
		}
		$data['link_action'] = create_link_shopping("checkout_process");
		$data['err'] = $err;
		$data['form_payment'] = formPayment($info);
		$data['nav_shopping'] = nav_shopping('checkout_confirmation');
    $this->skin->assign("data", $data);
    $this->skin->parse("html_payment_form");
    $nd['content'] = $this->skin->text("html_payment_form");
    $nd['f_title'] = '<h1>'.$vnT->lang['product']['f_checkout_process'].'</h1>';
    return $vnT->skin_box->parse_box("box_middle", $nd);
  }
}
?>