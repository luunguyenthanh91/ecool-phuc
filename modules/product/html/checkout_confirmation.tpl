<!-- BEGIN: modules -->
<div id="vnt-content">
    <div class="vnt-main-top">
      <div class="vnt-slide">
          <div id="vnt-slide" class="slick-init">
              <div class="item"><div class="img">
                  <img src="{DIR_IMAGE}/product/slide.jpg" alt="">
              </div></div>
          </div>
          <div id="vnt-navation" class="breadcrumb hidden-sm hidden-xs" itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb">
              <div class="wrapper">
                  <div class="navation">
                      {data.navation}
                      <div class="clear"></div>
                  </div>
              </div>
          </div>
      </div>
      
  </div>


<div class="wrapping">
  <div class="wrapCont">
    <div class="wrapper">
      {data.nav_shopping}
      <form action="{data.link_action}" method="post" name="f_checkout" onsubmit="return check_payment_form(this)">
        <div class="row">
          <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
            <div id="tableCart">
              {data.info_cart}
            </div>
          </div>
          <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
            {data.checkout_address}
            {data.info_shipping}
            {data.info_payment}
          </div>
        </div>
        <div class="divButton">
          <div class="fl"><a class="btnCart" href="{data.link_back}">{LANG.product.back_to_prev}</a></div>
          <div class="fr">
            <input name="do_process" type="hidden" value="1" />
            <button id="btnConfirm" name="btnConfirm" type="submit" class="btnCart blood" value="1">
              <span>{LANG.product.f_order_finished}</span>
            </button>
          </div>
          <div class="clear"></div>
        </div>
      </form>
    </div>
  </div>
  <div id="flagEnd"></div>
</div></div>
<!-- END: modules -->

<!-- BEGIN: checkout_address -->
<div class="boxCart">
  <div class="title">{LANG.product.payment_address}</div>
  <div class="content">
    {data.payment_address}
  </div>
</div>
<div class="boxCart">
  <div class="title">{LANG.product.shipping_address}</div>
  <div class="content">
    {data.shipping_address}
  </div>
</div>
{data.invoice_info}
<!-- END: checkout_address -->

<!-- BEGIN: html_info_method -->
<div class="boxCart">
  <div class="title">{data.f_title}</div>
  <div class="content">
    <p><strong>{data.title}</strong></p>
    {data.description}
  </div>
  {data.form_payment}
</div>
<!-- END: html_info_method -->