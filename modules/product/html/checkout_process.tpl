<!-- BEGIN: modules -->
<div id="vnt-content">
    <div class="vnt-main-top">
      <div class="vnt-slide">
          <div id="vnt-slide" class="slick-init">
              <div class="item"><div class="img">
                  <img src="{DIR_IMAGE}/product/slide.jpg" alt="">
              </div></div>
          </div>
          <div id="vnt-navation" class="breadcrumb hidden-sm hidden-xs" itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb">
              <div class="wrapper">
                  <div class="navation">
                      {data.navation}
                      <div class="clear"></div>
                  </div>
              </div>
          </div>
      </div>
      
  </div>
<div class="wrapping">
  <div class="wrapCont">
    <div class="wrapper">
      {data.content}
    </div>
  </div>
  <div id="flagEnd"></div>
</div></div>
<!-- END: modules -->

<!-- BEGIN: checkout_process -->
<script language="JavaScript" type="text/javascript">
  var mess = "{LANG.product.mess_cancel}";
  function check_cancel(theURL) {
  	if (confirm(mess)) {
  		window.location.href=theURL;
  	}			
  }
</script>
<script language="JavaScript" type="text/javascript">
	function submitDoc(formName) { 
    var obj;
    if (obj=findObj(formName)!=null){
      findObj(formName).submit(); 
    } else {
      alert('The form you are attempting to submit called \'' + formName + '\' couldn\'t be found. Please make sure the submitDoc function has the correct id and name.');
    }
  }
  window.onload = Redirector;
	function Redirector() {
    document.f_confirm.submit();			
	}
</script>
{data.nav_shopping}
<form action="{data.link_action}" method="post" name="f_confirm" id="f_confirm">
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr><td>&nbsp;</td></tr>
    <tr>
      <td>
		    <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr><td align="center" >{data.hidden_value}</td></tr>
          <tr>
            <td align="center" height="100">Please waitting ...... <br/><img src="{DIR_MOD}/images/progress.gif"  alt="" title="" onload="submitDoc('f_confirm')"  /></td>
          </tr>
	  	    <tr>
		        <td align="center"><input name="do_process" type="hidden" value="1" />&nbsp;&nbsp;</td>
          </tr>
        </table>
		  </td>
    </tr>
  </table>
</form>
<!-- END: checkout_process -->

<!-- BEGIN: html_payment_form -->
{data.err} 
<form action="{data.link_action}" method="post" name="f_confirm" onSubmit="return check_payment_form(this)" >
{data.form_payment} 
<p align="center">
<button  id="btnConfirm" name="btnConfirm" type="submit" class="btn" value="{LANG.product.btn_confirm}" ><span >{LANG.product.btn_confirm}</span></button>
<input name="do_process" type="hidden" value="1" />
</p>

</form>
<!-- END: html_payment_form -->