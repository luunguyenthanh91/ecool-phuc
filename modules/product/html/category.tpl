<!-- BEGIN: modules -->
<div class="product-inverter">
      <div class="background-color">
        <div class="container">
          <div class="begin-products-list">
            <div class="row">
              <div class="col-lg-12 col-12 text-center">
                <img src="{data.src_b}" alt="{data.cat_name}">
              </div>
              <!-- <div class="col-lg-5 col-12 products-list-content">
                <h1 class="fs-20 fs-lg-47 font-weight-medium">{data.cat_name}</h1>
                <h2 class="fs-16 fs-lg-28 font-weight-medium">{data.slogan}</h2>
              </div> -->
            </div>
          </div>
        </div>
      </div>
      <div class="container">
        <div class="row">
          <div class="col-12">
            <form class="form-search-system text-center" action="{data.link_action}">
              <input class="input-search-system form-group border-form-system form-control" type="text" placeholder="Tìm điều hòa" name="keyword" id="keyword" value="{INPUT.keyword}">
              <button class="btn-search-system" type="submit"><img class="img-btn-search" src="{DIR_IMAGE}/search.png"></button>
            </form>
            <nav class="fs-10 font-weight-light" aria-label="breadcrumb">
              {data.navation}
            </nav>
          </div>
        </div>
      </div>
      <div class="container">
        <div class="header-content">
          <h1 class="text-center product-cate-title">{data.cat_name}</h1>
          <div class="header-content__detail hidden">{data.description}</div>
          <br/><br/>
        </div>
       {data.main}
      </div>
       {data.news_focus}
      <br/>
    </div>
<!-- END: modules -->


<!-- BEGIN: html_list -->


 <div class="product-box">
          <div class="option-box-wrap">
            <div class="option-box d-flex justify-content-between bd-highlight mb-3">
              <!-- <div class="p-2 bd-highlight">9 kết quả</div> -->

              <div class="bd-highlight">{data.count} kết quả</div>
              <div class="bd-highlight d-flex justify-content-between">
                <div class="selector-ranger position-relative"><span>Giá</span><img class="select-box__icon" src="http://cdn.onlinewebfonts.com/svg/img_295694.svg" alt="Arrow Icon" aria-hidden="true">
                  <div class="price-slider" style="list-style: none;">
                    {data.Price_filter}
                  </div>
                </div>
                    <div class="select-box">
                      <div class="select-box__current" tabindex="1">
                        <div class="select-box__value">
                          <input class="select-box__input" type="radio" id="0" value="1" name="Công suất" checked="">
                          <p class="select-box__input-text">Công suất</p>
                        </div><img class="select-box__icon" src="http://cdn.onlinewebfonts.com/svg/img_295694.svg" alt="Arrow Icon" aria-hidden="true">
                      </div>
                      <ul class="select-box__list">

                        {data.status_filter}
                      </ul>
                    </div>

              </div>


              <div class="p-2 bd-highlight d-none d-lg-block">
                    <div class="select-box">
                      <div class="select-box__current" tabindex="1">
                        <div class="select-box__value">
                          <input class="select-box__input" type="radio" id="0" value="2" name="Sắp xếp theo" checked="">
                          <p class="select-box__input-text">Sắp xếp theo</p>
                        </div><img class="select-box__icon" src="http://cdn.onlinewebfonts.com/svg/img_295694.svg" alt="Arrow Icon" aria-hidden="true">
                      </div>
                      <ul class="select-box__list">

                        {data.Sort_Filter}

                      </ul>
                    </div>
              </div>
            </div>
          </div>
          <div class="product-list">
            <div class="row">

                {data.row_product}
            </div>

          </div>
          <div class="product-viewmore text-center">{data.nav}</div>
        </div>
<!-- END: html_list -->

<!-- BEGIN: html_search -->
<div class="formSearch">
  <form name='modSearch' id="modSearch" method='get' action="{data.link_action}" >
    <table width="90%" border="0" cellspacing="2" cellpadding="2" align="center" >
      <tr>
        <td class="col1" width="20%" nowrap="nowrap"><strong>{LANG.product.keyword}</strong></td>
        <td><input type="text" class="textfiled" name="keyword" id="keyword" value="{INPUT.keyword}" size="30"/></td>
      </tr>
      <tr>
        <td class="col1" nowrap="nowrap" ><strong>{LANG.product.category}</strong></td>
        <td>{data.list_cat}</td>
      </tr>
      <tr>
        <td class="col1" nowrap="nowrap">&nbsp;</td>
        <td>
          <button id="btnSearch" name="btnSearch" type="submit" class="btn" value="{LANG.product.btn_search}">
            <span>{LANG.product.btn_search}</span>
          </button>
        </td>
      </tr>
    </table>
  </form>
</div>
<p class="mess_result">{data.note_keyword} <br>{data.note_result}</p>
<div id="List_Product">{data.list_search}<br class="clear"></div>
{data.nav}
<!-- END: html_search -->

<!-- BEGIN: html_tags -->
<p class="mess_result">{data.note_result}</p>
<div id="List_Product">
    {data.row_product}
    <br class="clear">
</div>
{data.nav}
<!-- END: html_tags -->


<!-- BEGIN: html_news_focus -->
  <section class="news">
      <div class="container">
        <h2 class="fs-24 font-weight-semi-bold">{LANG.global.news}</h2>
        <div class="slide-news position-relative">
          <div class="slide-news__content">
            {data.list_news}

          </div>
          <div class="arrow"><span class="arrow__left"><img src="{DIR_IMAGE}/arrowleft.png"></span><span class="arrow__right"><img src="{DIR_IMAGE}/arrowright.png"></span></div>
        </div>
        <div class="text-center news__viewall"><a class="fs-11 font-weight-semi-bold" href="">{LANG.global.xemtttt}<img src="{DIR_IMAGE}/arrowdown.png"></a></div>
      </div>
    </section>
<!-- END: html_news_focus -->

<!-- BEGIN: item_news -->

<div class="row items-slide d-flex">
    <div class="col-12 col-lg-6">
      <div class="news__banner"><img src="{data.src}" alt="{data.title}">

      </div>
    </div>
    <div class="col-12 col-lg-6">
      <div class="news__text">
        <h3 class="fs-15 font-weight-semi-bold"><a itemprop="url" class="fs-15 font-weight-semi-bold" href="{data.link}">{data.title}</a></h3>
        {data.short}
      </div>
    </div>
  </div>

<!-- END: item_news-->
