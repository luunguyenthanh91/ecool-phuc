<!-- BEGIN: box_search -->
<script language="javascript" >
	function check_boxSearch(f)
	{
		var keyword = f.keyword.value;
		var key_default = "{LANG.product.keyword_default}" ;
		if(keyword==key_default)
		{
			f.keyword.value='';	
			alert("{LANG.product.key_search_empty}");	
			f.keyword.focus();
			return false;
		}
		
		var len = f.keyword.value.length;
		if(len<1){
			alert("{LANG.product.key_search_invalid}");
			f.keyword.focus();
			return false;
		}
		
		return true;
	}	

</script>
<div class="box">
<form id="formSearch" name="formSearch" method="get" action="{data.link_search}" onSubmit="return check_boxSearch(this);">
<input type="hidden" name="do_search" value="1" />
<table width="100%" cellspacing="2" cellpadding="2" border="0"  >
	<tr>
 	<td  ><input name="keyword" id="keyword" type="text" class="textfiled" onfocus="if(this.value=='{LANG.product.keyword_default}') this.value='';" onblur="if(this.value=='') this.value='{LANG.product.keyword_default}';"  value="{data.keyword}"  style="width:100%" /></td>
	<td width="38" ><button  id="btnSearch" name="btnSearch" type="submit" class="btn" value="{LANG.product.btn_search}" ><span >{LANG.product.btn_search}</span></button>  </td>
	</tr>
</table>
</form>
</div>
<!-- END: box_search -->