<!-- BEGIN: modules -->
<div id="vnt-content">
    <div class="vnt-main-top">
      <div class="vnt-slide">
          <div id="vnt-slide" class="slick-init">
              <div class="item"><div class="img">
                  <img src="{DIR_IMAGE}/about/slide.jpg" alt="">
              </div></div>
          </div>
          <div id="vnt-navation" class="breadcrumb hidden-sm hidden-xs" itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb">
              <div class="wrapper">
                  <div class="navation">
                      {data.navation}
                      <div class="clear"></div>
                  </div>
              </div>
          </div>
      </div>
      
  </div>

<div class="wrapping">
  <div class="wrapCont">
    <div class="wrapper">
      {data.main}
    </div>
  </div>
  <div id="flagEnd"></div>
</div>
</div>
<!-- END: modules -->

<!-- BEGIN: html_cart -->
{data.nav_shopping}
<form action="{data.link_action}" method="post" name="f_cart">
    {data.mess}
    <table class="addtocart2 res">
        <thead>
            <tr>
                <td>{LANG.product.product}</td>
                <td>{LANG.product.cart_price}</td>
                <td>{LANG.product.quantity}</td>
                <td>{LANG.product.total}</td>
                <td>{LANG.product.remove}</td>
            </tr>
        </thead>
        <tbody>
            {data.row_cart}
        </tbody>
    </table>
    <div class="coupon">
        <div class="infoShip">
            {data.note_cart}
        </div>
        <div class="totalPrice">
            <ul>
                <li>
                    <div class="p">{LANG.product.total_price}</div>
                    <div class="l v" id="total_price">{data.cart_total}</div>
                    <div class="clear"></div>
                </li>
            </ul>
        </div>
        <div class="clear"></div>
    </div>
    <div class="pay">
        {data.note_payment}
    </div>
    <div class="divButton">
        <div class="fl">
            <a class="btnCart" href="{data.link_continue_shopping}">{LANG.product.btn_continue_shopping}</a>
        </div>
        <div class="fr">
            <a class="btnCart" href="{data.link_remove}">{LANG.product.btn_empty_cart}</a>
            <input type="hidden" name="modify" value="modify" />
            <input type="hidden" name="link_ref" value="{data.link_ref}" />
            <button id="btnCheckout" name="btnCheckout" type="submit" class="btnCart blood" value="1">
                <span>{LANG.product.btn_check_out}</span>
            </button>
        </div>
        <div class="clear"></div>
    </div>
</form>
<!-- END: html_cart -->

<!-- BEGIN: html_empty_cart -->
{data.nav_shopping}
<div class="boxSuccess">
    <div class="txt1">{LANG.product.empty_cart}</div>
    <div class="txt2"><p>{LANG.product.note_empty_cart}</p></div>
    <div class="backHome"><a href="{data.link_back}">{LANG.product.back_to_home}</a></div>
</div>
<!-- END: html_empty_cart -->