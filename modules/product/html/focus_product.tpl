<!-- BEGIN: focus_product -->
<div class="box-product">
  <div class="box-title">
    <h1 class="titleL">{data.f_title}</h1>
    <div class="titleR">{data.more}</div>
    <div class="clear"></div>
  </div>
  <div class="box-content">
  	<div id="List_Product">
    {data.list}
    <div class="clear"></div>
    </div>
  </div>          
</div>
<!-- END: focus_product -->