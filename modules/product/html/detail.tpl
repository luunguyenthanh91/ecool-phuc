BEGIN: modules -->
<div class="product-detail">
      <div class="container">
        <div class="row">
          <div class="col-12">
            <form class="form-search-system text-center" action="#">
              <input class="input-search-system form-group border-form-system form-control" type="text" placeholder="Tìm điều hòa">
              <button class="btn-search-system" type="submit"><img class="img-btn-search" src="{DIR_IMAGE}/search.png"></button>
            </form>
            <nav class="fs-10 font-weight-light" aria-label="breadcrumb">
               {data.navation}
            </nav>
          </div>
        </div>
      </div>
      {data.main}
    </div>
<!-- END: modules -->




<!-- BEGIN: detail -->
<main class="main">
        <div class="shop-container container">
          <div class="product-main">
            <div class="row">
              <div class="col-lg-8">
                <div class="product-image">
                  <div class="col-big-img">
                    <div class="product-gallery">
                      {data.list_for}
                     <!--  <div class="slide-item"><img src="{DIR_IMAGE}/product_detail/ic-07.png"></div>
                      <div class="slide-item"><img src="{DIR_IMAGE}/product_detail/ic-07.png"></div>
                      <div class="slide-item"><img src="{DIR_IMAGE}/product_detail/ic-07.png"></div>
                      <div class="slide-item"><img src="{DIR_IMAGE}/product_detail/ic-07.png"></div> -->
                    </div>
                  </div>
                  <div class="col-thumb-img">
                    <div class="product-thumbnails">
                      {data.list_nav}
                      <!-- <a class="text-center"><img src="{DIR_IMAGE}/product_detail/ic-08.png"></a>
                      <a class="text-center"><img src="{DIR_IMAGE}/product_detail/ic-09.png"></a>
                      <a class="text-center"><img src="{DIR_IMAGE}/product_detail/ic-10.png"></a>
                      <a class="text-center"><img src="{DIR_IMAGE}/product_detail/ic-11.png"></a> --></div>
                  </div>
                </div>
              </div>
              <div class="col-lg-4 product-info">
                <div class="pd-top">
                  <h1 class="product-title">{data.p_name}</h1>
                  <p class="sku">{data.maso}</p>
                  <div class="product-short-description" style="font-size: 14px;">
                    {data.short}
                  </div>
                  <div class="variations">
                    {data.row_option}
                  </div>
                </div>
                <div class="pd-bot">
                  <div class="price-wrapper mb-3">
                    <a class="product-btn price">
                      {data.price_text}
                    </a>
                  </div>
                  <div class="product-contact"><a class="product-btn" href="{data.link}" target="_blank">{LANG.product.contact_product}</a></div>

                  <div class="product-map text-center">
                    <a href="{data.link_map}">
                      <img class="img-map" src="{DIR_IMAGE}/product_detail/ic-44.png" style="height:20px;margin-right: 10px;">
                      <span>{LANG.product.map_buy}</span></a>
                  </div>
                </div>

              </div>
            </div>
          </div>
          <div class="product-footer">

            <div class="product-tabs">
              <ul class="nav d-flex justify-content-between" id="myTab" role="tablist">
                <li><a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">{LANG.product.description}</a></li>
                <li><a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile2" role="tab" aria-controls="profile2" aria-selected="false">Thông số kỹ thuật</a></li>
                <li><a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile3" role="tab" aria-controls="profile3" aria-selected="false">Tính năng ưu việt</a></li>
                <li class="hidden"><a class="nav-link" id="contact-tab" data-toggle="tab" href="#profile4" role="tab" aria-controls="profile4" aria-selected="false">Chứng nhận chất lượng</a></li>
                <li><a class="nav-link" id="contact-tab" data-toggle="tab" href="#profile5" role="tab" aria-controls="profile5" aria-selected="false">Hỗ trợ</a></li>
              </ul>
              <div class="tab-content" id="myTabContent">

                <div class="content-profile active" id="home" role="tabpanel" aria-labelledby="home-tab">
                  <h2 class="product-title">{data.p_name}</h2>
                  <p class="sku">{data.maso}</p>
                  <div>{data.description}</div>


                  <div class="" style="width:100%">

                    {data.big}

                    <div class="w3-row-padding w3-section">
                        {data.small}

                    </div>

                  </div>
                  <style>
                  .center-cropped {
                    object-fit: cover; /* Do not scale the image */
                    object-position: center; /* Center the image within the element */
                    max-height: 500px;
                    width: 100%;
                    }
                    .center-cropped-small {
                      object-fit: cover; /* Do not scale the image */
                      object-position: center; /* Center the image within the element */
                      height: 100px;
                      width: 100%;
                      }
                      .img_his{
                        object-fit: cover; /* Do not scale the image */
                        object-position: center; /* Center the image within the element */
                        height: 200px;
                        width: 100%;
                      }
                  </style>
                  <script>
                  function currentDiv(n) {
                    showDivs(slideIndex = n);
                  }

                  function showDivs(n) {
                    var i;
                    var x = document.getElementsByClassName("mySlides");
                    var dots = document.getElementsByClassName("demo");
                    if (n > x.length) {slideIndex = 1}
                    if (n < 1) {slideIndex = x.length}
                    for (i = 0; i < x.length; i++) {
                      x[i].style.display = "none";
                    }
                    for (i = 0; i < dots.length; i++) {
                      dots[i].className = dots[i].className.replace(" w3-opacity-off", "");
                    }
                    x[slideIndex-1].style.display = "block";
                    dots[slideIndex-1].className += " w3-opacity-off";
                  }
                  </script>
                </div>

                <div class="content-profile" id="profile2" role="tabpanel" aria-labelledby="profile-tab">
                  {data.element}
                </div>

                 <div class="content-profile" id="profile3" role="tabpanel" aria-labelledby="profile-tab">
                  <div style="padding-top: 15px;padding-bottom: 15px;">
                    {data.tinhnang}
                  </div>
                </div>
                 <div class="content-profile hidden" id="profile4" role="tabpanel" aria-labelledby="profile-tab">
                  <div style="padding-top: 15px;padding-bottom: 15px;">
                   {data.chungnhan}
                 </div>
                </div>

                 <div class="content-profile" id="profile5" role="tabpanel" aria-labelledby="profile-tab">
                  <div style="padding-top: 15px;padding-bottom: 15px;">
                   {data.hotro}
                 </div>
                </div>

              </div>
            </div>




          </div>
          <div class="product-inverter">
              <section class="news">
                  <div class="container">
                      <h2 class="section__title  text-center font-weight-medium wow">
                      {LANG.product.histori_product}</h2>
                      <div class="row" >
                          {data.his}
                      </div>
                  </div>
              </section>
          </div>
        </div>
      </main>

<!-- END: detail -->

<!-- BEGIN: html_product_sub -->
<div class="productTable" id="flagsub">
  <div class="title">{data.num_sub}</div>
  <table>
    <tr>
      <th>{LANG.product.sub_img}</th>
      <th>{LANG.product.sub_maso}</th>
      <th>{LANG.product.sub_color}</th>
      <th>{LANG.product.sub_category}</th>
      <th>{LANG.product.sub_param}</th>
      <th>{LANG.product.sub_desc}</th>
      <th>{LANG.product.sub_price}</th>
      <th>{LANG.product.sub_quantity}</th>
    </tr>
    <!-- BEGIN: has_item -->
    <tr>
      <td><div class="img"><a href="{row.src}" class="popupSmall">{row.pic}</a></div></td>
      <td class="be" data-cont="{LANG.product.sub_maso}">{row.maso}</td>
      <td class="be" data-cont="{LANG.product.sub_color}">{row.color}</td>
      <td class="be" data-cont="{LANG.product.sub_category}">{row.cat_name}</td>
      <td class="be" data-cont="{LANG.product.sub_param}">{row.param}</td>
      <td class="be" data-cont="{LANG.product.sub_desc}">{row.short}</td>
      <td class="be" data-cont="{LANG.product.sub_price}">{row.price_text}</td>
      <td>
        <div class="quantity">
          <input type="text" name="quantity[{row.subid}]" id="quantity_{row.subid}" value="1" {row.disabled}/>
          <button type="button" class="{row.btn_class}" {row.onclick}>
            <i class="fa fa-shopping-cart"></i>
          </button>
        </div>
      </td>
    </tr>
    <!-- END: has_item -->
    <!-- BEGIN: no_item -->
    <tr>
      <td colspan="8" class="noItem">{LANG.product.no_have_sub}</td>
    </tr>
    <!-- END: no_item -->
  </table>
</div>
<!-- END: html_product_sub -->

<!-- BEGIN: item_belong -->
<div class="item">
  <div class="product" itemscope itemtype="http://schema.org/Product">
    <div class="img" itemprop="url">
      <a href="{data.link}" title="{data.p_name}">{data.pic}</a>
      <div class="ribbon">
        {data.box_status}
        {data.discount}
      </div>
    </div>
    <div class="caption">
      <div class="tend">
        <h3 itemprop="name"><a href="{data.link}" title="{data.p_name}">{data.p_name}</a></h3>
      </div>
      <div class="code">{LANG.product.maso} : {data.maso}</div>
      <div class="price" itemprop="offers" itemscope itemtype="http://schema.org/Offer">
        {data.price_old_text}
        <div class="red">{data.price_text}</div>
        <meta itemprop="price" content="{data.price}"/>
        <meta itemprop="priceCurrency" content="VND"/>
      </div>
      {data.out_stock}
    </div>
    <meta itemprop='productID' content='{data.maso}'/>
  </div>
</div>
<!-- END: item_belong -->

<!-- BEGIN: html_belong_product -->
<div class="productOther" id="flag2">
  <div class="title"><h2>{data.f_title}</h2></div>
  <div class="content">
    <div id="slideOther" class="slick-init">
      {data.list}
    </div>
  </div>
</div>
<!-- END: html_belong_product -->

<!-- BEGIN: html_comment -->
<script language="javascript">
  js_lang['err_name_empty'] = "{LANG.product.err_name_empty}";
  js_lang['err_email_empty'] = "{LANG.product.err_email_empty}";
  js_lang['err_email_invalid'] = "{LANG.product.err_email_invalid}";
  js_lang['security_code_invalid'] = "{LANG.product.security_code_invalid}";
  js_lang['err_security_code_empty'] = "{LANG.product.err_security_code_empty}";
  js_lang['err_title_empty'] = "{LANG.product.err_title_empty}";
  js_lang['err_content_comment_empty'] = "{LANG.product.err_content_comment_empty}";
  js_lang['send_comment_success'] = "{LANG.product.send_comment_success}";
  js_lang['send_reply_success'] = "{LANG.product.send_reply_success}";
  js_lang['err_mark_empty'] = "{LANG.product.err_mark_empty}";
  js_lang['err_conntent_minchar'] = "{LANG.product.err_conntent_minchar}";
  js_lang['mess_error_post'] = "{LANG.product.mess_error_post}";
</script>
<script type="text/javascript" src="{DIR_MOD}/js/comment.js"></script>
<div class="comment" id="flag1">
  <div class="title">{LANG.product.f_comment}</div>
  <div class="formComment">
    <form action="{data.link_action}" method="post" name="fComment" id="fComment" onsubmit="return vnTcomment.post_comment('{data.p_id}','{data.lang}');">
      <div class="w_content">
        <textarea id="com_content" name="com_content" class="form-control" placeholder="{LANG.product.comment_default}"></textarea>
        <div class="content-info" style="display: none;">
          <div class="info-title">{LANG.product.enter_info_comment}</div>
          <input type="text" name="com_email" id="com_email" class="form-control" placeholder="Email" value="{data.com_email}"/>
          <input type="text" name="com_name" id="com_name" class="form-control" placeholder="{LANG.product.full_name}" value="{data.com_name}"/>
          <div class="input-group">
            <input type="text" name="security_code" id="security_code" class="form-control" placeholder="{LANG.product.security_code}">
            <div class="input-group-img">
              <img class="security_ver" src="{data.ver_img}" alt="">
            </div>
          </div>
          <button id="btn-search" name="btn-search" class="btn" type="submit">{LANG.product.btn_send_comment}</button>
          <button id="btn-close" name="btn-close" class="btn" type="button">{LANG.product.btn_close}</button>
          </div>
      </div>
    </form>
  </div>
  {data.list_comment}
</div>
<!-- END: html_comment
