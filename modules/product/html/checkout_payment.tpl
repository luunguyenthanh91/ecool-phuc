<!-- BEGIN: modules -->
<div class="navation" id="navation">{data.navation}<div class="clear"></div></div>
 <div id="BoxShopping">
  {data.nav_shopping}
	{data.info_cart}
  <form action="{data.link_action}" method="post" name="f_checkout" id="f_checkout" onsubmit="return checkform(this);">	
	{data.delivery_address}
	{data.shipping_method}	
	{data.payment_method}


	<br />
	<table width="100%" border="0" cellspacing="2" cellpadding="2" align="center">
	
		<tr>
			<td  class="shopping_title"><strong>{LANG.product.comment_order}:</strong> </td>
		</tr>
	</table>
	<table width="100%" border="0" cellspacing="2" cellpadding="2">
				<tr>
					<td align="center" ><textarea name="comment" cols="7" rows="5" style='width:98%' class="textarea">{data.comment}</textarea></td>
				</tr>
			</table>
	
	<br />
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td align="center">
			<input name="do_process" type="hidden" value="1" />
			<input name="btnConfirm" type="submit" value="{LANG.product.btn_continue}"  class="button"/></td>
		</tr>
	</table>
	</form>
	<br />
	
	</div>
 
<!-- END: modules -->
 


<!-- BEGIN: delivery_address -->
<script language=javascript>
  
	function checkform(f) {	
			
		var re =/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,5})$/gi;

		if (f.c_name.value == '') {
				alert("{LANG.product.err_full_name_empty}");
				f.c_name.focus();
				return false;
		}
      
    if (f.c_address.value == '') {
				alert("{LANG.product.error_address_empty}");
				f.c_address.focus();
				return false;
		}

		return true;
	}
  
  
</script>
<br />

<table width="100%"  cellspacing="0" cellpadding="5" border="0" >
	<tr>
			<td height="29" class="shopping_title" >
			<b>{LANG.product.shipping_address}</b></td>
		</tr>
	<tr>
  
	<tr>
	<td >

      <table width="100%" cellspacing="0" cellpadding="0" border="0" class="tbl_from">
        <tr>
          <td  width="170"  class="td1">{LANG.product.full_name} (<span class="font_err">*</span>)</td>
          <td class="td2" > <input type="text"  style="width:250px;" id="c_name" class="textfiled" name="c_name" value="{data.c_name}"/></td>
        </tr>
        <tr>
          <td class="td1" >{LANG.product.address} (<span class="font_err">*</span>)</td>
          <td class="td2"><input type="text" style="width:250px;" id="c_address" class="textfiled" name="c_address" value="{data.c_address}"/></td>
        </tr>
        
        <tr>
          <td class="td1" >{LANG.product.country} (<span class="font_err">*</span>)</td>
          <td class="td2" >{data.list_country}</td>
        </tr>
        <tr>
          <td class="td1" >{LANG.product.city}</td>
          <td class="td2" ><span id="ext_city">{data.list_city}</span></td>
        </tr>
        <tr>
          <td class="td1" >{LANG.product.phone} </td>
          <td class="td2" > <input type="text"  style="width:250px;" id="c_phone" class="textfiled" name="c_phone" value="{data.c_phone}" /></td>
        </tr>

      </table>


	</td>
</tr>
</table>
<!-- END: delivery_address -->



<!-- BEGIN: shipping_method -->
<script language="javascript">
var selected;
function rowOverEffect(object) {
  if (!selected) {
    if (document.getElementById) {
      selected = document.getElementById('defaultSelected');
    } else {
      selected = document.all['defaultSelected'];
    }
  }

  if (selected) selected.className = 'moduleRow';
  object.className = 'moduleRowSelected';
  selected = object;


}




</script>
<br />
<table width="100%" border="0" cellspacing="2" cellpadding="2" align="center">
  <tr>
    <td  class="shopping_title"><strong>{LANG.product.shipping_method}:</strong> </td>
  </tr>
  <tr>
    <td>
    
    	<table width="100%" border="0" cellspacing="2" cellpadding="2">

      <tr>
        <td >
	        {data.list_shipping_method}
        </td>
      </tr>
    </table>
    
     </td>
  </tr>
</table>

<!-- END: shipping_method -->


<!-- BEGIN: payment_method -->
<script language="javascript">
var selected_payment;
function rowOverEffect_Payment(object, buttonSelect) {

  if (!selected_payment) {
    if (document.getElementById) {
      selected_payment = document.getElementById('defaultSelected_payment');
    } else {
      selected_payment = document.all['defaultSelected_payment'];
    }
  }

  if (selected_payment) selected_payment.className = 'moduleRow';
  object.className = 'moduleRowSelected';
  selected_payment = object;
	
  if (document.f_checkout.rPayment[0]) {
    document.f_checkout.rPayment[buttonSelect].checked=true;
  } else {
    document.f_checkout.rPayment.checked=true;
  }
}

</script>
<br />
<table width="100%" border="0" cellspacing="2" cellpadding="2" align="center">
  <tr>
    <td  class="shopping_title"><strong>{LANG.product.payment_method}:</strong> </td>
  </tr>
  
  <tr>
    <td>
    
    	<table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td >
	        {data.list_payment_method}
        </td>
      </tr>
    </table>
    
     </td>
  </tr>
</table>

<!-- END: payment_method -->

