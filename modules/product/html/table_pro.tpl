<!-- BEGIN: table_pro -->
<!-- <div class="col-lg-4 col-md-4 col-sm-6 col-xs-6" style="border: 1px solid #f4f4f4;">
    <div class="product" itemscope itemtype="http://schema.org/Product">
        <div class="img"><a href="{data.link}" title="{data.p_name}">{data.pic}</a></div>
        <div class="tend"><h3 itemprop="name"><a href="{data.link}">{data.p_name}</a></h3></div>
        <div class="price" itemprop="offers" itemscope itemtype="http://schema.org/Offer">
            <div class="red">{data.price_text}</div>
            {data.price_old_text}
            <meta itemprop="price" content="{data.price}"/>
            <meta itemprop="priceCurrency" content="VND"/>
        </div>
        <div class="ribbon">
          {data.box_status}
          {data.discount}
        </div>
        {data.out_stock}
        <meta itemprop='productID' content='{data.maso}'/> 
    </div>
</div> -->

<a class="col-12 col-lg-4 product product_item_style_1" href="{data.link}">
<div class="item__wrap">
  <div class="item__image">{data.pic}</div>
  <p class="item__title">{data.p_name}</p>
  <p class="item__price">{data.price_text}</p>
  <div class="item__shortdescription">
    <div class="divider"></div>
    
    {data.short}
  </div><span class="btn-detail">Chi tiết sản phẩm</span>
</div></a>

<!-- END: table_pro -->
