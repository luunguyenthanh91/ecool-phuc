<?php
/*================================================================================*\
|| 							Name code : main.php 		 		 																	  # ||
||  				Copyright © 2007 by Thai Son - CMS vnTRUST                					# ||
\*================================================================================*/
/**
 * @version : 1.0
 * @date upgrade : 11/12/2007 by Thai Son
 **/
if (! defined('IN_vnT'))
{
  die('Access denied');
}
$nts = new sMain();
class sMain
{
  var $output = "";
  var $skin = "";
  var $linkUrl = "";
  var $module = "product";
  var $act = "tags";
  
  /**
   * function sMain ()
   * Khoi tao 
   **/
  function sMain ()
  {
    global $vnT, $input;
    include ("function_" . $this->module . ".php");
    loadSetting();
    $this->skin = new XiTemplate( DIR_MODULE ."/". $this->module . "/html/". $this->module . ".tpl");
    $this->skin->assign('DIR_MOD', DIR_MOD);
    $this->skin->assign('LANG', $vnT->lang);
    $this->skin->assign('INPUT', $input);
    $this->skin->assign('CONF', $vnT->conf);
    $this->skin->assign('DIR_IMAGE', $vnT->dir_images);
    
    $vnT->html->addStyleSheet(DIR_MOD . "/css/" . $this->module . ".css");
    $vnT->html->addScript(DIR_MOD . "/js/" . $this->module . ".js");   
    
		$vnT->setting['menu_active'] = $this->module;
		
		$data['main'] = $this->do_Tags(); 
 		$data['navation'] = get_navation($cat_id,$vnT->lang['product']['f_tags']);
		$data['box_sidebar'] = box_sidebar(); 
		
    $this->skin->assign("data", $data);
    $this->skin->parse("modules");
    $vnT->output .= $this->skin->text("modules");
  
  }
	
  
  /**
   * function do_Tags 
   * 
   **/
  function do_Tags ()
  {
    global $DB, $func, $input, $vnT;
		
		$p = ((int) $input['p']) ? (int) $input['p'] : 1;
 	  $num_row=3;
		$view=1;
		$n_list = ($vnT->setting['n_list']) ? $vnT->setting['n_list'] : 10;
		$n_grid = ($vnT->setting['n_grid']) ? $vnT->setting['n_grid'] : 20;
		$n = ($view == 2) ? $n_list : $n_grid;
		
		 
		$ext_pag = "";
		$tagID = $input['tagID'];		
		$where = " and FIND_IN_SET('$tagID',tags)<>0 "; 
		
		$sql_num =   "SELECT n.p_id
									FROM products n , products_desc nd
									WHERE n.p_id=nd.p_id
									AND lang='$vnT->lang_name'
									AND display=1 
									$where ";
    //echo "<br>".$sql_num;
    $res_num = $vnT->DB->query($sql_num);
    $totals = $vnT->DB->num_rows($res_num);
		
		
			
    $num_pages = ceil($totals / $n);
    if ($p > $num_pages) $p = $num_pages;
    if ($p < 1) $p = 1;
    $start = ($p - 1) * $n;
		
		if($num_pages>1) {
			
			$root_link = LINK_MOD . "/tags/".$keyword ;	
			$nav = "<div class=\"pagination\">".$vnT->func->paginate_search($root_link,$totals,$n,$ext_pag,$p)."</div>" ;
		}
    
		$data['row_product'] = row_product($where, $start, $n, $num_row, $view);
    //$data['product_other'] = product_other($where,  ($start + $n), $n_other);
    $data['nav'] = $nav;
		
		$str_keyword.="<span class=\"font_keyword\">".$result_keyword."</span>" ;
 		$data['note_keyword'] = str_replace("{keyword}",$str_keyword,$vnT->lang['product']['note_keyword']);  
		
		$note_result = str_replace("{totals}","<b class=font_err>".$totals."</b>",$vnT->lang['product']['note_result']); 
		$data['note_result'] = str_replace("{num_pages}","<b>".$num_pages."</b>",$note_result);
 		
		$this->skin->assign("data", $data);
    $this->skin->parse("html_tags");
		$nd['content'] = $this->skin->text("html_tags");
		$nd['f_title'] = '<h1>' . $vnT->lang["product"]["f_tags"].": ".$vnT->func->get_tag_name($this->module,$tagID) . '</h1>';
    $textout = $vnT->skin_box->parse_box("box_middle", $nd);
 		
		
    return $textout;
		
  }
  

  
// end class
}
?>