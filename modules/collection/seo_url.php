<?php
	// no direct access
	if (!defined('IN_vnT')){
	 	die('Hacking attempt!');
	}
 	$seo_mod_name = $vnT->setting['seo_name'][$vnT->lang_name]['collection'];
	if( strstr($_SERVER['REQUEST_URI'],$seo_mod_name.".html" )){	 
		$_GET[$vnT->conf['cmd']] = "mod:collection";
		$QUERY_STRING = $vnT->conf['cmd']."=mod:collection";
	}
	if (in_array($seo_mod_name, $vnT->url_array)){
		$pos	= array_search ($seo_mod_name, $vnT->url_array);
		$mod	= "collection";	
		$act	= $vnT->url_array[$pos+1];
		if(strstr($act,".html")){
			$text_catid = $act;
			$catID = strtolower(substr($text_catid, strrpos($text_catid, "-") + 1));
			$catID = str_replace(".html","",$catID);
			$catID = (int) $catID;
			if((int)$catID){ 
				$_GET[$vnT->conf['cmd']] = "mod:$mod|act:category|catID:$catID" ;
				$QUERY_STRING = $vnT->conf['cmd'] . "=mod:$mod|act:category|catID:$catID";		 
			}
		}
		$pID = $vnT->url_array[$pos+1];
		if((int)$pID){
			$_GET[$vnT->conf['cmd']] = "mod:$mod|act:detail|pID:$pID" ;
			$QUERY_STRING = $vnT->conf['cmd'] . "=mod:$mod|act:detail|pID:$pID" ; 		 
		}
		//search
		if( $act =="search" || $act =="search.html" ){ 
			$extra = $vnT->url_array[$pos+2] ;
			if($extra) $extra_link = "|".$extra;
			$_GET[$vnT->conf['cmd']] = "mod:$mod|act:search".$extra_link;
			$QUERY_STRING = $vnT->conf['cmd']."=mod:$mod|do:search".$extra_link;
		}
		//booking
		if($vnT->url_array[$pos+1]=="booking" || $vnT->url_array[$pos+1]=="booking.html"){
			$extra = $vnT->url_array[$pos+2] ;
			if($extra) $ext_link = "|".$extra;
 			$_GET[$vnT->conf['cmd']] = "mod:$mod|act:booking".$ext_link;
			$QUERY_STRING = $vnT->conf['cmd']."=mod:$mod|act:booking".$ext_link;			 
		}
	}
?>