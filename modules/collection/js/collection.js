$(document).ready(function(){
    // SLIDE IMG
    $("#slideImg").slick({
        autoplay:true,
        speed:500,
    });
    // OTHER
    $("#slideOther").slick({
    	slidesToShow : 3,
    	autoplay:true,
    	speed:400,
        responsive: [
            {
                breakpoint: 991,
                settings: {
                    slidesToShow: 2,
                }
            },
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 1,
                }
            }
        ]
    });
    // POPUP
    $(".btnRecive a").fancybox({
        padding     : 0,
        maxWidth    : 870,
        width       : '99%',
        height      : '99%',
        autoSize    : true,
        fitToView   : false,
        autoHeight  : true,
        autoWidth   : true,
        type        : 'iframe',
        wrapCSS     : 'myDesignPopup',
        margin      : [80,20,20,20],
    });
    $(".collectContent .link a#contentFlag").fancybox({
        padding     : 0,
        maxWidth    : 740,
        width       : '99%',
        height      : '99%',
        autoSize    : true,
        fitToView   : false,
        autoHeight  : true,
        autoWidth   : true,
        type        : 'iframe',
        wrapCSS     : 'myDesignPopup',
        margin      : [80,20,20,20],
    });
    $("#slideImg .item a").fancybox({
        padding:0,
        minWidth: '100%',
        minHeight : '100%',
        margin: 0,
        type:'iframe',
        // closeBtn: false,
        wrapCSS: 'designGal',
    });
    // ZOOM HINH
    $("#slideGallery").slick({
        autoplay:true,
    });
    $('#slideNavGallery .item').each(function(index){
        $(this).attr("data-slick-index",index);
    });
    $("#slideNavGallery .item").click(function(){
        $("#vnt-news-slide-nav .item").removeClass("active");
        $(this).addClass("active");
        var index=$(this).attr("data-slick-index");
        $('#slideGallery').slick("slickGoTo",parseInt(index));
    });
    $(".aboutVideo .link").fancybox({
        padding:0,
    });
    $("#vFlag").click(function(){
        var op=$(this).attr("href");
        var opOffset=$(op).offset().top - 100;
        $('html,body').animate({scrollTop: opOffset},1000);
        return false;
    });
});