<?php
/*================================================================================*\
|| 							Name code : main.php 		 		 																	  # ||
||  				Copyright © 2007 by Thai Son - CMS vnTRUST                					# ||
\*================================================================================*/
/**
 * @version : 1.0
 * @date upgrade : 11/12/2007 by Thai Son
 **/
if (! defined('IN_vnT'))
{
  die('Access denied');
}
$nts = new sMain();
class sMain
{
  var $output = "";
  var $skin = "";
  var $linkUrl = "";
  var $module = "collection";
  var $action = "detail";

  function sMain (){
    global $vnT, $input;
    include ("function_".$this->module.".php");
    loadSetting();
    $this->skin = new XiTemplate( DIR_MODULE ."/". $this->module . "/html/". $this->module . ".tpl");
    $this->skin->assign('DIR_MOD', DIR_MOD);
    $this->skin->assign('LANG', $vnT->lang);
    $this->skin->assign('INPUT', $input);
    $this->skin->assign('CONF', $vnT->conf);
    $this->skin->assign('DIR_IMAGE', $vnT->dir_images);
    $this->skin->assign('DIR_JS', $vnT->dir_js);
    $this->skin->assign('DIR_STYLE', $vnT->dir_style);
    $this->linkMod = $vnT->cmd . "=mod:" . $this->module;
    $vnT->html->addStyleSheet(DIR_MOD . "/css/".$this->module . ".css");
    $vnT->html->addScript(DIR_MOD . "/js/" . $this->module . ".js");
    $vnT->html->addScript($vnT->dir_js . "/masonry/jquery.masonry.min.js");
		$vnT->setting['menu_active'] = $this->module;
    $pID = (int)$input['itemID'];
		if($_GET['preview']==1 && $_SESSION['admin_session'])	{
			$where = " ";
		}else{
	    $vnT->DB->query("UPDATE collection SET views=views+1 WHERE p_id=$pID");
			$where = " AND display=1 ";
		}	
		$result= $vnT->DB->query("SELECT * FROM collection p, collection_desc pd
															WHERE p.p_id =pd.p_id AND pd.lang='$vnT->lang_name'
															{$where} AND pd.p_id=$pID ");
		if($row = $vnT->DB->fetch_row($result)){
			if ($row['metadesc']) $vnT->conf['meta_description'] = $row['metadesc'];
			if ($row['metakey']) $vnT->conf['meta_keyword'] = $row['metakey'];
			if ($row['friendly_title']){
				$vnT->conf['indextitle'] = $row['friendly_title'];
			}
			$link_seo = ($vnT->muti_lang) ? ROOT_URL . $vnT->lang_name . "/" : ROOT_URL;
      $link_seo .= $row['friendly_url'] . ".html";
      $row['link_share'] = $link_seo;
			$input['catID'] = $row['cat_id'];
			switch ($input['do']) {
				case 'view_360':
					if($row['link360']){
						$data['link360'] = $row['link360'];
					 	$this->skin->reset("html_360");
						$this->skin->assign("data", $data);
				    $this->skin->parse("html_360");
						$vnT->output .= $this->skin->text("html_360");
					}else{
						$vnT->output .= '<div class="noiframe360 noItem">'.$vnT->lang['collection']['no_have_360'].'</div>';
					}
					break;
				case 'desc':
					$data['p_name'] = $row['p_name'];
					$data['description'] = $row['description'];
					$this->skin->reset("html_desc");
					$this->skin->assign("data", $data);
			    $this->skin->parse("html_desc");
					echo $this->skin->text("html_desc");
					exit();
					break;
				case 'slide':
					$slide = $this->do_Slide($row);
					$data['list_for'] = $slide['list_for'];
					$data['list_nav'] = $slide['list_nav'];
					$this->skin->reset("html_slide");
					$this->skin->assign("data", $data);
			    $this->skin->parse("html_slide");
					echo $this->skin->text("html_slide");
					exit();
					break;
				default:
					$data['main'] = $this->do_Detail($row);
					$navation = get_navation($input['catID']);
			    $data['navation'] = $vnT->lib->box_navation($navation);
			    $vnT->setting['banner'] = $vnT->lib->get_child_slide('child');
			    $data['fixed_sidebar'] = $vnT->lib->fixed_sidebar();
			    $this->skin->assign("data", $data);
			    $this->skin->parse("modules");
			    $vnT->output .= $this->skin->text("modules");
					break;
			}
 		}else {
			$linkref = LINK_MOD.".html";
      @header("Location: ".$linkref."");
      echo "<meta http-equiv='refresh' content='0; url=".$linkref."'/>";
		}
  }
  function do_Detail ($info){
    global $vnT,$func,$DB,$input,$conf;
		$data = $info;
		$pid = (int)$data['p_id'];
		$cat_id = (int)$data['cat_id'];
		$cur_link = create_link('detail',$info['p_id'],$info['friendly_url']);
		$pic_w = ($vnT->setting['imgdetail_width']) ? $vnT->setting['imgdetail_width'] : 870; 
		$p_name = $vnT->func->HTML($info['p_name']);
		$picture = ($info['picture']) ? $this->module."/".$info['picture'] : $this->module."/".$vnT->setting['pic_nophoto'];
		$data['src'] = $vnT->func->get_src_modules($picture, $pic_w ,'',1,'1.5:1');
		$res_pic = $vnT->DB->query("SELECT * FROM collection_picture WHERE p_id=".$info['p_id']);
		if ($num_pic = $vnT->DB->num_rows($res_pic)){
			while ($row_pic = $vnT->DB->fetch_row($res_pic)){
				$picture = $this->module."/".$row_pic['picture'];
        $src = MOD_DIR_UPLOAD.'/'.$row_pic['picture'];
        $pic_name = ($row_pic['pic_name']) ? $row_pic['pic_name'] : $info['p_name'];
        $src_pic = $vnT->func->get_src_modules($picture, 1170 ,'',1,'2:1');
        $link_slide = $cur_link.'/?do=slide';
        $list_pic.='<div class="item"><a href="'.$link_slide.'"><img src="'.$src.'" alt="'.$pic_name.'"/></a></div>';
			}
		}
		$data['list_pic'] = $list_pic;
		$data['title'] = $vnT->func->HTML($info['p_name']);
    $data['date_post'] = @date("d/m/Y", $info['date_post']);
		$data['date_update'] = @date("d/m/Y", $info['date_update']);
		$data['text_short'] = $vnT->func->cut_string($vnT->func->check_html($info['description'],'nohtml'),400,1);
		if($info['price']){
			$data['price_text']= '<div class="collectionPrice">
											        <div class="txt">'.$vnT->lang['collection']['price'].'</div>
											        <div class="pri">'.get_price_collection($info['price']).'</div>
											      </div>';
		}
		if($info['link360']){
			$link360 = $cur_link.'/?do=view_360';
			$data['btn360'] = '<a href="'.$link360.'" target="_blank" class="_360"><img src="'.$conf['icon360'].'" /></a>';
		}
		$data['link_desc'] = $cur_link.'/?do=desc';
		if ($data['options']){
			$list_option='';
			$arr_op = unserialize($data['options']);
			$res_op = $DB->query("SELECT * FROM collection_option n, collection_option_desc nd  
														WHERE n.op_id=nd.op_id AND lang ='$vnT->lang_name' AND n.display=1 
														ORDER BY n.op_order, n.op_id DESC");
			while ($r_op = $DB->fetch_row($res_op)){
				if($arr_op[$r_op['op_id']]){
					$list_option.= '<li>
								            <div class="at">'.$func->HTML($r_op['op_name']).'</div>
								            <div class="as">'.$arr_op[$r_op['op_id']].'</div>
								            <div class="clear"></div>
								          </li>';
				}
			}
		}
		$data['row_option'] = $list_option;
		$link_share = $info['link_share'];
		$data['link_share'] = $link_share;
    //plugin
    $list_social_network = '<ul>';
    if ($vnT->setting['social_network_share']) {
      $arr_share = @explode(",", $vnT->setting['social_network_share']);
      foreach ($arr_share as $val) {
        $list_social_network .= '<li>'.$vnT->lib->get_icon_share($val, $link_share).'</li>';
      }
    }
    if ($vnT->setting['social_network_like']) {
      $arr_share = @explode(",", $vnT->setting['social_network_like']);
      foreach ($arr_share as $val) {
        $list_social_network .= '<li>'.$vnT->lib->get_icon_like($val, $link_share).'</li>';
      }
    }
    $list_social_network .= '</ul>';
    $data['list_social_network'] = $list_social_network;
    //facebook_comment
    if ($vnT->setting['facebook_comment']) {
      $data['facebook_comment'] = '<div class="facebook-comment" style="padding-top:10px;"><div class="fb-comments" data-href="'.$link_share.'" data-width="100%" data-num-posts="5" data-colorscheme="light"></div><div id="fb-root"></div><script>(function(d, s, id) {  var js, fjs = d.getElementsByTagName(s)[0];  if (d.getElementById(id)) return;  js = d.createElement(s); js.id = id;  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1&appId=' . $vnT->setting['social_network_setting']['facebook_appId'] . '";  fjs.parentNode.insertBefore(js, fjs);}(document, "script", "facebook-jssdk"));</script></div>';
    }
    $data['other_collection'] = $this->other_collection($info);
    if($vnT->setting['comment']){
      $data['comment'] = $this->do_Comment($info);
    }
    $data['link_booking'] = LINK_MOD.'/booking.html/?id='.$info['p_id'];
    if($info['video']){
    	$data['btn_video'] = '<a href="#videoFlag" id="vFlag"><span>Video</span></a>';
    	$ytbID = $vnT->lib->regex_url_youtube($info['video']);
    	$video['link_embed'] = 'https://www.youtube.com/embed/'.$ytbID.'?autoplay=1&rel=0&loop=1&showinfo=0';
    	$video['desc'] = $info['video_desc'];
    	$video_pic = MOD_DIR_UPLOAD.'/'.$info['video_pic'];
    	$video['style_bg'] = 'style="background-image:url('.$video_pic.');"';
    	$this->skin->assign("row", $video);
    	$this->skin->parse("detail.html_video");
    }
    //$data['link_booking'] = LINK_MOD.'/booking.html/?do=success';
		$this->skin->assign("data", $data);
    $this->skin->parse("detail");
		return $this->skin->text("detail");
  }
  function do_Slide ($info){
    global $vnT,$func,$DB,$input,$conf;
    $arr_out = array();
		$pic_w = 1170;
		$p_name = $vnT->func->HTML($info['p_name']);
		$data['src'] = $vnT->func->get_src_modules($picture, $pic_w ,'',1,'1.5:1');
		$res_pic = $vnT->DB->query("SELECT * FROM collection_picture WHERE p_id=".$info['p_id']);
		if ($num_pic = $vnT->DB->num_rows($res_pic)){
			while ($row_pic = $vnT->DB->fetch_row($res_pic)){
				$picture = $this->module."/".$row_pic['picture'];
        $src = MOD_DIR_UPLOAD.'/'.$row_pic['picture'];
        $pic_name = ($row_pic['pic_name']) ? $row_pic['pic_name'] : $info['p_name'];
        $src_pic = $vnT->func->get_src_modules($picture, 1170 ,'',1,'2:1');
        $src_nav = $vnT->func->get_src_modules($picture, 170 ,'',1,'2:1');
        $link_slide = $cur_link.'/?do=slide';
        $list_for.='<div class="item"><div class="img"><img src="'.$src_pic.'" alt="'.$pic_name.'"/></div></div>';
        $list_nav.='<div class="item"><div class="img"><img src="'.$src_nav.'" alt="'.$pic_name.'"/></div></div>';
			}
		}
		$arr_out['list_for'] = $list_for;
		$arr_out['list_nav'] = $list_nav;
		return $arr_out;
  }
  function other_collection ($info){
    global $DB, $func, $input, $vnT;
    $n = 10;
    $p_id = (int) $info['p_id']; 		
		$cat_id = (int) $info['cat_id'];
		$where = get_where_cat($cat_id);
    $where .= " AND p.p_id<>$p_id ";    
		$where .= " ORDER BY date_post DESC ";
    $data['list'] = scroll_collection("other_collection",$where,0,$n);
		$data['f_title'] = '<h2>'.$vnT->lang['collection']['other_collection'].'</h2>';
    $this->skin->assign("data", $data);
    $this->skin->parse("html_other_collection");
		return $this->skin->text("html_other_collection");
  }
  function do_Comment($info){
    global $DB, $func, $input, $vnT;
    $vnT->html->addScript($vnT->dir_js . "/jquery_alerts/jquery.alerts.js");
    $vnT->html->addStyleSheet($vnT->dir_js . "/jquery_alerts/jquery.alerts.css");
    $p_id = (int)$info['p_id'];
    //check comment
    $data['list_comment'] = '<div id="ext_comment" class="grid-comment">';
    $data['list_comment'] .= "<script language=\"JavaScript\"> vnTcomment.show_comment('".$p_id."','".$vnT->lang_name."','0'); </script>";
    $data['list_comment'] .= '</div>';
    $data['mem_id'] = (int)$vnT->user['mem_id'];
    $data['com_name'] = '';
    $data['com_email'] = ''; 
    if ($vnT->user['mem_id'] > 0) {
      $data['com_name'] = $vnT->user['full_name'];
      $data['com_email'] = $vnT->user['email'];
    }
    $sec_code = rand(100000, 999999);
    $_SESSION['sec_code'] = $sec_code;
    $data['code_num'] = $sec_code;
    $scode = $vnT->func->NDK_encode($sec_code);    
    $data['ver_img'] = ROOT_URI . "includes/sec_image.php?code=$scode&h=40";
    $data['p_id'] = $p_id;
    $data['lang'] = $vnT->lang_name;
    $data['mem_id'] = $vnT->user['mem_id'];
    $data['link_login'] = ROOT_URI . "member/login.html/" . $vnT->func->NDK_encode($vnT->seo_url);
    $data['err'] = $err;
    $data['link_action'] = $link;
    $this->skin->assign("data", $data);
    $this->skin->parse("html_comment");
    $textout = $this->skin->text("html_comment");
    return $textout;
  }
}
?>