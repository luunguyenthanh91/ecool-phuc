$(document).ready(function(){
  $('.slide_product').slick({
    slidesToShow: 5,
    slidesToScroll: 1,
    arrows: true,
    dots: false,
    autoplay: true,
    autoplaySpeed: 3000,
    speed: 1000,
    pauseOnHover: true,
    responsive: [
      {
        breakpoint: 991,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 1
        }
      },
      {
          breakpoint: 769,
          settings: {
              slidesToShow: 2,
              slidesToScroll: 1
          }
      }
    ]
  });
  $('.slide_intro').slick({
      slidesToShow: 3,
      slidesToScroll: 1,
      arrows: false,
      dots: false,
      autoplay: true,
      autoplaySpeed: 3000,
      speed: 1000,
      pauseOnHover: true,
      responsive: [
        {
            breakpoint: 991,
            settings: {
                slidesToShow: 3,
                slidesToScroll: 1
            }
        },
        {
            breakpoint: 769,
            settings: {
                slidesToShow: 2,
                arrows: true,
                slidesToScroll: 1
            }
        },
        {
            breakpoint: 480,
            settings: {
                slidesToShow: 1,
                arrows: true,
                slidesToScroll: 1
            }
        }
      ]
  });
  $('.slide_trademark').slick({
      slidesToShow: 6,
      slidesToScroll: 1,
      arrows: true,
      dots: false,
      autoplay: true,
      autoplaySpeed: 3000,
      speed: 1000,
      pauseOnHover: true,
      responsive: [
        {
            breakpoint: 991,
            settings: {
                slidesToShow: 3,
                slidesToScroll: 1
            }
        },
        {
            breakpoint: 769,
            settings: {
                slidesToShow: 2,
                slidesToScroll: 1
            }
        }
      ]
  });
  $('#feeling_img').slick({
      asNavFor: '#feeling_text',
      draggable: false,
      centerMode: true,
      slidesToShow: 5,
      slidesToScroll: 1,
      centerPadding: '0px',
      adaptiveHeight: false,
      focusOnSelect: true,
      speed: 500,
      responsive: [
          {
              breakpoint: 992,
              settings: {
                  slidesToShow: 3
              }
          },
          {
              breakpoint: 640,
              settings: {
                  slidesToShow: 1
              }
          }
      ]
  });
  $('#feeling_text').slick({
      asNavFor: '#feeling_img',
      draggable: false,
      slidesToShow: 1,
      slidesToScroll: 1,
      swipe: false,
      adaptiveHeight: false,
      focusOnSelect: true,
      arrows: false,
      autoplay: false,
      speed: 500
  });
  $('#vnt-banner').addClass("active");

  $('.slide_news').slick({
        slidesToShow: 3,
        slidesToScroll: 1,
        arrows: true,
        dots: false,
        infinite: true,
        autoplay: false,
        autoplaySpeed: 3000,
        speed: 1000,
        pauseOnHover : true,
        responsive: [
          {
              breakpoint: 1024,
              settings: {
                  slidesToShow: 3,
                  slidesToScroll: 1,
                  infinite: true
              }
          },
          {
              breakpoint: 768,
              settings: {
                  slidesToShow: 2,
                  slidesToScroll: 1
              }
          },
          {
              breakpoint: 379,
              settings: {
                  slidesToShow: 1,
                  slidesToScroll: 1
              }
          }
        ]
    });
});
function thongbao(){
  $.fancybox({
    href: ROOT + 'san-pham/ajax/popup.html',
    type:'ajax',
    width:'auto',
    height:'auto',
    autoSize:false,
    fitToView:false,
    padding:'0',
    wrapCSS:'mypopup',
  });
}


