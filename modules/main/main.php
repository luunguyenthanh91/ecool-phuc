<?php
/*================================================================================*\
|| 							Name code : main.php 		 		 																	  # ||
||  				Copyright © 2007 by Thai Son - CMS vnTRUST                					# ||
\*================================================================================*/
/**
 * @version : 1.0
 * @date upgrade : 11/12/2007 by Thai Son
 **/
if (! defined('IN_vnT')) {
  die('Access denied');
}
$nts = new sMain();

class sMain
{
  var $output = "";
  var $skin = "";
  var $linkUrl = "";
  var $module = "main";

  function sMain (){
    global $vnT, $input;
    include ("function_" . $this->module . ".php");
    loadSetting();
    $this->skin = new XiTemplate(DIR_MODULE . "/" . $this->module . "/html/" . $this->module . ".tpl");
    $this->skin->assign('DIR_MOD', DIR_MOD);
    $this->skin->assign('LANG', $vnT->lang);
    $this->skin->assign('INPUT', $input);
    $this->skin->assign('CONF', $vnT->conf);
    $this->skin->assign('DIR_IMAGE', $vnT->dir_images);
    $vnT->html->addStyleSheet(DIR_MOD . "/css/" . $this->module . ".css");
    $vnT->html->addScript(DIR_MOD . "/js/" . $this->module . ".js");
    // $vnT->html->addScript($vnT->dir_js . "/jquery-mousewheel-master/jquery.mousewheel.js");
    $vnT->html->addStyleSheet($vnT->dir_style . "/animate.css");
     $vnT->html->addStyleSheet("./modules/news/css/news.css");
    // $vnT->html->addScript($vnT->dir_js . "/my_effect_scroll/my_effect_scroll.js");
		// menu_active
		$vnT->setting['menu_active'] = $this->module;
		//SEO
		$link_seo = ($vnT->muti_lang) ? ROOT_URL.$vnT->lang_name."/" : ROOT_URL;
		$vnT->conf['meta_extra'] .= "\n".'<link rel="canonical" href="'.$link_seo.'"/>';
		$vnT->conf['meta_extra'] .= "\n". '<link rel="alternate" media="handheld" href="'.$link_seo.'"/>';
    $data['fixed_sidebar'] = $vnT->lib->fixed_sidebar();
    $data['banner'] = $this->get_slide_home('main');
    $data['banner_mobile'] = $this->get_slide_home_mobile('main');
    $data['banner_child_home'] = $this->get_banner_home('banner_child_home');

    $result= $vnT->DB->query("SELECT * FROM advertise
                              WHERE pos='Main Footer' AND display=1 AND lang='$vnT->lang_name'
                              AND (FIND_IN_SET('$module',module_show) OR (module_show='') ) ORDER BY l_order");
    if ($num = $vnT->DB->num_rows($result)){
      while ($row = $vnT->DB->fetch_row($result)){
        $data['banner_footer'] = ROOT_URL."vnt_upload/weblink/".$vnT->func->HTML($row['img']);
        // echo $data['banner_footer'];die;
      }
    }


    $data['collection'] = $this->collection();

    // $data['project'] = $this->project();
    $data['product_focus'] = $this->product_focus();
    $data['box_new_news'] = $this->box_new_news();
    $data['box_cat_pro'] = $this->category_focus();
    $data['news_focus'] = $this->get_news_focus();
    $data['ps_focus'] = $this->get_focus_pro();

    $data['about'] = $vnT->func->load_Sitedoc('about');
    $data['intro_home'] = $vnT->func->load_SiteDoc("intro_home");
    $data['bg_about'] = $vnT->lib->get_src_banner('bg_about');
    $data['videos'] = $this->get_videos();
    $data['banner_popup'] = $this->banner_popup();
    $this->skin->assign("data", $data);
    $this->skin->parse("modules");
    $vnT->output .= $this->skin->text("modules");
  }

  function get_slide_home ($pos = 'main'){
    global $DB, $func, $input, $vnT;
    $textout = "";
    $module = ($input['mod']) ? $input['mod'] : $vnT->conf['module'];
    $result= $vnT->DB->query("SELECT * FROM advertise
                              WHERE pos='$pos' AND display=1 AND lang='$vnT->lang_name'
                              AND (FIND_IN_SET('$module',module_show) OR (module_show='') ) ORDER BY l_order");
    if ($num = $vnT->DB->num_rows($result)){
      while ($row = $vnT->DB->fetch_row($result)){
        $data['src'] = ROOT_URL."vnt_upload/weblink/".$vnT->func->HTML($row['img']);
          if ($row['link'] == '' || $row['link'] == 'http://') {
            $link = 'javascript:;';
          } else {
            $link = (!strstr($row['link'], "http://") && !strstr($row['link'], "https://")) ? $vnT->link_root.$row['link'] : $row['link'];

          }
          // echo "<pre/>";print_r($row);die;
          $textout .= '<div class="slider__item">
                        <div class="slider-img-bg" style="background: url('.$data['src'].') center no-repeat; background-size: cover;">
                          <div class="container">
                            <div class="row">
                              <div class="col-lg-4">
                                <div class="text-center slider__description">
                                  <h2 class="ff-mobifone text-white fs-28 text-uppercase wow fadeInUp" data-wow-delay="0.5s">'.$row['title'].'</h2>
                                  <div class="slider__detail wow fadeInUp" data-wow-delay="1s">
                                    '.$row['description'].'
                                  </div>

                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>';
      }
      //<a href="'.$link.'" class="btn btn-outline-light text-white see-more wow fadeInUp" data-wow-delay="1.5s">'.$vnT->lang['main']['view_more'].'</a>
    }
    return $textout;
  }

  function get_slide_home_mobile ($pos = 'main'){
    global $DB, $func, $input, $vnT;
    $textout = "";
    $module = ($input['mod']) ? $input['mod'] : $vnT->conf['module'];
    $result= $vnT->DB->query("SELECT * FROM advertise
                              WHERE pos='$pos' AND display=1 AND lang='$vnT->lang_name'
                              AND (FIND_IN_SET('$module',module_show) OR (module_show='') ) ORDER BY l_order");
    if ($num = $vnT->DB->num_rows($result)){
      while ($row = $vnT->DB->fetch_row($result)){
        $data['src'] = ROOT_URL."vnt_upload/weblink/".$vnT->func->HTML($row['mobile_image']);
          if ($row['link'] == '' || $row['link'] == 'http://') {
            $link = 'javascript:;';
          } else {
            $link = (!strstr($row['link'], "http://") && !strstr($row['link'], "https://")) ? $vnT->link_root.$row['link'] : $row['link'];

          }
          // echo "<pre/>";print_r($row);die;
          $textout .= '<div class="slider__item">
                        <div class="slider-img-bg" style="background: url('.$data['src'].') center no-repeat; background-size: cover;">
                          <div class="container">
                            <div class="row">
                              <div class="col-lg-4">
                                <div class="text-center slider__description">
                                  <h2 class="ff-mobifone text-white fs-28 text-uppercase wow fadeInUp" data-wow-delay="0.5s">'.$row['title'].'</h2>
                                  <div class="slider__detail wow fadeInUp" data-wow-delay="1s">
                                    '.$row['description'].'
                                  </div>

                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>';
      }
      //<a href="'.$link.'" class="btn btn-outline-light text-white see-more wow fadeInUp" data-wow-delay="1.5s">'.$vnT->lang['main']['view_more'].'</a>
    }
    return $textout;
  }


  // Banner cong nghe hien dai, ben bi
  function get_banner_home($pos = 'banner_child_home'){
    global $DB, $func, $input, $vnT;
    $textout = "";
    $module = ($input['mod']) ? $input['mod'] : $vnT->conf['module'];
    $result= $vnT->DB->query("SELECT * FROM advertise
                              WHERE pos='$pos' AND display=1 AND lang='$vnT->lang_name'
                              AND (FIND_IN_SET('$module',module_show) OR (module_show='') ) ORDER BY l_order");
    if ($num = $vnT->DB->num_rows($result)){
      while ($row = $vnT->DB->fetch_row($result)){
        $data['src'] = ROOT_URL."vnt_upload/weblink/".$vnT->func->HTML($row['img']);
          if ($row['link'] == '' || $row['link'] == 'http://') {
            $link = 'javascript:;';
          } else {
            $link = (!strstr($row['link'], "http://") && !strstr($row['link'], "https://")) ? $vnT->link_root.$row['link'] : $row['link'];

          }
          // echo "<pre/>";print_r($row);die;
          $textout .= '<div class="col-12 col-md-4">
                        <div class="portfolio__item position-relative wow fadeInUp" data-wow-delay="1.5s"><img style="height: auto;" class="w-100" src="'.$data['src'].'">
                          <p class="position-absolute text-white">'.$row['title'].'</p>
                        </div>
                      </div>';
      }
    }
    return $textout;
  }



  function project ($pos = 'project'){
    global $DB, $func, $input, $vnT;
    $textout = "";
    $module = ($input['mod']) ? $input['mod'] : $vnT->conf['module'];
    $result= $vnT->DB->query("SELECT * FROM advertise
                              WHERE pos='$pos' AND display=1 AND lang='$vnT->lang_name'
                              AND (FIND_IN_SET('$module',module_show) OR (module_show='') ) ORDER BY l_order");
    if ($num = $vnT->DB->num_rows($result)){
      while ($row = $vnT->DB->fetch_row($result)){
        $data['title'] = $vnT->func->HTML($row['title']);
        if ($row['link'] == '' || $row['link'] == 'http://') {
          $data['link'] = 'javascript:;';
        } else {
          $data['link'] = (!strstr($row['link'], "http://") && !strstr($row['link'], "https://")) ? $vnT->link_root.$row['link'] : $row['link'];
        }
        $data['src'] = ROOT_URL."vnt_upload/weblink/".$vnT->func->HTML($row['img']);
        $this->skin->reset("item_project");
        $this->skin->assign("data", $data);
        $this->skin->parse("item_project");
        $nd['list_project'] .= $this->skin->text("item_project");
      }
      $nd['category'] = $this->project_cat_focus();
      //$nd['link_project'] = $vnT->link_root.$vnT->setting['seo_name'][$vnT->lang_name]['project'].'.html';
      $this->skin->reset("html_project");
      $this->skin->assign("data", $nd);
      $this->skin->parse("html_project");
      $textout = $this->skin->text("html_project");
    }
    return $textout;
  }
  function project_cat_focus(){
    global $vnT,$func,$DB,$input;
    $text = '';
    $where = " AND is_focus = 1 AND parentid = 0 ";
    $where .= " ORDER BY cat_order ASC, p.cat_id DESC, date_post DESC";
    $result = $DB->query("SELECT p.cat_id, cat_name,friendly_url
                          FROM project_category p, project_category_desc pd
                          WHERE p.cat_id=pd.cat_id AND display=1 AND lang='$vnT->lang_name' $where");
    $link_pro = $vnT->link_root.$vnT->setting['seo_name'][$vnT->lang_name]['project'].'.html';
    if($num = $DB->num_rows($result)){
      $text .= '<ul>';
      while ($row = $DB->fetch_row($result)) {
        $cat_id = (int) $row['cat_id'];
        $link = create_link("category",$cat_id,$row['friendly_url']);
        $cat_name = $vnT->func->HTML($row['cat_name']);
        $text .= '<li><a href="'.$link.'" title="'.$cat_name.'">'.$cat_name.'</a></li>';
      }
      $text .= '<li class="all"><a href="'.$link_pro.'">'.$vnT->lang['main']['view_all'].'</a></li>';
      $text .= '</ul>';
    }
    return $text;
  }
  function collection(){
    global $vnT,$func,$DB,$input;
    $textout = '';
    $data['slide_collection'] = $this->get_slide_collection('collection');
    $data['category'] = $this->collection_cat_focus();
    $this->skin->reset("html_collection");
    $this->skin->assign("data", $data);
    $this->skin->parse("html_collection");
    $textout = $this->skin->text("html_collection");
    return $textout;
  }
  function get_slide_collection ($pos = 'collection'){
    global $DB, $func, $input, $vnT;
    $textout = "";
    $module = ($input['mod']) ? $input['mod'] : $vnT->conf['module'];
    $result= $vnT->DB->query("SELECT * FROM advertise
                              WHERE pos='$pos' AND display=1 AND lang='$vnT->lang_name'
                              AND (FIND_IN_SET('$module',module_show) OR (module_show='') ) ORDER BY l_order");
    if ($num = $vnT->DB->num_rows($result)){
      $textout .= '<div id="slideCollect" class="slick-init">';
      while ($row = $vnT->DB->fetch_row($result)){
        $title = $vnT->func->HTML($row['title']);
        if ($row['link'] == '' || $row['link'] == 'http://') {
          $link = 'javascript:;';
        } else {
          $link = (!strstr($row['link'], "http://") && !strstr($row['link'], "https://")) ? $vnT->link_root.$row['link'] : $row['link'];
        }
        $src = ROOT_URL."vnt_upload/weblink/".$vnT->func->HTML($row['img']);
        $textout.= '<div class="item">
                      <div class="img"><a href="'.$link.'"><img src="'.$src.'" alt="'.$title.'" /></a></div>
                      <div class="tend"><h3>'.$title.'</h3></div>
                    </div>';
      }
      $textout .= '</div>';
    }
    return $textout;
  }
  function collection_cat_focus(){
    global $vnT,$func,$DB,$input;
    $text = '';
    $where = " AND is_focus = 1 AND parentid = 0 ";
    $where .= " ORDER BY cat_order ASC, p.cat_id DESC, date_post DESC LIMIT 0,3 ";
    $result = $DB->query("SELECT p.cat_id,picture, cat_name,friendly_url
                          FROM collection_category p, collection_category_desc pd
                          WHERE p.cat_id=pd.cat_id AND display=1 AND lang='$vnT->lang_name' $where");
    $link_pro = $vnT->link_root.$vnT->setting['seo_name'][$vnT->lang_name]['collection'].'.html';
    if($num = $DB->num_rows($result)){
      while ($row = $DB->fetch_row($result)) {
        $cat_id = (int) $row['cat_id'];
        $data['link'] = create_link("category",$cat_id,$row['friendly_url']);
        $data['cat_name'] = $vnT->func->HTML($row['cat_name']);
        $data['src'] = ROOT_URL."vnt_upload/collection/".$row['picture'];
        $this->skin->reset("item_collection_category");
        $this->skin->assign("data", $data);
        $this->skin->parse("item_collection_category");
        $text .= $this->skin->text("item_collection_category");
      }
    }
    return $text;
  }
  function get_news_focus(){
    global $DB, $func, $input, $vnT;
    $textout = '';
    $sql = "SELECT * FROM news n, news_desc nd
            WHERE n.newsid = nd.newsid AND focus_main = 1 AND display = 1 AND lang = '{$vnT->lang_name}'
            ORDER BY focus_order ASC, n.newsid DESC LIMIT 0,3";
    $result = $DB->query($sql);
    if($num = $DB->num_rows($result)){
      while ($row = $DB->fetch_row($result)) {
        $data['link'] = create_link("detail",$row['p_id'],$row['friendly_url']);
        $picture = ($row['picture']) ? "news/".$row['picture'] : "news/no_photo.jpg";
        $data['date_post'] = @date('d/m/Y',$row['date_post']);
        $data['date_post_meta'] = @date('Y-m-d',$row['date_post']);
        $data['date_update_meta'] = @date('Y-m-d',$row['date_update']);
        $data['title'] = $vnT->func->HTML($row['title']);
        //$data['cat_name'] = $vnT->setting['news_cat'][$row['cat_id']];
        $data['short']=$vnT->func->cut_string($vnT->func->check_html($row['short'],'nohtml'),300,1);
        $data['src'] = $vnT->func->get_src_modules($picture, 632 ,'',1,'1.8:1');
        $this->skin->reset("item_news");
        $this->skin->assign("data", $data);
        $this->skin->parse("item_news");
        $nd['list_news'] .= $this->skin->text("item_news");
      }
      $nd['link_news'] = $vnT->link_root.$vnT->setting['seo_name'][$vnT->lang_name]['news'].'.html';
      $this->skin->reset("html_news_focus");
      $this->skin->assign("data", $nd);
      $this->skin->parse("html_news_focus");
      $textout = $this->skin->text("html_news_focus");
    }
    return $textout;
  }
  function product_focus(){
    global $vnT,$func,$DB,$input;
    $textout = '';
    $where = " AND focus = 1 ";
    //$where.= " AND FIND_IN_SET('$cat_id',cat_list)<>0 ";
    $where.= " ORDER BY focus_order ASC, p_order DESC, date_post DESC ";
    $result = $DB->query("SELECT * FROM products p, products_desc pd
                          WHERE p.p_id=pd.p_id AND display=1 AND lang='$vnT->lang_name' $where LIMIT 0,12");
    if($num = $DB->num_rows($result)){
      $w = ($vnT->setting['img_width_grid']) ? $vnT->setting['img_width_grid'] : 240;
      while ($row = $DB->fetch_row($result)) {
        $pID = (int) $row['p_id'];
        $data['link'] = create_link("detail",$pID,$row['friendly_url']);
        $picture = ($row['picture']) ? "product/".$row['picture'] : "product/".$vnT->setting['pic_nophoto'];
        $data['pic'] = $vnT->func->get_pic_modules($picture, $w, 0, " alt='".$row['p_name']."' title='".$row['p_name']."' ",1,0,array("fix_width"=>1));
        $data['p_name'] = $vnT->func->HTML($row['p_name']);
        $data['price'] = $row['price'];
        $data['price_text'] = get_price_product($row['price']);
        $data['price_old_text'] = $data['discount'] = '';
        if($row['price_old'] && $row['price_old'] > $row['price']){
          $data['price_old_text'] = '<div class="nor">'.get_price_product($row['price_old']).'</div>';
          $dis = $row['price_old'] - $row['price'];
          $discount = ROUND(($dis * 100)/$row['price_old']);
          $data['discount'] = ($discount > 1) ? '<div class="rib pro">-'.$discount.'%</div>' : '';
        }
        $data['maso'] = $row['maso'];
        $data['box_status'] = box_status($row['status']);
        $data['out_stock'] = ($row['is_parent']==0 && $row['onhand'] < 1) ? '<div class="hethang">'.$vnT->lang['main']['out_stock'].'</div>' : '';
        $this->skin->reset("item_product");
        $this->skin->assign("data", $data);
        $this->skin->parse("item_product");
        $nd['list_product'] .= $this->skin->text("item_product");
      }
      $nd['list_category'] = $this->category_focus();
      $nd['link_product'] = $vnT->link_root.$vnT->setting['seo_name'][$vnT->lang_name]['product'].'.html';
      $this->skin->reset("html_product_focus");
      $this->skin->assign("data", $nd);
      $this->skin->parse("html_product_focus");
      $textout = $this->skin->text("html_product_focus");
    }
    return $textout;
  }
  function category_focus(){
    global $vnT,$func,$DB,$input;
    $text = '';
    $where = "AND is_focus=1 AND parentid = 0 ";
    $where .= " ORDER BY cat_order asc, p.cat_id DESC, date_post DESC ";
    $result = $DB->query("SELECT *
                          FROM product_category p, product_category_desc pd
                          WHERE p.cat_id=pd.cat_id AND display=1 AND lang='$vnT->lang_name' $where");
    $result1 = $DB->query("SELECT *
                          FROM product_category p, product_category_desc pd
                          WHERE p.cat_id=pd.cat_id AND display=1 AND lang='$vnT->lang_name' $where");
    $link_pro .= $vnT->link_root.$vnT->setting['seo_name'][$vnT->lang_name]['product'].'.html';
    if($num = $DB->num_rows($result)){
      $text = '<ul class="nav justify-content-center" role="tablist">';
      $stt = 1;
      while ($row = $DB->fetch_row($result)) {
        if($stt == 1){
          $act1 = "active";
        }else{
          $act1 = "";
        }

        $cat_id = (int) $row['cat_id'];

        $text .= '<li class="btn-product-tab nav-item '.$act1.' wow fadeInUp" data-wow-duration="1s" data-wow-delay="1s"><a class="nav-link text-black fs-20 font-weight-medium text-uppercase '.$act1.'" data-toggle="pill" href="#pills-'.$cat_id.'" role="tab" aria-controls="pills-'.$cat_id.'" aria-selected="true">'.$row['cat_name'].'</a></li>';
        $stt++;
      }
      $text .= '</ul>';

      // echo "<pre/>";print_r("");die;

      $stt1 = 1;
       $text .= '<div class="tab-content " id="pills-tabContent">';
      while ($row1 = $DB->fetch_row($result1)) {

        if($stt1 == 1){
          $act = "active";
        }else{
          $act = "";
        }
        $cat_id1 = (int) $row1['cat_id'];
        $link1 = create_link("category",$cat_id1,$row1['friendly_url']);
        $picture = ($row1['picture']) ? "product/".$row1['picture'] : "product/".$vnT->setting['pic_nophoto'];
        $data['pic'] = $vnT->func->get_pic_modules($picture, $w, 0, " alt='".$row1['name']."' title='".$row1['name']."' ",1,0,array("fix_width"=>1));
        $cat_name = $vnT->func->HTML($row1['cat_name']);
        $text .= '<div class="tab-pane show '.$act .'" id="pills-'.$cat_id1.'" role="tabpanel" aria-labelledby="pills-home-tab">
          <div class="container">
            <div class="row">
              <div class="col-12 col-md-6 col--product-left wow fadeInUp" data-wow-delay="1s" ><a class="h-100 d-flex flex-column justify-content-around bg-gray product-left" href="'.$link1.'">'.$data['pic'].'
                  <div class="detail text-center">
                    <p>'.$cat_name.'</p>
                    <div class="desc">Tiết kiệm điện tuyệt đối với<br/>Điều hòa wifi Ecool<br/>Điều khiển bằng app tiện dụng</div>
                  </div></a></div>
              <div class="col-12 col-md-6 col--product-right wow fadeInUp"data-wow-delay="1.5s">
                <div class="row">';
                $where_p = " AND cat_id =  ".$row1['cat_id'];
                        $where_p .= " ORDER BY p_order DESC, p.p_id DESC, date_post DESC LIMIT 0,4";
                        $result_p = $DB->query("SELECT *
                                          FROM products p, products_desc pd
                                          WHERE p.p_id=pd.p_id AND display=1 AND lang='$vnT->lang_name' $where_p");
                            if($num_p = $DB->num_rows($result_p)){
                              while ($row_p = $DB->fetch_row($result_p)) {
                                  $p_id_p = (int) $row_p['p_id'];
                                  $link_p = create_link("detail",$p_id_p,$row_p['friendly_url']);
                                  $picture_p = ($row_p['picture']) ? "product/".$row_p['picture'] : "product/".$vnT->setting['pic_nophoto'];
                                    $data['pic'] = $vnT->func->get_pic_modules($picture_p, $w, 0, " alt='".$row_p['p_name']."' title='".$row_p['p_name']."' ",1,0,array("fix_width"=>1));
                                $text .= '<div class="product-right col-6  fadeInUp"><a class="h-100 d-flex flex-column justify-content-around bg-gray" href="'.$link_p.'">'.$data['pic'].'
                                    <p class="m-0 text-center fs-16 text-black">'.$row_p['p_name'].'</p></a></div>';
                                }
                }
                $text .= '</div>
              </div>
            </div>
          </div>
        </div>';


        $stt1++;
      }
      $text .= '</div>';

    }
    return $text;
  }

  function get_videos (){
    global $DB, $func, $input, $vnT;
    $textout = "";
    $result = $DB->query("SELECT *
                        FROM dealer n , dealer_desc nd
                        WHERE n.did=nd.did
                        AND lang='$vnT->lang_name'
                        LIMIT 0,10 ");
    if ($num = $vnT->DB->num_rows($result)){
      while ($row = $vnT->DB->fetch_row($result)){
        // echo $row['short'] ;
        $data['title'] = $vnT->func->HTML($row['title']);
        $data['src'] = ROOT_URL."vnt_upload/dealer/".$vnT->func->HTML($row['picture']);
        $data['description'] = $row['description'];
        $data['link'] = create_link("detail",$row['did'],$row['friendly_url']);
        $textout .= '<div class="video__item wow fadeInUpBig" data-wow-delay="1s">
          <div class="video__img position-relative">
            <a href="'.$data['link'].'">
              <img src="'.$data['src'].'">
            </a>
          </div>
          <div class="video__content">
            <p class="fs-17 text-white text-center">'.$row['short'].'</p>
            <h3 class="text-white fs-33 font-weight-semi-bold text-center text-uppercase">'.$data['title'] .'</h3>
          </div>
        </div>';
      }

    }
    return $textout;
  }

  function get_focus_pro (){
    global $DB, $func, $input, $vnT;
    $textout = "";
    $result = $DB->query("SELECT *
                        FROM products p , products_desc pd
                        WHERE p.p_id=pd.p_id
                        AND lang='$vnT->lang_name'
                        LIMIT 0,5 ");
    if ($num = $vnT->DB->num_rows($result)){
      while ($row = $vnT->DB->fetch_row($result)){
        $data['p_name'] = $vnT->func->HTML($row['p_name']);
        $picture = ($row['picture']) ? "product/".$row['picture'] : "product/".$vnT->setting['pic_nophoto'];
        $data['src'] = $vnT->func->get_src_modules($picture, $w ,'',1,'1.5:1');
        $data['link'] = create_link("detail",$row['p_id'],$row['friendly_url']);
        $textout .= '<div class="col col--home-product"><a class="d-inline-block product-card text-center" href="'.$data['link'].'"><img src="'.$data['src'].'">
              <p class="fs-14 text-black">'.$data['p_name'].'</p>
              <div class="btn btn--detail">Chi tiết</div></a></div>';
      }

    }
    return $textout;
  }



  function banner_popup($pos = 'main_popup'){
    global $vnT,$DB;
    $textout = '';
    if(empty($_SESSION['popup']) || $_SESSION['popup'] != 1){
      $result = $DB->query("SELECT * FROM advertise
                            WHERE pos='$pos' AND display=1 AND lang='$vnT->lang_name' ORDER BY l_order LIMIT 0,1");
      if($row = $DB->fetch_row($result)){
        $link_pro = $vnT->link_root.$vnT->setting['seo_name'][$vnT->lang_name]['product'];
        $data['link_popup'] = $link_pro.'/ajax/popup.html';
        $this->skin->reset("html_popup");
        $this->skin->assign("data", $data);
        $this->skin->parse("html_popup");
        $textout = $this->skin->text("html_popup");
        $_SESSION['popup'] = 1;
      }
    }
    return $textout;
  }

  function box_new_news ()
  {
    global $vnT, $func, $DB, $conf,$input;
    $textout = "";
    $where = ' ORDER BY focus_order DESC, date_post DESC';
    $result = $DB->query("SELECT *
                        FROM news n , news_desc nd
                        WHERE n.newsid=nd.newsid
                        AND lang='$vnT->lang_name'
                        AND display=1

                        {$where}
                        LIMIT 0,10 ");
    if ($num = $DB->num_rows($result)) {
      $i = 0;
      $w = ($vnT->setting['imgthumb_width']) ? $vnT->setting['imgthumb_width'] : 585;

      while ($row = $DB->fetch_row($result)) {
        $i ++;
        $link = create_link("detail", $row['newsid'], $row['friendly_url'] );



        $title = $vnT->func->cut_string($vnT->func->HTML($row['title']),60,1);
        $short = $vnT->func->cut_string($vnT->func->check_html($row['short'],'nohtml'),120,1);
        $pic = ($row['picture']) ? $pic = '<div class="img"><table width="'.$w.'" border="0" cellspacing="0" cellpadding="0"><tr><td height="'.$w.'" align="center">'.$this->get_pic_news($row['picture'],$w ,1).'</td></tr></table></div>' : "" ;


        $last = ($i==$num) ? "class='last'" : "";

        $text .= '<div class="item effect_zoom" itemscope="" itemtype="http://schema.org/Article">
                                <div class="wrap_news">
                                    <div class="i-{DIR_IMAGE}" itemprop="image" itemscope="" itemtype="https://schema.org/ImageObject">
                                        <a href="'.$link.'" title="">
                                            '.$this->get_pic_news($row['picture'],$w ,1).'
                                            <span class="i-date">04/12/2018</span>
                                        </a>
                                    </div>
                                    <div class="i-desc">
                                        <div class="i-title">
                                            <h3 itemprop="headline"><a itemprop="url" href="'.$link.'" title="">'.$row['title'].'</a></h3>
                                        </div>
                                        <div class="i-content" itemprop="description">'.$short.'</div>
                                    </div>
                                </div>
                            </div>';
      }

      $textout .=  $text;

     }

    return $textout ;
  }

  /*-------------- create_link --------------------*/
  function create_link ($act, $id, $title, $extra = "")
  {
    global $vnT, $func, $DB, $conf;

    switch ($act)
    {
      case "category" : $text = $vnT->link_root.$title.".html";  break ;
      case "detail" :  $text = $vnT->link_root.$title.".html";   break ;
      default : $text = LINK_MOD."/".$act."/".$id."/".$func->make_url($title).".html";   break ;
    }

    $text .= ($extra) ? "/" . $extra : "";
    return $text;
  }

  function get_pic_news ($picture, $w = "" ,$thumb=0, $ext = "")
  {
    global $vnT, $func;
    $out = "";
    $ext = "";
    $w_thumb = ($vnT->setting['imgthumb_width']) ? $vnT->setting['imgthumb_width'] : 100;
    $linkhinh = "vnt_upload/news/" . $picture;
    $linkhinh = str_replace("//", "/", $linkhinh);
    $dir = substr($linkhinh, 0, strrpos($linkhinh, "/"));
    $pic_name = substr($linkhinh, strrpos($linkhinh, "/") + 1);
    if ($w) {
      if ($thumb && file_exists($vnT->conf['rootpath'] . $linkhinh)) {
        $file_thumbs = $dir . "/thumbs/{$w}_" . substr($linkhinh, strrpos($linkhinh, "/") + 1);
        $linkhinhthumbs = $vnT->conf['rootpath'] . $file_thumbs;
        if (! file_exists($linkhinhthumbs)) {
          if (@is_dir($vnT->conf['rootpath'] . $dir . "/thumbs")) {
            @chmod($vnT->conf['rootpath'] . $dir . "/thumbs", 0777);
          } else {
            @mkdir($vnT->conf['rootpath'] . $dir . "/thumbs", 0777);
            @chmod($vnT->conf['rootpath'] . $dir . "/thumbs", 0777);
          }
          // thum hinh
          $vnT->func->thum($vnT->conf['rootpath'] . $linkhinh, $linkhinhthumbs, $w);
        }
        $src = ROOT_URI . $file_thumbs;
      } else {
        if ($w < $w_thumb)
          $ext = " width='$w' ";
        $src = ROOT_URI . $dir . "/thumbs/" . $pic_name;
      }
    } else {
      $src = MOD_DIR_UPLOAD . "/" . $picture;
    }
    $alt = substr($pic_name, 0, strrpos($pic_name, "."));
    $out = "<img  src=\"{$src}\" alt=\"{$alt}\"  {$ext} >";
    return $out;
  }
}
?>
