<?php
/*================================================================================*\
|| 							Name code : function_main.php 		 		 											  # ||
||  				Copyright © 2007 by Thai Son - CMS vnTRUST                					# ||
\*================================================================================*/
/**
 * @version : 1.0
 * @date upgrade : 11/12/2007 by Thai Son
 **/
if (! defined('IN_vnT')) {
  die('Access denied');
}
define("DIR_MOD", ROOT_URI . "modules/main");
define("DIR_MOD_IMAGE", ROOT_URI . "modules/main/images");

function loadSetting (){
	global $vnT,$func,$DB,$conf;
	$setting = array();
	$result = $DB->query("SELECT * FROM product_setting WHERE lang='$vnT->lang_name' ");
	$setting = $DB->fetch_row($result);
	foreach ($setting as $k => $v)	{
		$vnT->setting[$k] = stripslashes($v);
	}
	$res_s = $DB->query("SELECT status_id, picture FROM product_status WHERE display = 1 ");
  while ($row_s = $DB->fetch_row($res_s)) {
    $vnT->setting['pic_status'][$row_s['status_id']] = ($row_s['picture']) ? "<img src=\"".ROOT_URL."vnt_upload/product/".$row_s['picture']."\"/>" : "";
  }
  // $res_s= $DB->query("SELECT p.cat_id,cat_code,list_price,cat_name
		// 									FROM product_category p, product_category_desc pd
		// 									WHERE p.cat_id = pd.cat_id AND lang='$vnT->lang_name' AND display = 1 ");
  // while ($row_s = $DB->fetch_row($res_s)){
  // 	$vnT->setting['cat_name'][$row_s['cat_id']] = $vnT->func->HTML($row_s['cat_name']);
  //   $vnT->setting['cat_code'][$row_s['cat_id']] = $row_s['cat_code'];
  //   $arr = explode('_', $row_s['cat_code']);
  //   $vnT->setting['cat_root'][$row_s['cat_id']] = $arr[0];
  //   $vnT->setting['cat_list_price'][$row_s['cat_id']] = $row_s['list_price'];
  // }
  unset($row_s);
	unset($setting);
}

function create_link ($act,$id,$title,$extra=""){
	global $vnT,$func,$DB,$conf;
	switch ($act){
		case "category" : $text = $vnT->link_root.$title.".html";	break;
		case "detail" : $text = $vnT->link_root.$title.".html";	break;
		default : $text = LINK_MOD."/".$act."/".$id."/".$func->make_url($title).".html"; break;
	}
	$text .= ($extra) ? "/".$extra : "";
	return $text;
}
function get_price_product ($price,$unit ='đ',$default=""){
	global $func,$DB,$conf,$vnT;
	if ($price){
		$price = $func->format_number($price).$unit;
	}else{
		$price = ($default) ? $default : $vnT->lang['main']['price_default'];
	}
	return $price;
}
function box_status($list_status){
	global $vnT, $DB;
	$text = '';
	$arr = explode(',', $list_status);
	if(is_array($arr)){
		foreach ($arr as $key => $value) {
			$text .= '<div class="rib">'.$vnT->setting['pic_status'][$value].'</div>';
		}
	}
	return $text;
}
?>