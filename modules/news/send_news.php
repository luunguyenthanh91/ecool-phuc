<?php
define('IN_vnT', 1);
define('DS', DIRECTORY_SEPARATOR);
require_once ("../../_config.php");
require_once ($conf['rootpath'] . "includes/class_db.php");
$DB = new DB();
//load mailer
require_once ($conf['rootpath'] . "libraries/phpmailer/phpmailer.php");
$vnT->mailer = new PHPMailer();
require_once ($conf['rootpath'] . "includes/class_functions.php");
$func = new Func_Global();
$conf = $func->fetchDbConfig($conf);
$vnT->conf = $conf;
 
 
$vnT->lang_name = (isset($_GET['lang'])) ? $_GET['lang'] : "vn";
$func->load_language('news');
$ok_send = 0;
$mess = "";
$id = (int) $_GET["id"];

  
$sql = "select * from news n, news_desc nd  WHERE n.newsid=nd.newsid AND display=1 AND lang='$vnT->lang_name'	AND n.newsid ={$id}";
$result = $DB->query($sql);
if ($row = $DB->fetch_row($result)) 
{ 
	$title_news = $row['title'];
	if ($func->is_muti_lang()) {
		 $address = $conf['rooturl'] .$vnT->lang_name."/";
	}else{
		 $address = $conf['rooturl'] ;
	}
	$address .= "news/detail/" . $row['friendly_url'] . "-". $news_id . ".html";
}
 

$data = $_POST;
$data['mess_info'] = '<div class="mess_info">'.$title_news.'</div>'; 

if (isset($_POST['btnSend'])) 
{
	
  $content_email = $func->load_MailTemp("send_news");
  $qu_find = array(
    '{name}' , 
    '{content}' , 
    '{url}' , 
    '{host}');
  $qu_replace = array(
    $_POST['name_from']." (".$_POST['email_from'].") " , 
    $_POST['content'] , 
    $address , 
    $_SERVER['HTTP_HOST']);
  $message = str_replace($qu_find, $qu_replace, $content_email);
  $subject = $vnT->lang['news']['subject_send_news'];
  $sent = $func->doSendMail($_POST['email_to'], $subject, $message, $vnT->conf['email']);
  $mess = $func->html_mess($vnT->lang['news']['send_email_success']);
  //end send
  $ok_send = 1;
}

mt_srand((double) microtime() * 1000000);
$num = mt_rand(100000, 999999);
$scode = $func->NDK_encode($num);
$img_code = "../../includes/do_image.php?code=$scode";
?>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />

<link href="css/popup.css" rel="stylesheet" type="text/css" />
<script language=javascript>
	function checkform(f) {			
		var re =/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,5})$/gi;
		
		var name_from = f.name_from.value;
		if (name_from == '') {
			alert('<?=$vnT->lang['news']['err_name_from_empty']?>');
			f.name_from.focus();
			return false;
		}
			 
		email_from = f.email_from.value;
		if (email_from == '') {
			alert('<?=$vnT->lang['news']['err_email_from_empty']?>');
			f.email_from.focus();
			return false;
		}
		if (email_from != '' && email_from.match(re)==null) {
			alert('<?=$vnT->lang['news']['err_email_invalid']?>');
			f.email_from.focus();
			return false;
		}
		
		var name_to = f.name_to.value;
		if (name_to == '') {
			alert('<?=$vnT->lang['news']['err_name_to_empty']?>');
			f.name_to.focus();
			return false;
		}
			 
		email_to = f.email_to.value;
		if (email_to == '') {
			alert('<?=$vnT->lang['news']['err_email_to_empty']?>');
			f.email_to.focus();
			return false;
		}
		if (email_to != '' && email_to.match(re)==null) {
			alert('<?=$vnT->lang['news']['err_email_invalid']?>');
			f.email_to.focus();
			return false;
		}
		
		var content = f.content.value;
		if (content == '') {
			alert('<?=$vnT->lang['news']['err_content_empty']?>');
			f.content.focus();
			return false;
		}
		
		if (f.h_code.value != f.security_code.value ) {
			alert('<?=$vnT->lang['news']['err_security_code_invalid']?>');
			f.security_code.focus();
			return false;
		}

		return true;
	}
</script>

 
</head>
<body  >
<div   style="margin:5px" >
<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td  height="30" class="font_f_title" ><img src="images/mail_send.gif"  align="absmiddle"/>&nbsp;<?=$vnT->lang['news']['f_intro_to_friend']?></td>
	</tr>
	<tr>
		<td  bgcolor="#FF0000" height="2" ></td>
	</tr>
</table>
<br />

<?php
	 
	 echo $mess;
  if ($ok_send==0)
	{
     echo $data['mess_info'] ;
 ?>
  
<form action="<?=$link_action?>" method="post" name="myFrom" id="myFrom" onSubmit="return checkform(this);">

<table width="100%" border="0" cellspacing="3" cellpadding="3">
  <tr>
    <td><label for="name"><strong><?php echo $vnT->lang['news']['name_from']; ?></strong> <font color="red">(*)</font> :</label>
    <input class="textfiled" name="name_from" type="text" id="name_from"  style="width:100%" maxlength="250" value="<?=$_POST['name_from']?>" />
    </td>
    <td><label for="email"><strong><?php echo $vnT->lang['news']['email_from']; ?></strong> <font color="red">(*)</font> :</label>
    <input class="textfiled" name="email_from" type="text" id="email_from"  style="width:100%" maxlength="250" value="<?=$_POST['email_from']?>" /></td>
  </tr>
  
   <tr>
    <td><label for="name"><strong><?php echo $vnT->lang['news']['name_to']; ?></strong> <font color="red">(*)</font> :</label>
    <input class="textfiled" name="name_to" type="text" id="name_to"  style="width:100%" maxlength="250" value="<?=$_POST['name_to']?>" />
    </td>
    <td><label for="email"><strong><?php echo $vnT->lang['news']['email_to']; ?></strong> <font color="red">(*)</font> :</label>
    <input class="textfiled" name="email_to" type="text" id="email_to"  style="width:100%" maxlength="250" value="<?=$_POST['email_to']?>" /></td>
  </tr>
  
  <tr>
    <td colspan="2" ><label for="message"><strong><?php echo $vnT->lang['news']['message']; ?></strong> <font color="red">(*)</font> :</label>
     <textarea class="textarea" name="content"  style="width:100%" rows="5"  id="content"><?=$_POST['content']?></textarea>
     </td>
  </tr>
  
  <tr>
    <td align="left" nowrap>  
    	<strong><?php echo $vnT->lang['news']['security_code']; ?> :</strong>  <font color="red">(*)</font> <input id="security_code" name="security_code" size="15" maxlength="6" class="textfiled"/>&nbsp;<img src="<?=$img_code?>" align="absmiddle" />
        
    </td>
    <td align="right">  
    		<input type="hidden" name="h_code" value="<?=$num?>">
        <button  id="btnSend" name="btnSend" type="submit" class="btn" value="<?php echo $vnT->lang['news']['btn_send']; ?>" ><span ><?php echo $vnT->lang['news']['btn_send']; ?></span></button>&nbsp; 
        <button  id="btnReset" name="btnReset" type="reset" class="btn" value="<?php echo $vnT->lang['news']['btn_reset']; ?>" onClick="self.parent.tb_remove();" ><span ><?php echo $vnT->lang['news']['btn_reset']; ?></span></button>   </td>
  </tr>
  
</table>
 
  
  </form>
<?php
	}else{
?>
	 <table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
	<td height="150"><div align="center" class="font_err"><a href = "javascript:self.parent.tb_remove();">Close Window</a> </div> </td>
	</tr>
	</table> 
<?php  
	}
?>
		 
 
</div>
</body>

</html>