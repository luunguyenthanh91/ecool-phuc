<?php
/*================================================================================*\
|| 							Name code : function_news.php 		 		 												  # ||
||  				Copyright © 2007 by Thai Son - CMS vnTRUST                					# ||
\*================================================================================*/
/**
 * @version : 1.0
 * @date upgrade : 17/12/2007 by Thai Son
 **/
/*
include Function - News cat
include Function - News Focus
*/
if (! defined('IN_vnT')) {
  die('Access denied');
}
define("DIR_MOD", ROOT_URI . "modules/news");
define('MOD_DIR_UPLOAD', ROOT_URI . 'vnt_upload/news');
define("MOD_DIR_IMAGE", ROOT_URI . "modules/news/images");
define("LINK_MOD", $vnT->link_root . $vnT->setting['seo_name'][$vnT->lang_name]['news']);

function loadSetting (){
  global $vnT, $func, $DB, $conf;
  $setting = array();
  $result = $DB->query("SELECT * FROM news_setting WHERE lang='$vnT->lang_name' ");
  $setting = $DB->fetch_row($result);
  foreach ($setting as $k => $v) {
    $vnT->setting[$k] = stripslashes($v);
  }
  $res_s= $DB->query("SELECT n.cat_id, cat_name FROM news_category n, news_category_desc nd
                      WHERE n.cat_id = nd.cat_id AND display = 1 AND lang='$vnT->lang_name' ");
  while ($row_s = $DB->fetch_row($res_s)) {
    $vnT->setting['cat_name'][$row_s['cat_id']] = $vnT->func->HTML($row_s['cat_name']);
  }
  unset($setting);
}
function get_height_pic ($w, $tyle=0){
	global $vnT, $input;
	$tyle = ($tyle) ? $tyle : $vnT->setting['tyle'];
	$tyle = ($tyle > 0) ? $tyle : 1;
	$height = $w/$tyle;
	$height = round($height,0);
 	return $height;
}
function create_link ($act, $id, $title, $extra = ""){
  global $vnT, $func, $DB, $conf;
	switch ($act){
		case "category" : $text = $vnT->link_root.$title.".html";	 break ;
		case "detail" :  $text = $vnT->link_root.$title.".html";	 break ;
		default : $text = LINK_MOD."/".$act."/".$id."/".$func->make_url($title).".html";	 break ;
	}
  $text .= ($extra) ? "/" . $extra : "";
  return $text;
}
function load_html ($file, $data){
  global $vnT, $input;
  $html = new XiTemplate(DIR_MODULE . "/news/html/" . $file . ".tpl");
  $html->assign('DIR_MOD', DIR_MOD);
  $html->assign('LANG', $vnT->lang);
  $html->assign('INPUT', $input);
  $html->assign('CONF', $vnT->conf);
  $html->assign('DIR_IMAGE', $vnT->dir_images);
  $html->assign("data", $data);
  $html->parse($file);
  return $html->text($file);
}
function get_navation ($cat_id,$ext=''){
  global $DB,$conf,$func,$vnT,$input;
	$output.= '<li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem" class="breadcrumb-item"><a href="/" ><span itemprop="name">Home</span></a></li> ';
  $output.= '<li  class="breadcrumb-item"><a itemscope="" itemtype="http://schema.org/Thing" itemprop="item" href="'.LINK_MOD.'.html"><span itemprop="name">'.$vnT->lang['news']['news'].'</span></a></li> ';
	if($cat_id) {
		$res = $DB->query("SELECT cat_code,cat_id FROM news_category where cat_id in (".$cat_id.")  ");
		if ($cat=$DB->fetch_row($res)) {
			$cat_code = $cat['cat_code'];
		}
		$tmp = explode("_",$cat_code);
		$num = count($tmp) ;
		for ($i=0;$i<count($tmp);$i++){
			$res= $DB->query("SELECT cat_id,cat_name,friendly_url 
                        FROM news_category_desc 
                        WHERE cat_id=".$tmp[$i]." and lang='{$vnT->lang_name}' ");
			if ($r = $DB->fetch_row($res)){
				$link = create_link("category",$tmp[$i],$r['friendly_url']);
        $output.='<li   class="breadcrumb-item"><a itemscope="" itemtype="http://schema.org/Thing" itemprop="item" href="'.$link.'"><span itemprop="name">'.$func->HTML($r['cat_name']).'</span></a></li>';
			}
		}
	}
  if($ext)
    $output .= '<li  class="breadcrumb-item"><span itemprop="name">'.$ext.'</span></li> ';
	return $output;
}
function get_nav_sub ($cid){
  global $DB,$conf,$func,$vnT,$input;
 	$text = '';
 	$res= $vnT->DB->query("select n.cat_id,nd.cat_name,nd.friendly_url from news_category n , news_category_desc nd where n.cat_id=nd.cat_id AND nd.lang='$vnT->lang_name' AND display=1 AND n.parentid=".$cid." ORDER BY cat_order DESC , date_post DESC ");
	if($num = $vnT->DB->num_rows($res))
	{
		$i=0; $text = '<ul class="navation_sub"><div class="arr">&nbsp;</div>' ;
		while ($r = $vnT->DB->fetch_row($res))
		{
			$i++;
			$link = create_link("category",$r['cat_id'],$r['friendly_url']);
			$last = ($i==$num) ? "class='last'" : "";
			$text.='<li '.$last.' ><a href="'.$link.'">'.$vnT->func->HTML($r['cat_name']).'</a></li>' ;
		}
		$text .= '</ul>';
	}
	return $text;
}
function get_where_cat ($cat_id){
  global $input, $conf, $vnT;
  $text = " AND FIND_IN_SET('$cat_id',cat_list)<>0 ";
  return $text;
}
function get_cat_name ($cat_id){
  global $func, $conf, $DB, $vnT;
  $cat_name = $vnT->lang['news']['category'];
  $query = $DB->query("SELECT * FROM news_category_desc WHERE cat_id ={$cat_id} AND lang='{$vnT->lang_name}' ");
  if ($row = $DB->fetch_row($query)) {
    $cat_name = $func->HTML($row['cat_name']);
  }
  return $cat_name;
}
function get_friendly_url ($cat_id, $lang){
  global $vnT, $func, $DB, $conf;
	$out="";
  $result = $DB->query("select friendly_url from news_category  where cat_id=$cat_id and lang='$lang'");
  if ($row = $DB->fetch_row($result)) {
    $out = $func->HTML($row['friendly_url']);
  }
  return $out;
}
function get_source ($sid){
  global $func, $conf, $DB, $vnT;
  $query = $DB->query("SELECT * FROM news_source WHERE sid ={$sid}  ");
  if ($row = $DB->fetch_row($query)) {
    $out = $func->HTML($row['s_content']);
  } else
    $out = "";
  return $out;
}
function get_iconnew ($date_post){
  global $vnT;
  if (@date("d/m/Y", $date_post) == @date("d/m/Y")) {
    $out = "&nbsp;<img src=\"" . MOD_DIR_IMAGE . "/icon_new.gif\" align='absmiddle' />";
  } else {
    $out = "";
  }
  return $out;
}
function get_datePost ($date_post){
  global $vnT;
  $day_array = array(
    $vnT->lang['news']['sunday'] ,
    $vnT->lang['news']['monday'] ,
    $vnT->lang['news']['tuesday'] ,
    $vnT->lang['news']['wednesday'] ,
    $vnT->lang['news']['thursday'] ,
    $vnT->lang['news']['friday'] ,
    $vnT->lang['news']['saturday']);
  $out = $day_array[date("w", $date_post)] . ", " . date("d/m/Y, H:i", $date_post);
  return $out;
}
function get_src_pic_news ($picture, $w = ""){
  global $vnT, $func;
  $out = "";
	$linkhinh = "vnt_upload/news/" . $picture;
	$linkhinh = str_replace("//", "/", $linkhinh);
	$dir = substr($linkhinh, 0, strrpos($linkhinh, "/"));
	$pic_name = substr($linkhinh, strrpos($linkhinh, "/") + 1);
	if($w){
		$file_thumbs = $dir . "/thumbs/{$w}_" . substr($linkhinh, strrpos($linkhinh, "/") + 1);
		$linkhinhthumbs = $vnT->conf['rootpath'] . $file_thumbs;
		if (! file_exists($linkhinhthumbs)) {
			if (@is_dir($vnT->conf['rootpath'] . $dir . "/thumbs")) {
				@chmod($vnT->conf['rootpath'] . $dir . "/thumbs", 0777);
			} else {
				@mkdir($vnT->conf['rootpath'] . $dir . "/thumbs", 0777);
				@chmod($vnT->conf['rootpath'] . $dir . "/thumbs", 0777);
			}
			// thum hinh
			$vnT->func->thum($vnT->conf['rootpath'] . $linkhinh, $linkhinhthumbs, $w);
		}
		$src = ROOT_URI . $file_thumbs;
	} else {
		$src = MOD_DIR_UPLOAD . "/" . $picture;
	}

  return $src;
}
function get_pic_news ($picture, $w = "" ,$thumb=0, $ext = ""){
  global $vnT, $func;
  $out = "";
  $ext = "";
  $w_thumb = ($vnT->setting['imgthumb_width']) ? $vnT->setting['imgthumb_width'] : 100;
  $linkhinh = "vnt_upload/news/" . $picture;
  $linkhinh = str_replace("//", "/", $linkhinh);
  $dir = substr($linkhinh, 0, strrpos($linkhinh, "/"));
  $pic_name = substr($linkhinh, strrpos($linkhinh, "/") + 1);
  if ($w) {
    if ($thumb && file_exists($vnT->conf['rootpath'] . $linkhinh)) {
      $file_thumbs = $dir . "/thumbs/{$w}_" . substr($linkhinh, strrpos($linkhinh, "/") + 1);
      $linkhinhthumbs = $vnT->conf['rootpath'] . $file_thumbs;
      if (! file_exists($linkhinhthumbs)) {
        if (@is_dir($vnT->conf['rootpath'] . $dir . "/thumbs")) {
          @chmod($vnT->conf['rootpath'] . $dir . "/thumbs", 0777);
        } else {
          @mkdir($vnT->conf['rootpath'] . $dir . "/thumbs", 0777);
          @chmod($vnT->conf['rootpath'] . $dir . "/thumbs", 0777);
        }
        // thum hinh
        $vnT->func->thum($vnT->conf['rootpath'] . $linkhinh, $linkhinhthumbs, $w);
      }
      $src = ROOT_URI . $file_thumbs;
    } else {
      if ($w < $w_thumb)
        $ext = " width='$w' ";
      $src = ROOT_URI . $dir . "/thumbs/" . $pic_name;
    }
  } else {
    $src = MOD_DIR_UPLOAD . "/" . $picture;
  }
  $alt = substr($pic_name, 0, strrpos($pic_name, "."));
  $out = "<img  src=\"{$src}\" alt=\"{$alt}\"  {$ext} >";
  return $out;
}
function get_pic_news_detail ($picture, $pic_des, $w = 200){
  global $vnT;
  $out = "";
  if (! empty($picture)) {
    $link_image = $vnT->conf['rootpath']. "vnt_upload/news/" . $picture;
    $lastx = strrpos($picture, "/");
    $fext = strtolower(substr($picture, strrpos($picture, ".") + 1));
    switch ($fext) {
      case 'jpg':
        $img = @imagecreatefromjpeg($link_image);
      break;
      case 'gif':
        $img = @imagecreatefromgif($link_image);
      break;
      case 'png':
        $img = @imagecreatefrompng($link_image);
      break;
    }
    $img_w = @imagesx($img);
    if ($img_w > $w)  $img_w = $w;
    $src =  MOD_DIR_UPLOAD . "/" . $picture;
    $class = (! empty($vnT->setting['img_align'])) ? "imgDetail_" . $vnT->setting['img_align'] : "imgDetail_left";
    $out = "<div class='{$class}'  style='width:{$img_w}px;' ><a href=\"{$src}\" class='thickbox' title='Zoom' ><img src=\"{$src}\" width={$img_w} alt=\"{$pic_des}\" title=\"{$pic_des}\" /></a>";
		if($pic_des)	$out .= "<p class='fDespic'>{$pic_des}</p>";
		$out .= "</div>";
  }
  return $out;
}
function get_banner_news ($cat_id, $n = ""){
  global $DB, $input, $vnT;
  $html_advertise = "";
  $sql = "select * from news_advertise where cat_id=$cat_id ORDER BY l_order ";
  if ($n) $sql .= " LIMIT 0,$n ";

  $result = $DB->query($sql);
  while ($row = $DB->fetch_row($result)) {
    $src = MOD_DIR_UPLOAD . "/advertise/" . $row['picture'];
    $l_link = (! strstr($row['link'], "http://")) ? $vnT->link_root .$row['link'] : $row['link'] ;

    if ($row["type"] == "swf") {
      $html_advertise .= "<p class='advertise'><object classid='clsid:D27CDB6E-AE6D-11cf-96B8-444553540000' codebase='http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=7,0,19,0' width=" . $row["width"] . " height=" . $row["height"] . "><param name='movie' value=" . $src . " /><param name='quality' value='high' /><param value='transparent' name='wmode'/><embed src=" . $src . " quality='high' pluginspage='http://www.macromedia.com/go/getflashplayer' type='application/x-shockwave-flash' width=" . $row["width"] . " height=" . $row["height"] . " wmode='transparent' ></embed></object><p>";
    } else {
     	$target = ($row['target']) ? $row['target'] : "_blank";
      $html_advertise .= "<p class='advertise'><a onmousedown=\"return rwt(this,'advertise',".$row['l_id'].")\" href='{$l_link}'   target='{$target}'> <img  src='{$src}' width='" . $row['width'] . "' alt='{$title}' /></a></p>";
    }
  }
  return $html_advertise;
}
function get_total_news ($cat_id){
  global $input, $conf, $vnT;
  $table_news = "news";
  $out = "";
  $where = " where display=1 " . get_where_cat($cat_id) . "   ";
  $sqlnews = "select * from $table_news where cat_id=$cat_id  and display=1 ";
  $result = $vnT->DB->query($sqlnews);
  $out = $vnT->DB->num_rows($result);
  return $out;
}
function List_Cat_News ($did = -1, $ext=""){
  global $func, $DB, $conf, $vnT;

  $text = "<select size=1  name=\"cat_id\" id=\"cat_id\"  class=\"select\" {$ext} >";
  $text .= "<option value=\"\">".$vnT->lang['news']['select_category']."</option>";
  $query = $DB->query("SELECT n.*, nd.cat_name FROM news_category n,news_category_desc nd 
					WHERE n.cat_id=nd.cat_id
					AND nd.lang='$vnT->lang_name' 
					AND n.parentid=0 
					ORDER BY cat_order DESC , date_post DESC");
  while ($cat = $DB->fetch_row($query)) {
    $cat_name = $vnT->func->HTML($cat['cat_name']);
    if ($cat['cat_id'] == $did)
      $text .= "<option value=\"{$cat['cat_id']}\" selected>{$cat_name}</option>";
    else
      $text .= "<option value=\"{$cat['cat_id']}\" >{$cat_name}</option>";
    $n = 1;
    $text .= List_Sub_News($cat['cat_id'], $n, $did);
  }
  $text .= "</select>";
  return $text;
}
function List_Sub_News ($cid, $n, $did = -1){
  global $vnT, $func, $DB, $conf;
  $output = "";
  $k = $n;
  $query = $DB->query("SELECT n.*, nd.cat_name FROM news_category n,news_category_desc nd 
					WHERE n.cat_id=nd.cat_id
					AND nd.lang='$vnT->lang_name' 
					AND n.parentid=$cid 
					ORDER BY cat_order DESC , date_post DESC");
  while ($cat = $DB->fetch_row($query)) {
    $cat_name = $vnT->func->HTML($cat['cat_name']);
    if ($cat['cat_id'] == $did) {
      $output .= "<option value=\"{$cat['cat_id']}\" selected>";
      for ($i = 0; $i < $k; $i ++)
        $output .= "|-- ";
      $output .= " {$cat_name}</option>";
    } else {
      $output .= "<option value=\"{$cat['cat_id']}\" >";
      for ($i = 0; $i < $k; $i ++)
        $output .= "|-- ";
      $output .= " {$cat_name}</option>";
    }
    $n = $k + 1;
    $output .= List_Sub_News($cat['cat_id'], $n, $did);
  }
  return $output;
}
function Check_Sub_News ($cid){
  global $DB;
  $query = $DB->query("SELECT * FROM news_category WHERE parentid ={$cid} ");
  if ($scat = $DB->fetch_row($query))
    return 1;
  else
    return 0;
}
function get_relate_news ($list_id){
  global $DB, $func, $conf, $vnT;
	$textout = "";
	$result= $vnT->DB->query("SELECT newsid,title,friendly_url FROM news 
                            WHERE newsid in (".$list_id.") ORDER BY display_order DESC,  date_post DESC ");
	if($num = $vnT->DB->num_rows($result)){
		$i=0;
		$textout = "> ";
		while($row = $vnT->DB->fetch_row($result)){
			$i++;
			$link = create_link("detail",$row['newsid'],$row['friendly_url']) ;
			$textout .= "<a href='".$link."' >".$func->HTML($row['title'])."</a>" ;
			if($i<$num) $textout .= "/ ";
		}
	}
  return $textout;
}
function html_item ($row){
  global $vnT, $DB, $func, $input, $conf;
  $w = ($vnT->setting['imgthumb_width']) ? $vnT->setting['imgthumb_width'] : 100 ;
  $data['link'] = create_link("detail", $row['newsid'], $row['friendly_url']);
  $data['ref'] = ($row['ref']==0) ? ' ref="nofollow" ' : "";
  // $picture = ($row['picture']) ? "news/".$row['picture'] : "news/".$vnT->setting['pic_nophoto'];
  //$data['src'] = $vnT->func->get_src_modules($picture, $w ,'',1,'1.5:1');
  // $data['pic'] = $vnT->func->get_pic_modules($picture, $w, 0, " alt='".$row['p_name']."' ",1,0,array("fix_width"=>1));
  $picture = ($row['picture']) ? "news/".$row['picture'] : "news/".$vnT->setting['pic_nophoto'];
  $src = $vnT->func->get_src_modules($picture, $w ,'',1,'1.5:1');
   $data['pic'] = "<img src='".$src."' />";

  $data['title'] = $func->HTML($row['title']);
  $data['date_post'] = @date('d/m/Y', $row['date_post']);
  $data['date_post_meta'] = @date('Y-m-d', $row['date_post']);
  $data['date_update_meta'] = @date('Y-m-d', $row['date_update']);
  $data['short'] = $vnT->func->cut_string($vnT->func->check_html($row['short'],'nohtml'),280,1);
  //$data['cat_name'] = $vnT->func->HTML($vnT->setting['cat_name'][$row['cat_id']]);
  $text = load_html('item_news',$data);
  return $text;
}
function row_news ($where, $start, $n, $num_row=4, $view=2 ,$is_home=0){
  global $vnT, $DB, $func, $input, $conf;
  $text = "";
  $sqlnews = "SELECT * FROM news n, news_desc nd
							WHERE n.newsid=nd.newsid AND lang='$vnT->lang_name'
							AND display=1 $where  
							ORDER BY display_order DESC, date_post DESC LIMIT $start,$n";
  $result = $vnT->DB->query($sqlnews);
  if ($num = $vnT->DB->num_rows($result)){
    while ($row = $DB->fetch_row($result)){
      $text .= html_item($row);
    }
  }else{
    $text = "<div class='noItem'>{$vnT->lang['news']['no_have_news']}</div>";
  }
  return $text;
}
function row_news2 ($where, $start, $n, $num_row=4, $view=2 ,$is_home=0){
  global $vnT, $DB, $func, $input, $conf;
  $text = "";
  $sqlnews = "SELECT * FROM news n, news_desc nd
              WHERE n.newsid=nd.newsid AND lang='$vnT->lang_name'
              AND display=1 $where  
              ORDER BY display_order DESC, date_post DESC LIMIT $start,$n";
  $result = $vnT->DB->query($sqlnews);
  if ($num = $vnT->DB->num_rows($result)){
    while ($row = $DB->fetch_row($result)){
      $link = create_link("detail", $row['newsid'], $row['friendly_url']);
      $picture = ($row['picture']) ? "news/".$row['picture'] : "news/".$vnT->setting['pic_nophoto'];
      $src = $vnT->func->get_src_modules($picture, $w ,'',1,'1.5:1');
      $text .= '<div class="sale-item">
                  <div class="row">
                    <div class="col-lg-4 col-12"><img class="w-100 img-sale-item" src="'. $src .'" alt=""></div>
                    <div class="col-lg-8 col-12">
                      <h5 class="name-item-sale fs-12"><a href="'.$link.'"><b>'.$row['title'].'</b></a></h5>
                      <p class="fs-10"> ' .@date("d/m/Y H:i", $row['date_post']).'</p>
                      <p class="fs-10">'.$row['short'].'</p>
                    </div>
                  </div>
                </div>';
    }
  }else{
    $text = "<div class='noItem'>{$vnT->lang['news']['no_have_news']}</div>";
  }
  return $text;
}
function news_other ($where , $start, $n_other){
  global $vnT, $DB, $func, $input;
  $text = "";
  $sql = "SELECT * FROM news n, news_desc nd
					WHERE n.newsid=nd.newsid AND lang='$vnT->lang_name' AND display=1 $where 
					ORDER BY display_order DESC, date_post DESC  LIMIT $start,$n_other";
  $result = $vnT->DB->query($sql);
  if ($vnT->DB->num_rows($result)) {
    $text .= '<section class="news-related bg-gray">
    <div class="container container--850">
      <div class="related-items">';
    while ($row = $vnT->DB->fetch_row($result)) {
      $link_other = create_link("detail", $row['newsid'], $row['friendly_url']);
      $picture = ($row['picture']) ? "news/".$row['picture'] : "news/".$vnT->setting['pic_nophoto'];
      $src = $vnT->func->get_src_modules($picture, $w ,'',1,'1.5:1');
$cat_name = $vnT->setting['cat_name'][$row['cat_id']];
      $text .= '<a href="'.$link_other.'">
          <div class="related-item"><img class="w-100 img-sale-item" src="'.$src.'">
            <p class="related-tag text-uppercase">'.$cat_name .'</p>
            <h5>'.$func->HTML($row['title']).'</h5>
          </div></a>';
    }
    $text .= '</div> </div> </section>';


  }
  return $text;


}
/*--------------------------- html_row ----------------*/
function html_row ($data){
global $input, $conf, $vnT;
return <<<EOF
{$data['pic']}
<h3>{$data['title']}</h3>
{$data['date_post']}
<div class="short">
{$data['short']}
</div> 
EOF;
}
function ListDay ($did){
  global $input, $conf, $vnT;
  $text = '<select name="day"  style="width:50px; text-align:center;">';
  // $text .= "<option value=\"0\" selected > --- </option>";
  for ($i = 1; $i <= 31; $i ++) {
    if ($i < 10)
      $value = "0" . $i;
    else
      $value = $i;
    if ($value == $did) {
      $text .= "<option value=\"{$value}\" selected >" . $value . "</option>";
    } else {
      $text .= "<option value=\"{$value}\" >" . $value . "</option>";
    }
  }
  $text .= '</select>';
  return $text;
}
function ListMonth ($did){
  global $input, $conf, $vnT;
  $text = '<select name="month"  style="width:50px; text-align:center;">';
  for ($i = 1; $i <= 12; $i ++) {
    if ($i < 10)
      $value = "0" . $i;
    else
      $value = $i;
    if ($value == $did) {
      $text .= "<option value=\"{$value}\" selected >" . $value . "</option>";
    } else {
      $text .= "<option value=\"{$value}\" >" . $value . "</option>";
    }
  }
  $text .= '</select>';
  return $text;
}
function ListYear ($did){
  global $input, $conf, $vnT;
  $text = '<select name="year"  style="width:70px; text-align:center;">';
  $yearBegin = date('Y') - 10;
  for ($i = $yearBegin; $i <= date('Y'); $i ++) {
    if ($i < 10)
      $value = "0" . $i;
    else
      $value = $i;
    if ($value == $did) {
      $text .= "<option value=\"{$value}\" selected >" . $value . "</option>";
    } else {
      $text .= "<option value=\"{$value}\" >" . $value . "</option>";
    }
  }
  $text .= '</select>';
  return $text;
}
function get_keyword ($keyword){
  global $input, $conf, $vnT;
  $lower = '
	a|b|c|d|e|f|g|h|i|j|k|l|m|n|o|p|q|r|s|t|u|v|w|x|y|z
	|á|à|ả|ã|ạ|ă|ắ|ặ|ằ|ẳ|ẵ|â|ấ|ầ|ẩ|ẫ|ậ
	|đ
	|é|è|ẻ|ẽ|ẹ|ê|ế|ề|ể|ễ|ệ
	|í|ì|ỉ|ĩ|ị
	|ó|ò|ỏ|õ|ọ|ô|ố|ồ|ổ|ỗ|ộ|ơ|ớ|ờ|ở|ỡ|ợ
	|ú|ù|ủ|ũ|ụ|ư|ứ|ừ|ử|ữ|ự
	|ý|ỳ|ỷ|ỹ|ỵ';
  $upper = '
	A|B|C|D|E|F|G|H|I|J|K|L|M|N|O|P|Q|R|S|T|U|V|W|X|Y|Z
	|Á|À|Ả|Ã|Ạ|Ă|Ắ|Ặ|Ằ|Ẳ|Ẵ|Â|Ấ|Ầ|Ẩ|Ẫ|Ậ
	|Đ
	|É|È|Ẻ|Ẽ|Ẹ|Ê|Ế|Ề|Ể|Ễ|Ệ
	|Í|Ì|Ỉ|Ĩ|Ị
	|Ó|Ò|Ỏ|Õ|Ọ|Ô|Ố|Ồ|Ổ|Ỗ|Ộ|Ơ|Ớ|Ờ|Ở|Ỡ|Ợ
	|Ú|Ù|Ủ|Ũ|Ụ|Ư|Ứ|Ừ|Ử|Ữ|Ự
	|Ý|Ỳ|Ỷ|Ỹ|Ỵ';
  $arrayUpper = explode('|', preg_replace("/\n|\t|\r/", "", $upper));
  $arrayLower = explode('|', preg_replace("/\n|\t|\r/", "", $lower));
  $text = str_replace($arrayUpper, $arrayLower, $keyword);
  return $text;
}
function List_Accurate ($did){
  global $input, $vnT;
  $out = "";
  if ($did == 0)
  {
    $out .= "<input name=\"r_AC\" type=\"radio\" value=\"0\" checked align='absmiddle' />&nbsp;" . $vnT->lang['news']['no_accurate'] . " &nbsp;
		<input name=\"r_AC\" type=\"radio\" value=\"1\" align='absmiddle' />&nbsp;" . $vnT->lang['news']['accurate'];
  } else
  {
    $out .= "<input name=\"r_AC\" type=\"radio\" value=\"0\" align='absmiddle' />&nbsp;" . $vnT->lang['news']['no_accurate'] . " &nbsp;
		<input name=\"r_AC\" type=\"radio\" value=\"1\" checked align='absmiddle' />&nbsp;" . $vnT->lang['news']['accurate'];
  }
  return $out;
}
function get_str_where ($keyword, $data = ""){
  global $input, $vnT;
  $out = array();
  $where = " ";
  $ext = "";
  $cat_id = $data['cat_id'];
  $r_AC = $data['r_AC'];

  $title = "title";
	$short = "short";
	if ($cat_id){
		$where .= get_where_cat($cat_id);
		$ext.="&cat_id=".$cat_id;
	}
  if ($keyword){
		$qu_find = array(
			'%20',
			'  ',	// double spaces to single spaces
			'+ ',	// replace '+ ' with '+'
			'- ',	// replace '- ' with '-'
			'or ',	// remove 'OR '
			'and '	// replace 'AND ' with '+'
		);
		$qu_replace = array(
			' ',
			' ',	// double spaces to single spaces
			'+',	// replace '+ ' with '+'
			'-',	// replace '- ' with '-'
			'',		// remove 'OR '
			'+' 	// replace 'AND ' with '+'
		);
		$str_keyword = str_replace($qu_find, $qu_replace, $keyword);
		$ext.="&keyword=". rawurlencode($str_keyword);
		$where.=" and ( $title like '%{$str_keyword}%' or short like '%{$str_keyword}%' ) ";
		$result_keyword = $str_keyword;
	}
  if($data['date_begin'] || $data['date_end'] ){
		$tmp1 = @explode("/", $data['date_begin']);
    $date_begin = @mktime(0, 0, 0, $tmp1[1], $tmp1[0], $tmp1[2]);

		$tmp2 = @explode("/", $data['date_end']);
    $date_end = @mktime(23, 59, 59, $tmp2[1], $tmp2[0], $tmp2[2]);

		$where.=" AND (date_post BETWEEN {$date_begin} AND {$date_end} ) ";
		$ext.="&date_begin=".$data['date_begin']."&date_end=".$data['date_end'];
	}
  $out['result_keyword'] = $result_keyword;
  $out['where'] = $where;
  $out['ext'] = $ext;
  return $out;
}
function row_search ($data){
  global $vnT, $DB, $func, $input, $conf;
  $text = "";
  $link = create_link("detail", $data['newsid'], $data['friendly_url']);
  $title = $data["title"];
  $short = $data["short"];
  $tmp = explode(",", $data['result_keyword']);
  if (is_array($tmp)){
    for ($j = 0; $j < count($tmp); $j ++){
      $k = "{#}" . trim($tmp[$j]) . "{/#}";
      switch ($data['w_search']){
        case 1:
            if ($tmp[$j] != "<keyword>" && $tmp[$j] != "</keyword>")  {
              $title = str_replace(trim($tmp[$j]), trim($k), $title);
            }
        break;
        case 2:
            if ($tmp[$j] != "<keyword>" && $tmp[$j] != "</keyword>") {
              $short = str_replace(trim($tmp[$j]), trim($k), $short);
            }
        break;
        default:
            if ($tmp[$j] != "<keyword>" && $tmp[$j] != "</keyword>")  {
              $title = str_replace(trim($tmp[$j]), trim($k), $title);
              $short = str_replace(trim($tmp[$j]), trim($k), $short);
            }
        break;
      }

    }
    $qu_find = array(
      '{#}' , '{/#}'
    );
    $qu_replace = array(
      '<font class="font_keyword">' , '</font>'
    );
    //print "title = ".$title."<br>";
    $title = str_replace($qu_find, $qu_replace, $title);
    $short = str_replace($qu_find, $qu_replace, $short);
  }

	if ($vnT->setting['imgthumb_align'] == "right") {
		$class_img = "class='img_right'";
	} else {
		$class_img = "class='img_left'";
	}


	$w = ($vnT->setting['imgthumb_width']) ? $vnT->setting['imgthumb_width'] : 100 ;

	if ($data['picture'] || $vnT->setting['nophoto'])
	{
		$picture = ($data['picture']) ? "news/".$data['picture'] : "news/nophoto/nophoto.jpg";
		$picturedes = ($data["picturedes"]) ? $data["picturedes"] : $data["title"];
		$pic = $vnT->func->get_pic_modules($picture, $w, $h, " alt='".$picturedes."' title='".$picturedes."' ", 1, 0, array("fix_min"=>1));
		$pic = '<div '.$class_img.' ><table width="'.$w.'" border="0" cellspacing="0" cellpadding="0"><tr><td height="'.$h.'" align="center"><a href="' . $link . '" '.$ref.' >' . $pic . '</a></td> </tr></table></div>';
	} else {
		$pic = "";
	}

	$data['pic'] = $pic;


  $data['title'] = "<a href=\"{$link}\">" . $title . "</a> ". get_iconnew($data['date_post']);
  $data['short'] = $short;


	 $data['date_post'] = "<div class='date_news'><span>".@date("H:i", $data['date_post'])."</span> | " . @date("d/m/Y", $row['date_post']) . "</div>";

	if($data['relate_news'])
	{
		$data['short'] = $data['short'] . '<div class="relate_news" >'.get_relate_news($data['relate_news']).'</div>' ;
	}
	$text = html_row($data);
	return $text ;
}
function tranferHTML($t="") {
	$t = str_replace( "&lt;", "<"  , $t );
	$t = str_replace( "&gt;", ">"  , $t );
	$t = str_replace( '&quot;', '"', $t );
	$t = str_replace( "&#039;", "'", $t );
	return $t;
}
function box_sidebar (){
	global $vnT, $input;
  //$textout = $vnT->lib->get_banner_sidebar('sidebar');
	$textout.= focus_news();
 	return $textout;
}
function focus_news (){
  global $vnT,$DB;
  $textout = $text = "";
  $n = ($vnT->setting['other_detail']) ? $vnT->setting['other_detail'] : 10;
  $where = ' ORDER BY viewnum DESC, date_post DESC';
  $result=$DB->query("SELECT * FROM news n , news_desc nd
                      WHERE n.newsid=nd.newsid AND lang='$vnT->lang_name'AND display=1 {$where} LIMIT 0,$n");
  if ($num = $DB->num_rows($result)) {
    $w = ($vnT->setting['imgblock_width']) ? $vnT->setting['imgblock_width'] : 370;
    $text .= '<div class="box_news_sidebar"><div class="bn_title"><h3>'.$vnT->lang['news']['best_view'].'</h3></div><div class="bn_content">';
    while ($row = $DB->fetch_row($result)) {
      $link = create_link("detail", $row['newsid'], $row['friendly_url'] );
      $title = $vnT->func->HTML($row['title']);
      $picture = ($row['picture']) ? "news/".$row['picture'] : "news/".$vnT->setting['pic_nophoto'];
      $src = $vnT->func->get_src_modules($picture, $w ,'',1,'1.5:1');
      $text .= '<div class="w_item"><div class="item">
                  <div class="i-image">
                    <a href="'.$link.'" title="'.$title.'"><img src="'.$src.'" alt="'.$title.'" /></a>
                  </div>
                  <div class="i-title"><a href="'.$link.'" title="'.$title.'">'.$title.'</a></div>
                </div></div>';
    }
    $text .= '</div></div>';
  }
  return $text;
}
function box_category(){
  global $func, $DB, $conf, $vnT, $input;
  $text = '';
  $sql = "SELECT * FROM news_category n, news_category_desc nd
          WHERE n.cat_id = nd.cat_id AND lang = '$vnT->lang_name' AND parentid = 0 AND display = 1
          ORDER BY cat_order ASC, date_post DESC";
  $result = $DB->query($sql);
  if ($num = $DB->num_rows($result)) {
    $i = 0;
    $text = '';
    $active_main = (!$input['catID']) ? 'class="current"' : '';
    while ($row = $DB->fetch_row($result)) {
      $i ++;
      $active = ($row['cat_id']==$input['catID']) ?  "active" : "" ;
      $link = create_link("category", $row['cat_id'], $row['friendly_url']);
      $cat_name = $vnT->func->HTML($row['cat_name']);

      $text .= '<div class="col-6 col-lg-3 p-0"><a href="'.$link.'">
                <p class="fs-16 font-weight-medium menu-sub-news-title '.$active.'">'.$cat_name.'</p></a></div>';
    }
  }
  return $text;
}
?>