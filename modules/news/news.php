<?php
/*================================================================================*\
|| 							Name code : news.php	 		 																		  # ||
||  				Copyright © 2007 by Thai Son - CMS vnTRUST                					# ||
\*================================================================================*/
/**
 * @version : 1.0
 * @date upgrade : 11/12/2007 by Thai Son
 **/
if (! defined('IN_vnT')) {
  die('Access denied');
}
$nts = new sMain();

class sMain
{
  var $output = "";
  var $skin = "";
  var $module = "news";
  var $action = "news";

  function sMain (){
    global $vnT, $input;
    include ("function_" . $this->module . ".php");
    loadSetting();
    $this->skin = new XiTemplate(DIR_MODULE . "/" . $this->module . "/html/" . $this->module . ".tpl");
    $this->skin->assign('DIR_MOD', DIR_MOD);
    $this->skin->assign('LANG', $vnT->lang);
    $this->skin->assign('INPUT', $input);
    $this->skin->assign('CONF', $vnT->conf);
    $this->skin->assign('DIR_IMAGE', $vnT->dir_images);
    $this->skin->assign('LINK_MOD', LINK_MOD);
    $vnT->html->addStyleSheet(DIR_MOD . "/css/" . $this->module . ".css");
    $vnT->html->addScript(DIR_MOD . "/js/" . $this->module . ".js");
		//active menu
		$vnT->setting['menu_active'] = $this->module;
		//SEO		
		if ($vnT->setting['metakey'])	$vnT->conf['meta_keyword'] = $vnT->setting['metakey'];
		if ($vnT->setting['metadesc'])	$vnT->conf['meta_description'] = $vnT->setting['metadesc'];		
		if ($vnT->setting['friendly_title']){	 
			$vnT->conf['indextitle'] = $vnT->setting['friendly_title'] ;	
		} 
		$link_seo = ($vnT->muti_lang) ? ROOT_URL.$vnT->lang_name."/" : ROOT_URL ;
		$link_seo .= $vnT->setting['seo_name'][$vnT->lang_name]['news'].".html";			
		$vnT->conf['meta_extra'] .= "\n".'<link rel="canonical" href="'.$link_seo.'" />';
		$vnT->conf['meta_extra'] .= "\n". '<link rel="alternate" media="handheld" href="'.$link_seo.'"/>';
    $data['main'] = $this->do_List($data);
		$navation = get_navation($cat_id);
		$data['navation'] = $vnT->lib->box_navation($navation);
    $vnT->setting['banner'] = $vnT->lib->get_child_slide('child');
    $data['fixed_sidebar'] = $vnT->lib->fixed_sidebar();
    $data['link_videls']  = $vnT->link_root.$vnT->setting['seo_name'][$vnT->lang_name]['dealer'].'.html';
    $this->skin->assign("data", $data);
    $this->skin->parse("modules");
    $vnT->output .= $this->skin->text("modules");
  }
  function do_List ($info){
    global $vnT, $input, $DB, $func, $conf;
    $vnT->html->addStyleSheet($vnT->dir_js ."/datepicker/datepicker.css");
    $vnT->html->addScript($vnT->dir_js ."/datepicker/datepicker.js");
    $text = "";
    $where = "";
    $date_post = ($input['date_post']) ? $input['date_post'] : '';
    if($date_post){
      $arr = explode('/', $date_post);
      $time_begin = mktime(0,0,0,$arr[1],$arr[0],$arr[2]);
      $time_end = mktime(23,59,59,$arr[1],$arr[0],$arr[2]);
      $where .= " AND date_post BETWEEN $time_begin AND $time_end ";
      $ext_pag .= "&date_post=".$date_post;
    }
    $p = ((int) $input['p']) ? (int) $input['p'] : 1;
    $n = (! empty($vnT->setting['n_cat'])) ? $vnT->setting['n_cat'] : 10;
 		$num_row = 2;
		$view = 2;
		$sql_num = "SELECT n.newsid FROM news n, news_desc nd
								WHERE n.newsid=nd.newsid AND lang='$vnT->lang_name' AND display=1 $where ";
                //die($sql_num);
		$res_num = $DB->query($sql_num);
    $totals = $DB->num_rows($res_num);
    $num_pages = ceil($totals / $n);
    if ($p > $num_pages) $p = $num_pages;
    if ($p < 1) $p = 1;
    $start = ($p - 1) * $n;
    if ($num_pages > 1) {
      $root_link = LINK_MOD.".html" ;
      $nav = "<div class='pagination'>".$vnT->func->htaccess_paginate($root_link,$totals,$n,$ext_pag,$p)."</div>";
    }
		$data['list_news'] = row_news($where, $start, $n ,$num_row,$view);
    $data['nav'] = $nav;
    $data['box_focus'] = $this->focusNews();
    $data['box_category'] = box_category();
    $this->skin->assign("data", $data);
    $this->skin->parse("html_list");
    return $this->skin->text("html_list");
   
    
  }
  function focusNews (){
    global $DB, $conf, $func, $vnT, $input;
    $list = "";
    $n = ($vnT->setting['n_focus_cat']) ? $vnT->setting['n_focus_cat'] : 4;
    $where =  " AND focus_main=1 ";
    $sql = "SELECT * FROM news n, news_desc nd
            WHERE n.newsid=nd.newsid AND lang='$vnT->lang_name' AND display=1 $where 
            order by focus_order DESC, date_post desc LIMIT 0,".$n;
    $result = $DB->query($sql);
    if($num = $vnT->DB->num_rows($result)){
      $i=0;
      $w = ($vnT->setting['imgfocus_width']) ? $vnT->setting['imgfocus_width'] : 570; 
      while ($row = $vnT->DB->fetch_row($result)){
        $i++;
        $news_id = $row['news_id'];
        $data['link'] = create_link("detail", $row['newsid'], $row['friendly_url'],$ext_pag);
        $picture = ($row['picture']) ? "news/".$row['picture'] : "news/".$vnT->setting['pic_nophoto'];
        $data['src'] = $vnT->func->get_src_modules($picture, $w ,'',1,'1.5:1');
        $data['title'] = $func->HTML($row['title']);
        $data['date_post'] = @date('d/m/Y - H:i', $row['date_post']);
        $data['date_post_meta'] = @date('Y-m-d', $row['date_post']);
        $data['date_update_meta'] = @date('Y-m-d', $row['date_update']);
        $data['short'] = $vnT->func->cut_string($vnT->func->check_html($row['short'],'nohtml'),300,1);
        $data['cat_name'] = $vnT->setting['cat_name'][$row['cat_id']];
        $this->skin->reset("item_focus");
        $this->skin->assign("data", $data);
        $this->skin->parse("item_focus");
        $list .= $this->skin->text("item_focus");
      }
      return '<div class="slideNews slick-init">'.$list.'</div>';
    } else {
      return '';
    }
  }
}
?>