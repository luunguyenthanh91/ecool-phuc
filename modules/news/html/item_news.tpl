<!-- BEGIN: item_news -->

<div class="col-6 col-md-6 col-sm-4"><a href="{data.link}">
    <div class="items-news-event">{data.pic}
      <h3 class="name-news-event fs-14 font-weight-bold">{data.title}</h3>
      <p class="short-description-news">{data.short}</p>
    </div></a>
</div>
<!-- END: item_news -->