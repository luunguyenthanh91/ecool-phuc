<!-- BEGIN: html_print -->
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>{CONF.indextitle} {CONF.extra_title}</title>
<meta name="robots" content="index, follow"/>
<meta name="author" content="{CONF.indextitle}"/>
<meta name="description" CONTENT="{CONF.meta_description}" />
<meta name="keywords" CONTENT="{CONF.meta_keyword}" />
<link rel="SHORTCUT ICON" href="{CONF.rooturl}favicon.ico" type="image/x-icon" />
<link rel="icon" href="{CONF.rooturl}favicon.ico" type="image/gif" />
<link rel="alternate" type="application/rss+xml" title="{CONF.indextitle}" href="{CONF.rooturl}rss/last10_news_vn.php"/>
<link href="{DIR_MOD}/css/popup.css" rel="stylesheet" type="text/css" />
<link href="{DIR_MOD}/css/news.css" rel="stylesheet" type="text/css" />

<script language="javascript" >
	var ROOT = "{CONF.rooturl}";
	var DIR_IMAGE = "{DIR_IMAGE}";
	var cmd = "{CONF.cmd}";
</script>
<script type="text/javascript" src="{DIR_JS}/javascript.js"></script>
</head>

<table border="0" cellpadding="0" cellspacing="0" width="700"
	align="center">
	<tr>
		<td align="left" width="40%">&nbsp;</td>
		<td align="right"><a href="javaScript:window.print();"><img src="{DIR_MOD}/images/btn-print.gif" /></a></td>
	</tr>
</table>
<hr color="#4d5764" width="700" align="center">

<table width="700" border="0" cellspacing="0" cellpadding="0" align="center">
  <tr>
    <td>

<div id="boxNews" class="boxDetail">
	<p class='title'>{data.title}</p>
	<p class="date_news">{data.date_post}</p>
	<div class="content-news">{data.content_news}</div>
	<br class="clear" />
	{data.note}
	<div style='padding-right:10px;' align="right">{data.source}</div>
	<div style='padding-right:10px;' align="right">{data.post_by}</div>
	<br class="clear" />  
</div>

</td>
  </tr>
</table>
<hr color="#4d5764" width="700" align="center">
<table border="0" cellpadding="0" cellspacing="0" width="700"
	align="center">
	<tr>
		<td align="left">{LANG.global.copyright}</td>
		<td align="right"><a href="javaScript:window.print();"><img src="{DIR_MOD}/images/btn-print.gif" /></a></td>
	</tr>
</table>

</body>
</html>
<!-- END: html_print -->
