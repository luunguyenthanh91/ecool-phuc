<!-- BEGIN: modules -->
<div class="ecool-news">
      <div class="position-relative"><img class="w-100" src="{DIR_IMAGE}/banner-news.png" alt="Sơn Hà">
        <div class="container">
          <div class="row menu-sub-news text-center">
             {data.box_category}
                <div class="col-6 col-lg-3 p-0"><a href="{data.link_videls}">
                <p class="fs-16 font-weight-medium menu-sub-news-title ">Videos</p></a></div>

          </div>
        </div>
      </div>

      <div class="news-event">
        <div class="container">
          {data.main}
        </div>
      </div>
    </div>
<!-- END: modules -->






<!-- BEGIN: html_list -->
<div class="">
  <div class="row">
            <div class="col-12">
              <h3 class="title-news-event">{data.f_title}</h3>
            </div>
          </div>
          <div class="row row-news-event">
           {data.list_news}
          </div>
</div>

<!-- END: html_list -->

<!-- BEGIN: html_list_ctud -->
<div class="">
  <div class="row">
            <div class="col-12">
              <h3 class="title-news-event">{data.f_title}</h3>
            </div>
          </div>
          <div class="row row-news-event">
           {data.list_news}
          </div>
</div>
<!-- END: html_list_ctud -->

<!-- BEGIN: item_focus -->

<div class="item">
  <div class="newsS" itemscope itemtype="http://schema.org/Article">
    <div class="img">
      <div class="wrap" itemprop="image" itemscope itemtype="https://schema.org/ImageObject">
        <a href="{data.link}">
          <img itemprop="url" src="{data.src}" alt="{data.title}" />
          <meta itemprop="width" content="585"><meta itemprop="height" content="390">
        </a>
      </div>
    </div>
    <div class="caption">
      <div class="per"><span>{data.cat_name}</span></div>
      <div class="date">{data.date_post}</div>
      <div class="tend">
        <h3 itemprop="headline">
          <a itemprop="url" href="{data.link}">{data.title}</a>
        </h3>
      </div>
      <div class="des" itemprop="description">{data.short}</div>
      <meta itemprop="datePublished" content="{data.date_post_meta}" />
      <meta itemprop="author" content="admin" />
      <meta itemprop="dateModified" content="{data.date_update_meta}" />
      <meta itemprop="mainEntityOfPage" content="123" />
      <div itemprop="publisher" itemscope itemtype="https://schema.org/Organization">
        <meta itemprop="name" content="admin" />
        <div itemprop="logo" itemscope itemtype="http://schema.org/ImageObject">
          <meta itemprop="url" content="http://www.mycorp.com/logo.jpg">
        </div>
      </div>
    </div>
  </div>
</div>
<!-- END: item_focus -->

<!-- BEGIN: item_other -->

                    <li><a href="{data.link}">{data.title}</a></li>


<!-- END: item_other -->

<!-- BEGIN: html_detail -->
<div class="the-title"><h1>{data.title}</h1></div>
<div class="the-date">{data.date_post}</div>
<div class="mod-content row">
    <div id="vnt-main" class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
        <div class="the-content desc">
            {data.content_news}
        </div>
        {data.list_tags}
        <div class="myTools">
            <div class="share">
                {data.list_social_network}
                <div class="clear"></div>
            </div>
            <div class="print hidden-sm hidden-xs">
                <ul>
                    <li>{data.print}</li>
                    <li>
                      <a href="javascript:;" class="fa-mail-reply-all" onclick="window.history.back();">{LANG.news.back}</a>
                    </li>
                </ul>
                <div class="clear"></div>
            </div>
            <div class="clear"></div>
        </div>
        {data.comment}
        <!-- <div class="box">
            <div class="box-title">
                <div class="fTitle">other news</div>
            </div>
            <div class="box-content">
                <ul class="news-other-list"> -->
        {data.other_news}
         <!-- </ul>
            </div>
            <div id="limit"></div>
        </div> -->

    </div>
    <div id="vnt-sidebar" class="col-lg-3 col-md-3 col-sm-12 col-xs-12 hidden-sm hidden-xs">
      {data.box_sidebar}
        <!-- <div class="newsMost">
            <div class="title">last news</div>
            <div class="content">
                <div class="newsM">
                    <div class="img"><a href=""><img src="images/news/1.jpg" alt="" /></a></div>
                    <div class="tend"><h3><a href="">The small house expanded to apply one of 10 ideas store this furniture</a></h3></div>
                </div>
                <div class="newsM">
                    <div class="img"><a href=""><img src="images/news/2.jpg" alt="" /></a></div>
                    <div class="tend"><h3><a href="">The small house expanded to apply one of 10 ideas store this furniture</a></h3></div>
                </div>
                <div class="newsM">
                    <div class="img"><a href=""><img src="images/news/3.jpg" alt="" /></a></div>
                    <div class="tend"><h3><a href="">The small house expanded to apply one of 10 ideas store this furniture</a></h3></div>
                </div>
                <div class="newsM">
                    <div class="img"><a href=""><img src="images/news/4.jpg" alt="" /></a></div>
                    <div class="tend"><h3><a href="">The small house expanded to apply one of 10 ideas store this furniture</a></h3></div>
                </div>
                <div class="newsM">
                    <div class="img"><a href=""><img src="images/news/5.jpg" alt="" /></a></div>
                    <div class="tend"><h3><a href="">The small house expanded to apply one of 10 ideas store this furniture</a></h3></div>
                </div>
            </div>
        </div> -->

    </div>
</div>
<!-- END: html_detail -->

<!-- BEGIN: html_comment -->
<script language="javascript">
  js_lang['err_name_empty'] = "{LANG.news.err_name_empty}";
  js_lang['err_email_empty'] = "{LANG.news.err_email_empty}";
  js_lang['err_email_invalid'] = "{LANG.news.err_email_invalid}";
  js_lang['security_code_invalid'] = "{LANG.news.security_code_invalid}";
  js_lang['err_security_code_empty'] = "{LANG.news.err_security_code_empty}";
  js_lang['err_title_empty'] = "{LANG.news.err_title_empty}";
  js_lang['err_content_comment_empty'] = "{LANG.news.err_content_comment_empty}";
  js_lang['send_comment_success'] = "{LANG.news.send_comment_success}";
  js_lang['send_reply_success'] = "{LANG.news.send_reply_success}";
  js_lang['err_conntent_minchar'] = "{LANG.news.err_conntent_minchar}";
  js_lang['mess_error_post'] = "{LANG.news.mess_error_post}";
</script>
<script type="text/javascript" src="{DIR_MOD}/js/comment.js"></script>
<input type="hidden" name="sID" id="sID" value="{data.newsid}">
<div class="comment">
  <div class="title">{LANG.news.f_comment}</div>
  <div class="formComment">
    <form action="{data.link_action}" method="post" name="fComment" id="fComment" onsubmit="return vnTcomment.post_comment('{data.newsid}','{data.lang}');">
      <div class="w_content">
        <textarea id="com_content" name="com_content" class="form-control" placeholder="{LANG.news.comment_default}"></textarea>
        <div class="content-info" style="display: none;">
          <div class="info-title">{LANG.news.enter_info_comment}</div>
          <input type="text" name="com_email" id="com_email" class="form-control" placeholder="Email" value="{data.com_email}"/>
          <input type="text" name="com_name" id="com_name" class="form-control" placeholder="{LANG.news.your_name}" value="{data.com_name}"/>
          <div class="input-group">
            <input type="text" name="security_code" id="security_code" class="form-control" placeholder="{LANG.news.security_code}">
            <div class="input-group-img">
              <img class="security_ver" src="{data.ver_img}" alt="">
            </div>
          </div>
          <button id="btn-search" name="btn-search" class="btn" type="submit">{LANG.news.btn_send_comment}</button>
          <button id="btn-close" name="btn-close" class="btn" type="button">{LANG.news.btn_close}</button>
          </div>
      </div>
    </form>
  </div>
  {data.list_comment}
</div>
<!-- END: html_comment -->

<!-- BEGIN: tool_search -->
<div class="toolSearch">
  <form action="{data.link_action}" method="post" name="Search">
  <input type="hidden" name="do_search" value="1"/>
  <table border="0" cellspacing="2" cellpadding="2">
    <tr>
      <td nowrap="nowrap"><strong>{LANG.news.search_date} : </strong>   </td>
      <td style="padding:0px 5px;">{data.list_day}&nbsp;{data.list_month}&nbsp;{data.list_year}&nbsp;</td>
      <td><input type="submit" name="btnSearch" value="{LANG.news.btn_search}" class="btn_search"/></td>
    </tr>
  </table>
  </form>
</div>
<!-- END: tool_search -->

<!-- BEGIN: html_search -->
<p class="mess_result">
 {data.note_keyword} <br>
{data.note_result}
</p>
{data.list_news}
{data.nav}
<!-- END: html_search -->

<!-- BEGIN: html_tags -->
<p class="mess_result">{data.note_result}</p>
<div class="vnt-news">
  {data.list_news}
</div>
{data.nav}
<!-- END: html_tags -->
