<!-- BEGIN: modules -->
<div class="news-detail">
      <div class="container">
        <nav class="fs-10 font-weight-light" aria-label="breadcrumb">
          <!-- <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="/home.html">Home</a></li>
            <li class="breadcrumb-item"><a href="/home.html">Tin tức</a></li>
            <li class="breadcrumb-item"><a href="/home.html">Góc tư vấn</a></li>
            <li class="breadcrumb-item active" aria-current="page"><a href="#">ECool wifi và lưu ý sử dụng</a></li>
          </ol> -->
          {data.navation}
        </nav>
      </div>
      
     {data.main}
    </div>
<!-- END: modules -->

<!-- BEGIN: html_detail1 -->



<div class="container container--790 pb-5">
        <div class="row">
          <div class="col-12 text-center">
            <h1 class="news-title">{data.title}</h1>
          </div>
          <div class="col-12 text-center news-social">
            <div class="social-news-detail mt-4 mb-4"><a href="http://www.facebook.com/sharer/sharer.php?u={data.link}&t={data.title}" title="Share Facebook"><img class="social__icon" src="{DIR_IMAGE}/icon10.png"></a><a href="#"><img class="social__icon" src="{DIR_IMAGE}/icon11.png"></a></div>
          </div>
          <div class="col-12 fs-15 news-content text-justify">
             {data.content_news}


            <span class="news-date float-right">{data.date_post}</span>
          </div>
        </div>
      </div>

{data.other_news}
<!-- END: html_detail1 -->

<!-- BEGIN: html_comment -->
<script language="javascript">
  js_lang['err_name_empty'] = "{LANG.news.err_name_empty}";
  js_lang['err_email_empty'] = "{LANG.news.err_email_empty}";
  js_lang['err_email_invalid'] = "{LANG.news.err_email_invalid}";
  js_lang['security_code_invalid'] = "{LANG.news.security_code_invalid}";
  js_lang['err_security_code_empty'] = "{LANG.news.err_security_code_empty}";
  js_lang['err_title_empty'] = "{LANG.news.err_title_empty}";
  js_lang['err_content_comment_empty'] = "{LANG.news.err_content_comment_empty}";
  js_lang['send_comment_success'] = "{LANG.news.send_comment_success}";
  js_lang['send_reply_success'] = "{LANG.news.send_reply_success}";
  js_lang['err_conntent_minchar'] = "{LANG.news.err_conntent_minchar}";
  js_lang['mess_error_post'] = "{LANG.news.mess_error_post}";
</script>
<script type="text/javascript" src="{DIR_MOD}/js/comment.js"></script>
<input type="hidden" name="sID" id="sID" value="{data.newsid}">
<div class="comment">
  <div class="title">{LANG.news.f_comment}</div>
  <div class="formComment">
    <form action="{data.link_action}" method="post" name="fComment" id="fComment" onsubmit="return vnTcomment.post_comment('{data.newsid}','{data.lang}');">
      <div class="w_content">
        <textarea id="com_content" name="com_content" class="form-control" placeholder="{LANG.news.comment_default}"></textarea>
        <div class="content-info" style="display: none;">
          <div class="info-title">{LANG.news.enter_info_comment}</div>
          <input type="text" name="com_email" id="com_email" class="form-control" placeholder="Email" value="{data.com_email}"/>
          <input type="text" name="com_name" id="com_name" class="form-control" placeholder="{LANG.news.your_name}" value="{data.com_name}"/>
          <div class="input-group">
            <input type="text" name="security_code" id="security_code" class="form-control" placeholder="{LANG.news.security_code}">
            <div class="input-group-img">
              <img class="security_ver" src="{data.ver_img}" alt="">
            </div>
          </div>
          <button id="btn-search" name="btn-search" class="btn" type="submit">{LANG.news.btn_send_comment}</button>
          <button id="btn-close" name="btn-close" class="btn" type="button">{LANG.news.btn_close}</button>
          </div>
      </div>
    </form>
  </div>
  {data.list_comment}
</div>
<!-- END: html_comment -->

<!-- BEGIN: tool_search -->
<br />
<div  class="toolSearch" >
<form action="{data.link_action}" method="post"   name="Search" >
<input type="hidden" name="do_search" value="1" />
<table  border="0" cellspacing="2" cellpadding="2">
  <tr> 
    <td nowrap="nowrap"><strong>{LANG.news.search_date} : </strong>   </td>
    <td style="padding:0px 5px;">{data.list_day}&nbsp;{data.list_month}&nbsp;{data.list_year}&nbsp;</td> 
    <td><input type="submit" name="btnSearch" value="{LANG.news.btn_search}" class="btn_search" /></td> 
  </tr>
</table> 
</form>
</div>
<!-- END: tool_search -->

<!-- BEGIN: html_search --> 
<p class="mess_result">
 {data.note_keyword} <br>
{data.note_result}
</p>
{data.list_news}
{data.nav}
<!-- END: html_search -->

<!-- BEGIN: html_tags -->
<p class="mess_result">{data.note_result}</p>
<div class="vnt-news">
  <div class="gridN">
    {data.list_news}
  </div>
</div>
{data.nav}
<!-- END: html_tags -->