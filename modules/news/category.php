<?php
/*================================================================================*\
|| 							Name code : category.php	 		 																		  # ||
||  				Copyright © 2007 by Thai Son - CMS vnTRUST                					# ||
\*================================================================================*/
/**
 * @version : 1.0
 * @date upgrade : 11/12/2007 by Thai Son
 **/
if (! defined('IN_vnT')) {
  die('Access denied');
}
$nts = new sMain();

class sMain
{
  var $output = "";
  var $skin = "";
  var $linkUrl = "";
  var $module = "news";
  var $action = "category";

  function sMain (){
    global $vnT, $input;
    include ("function_" . $this->module . ".php");
    loadSetting();
    $this->skin = new XiTemplate(DIR_MODULE . "/" . $this->module . "/html/" . $this->module . ".tpl");
    $this->skin->assign('DIR_MOD', DIR_MOD);
    $this->skin->assign('LANG', $vnT->lang);
    $this->skin->assign('INPUT', $input);
    $this->skin->assign('CONF', $vnT->conf);
    $this->skin->assign('DIR_IMAGE', $vnT->dir_images);
    $this->linkMod = $vnT->cmd . "=mod:" . $this->module;
    $vnT->html->addStyleSheet(DIR_MOD . "/css/" . $this->module . ".css");
    $vnT->html->addScript(DIR_MOD . "/js/" . $this->module . ".js");
		//active menu
		$vnT->setting['menu_active'] = $this->module;
    $cat_id = (int) $input['catID'];
    $res_ck = $vnT->DB->query("SELECT * FROM news_category  n , news_category_desc nd
															 WHERE n.cat_id=nd.cat_id AND display=1 
															 AND lang='{$vnT->lang_name}' AND n.cat_id=$cat_id ");
    if ($row = $vnT->DB->fetch_row($res_ck)) {
			//SEO
			$arr_catCode = explode("_",$row['cat_code']);
			$input['catRoot'] = $arr_catCode[0] ;
			$vnT->setting['menu_active'] = $vnT->setting['friendly_url'][$input['catRoot']];
			if ($row['metadesc'])	$vnT->conf['meta_description'] = $row['metadesc'];
			if ($row['metakey'])	$vnT->conf['meta_keyword'] = $row['metakey'];
			if ($row['friendly_title'])	 $vnT->conf['indextitle'] =  $row['friendly_title'] ;			
			$link_seo = ($vnT->muti_lang) ? ROOT_URL.$vnT->lang_name."/" : ROOT_URL ;
			$link_seo .= $row['friendly_url'].".html";			
			$vnT->conf['meta_extra'] .= "\n".'<link rel="canonical" href="'.$link_seo.'" />';
			$vnT->conf['meta_extra'] .= "\n". '<link rel="alternate" media="handheld" href="'.$link_seo.'"/>';
			if($vnT->muti_lang>0){
				$res_lang= $vnT->DB->query("SELECT friendly_url,lang FROM news_category_desc 
                                    WHERE cat_id=".$row['cat_id']." AND display=1 ");
				while ($row_lang = $vnT->DB->fetch_row($res_lang)){
					$link_lang = create_link ("category",$row['cat_id'],$row_lang['friendly_url']);
				}
			}
      $data['main'] = $this->do_Category($row);
    } else {
      @header("Location: ".LINK_MOD.".html");
      echo "<meta http-equiv='refresh' content='0; url=" . LINK_MOD . ".html' />";
    }
    $navation = get_navation($input['catID']);
    $data['navation'] = $vnT->lib->box_navation($navation);
    $vnT->setting['banner'] = $vnT->lib->get_child_slide('child');
    $data['fixed_sidebar'] = $vnT->lib->fixed_sidebar();
    $this->skin->assign("data", $data);
    $this->skin->parse("modules");
    $vnT->output .= $this->skin->text("modules");
  }
  function focus_cat ($cat_id){
    global $DB, $conf, $func, $vnT, $input;
    $content = "";
    $n = ($vnT->setting['n_focus_cat']) ? $vnT->setting['n_focus_cat'] : 4 ;
    $where =  " AND focus_cat=1 ";
    $where .= get_where_cat($cat_id);
    $sql = "SELECT * FROM news n, news_desc nd
            WHERE n.newsid=nd.newsid AND lang='$vnT->lang_name' AND display=1 $where 
            order by focus_cat_order DESC, date_post desc LIMIT 0," . $n; 
    $result = $DB->query($sql);
    if ($num=$DB->num_rows($result)){
      $i = 0;
      $w = ($vnT->setting['imgfocus_width']) ? $vnT->setting['imgfocus_width'] : 200; 
      while ($row = $vnT->DB->fetch_row($result)){
        $i++;
        $news_id = $row['news_id'];
        $data['link'] = create_link("detail", $row['newsid'], $row['friendly_url'],$ext_pag);
        $picture = ($row['picture']) ? "news/".$row['picture'] : "news/".$vnT->setting['pic_nophoto'];
        $data['src'] = $vnT->func->get_src_modules($picture, $w ,'',1,'1.5:1');
        $data['title'] = $func->HTML($row['title']);
        $data['date_post'] = @date('d/m/Y, H:i', $row['date_post']);
        $data['date_post_meta'] = @date('Y-m-d', $row['date_post']);
        $data['date_update_meta'] = @date('Y-m-d', $row['date_update']);
        $data['short'] = $vnT->func->cut_string($vnT->func->check_html($row['short'],'nohtml'),300,1);
        $this->skin->reset("item_focus");
        $this->skin->assign("data", $data);
        $this->skin->parse("item_focus");
        $list .= $this->skin->text("item_focus");
      }
      return '<div class="slideNews slick-init">'.$list.'</div>';
    } else {
      return '';
    }
  }
  function do_Category ($info){
    global $vnT, $input, $DB, $func, $conf;
    $vnT->html->addStyleSheet($vnT->dir_js ."/datepicker/datepicker.css");
    $vnT->html->addScript($vnT->dir_js ."/datepicker/datepicker.js");
    $text = "";
    $where = "";
    $cat_id = (int) $info['cat_id'];
    $p = ((int) $input['p']) ? (int) $input['p'] : 1;
    $n = (! empty($vnT->setting['n_cat'])) ? $vnT->setting['n_cat'] : 10;
    $n_other = (! empty($vnT->setting['other_cat'])) ? $vnT->setting['other_cat'] : 10;
    $where .= get_where_cat($cat_id);
    $date_post = ($input['date_post']) ? $input['date_post'] : '';
    if($date_post){
      $arr = explode('/', $date_post);
      $time_begin = mktime(0,0,0,$arr[1],$arr[0],$arr[2]);
      $time_end = mktime(23,59,59,$arr[1],$arr[0],$arr[2]);
      $where .= " AND date_post BETWEEN $time_begin AND $time_end ";
      $ext_pag .= "&date_post=".$date_post;
    }
		$sql_num = "SELECT n.newsid FROM news n , news_desc nd
								WHERE n.newsid=nd.newsid AND lang='$vnT->lang_name' AND display=1 $where ";
		$res_num = $DB->query($sql_num);
    $totals = $DB->num_rows($res_num);
    $num_pages = ceil($totals / $n);
    if ($p > $num_pages) $p = $num_pages;
    if ($p < 1) $p = 1;
    $start = ($p - 1) * $n;
    if ($num_pages > 1) {
      $root_link = create_link("category",$cat_id,$info['friendly_url']) ;
      $nav = "<div class='pagination'>".$vnT->func->htaccess_paginate($root_link,$totals,$n,$ext_pag, $p)."</div>";
    }
    $num_row = 2;
		$view = 2;
		
    $data['nav'] = $nav;
    $data['mess'] = $mess;
    $data['box_focus'] = $this->focus_cat($cat_id);
    $data['box_category'] = box_category();
    $data['link_videls']  = $vnT->link_root.$vnT->setting['seo_name'][$vnT->lang_name]['dealer'].'.html';
  //   $this->skin->reset("html_list");
  //   $this->skin->assign("data", $data);
  //   $this->skin->parse("html_list");
  //   $nd['content'] = $this->skin->text("html_list");
		$data['f_title'] = $vnT->func->HTML($info['cat_name']);
  //   $nd['more'] = ($info['slogan']) ? $vnT->func->HTML($info['slogan']) : $vnT->setting['slogan'];
  //   $text .= $vnT->skin_box->parse_box("box_middle", $nd);
  //  	return $text;

    if($cat_id == 3){
      $data['list_news'] = row_news2($where, $start, $n, $num_row, $view);
      $this->skin->assign("data", $data);
      $this->skin->parse("html_list_ctud");
      return $this->skin->text("html_list_ctud");
    }else{
      $data['list_news'] = row_news($where, $start, $n, $num_row, $view);
      $this->skin->assign("data", $data);
      $this->skin->parse("html_list");
      return $this->skin->text("html_list");
    }
    

  }
  // end class
}
?>