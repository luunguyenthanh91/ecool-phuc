<?php
/*================================================================================*\
|| 							Name code : news.php	 		 																		  # ||
||  				Copyright © 2007 by Thai Son - CMS vnTRUST                					# ||
\*================================================================================*/
/**
 * @version : 1.0
 * @date upgrade : 11/12/2007 by Thai Son
 **/
if (! defined('IN_vnT')) {
  die('Access denied');
}
$nts = new sMain();

class sMain
{
  var $output = "";
  var $skin = "";
  var $linkUrl = "";
  var $module = "news";
  var $action = "print";

  /**
   * function sMain ()
   * Khoi tao 
   **/
  function sMain ()
  {
    global $vnT, $input;
    include ("function_" . $this->module . ".php");
    loadSetting();
    $this->skin = new XiTemplate(DIR_MODULE . "/" . $this->module . "/html/" . $this->action . ".tpl");
    $this->skin->assign('DIR_MOD', DIR_MOD);
    $this->skin->assign('LANG', $vnT->lang);
    $this->skin->assign('INPUT', $input);
    $this->skin->assign('CONF', $vnT->conf);
    $this->skin->assign('DIR_IMAGE', $vnT->dir_images);
		$this->skin->assign('DIR_JS', $vnT->dir_js);
		
    $newsid = (int) $input['newsid'];
    
    $res_ck = $vnT->DB->query("select * from news n, news_desc nd  WHERE n.newsid=nd.newsid AND display=1 AND lang='$vnT->lang_name' AND n.newsid=$newsid  ");
    if ($row = $vnT->DB->fetch_row($res_ck)) {
       
      //SEO
			if ($row['metadesc'])	$vnT->conf['meta_description'] = $row['metadesc'];
			if ($row['metakey'])	$vnT->conf['meta_keyword'] = $row['metakey'];
			$vnT->conf['indextitle'] =  $row['friendly_title'];
			//$vnT->conf['indextitle'] ="";
		
			//check quyen xem
			if ($row['permesion_view'] == 1 && $vnT->user['mem_id'] == 0) {
				$mess = $vnT->lang['news']['only_member'];
				$url = LINK_MOD . ".html";
				$vnT->func->html_redirect($url, $mess);
			}
			 
			 
			if ($row['picture'])
				$data['pic'] = get_pic_news_detail($row['picture'], $row['picturedes'], $vnT->setting['imgdetail_width']);
			$data['title'] = $vnT->func->HTML($row['title']);
			if ($row['note']) {
				$data['note'] = "<div class='noteNews'>" . $vnT->func->HTML($row['note']) . "</div>";
			}
			$data['source'] = get_source($row['source']);
			$data["date_post"] = get_datePost($row["date_post"]);
			if ($row['poster'])
				$data['post_by'] = $vnT->lang['news']['post_by'] . " : " . $vnT->func->HTML($row['poster']);
 
			
			$content_news = 	$row['content'];
			 
			
			
			// type_show
			switch ((int) $row['type_show']) {
				case 3:
					$data['content_news'] = "<div align=\"justify\">" ."<strong>" . $row['short'] . "</strong>". $content_news . "</div>";
				break;
				case 2:
					$data['content_news'] = "<div align=\"justify\">" . $content_news . "</div>";
				break;
				case 1:
					$data['content_news'] = "<div align=\"justify\">" . $data['pic'] . "" . $content_news . "</div>";
				break;
				case 0:			
					$data['content_news'] = "<div align=\"justify\">" . $data['pic'] . "<strong>" . $row['short'] . "</strong>" . $content_news . "</div>";
				break;
			}
   
	 
    } else {
      $mess = $vnT->lang['news']['not_found_news'] . ' ID <strong>' . $newsid . '</strong>';
      $url = LINK_MOD . ".html";
      $vnT->func->html_redirect($url, $mess);
    }

    $this->skin->assign("data", $data);
    $this->skin->parse("html_print");
    flush();
		echo $this->skin->text("html_print");
		exit();
  }
 	
  // end class
}
?>