<?php
// no direct access
if (! defined('IN_vnT')) {
  die('Hacking attempt!');
}
 
$seo_mod_name = $vnT->setting['seo_name'][$vnT->lang_name]['news'] ;	 
$ext_link= '';
$url_last = $vnT->url_array[count($vnT->url_array)-1] ;	 
if( strstr($url_last,"p-")){
	$cmd_arr = @explode(",",$url_last)	;
	foreach ($cmd_arr as $value)
	{
		if (! empty($value))
		{
			$k = trim(substr($value, 0, strpos($value, "-")));
			$v = trim(substr($value, strpos($value, "-") + 1));
			$ext_link .= "|".$k.":".$v;
		}
	}
}
//news
if (strstr($_SERVER['REQUEST_URI'], $seo_mod_name.".html")) {
  $_GET[$vnT->conf['cmd']] = "mod:news".$ext_link ;
  $QUERY_STRING = $vnT->conf['cmd'] . "=mod:news".$ext_link;
}

if (in_array($seo_mod_name, $vnT->url_array)) {
  $pos = array_search($seo_mod_name, $vnT->url_array);
  //		print_r($vnT->url_array);
  $mod = "news";
  $act = $vnT->url_array[$pos + 1];
 			
  //category
  $text_catid = $act;
	$catID = strtolower(substr($text_catid, strrpos($text_catid, "-") + 1));
	$catID = str_replace(".html","",$catID);
	$catID = (int) $catID ;
	
	if(strstr($act,".html"))
	{
		$text_catid = $act;
		$catID = strtolower(substr($text_catid, strrpos($text_catid, "-") + 1));
		$catID = str_replace(".html","",$catID);
		$catID = (int) $catID ;
		
		if((int)$catID)
		{  
			$vnT->setting['cur_friendly_url'] = substr($text_catid, 0, strrpos($text_catid, "-") );
			$_GET[$vnT->conf['cmd']] = "mod:$mod|act:category|catID:$catID".$ext_link ;
			$QUERY_STRING = $vnT->conf['cmd'] . "=mod:$mod|act:category|catID:$catID".$ext_link ;		 
			//echo "QUERY_STRING = ".$QUERY_STRING;
		}
	} 
	
	$nID = $vnT->url_array[$pos+1];
	if((int)$nID && !strstr ($nID,".html"))
	{
		$vnT->setting['cur_friendly_url'] = str_replace(".html","",$vnT->url_array[$pos+2]);
		$_GET[$vnT->conf['cmd']] = "mod:$mod|act:detail|newsid:$nID" ;
		$QUERY_STRING = $vnT->conf['cmd'] . "=mod:$mod|act:detail|newsid:$nID" ;  
	}
	 
   //search
	if($act=="search"  || $act=="search.html" )	
	{	 
		$_GET[$vnT->conf['cmd']] = "mod:$mod|act:search".$ext_link ;
		$QUERY_STRING = $vnT->conf['cmd']."=mod:$mod|act:search".$ext_link ;
	}	 
	//tags
	if( $act =="tags" || $act =="tags.html" ) {
		$tagID = $vnT->url_array[$pos + 2]; 
		$_GET[$vnT->conf['cmd']] = "mod:$mod|act:tags|tagID:$tagID" ;
		$QUERY_STRING = $vnT->conf['cmd'] . "=mod:$mod|act:tags|tagID:$tagID" ;
	}
	//print
  if ($act == "print") {
    $newsid = $vnT->url_array[$pos + 2];
    $_GET[$vnT->conf['cmd']] = "mod:$mod|act:print|newsid:$newsid";
    $QUERY_STRING = $vnT->conf['cmd'] . "=mod:$mod|act:print|newsid:$newsid";
  }
	
	//search
	if($act=="rss.html" || $act=="rss")	
	{
		$seoCat = $vnT->url_array[$pos+2] ;
		$seoCat = str_replace(".feed","",$seoCat);		
		$ext_link = ($seoCat) ? "|rss_name:".$seoCat : "";
		
		$_GET[$vnT->conf['cmd']] = "mod:$mod|act:rss".$ext_link;
		$QUERY_STRING = $vnT->conf['cmd']."=mod:$mod|act:rss".$ext_link;
	}	
} //end news
?>