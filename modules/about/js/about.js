$(document).ready(function(){
    $(".aboutVideo .link").fancybox({
    	padding:0,
    });
    $("#slideAbout").slick({
    	slidesToShow : 4,
    	autoplay:true,
    	arrows:false,
    	speed:500,
    	responsive: [
            {
                breakpoint: 991,
                settings: {
                    slidesToShow: 3,
                }
            },
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 3,
                }
            },
            {
                breakpoint: 600,
                settings: {
                    slidesToShow: 2,
                }
            },
            {
                breakpoint: 420,
                settings: {
                    slidesToShow: 1,
                }
            },
        ]
    });
});