<?php
/*================================================================================*\
|| 							Name code : function_about.php 		 		 												  # ||
||  				Copyright © 2007 by Thai Son - CMS vnTRUST                					# ||
\*================================================================================*/
/**
 * @version : 1.0
 * @date upgrade : 17/12/2007 by Thai Son
 **/
if (! defined('IN_vnT')) {
  die('Access denied');
}
define("DIR_MOD", ROOT_URI . "modules/about");
define("DIR_MOD_IMAGE", ROOT_URI . "modules/about/images");
define("LINK_MOD", $vnT->link_root . $vnT->setting['seo_name'][$vnT->lang_name]['about']); 
define('MOD_DIR_UPLOAD', ROOT_URI . 'vnt_upload/about');

function loadSetting (){
  global $vnT, $func, $DB, $conf;
  $setting = array();
  $result = $DB->query("SELECT * FROM about_setting WHERE lang='$vnT->lang_name' ");
  $setting = $DB->fetch_row($result);
  foreach ($setting as $k => $v) {
    $vnT->setting[$k] = stripslashes($v);
  }
  unset($setting);
}
/*-------------- create_link --------------------*/
function create_link ($id, $title, $extra = ""){
  global $vnT, $func, $DB, $conf;
	$text = $vnT->link_root.$title.".html";
  return $text;
}
function box_category (){
  global $vnT, $func, $DB, $conf, $input;
  $query= $DB->query("SELECT n.*,nd.title,nd.friendly_url FROM about n,about_desc nd  WHERE n.aid=nd.aid 
  										AND lang='{$vnT->lang_name}' AND display=1 And parentid=0
  										ORDER BY display_order ASC , date_post DESC");
  if ($num = $DB->num_rows($query)) {
    $i = 0;
    $text = "<div class='box_category'><ul >";
    while ($row = $DB->fetch_row($query)) {
      $i ++;
      $link = create_link($row['aid'], $row['friendly_url']);
      $class = ($input['aID'] == $row['aid']) ? ' class="current"' : "";
      //check sub
      $res_sub = $DB->query("SELECT * FROM about WHERE parentid=" . $row['aid'] . " ");
      while ($row_sub = $DB->fetch_row($res_sub)) {
        if ($input['aID'] == $row_sub['aid']) {
          $class = ' class="current"';
        }
      }
      $last = ($i == $num) ? " class ='last' " : "";
      $text .= "<li {$last} ><a href='{$link}' {$class}><span>" . $func->HTML($row['title']) . "</span></a>";
      $res_sub = $DB->query("SELECT n.*,nd.title,nd.friendly_url FROM about n,about_desc nd  WHERE n.aid=nd.aid 
										 AND lang='{$vnT->lang_name}'
										 AND display=1
										 And parentid=" . $row['aid'] . "
										 ORDER BY display_order ASC , date_post DESC");
      if ($num_sub = $DB->num_rows($res_sub)) {
        $text .= "<ul >";
        while ($row_sub = $DB->fetch_row($res_sub)) {
          $link_sub = create_link($row_sub['aid'], $row_sub['friendly_url']);
          $text .= "<li><a href='{$link_sub}' {$class}>" . $func->HTML($row_sub['title']) . "</a></li>";
        }
        $text .= "</ul>";
      }
      $text .= "</li>";
    }
    $text .= "</ul></div>"; 
		
		$nd['f_title']=$vnT->lang['about']['about'];
		$nd['content'] = $text ;
		$textout .=  $vnT->skin_box->parse_box("box",$nd);
		
    return $textout;
  }
}
function box_sidebar (){
  global $vnT, $con, $input, $DB, $func;
  $output = '';
  $output = box_category(); 
  return $output;
}
?>