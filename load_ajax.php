<?php
if(empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) != 'xmlhttprequest') {
	die('Hacking attempt!');
}
@ini_set("display_errors", "0");
session_start();
define('IN_vnT', 1);
define('PATH_ROOT', dirname(__FILE__));
define('DS', DIRECTORY_SEPARATOR);
require_once("_config.php");
require_once (PATH_ROOT . DS . 'includes' . DS . 'defines.php');
require_once("includes/class_db.php");
require_once("includes/class_functions.php");
require_once ('includes/class_global.php');
// initialize the data registry
$vnT = new vnT_Registry();
$DB = $vnT->DB ;
$func = $vnT->func ;
$conf = $vnT->conf ;

require_once("includes/JSON.php");
$vnT->user = array();
$vnT->user['mem_id'] =0;
$arr_cookie_member = explode("|",$_COOKIE[MEMBER_COOKIE]);
$res_mem = $DB->query("SELECT mem_id,password,username,mem_type FROM members WHERE username='".$arr_cookie_member[0]."' ");
if($row_mem = $DB->fetch_row($res_mem))
{
	$mem_hash = md5($row_mem['mem_id'] . '|' . $row_mem['password']);
	if($arr_cookie_member[1] ==$mem_hash )	{
		$vnT->user  = $row_mem  ;
	}
}


/*echo "<pre>";
print_r($vnT->user);
echo "</pre>";
*/
$vnT->lang_name = (isset($_POST['lang'])) ? $_POST['lang']  : "vn" ;
$func->load_language('global');

switch ($_GET['do'])
{
	case "optionCity" : $jsout = get_option_city() ;break;
	case "list_city" : $jsout = get_list_city() ;break;
	case "optionState" : $jsout = get_option_state() ;break;
	case "list_state" : $jsout = get_list_state() ;break;

	case "regMaillist" : $jsout = do_regMaillist() ; break;
	case "check_sec_code" : $jsout = do_checkSecCode() ; break;
	case "ajax_logout" : $jsout = do_ajax_logout() ; break;
	case "statistics" : $jsout = do_Statistics() ; break;
	case "popupBanner" : $jsout = get_popupBanner() ;break;
	case "auto_facebook" : $jsout = do_auto_facebook() ;break;
	default :  $jsout ="Error" ; break;
}

//get_list_city
function get_list_city() {
	global $DB,$func,$conf,$vnT;
	$textout="";

	$country = $_POST['country'];
	$selname = ($_POST['selname']) ? $_POST['selname'] : "city";
	$ext = ($_POST['ext']) ? $_POST['ext'] : "";

	$sql="SELECT * FROM iso_cities where display=1 and country='$country'  order by c_order ASC , name ASC  ";
	//	echo $sql;
	$result = $DB->query ($sql);
	if($num = $DB->num_rows($result))
	{
		$textout= "<select name=\"{$selname}\" id=\"{$selname}\" class='select form-control'  {$ext} >";
		$textout.="<option value=\"0\" selected>".$vnT->lang['global']['select_city']."</option>";
		while ($row = $DB->fetch_row($result)){
			$textout .= "<option value=\"{$row['id']}\"  >".$func->HTML($row['name'])."</option>";
		}
		$textout.="</select>";
	}else{
		$textout  ="<input type='text'  value='' class='textfiled form-control' name='{$selname}' id='{$selname}' {$ext} />";
	}

	return $textout;
}

//get_option_city
function get_option_city()
{
	global $DB, $func, $conf, $vnT;

	$text = "";
	$country = $_POST['country'];
	$city = $_POST['city'];

	$sql = "SELECT * FROM iso_cities where display=1 and country='$country'  order by c_order ASC , name ASC  ";
	//	echo $sql;
	$result = $DB->query($sql);
	if ($num = $DB->num_rows($result)) {

		$text .="<option value=\"0\" >".$vnT->lang['global']['select_city']."</option>";

		while ($row = $DB->fetch_row($result)) {
			$selected = ($row['id'] == $city) ? "selected" : "";
			$text .= "<option value=\"{$row['id']}\" {$selected} >" . $func->HTML($row['name']) . "</option>";
		}

	}else{
		$text  ="<input type='text'  value='' class='textfiled form-control' name='{$selname}' id='{$selname}'  />";
	}

	return $text;
}

function get_list_state() {
	global $DB,$func,$conf,$vnT;
	$textout="";
	$ext = "";
	$city = $_POST['city'];
	$act = $_POST['act'];
	$sql="SELECT * FROM iso_states where display=1 and city=".$city." ORDER BY s_order ASC , name ASC  ";
	$result = $DB->query ($sql);
	$textout= "<select name=\"{$act}\" id=\"{$act}\" class='form-control' {$ext} >";
	$textout.="<option value=\"0\" selected>".$vnT->lang['global']['select_state']."</option>";
	if($num = $DB->num_rows($result)){
		while ($row = $DB->fetch_row($result)){
			if ($row['id']==$did){
				$textout .= "<option value=\"{$row['id']}\" selected>".$func->HTML($row['name'])."</option>";
			} else{
				$textout .= "<option value=\"{$row['id']}\">".$func->HTML($row['name'])."</option>";
			}
		}
	}
	$textout.="</select>";
	return $textout;
}


//get_option_state
function get_option_state() {
	global $DB,$func,$conf,$vnT;

	$city = $_POST['city'];
	$state = $_POST['state'];
	$arr_json = array();


	$text  ="<option value=\"0\" >Chọn quận huyện</option>";
	$sql="SELECT * FROM iso_states where display=1 and city=".$city."  order by s_order ASC , name ASC  ";
	//	echo $sql;
	$result = $DB->query ($sql);
	if($num = $DB->num_rows($result))
	{

		while ($row = $DB->fetch_row($result)) {
			$selected = ($row['id'] == $state) ? "selected" : "";
			$text .= "<option value=\"{$row['id']}\" {$selected} >" . $func->HTML($row['name']) . "</option>";
		}
	}

	return $text;
}


//do_regMaillist
function do_regMaillist() {
	global $DB,$func,$conf,$vnT;
	$arr_json = array();
	$email = $_POST['email'] ;
	$name = ($_POST['name']) ? $_POST['name'] :  "Khách hàng website" ;

	$res = $DB->query("SELECT id FROM listmail WHERE  email='$email' ");
	if(!$row = $DB->fetch_row($res))
	{
		$cot['email'] = $email;
		$cot['cat_id'] = ($vnT->user['mem_id']) ? 1 : 2;
		$cot['name'] = $name ;
		$cot['datesubmit'] = time();
		$DB->do_insert("listmail",$cot);
		$mess = "Email : ".$email ." đã được cập nhật nhận thông báo mới "  ;
	}else{
		$mess = "Email : ".$email ." đã đăng ký nhận thông báo mới rồi " ;
	}

	$arr_json['mess'] = $mess;
	$json = new Services_JSON( );
	$textout = $json->encode($arr_json);

	return $textout;
}

//do_checkSecCode
function do_checkSecCode() {
	global $DB,$func,$conf,$vnT;
	$textout="";
	$security_code = $_POST['security_code'];
	if ($security_code == $_SESSION['sec_code']) {
		$valid =  true;
	} else {
		$valid = false;
	}

	$arr_json['valid'] = $valid;

	$json = new Services_JSON( );
	$textout = $json->encode($arr_json);

	return $textout;

}




//do_ajax_logout
function do_ajax_logout() {
	global $DB,$func,$conf,$vnT;
	$arr_json = array();
	$time = time();
	$mem_id = $vnT->user['mem_id'];
	$vnT->DB->query("UPDATE sessions SET time='{$time}',mem_id=0 WHERE ( mem_id=".$mem_id." OR s_id='".$vnT->session->get("s_id")."' ) ");
	$vnT->func->vnt_clear_member_cookie();
	$arr_json['ok'] = 1 ;
	$json = new Services_JSON( );
	$textout = $json->encode($arr_json);

	return $textout;
}


//get_popupBanner
function get_popupBanner() {
	global $DB,$func,$conf,$vnT;
	$text="";
	$arr_json = array();
	$arr_json['show'] = 0;
	if( empty($_SESSION["show_popup"]))	{

		$res_b = $DB->query(" Select * from advertise where pos='popup' and display=1  and lang='$vnT->lang_name'   order by l_order LIMIT 0,1");
		if ($row_b = $DB->fetch_row($res_b))
		{

			$title = $func->HTML($row_b['title']);
			$popup_w = ($row_b['width']) ? $row_b['width'] : 600 ;
			$popup_h = ($row_b['height']) ? $row_b['height'] : 500 ;
			$extra_style ='';

			if($row_b['type_ad']==1)
			{
				$popup_banner = '<div style="padding:10px; text-align:left;">'.$row_b['img']."</div>";
			}else{
				$src = ROOT_URL . "vnt_upload/weblink/" . $row_b['img'];
				if ($row_b["type"] == "swf") {
					$popup_banner = "<object classid='clsid:D27CDB6E-AE6D-11cf-96B8-444553540000' codebase='http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=7,0,19,0' width=" . $row_b["width"] . " height=" . $row_b["height"] . "><param name='movie' value=" . $src . " /><param name='quality' value='high' /><param value='transparent' name='wmode'/><embed src=" . $src . " quality='high' pluginspage='http://www.macromedia.com/go/getflashplayer' type='application/x-shockwave-flash' width=" . $row_b["width"] . " height=" . $row_b["height"] . " wmode='transparent' ></embed></object>";
				} else {

					$link_image = $vnT->conf['rootpath']. "vnt_upload/weblink/" . $row_b['img'];
					$fext = strtolower(substr($row_b['img'], strrpos($row_b['img'], ".") + 1));
					switch ($fext) {
						case 'jpg':	$img = @imagecreatefromjpeg($link_image);	break;
						case 'gif':	$img = @imagecreatefromgif($link_image); break;
						case 'png':	$img = @imagecreatefrompng($link_image); break;
					}
					/*$img_h = @imagesy($img);
					if($img_h > $popup_h)	{
						$extra_style= 'style="height:500px; overflow-y:scroll; overflow-x:hidden;"' ;
					}*/

					$target = ($row_b['target']) ? $row_b['target'] : "_blank";
					$l_link = $vnT->conf['rooturl'].'?vnTRUST=mod:advertise|type:advertise|lid:'.$row_b['l_id'] ;
					$popup_banner = "<a href ='{$l_link}' target='{$target}' title='{$title}' > <img  src='{$src}' width='" . $row_b['width'] . "' alt='{$title}' /></a>";
				}
			}


			$text = '<div id="bannerPopup" > 
						<div class="bannerPopupContent" '.$extra_style.'>'.$popup_banner.'</div>				
					</div>' ;

			$arr_json['show']=1;
			$arr_json['popup_w'] = $popup_w;
			$arr_json['popup_h'] = $popup_h;
			$arr_json['html'] = $text;
		}

		$_SESSION["show_popup"]=1;
	}

	$json = new Services_JSON( );
	$textout = $json->encode($arr_json);
	return $textout;
}

//do_Statistics
function do_Statistics() {
	global $DB,$func,$conf,$vnT;
	$arr_json = array();


	$thoihan = time() - 1800;
	// so online
	$get_online = $DB->query("SELECT s_id FROM sessions WHERE time >= {$thoihan} ");
	$online = (int)$DB->num_rows($get_online);
	// He he....Cai na`y goi la` an gian ^.^
	//$randnum = rand(1,5);
	if ($vnT->conf['random_online'])
	{
		$randnum = rand(1, $vnT->conf['random_online']);
	}
	$online += $randnum;
	// so truy cap
	$totals = (int) $vnT->conf['counter_default'];
	$res_totals = $DB->query("select sum(count) as totals  from counter");
	if ($row = $DB->fetch_row($res_totals))
	{
		$totals += $row['totals'];
	}

	// so thanh vien
	$query = $DB->query("SELECT s_id FROM sessions WHERE time >= {$thoihan} and mem_id<>0 ");
	$mem_online = (int)$DB->num_rows($query);

	$arr_json['totals'] = $totals;
	$arr_json['online'] = $online;
	$arr_json['mem_online'] = $mem_online;

	$json = new Services_JSON( );
	$textout = $json->encode($arr_json);

	return $textout;
}



//do_auto_facebook
function do_auto_facebook() {
	global $DB,$func,$conf,$vnT;
	$arr_json = array();
	$ok =1;
	$mess ='';
	$sub = $_GET['sub'] ;

	$res_sn = $vnT->DB->query("SELECT * FROM social_network_setting WHERE id=1");
	if($row_sn = $vnT->DB->fetch_row($res_sn))
	{
		$user_id = $row_sn['facebook_id'];
		$access_token = $row_sn['facebook_access_token'];

		$page_id = $row_sn['fanpage_id'];
		$fanpage_access_token = $row_sn['fanpage_access_token'];
	}

	if($sub=="check"){
		if(empty($user_id) && empty($page_id)){
			$ok=0;
			$mess = 'Chưa cập nhật Uer or Page ID';
		}
		if(empty($access_token) && empty($fanpage_access_token) ){
			$ok=0;
			$mess = 'Chưa cập nhật Access Token ';
		}

	}else{

		$mod = ($_POST['mod']) ? $_POST['mod'] : "news";
		$id = (int)$_POST['id'];

		switch($mod){
			case "product" :
				// $sql =" ";
				$sql = "SELECT n.p_id, n.price, n.price_old, n.date_from, n.date_to, n.picture, nd.p_name as title, nd.friendly_url, nd.metadesc FROM products n, products_desc nd WHERE n.p_id=nd.p_id AND lang='".$vnT->lang_name."' AND n.p_id=".$id;
				break;
			default :
				$sql = "SELECT n.newsid,n.picture ,nd.title,nd.friendly_url,nd.metadesc FROM news n, news_desc nd WHERE n.newsid=nd.newsid AND lang='".$vnT->lang_name."' AND n.newsid=".$id;
				break;
		}
		$res_ck = $vnT->DB->query($sql);
		if($row_ck  = $vnT->DB->fetch_row($res_ck))
		{

			$price_item = $row_ck['price'];


			$picture = ($row_ck['picture']) ? $vnT->conf['rooturl']."vnt_upload/".$mod."/".$row_ck['picture'] : '';
			$link = $vnT->conf['rooturl'].$row_ck['friendly_url'].".html";
			$caption = $vnT->func->HTML($row_ck['title']) . ' - ' . $func->format_number($price_item)." VNĐ";;
			// $description = $vnT->func->HTML($row_ck['metadesc']);
			$description = '';

			$data = array();
			$data['picture'] = $picture;
			$data['link'] = $link ;
			$data['message'] =  $caption;
			$data['caption'] = '';
			$data['description'] = $description;

			$mess = 'Đăng thành công lên Facebook với Post ID: ';
			$ok1 = 1;	 $err1 = ''; $ok2=1 ; $err2 ='';
			if($user_id && $access_token){


				$data['access_token'] = $access_token;
				$post_url = 'https://graph.facebook.com/'.$user_id.'/feed';
				$ch = curl_init();
				curl_setopt($ch, CURLOPT_URL, $post_url);
				curl_setopt($ch, CURLOPT_POST, 1);
				curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
				$return = curl_exec($ch);
				curl_close($ch);
				$obj_value = json_decode($return,true);

				if($obj_value['id']){
					$arr_json['post_id'] = $obj_value['id'] ;
					$mess .=  $obj_value['id'].", ";
				}else{
					$ok1 = 0;
					$err1 = $obj_value['error']['message'];
				}

			}

			if($page_id && $fanpage_access_token){

				$data['access_token'] = $fanpage_access_token;
				$post_url = 'https://graph.facebook.com/'.$page_id.'/feed';
				$ch = curl_init();
				curl_setopt($ch, CURLOPT_URL, $post_url);
				curl_setopt($ch, CURLOPT_POST, 1);
				curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
				$return = curl_exec($ch);
				curl_close($ch);
				$obj_value = json_decode($return,true);

				if($obj_value['id']){
					$arr_json['post_id'] = $obj_value['id'] ;
					$mess .=  $obj_value['id'].", ";
				}else{
					$ok2 = 0;
					$err2 = $obj_value['error']['message'];
				}

			}

			if($ok1==0 && $ok2==0) {
				$ok=0;
				$mess = $err1 ." , ".$err2;
			}


		}else{
			$ok=0;
			$mess = 'Không tìm thấy ID';
		}
	}

	$arr_json['ok'] = $ok ;
	$arr_json['mess'] = $mess ;
	$json = new Services_JSON( );
	$textout = $json->encode($arr_json);

	return $textout;
}



$vnT->DB->close();

flush();
echo $jsout;
exit();
?>
