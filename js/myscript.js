$(document).ready(function () {
    new WOW().init();
    $('#home-slider').on('init', function (e, slick) {
        var $firstAnimatingElements = $('#home-slider .slider__item:first-child').find('[data-animation]');
        doAnimations($firstAnimatingElements);
    });
    $("#home-slider").slick({
        autoplay: true,
        autoplaySpeed: 5000,
        dots: true,
        fade: true,
        arrows: false
    });
    $('#home-slider').on('beforeChange', function (e, slick, currentSlide, nextSlide) {
        var $animatingElements = $('#home-slider .slider__item[data-slick-index="' + nextSlide + '"]').find('[data-animation]');
        doAnimations($animatingElements);
    });


    $('#home-slider-mobile').on('init', function (e, slick) {
        var $firstAnimatingElements = $('#home-slider-mobile .slider__item:first-child').find('[data-animation]');
        doAnimations($firstAnimatingElements);
    });
    $("#home-slider-mobile").slick({
        autoplay: true,
        autoplaySpeed: 5000,
        dots: true,
        fade: true,
        arrows: false
    });
    $('#home-slider-mobile').on('beforeChange', function (e, slick, currentSlide, nextSlide) {
        var $animatingElements = $('#home-slider-mobile .slider__item[data-slick-index="' + nextSlide + '"]').find('[data-animation]');
        doAnimations($animatingElements);
    });


    function doAnimations(elements) {
        var animationEndEvents = 'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend';
        elements.each(function () {
            var $this = $(this);
            var $animationDelay = $this.data('delay');
            var $animationType = 'animated ' + $this.data('animation');
            $this.css({
                'animation-delay': $animationDelay,
                '-webkit-animation-delay': $animationDelay
            });
            $this.addClass($animationType).one(animationEndEvents, function () {
                $this.removeClass($animationType);
            });
        });
    }
    $("#home-video-slider").slick({
        centerMode: true,
        autoplay: true,
        autoplaySpeed: 3000,
        slidesToShow: 1,
        dots: true,
        speed: 500,
        arrows: true,
        fade: false,
        responsive: [
            {
                breakpoint: 768,
                settings: {
                    centerMode: false,
                }
            },
        ]
    });
    $("#mb-banner-child").slick({
        centerMode: true,
        autoplay: true,
        autoplaySpeed: 2000,
        slidesToShow: 1,
        dots: true,
        speed: 500,
        arrows: true,
        fade: false,
        responsive: [
            {
                breakpoint: 768,
                settings: {
                    centerMode: false,
                }
            },
        ]
    });
    $('.product-thumbnails').slick({
        dots: false,
        vertical: true,
        slidesToShow: 5,
        slidesToScroll: 1,
        focusOnSelect: true,
        verticalSwiping: true,
        centerMode: true,
        centerPadding: 10,
        asNavFor: ".product-gallery",
        responsive: [
            {
                breakpoint: 992,
                settings: {
                    verticalSwiping: false,
                    vertical: false,
                    centerMode: true,
                    centerPadding: '10px',
                }
            },
        ]
    });

    $(".product-gallery").slick({
        autoplay: false,
        arrows: false,
        asNavFor: ".product-thumbnails",
        responsive: [
            {
                breakpoint: 768,
                settings: {
                    dots: true,
                }
            },
        ]
    });

    $("#slides-sonha").slick({
        slidesToShow: 1,
        dots: false,
        arrows: false,
        autoplay: true,
        autoplaySpeed: 5000,
        asNavFor: '#slides-sonha-item',
    });
    $("#slides-sonha-item").slick({
        slidesToShow: 6,
        slidesToScroll: 1,
        focusOnSelect: true,
        autoplay: true,
        arrows: true,
        asNavFor: '#slides-sonha',
        responsive: [
            {
                breakpoint: 992,
                settings: {
                    slidesToShow: 3,
                    arrows: false
                }
            },
        ]
    });

    $('.slide-featured-product-thumbnails').slick({
        dots: false,
        slidesToShow: 6,
        slidesToScroll: 1,
        focusOnSelect: true,
        arrows: true,
        centerMode: true,
        asNavFor: ".slide-featured-product"
    });

    $(".slide-featured-product").slick({
        autoplay: false,
        arrows: false,
        asNavFor: ".slide-featured-product-thumbnails"
    });

    $(".news__slides").slick({
        slidesToShow: 1,
        dots: false,
        arrows: false,
    });

    $(".related-items").slick({
        slidesToShow: 3,
        dots: false,
        arrows: true,
        responsive: [
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 2,
                    arrows: false
                }
            },
        ]
    });

    function initMap() {
        // The location of Uluru
        var uluru = { lat: -25.344, lng: 131.036 };
        // The map, centered at Uluru
        var map = new google.maps.Map(
            document.getElementById('map'), { zoom: 4, center: uluru });
        // The marker, positioned at Uluru
        var marker = new google.maps.Marker({ position: uluru, map: map });
    }

    $(".slide-news__content").slick({
        slidesToShow: 1,
        dots: false,
        arrows: false,
        // centerMode: true,
        // prevArrow: $('.arrow__left'),
        // nextArrow: $('.arrow__right'),
        autoplay: true,
        autoplaySpeed: 3000,
    });
    $(".selector-ranger").click(function () {
        $(this).addClass("active");
    });
    $(document).on("click", function (e) {
        if ($(e.target).is(".selector-ranger") === false) {
            $(".selector-ranger").removeClass("active");
        }
    });

    $(document).click(function(event){

        if (!$(event.target).hasClass('search')) {
            $("#form-search").removeClass("show-search");
            $('#search-footer').removeClass('show-search-footer')
        }
    });

    $('#search-top').click(function () {
        $('#form-search').addClass('show-search')
        $('#form-search input').focus()
    })

    $('.language-footer .search').click(function () {
        $('#search-footer').addClass('show-search-footer')
        $('#search-footer input').focus()
    })


    // $("#slider-range").slider({
    //     range: true,
    //     min: 0,
    //     max: 3500,
    //     step: 50,
    //     values: [0, 3500],
    //     slide: function (event, ui) {
    //         console.log(ui.values)
    //         $("#min-price").html(ui.values[0]);
    //         suffix = '';
    //         if (ui.values[1] == $("#max-price").data('max')) {
    //             suffix = ' +';
    //         }
    //         $("#max-price").html(ui.values[1] + suffix);
    //     }
    // });

    $('.back-to-top').each(function(){
        $(this).click(function(){
            $('html,body').animate({ scrollTop: 0 }, 800);
            return false;
        });
    });

    $('.menu-toggle').click(function () {
        $('.menu-toggle').toggleClass('open');
        $('.mb-menu-box').toggleClass('open');
    });

    $('.mb-search .search').click(function () {
        $('.mb-search').toggleClass('open');
    });

    $('.mb-search-bot .search').click(function () {
        $('.mb-search-bot').toggleClass('open');
    });
});
