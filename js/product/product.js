$(document).ready(function(){
    // FILTER
    $(".myCheckbox").click(function(){
        $(this).siblings(".myCheckbox").removeClass("active");
        $(this).siblings(".myCheckbox").find("input").prop('checked', true);;
        if(!$(this).hasClass("active")){
            $(this).addClass("active");
            $(this).find("input").prop('checked', true);
        }
        else{
            $(this).removeClass("active");
            $(this).find("input").prop('checked', false);
        }
    });
    $(".menuTab .mc-menu").click(function(){
        if(!$(this).parents(".menuTab").hasClass("active")){
            $(this).parents(".menuTab").addClass("active");
        }
        else{
            $(this).parents(".menuTab").removeClass("active");
        }
    });
    // MENU 
    $(".menuProduct>ul>li").each(function(){
        if($(this).find("ul").size()>0){
            $(this).append("<div class='sub'></div>");
        }
    });
    $(".menuProduct>ul>li>.sub").click(function(){
        $(this).parents("li").siblings().removeClass("current");
        $(this).parents("li").siblings().find(">ul").stop().slideUp(300,function(){
            $(this).css({"display":"none"});
        });
        if(!$(this).parents("li").hasClass("current")){
            $(this).parents("li").addClass("current");
            $(this).parents("li").find(">ul").stop().slideDown(300);
        }
        else{
            $(this).parents("li").removeClass("current");
            $(this).parents("li").find(">ul").stop().slideUp(300,function(){
                $(this).css({"display":"none"});
            });
        }
    });
    // PRODUCT THUMBNAIL
    $("#vnt-thumbnail-for").slick({
        arrows:false,
        slidesToShow:1,
        slidesToScroll:1,
        asNavFor:"#vnt-thumbnail-nav",
    });
    $("#vnt-thumbnail-nav").slick({
        slidesToShow:4,
        slidesToScroll:1,
        asNavFor:"#vnt-thumbnail-for",
        focusOnSelect: true,
        vertical:true,
        responsive: [
            {
                breakpoint: 680,
                settings: {
                    vertical:false,
                    arrows:false,
                }
            },
            {
                breakpoint: 420,
                settings: {
                    slidesToShow : 3,
                    vertical:false,
                    arrows:false,
                }
            },
        ]
    });
    // TAB PRODUCT
    $(".productContent .mc-tab").click(function(){
        if(!$(this).parents(".productContent").hasClass("active")){
            $(this).parents(".productContent").addClass("active");
        }
        else{
            $(this).parents(".productContent").removeClass("active");
        }
    });
    $(".productContent .tab-list li a").click(function(){
        if($(window).innerWidth()<=991){
            $(".productContent").removeClass("active");
        }
    });
    // OTHER
    $("#slideOther").slick({
        slidesToShow:4,
        autoplay:true,
        speed:991,
        arrows:false,
        responsive: [
            {
                breakpoint: 991,
                settings: {
                    slidesToShow : 3,
                }
            },
            {
                breakpoint: 768,
                settings: {
                    slidesToShow : 2,
                }
            },
        ]
    });
});