var mailfilter = /^[_\.0-9a-z-]+@([0-9a-z][0-9a-z-]+\.)+[a-z]{2,6}$/;
var numcheck = /^([0-9])+$/;
var namecheck = /^([a-zA-Z0-9_-])+$/;
var md5check = /^[a-z0-9]{32}$/;
var imgexts = /^.+\.(jpg|gif|png|bmp)$/;
var iChars = "!@#$%^&*()+=-[]\\\';,./{}|\":<>?";
var specialchars = /\$|,|@|#|~|`|\%|\*|\^|\&|\(|\)|\+|\=|\[|\-|\_|\]|\[|\}|\{|\;|\:|\'|\"|\<|\>|\?|\||\\|\!|\$|\./g
var ie45,ns6,ns4,dom;
if (navigator.appName=="Microsoft Internet Explorer") ie45=parseInt(navigator.appVersion)>=4;
else if (navigator.appName=="Netscape"){  ns6=parseInt(navigator.appVersion)>=5;  ns4=parseInt(navigator.appVersion)<5;}
dom=ie45 || ns6;

function formatStringAsUriComponent(s) {

	// replace html with whitespace
	s = s.replace(/<\/?[^>]*>/gm, " ");

	// remove entities
	s = s.replace(/&[\w]+;/g, "");

	// remove 'punctuation'
	s = s.replace(/[\.\,\"\'\?\!\;\:\#\$\%\&\(\)\*\+\-\/\<\>\=\@\[\]\\^\_\{\}\|\~]/g, "");

	// replace multiple whitespace with single whitespace
	s = s.replace(/\s{2,}/g, " ");

	// trim whitespace at start and end of title
	s = s.replace(/^\s+|\s+$/g, "");

	return s;
}
 

function is_array(mixed_var) {
	return ( mixed_var instanceof Array );
}

// strip_tags('<p>Kevin</p> <b>van</b> <i>Zonneveld</i>', '<i><b>');
function strip_tags(str, allowed_tags) {
	var key = '', allowed = false;
	var matches = [];
	var allowed_array = [];
	var allowed_tag = '';
	var i = 0;
	var k = '';
	var html = '';

	var replacer = function(search, replace, str) {
		return str.split(search).join(replace);
	}
	// Build allowes tags associative array
	if (allowed_tags) {
		allowed_array = allowed_tags.match(/([a-zA-Z0-9]+)/gi);
	}

	str += '';

	// Match tags
	matches = str.match(/(<\/?[\S][^>]*>)/gi);

	// Go through all HTML tags
	for (key in matches) {
		if (isNaN(key)) {
			// IE7 Hack
			continue;
		}

		// Save HTML tag
		html = matches[key].toString();

		// Is tag not in allowed list ? Remove from str !
		allowed = false;

		// Go through all allowed tags
		for (k in allowed_array) {
			// Init
			allowed_tag = allowed_array[k];
			i = -1;

			if (i != 0) {
				i = html.toLowerCase().indexOf('<' + allowed_tag + '>');
			}
			if (i != 0) {
				i = html.toLowerCase().indexOf('<' + allowed_tag + ' ');
			}
			if (i != 0) {
				i = html.toLowerCase().indexOf('</' + allowed_tag);
			}

			// Determine
			if (i == 0) {
				allowed = true;
				break;
			}
		}

		if (!allowed) {
			str = replacer(html, "", str);
			// Custom replace. No regexing
		}
	}

	return str;
}

// trim(' Kevin van Zonneveld ');
function trim(str, charlist) {
	var whitespace, l = 0, i = 0;
	str += '';

	if (!charlist) {
		whitespace = " \n\r\t\f\x0b\xa0\u2000\u2001\u2002\u2003\u2004\u2005\u2006\u2007\u2008\u2009\u200a\u200b\u2028\u2029\u3000";
	} else {
		charlist += '';
		whitespace = charlist.replace(/([\[\]\(\)\.\?\/\*\{\}\+\$\^\:])/g, '$1');
	}

	l = str.length;
	for ( i = 0; i < l; i++) {
		if (whitespace.indexOf(str.charAt(i)) === -1) {
			str = str.substring(i);
			break;
		}
	}

	l = str.length;
	for ( i = l - 1; i >= 0; i--) {
		if (whitespace.indexOf(str.charAt(i)) === -1) {
			str = str.substring(0, i + 1);
			break;
		}
	}

	return whitespace.indexOf(str.charAt(0)) === -1 ? str : '';
}

// rawurlencode('Kevin van Zonneveld!'); = > 'Kevin%20van%20Zonneveld%21'
function rawurlencode(str) {

	str = (str + '').toString();
	return encodeURIComponent(str).replace(/!/g, '%21').replace(/'/g, '%27').replace(/\(/g, '%5B').replace(/\)/g, '%5D').replace(/\*/g, '%2A');
}

// rawurldecode('Kevin+van+Zonneveld%21'); = > 'Kevin+van+Zonneveld!'
function rawurldecode(str) {
	return decodeURIComponent(str);
}

function is_numeric(mixed_var) {
	return ! isNaN(mixed_var);
}

function intval(mixed_var, base) {
	var type = typeof (mixed_var );

	if (type === 'boolean') {
		return (mixed_var) ? 1 : 0;
	} else if (type === 'string') {
		tmp = parseInt(mixed_var, base || 10);
		return (isNaN(tmp) || ! isFinite(tmp)) ? 0 : tmp;
	} else if (type === 'number' && isFinite(mixed_var)) {
		return Math.floor(mixed_var);
	} else {
		return 0;
	}
}

function randomNum(a) {
	for (var b = "", d = 0; d < a; d++) {
		b += "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890".charAt(Math.floor(Math.random() * 62));
	}
	return b
}
function resize_byWidth(a, b, d) {
	return Math.round(d / a * b);
}
function resize_byHeight(a, b, d) {
	return Math.round(d / b * a);
}
function calSize(a, b, d, e) {
	if (a > d) {
		b = resize_byWidth(a, b, d);
		a = d;
	}
	if (b > e) {
		a = resize_byHeight(a, b, e);
		b = e
	}
	return [a, b];
}
function calSizeMax(a, b, d, e) {
	var g = d;
	d = resize_byWidth(a, b, d);
	if (!(d <= e )) {
		d = e;
		g = resize_byHeight(a, b, e);
	}
	return [g, d];
}
function calSizeMin(a, b, d, e) {
	var g = d;
	d = resize_byWidth(a, b, d);
	if (!(d >= e )) {
		d = e;
		g = resize_byHeight(a, b, e);
	}
	return [g, d];
}
function is_numeric(a) {
	return ( typeof a === "number" || typeof a === "string" ) && a !== "" && !isNaN(a);
}
function is_num(event,f){
if (event.srcElement) {kc =  event.keyCode;} else {kc =  event.which;}
if ((kc < 47 || kc > 57) && kc != 8 && kc != 0) return false;
return true;
}
function showhide(id) {
el = document.all ? document.all[id] :   dom ? document.getElementById(id) :   document.layers[id];
els = dom ? el.style : el;
  if (dom){
    if (els.display == "none") {
        els.display = "";
      } else {
        els.display = "none";
      }
    }
  else if (ns4){
    if (els.display == "show") {
        els.display = "hide";
      } else { 
      els.display = "show";
         }
  }
}

function getobj(id) {
el = document.all ? document.all[id] :   dom ? document.getElementById(id) :   document.layers[id];

return el;
}

function showobj(id) {
obj=getobj(id);
els = dom ? obj.style : obj;
 	if (dom){
	    els.display = "";
    } else if (ns4){
        els.display = "show";
  	}
}

function hideobj(id) {
obj=getobj(id);
els = dom ? obj.style : obj;
 	if (dom){
	    els.display = "none";
    } else if (ns4){
        els.display = "hide";
  	}
}


// khong the phong to cua so
function openPopUp(url, windowName, w, h, scrollbar) {
   var winl = (screen.width - w) / 2;
   var wint = (screen.height - h) / 2;
   winprops = 'height='+h+',width='+w+',top='+wint+',left='+winl+',scrollbars='+scrollbar ;
   win = window.open(url, windowName, winprops);
   if (parseInt(navigator.appVersion) >= 4) { 
       	win.window.focus(); 
   } 
}

// co the phong to cua so
var win=null;
function NewWindow(mypage,myname,w,h,scroll,pos){
if(pos=="random"){LeftPosition=(screen.width)?Math.floor(Math.random()*(screen.width-w)):100;TopPosition=(screen.height)?Math.floor(Math.random()*((screen.height-h)-75)):100;}
if(pos=="center"){LeftPosition=(screen.width)?(screen.width-w)/2:100;TopPosition=(screen.height)?(screen.height-h)/2:100;}
else if((pos!="center" && pos!="random") || pos==null){LeftPosition=0;TopPosition=20}
settings='width='+w+',height='+h+',top='+TopPosition+',left='+LeftPosition+',scrollbars='+scroll+',location=no,directories=no,status=no,menubar=no,toolbar=no,resizable=yes';
win=window.open(mypage,myname,settings);}


function selected_item_cus(fName){	
	var f = document.getElementById(fName);
	var name_count = f.length;
	for (i=0;i<name_count;i++){
		if (f.elements[i].checked){
			return true;
		}
	}
	alert(lang_js['please_chose_item']);
	return false;
}

function do_submit_cus(action,fName) {
	
	var f = document.getElementById(fName);	
	f.do_action.value=action;
	if (selected_item_cus(fName)){
		f.submit();
	}
}

function do_movecat(action){
	  cat_chose = $('#cat_chose').val();
		if (cat_chose==0){
			alert('Vui lòng chọn danh mục cần chuyển đến');	
		}else{
			document.manage.do_action.value=cat_chose;
			if (selected_item()){
				document.manage.submit();
			}
		}
} 
	

function select_row(row_id)	{
	cur_class = document.getElementById(row_id).className;
	if (cur_class=="row_select"){
		document.getElementById(row_id).className ="row0";	
		
	}else{
		document.getElementById(row_id).className = "row_select";
	}

}

/*------------------------*/
function moveSingleElement()	{
	var items = new Array();
	for(var no=0;no<document.f_form.tid.options.length;no++){
		if(document.f_form.tid.options[no].selected){
			items[1] = document.f_form.tid.options[no].value;
			items[0] = document.f_form.tid.options[no].text;
		}
	}
	n = document.f_form.team_chose.options.length;
	document.f_form.team_chose.options[n] = new Option(items[0],items[1]);
}


/*------------------------*/
function addItems_MutiSelect(fromCtrl, toCtrl) {
var i;
var j;
var itemexists;
var nextitem;

// step through all items in fromCtrl
for (i = 0; i < fromCtrl.options.length; i++) {
 if (fromCtrl.options[i].selected) {
  // search toCtrl to see if duplicate
  j = 0;
  itemexists = false;
  while ((j < toCtrl.options.length) && (!(itemexists))) {
   if (toCtrl.options[j].value == fromCtrl.options[i].value) {
    itemexists = true;
    alert(fromCtrl.options[i].value + " found!");
   }
   j++;
  }
  if (!(itemexists)) {
   // add the item
   nextitem = toCtrl.options.length;
   toCtrl.options[nextitem] = new Option(fromCtrl.options[i].text);
   toCtrl.options[nextitem].value = fromCtrl.options[i].value;
  }
 }
}
}

function removeItems_MutiSelect(fromCtrl) {
var i = 0;
var j;
var k = 0;

while (i < (fromCtrl.options.length - k)) {
 if (fromCtrl.options[i].selected) {
  // remove the item
  for (j = i; j < (fromCtrl.options.length - 1); j++) {
   fromCtrl.options[j].text = fromCtrl.options[j+1].text;
   fromCtrl.options[j].value = fromCtrl.options[j+1].value;
   fromCtrl.options[j].selected = fromCtrl.options[j+1].selected;
  }
  k++;
 } else {
  i++;
 }
}
for (i = 0; i < k; i++) {
 fromCtrl.options[fromCtrl.options.length - 1] = null;
}
}


function edInsertContent(myField, myValue) {
	//IE support
	if (document.selection) {
		myField.focus();
		sel = document.selection.createRange();
		sel.text = myValue;
		myField.focus();
	}
	//MOZILLA/NETSCAPE support
	else if (myField.selectionStart || myField.selectionStart == '0') {
		var startPos = myField.selectionStart;
		var endPos = myField.selectionEnd;
		myField.value = myField.value.substring(0, startPos)
		              + myValue
                      + myField.value.substring(endPos, myField.value.length);
		myField.focus();
		myField.selectionStart = startPos + myValue.length;
		myField.selectionEnd = startPos + myValue.length;
	} else {
		myField.value += myValue;
		myField.focus();
	}
}

// send html to the post textbox
function send_to_textbox(ojb,text) {
	$("#"+ojb).val(text);	
	tb_remove();
}

// send html to the post textbox
function send_to_modules(ojb,text,thumb) {
	$("#ext_"+ojb).html('<img src="'+thumb+'" alt=""   /> <a href="javascript:del_picture(\''+ojb+'\')" class="del">Xóa</a>');	
	$("#"+ojb).val(text);	
	$("#btnU_"+ojb).hide();
	tb_remove();
}

// send html to the send_to_object textbox
function send_to_object(ojb,text,pic) {
	if(pic){
		$("#ext_"+ojb).html('<img src="'+pic+'" alt="" /> <a href="javascript:del_picture(\''+ojb+'\')" class="del">Xóa</a>');	
	}
	$("#"+ojb).val(text);	
	tb_remove();
}

function del_picture(ojb) {
	$("#ext_"+ojb).html('');	
	$("#"+ojb).val('');	
	$("#btnU_"+ojb).show();
}



//do_chkUpload
function do_ChoseUpload(objName,id)
{		
	$("."+objName+" :input:radio").each( function() {
 		if (id == $(this).val() ){
			$(this).attr('checked','checked');
		}																																	 
	} );	
}



jQuery(document).ready( function($) {
	// sidebar admin menu
	
	$('#menu-expand').click(function() {																 
		$( '#admin-menu li.menu-item' ).each( function() {
																				 
			submenu = $(this).find('.sub-menu');		
			submenu.hide(150);
			
			img_menu = $(this).find('.menu-toggle img');	
			img_menu.attr({ 
						src: DIR_IMAGE+"/but_cong.gif",
						title: "Expand",
						alt: "but_cong"
					})
			
		});
		
	});
	
	$('#menu-collapse').click(function() {
																	 
		$( '#admin-menu li.menu-item' ).each( function() {
			
			submenu = $(this).find('.sub-menu');		
			submenu.show(150);
			
			img_menu = $(this).find('.menu-toggle img');	
			img_menu.attr({ 
						src: DIR_IMAGE+"/but_tru.gif",
						title: "Collapse",
						alt: "but_tru"
					})
		});
		
	});
	
	
	
	$('.menu-title h2').click(function() {
		
		menu = $(this).parent().parent().parent();
		submenu = menu.find('.sub-menu');	
		submenu.slideToggle(150);
		
		img_menu = menu.find('.menu-toggle img');	
		
		if(img_menu.attr("src")==DIR_IMAGE+"/but_cong.gif")
		{
			img_menu.attr({ 
				src: DIR_IMAGE+"/but_tru.gif",
				title: "Collapse",
				alt: "but_tru"
			})
		}else{
			img_menu.attr({ 
				src: DIR_IMAGE+"/but_cong.gif",
				title: "Collapse",
				alt: "but_tru"
			})	
		}
		
	});
	
	$('.menu-toggle img').click(function() {
		
		menu = $(this).parent().parent().parent();
		submenu = menu.find('.sub-menu');	
		submenu.slideToggle(150);
		if($(this).attr("src")==DIR_IMAGE+"/but_cong.gif")
		{
			$(this).attr({ 
				src: DIR_IMAGE+"/but_tru.gif",
				title: "Collapse",
				alt: "but_tru"
			})
		}else{
			$(this).attr({ 
				src: DIR_IMAGE+"/but_cong.gif",
				title: "Collapse",
				alt: "but_tru"
			})	
		}
		
	});

	// Basic form validation
	if ( ( 'undefined' != typeof vntAjax ) && $.isFunction( vntAjax.validateForm ) ) {
		$('form.validate').submit( function() { return vntAjax.validateForm( $(this) ); } );
	}
	
	
	// check all checkboxes
	$( '.adminlist tbody :checkbox' ).click( function(e) {
		var c = $(this).attr('checked');
		var row_id = 'row_'+$(this).val();
		if (c){
			$('#'+row_id).addClass('row_select')	;
		}else{
			$('#'+row_id).removeClass('row_select')	;	
		}
		
	} );
																																		 
	$( '#checkall' ).click( function(e) {
		var c = $(this).attr('checked');
		
		$(this).parents( 'form:first' ).find( '.adminlist :checkbox' ).attr( 'checked', function() {
			var row_id = 'row_'+$(this).val();
			if (c){
				$('#'+row_id).addClass('row_select')	;
				return 'checked';
			}else{
				$('#'+row_id).removeClass('row_select')	;	
				return false;	
			}
		});
		
	} );
	
	$('.desc_title').click(function() {
			desc = $(this).parent();
			desc_content = desc.find('.desc_content');	
			desc_content.slideToggle(200);
			img = $(this).find('img');	
			if(img.attr("src")==DIR_IMAGE+"/toggle_minus.png")
			{
					img.attr({ 
							src: DIR_IMAGE+"/toggle_plus.png",
							title: "Expand",
							alt: "bt_add"
					})
			}else{
					img.attr({ 
							src: DIR_IMAGE+"/toggle_minus.png",
							title: "Collapse",
							alt: "bt_except"
					})	
			}
			
	});
	
});


// do_check
function do_check (id){
	
	$(".adminlist tbody :input:checkbox").each( function() {
		var row_id = 'row_'+$(this).val();
		if (id == $(this).val() ){
			$('#'+row_id).addClass('row_select')	;
			$(this).attr('checked','checked');
		}																																	 
	} );
}

// del_item
function del_item(theURL) {
    if (confirm(lang_js['are_you_sure_del'])) {
          window.location.href=theURL;
       }
       else {
          alert ('Phew~');
       } 
}

// selected_item
function selected_item(){
	var ok = 0 ;
	$(".adminlist tbody :input:checkbox").each( function() {
		var c = $(this).attr('checked');
		if (c){
			ok = 1;
		}																																	 
	} );
	if(ok) {
		return true;	
	}else{
		alert(lang_js['please_chose_item']);
		return false ;
	}
}
	
function del_selected(action) {
		if (selected_item()){
			question = confirm(lang_js['are_you_sure_del'])
			if (question != "0"){
				$("#manage").attr("action", action);
				$("#manage").submit();
			}else{
			  alert ('Phew~');
		    }
		}
	
}


function action_selected(action) {
	if (selected_item()){
		document.manage.action=action;
		document.manage.submit();
	}
}

function update_selected(action) {
	if (selected_item()){
		document.manage.action=action;
		document.manage.submit();
	}
}

function do_edit(action) {
		if (selected_item()){
			for ( i=0;i < document.manage.elements.length ; i++ ){
				if (document.manage.elements[i].type=="checkbox" && document.manage.elements[i].name!="all" && document.manage.elements[i].checked == true){
					id = document.manage.elements[i].value;
					break;
				}else{
					id=1;
				}	
			}
			action=action+'&id='+id;
			document.manage.action=action;
			document.manage.submit();
		}
}

function do_submit(action) {
	document.manage.do_action.value=action;
	if (selected_item()){
		document.manage.submit();
	}

} 

/*MXH*/
vnTMXH = {
 	
	setTitle:function(text){  
		var link_seo = $("#link_seo").text(); 
		$("#friendly_title").val(text);
		$("#picturedes").val(text);
		$(".title_mxh").text(text); 		
		$.ajax({ 
			dataType: 'json',
			url: "ajax.php?do=friendly_url",
			type: 'POST',
			data: 'text='+text ,
			success: function (data) {			
				 $("#friendly_url").val(data.html);
				 link_mxh =  link_seo.replace("xxx", data.html);
				 $(".link_mxh").text(link_mxh);
				 updateURL();
				 updateTitle();
			}
		});		
		
	},
	
	setFriendlyUrl:function(text){  
 		var link_seo = $("#link_seo").text(); 
		 var link_mxh =  link_seo.replace("xxx", text);
		 $(".link_mxh").text(link_mxh);	
	},
	setFriendlyTitle:function(text){   
		$(".title_mxh").text(text); 		 
	},
	setMetaDesc:function(text){  
		$(".description_mxh").text(text);  		
	},
	 
	
};  