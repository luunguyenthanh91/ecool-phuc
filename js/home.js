$(document).ready(function(){
    $('#vnt-banner').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: true,
        dots: true,
        autoplay: true,
        fade: true,
        autoplaySpeed: 5000,
        speed: 800,
        pauseOnHover: false
    });


    $(".support-hotline .div_title").click(function(){
        if(! $(this).parents(".support-hotline").hasClass("show")){
            $(this).parents(".support-hotline").addClass("show");
        }else{
            $(this).parents(".support-hotline").removeClass("show");
        }
    });
    $(window).scroll(function(){
        if($(window).scrollTop() >= 670){
            $(".support-hotline .div_title").addClass("show");
        }else{
            $(".support-hotline .div_title").removeClass("show");
            $(".support-hotline").removeClass("show");
        }
    });

    $("#contentComment").focus(function(){
      $(this).parents(".w_content").find(".content-info").stop().slideDown(700);
    });
    $("#btn-close").click(function(){
      $(this).parents(".w_content").find(".content-info").stop().slideUp(700);
    });

    $(".cat_style .cp_title").click(function(){
        if(!$(this).parents(".cat_style").hasClass("active")){
            $(this).parents(".cat_style").addClass("active");
        }
        else{
            $(this).parents(".cat_style").removeClass("active");
        }
    });
    $(window).bind('click',function(e){
        var $clicked = $(e.target);
        if(! $clicked.parents().hasClass("cat_style")){
            $(".cat_style").removeClass("active");
        }
    });

    $(".div_auto_complete .chosen-value").focus(function (e) {
        $(this).parents(".div_auto_complete").addClass("active");
        $(this).parents(".div_auto_complete").find(".dropdown_select").scrollTop(0);
    });
    $(".div_auto_complete .chosen-value").keyup(function(e){
        checkval($(this));
    });
    $(".div_auto_complete .chosen-value").blur(function (e) {
        var __this = $(this);
        setTimeout(function (e) {
            __this.parents(".div_auto_complete").removeClass("active");
            __this.parents(".div_auto_complete").find(".dropdown_select li").removeClass("closed");
        },200);
    });
    $(".div_auto_complete .dropdown_select li").click(function(e){
        $(this).parents(".div_auto_complete").find(".chosen-value").val($(this).find("> .text").html());
    });
    function checkval(_this){
        var $text = _this.val();
        if ($text.length > 0) {
            _this.parents(".div_auto_complete").find(".dropdown_select li").each(function(e){
                var $choose_text = $(this).find("> .text").html();
                if (!($text.substring(0, $text.length).toLowerCase() === $choose_text.substring(0, $text.length).toLowerCase())) {
                    $(this).addClass("closed");
                } else {
                    $(this).removeClass("closed");
                }
            });
            _this.parents(".div_auto_complete").find(".dropdown_select li").each(function(e){
                if($(this).find("li").size() != $(this).find("li.closed").size()){

                    $(this).removeClass("closed");
                }
            });
        } else {
            _this.parents(".div_auto_complete").find(".dropdown_select li").removeClass("closed");
        }
    }
});





