﻿<!-- BEGIN: body -->
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="keywords" content="eCool">
    <meta name="description" content="HTML5 eCool">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <title>e-Cool</title>
    <!-- favicon icon-->
    <link rel="shortcut icon" href="{DIR_IMAGE}/favicon.png">
    <!-- inject css start-->
    <!-- == bootstrap-->
    <link href="{DIR_STYLE}/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="{DIR_STYLE}/animate.min.css" rel="stylesheet" type="text/css">
    <link href="{DIR_STYLE}/slick.css" rel="stylesheet" type="text/css">
    <link href="{DIR_STYLE}/slick-theme.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,500,600,700&amp;display=swap&amp;subset=vietnamese" rel="stylesheet">
    <link href="{DIR_STYLE}/main.css" rel="stylesheet" type="text/css">
    <link href="https://www.w3schools.com/w3css/4/w3.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" type="text/css" href="{DIR_STYLE}/pages/index.css">
    <link rel="stylesheet" type="text/css" href="{DIR_STYLE}/pages/product-detail.css">
    <link rel="stylesheet" type="text/css" href="{DIR_STYLE}/pages/supports.css">
  </head>
  <body>
    <header>
      <div class="container">
        <nav class="w-100 navbar navbar-expand-lg navbar-light bg-white">
          <a class="navbar-brand" href="/"><img src="{DIR_IMAGE}/logo.png"></a>
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarToggler" aria-controls="navbarToggler" aria-expanded="false" aria-label="Toggle navigation"><span class="navbar-toggler-icon"></span></button>
          <div class="collapse navbar-collapse" id="navbarToggler">

            {data.menu.menutop}
           <div class="search-block"><img class="search d-none d-lg-block" src="{DIR_IMAGE}/search.png" id="search-top">
              {data.box_search.pc}
            </div>

            {data.box_lang}
          </div>
          <div class="search-block mb-search"><img class="search" src="{DIR_IMAGE}/search.png" id="search-top">
            {data.box_search.pc}
          </div>

          <span class="menu-toggle"><i class="icon-bars"></i></span>
          <div class="mb-menu-box">

            <div class="menu-list">
              {data.menu.menutop}

              <div class="lang-switch">
                <a class="language" href="/vn/trang-chu.html" title="Vietnamese">
                  <img src="/vnt_upload/lang/flag-vn.jpg" alt="vn">
                </a>
                <a class="language" href="/en/main.html" title="English">
                  <img src="/vnt_upload/lang/flag-en.jpg" alt="en">
                </a>
              </div>
            </div>
          </div>
        </nav>
      </div>
    </header>
    {PAGE_CONTENT}
    <footer>
      <section class="subscribe bg-gray">
        <div class="container text-center">
          <h3 class="font-weight-semi-bold fs-20">{LANG.global.dangkynhanmail}</h3>
          <p>{LANG.global.smallnhanmail}</p>
          <div class="row">
            <div class="col-md-8 offset-md-2">
              <form class="subscribe__form" method="post">
                <input class="subscribe__input" id="femail" pattern="/^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/" type="text" placeholder="{LANG.global.yourmail}..." required="">
                <button class="fs-20 font-weight-semi-bold text-white subscribe__submit" type="button" onclick="newsletter();">{LANG.global.xacnhan}</button>
              </form>
            </div>
          </div>
        </div>
      </section>
      <section class="menu bg-blue">
        <div class="container">
          <div class="row">
            <div class="col-6 col-lg-3">
              <div class="menu__logo"><img src="{DIR_IMAGE}/logo.png"></div>
              <p class="text-white sologan-footer hidden">
                 {LANG.global.mangthiennhien}
              </p>
            </div>
            {data.menu_footer}
            <div class="col-12 nav-bot nav-desktop">
              <div class="search-desktop search-block">
                <img class="search search_footer" src="{DIR_IMAGE}/search.png" id="search-top">
                <form action="/vn/tim-kiem.html" method="post" id="form-search" class="search_footer_form">
                  <input class="fs-11" type="text" value="" placeholder="Từ khóa" name="keyword">
                  <button type="submit" value="1"><img src="/skins/default/images/search.png"></button>
                  <input name="do_search" value="1" type="hidden">
                </form>


              </div>
              <div class="lang-switch">
                <a class="language" href="/vn/trang-chu.html" title="Vietnamese">
                  <img src="/vnt_upload/lang/flag-vn.jpg" alt="vn">
                </a>
                <a class="language" href="/en/main.html" title="English">
                  <img src="/vnt_upload/lang/flag-en.jpg" alt="en">
                </a>
              </div>
            </div>
          </div>
        </div>
      </section>
      <section class="mb-menu">
        <span class="back-to-top">Trở lại đầu trang <img class="d-lg-block" src="{DIR_IMAGE}/chevron-up.svg"></span>
        <div class="accordion mb-menu-accordion" id="mb-menu">
          <div class="card">
            <div class="card-header" id="mbmenu1">
              <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#menu1" aria-expanded="false" aria-controls="menu1">Giới thiệu</button>
            </div>

            <div id="menu1" class="collapse" aria-labelledby="mbmenu1" data-parent="#mb-menu">
              <div class="card-body">
                <ul class="mb-menu-sub">
                  <li><a href="http://ecool.phucpham.online/vn/gioi-thieu-ve-son-ha.html">Về Sơn Hà</a></li>
                  <li><a href="http://ecool.phucpham.online/vn/gioi-thieu-ve-ecool.html">Về Ecool</a></li>
                </ul>
              </div>
            </div>
          </div>
          <div class="card">
            <div class="card-header" id="mbmenu2">
              <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#menu2" aria-expanded="false" aria-controls="menu2">Sản phẩm</button>
            </div>
            <div id="menu2" class="collapse" aria-labelledby="mbmenu2" data-parent="#mb-menu">
              <div class="card-body">
                <ul class="mb-menu-sub">
                  <li><a href="http://ecool.phucpham.online/vn/dieu-hoa-inverter.html">Điều hòa Inverter</a></li>
                  <li><a href="http://ecool.phucpham.online/vn/dieu-hoa-wifi.html">Điều hòa Wifi</a></li>
                  <li><a href="http://ecool.phucpham.online/vn/dieu-hoa-1-chieu.html">Điều hòa 1 Chiều</a></li>
                  <li><a href="http://ecool.phucpham.online/vn/dieu-hoa-2-chieu.html">Điều hòa 2 Chiều</a></li>
                </ul>
              </div>
            </div>
          </div>
          <div class="card">
            <div class="card-header" id="mbmenu3">
              <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#menu3" aria-expanded="false" aria-controls="menu3">Hỗ trợ</button>
            </div>
            <div id="menu3" class="collapse" aria-labelledby="mbmenu3" data-parent="#mb-menu">
              <div class="card-body">
                <ul class="mb-menu-sub">
                  <li><a href="http://ecool.phucpham.online/vn/ho-tro.html">Trợ giúp</a></li>
                  <li><a href="/vn/tai-lieu.html">Tài liệu</a></li>
                  <li><a href="/vn/dai-ly.html">Hệ thống đại lý</a></li>
                </ul>
              </div>
            </div>
          </div>
          <div class="card">
            <div class="card-header" id="mbmenu4">
              <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#menu4" aria-expanded="false" aria-controls="menu4">Tin tức</button>
            </div>
            <div id="menu4" class="collapse" aria-labelledby="mbmenu4" data-parent="#mb-menu">
              <div class="card-body">
                <ul class="mb-menu-sub">
                  <li><a href="http://ecool.phucpham.online/vn/tin-tuc-va-su-kien.html">Tin tức và sự kiện</a></li>
                  <li><a href="http://ecool.phucpham.online/vn/goc-tu-van.html">Góc tư vấn</a></li>
                  <li><a href="http://ecool.phucpham.online/vn/chuong-trinh-uu-dai.html">Chương trình ưu đãi</a></li>
                  <li><a href="http://ecool.phucpham.online/vn/list-videos.html">Video</a></li>
                </ul>
              </div>
            </div>
          </div>
          <div class="card">
            <div class="card-header" id="mbmenu5">
              <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#menu5" aria-expanded="false" aria-controls="menu5">Liên hệ</button>
            </div>
          </div>
        </div>
        <div class="nav-bot">
          <div class="search-block mb-search-bot"><img class="search" src="{DIR_IMAGE}/search.png" id="search-top">
            {data.box_search.pc}
          </div>
          <div class="lang-switch">
            <a class="language" href="/vn/trang-chu.html" title="Vietnamese">
              <img src="/vnt_upload/lang/flag-vn.jpg" alt="vn">
            </a>
            <a class="language" href="/en/main.html" title="English">
              <img src="/vnt_upload/lang/flag-en.jpg" alt="en">
            </a>
          </div>
        </div>
      </section>
      <section class="copyright bg-black">
        <div class="mb-hide">{data.mangxahoi}</div>
        <div class="copyright__info">
          {data.content_footer}
        </div>
        <div class="mb-show social">
          <div class="text-white">Kết nối với chúng tôi</div>
          {data.mangxahoi}
        </div>
      </section>
    </footer>
    <!-- == jquery-->
    <script src="{DIR_JS}/jquery.min.js"></script>
    <!-- == bootstrap-->
    <script src="{DIR_JS}/bootstrap.min.js"></script>
    <!-- == slick slide-->
    <script src="{DIR_JS}/slick.js"></script>
    <script src="{DIR_JS}/wow.min.js"></script>
    <script src="{DIR_JS}/myscript.js"></script>
    <script type="text/javascript" src="{DIR_JS}/style.js"></script>
    <script>
        $( document ).ready(function() {
            $(".search_footer").on("click",function(){
                $(".search_footer_form").attr("style","display  : block");
            });
        });

    </script>
  </body>
</html>

<!-- END: body -->
