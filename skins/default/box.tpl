<!-- BEGIN: box -->
<div class="box">
	 <div class="box-title"><div class="fTitle"> {data.f_title}</div></div>
  <div class="box-content">
    {data.content}
  </div>
</div>
<!-- END: box -->

<!-- BEGIN: box_left -->
<div class="box">
	 <div class="box-title"><div class="fTitle"> {data.f_title}</div></div>
  <div class="box-content">
    {data.content}
  </div>
</div>
<!-- END: box_left -->

<!-- BEGIN: box_middle -->
<div class="box_mid">
  <div class="mid-title">
    <div class="titleL">{data.f_title}</div>
    <div class="titleR">{data.more}</div>
    <div class="clear"></div>
  </div>
  <div class="mid-content">
    {data.content}
  </div>
</div>
<!-- END: box_middle -->

<!-- BEGIN: box_middle2 -->
<div class="box_mid">
  <div class="mid-title">
    <div class="titleL">{data.f_title}</div>
    <div class="clear"></div>
  </div>
  <div class="mid-content">
    {data.content}
  </div>
</div>
<!-- END: box_middle2 -->

<!-- BEGIN: box_right -->
<div class="box">
	 <div class="box-title"><div class="fTitle"> {data.f_title}</div></div>
  <div class="box-content">
    {data.content}
  </div>
</div>
<!-- END: box_right -->

<!-- BEGIN: advertise -->
<!-- BEGIN: html_item --><div class="advertise"><a href="{row.link}" title="{row.title}"  onmousedown="return rwt(this,'advertise','{row.l_id}')" target='{row.target}'><img src="{row.src}" alt="{row.title}" /></a></div><!-- END: html_item -->
<!-- END: advertise -->

<!-- BEGIN: box_search -->
<script language="javascript" >
  function check_search(f){
    var key_default = "{LANG.global.keyword_default}";
    var keyword = f.keyword.value;
    var key_len = f.keyword.value.length;
    console.log($(f),f);
    if(! $(f).find(".wrap-form-search").hasClass("active")){
        $(f).find(".wrap-form-search").addClass("active");
        $(".vnt-menutop").addClass("visible-hidden");
        return false;
    }else{
      if( (keyword==key_default) || (keyword=='') )   {
        alert("{LANG.global.key_search_empty}");
        f.keyword.focus();
        return false;
      }
      if( key_len<2 ){
        alert("{LANG.global.key_search_invalid}");
        f.keyword.focus();
        return false;
      }
      return true;
    }
  }
</script>

<form action="{data.link_search}"  method="post" id="form-search" class="">
  <input class="fs-11" type="text" value="{data.keyword}" placeholder="{LANG.global.keyword_default}" name="keyword">
  <button type="submit" value="1"><img src="{DIR_IMAGE}/search.png"></button>
  <input name="do_search" value="1" type="hidden">
</form>
<!-- END: box_search -->

<!-- BEGIN: box_search_mb -->
<script language="javascript" >
  function search_mb(f){
    var key_default = "{LANG.global.keyword_default}";
    var keyword = f.keyword.value;
    var key_len = f.keyword.value.length;
    if( (keyword==key_default) || (keyword=='') ) {
      alert("{LANG.global.key_search_empty}");
      f.keyword.focus();
      return false;
    }
    if( key_len<2 ){
      alert("{LANG.global.key_search_invalid}");
      f.keyword.focus();
      return false;
    }
    return true;
  }
</script>
<div class="formSearch hidden-lg hidden-md">
  <form name="formSearch" method="post" action="{data.link_search}" onSubmit="return search_mb(this);">
    <input name="keyword" type="text" value="{data.keyword}" placeholder="{LANG.global.keyword_default}"/>
    <button name="btn-search" type="submit" value="1"><i class="fa fa-search"></i></button>
    <input name="do_search" value="1" type="hidden">
  </form>
</div>
<!-- END: box_search_mb -->

<!-- BEGIN: box_search_side -->
<form name="formSearch" method="post" action="{data.link_search}" onSubmit="return search_mb(this);">
  <span class="close-search"><i class="fa fa-close"></i></span>
  <input name="keyword" type="text" value="{data.keyword}" class="text_search" placeholder="{LANG.global.keyword_default}"/>
  <button name="btn-search" type="submit" class="btnSubmit" value="1"><i class="fa fa-search"></i></button>
  <input name="do_search" value="1" type="hidden">
</form>
<!-- END: box_search_side -->

<!-- BEGIN: box_redirect -->
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr">
<head>
<title>{data.mess}</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta http-equiv='refresh' content='{data.time_ref}; url={data.url}' />
<link href="{DIR_STYLE}/screen.css" rel="stylesheet" type="text/css">
</head>
<body>
<div style="overflow:hidden; margin:200px auto;" >
  <div id="box_redirect">
  	<div class="top"><img src="{DIR_IMAGE}/thongbao.gif" width="32" height="22" align="absmiddle" />&nbsp;{LANG.global.announce}</div>
    <div class="middle" >
    	<p class="fontMess" >{data.mess}</p>
        <p style="text-align:center"><img src="{DIR_IMAGE}/loading.gif" width="78" height="7" /></p>
        <p class="font_err" style="text-align:center">({data.mess_redirect})</p>
    </div>
    <div class="bottom">.::[ {LANG.global.copyright} ]::.</div>
  </div>
</div>
</body>
</html>
<!-- END: box_redirect -->

<!-- BEGIN: box_fixed -->
<div id="wrapMenuTools" class="hidden-sm hidden-xs">
  <div id="menuTools">
    <div class="menuOnTools">
      <div class="icon_menu"><span class="style_menu"></span>Menu</div>
      <div class="popup">
        {data.menutop}
      </div>
    </div>
    <div class="searchOnTools">
      {data.fixed_search}
    </div>
    <div class="hotlineOnTools">
      <div class="icon"><i class="fa fa-phone"></i></div>
      <div class="popup">
        <div class="title">{LANG.global.support_hotline}</div>
        <div class="hotline"><a href="tel:{CONF.hotline}"><span>{CONF.hotline}</span></a></div>
        <div class="hotline"><a href="tel:{CONF.hotline2}"><span>{CONF.hotline2}</span></a></div>
      </div>
    </div>
    {data.link_map}
    {data.surveymenu}
    <div class="socialOnTools">
      {data.social_icon}
    </div>
  </div>
</div>
<!-- END: box_fixed -->

<!-- BEGIN: box_project_filter -->
<div class="col">
  <div class="filter">
    <div class="txt">{data.f_title}</div>
    <div class="formFa">
      {data.content}
    </div>
  </div>
</div>
<!-- END: box_project_filter -->

<!-- BEGIN: box_select_j -->
<div class="wrapper hidden-lg hidden-md">
  <div class="select-j">
    <div class="title">{data.f_title}</div>
    <div class="content">
      {data.content}
    </div>
  </div>
</div>
<!-- END: box_select_j -->

<!-- BEGIN: box_select_filter -->
<div class="col">
  <div class="filter">
    <div class="txt">{data.f_title}</div>
    <div class="icon">{data.cur_title}</div>
    <div class="popup">
      {data.content}
    </div>
  </div>
</div>
<!-- END: box_select_filter -->

<!-- BEGIN: not_member_pc -->
<div class="icon"><i class="fa fa-user"></i> {LANG.global.member}</div>
<div class="popup">
  <div class="listNot">
    <ul>
      <li><a href="{data.register_link}"><span>{LANG.global.register}</span></a></li>
      <li><a href="{data.login_link}"><span>{LANG.global.login}</span></a></li>
    </ul>
  </div>
</div>
<!-- END: not_member_pc -->

<!-- BEGIN: not_member_mb -->
<div class="listNot">
  <ul>
    <li><a href="{data.login_link}"><span>{LANG.global.login}</span></a></li>
    <li><a href="{data.register_link}"><span>{LANG.global.register}</span></a></li>
  </ul>
</div>
<!-- END: not_member_mb -->

<!-- BEGIN: is_member_pc -->
<div class="icon ok">{LANG.global.hello} <span>{data.full_name}</span></div>
<div class="popup">
  <div class="mc-account">
    <div class="avatar">
      <div class="img">{data.pic}</div>
      <a href="{data.manager_link}" class="change-avatar">{LANG.global.change_img}</a>
    </div>
    <div class="info">
      <div class="title">{data.full_name}</div>
      <ul>
        <li><a href="{data.manager_link}">{LANG.global.account_information}</a></li>
        <li><a href="{data.order_link}">{LANG.global.your_order}</a></li>
      </ul>
    </div>
    <div class="clear"></div>
  </div>
  <div class="mc-button">
    <a href="{data.changepass_link}" class="fl btn-mc">{LANG.global.change_pass}</a>
    <a href="{data.logout_link}" class="fr btn-mc">{LANG.global.logout}</a>
    <div class="clear"></div>
  </div>
</div>
<!-- END: is_member_pc -->

<!-- BEGIN: is_member_mb -->
<div class="icon">{LANG.global.hello}, <strong>{data.full_name}</strong></div>
<div class="popup">
  <div class="mc-account">
    <div class="avatar">
      <div class="img">{data.pic}</div>
      <a href="{data.manager_link}" class="change-avatar">{LANG.global.change_img}</a>
    </div>
    <div class="info">
      <div class="title">Nguyễn Anh</div>
      <ul>
        <li><a href="{data.manager_link}">{LANG.global.account_information}</a></li>
        <li><a href="{data.order_link}">{LANG.global.your_order}</a></li>
      </ul>
    </div>
    <div class="clear"></div>
  </div>
  <div class="mc-button">
    <a href="{data.changepass_link}" class="fl btn-mc">{LANG.global.change_pass}</a>
    <a href="{data.logout_link}" class="fr btn-mc">{LANG.global.logout}</a>
    <div class="clear"></div>
  </div>
</div>
<!-- END: is_member_mb -->