﻿<!-- BEGIN: body --><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="{data.meta_lang}" xmlns="http://www.w3.org/1999/xhtml" xmlns:fb="http://www.facebook.com/2008/fbml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset={CONF.charset}"/>
<meta name="viewport" content="width=device-width, initial-scale=1" />
<title>{CONF.indextitle}</title>
<meta name="keywords" CONTENT="{CONF.meta_keyword}"/>
<meta name="description" CONTENT="{CONF.meta_description}"/>
<meta name="robots" content="index, follow"/>
{CONF.meta_extra}
<link rel="SHORTCUT ICON" href="{CONF.favicon}" type="image/x-icon"/>
<link rel="icon" href="{CONF.favicon}" type="image/gif"/>


<link href="{DIR_JS}/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
<link href="{DIR_SKIN}/fontawesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
<link href="{DIR_JS}/fancybox/jquery.fancybox.css" rel="stylesheet" type="text/css"/>
<link href="{DIR_STYLE}/screen.css" rel="stylesheet" type="text/css"/>
<link href="{DIR_JS}/slideSlick/css/slick.css" type="text/css" rel="stylesheet" />
<link href="{DIR_JS}/slideSlick/css/slick-theme.css" type="text/css" rel="stylesheet" />
<link href="{DIR_JS}/tooltipster/css/tooltipster.css" type="text/css" rel="stylesheet" />

<script language="javascript">
    var ROOT = "{CONF.rooturl}"; var ROOT_MOD = "{data.link_mod}"; var DIR_IMAGE = "{DIR_IMAGE}";
    var ROOT_PRO = "{data.link_pro}"; var ROOT_MEM = "{data.link_member}";
    var cmd= "{CONF.cmd}"; var lang = "{data.lang}"; var mem_id = {data.mem_id}; var js_lang = new Array();
    js_lang['announce'] ="{LANG.global.announce}"; js_lang['error'] = "{LANG.global.error}";
</script>
<script type="text/javascript" src="{DIR_JS}/jquery.min.js"></script>
<script type="text/javascript" src="{DIR_JS}/jquery-migrate.min.js"></script>
<script type="text/javascript" src="{DIR_JS}/bootstrap/js/bootstrap.min.js"></script>
<script type="text/javascript" src="{DIR_JS}/fancybox/jquery.fancybox.js"></script>
<script type="text/javascript" src="{DIR_JS}/core.js"></script>
<script type="text/javascript" src="{DIR_JS}/slideSlick/js/slick.js"></script>
<script type="text/javascript" src="{DIR_JS}/style.js"></script>
<script type="text/javascript" src="{DIR_JS}/tooltipster/js/jquery.tooltipster.min.js"></script>
<script type="text/javascript" src="{DIR_JS}/home.js"></script>
<!--===LAZYLOADING===-->
<link href="{DIR_STYLE}/animate.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="{DIR_JS}/lazingloading/lazingloading.js"></script>
    <script type="text/javascript" src="{DIR_JS}/lazingloading/my_effect_scroll.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/wow/1.1.2/wow.min.js"></script>
{EXT_HEAD}

</head>
<body>
    <div id="vnt-wrapper">
        <div id="vnt-container">
            <!--==BEGIN - HEADER==-->
            <div id="vnt-header">
      <div class="vnt-header">
        <div class="wrapper">
          <div class="main-header">
            <div class="logo"><h1><a href='/' target='_self' title=''>
               {data.logo.header}
            </a></h1></div>
            <div class="tools_top">
              <div class="hotLine_top">Hotline: <strong>{CONF.hotline}</strong></div>
              <div class="logIn"><a href="member/login.html">Đăng nhập</a></div>
                        <div class="signUp"><a href="member/register.html">Đăng ký </a></div>
              <div class="social_top">
                <ul><li><a href="https://www.facebook.com/bichvan.tranthi.50159" title="Facebook" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a></li><li><a href="http://plus.google.com/" title="Google" target="_blank"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li><li><a href="http://twitter.com/" title="Twitter" target="_blank"><i class="fa fa-twitter" aria-hidden="true"></i></a></li><li><a href="http://youtube.com/" title="Youtube" target="_blank"><i class="fa fa-youtube-play" aria-hidden="true"></i></a></li></ul>
              </div>
              
              <div class="cartTop"><a href="{data.link_cart}" title="Giỏ hàng">
                  <img src="{DIR_JS}/icon_cart.png" alt=""><span>0</span>
                </a></div>
              <div class="menu_mobile">
                <div class="icon_menu"><span class="style_icon"></span>Menu</div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="vnt-banner">
        {data.banner}


      <div class="main_menu">
        <div class="wrapper">
          <div class="vnt_menu">
              {data.menu.menutop}
              <div class="clear"></div>
          </div>
        </div>
      </div>
    </div>
            <!--==END - HEADER==-->
            <!--==BEGIN - CONTENT==-->
            {PAGE_CONTENT}
            <!--==END - CONTENT==-->
            <!--==BEGIN - FOOTER==-->
            <div id="vnt-footer">
      <div class="form_footer">
        <div class="wrapper">
          <div class="text">đăng ký nhận tin mới nhất từ Berry SkinCare</div>
          <form name="formNewsletter" id="formNewsletter" method="post" onsubmit="return registerMaillist(this);">
            <input type="text" id="femail" name="femail" placeholder="Nhập email của bạn ..." class="form-control"/>
            <button type="submit" value="1">Gửi</button>
          </form>
        </div>
      </div>

      <div class="bottom-footer">
        <div class="wrapper">
          <div class="fl">
            <div class="logo_footer"><a href="/" title="Mỹ phẩm Berry">{data.logo.footer} </a></div>
            {data.address_footer}
          </div>
          <div class="fr">
            <div class="bocongthuong">
              <div class="b-img"><a href="http://online.gov.vn/HomePage/CustomWebsiteDisplay.aspx?DocId=42904"><img alt="bocongthuong" src="vnt_upload/File/02_2018/bocongthuong.png" /></a></div>
            </div>
            <div class="social-ft">
              <ul><li data-color="#3b5998"><a href="https://www.facebook.com/" title="Facebook" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a></li><li data-color="#ec4235"><a href="http://plus.google.com/" title="Google" target="_blank"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li><li data-color="#53adee"><a href="http://twitter.com/" title="Twitter" target="_blank"><i class="fa fa-twitter" aria-hidden="true"></i></a></li><li data-color="#ee1c1b"><a href="http://youtube.com/" title="Youtube" target="_blank"><i class="fa fa-youtube-play" aria-hidden="true"></i></a></li></ul>
            </div>
          </div>
          <div class="clear"></div>
        </div>
      </div>
      <div class="tools_footer">
        <ul>
          <li><a href="tel:0938 172 517 - 09 8888 0404"><span class="fa-phone">Hotline</span></a></li>
          <li><a href="#"><span class="fa-map-marker">Xem bản đồ</span></a></li>
        </ul>
      </div>
    </div>
    <div class="menu_mobile">
      <div class="divmm">
        <div class="mmContent">
          <div class="mmMenu">
            <ul><li class="current"><a href="index.html">Trang chủ <span class="homeM"><i class="fa fa-home" aria-hidden="true"></i></span></a></li><li ><a href="gioi-thieu.html" title="Giới thiệu" target="_self">Giới thiệu</a></li><li ><a href="thuong-hieu.html" title="Thương hiệu" target="_self">Thương hiệu</a><ul><li><a href="saroma.html" target="_self">Saroma</a></li></li><li><a href="pattrena.html" target="_self">Pattrena</a></li></li><li><a href="ling.html" target="_self">Ling</a></li></li><li><a href="klapp.html" target="_self">Klapp</a></li></li><li><a href="404.html" target="_self">MCL</a></li></li></ul></li><li ><a href="san-pham.html" title="Sản phẩm" target="_self">Sản phẩm</a><ul><li><a href="tinh-dau-thien-nhien.html" target="_self">Tinh dầu thiên nhiên</a></li></li><li><a href="cham-soc-toc-2.html" target="_self">Chăm Sóc Tóc</a></li></li><li><a href="cham-soc-co-the.html" target="_self">Chăm Sóc Cơ Thể</a></li></li><li><a href="cham-soc-da.html" target="_self">Chăm Sóc Da</a></li></li><li><a href="spa-and-home-spa.html" target="_self">Spa &amp; Home Spa</a></li></li><li><a href="den-xong-tinh-dau.html" target="_self">Đèn Xông Tinh Dầu</a></li></li></ul></li><li ><a href="dich-vu-cham-soc.html" title="Dịch vụ chăm sóc" target="_self">Dịch vụ chăm sóc</a></li><li ><a href="e-catalogues.html" title="Tạp chí Beauty" target="_self">Tạp chí Beauty</a></li><li ><a href="he-thong-phan-phoi.html" title="Hệ thống phân phối" target="_self">Hệ thống phân phối</a></li><li ><a href="tin-tuc.html" title="Tin tức" target="_self">Tin tức</a></li><li ><a href="lien-he.html" title="Liên hệ" target="_self">Liên hệ</a></li></ul>
            <div class="mmcol">
              <div class="sub">
                <div class="fl"><a href="member/login.html">Đăng nhập </a></div>
                        <div class="fr"><a href="member/register.html"> Đăng ký</a></div>
                <div class="clear"></div>
              </div>
            </div>
            <div class="mmSearch">
              
<script language="javascript" >
    function check_search(f){
        var key_default = "Từ khóa" ;
        var keyword = f.keyword.value;      
        var key_len = f.keyword.value.length;
        if( (keyword==key_default) || (keyword=='') )       {
            alert("Vui lòng nhập từ khóa");
            f.keyword.focus();
            return false;
        }
        if( key_len<2 ){
            alert("Từ khóa tới thiểu là 2 ký tự");
            f.keyword.focus();
            return false;
        } 
        return true;
    }
</script>
<form name="formSearch" method="post" action="http://mcwaybeauty.vn/tim-kiem.html" onSubmit="return check_search(this);" class="box_search">
  <input name="do_search" value="1" type="hidden" />
  <input name="keyword" type="text" class="form-control" value="" placeholder="Từ khóa"/>
  <button name="btn-search" type="submit" class="btn" value="1"><i class="fa fa-search"></i></button>
</form>

            </div>
          </div>
          <div class="close-mmenu">
            <div class="icon_menu_close"><img src="skins/default/images/close_menu.jpg" alt="Close"></div>
          </div>
        </div>
        <div class="divmmbg"></div>
      </div>
    </div>
    <div id="maskmenu" class=""></div>
    <div class="support-hotline">
      <div class="div_title">
        <span class="icon"><i class="fa fa-phone" aria-hidden="true"></i></span>
      </div>
      <div class="div_content">
        <div class="box-content">
<div class="box-left">
<h2>Hotline 1 - Kinh Doanh Online</h2>
<strong>0937 166 090</strong></div>

<div class="box-right">
<ul>
    <li><a href="tel:02838724754"><img alt="" src="vnt_upload/File/02_2018/viper.png" /></a></li>
    <li><a href="tel:02838724754"><img alt="" src="vnt_upload/File/02_2018/zalo2.png" /></a></li>
    <li><a href="skype:nick_skype?call"><img alt="" src="vnt_upload/File/02_2018/skyper.png" /></a></li>
</ul>

<div class="clear">&nbsp;</div>
</div>

<div class="clear">&nbsp;</div>
</div>

<div class="box-content">
<div class="box-left">
<h2>Hotline 2 - CSKH</h2>
<strong>0938 172 517</strong></div>

<div class="box-right">
<ul>
    <li><a href="tel:02838724754"><img alt="" src="vnt_upload/File/02_2018/viper.png" /></a></li>
    <li><a href="tel:0938 172 517"><img alt="" src="vnt_upload/File/02_2018/zalo2.png" /></a></li>
    <li><a href="skype:nick_skype?call"><img alt="" src="vnt_upload/File/02_2018/skyper.png" /></a></li>
</ul>

<div class="clear">&nbsp;</div>
</div>

<div class="clear">&nbsp;</div>
</div>

      </div>
    </div>
  </div>
</div>


{CONF.extra_footer}

<script type="text/javascript" src="https://apis.google.com/js/plusone.js"></script>
</body>
</html>
<!-- END: body -->