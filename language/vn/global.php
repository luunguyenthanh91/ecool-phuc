<?php 
/**
* @Name: lang global	
* @version : 1.0
* @date upgrade : 11/12/2007 by Thai son
**/
if ( !defined('IN_vnT') )	{ die('Access denied');	} 
$lang = array ( 
	about	 => 	'Giới thiệu' ,
	account_information	 => 	'Thông tin tài khoản' ,
	announce	 => 	'Thông báo' ,
	btn_search	 => 	'Tìm' ,
	cart	 => 	'Giỏ hàng' ,
	change_img	 => 	'Đổi hình' ,
	change_pass	 => 	'Đổi mật khẩu' ,
	contact	 => 	'Liên hệ - góp ý' ,
	copyright	 => 	'Copyright © 2020 <strong>Công Ty SOn Ha</strong>' ,
	custom	 => 	'khảo</br>sát' ,
	error	 => 	'Báo lỗi' ,
	faqs	 => 	'' ,
	find_us	 => 	'TÌM CHÚNG TÔI TẠI' ,
	gpkd	 => 	'GPĐKKD số 123123213213 do Sở KHĐT TP.HCM cấp ngày 23/08/2006' ,
	hello	 => 	'Xin chào' ,
	homepage	 => 	'Trang chủ' ,
	hotline	 => 	'Hotline' ,
	key_search_empty	 => 	'Vui lòng nhập từ khóa' ,
	key_search_invalid	 => 	'Từ khóa tới thiểu là 2 ký tự' ,
	keyword_default	 => 	'Từ khóa' ,
	login	 => 	'Đăng nhập' ,
	logout	 => 	'Đăng xuất' ,
	member	 => 	'Thành viên' ,
	mess_redirect	 => 	'Vui lòng đợi hoặc click vào đây' ,
	news	 => 	'Tin tức nổi bật' ,
	online	 => 	'Online' ,
	pages	 => 	'Trang' ,
	product	 => 	'Sản phẩm' ,
	register	 => 	'Đăng ký' ,
	search	 => 	'Tìm kiếm' ,
	select_city	 => 	'-- Chọn tỉnh thành --' ,
	select_state	 => 	'-- Chọn quận huyện --' ,
	service	 => 	'Dịch vụ' ,
	showroom	 => 	'VR showroom' ,
	support_hotline	 => 	'Hotline Hỗ Trợ' ,
	vistited	 => 	'Lượt truy cập' ,
	your_order	 => 	'Quản lý đơn hàng' ,
	mangthiennhien => 'MANG THIÊN NHIÊN ĐẾN NGÔI NHÀ BẠN',
	dangkynhanmail => 'Đăng ký nhận thông tin khuyến mại từ Ecool',
	smallnhanmail => 'Tất cả thông tin về chương trình khuyến mại, chăm sóc khách hàng, sự kiện, hội thảo sẽ được gửi đến Email của bạn.',
	yourmail => 'Địa chỉ email của bạn',
	xacnhan => 'Xác nhận',
	xemtttt => 'Xem tất cả tin tức',
); 
?>
