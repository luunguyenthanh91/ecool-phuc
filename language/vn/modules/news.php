<?php 
/**
* @Name: lang news
* @version : 1.0
* @date upgrade : 29/12/2007 by Thai son
**/
if ( !defined('IN_vnT') )	{ die('Access denied');	} 
$lang = array ( 
	back	 => 	'Quay lại' ,
	cat_news	 => 	'Danh Mục' ,
	category	 => 	'Chuyên mục' ,
	all => 'Tất cả',
	view_by_date => 'XEM THEO NGÀY',
	date_post	 => 	'Ngày' ,
	f_navation	 => 	'Tin tức' ,
	f_title_send_email	 => 	'GỬI BÀI VIẾT CHO BẠN BÈ QUA EMAIL' ,
	feedback	 => 	'Viết phản hồi' ,
	focus_news	 => 	'Có thể bạn quan tâm' ,
	news_new	=>	'Tin tức',
	friday	 => 	'Thứ sáu' ,
	read_more => 'Đọc thêm',
	keyword	 => 	'Tiêu đề' ,
	monday	 => 	'Thứ hai' ,
	news	 => 	'Tin tức' ,
	no_have_news	 => 	'Không có bài viết phù hợp' ,
	not_found_cat	 => 	'Không tìm thấy dang mục' ,
	not_found_news	 => 	'Không tìm thấy tin' ,
	other_news	 => 	'Các tin khác' ,
	post_by	 => 	'Người viết' ,
	print_new	 => 	'In bài viết' ,
	reference_news	 => 	'Các tin liên quan' ,
	saturday	 => 	'Thứ bảy' ,
	search	 => 	'Tìm' ,
	search_date	 => 	'Tìm theo ngày' ,
	search_not_found	 => 	'Không tìm thấy tin nào thỏa yêu cầu' ,
	send_news	 => 	'Gửi cho bạn bè' ,
	sunday	 => 	'Chủ nhật' ,
	thursday	 => 	'Thứ năm' ,
	tuesday	 => 	'Thứ ba' ,
	view_all	 => 	'{Xem hết}' ,
	view_detail => '[— Xem]',
	wednesday	 => 	'Thứ tư' ,
	back	=>	'Quay lại',
	save_news	=>	'Lưu tin',
	best_view	=>	'Tin xem nhiều',
	rss_home	=>	'Tin tức mới ',
	f_tags	=>	'Từ khóa',
	
	//COMMENT 
	write_comment	=>	'Viết đánh giá',
	title	=>	'Tiêu đề',
	hidden_email	=>	'Ẩn email của tôi',
	err_name_empty	=>	'Vui lòng nhập họ tên',
	err_email_empty	=>	'Vui lòng nhập Email',	
	err_email_invalid	=>	'Email không hợp lệ',	
	err_title_empty	=>	'Vui lòng nhập tiêu đề',	
	err_content_comment_empty	=>	'Vui lòng nhập nội dung đánh giá',
	err_mark_empty	=>	'Vui lòng cho điểm đánh giá',
	post_at	=>	'Gửi vào lúc',
	vietkey	=>	'Kiểu gõ Tiếng Việt',
	security_code	=>	'Mã bảo vệ',
	err_security_code_empty	=>	'Vui lòng nhập mã bảo vệ',
	security_code_invalid	=>	'Mã bảo vệ không đúng',
	err_length	=>	'Độ dài tối thiểu là ',
	char	=>	'ký tự',
	go_login	=>	'Bạn chưa đăng nhập, bạn có muốn đăng nhập không?',
	err_conntent_minchar	=>	'Nội dung đánh giá phải có ít nhất là 10 ký tự!',
	err_conntent_maxchar	=>	'Nội dung đánh giá không vượt quá 200 ký tự, cám ơn!', 
	vietkey	=>	'Bộ gõ',
	
	
	/*TELL FRIEND*/
	f_intro_to_friend	 => 	'GIỚI THIỆU TIN TỨC CHO BẠN BÈ' ,
	send_email_success	 => 	'Email giới thiệu của bạn đã được gửi đi' ,
	
	name_from	 => 	'Tên người gửi' ,
	email_from	 => 	'Email người gửi' ,	
	name_to	 => 	'Tên người nhận' ,
	email_to	 => 	'Email người nhận' ,
	message	 => 	'Lời nhắn' ,
	subject_send_news	=>	'Giới thiệu tin tức',
	
	err_name_to_empty	=>	'Vui lòng nhập tên người nhận',
	err_email_to_empty	=>	'Vui lòng nhập email người gửi',
	err_name_from_empty	=>	'Vui lòng nhập tên người nhận', 
	err_email_from_empty	=>	'Vui lòng nhập email người nhận',
	err_content_empty	=>	'Vui lòng nhập lời nhắn',
	err_security_code_invalid	=>	'Mã bảo vệ không chính xác',
	
	btn_send	=>	'Gửi',
	btn_reset	=>	'Hủy bỏ',
	
	
	f_search	=>	'Kết quả tìm kiếm',
	no_have_result	=>	'Không tìm thấy kết quả nào',
	keyword	=>	'Từ cần tìm',
	category	=>	'Danh mục',
	select_category	=>	'--- Chọn danh mục ---'	,
	advanced_search	=>	'Tìm kiếm nâng cao',
	search_by	=>	'Tên kiếm theo',
	note_key_search	=>	'Kết quả tìm kiếm <b class=font_err>tất cả</b> với từ khóa  {keyword} ',
	note_keyword	=>	'Kết quả tìm kiếm với từ khóa  {keyword} ',
	note_result => 'Tổng cộng tìm thấy {totals} tin trong {num_pages} trang',
	btn_search	=>	'Tìm',
	no_accurate	=>	'Tìm gần đúng',
	accurate => 'Tìm chính xác cụm từ',
	search_date	=>	'Giới hạn tin theo ngày',
	s_form => 'từ', 
	s_to	=>	'đến',
	search	=>	'Tìm kiếm',
	keyword_default	=>	'Từ khóa',
	key_search_invalid	=> 'Từ khóa tới thiểu là 1 ký tự',
	key_search_empty	=>	'Vui lòng nhập từ khóa',
	f_comment	=>	'Ý kiến của bạn',
	your_name	=>	'Họ và tên',
	error_comment_failt	 => 	'Viết nhận xét thất bại . Thử lại lần nữa' ,
	comment_default => 'Nhập ý kiến hoặc câu hỏi của bạn tại đây',
	reply_default => 'Nhập câu trả lời của bạn',
	enter_code => 'Nhập mã bảo vệ',
	enter_info_comment => 'Nhập thông tin của bạn',
	reply => 'Trả lời',
	btn_send_comment	 => 	'Gửi' ,
	btn_close => 'Đóng',
	send_comment_success	 => 	'Viết đánh giá thành công . Vui lòng đợi xét duyệt của BQT' ,
	send_reply_success => 'Gửi câu trả lời thành công . Vui lòng đợi xét duyệt của BQT' ,
	
); 
?>
