<?php 
/**
* @Name: lang main
* @version : 1.0
* @date upgrade : 29/12/2007 by Thai son
**/
if ( !defined('IN_vnT') )	{ die('Access denied');	} 
$lang = array ( 
	collection	 => 	'Bộ sưu tập' ,
	collection_slogan	 => 	'Sáng tạo, trẻ trung và đa dạng trong từng thiết kế' ,
	f_navation	 => 	'Trang chủ' ,
	homepage	 => 	'Trang chủ' ,
	maso	 => 	'Mã' ,
	news	 => 	'tin tức' ,
	news_slogan	 => 	'Hữu ích - chính xác' ,
	out_stock	 => 	'Hết hàng' ,
	price_default	 => 	'Liên hệ' ,
	product	 => 	'Sản phẩm nổi bật' ,
	product_desc	 => 	'Your Home, Your Design' ,
	project	 => 	'Các dự án thi công nội thất' ,
	project_desc	 => 	'Dự án' ,
	project_slogan	 => 	'Đẹp và khác biệt' ,
	underconstruction	 => 	'ĐANG XÂY DỰNG' ,
	view_all	 => 	'Xem tất cả' ,
	view_detail	 => 	'Xem chi tiết' ,
	view_more	 => 	'xem thêm' ,
	view_post	 => 	'Xem' 

); 
?>
