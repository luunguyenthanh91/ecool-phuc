<?php
/**
 * @Name: lang product vn
 * @version : 1.0
 * @date upgrade : 29/12/2007 by Thai son
 **/
if ( !defined('IN_vnT') )	{ die('Access denied');	}
$lang = array (
	histori_product => 'CÁC SẢN PHẨM ĐÃ XEM',
	contact_product => 'Liên Hệ',
	map_buy => 'Mua Ở Đâu',
	specool => 'ĐIỀU HÒA ECOOL',
	all => 'Tất cả',
	add_cart	 => 	'Thêm vào giỏ' ,
	address	 => 	'Địa chỉ' ,
	best_view	 => 	'Xem nhiều nhất' ,
	best_vote	 => 	'Đánh giá cao' ,
	btn_cancel	 => 	'Huỷ bỏ' ,
	btn_check_out	 => 	'Thanh toán' ,
	btn_confirm	 => 	'Xác nhận' ,
	btn_continue	 => 	'Tiếp tục' ,
	no_have_product => 'Không có sản phẩm phù hợp',
	btn_continue_checkout	 => 	'Tiếp tục thanh toán' ,
	btn_continue_shopping	 => 	'Tiếp tục mua hàng' ,
	btn_empty_cart	 => 	'Hủy bỏ' ,
	btn_login	 => 	'Đăng nhập' ,
	btn_register => 'Đăng ký',
	btn_recalculate	 => 	'Tính tiền lại' ,
	btn_regsiter	 => 	'Đăng ký' ,
	btn_reset	 => 	'Hủy bỏ' ,
	select_category => '-- Chọn danh mục --',
	cart	 => 	'Giỏ hàng' ,
	char	 => 	'ký tự' ,
	check_not_same	 => 	'Giao hàng tại địa chỉ khác' ,
	check_same	 => 	'Giao hàng tới cùng địa chỉ' ,
	checkout_process	 => 	'Xử lý hóa đơn' ,
	checkout_shipping	 => 	'Thông tin giao hàng' ,
	city	 => 	'Tỉnh/Thành phố ' ,
	state => 'Quận/Huyện',
	color	 => 	'Màu' ,
	color_code	 => 	'Mã màu' ,
	comment_order	 => 	'Ghi chú' ,
	confirmation	 => 	'Thông tin đặt hàng' ,
	description	 => 	'Mô tả sản phẩm' ,
	empty_cart	 => 	'Giỏ hàng của bạn đang rỗng' ,
	note_empty_cart	 => 	'Nếu bạn đã cố gắng thêm sản phảm vào giỏ hàng nhưng giỏ hàng vẫn rỗng, có lẽ do trình duyệt web của bạn đã tắt chức năng lưu Cookies. Vui lòng kiểm tra cấu hình của trình duyệt web để đảm bảo rằng trình duyệt web của bạn hỗ trợ tốt chức năng lưu Cookies.' ,
	err_address_empty	 => 	'Vui lòng nhập địa chỉ' ,
	err_city_empty	 => 	'Vui lòng chọn tỉnh/thành' ,
	err_conntent_maxchar	 => 	'Nội dung đánh giá không vượt quá 200 ký tự, cám ơn!' ,
	err_conntent_minchar	 => 	'Nội dung đánh giá phải có ít nhất là 10 ký tự!' ,
	err_content_empty	 => 	'Vui lòng nhập nội dung đánh giá' ,
	err_email_empty	 => 	'Vui lòng nhập Email' ,
	err_email_invalid	 => 	'Email không hợp lệ' ,
	err_full_name_empty	 => 	'Vui lòng nhập họ tên' ,
	err_fullname_empty	 => 	'Vui lòng nhập họ tên' ,
	err_length	 => 	'Độ dài tối thiểu là' ,
	err_mark_empty	 => 	'Vui lòng cho điểm đánh giá' ,
	err_name_empty	 => 	'Vui lòng nhập họ tên' ,
	err_notfound	 => 	'Không tìm thấy sản phẩm này' ,
	err_phone_empty	 => 	'Vui lòng nhập điện thoại' ,
	err_promotion_code_invalid	 => 	'Mã khuyến mãi <b>{code}</b> không tồn tại hoặc đã sử dụng' ,
	err_promotion_code_limit	 => 	'Số lượng mã giảm giá dùng vượt mức cho phép . Bạn chỉ được dùng tối đa <b>{max_code}</b> mã giảm giá cho đơn hàng có tổng trị giá <b>{price}</b>' ,
	err_promotion_date	 => 	'Mã khuyến mãi <b>{code}</b> đã hết hạn sử dụng' ,
	err_promotion_quantity	 => 	'Mã khuyến mãi <b>{code}</b> đã dùng hết số lượng cho phép' ,
	err_security_code_empty	 => 	'Vui lòng nhập mã bảo vệ' ,
	err_security_code_invalid	 => 	'Mã bảo vệ không chính xác' ,
	err_state_empty	 => 	'Vui lòng chọn quận/huyện' ,
	err_ward_empty	 => 	'Vui lòng chọn phường/xã' ,
	err_company_empty => 'Vui lòng nhập tên công ty',
	err_mst_empty => 'Vui lòng nhập mã số thuế',
	exbill => 'Xuất hóa đơn công ty',
	invoice_info => 'Thông tin hóa đơn',
	f_cart	 => 	'Giỏ hàng' ,
	f_checkout_cancel	 => 	'HỦY BỎ HÓA ĐƠN' ,
	f_checkout_confirmation	 => 	'Xác nhận hóa đơn' ,
	f_checkout_method	 => 	'Thông tin đơn hàng' ,
	f_checkout_payment	 => 	'Thông tin thanh toán' ,
	f_checkout_process	 => 	'XỬ LÝ HÓA ĐƠN' ,
	f_checkout_shipping	 => 	'Thông tin giao hàng' ,
	f_custom_search	 => 	'Sản phẩm theo nhu cầu' ,
	f_info_cart	 => 	'THÔNG TIN GIỎ HÀNG' ,
	f_info_customer	 => 	'THÔNG TIN KHÁCH HÀNG' ,
	f_info_order	 => 	'Thông tin đặt hàng' ,
	f_intro_to_friend	 => 	'GIỚI THIỆU SẢN PHẨM CHO BẠN BÈ' ,
	f_navation	 => 	'Sản phẩm' ,
	f_options	 => 	'Tính năng chi tiết' ,
	f_order_detail	 => 	'CHI TIẾT HÓA ĐƠN' ,
	f_order_finished	 => 	'Hoàn tất' ,
	finished	 => 	'Hoàn tất' ,
	full_name	 => 	'Họ tên' ,
	go_login	 => 	'Bạn chưa đăng nhập, bạn có muốn đăng nhập không?' ,
	hidden_email	 => 	'Ẩn email của tôi' ,
	mark	 => 	'Đánh giá' ,
	mark_vote	 => 	'Điểm đánh giá' ,
	maso	 => 	'Mã' ,
	member_login	 => 	'Đăng nhập thành viên' ,
	new_customer	 => 	'Mua hàng nhanh' ,
	social_login => 'Hoặc bằng :',
	fblogin => 'Đăng nhập bằng facebook',
	gplogin => 'Đăng nhập bằng google',
	remember_login => 'Ghi nhớ đăng nhập',
	lostpass => 'Bạn quên password?',
	password => 'Mật khẩu',
	note	 => 	'Ghi chú thêm' ,
	note_new_customer	 => 	'Bạn là một khách hàng mới, muốn mua một cách nhanh chóng mà không cần đăng ký' ,
	other_product	 => 	'Sản phẩm khác' ,
	payment	 => 	'Thanh toán' ,

	payment_address	 => 	'Thông tin thanh toán' ,
	payment_method	 => 	'Phương thức thanh toán' ,
	phone	 => 	'Điện thoại' ,
	mst => 'Mã số thuế',
	company => 'Tên công ty',
	post_at	 => 	'Gửi vào lúc' ,
	cart_maso => 'Mã SP',
	cart_price => 'Đơn giá',
	remove => 'Xóa',
	price	 => 	'Giá bán' ,
	price_old => 'Giá gốc',
	after_old => 'Giảm còn',
	price_asc	 => 	'Giá tăng dần' ,
	price_desc	 => 	'Giá giảm dần' ,
	product	 => 	'Sản phẩm' ,
	product_new	 => 	'Sản phẩm mới' ,
	product_not_found	 => 	'Sản phẩm <b>{pID}</b> không tồn tại' ,
	quantity	 => 	'Số lượng' ,
	register_title	 => 	'Đặt mua sản phẩm' ,
	s_price	 => 	'Giá vận chuyển' ,
	security_code	 => 	'Mã bảo vệ' ,
	security_code_invalid	 => 	'Mã bảo vệ không đúng' ,
	select_city	 => 	'Chọn tỉnh thành' ,
	select_state	 => 	'Chọn quận huyện' ,
	select_ward	 => 	'Chọn phường xã' ,
	shipping_address	 => 	'Thông tin giao hàng' ,
	shipping_free	 => 	'MIỄN PHÍ VẬN CHUYỂN<span>Cho hóa đơn trên 350.000 Đ</span>' ,
	shipping_method	 => 	'Phương thức vận chuyển' ,
	shipping_information => 'Thông tin giao hàng của quý khách',
	size	 => 	'Size' ,
	subject_email_order	 => 	'Thông tin đặt hàng tại {domain}' ,
	total	 => 	'Thành tiền' ,
	total_cart	 => 	'Tổng giỏ hàng' ,
	total_price	 => 	'Tổng cộng' ,
	vietkey	 => 	'Kiểu gõ Tiếng Việt' ,
	write_comment	 => 	'Viết đánh giá' ,
	your_name	 => 	'Tên của bạn:' ,
	err_use_discount	=>	'Không được sử dụng đồng thời mã CTV và mã Coupon',
	f_promotion	=>	'Mã Coupon hoàn tiền (Nếu có)',
	enter_promotion_code	=>	'Điền mã Coupon vào đây',
	note_promotion	=>	'Nhập nhiều mã cách nhau bằng dấu ,<br>Không sử dụng đồng thời với mã CTV',
	f_ctv	=>	'Mã giảm giá cộng tác viên (Nếu có)',
	enter_ctv_code	=>	'Điền mã CTV vào đây',
	note_ctv	=>	'Không sử dụng đồng thời với mã Coupon',
	view_now => 'Xem ngay',
	views => 'Lượt xem',
	buy_now => 'mua ngay',
	not_buy => 'Tạm hết hàng',
	price_default => 'Liên hệ',
	element => 'Thông số kỹ thuật',
	unit => 'VNĐ',
	stock_title => 'Tình trạng',
	in_stock => 'Còn hàng',
	out_stock => 'Hết hàng',
	stock_coming => 'Hàng đang về',
	your_comment	=>	'Đánh giá của bạn',
	f_comment	=>	'Ý kiến khách hàng',
	error_comment_failt	 => 	'Viết nhận xét thất bại . Thử lại lần nữa' ,
	comment_default => 'Nhập ý kiến hoặc câu hỏi của bạn tại đây',
	reply_default => 'Nhập câu trả lời của bạn',
	enter_code => 'Nhập mã bảo vệ',
	enter_info_comment => 'Nhập thông tin của bạn',
	reply => 'Trả lời',
	btn_send_comment	 => 	'Gửi' ,
	btn_close => 'Đóng',
	send_comment_success	 => 	'Viết đánh giá thành công . Vui lòng đợi xét duyệt của BQT' ,
	send_reply_success => 'Gửi câu trả lời thành công . Vui lòng đợi xét duyệt của BQT' ,
	display => 'Hiển thị',
	price_low => 'Từ thấp đến cao',
	price_hight => 'Từ cao đến thấp',
	sort_by => 'Chọn sắp xếp',
	status_filter => 'Phân loại theo',
	select_status => 'Chọn trạng thái',
	chose_status => '-- Chọn phân loại --',
	chose_attr => '-- Chọn loại --',
	attr_filter => 'Loại',
	price_filter => 'Giá',
	select_price => 'Chọn khoảng giá',
	select_attr => 'Chọn loại',
	desc_status => 'KHUYẾN MÃI',
	select_menu => '--- Chọn Menu ---',
	male => 'Nam',
	female => 'Nữ',
	cat_filter => 'Theo danh mục',
	price_to => ' đến ',
	price_over => 'Trên ',
	price_filter => 'Giá',
	select_price => 'Chọn khoảng giá',
	sort_new => 'Mới nhất',
	sort_old => 'Cũ nhất',
	price_low => 'Giá tăng dần',
	price_hight => 'Giá giảm dần',
	sort_filter => 'Sắp xếp',
	select_filter => '-- Lựa chọn --',
	display_filter => 'Hiển thị',
	prints => 'In bài viết',
	video => 'Video',
	belong_product => 'Sản phẩm liên quan',
	num_sub => 'Có {num} sản phẩm khác',
	sub_img => 'Hình',
	sub_maso => 'Mã hàng',
	sub_color => 'Màu sắc',
	sub_category => 'Danh mục',
	sub_param => 'Thông số',
	sub_desc => 'Mô tả',
	sub_price => 'Giá bán',
	sub_quantity => 'Số lượng',
	no_have_sub => 'Không có sản phẩm phù hợp',
	alert_outstock => 'Sản phẩm này tạm thời hết hàng. Vui lòng quay lại sau hoặc đặt sản phẩm tương tự.',
	quantity0 => 'Trước khi đặt hàng, bạn phải nhập số lượng cần mua.',
	username => 'Tên tài khoản',
	back_to_prev => 'Quay lại trang trước',
	back_to_home => 'Quay về trang chủ',
	mess_out_stock => 'Sản phẩm {p_name} đã hết hàng. Vui lòng chọn sản phẩm khác',
	mess_not_enough => 'Sản phẩm {p_name} chỉ được phép đặt tối đa {onhand} sản phẩm',

);
?>
