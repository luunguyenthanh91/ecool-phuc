<?php 
/**
* @Name: lang global	
* @version : 1.0
* @date upgrade : 11/12/2007 by Thai 2asdasd
**/
if ( !defined('IN_vnT') )	{ die('Access denied');	} 
$lang = array ( 
	about	 => 	'About' ,
	account_information	 => 	'Account information' ,
	announce	 => 	'Announce' ,
	btn_search	 => 	'Search' ,
	cart	 => 	'Cart' ,
	change_img	 => 	'Change' ,
	change_pass	 => 	'Change password' ,
	contact	 => 	'Contact' ,
	copyright	 => 	'Copyright © 2018 <strong>CÔNG TY Son Ha</strong>' ,
	custom	 => 	'khảo</br>sát' ,
	error	 => 	'Error' ,
	f_navation	 => 	'Home page' ,
	faqs	 => 	'FAQs' ,
	find_us	 => 	'Find us at' ,
	gpkd	 => 	'GPĐKKD số 12345678 do Sở KHĐT TP.HCM cấp ngày 23/08/2006' ,
	hello	 => 	'Hello' ,
	homepage	 => 	'Home page' ,
	hotline	 => 	'Hotline' ,
	key_search_empty	 => 	'Please enter the keywords' ,
	key_search_invalid	 => 	'Keyword must least 2 character' ,
	keyword_default	 => 	'Keyword' ,
	login	 => 	'Login' ,
	logout	 => 	'Logout' ,
	member	 => 	'Account' ,
	mess_redirect	 => 	'Site is loading...Please wait for a moment or click here' ,
	news	 => 	'News' ,
	online	 => 	'Online' ,
	pages	 => 	'Pages' ,
	product	 => 	'Product' ,
	register	 => 	'Register' ,
	search	 => 	'Search' ,
	select_city	 => 	'-- Select city --' ,
	select_state	 => 	'-- Select states --' ,
	service	 => 	'Service' ,
	showroom	 => 	'virtual showroom' ,
	support_hotline	 => 	'Support hotline' ,
	vistited	 => 	'Vistited' ,
	your_order	 => 	'Your order' ,
	mangthiennhien => 'BRING NATURE TO YOUR HOME',
	dangkynhanmail => 'Sign up to receive promotional information from Ecool',
	smallnhanmail => 'All information about promotions, customer care, events, seminars will be sent to your Email.',
	yourmail => 'Your email address',
	xacnhan => 'Confirm',
	xemtttt => 'View all news',
); 
?>
