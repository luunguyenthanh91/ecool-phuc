<?php 
/**
* @Name: lang main
* @version : 1.0
* @date upgrade : 29/12/2007 by Thai son
**/
if ( !defined('IN_vnT') )	{ die('Access denied');	} 
$lang = array ( 
	f_navation	=> 	'Homepage' ,
	homepage	 => 	'Homepage' ,
	view_post => '[— View]',
	view_detail => 'View detail',
	news => 'News',
	news_slogan => 'Useful - accurate',
	product => 'Product Focus',
	product_desc => 'Your Home, Your Design',
	maso => 'Code',
	price_default => 'Contact',
	out_stock => 'Out of stock',
	view_more => 'View more',
	view_all => 'View all',
	collection => 'Collection',
	collection_slogan => 'Sáng tạo, trẻ trung và đa dạng trong từng thiết kế',
	project => 'Construction',
	project_slogan => 'Giá trị và sự khác biệt',
	project_desc => 'Project'
	
); 
?>
