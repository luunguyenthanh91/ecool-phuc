<?php
/*================================================================================*\
|| 							Name code : gallery_setting.php 		 		            	  ||
||  				Copyright @2008 by Thai Son - CMS vnTRUST                     ||
\*================================================================================*/
/**
 * @version : 2.0
 * @date upgrade : 09/01/2009 by Thai Son
 **/
if (! defined('IN_vnT'))
{
  die('Hacking attempt!');
}

$vntModule = new vntModule();
class vntModule
{
  var $output = "";
  var $skin = "";
  var $linkUrl = "";
  var $module = "epaper";
	var $action = "setting";
	
  /**
   * function vntModule ()
   * Khoi tao 
   **/
  function vntModule ()
  {
    global $Template, $vnT, $func, $DB, $conf;
    require_once ("function_".$this->module.".php");
    $this->skin = new XiTemplate(DIR_MODULE . DS . $this->module . "_ad" . DS . "html" . DS . $this->action . ".tpl");
    $this->skin->assign('LANG', $vnT->lang);
    
    $lang = ($vnT->input['lang']) ? $lang = $vnT->input['lang'] : $func->get_lang_default();
    $this->linkUrl = "?mod=".$this->module."&act=".$this->action."&lang=" . $lang;
		
    switch ($vnT->input['sub'])
    {      
      case 'edit':
	   		 $nd['content']=$this->do_Edit($lang);
      break;
			default:
        $nd['f_title'] = $vnT->lang['manage_setting_epaper'];
        $nd['content'] = $this->do_Manage($lang);
        break;
    }
    $nd['menu'] = $func->getToolbar_Small($this->module,$this->action, $lang);
		$nd['row_lang'] = $func->html_lang("?mod=".$this->module."&act=".$this->action, $lang);
		
    $Template->assign("data", $nd);
    $Template->parse("box_main");
    $vnT->output .= $Template->text("box_main");
  
  }    

	/**
   * function do_Edit 
   * Cap nhat gioi thieu 
   **/
  function do_Edit ($lang)
  {
    global $vnT, $func, $DB, $conf;

    if ($vnT->input['do_submit']) {
      $data = $_POST;
			$res_check = $DB->query("select * from epaper_setting where id=1 ");
      if($row = $DB->fetch_row($res_check))
			{
				foreach ($row as $key => $value) 
				{
					if ($key != 'id') {
						if($vnT->input[$key]) {
							$dup[$key] = $vnT->input[$key];
						}
					}
				}
				
				$res_lang = $DB->query("SELECT id FROM epaper_setting WHERE lang='$lang'");
				if($DB->num_rows($res_lang))
				{
					$ok = $DB->do_update("epaper_setting", $dup, "lang='$lang'");	
				}else{
					$dup['lang'] = $lang ;
					$ok = $DB->do_insert("epaper_setting", $dup );	
				}			 
			}
			
			//xoa cache
      $func->clear_cache();
			//insert adminlog
      $func->insertlog("Setting", $_GET['act'], $id);
			$err = $vnT->lang["edit_setting_epaper_success"];
			$url = $this->linkUrl;
			$func->html_redirect($url, $err);
		}
  }
	
  /**
   * function do_Manage() 
   * Quan ly cac gioi thieu
   **/
  function do_Manage ($lang)
  {
    global $vnT, $func, $DB, $conf;    
		
		$result = $DB->query("select * from epaper_setting where lang='$lang' ");
		$data = $DB->fetch_row($result) ;
		
		$data['wishlist']	=  vnT_HTML::list_yesno("wishlist",$data['wishlist']);
		$data['sendmail']	= vnT_HTML::list_yesno("sendmail",$data['sendmail']);
		$data['toolsearch']	= vnT_HTML::list_yesno("toolsearch",$data['toolsearch']);
		$data['nophoto'] = vnT_HTML::list_yesno("nophoto",$data['nophoto']);
		$data['thum_size'] = vnT_HTML::list_yesno("thum_size",$data['thum_size']); 
 		$data['comment']	= vnT_HTML::list_yesno("comment", $data['comment']);
		$data['imgthumb_align'] = vnT_HTML::selectbox("imgthumb_align", array(
      'left' => $vnT->lang['align_left'] , 
      'right' => $vnT->lang['align_right']), $data['imgthumb_align']);
     
  	$data['folder_upload'] = vnT_HTML::selectbox("folder_upload", array(
																											0 => $vnT->lang['folder_upload0'],
																											1 => $vnT->lang['folder_upload1'],
																											2 => $vnT->lang['folder_upload2'],
																											3 => $vnT->lang['folder_upload3']
																										), $data['folder_upload'])  ;
	
		
		$data['link_action'] = $this->linkUrl."&sub=edit";				
    /*assign the array to a template variable*/
    $this->skin->assign('data', $data);
    
    $this->skin->parse("manage");
    return $this->skin->text("manage");
  }
	
  // end class
}

?>
