
<!-- BEGIN: edit -->
<script type="text/javascript">
	/* <![CDATA[*/
	
function attachCallbacks(a) {
	a.bind("Error", function (a, e) { 
		$('#bugcallbacks').append("<div>Error: " + e.code +   ", Message: " + e.message +   (e.file ? ", File: " + e.file.name : "") + "</div>"   );	 
     a.refresh(); // Reposition Flash/Silverlight
		$('#bugcallbacks').show();
	});
	a.bind("FileUploaded", function (b, e, c) { 	 
		1 == a.total.queued && ("undefined" == typeof debug ? $("#myForm").submit() : ($("#bugcallbacks").show(), 0 < b.e.code && $("#bugcallbacks").append("<div>File: " + b.result + " <br/ > Error code: " + e.message + " <hr></div>")))
	})	
}

// Convert divs to queue widgets when the DOM is ready
$(function() {
	
	$("#uploader").pluploadQueue({
		// General settings
		runtimes : 'gears,flash,silverlight,browserplus,html5',
		url : 'modules/epaper_ad/ajax/upload.php',
		max_file_size : '{data.max_upload}',
		chunk_size : '1mb', 
		rename : true,
		multipart: true,
		multipart_params: {
			'id': '{data.e_id}',  
			'folder_upload': '{data.folder_upload}',  
			'folder': '{data.folder}', 
			'w' : '{data.w_pic}',	
			'crop' : '0',	
			'thumb' : '1',		
			'w_thumb': '{data.w_thumb}'
		},		

		// Resize images on clientside if we can
		resize : {width : 1000, height : 1000, quality : 90}, 
		// Specify what files to browse for
		filters : [
			{title : "Image files", extensions : "jpg,gif,png"}
		],

		flash_swf_url : ROOT+'js/plupload/plupload.flash.swf',
		silverlight_xap_url : ROOT+'js/plupload/plupload.silverlight.xap' ,
		preinit: attachCallbacks
	});
 	
	
});

	/* ]]> */
</script>
<style>

#uploader { width:800px; margin:0px auto;} 
.square.note {
    background: none repeat scroll 0 0 #ECECEC;
    border: 1px solid #CCCCCC;
    color: #333333;
    padding: 5px 10px 10px; 
}

</style>
<table width="80%" border="0" cellspacing="1" cellpadding="1" align="center"  bgcolor="#CCCCCC">
  <tr bgcolor="#FFFFFF">
    <td width="150" align="center" style="padding:5px;">{data.pic}</td>
    <td style="padding:5px;line-height:20px;">
				Title: <strong>{data.title}</strong><br />
        E-ID : #<strong>{data.e_id}</strong>
    		</td>
  </tr>
</table>
<br />
<p align="center" ><strong>Danh sách các trang cho E-catalogue này:</strong></p>
<form action="{data.link_action}" method="post"  name="myForm"  >
<table width="100%" border="0" cellspacing="2" cellpadding="5">
  <tr> {data.list_pic_old} </tr>
</table>
<p align="center"> 
				<input type="submit" name="btnUpdate" value="Cập nhật thứ tự" class="button"> 
  </p>
</form> 

{data.err}

<form action="{data.link_action}" method="post" name="myForm" id="myForm"   enctype="multipart/form-data" >

  <table width="100%"  border="0"  cellspacing="1" cellpadding="1" class="admintable">
  
 	<tr >
 
       <td class="row0" > 
       
       <div style="padding:10px 30px;">
      
	<input type="hidden" name="hlistfile" id="hlistfile" value="" />
	<div id="uploader">
		<p>You browser doesn't have Flash, Silverlight, Gears, BrowserPlus or HTML5 support.</p>
	</div>
  
 
<br />

<div class="square note">
  <ul>
  <li>Mỗi lần upload tối đa <b>50 ảnh</b>. Giữ phím <b>Ctrl</b> để chọn thêm ảnh, phím <b>Shift</b> để chọn cùng lúc nhiều ảnh.</li>
      <li>Dung lượng mỗi ảnh tối đa <b>{data.max_upload}</b></li>
      <li>Định dạng ảnh cho phép: <b>.jpg</b>, <b>.jpeg</b>, <b>.gif</b> hoặc <b>.png</b></li>
  </ul>
    <div class="clear"></div>
</div>			

<div id="bugcallbacks" style="display: none "></div>
       </div>
        </td>
    </tr>
     

	</table>
</form>

<br>
<!-- END: edit -->



<!-- BEGIN: edit_pic -->
<table width="80%" border="0" cellspacing="1" cellpadding="1" align="center"  bgcolor="#CCCCCC">
  <tr bgcolor="#FFFFFF">
    <td width="150" align="center" style="padding:5px;">{data.pic}</td>
    <td style="padding:5px;line-height:20px;">
				Title: <strong>{data.title} </strong><br />
        E-ID : #<strong>{data.e_id}</strong>
    		</td>
  </tr>
</table>
<br />

{data.err}


<form action="{data.link_action}" method="post" enctype="multipart/form-data" name="myForm" >

<table width="100%"  border="0"  cellspacing="1" cellpadding="1" class="admintable">

		<tr {data.style} >
        <td class="row1" width="15%">Số thứ tự : </td>
        <td class="row0"><input name="pic_order" id="pic_order" type="text" size="40" value="{data.pic_order}" /></td>
    </tr>

		<tr>
            <td class="row1" width="15%">Name : </td>
            <td class="row0"><input name="pic_name" id="pic_name" type="text" size="40" value="{data.pic_name}" /></td>
          </tr>

			<tr>
            <td class="row1"  width="15%" >Hình  : </td>
            <td class="row0">{data.picture}<br /><input name="image" type="file" id="image" size="40" maxlength="250"> (*.jpg,*.gif) Only !</td>
          </tr>
         
		
   	<tr >
     <td class="row1" > Nội dung trang </td>
    	<td class="row0"  >
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="100"><strong class="font_err">Ghi chú :</strong></td>
    <td class="font_err"><p>Nếu muốn chèn popup cho 1 link thì . Trong Popup "Chèn/Sửa liên kết" của hình, click qua tab "Mở rộng" rồi điền vào ô "Lớp Stylesheet" = <strong>lightbox</strong></p><p>Nếu tạo slideshow hình ảnh . Trong Popup "Chèn/Sửa liên kết" của hình, click qua tab "Mở rộng" rồi điền vào ô "Group gallery lightbox" với <strong>tên giống nhau</strong></p></td>
  </tr>
</table>
 {data.html_content}   </td>
		</tr>
    <tr align="center">           
          <td class="row1" >&nbsp; </td>
            <td class="row0">
			 			 <input type="submit" name="btnUpdate" value="Submit" class="button">
              <input type="reset" name="Submit" value="Reset" class="button">
            </td></tr>
        </table>
    </form>


<br>
<!-- END: edit_pic -->


<!-- BEGIN: manage -->
<form action="{data.link_fsearch}" method="post" name="fSearch">
<table width="100%" border="0" cellspacing="2" cellpadding="2" align="center" class="tableborder">
  <tr>
    <td width="15%" align="left"><strong>{LANG.totals}:</strong> &nbsp;</td>
    <td width="85%" align="left"><b class="font_err">{data.totals}</b></td>
  </tr>
  <tr>
    <td  colspan="2"><strong>{LANG.search} :</strong> {data.list_search} <strong> {LANG.keyword} :</strong>  <input name="keyword" value="{data.keyword}" size="20" type="text" />  &nbsp;<input type="submit" name="btnGo" value=" Search " class="button"></td>
  </tr>
</table>
</form>
{data.err}
<br />
{data.table_list}
<br />
<table width="100%"  border="0" align="center" cellspacing="1" cellpadding="1" class="bg_tab">
  <tr>
    <td  height="25">{data.nav}</td>
  </tr>
</table>
<br />
<!-- END: manage -->