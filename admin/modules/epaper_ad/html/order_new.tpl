
<!-- BEGIN: edit -->
<table width="100%"  border="0"  cellspacing="1" cellpadding="1" class="admintable">
 	<tr>
     <td width="20%" class="row1"><strong>{LANG.full_name} </strong>:</td>
      <td class="row0">{data.name}</td>
    </tr>  
    <tr>
      <td  class="row1"><strong>{LANG.address} </strong>:</td>
      <td class="row0">{data.address}</td>
    </tr> 
    <tr>
      <td  class="row1"><strong>{LANG.phone} </strong>:</td>
      <td class="row0">{data.phone}</td>
    </tr>
    <tr>
      <td  class="row1"><strong>Email</strong>:</td>
      <td class="row0">{data.email}</td>
    </tr> 
</table>
<br />
<table width="100%"  border="0"  cellspacing="1" cellpadding="1" class="admintable">
	
	<tr>
  	<td class="row1" width="20%"><strong>Nội dung góp ý </strong>: </td>
		<td class="row0"><p align="justify"  >{data.content}</p></td>

	</tr>
</table>
<br />
{data.err}
<form action="{data.link_action}" method="post" enctype="multipart/form-data" name="myForm"  class="validate">
<table width="100%"  border="0"  cellspacing="1" cellpadding="1" class="admintable">
	<tr class="form-required">
			<td class="row1" width="20%" >{LANG.send_to_email} : </td>
			<td  align="left" class="row0"><input name="email_to" id="email_to" type="text" size="60" maxlength="250" value="{data.email}"></td>
		</tr>
	<tr>
   <td class="row1">{LANG.re_subject} : </td>
   <td class="row0"><input name="re_subject" id="re_subject" type="text" size="60" maxlength="250" value="{data.re_subject}"></td>
  </tr>
  
  <tr>
   <td class="row1">{LANG.content_reply} : </td>
   <td class="row0">{data.html_content}</td>
  </tr>
  
 		<tr class="row1">
    <td  colspan="2" align="center" >&nbsp; 
		<input type="hidden" name="content" value="{data.content}" >		
    <input type="button" name="btnUnread" value="{LANG.unread}" onclick="window.location.href='?mod=contact&act=comment&sub=markUnread&id={data.id}'" class="button">&nbsp;	
		<input type="submit" name="btnReply" value="{LANG.btn_reply}" class="button" >&nbsp;
		<input type="submit" name="btnForward" value="{LANG.btn_forward}" class="button" >
			</td>
		</tr>
	</table>
</form>

<!-- END: edit -->

<!-- BEGIN: manage -->
<form action="{data.link_fsearch}" method="post" name="myform">
<table width="100%" border="0" cellspacing="2" cellpadding="2" align="center" class="tableborder">
  <tr>
    <td width="20%" align="left">{LANG.totals}: &nbsp;</td>
    <td  align="left"><b class="font_err">{data.totals}</b></td>
  </tr>

  <tr>
 </table>
 <table border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td><img src="{DIR_IMAGE_MOD}/vnt_mail_unread.png" align="absmiddle" />&nbsp; {LANG.email_unread} &nbsp;&nbsp;&nbsp;</td>
		<td><img src="{DIR_IMAGE_MOD}/vnt_mail_read.png" align="absmiddle" />&nbsp; {LANG.email_read}&nbsp;&nbsp;&nbsp;</td>
	</tr>
	<tr>

		<td><img src="{DIR_IMAGE_MOD}/vnt_mail_reply.png" align="absmiddle" />&nbsp; {LANG.email_reply}&nbsp;&nbsp;&nbsp;</td>
		<td><img src="{DIR_IMAGE_MOD}/vnt_mail_forward.png" align="absmiddle" />&nbsp;&nbsp;{LANG.email_forward}&nbsp;&nbsp;</td>
	</tr>
</table>
</form>
{data.err}
<br />
{data.table_list}
<br />
<table width="100%"  border="0" align="center" cellspacing="1" cellpadding="1" class="bg_tab">
  <tr>
    <td  height="25">{data.nav}</td>
  </tr>
</table>
<br />
<!-- END: manage -->