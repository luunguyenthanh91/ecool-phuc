<!-- BEGIN: edit -->
<script language=javascript>
$(document).ready(function() {
	$('#myForm').validate({
		rules: {			
				name: {
					required: true 
				},
				title: {
					required: true,
					minlength: 3
				}
	    },
	    messages: {
	    	
				name: {
						required: "{LANG.err_text_required}"
				} ,
				title: {
						required: "{LANG.err_text_required}",
						minlength: "{LANG.err_length} 3 {LANG.char}" 
				} 
		}
	});
});
</script>
{data.err}
<form action="{data.link_action}" method="post" enctype="multipart/form-data" name="myForm" id="myForm"  class="validate">
  <table width="100%"  border="0"  cellspacing="1" cellpadding="1" class="admintable">
		
    <!-- BEGIN: select_lang -->
		<tr >
			<td class="row1" >{LANG.language} : </td>
			<td  align="left" class="row0"><input name="rLang" id="rLang" type="radio" value="0" /> {LANG.only_this_lang} <input name="rLang" id="rLang" type="radio" value="1" checked="checked" /> {LANG.all_lang}</td>
		</tr>
    <!-- END: select_lang -->
    
    <tr class="form-required">
     <td class="row1" width="20%">{LANG.name}: </td>
     <td class="row0"><input name="name" id="name" type="text" size="50" maxlength="250" value="{data.name}"></td>
    </tr>
    
		<tr class="form-required">
     <td class="row1" width="20%">{LANG.title}: </td>
     <td class="row0"><input name="title" id="title" type="text" size="50" maxlength="250" value="{data.title}"></td>
    </tr>
    
    <tr >
     <td class="row1" width="20%">{LANG.picture}: </td>
     <td class="row0">
     	{data.pic}
      <div class="ext_upload">
      <input name="chk_upload" id="chk_upload" type="radio" value="0" checked> 
      Insert URL's image &nbsp; <input name="picture" type="text" size="50" maxlength="250" onchange="do_ChoseUpload('ext_upload',0);" > <br>
      <input name="chk_upload" id="chk_upload" type="radio" value="1"> Upload Picture &nbsp;&nbsp;&nbsp;
      <input name="image" type="file" id="image" size="30" maxlength="250" onclick="do_ChoseUpload('ext_upload',1);" >
      </div>
    </td>
    </tr>    
 
    
			
		<tr align="center">
    <td class="row1" >&nbsp; </td>
			<td class="row0" >
				<input type="hidden" name="do_submit"	 value="1" />
				<input type="submit" name="btnAdd" value="Submit" class="button">
				<input type="reset" name="btnReset" value="Reset" class="button">
			</td>
		</tr>
	</table>
</form>


<br>
<!-- END: edit -->

<!-- BEGIN: manage -->
<form action="{data.link_fsearch}" method="post" name="fSearch">
<table width="100%" border="0" cellspacing="2" cellpadding="2" align="center" class="tableborder">
  <tr>
    <td width="15%" align="left">{LANG.totals}: &nbsp;</td>
    <td width="85%" align="left"><b class="font_err">{data.totals}</b></td>
  </tr>
</table>
</form>
{data.err}
<br />
{data.table_list}
<br />
<table width="100%"  border="0" align="center" cellspacing="1" cellpadding="1" class="bg_tab">
  <tr>
    <td  height="25">{data.nav}</td>
  </tr>
</table>
<br />
<!-- END: manage -->