<?php
/*================================================================================*\
|| 							Name code : epaper.php 		 		            	  ||
||  				Copyright @2008 by Thai Son - CMS vnTRUST                     ||
\*================================================================================*/
/**
 * @version : 2.0
 * @date upgrade : 09/01/2009 by Thai Son
 **/
if (! defined('IN_vnT'))
{
  die('Hacking attempt!');
}

$vntModule = new vntModule();
class vntModule
{
  var $output = "";
  var $skin = "";
  var $linkUrl = "";
  var $module = "epaper";
	var $action = "epaper";
	
  function vntModule (){
    global $Template, $vnT, $func, $DB, $conf;
    require_once ("function_".$this->module.".php");
		$this->skin = new XiTemplate(DIR_MODULE.DS.$this->module . "_ad" . DS . "html" . DS . $this->action . ".tpl");
    $this->skin->assign('LANG', $vnT->lang);
		$this->skin->assign('DIR_IMAGE', $vnT->dir_images);
    $lang = ($vnT->input['lang']) ? $lang = $vnT->input['lang'] : $func->get_lang_default();
		loadSetting($lang);
    $this->linkUrl = "?mod=".$this->module."&act=".$this->action."&lang=" . $lang;		
		$vnT->html->addScript("modules/" . $this->module."_ad/js/".$this->module.".js");
		$vnT->html->addStyleSheet( "modules/epaper_ad/css/selectbox.css");
		$vnT->html->addScript("modules/epaper_ad/js/selecbox.js");
		$vnT->html->addStyleSheet("modules/" . $this->module."_ad/css/".$this->module.".css");
		$vnT->html->addScript("modules/" . $this->module."_ad/js/yetii.js");
    switch ($vnT->input['sub']){
      case 'add':
        $nd['f_title'] = $vnT->lang['add_epaper'];
        $nd['content'] = $this->do_Add($lang);
        break;
      case 'edit':
        $nd['f_title'] = $vnT->lang['edit_epaper'];
        $nd['content'] = $this->do_Edit($lang);
        break;
      case 'del':
        $this->do_Del($lang);
        break;
      default:
        $nd['f_title'] = $vnT->lang['manage_epaper'];
        $nd['content'] = $this->do_Manage($lang);
        break;
    }
    $nd['menu'] = $func->getToolbar($this->module,$this->action, $lang);
    $nd['row_lang'] = $func->html_lang("?mod=".$this->module."&act=".$this->action, $lang);
    $Template->assign("data", $nd);
    $Template->parse("box_main");
    $vnT->output .= $Template->text("box_main");
  }
  function do_Add ($lang){
    global $vnT, $func, $DB, $conf;
    $err = "";
    $vnt->input['price']=0;
		$vnt->input['display'] =1;
 		$rate = ($vnT->setting['rate']) ? $vnT->setting['rate'] : 1;
		$w_thum = 175;
		$h_thum = 248;
		$w = 550;
		$h = 778;
    if ($vnT->input['do_submit'] == 1){
			$data = $_POST;
			$cat_id = @implode(",",$vnT->input['cat_chose']);
			$maso = trim($vnT->input['maso']);
			$title = $vnT->input['title'];
			//check maso
			if ($maso){
				$res_ck = $DB->query("SELECT e_id,maso FROM epaper WHERE maso='$maso' AND lang='$lang'  ") ;
				if ($DB->num_rows($res_ck)) $err = $func->html_err($vnT->lang['maso_existed']);
			}
			if (empty($err)){
				$dir = "book_image"; 
				//upload
				if( $vnT->input['chk_upload'] && !empty($_FILES['image']) && $_FILES['image']['name']!="" ) {
					$up['path'] = MOD_DIR_UPLOAD;
					$up['dir'] = $dir;
					$up['file'] = $_FILES['image'];
					$up['type'] = "hinh";
					$up['resize_crop']= 1;
					$up['w']= $w;
					$up['h']= $h;
					$up['thum'] = 1;
					$up['w_thum'] = $w_thum;
					$up['h_thum'] = $h_thum;
					$result = $vnT->File->Upload($up);
					if (empty($result['err'])) {
						$picture = $result['link'];
						$file_type = $result['type'];
					} else {
						$err = $func->html_err($result['err']);
					}
				}
			}
			$link_down = chop($vnT->input['link_down']);
			$type = strtolower(substr($link_down, strrpos($link_down, ".") + 1));
			$link_mp3 = chop($vnT->input['link_mp3']);
      // insert CSDL
      if (empty($err)){
				$cot['cat_id'] = $cat_id;
				$cot['maso'] = $maso;			 
				$cot['picture'] = $picture; 
				$cot['link_down'] = $link_down;
				$cot['link_mp3'] = $link_mp3;
				$cot['date_post'] = time();
				$cot['date_update'] = time();			
				$cot['adminid'] =  $vnT->admininfo['adminid'];
        $ok = $DB->do_insert("epaper", $cot);
        if ($ok){
					$e_id = $DB->insertid();
					//update maso
					if(empty($maso)){
						$dataup['maso'] = create_maso ($cat_id,$e_id);
 					}
 					$dataup['e_order'] = $e_id;
 					$DB->do_update('epaper',$dataup,"e_id = {$e_id}");
				 	//epaper content 
					$cot_d['e_id'] = $e_id;
					$cot_d['title'] = $title;
					$cot_d['description'] = $DB->mySQLSafe($_POST['description']); 
					$cot_d['display'] = $vnT->input['display'];
					//SEO
					$cot_d['friendly_url'] = (trim($vnT->input['friendly_url'])) ? trim($vnT->input['friendly_url']) :  $func->make_url($title);
					$cot_d['friendly_title'] = (trim($vnT->input['friendly_title'])) ? trim($vnT->input['friendly_title']) :  $func->utf8_to_ascii($title);
					$cot_d['metakey'] = (trim($vnT->input['metakey'])) ? trim($vnT->input['metakey']) :  $title ;
					$cot_d['metadesc'] = (trim($vnT->input['metadesc'])) ? trim($vnT->input['metadesc']) :  $func->cut_string($func->check_html($_POST['description'],'nohtml'),200,1) ;
					$query_lang = $DB->query("select name from language ");
					while ( $row = $DB->fetch_row($query_lang) ) {
						$cot_d['lang'] = $row['name'];
						$DB->do_insert("epaper_desc",$cot_d);
					}
          $func->clear_cache();
          //insert adminlog
          $func->insertlog("Add", $_GET['act'], $e_id);
          $mess = $vnT->lang['add_success'];
          if(isset($_POST['btn_preview']))	{
						$url = $this->linkUrl . "&sub=edit&id=$e_id&preview=1";
						$DB->query("Update epaper SET display=0 WHERE e_id=$e_id ");
					}else{
						$url = $this->linkUrl . "&sub=add";						
					}
          $func->html_redirect($url, $mess);
        } else {
          $err = $func->html_err($vnT->lang['add_failt'].$DB->debug());
        }
      }
    }
		$data['list_display'] = vnT_HTML::list_yesno ("display",$vnt->input['display']) ;
		$data["html_content"] = $vnT->editor->doDisplay('description', $vnT->input['description'], '100%', '450', "Default");
    $data['err'] = $err;
    $data['link_action'] = $this->linkUrl . "&sub=add";
    $this->skin->assign('data', $data);
    $this->skin->parse("edit");
    return $this->skin->text("edit");
  }
  function do_Edit ($lang){
    global $vnT, $func, $DB, $conf;
    $id = (int) $vnT->input['id'];
    $rate = ($vnT->setting['rate']) ? $vnT->setting['rate'] : 1;
		$w_thum = 175 ;
		$h_thum = 248 ;
		$w = 550;
		$h = 778;
    if ($vnT->input['do_submit']){
      $data = $_POST;
 			$cat_id = @implode(",",$vnT->input['cat_chose']);
			$maso = trim($vnT->input['maso']);
			$title = $vnT->input['title'];
			$ngay = explode("/",$vnT->input['date_issue']);
			$price = $vnT->input['price'];
			if($vnT->input['op_value']){
				$options = $vnT->format->txt_serialize($vnT->input['op_value']);
			}
			//check maso
			if ($maso){
				$res_ck = $DB->query("select e_id,maso from epaper where maso='$maso' AND lang='$lang' and e_id<>$id ") ;
				if ($DB->num_rows($res_ck)) $err = $func->html_err($vnT->lang['maso_existed']);
			}
			//check upload
			if (empty($err)){
				// tao thu muc cho san pham 
				$dir = "book_image";		
				//upload
				if( $vnT->input['chk_upload'] && !empty($_FILES['image']) && $_FILES['image']['name']!="" ){
					$up['path'] = MOD_DIR_UPLOAD;
					$up['dir']= $dir;
					$up['file']= $_FILES['image'];
					$up['type']= "hinh";
					$up['resize_crop']= 1;
					$up['w']= $w;
					$up['h']= $h;
					$up['thum'] = 1;
					$up['w_thum'] = $w_thum;
					$up['h_thum'] = $h_thum;
					$result = $vnT->File->Upload($up);
					if (empty($result['err'])) {
						$picture = $result['link'];
						$file_type = $result['type'];
					} else {
						$err = $func->html_err($result['err']);
					}
				}
			}//end check upload
      
			$link_down = chop($vnT->input['link_down']);
			$type = strtolower(substr($link_down, strrpos($link_down, ".") + 1));
			
			$link_mp3 = chop($vnT->input['link_mp3']);
      if (empty($err)){
				$cot['cat_id'] =  $cat_id;
				$cot['maso'] = $maso;			
				$cot['link_down'] = $link_down;
				$cot['link_mp3'] = $link_mp3;
				$cot['date_update'] = time();
				if ($vnT->input['chk_upload']==1 || !empty($picture) )	{										 
					$cot['picture'] = $picture;
 				}
        $ok = $DB->do_update("epaper", $cot, "e_id=$id");
        if ($ok){
					//update content
					$cot_d['title'] = $title;
					$cot_d['description'] = $DB->mySQLSafe($_POST['description']); 
					$cot_d['display'] = $vnT->input['display'];
					
					//SEO
					$cot_d['friendly_url'] = (trim($vnT->input['friendly_url'])) ? trim($vnT->input['friendly_url']) :  $func->make_url($title);
					$cot_d['friendly_title'] = (trim($vnT->input['friendly_title'])) ? trim($vnT->input['friendly_title']) :  $func->utf8_to_ascii($title);
					$cot_d['metakey'] = (trim($vnT->input['metakey'])) ? trim($vnT->input['metakey']) :  $title ;
					$cot_d['metadesc'] = (trim($vnT->input['metadesc'])) ? trim($vnT->input['metadesc']) :  $func->cut_string($func->check_html($_POST['description'],'nohtml'),200,1) ;
					
					$DB->do_update("epaper_desc",$cot_d," e_id={$id} and lang='{$lang}'");
				 
					//xoa cache
          $func->clear_cache();
					
          //insert adminlog
          $func->insertlog("Edit", $_GET['act'], $id);          
          $err = $vnT->lang["edit_success"];
					
					if(isset($_POST['btn_preview']))	{
						$url = $this->linkUrl . "&sub=edit&id=$id&preview=1";
					}else{
						$url = $this->linkUrl . "&sub=edit&id=$id";
					}
          
          $func->html_redirect($url, $err);
        } else
          $err = $func->html_err($vnT->lang["edit_failt"].$DB->debug());
      }
    
    }
    
    $query = $DB->query("SELECT *
													FROM epaper p, epaper_desc pd
													WHERE p.e_id=pd.e_id 
													AND pd.lang='$lang'
													AND p.e_id=$id ");
    if ($data = $DB->fetch_row($query))
    {
			if($vnT->input['preview']==1)	{
				  
				$link_preview = $conf['rooturl'];
				if($vnT->muti_lang) $link_preview .= $lang."/";				
				$link_preview.= "epaper/detail/".$data['friendly_url']."-".$id.".html/?preview=1";
				
				$mess_preview = str_replace(array("{title}","{link}"),array($data['title'],$link_preview),$vnT->lang['mess_preview']);
				$data['js_preview'] = "tb_show('".$mess_preview."', '".$link_preview."&TB_iframe=true&width=1000&height=700',null)";
			}
			
			if (!empty($data['picture'])) 	{
				$data['pic'] = get_pic_epaper($data['picture'],$w_thumb)."<br>";
			} 
			if (! empty($data['link_down'])) {
        $link_down = $data['link_down'];        
        $file = "<a href='{$link_down}' target='_blank' > " . $data['link_down'] . "</a>";
        $data['file'] = "<div style='padding-bottom:5px'>{$file}</div>";
        $data['file'] .= "<input name=\"chk_upload_file\" type=\"radio\" value=\"1\"> Upload File &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
        $data['file'] .= "<input name=\"chk_upload_file\" type=\"radio\" value=\"0\" checked> Keep File <br>";
      } else
        $data['file'] = "<input name=\"chk_upload_file\" type=\"hidden\" value=\"1\">";
				
			if (! empty($data['link_mp3'])) {
        $link_mp3 = $data['link_mp3'];        
        $file_mp3 = "<a href='{$link_mp3}' target='_blank' > " . $data['link_mp3'] . "</a>";
        $data['file_mp3'] = "<div style='padding-bottom:5px'>{$file_mp3}</div>";
        $data['file_mp3'] .= "<input name=\"chk_upload_file\" type=\"radio\" value=\"1\"> Upload File &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
        $data['file_mp3'] .= "<input name=\"chk_upload_file\" type=\"radio\" value=\"0\" checked> Keep File <br>";
      } else
        $data['file_mp3'] = "<input name=\"chk_upload_file\" type=\"hidden\" value=\"1\">";
			//option
			if($data['options']){
				$options = unserialize($data['options']);
			}
			
			
			$res_op = $DB->query("select * from epaper_option n, epaper_option_desc nd  
														where  n.oe_id=nd.oe_id
														AND nd.lang='$lang'
														AND n.display=1 
														order by n.op_order, nd.oe_id DESC ");
			while ($r_op = $DB->fetch_row($res_op)){
				$r_op['otitle'] = $func->HTML($r_op['otitle']);			
				$r_op['op_value'] = $options[$r_op['oe_id']];
				$this->skin->assign('row', $r_op);
				$this->skin->parse("edit.row_option");
			}
			
    }else{
      $mess = $vnT->lang['not_found'] . " ID : " . $id;
      $url = $this->linkUrl;
      $func->html_redirect($url, $mess);
    }
     
		 
		$data['list_display'] = vnT_HTML::list_yesno ("display",$data['display']) ;
		
		$data["html_content"] = $vnT->editor->doDisplay('description', $data['description'], '100%', '450', "Default"); 
    $data['err'] = $err;
    $data['link_action'] = $this->linkUrl . "&sub=edit&id=$id";
    
    /*assign the array to a template variable*/
    $this->skin->assign('data', $data);
    
    $this->skin->parse("edit");
    return $this->skin->text("edit");
  }
  
  /**
   * function do_Del 
   * Xoa 1 ... n  gioi thieu 
   **/
  function do_Del ($lang)
  {
    global $func, $DB, $conf, $vnT;

		$id = (int) $vnT->input['id'];
		$ext = $vnT->input["ext"];
		$del = 0;
		$qr = "";
			
		if ($id != 0) {
			$ids = $id;
		}
		if (isset($vnT->input["del_id"])){
			$ids = implode(',', $vnT->input["del_id"]);
		} 		
 		 
		$res = $DB->query("SELECT * FROM epaper WHERE e_id IN (" . $ids . ") ");
    if ($DB->num_rows($res))
    {			
      while ($row = $DB->fetch_row($res))  {
        $res_d = $DB->query("SELECT id FROM epaper_desc WHERE e_id=".$row['e_id']." AND lang<>'".$lang."' ");
				if(!$DB->num_rows($res_d))
				{
					$DB->query("DELETE FROM epaper WHERE  e_id=".$row['e_id']."  ");
					$DB->query('DELETE FROM epaper_picture WHERE e_id IN ('.$ids.')');
					$DB->query('DELETE FROM epaper_comment WHERE id IN ('.$ids.')');
				}	
				$DB->query("DELETE FROM epaper_desc WHERE  e_id=".$row['e_id']." AND lang='".$lang."' ");	
      }
      $mess = $vnT->lang["del_success"];
			//xoa cache
      $func->clear_cache();
    } else  {
      $mess = $vnT->lang["del_failt"];
    } 
		 
		$ext_page = str_replace("|", "&", $ext);
		$url = $this->linkUrl . "&{$ext_page}";
		$func->html_redirect($url, $mess);
  }
  
  /**
   * function render_row 
   * list cac record
   **/
  function render_row ($row_info, $lang)
  {
    global $func, $DB, $conf, $vnT;
    $row = $row_info;
    // Xu ly tung ROW
    $id = $row['e_id'];
    $row_id = "row_" . $id;
    $output['check_box'] = vnT_HTML::checkbox("del_id[]", $id, 0, " ");
    $link_edit = $this->linkUrl . '&sub=edit&id=' . $id . '&ext=' . $row['ext_page'];
    $link_del = "javascript:del_item('" . $this->linkUrl . "&sub=del&id=" . $id . "&ext=" . $row['ext_page'] . "')";
    $link_pic = "?mod=epaper&act=pic_epaper&id={$id}&lang=$lang&sub=edit";
		
			
		if ($row['picture']){
			$output['picture']= get_pic_epaper($row['picture'],50);
		}else $output['picture'] ="No image";
 		
		$output['price'] = "<input name=\"txtPrice[{$id}]\" type=\"text\" style=\"text-align:center\" size=\"10\"  value=\"{$row['price']}\" onchange='javascript:do_check($id)' />";
    	 
    $output['title'] = "<strong><a href='" . $link_edit . "' >" . $func->HTML($row['title']) . "</a></strong>";
		$output['title'] .= '<div style="padding:2px;"> <span class=font_err >(MS: <b >'.$row['maso'].'</b>)</div>'; 
		 
 		$info = "<div style='padding:2px;'>".$vnT->lang['date_post']." : <b>".@date("d/m/Y",$row['date_post'])."</b></div>";
 		$info .=  "<div style='padding:2px;'>".$vnT->lang['views']." : <strong>".(int)$row['views']."</strong></div>";
		$output['info'] = $info ;
		
		$output['pic_other'] = '<a href="' . $link_pic . '"><img src="' . $vnT->dir_images . '/ico_hinhanh.gif"  alt="Picture "></a>';
		
		$output['e_order'] = $row['ext']."<input name=\"txt_Order[{$id}]\" type=\"text\" size=\"3\" maxlength=\"50\" style=\"text-align:center\" value=\"{$row['e_order']}\"  onkeypress=\"return is_num(event,'txtOrder')\" onchange='javascript:do_check($id)' />";
		
		$link_display = $this->linkUrl.$row['ext_link']; 		
    if ($row['display'] == 1) {
      $display = "<a href='".$link_display."&do_hidden=$id' title='".$vnT->lang['click_do_hidden']."' ><img src=\"" . $vnT->dir_images . "/dispay.gif\" width=15  /></a>";
    } else {
      $display = "<a href='".$link_display."&do_display=$id'  title='".$vnT->lang['click_do_display']."' ><img src=\"" . $vnT->dir_images . "/nodispay.gif\"  width=15 /></a>";
    }
		
    $output['action'] = '<input name=h_id[]" type="hidden" value="' . $id . '" />';    
    $output['action'] .= '<a href="' . $link_edit . '"><img src="' . $vnT->dir_images . '/edit.gif"  alt="Edit "></a>&nbsp;';
		$output['action'] .= $display.'&nbsp;';
    $output['action'] .= '<a href="' . $link_del . '"><img src="' . $vnT->dir_images . '/delete.gif"  alt="Delete "></a>';
    return $output;
  }
  
  /**
   * function do_Manage() 
   * Quan ly cac gioi thieu
   **/
  function do_Manage ($lang)
  {
    global $vnT, $func, $DB, $conf;
    $vnT->html->addStyleSheet( $vnT->dir_js . "/jquery_ui/themes/base/ui.all.css");
		$vnT->html->addScript($vnT->dir_js . "/jquery_ui/ui.core.js");		
		$vnT->html->addScript($vnT->dir_js . "/jquery_ui/ui.datepicker.js");  
    $vnT->html->addScriptDeclaration("
	 		$(function() {
				$('#date_begin').datepicker({
						showOn: 'button',
						buttonImage: '".$vnT->dir_images."/calendar.gif',
						buttonImageOnly: true,
						changeMonth: true,
						changeYear: true
					});
					$('#date_end').datepicker({
						showOn: 'button',
						buttonImage: '".$vnT->dir_images."/calendar.gif',
						buttonImageOnly: true,
						changeMonth: true,
						changeYear: true
					});

			});
		
		"); 
		
    //update
    if ($vnT->input["do_action"])
    {
      //xoa cache
      $func->clear_cache();
      if ($vnT->input["del_id"]) $h_id = $vnT->input["del_id"];
      switch ($vnT->input["do_action"])
      {
        case "do_edit":
						if (isset($vnT->input["txt_Order"])) $arr_order = $vnT->input["txt_Order"];
            $mess .= "- " . $vnT->lang['edit_success'] . " ID: <strong>";
            $str_mess = "";
            for ($i = 0; $i < count($h_id); $i ++)
            { 
							$dup['date_update'] = time();
							$dup['e_order'] = $arr_order[$h_id[$i]];
              $ok = $DB->do_update("epaper", $dup, "e_id=" . $h_id[$i]);
              if ($ok) {
                $str_mess .= $h_id[$i] . ", ";
								
              }
            }
            $mess .= substr($str_mess, 0, - 2) . "</strong><br>";
            $err = $func->html_mess($mess);
          break;
        case "do_hidden":
            $mess .= "- " . $vnT->lang['hidden_success'] . " ID: <strong>";
            for ($i = 0; $i < count($h_id); $i ++)
            {
              $dup['display'] = 0;
              $ok = $DB->do_update("epaper_desc", $dup, "lang='$lang' AND e_id=" . $h_id[$i]);
              if ($ok){
                $str_mess .= $h_id[$i] . ", ";
              }
            }
            $mess .= substr($str_mess, 0, - 2) . "</strong><br>";
            $err = $func->html_mess($mess);

          break;
        case "do_display":
            $mess .= "- " . $vnT->lang['display_success'] . "  ID: <strong>";
            for ($i = 0; $i < count($h_id); $i ++)
            {
              $dup['display'] = 1;
              $ok = $DB->do_update("epaper_desc", $dup, "lang='$lang' AND e_id=" . $h_id[$i]);
              if ($ok){
                $str_mess .= $h_id[$i] . ", ";
              }
            }
            $mess .= substr($str_mess, 0, - 2) . "</strong><br>";
            $err = $func->html_mess($mess);
          break;
      }
    }
		
		if((int)$vnT->input["do_display"]) {				
			$ok = $DB->query("Update epaper_desc SET display=1 WHERE lang='$lang' AND e_id=".$vnT->input["do_display"]);
			if($ok){ 
				$mess .= "- " . $vnT->lang['display_success'] . "  ID: <strong>".$vnT->input["do_display"] . "</strong><br>";	
				$err = $func->html_mess($mess);
			}        
			//xoa cache
      $func->clear_cache();
		}
		if((int)$vnT->input["do_hidden"]) {				
			$ok = $DB->query("Update epaper_desc SET display=0 WHERE lang='$lang' AND e_id=".$vnT->input["do_hidden"]);
			if($ok){ 
				$mess .= "- " . $vnT->lang['display_success'] . "  ID: <strong>".$vnT->input["do_hidden"] . "</strong><br>";	
				$err = $func->html_mess($mess);
			}    
			//xoa cache
      $func->clear_cache();
		}
    
		$p = ((int) $vnT->input['p']) ? $p = $vnT->input['p'] : 1;
		$n = ($conf['record']) ? $conf['record'] : 30;
		
		$cat_id = ((int) $vnT->input['cat_id']) ?   $vnT->input['cat_id'] : 0;
		$status_id = ((int) $vnT->input['status_id']) ?   $vnT->input['status_id'] : 0; 
		
		$search = ($vnT->input['search']) ?  $vnT->input['search'] : "e_order";
		$keyword = ($vnT->input['keyword']) ?  $vnT->input['keyword'] : "";
		$direction = ($vnT->input['direction']) ?   $vnT->input['direction'] : "DESC";
		$adminid = ((int) $vnT->input['adminid']) ?   $vnT->input['adminid'] : 0;
		$date_begin = ($vnT->input['date_begin']) ?  $vnT->input['date_begin'] : "";
		$date_end = ($vnT->input['date_end']) ?  $vnT->input['date_end'] : "";
		
		$where ="  ";
	
		if(!empty($cat_id)){
			$a_cat_id = List_SubCat($cat_id);
			$a_cat_id  = substr($a_cat_id,0,-1);
			if (empty($a_cat_id))
				$where .=" and FIND_IN_SET('$cat_id',cat_id)<>0 ";
			else{
				$tmp = explode(",",$a_cat_id);
				$str_= " FIND_IN_SET('$cat_id',cat_id)<>0 ";	
				for ($i=0;$i<count($tmp);$i++){
					$str_ .=" or FIND_IN_SET('$tmp[$i]',cat_id)<>0 ";
				}
				$where .=" and (".$str_.") ";
			} 
			$ext_page .="cat_id=$cat_id|";	
			$ext.="&cat_id=$cat_id";
		}
 		 
		if($status_id){
			$where .=" and status=$status_id ";
			$ext_page.="status_id=$status_id|";
			$ext.="&status_id={$status_id}";
		}
		
		if($date_begin || $date_end )
		{
			$tmp1 = @explode("/", $date_begin);
			$time_begin = @mktime(0, 0, 0, $tmp1[1], $tmp1[0], $tmp1[2]);
			
			$tmp2 = @explode("/", $date_end);
			$time_end = @mktime(23, 59, 59, $tmp2[1], $tmp2[0], $tmp2[2]);
			
			$where.=" AND (date_post BETWEEN {$time_begin} AND {$time_end} ) ";
			$ext.="&date_begin=".$date_begin."&date_end=".$date_end;
			$ext_page.= "date_begin=".$date_begin."|date_end=".$date_end."|";
		}
		
		if(!empty($search)){
			$ext_page.="search=$search|";
			$ext.="&search={$search}";
		}
		
		if(!empty($keyword)){
			switch($search){
				case "e_id" : $where .=" and  p.e_id = $keyword ";   break;
				case "e_order" : $where .=" and  p.e_id = $keyword, date_post DESC ";   break;
				case "date_post" : $where .=" and DATE_FORMAT(FROM_UNIXTIME(date_post),'%d/%m/%Y') = '{$keyword}' "; break;
				default :$where .=" and $search like '%$keyword%' ";break;		
			}
			
			$ext_page.="keyword=$keyword|";
			$ext.="&keyword={$keyword}";
		}
		 
		$order_by = ($search=="e_id") ? " order by p.e_id $direction " : " order by $search $direction ";
		$order_by = ($search=="e_order") ? " order by p.e_order $direction , date_post DESC" : " order by $search $direction ";
		$ext_page=$ext_page."direction=$direction|p=$p";
		$ext.="&direction=$direction";
		 
    $query = $DB->query("SELECT p.e_id
													FROM epaper p, epaper_desc pd
													WHERE p.e_id=pd.e_id 
													AND pd.lang='$lang'
													$where");
    $totals = intval($DB->num_rows($query));    
		
    $num_pages = ceil($totals / $n);
    if ($p > $num_pages) $p = $num_pages;
    if ($p < 1) $p = 1;
    $start = ($p - 1) * $n;
    
    $nav = $func->paginate($totals, $n, $ext, $p);
    
    $table['link_action'] = $this->linkUrl . "{$ext}&p=$p";
		$ext_link = $ext."&p=$p" ;
		
    $table['title'] = array(
			'check_box' => "<input type=\"checkbox\" name=\"checkall\" id=\"checkall\" class=\"checkbox\" />|5%|center" , 
			'e_order' => $vnT->lang['order']."|5%|center",	
			'picture' => $vnT->lang['picture']."|10%|center",				
			'title' => $vnT->lang['epaper_name']."|35%|left",
			'pic_other' => $vnT->lang['other_pic']."|10%|center",
			'info' =>$vnT->lang['infomartion']. "|20%|left",
			'action' => "Action|15%|center"
		);
    $sql = "SELECT * FROM epaper p, epaper_desc pd
						WHERE p.e_id=pd.e_id AND pd.lang='$lang' $where 
						ORDER BY e_order DESC , date_post ASC LIMIT $start,$n";
    $reuslt = $DB->query($sql);
    if ($DB->num_rows($reuslt)){
      $row = $DB->get_array($result);
      for ($i = 0; $i < count($row); $i ++){
				$row[$i]['ext_link'] = $ext_link ;
				$row[$i]['ext_page'] = $ext_page;
        $row_info = $this->render_row($row[$i],$lang);
        $row_field[$i] = $row_info;
        $row_field[$i]['stt'] = ($i + 1);
        $row_field[$i]['row_id'] = "row_" . $row[$i]['e_id'];
        $row_field[$i]['ext'] = "";
      }
      $table['row'] = $row_field;
    } else {
      $table['row'] = array();
      $table['extra'] = "<div align=center class=font_err >" . $vnT->lang['no_have_epaper'] ."</div>";
    }
		$table['button'] = '<input type="button" name="btnHidden" value=" '.$vnT->lang['hidden'].' " class="button" onclick="do_submit(\'do_hidden\')">&nbsp;';
		$table['button'] .= '<input type="button" name="btnDisplay" value=" '.$vnT->lang['display'].' " class="button" onclick="do_submit(\'do_display\')">&nbsp;';
		$table['button'] .= '<input type="button" name="btnEdit" value=" '.$vnT->lang['update'].' " class="button" onclick="do_submit(\'do_edit\')">&nbsp;';
		$table['button'] .= '<input type="button" name="btnDel" value=" '.$vnT->lang['delete'].' " class="button" onclick="del_selected(\''.$this->linkUrl.'&sub=del&ext='.$ext_page.'\')">';
    
    $table_list = $func->ShowTable($table);
    $data['table_list'] = $table_list;
    $data['totals'] = $totals;
    
    $data['err'] = $err;
    $data['nav'] = $nav;
    
    /*assign the array to a template variable*/
    $this->skin->assign('data', $data);
    
    $this->skin->parse("manage");
    return $this->skin->text("manage");
  }
	
  // end class
}

?>
