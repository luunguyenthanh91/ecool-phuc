<?php
/*================================================================================*\
|| 							Name code : epaper.php 		 			                 							# ||
||  				Copyright © 2007 by Thai Son - CMS vnTRUST                          # ||
\*================================================================================*/
/**
* @version : 1.0
* @date upgrade : 11/12/2007 by Thai Son
**/

if (!defined('IN_vnT')){
     die('Hacking attempt!');
}
$vntModule = new vntModule();
class vntModule
{
  var $output = "";
  var $skin = "";
  var $linkUrl = "";
  var $module = "epaper";
	var $action = "epaper_status";
	
  /**
   * function vntModule ()
   * Khoi tao 
   **/
  function vntModule ()
  {
    global $Template, $vnT, $func, $DB, $conf;
    require_once ("function_".$this->module.".php");
    loadSetting();
    $lang = ($vnT->input['lang']) ? $lang = $vnT->input['lang'] : $func->get_lang_default();
    $this->linkUrl = "?mod=".$this->module."&act=".$this->action."&lang=" . $lang;
		
    switch ($vnT->input['sub'])
    {      
      case 'edit':
	   		 $nd['content']=$this->do_Edit($lang);
      break;
			default:
        $nd['f_title'] = "Cập nhật trạng thái sản phẩm";
        $nd['content'] = $this->do_Manage($lang);
        break;
    }
    $nd['menu'] = $func->getToolbar_Small($this->module,$this->action, $lang);
		
    $Template->assign("data", $nd);
    $Template->parse("box_main");
    $vnT->output .= $Template->text("box_main");
  
  }    

function render_row($row_info,$lang) {
	global $func,$DB,$conf,$vnT;
	$row=$row_info;
	// Xu ly tung ROW
		$id = $row['e_id'];
		$row_id = "row_".$id;
		$output['check_box'] = "<input type=\"checkbox\" name=\"del_id[]\" value=\"{$id}\" class=\"checkbox\" onclick=\"select_row('{$row_id}')\">";
		$link_edit ="?mod=epaper&act=epaper&sub=edit&id={$id}&ext={$row['ext_page']}";
		$link_del = "javascript:del_item('".$this->linkUrl."&sub=del&id={$id}&ext={$row['ext_page']}')";
		if ($row['picture']){
			$output['picture']= get_pic_epaper($row['picture'],50);
		}else $output['picture'] ="No image";
	
	$output['maso'] = "<b class=font_err>".$row['maso']."</b>&nbsp;";
	$output['p_name'] = "<a href=\"{$link_edit}\">".$func->HTML($row['p_name'])."</a>";
	
	$output['status'] = List_Status_Pro ("status[$id]",$row['status'],$lang," style='width:95%;' onchange='javascript:do_check($id)' ");
	
	if ($row['display']==1){
		$is_display ="<img src=\"".$vnT->dir_images."/dispay.gif\" width=15  />" ;
	}else{
		$is_display ="<img src=\"".$vnT->dir_images."/nodispay.gif\"  width=15 />" ;
	}
	
	$output['action'] = "
		<input name=\"h_id[]\" type=\"hidden\" value=\"{$id}\" />
		<a href=\"{$link_edit}\"><img src=\"".$vnT->dir_images."/edit.gif\"  alt=\"Edit \"></a>&nbsp;	
		{$is_display}&nbsp;";
		
		
	return $output;
}
//============
function do_Manage($lang){
	global $vnT,$func,$DB,$conf;
	
	
	//update
    if ($vnT->input["do_action"])
    {
      //xoa cache
      $func->clear_cache();
      if ($vnT->input["del_id"]) $h_id = $vnT->input["del_id"];
      switch ($vnT->input["do_action"])
      {
        case "do_edit":
						$mess="";
						if (isset($_POST["status"])) $arr_status = $_POST["status"] ; else $arr_status="";
						$mess.="- ".$vnT->lang['edit_success']." SP ID: <strong>";
						$str_mess="";
						for ($i=0;$i<count($h_id);$i++) {
							$dup['status'] = $arr_status[$h_id[$i]];
							$dup['date_update'] = time();
							$ok=$DB->do_update("epaper",$dup,"e_id={$h_id[$i]}");
              if ($ok) {
                $str_mess .= $h_id[$i] . ", ";
              }
            }
            $mess .= substr($str_mess, 0, - 2) . "</strong><br>";
            $err = $func->html_mess($mess);
          break;         
      }
    }
    
		$p = ((int) $vnT->input['p']) ? $p = $vnT->input['p'] : 1;
		$n = ($conf['record']) ? $conf['record'] : 30;
		
		$cat_id = ((int) $vnT->input['cat_id']) ?   $vnT->input['cat_id'] : 0;
		$status_id = ((int) $vnT->input['status_id']) ?   $vnT->input['status_id'] : 0;
		$search = ($vnT->input['search']) ?   $vnT->input['search'] : "e_id";
		$keyword = ($vnT->input['keyword']) ?  $vnT->input['keyword'] : "";
		$direction = ($vnT->input['direction']) ?  $vnT->input['direction'] : "DESC";
		$where ="  ";
	
		if(!empty($cat_id)){
			$a_cat_id = List_SubCat($cat_id);
			$a_cat_id  = substr($a_cat_id,0,-1);
			if (empty($a_cat_id))
				$where .=" and FIND_IN_SET('$cat_id',cat_id)<>0 ";
			else{
				$tmp = explode(",",$a_cat_id);
				$str_= " FIND_IN_SET('$cat_id',cat_id)<>0 ";	
				for ($i=0;$i<count($tmp);$i++){
					$str_ .=" or FIND_IN_SET('$tmp[$i]',cat_id)<>0 ";
				}
				$where .=" and (".$str_.") ";
			} 
			$ext_page .="cat_id=$cat_id|";	
			$ext.="&cat_id=$cat_id";
		}
		
		if($status_id){
			$where .=" and status=$status_id ";
			$ext_page.="search=$search|";
			$ext.="&search={$search}";
		}
		
		if(!empty($search)){
			$ext_page.="search=$search|";
			$ext.="&search={$search}";
		}
		
		if(!empty($keyword)){
			switch($search){
				case "e_id" : $where .=" and  p.e_id = $keyword ";   break;
				case "date_post" : $where .=" and DATE_FORMAT(FROM_UNIXTIME(date_post),'%d/%m/%Y') = '{$keyword}' "; break;
				default :$where .=" and $search like '%$keyword%' ";break;		
			}
			
			$ext_page.="keyword=$keyword|";
			$ext.="&keyword={$keyword}";
		}
		 
		$order_by = ($search=="e_id") ? " order by p.e_id $direction " : " order by $search $direction ";
		$ext_page=$ext_page."direction=$direction|p=$p";
		$ext.="&direction=$direction";
	
    $query = $DB->query("SELECT p.e_id
												FROM epaper p, epaper_desc pd
												WHERE p.e_id=pd.e_id 
												AND pd.lang='$lang'
												$where");
    $totals = intval($DB->num_rows($query));    
		
    $num_pages = ceil($totals / $n);
    if ($p > $num_pages) $p = $num_pages;
    if ($p < 1) $p = 1;
    $start = ($p - 1) * $n;
    
    $nav = $func->paginate($totals, $n, $ext, $p);
    
    $table['link_action'] = $this->linkUrl . "&sub=manage";
    $table['title'] = array(
				'check_box' => "<input type=\"checkbox\" name=\"checkall\" id=\"checkall\" class=\"checkbox\" />|5%|center" , 
				'picture' => "H&#236;nh|5%|center",
				'maso' => $vnT->lang['maso']."|10%|center",
				'p_name' => $vnT->lang['epaper_name']."|30%|left",
				'status' => $vnT->lang['pro_status']."|25%|center",
				'action' => "Action|15%|center"
				);
    
    $sql = "SELECT *
						FROM epaper p, epaper_desc pd
						WHERE p.e_id=pd.e_id 
						AND pd.lang='$lang'
						$where 
						$order_by LIMIT $start,$n"; 
    $reuslt = $DB->query($sql);
    if ($DB->num_rows($reuslt))
    {
      $row = $DB->get_array($result);
      for ($i = 0; $i < count($row); $i ++)
      {
        $row_info = $this->render_row($row[$i],$lang);
        $row_field[$i] = $row_info;
        $row_field[$i]['stt'] = ($i + 1);
        $row_field[$i]['row_id'] = "row_" . $row[$i]['e_id'];
        $row_field[$i]['ext'] = "";
      }
      $table['row'] = $row_field;
    }
    else
    {
      $table['row'] = array();
      $table['extra'] = "<div align=center class=font_err >" . $vnT->lang['no_have_epaper'] ."</div>";
    }
    
		$table['button'] = '<input type="button" name="btnEdit" value=" '.$vnT->lang['update'].' " class="button" onclick="do_submit(\'do_edit\')">&nbsp;';

    
    $table_list = $func->ShowTable($table);
    $data['table_list'] = $table_list;
    $data['totals'] = $totals;
		$data['listcat']=Get_Cat($cat_id,$lang);
		$data['list_search']=List_Search($search);
		$data['list_direction']=List_Direction($direction);
		$data['list_status'] = List_Status_Pro("status_id",$status_id,$lang,"");
		$data['keyword'] = $keyword;

	 $data['link_fsearch'] = $this->linkUrl;	
	 $data['err'] = $err;
	 $data['nav'] = $nav;
	
	 return $this->html_manage($data);

}



//=============================================
function html_manage($data){
global $func,$conf,$vnT;
return<<<EOF
<script language="javascript" src="js/highslide.js"></script>
{$data['err']}
<br>
<table width="100%"  border="0" align="center" cellpadding="2" cellspacing="2" class="tableborder">
  <form action="{$data['link_fsearch']}" method="post" name="catform">
    <tr>
	  <td  width="18%"  ><strong>{$vnT->lang['totals']} : </strong>  </td>
      <td  ><span class=font_err><strong>{$data['totals']}</strong></span> </td>
    </tr>
	<tr>
	   <td ><strong>{$vnT->lang['category']} : </strong>  </td>
      <td > {$data['listcat']} </td>
    </tr>
    <tr>
    	<td ><strong>{$vnT->lang['status']} : </strong>  </td>
      <td > {$data['list_status']} </td>
    </tr>
    <tr>
      <td  colspan="2"><strong>{$vnT->lang['search']} :</strong> {$data['list_search']} <strong> {$vnT->lang['keyword']} :</strong>  <input name="keyword" value="{$data['keyword']}" size="20" type="text" /> <strong>{$vnT->lang['order']}</strong> {$data['list_direction']}&nbsp;<input type="submit" name="btnGo" value=" Search " class="button"></td>
    </tr>
	<tr>
	   <td  height="10"> </td>
	 </tr>
	

  </form>
</table>

<br />
{$data['table_list']}

<table width="100%"  border="0" align="center" cellspacing="1" class="bg_tab" cellpadding="1">
  <tr>
    <td  height="25">{$data['nav']}</td>
  </tr>
</table>
<br />
EOF;
}
// end class
}
?>