<?php
/*================================================================================*\
|| 							Name code : pic_epaper.php 		 		            	  ||
||  				Copyright @2008 by Thai Son - CMS vnTRUST                     ||
\*================================================================================*/
/**
 * @version : 2.0
 * @date upgrade : 09/01/2009 by Thai Son
 **/
if (! defined('IN_vnT'))
{
  die('Hacking attempt!');
}

$vntModule = new vntModule();
class vntModule
{
  var $output = "";
  var $skin = "";
  var $linkUrl = "";
  var $module = "epaper";
  var $action = "pic_epaper";

  function vntModule (){
    global $Template, $vnT, $func, $DB, $conf;
    require_once ("function_" . $this->module . ".php");
		loadSetting();
    $this->skin = new XiTemplate(DIR_MODULE.DS.$this->module . "_ad" . DS . "html" . DS . $this->action . ".tpl");
    $this->skin->assign('LANG', $vnT->lang);
    $lang = ($vnT->input['lang']) ? $lang = $vnT->input['lang'] : $func->get_lang_default();
    $this->linkUrl = "?mod=" . $this->module . "&act=" . $this->action . "&lang=" . $lang;
    $vnT->html->addStyleSheet( "modules/".$this->module."_ad/css/".$this->module.".css");
		$vnT->html->addScript( "modules/".$this->module."_ad/js/".$this->module.".js");
    switch ($vnT->input['sub']){
      case 'edit':
        $nd['f_title'] = $vnT->lang['edit_pic'];
        $nd['content'] = $this->do_Edit($lang);
        break;
			case 'edit_pic' :
			 $nd['f_title']=  $vnT->lang["edit_pic"];
			 $nd['content']= $this->do_Edit_Pic($lang);
				break;
      case 'del':
        $this->do_Del($lang);
        break;
			case 'del_all':
        $this->do_Del_All($lang);
        break;
      default:
        $nd['f_title'] = $vnT->lang['edit_pic'];
        $nd['content'] = $this->do_Manage($lang);
        break;
    }
    $nd['menu'] = $func->getToolbar($this->module, $this->action, $lang);
    $nd['row_lang'] = $func->html_lang("?mod=" . $this->module . "&act=" . $this->action, $lang);
    
    $Template->assign("data", $nd);
    $Template->parse("box_main");
    $vnT->output .= $Template->text("box_main");
  }
  function do_Edit ($lang){
    global $vnT, $func, $DB, $conf;
		$vnT->html->addStyleSheet($vnT->dir_js . "/plupload/jquery.plupload.queue/css/jquery.plupload.queue.css");
    $vnT->html->addScript($vnT->dir_js . "/plupload/browserplus-min.js");
    $vnT->html->addScript($vnT->dir_js . "/plupload/plupload.full.js");
    $vnT->html->addScript($vnT->dir_js . "/plupload/i18n/vi.js");
    $vnT->html->addScript($vnT->dir_js . "/plupload/jquery.plupload.queue/jquery.plupload.queue.js");
	  $id = (int) $vnT->input['id'];
		if($vnT->input['btnUpdate']){
			$arr_order = $vnT->input['txt_Order'];
			// up hinh phu
			foreach ($arr_order as $key => $value){
			 	$DB->query("UPDATE epaper_picture SET pic_order=".(int)$value." WHERE id=".$key);
			}
		 	$func->clear_cache();
		 	//insert adminlog
			$func->insertlog("Edit Order",$_GET['act'],$id);
		 	$err = "Cập nhật thứ tự thành công";
		 	$url = $this->linkUrl."&sub=edit&id=$id";
		 	$func->html_redirect($url,$err);
		}
		$res_pro= $DB->query("SELECT p.picture, p.e_id, pd.title
													FROM epaper p, epaper_desc pd
													WHERE p.e_id=pd.e_id AND pd.lang='$lang' AND p.e_id=$id") ;
		if($row_p = $DB->fetch_row($res_pro)){
		
		}else{
			$mess = $vnT->lang['not_found']." ID : ".$id ; 
			$url = $this->linkUrl;
			$func->html_redirect($url,$mess);
		}
		$result = $DB->query("SELECT * FROM epaper_picture WHERE e_id={$id} ORDER BY pic_order ASC, id");
		if($num_old = $DB->num_rows($result)){
			$list_pic_old="";
			$i=0;
			while ($row=$DB->fetch_row($query)){
				$i++;
				$pic = get_pic_epaper_pages($id,$row['picture'],100);
				$first = ''; $last = '';
				$text_stt = "<strong>STT: ".$row['pic_order']."</strong>";
				$list_pic_old .= "<td align=\"center\" valign='bottom'><table border=\"0\" bgcolor=\"#CCCCCC\" cellspacing=\"1\" cellpadding=\"1\" > <tr><td align=\"center\" bgcolor=\"#FFFFFF\" ><a href=\"".$this->linkUrl."&sub=edit_pic&e_id=$id&id={$row['id']}\">".$pic."</a></td> </tr><tr>
				<td align=\"center\"><a href=\"".$this->linkUrl."&sub=del&e_id=$id&id={$row['id']}\"><img src=\"{$vnT->dir_images}/delete.gif\" align=absmiddle /> Delete</a></td>
				</tr></table> STT: <input name='txt_Order[".$row['id']."]' id='txt_Order' type='text' size='3' value='".(int)$row['pic_order']."' style='text-align:center'  /></td>";
				if ($i%7==0) { $list_pic_old .= "</tr><tr>"; }
			}
		}else{
			$list_pic_old = "<td class=font_err align=center>Chưa có hình  nào</td>";
		}
		$data['list_pic_old']	= $list_pic_old;
		if (!empty($row_p['picture'])){
			$pic = get_pic_epaper ($row_p['picture'],100);
			$data['pic'] = "{$pic}<br>";
		}
		$data['num'] = $num;
		$data['e_id'] = $row_p['e_id'];
		$data['title'] = $func->HTML($row_p['title']);
		$dir_pages = "pages".$id;
		$dir = get_dir_upload($dir_pages) ;
		$folder_upload = $conf['rootpath']."vnt_upload/epaper/".$dir ;
		$w_pic = ($vnT->setting['img_width']) ? $vnT->setting['img_width'] : 2000;
    $w_thumb = ($vnT->setting['imgthumb_width']) ? $vnT->setting['imgthumb_width'] : 200;
		$data['w_pic'] = $w_pic;
		$data['w_thumb'] = $w_thumb;
		$data['folder_upload'] = $folder_upload ;
		$data['folderpath'] =   $conf['rootpath']."vnt_upload/epaper" ;
		$data['folder'] = $dir ;
		$data['max_upload'] = ini_get('upload_max_filesize');
		
    $data['err'] = $err;
    $data['link_action'] = $this->linkUrl . "&sub=edit&id=$id";
    
    /*assign the array to a template variable*/
    $this->skin->assign('data', $data);
    
    $this->skin->parse("edit");
    return $this->skin->text("edit");
  }
	
	
	/**
   * function do_Edit_Pic
   * Cap nhat 
   **/
  function do_Edit_Pic ($lang)
  {
    global $vnT, $func, $DB, $conf;
    
    $id = (int) $vnT->input['id'];
		$e_id = (int) $vnT->input['e_id'];
			
		
		if (isset($_POST['btnUpdate']))
		{
			if ($_POST['chk_upload']==1)
			{
				// tao thu muc cho san pham 
				$dir_pages = "pages".$e_id;
				$dir = $dir_pages;//get_dir_upload($dir_pages) ;
				$w_thum = ($vnT->setting['img_width_grid']) ? $vnT->setting['img_width_grid'] : 100 ;
				$w = 2000;
				if (!empty($_FILES['image']) && ($_FILES['image']['name']!="") )
				{
					$up['path']= MOD_DIR_UPLOAD;
					$up['dir']= $dir;
					$up['file']= $_FILES['image'];
					$up['type']= "hinh";
					$up['resize']= 1;
					$up['w']= $w;
					$up['thum'] = 1;
					$up['w_thum'] = 150;
					$result = $vnT->File->Upload($up);
					if (empty($result['err'])) 
					{
						$picture= $result['link'];
					} else {
						$err = $func->html_err($result['err']);
					}
					
					if($dir && $picture){
						$picture = $dir."/".$picture ;
					}
					
					$dup['picture'] = $picture;	
				}
			}
			
			$dup['pic_order'] = $vnT->input['pic_order'];
			$dup['pic_name'] = $vnT->input['pic_name'];
			$dup['description'] = $DB->mySQLSafe($_POST['description']); 
			$ok = $DB->do_update ("epaper_picture",$dup,"e_id=$e_id and id=$id");
			if ($ok)
			{
				 
				//xoa cache
				$func->clear_cache();
				//insert adminlog
				 $func->insertlog("Edit Pic",$_GET['act'],$id);
					 
				$mess = $vnT->lang['edit_success'];
				$url =  $this->linkUrl . "&sub=edit&id=$e_id";
				$func->html_redirect($url,$mess);
			}else{
				$err = $func->html_err($DB->debug());
			}
		}
	
	
		$res_pro = $DB->query("SELECT p.picture, p.e_id,  pd.title
						FROM epaper p, epaper_desc pd
						WHERE p.e_id=pd.e_id 
						AND pd.lang='$lang'
						AND p.e_id=$e_id") ;
		if($row_p = $DB->fetch_row($res_pro)){
		
		}else{
			$mess = $vnT->lang['not_found']." ID : ".$id ; 
			$url = $this->linkUrl;
		  $func->html_redirect($url,$mess);
		}
		
		$result = $DB->query("select * from epaper_picture where id=$id");
		if($row = $DB->fetch_row($result))
		{
			$data['pic_order'] = $row['pic_order'];
			$data['picture'] .= "<img src=\"../vnt_upload/epaper/".$row['picture']."\" width='300px'  ><br>";
			$data['picture'] .= "<input name=\"chk_upload\" type=\"radio\" value=\"1\"> Upload Image &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
			$data['picture'] .= "<input name=\"chk_upload\" type=\"radio\" value=\"0\" checked> Keep Image <br>";
			$data['picture'] .= "<input name=\"pic_old\" type=\"hidden\" value=\"".$row['picture']."\" >";
			
			$data['pic_name'] = $row['pic_name'];
			$data['style'] = ($row['pic_order']==-2147483648 || $row['pic_order']==2147483647) ? 'style="display:none;"' : '';
			
			$data["html_content"] = $vnT->editor->doDisplay('description', $row['description'], '100%', '350', "Default"); 
			
		}
		
		
		$data['e_id'] = $e_id;
		if (!empty($row_p['picture'])) 
		{
			$pic = get_pic_epaper ($row_p['picture'],100);
			$data['pic'] = "{$pic}<br>";
		}

		$data['title'] = $func->HTML($row_p['title']);
    
    $data['err'] = $err;
    $data['link_action'] = $this->linkUrl . "&sub=edit_pic&e_id=$e_id&id=$id";
    
    /*assign the array to a template variable*/
    $this->skin->assign('data', $data);
    
    $this->skin->parse("edit_pic");
    return $this->skin->text("edit_pic");
  }
  
  /**
   * function do_Del 
   * Xoa 1 ... n  gioi thieu 
   **/
  function do_Del ($lang)
  {
    global $func, $DB, $conf, $vnT;
    $e_id	= (int) $vnT->input['e_id'];
    $id = (int) $vnT->input['id'];
    $ext = $vnT->input["ext"];
    $del = 0;
    $qr = "";
    
    if ($id != 0)
    {
      $ids = $id;
    }
    if (isset($vnT->input["del_id"]))
    {
      $ids = implode(',', $vnT->input["del_id"]);
    }
    
		// Del Image
		$query = $DB->query("SELECT picture FROM epaper_picture WHERE e_id=$e_id and  id IN (" . $ids . ")");
		if ($img=$DB->fetch_row($query)) 
		{
			if ($img['picture']){
				$src_name = substr($img['picture'],strrpos($img['picture'],"/")+1);
				$dir = substr($img['picture'],0,strrpos($img['picture'],"/"));
				$path_file = MOD_DIR_UPLOAD.$img['picture'];
				$path_filethumbs = MOD_DIR_UPLOAD.$dir."/thumbs/".$src_name;
						
				if ( file_exists($path_file) && $img['picture'] ) @unlink($path_file);
				if ( file_exists($path_filethumbs) ) @unlink($path_filethumbs);
			}
		}
	// End del image

    $query = 'DELETE FROM epaper_picture WHERE e_id='.$e_id.' and id IN (' . $ids . ')';
    if ($ok = $DB->query($query))
    {
			 
      $mess = $vnT->lang["del_success"];
    } else
      $mess = $vnT->lang["del_failt"];
    
    $ext_page = str_replace("|", "&", $ext);
    $url = $this->linkUrl."&sub=edit&id=".$e_id;
    $func->html_redirect($url, $mess);
  }
	
	/**
   * function do_Del_All 
   * Xoa 1 ... n  
   **/
	 
	 function do_Del_All ($lang)
  {
    global $func, $DB, $conf, $vnT;

    $id = (int) $vnT->input['id'];
    $ext = $vnT->input["ext"];
    $del = 0;
    $qr = "";
    
    if ($id != 0)
    {
      $ids = $id;
    }
    if (isset($vnT->input["del_id"]))
    {
      $ids = implode(',', $vnT->input["del_id"]);
    }
    
		// Del Image
		$query = $DB->query("SELECT picture FROM epaper_picture WHERE e_id IN (" . $ids . ")");
		if ($img=$DB->fetch_row($query)) 
		{
			if ($img['picture']){
				$src_name = substr($img['picture'],strrpos($img['picture'],"/")+1);
				$dir = substr($img['picture'],0,strrpos($img['picture'],"/"));
				$path_file = MOD_DIR_UPLOAD.$img['picture'];
				$path_filethumbs = MOD_DIR_UPLOAD.$dir."/thumbs/".$src_name;
						
				if ( file_exists($path_file) && $img['picture'] ) @unlink($path_file);
				if ( file_exists($path_filethumbs) ) @unlink($path_filethumbs);
			}
		}
	// End del image

    $query = 'DELETE FROM epaper_picture WHERE e_id IN (' . $ids . ')';
    if ($ok = $DB->query($query))
    {
      $mess = $vnT->lang["del_success"];
    } else
      $mess = $vnT->lang["del_failt"];
    
    $ext_page = str_replace("|", "&", $ext);
    $url = $this->linkUrl;
    $func->html_redirect($url, $mess);
  }
  function render_row ($row_info, $lang){
    global $func, $DB, $conf, $vnT;
    $row = $row_info;
    // Xu ly tung ROW
    $id = $row['e_id'];
    $row_id = "row_" . $id;
    $output['check_box'] = vnT_HTML::checkbox("del_id[]", $id, 0, " ");
    $link_edit = $this->linkUrl . '&sub=edit&id=' . $id . '&ext=' . $row['ext_page'];
   	$link_del = "javascript:del_item('".$this->linkUrl."&sub=del_all&id={$id}&ext={$row['ext_page']}')";
		$link_pro = "?mod=epaper&act=epaper&sub=edit&id={$id}&lang=$lang";
		if ($row['picture']){
			$output['picture'] = get_pic_epaper ($row['picture'],50);
		}else $output['picture'] ="No image";
		$output['title'] = "<strong><a href=\"{$link_pro}\">".$func->HTML($row['title'])."</a></strong>";
		$pic_other ='<table border="0" cellspacing="2" cellpadding="2">
									  <tr>';
		$res = $DB->query("SELECT * from epaper_picture WHERE e_id=".$id." ORDER BY pic_order, id");
		$i=0;
		if($num = $DB->num_rows($res)){
			while ($r = $DB->fetch_row($res)){
				$link_update = $this->linkUrl."&sub=edit_pic&e_id={$id}&id=".$r['id'];
				$link_del_pic = $this->linkUrl."&sub=del&e_id={$id}&id=".$r['id'];
				$order = 'stt: '.$r['pic_order'];
				if($r['pic_order']==-2147483648) $order = '[Trang đầu]';
				if($r['pic_order']==2147483647) $order = '[Trang cuối]';
				if($i%10 == 0)
					$pic_other .="</tr><tr>";
				$pic_other.="<td align='center' style='border:1px solid #ccc;padding:1px;'><table border=0 cellspacing=1 cellpadding=1 align='center'>
					<tr><td align='center'>".$order."</td></tr>
		<tr>
			<td align='center'><a href='".$link_update."' title='Cập nhật lại '>".get_pic_epaper_pages($id,$r['picture'],40)."</a></td>
		</tr>
		<tr>
			<td align='center'><a href=\"{$link_update}\"><img src=\"{$vnT->dir_images}/edit.gif\" title=\"Edit \" width='30px'></a>&nbsp;<a href=\"{$link_del_pic}\"><img src=\"{$vnT->dir_images}/delete.gif\"  title=\"Delete \" width='30px'></a><td>
		</tr>
	</table></td>";
				$i++;
			}
		}else{
			$pic_other.='<td align="center">Chưa có</td>';
		}
		$pic_other.='</tr></table>';
	
	
	  $output['pic_other'] =$pic_other;
	
		
		if ($row['display']==1){
			$display ="<img src=\"{$vnT->dir_images}/dispay.gif\" width=15  />" ;
		}else{
			$display ="<img src=\"{$vnT->dir_images}/nodispay.gif\"  width=15 />" ;
		}
		
    $output['action'] = '<input name=h_id[]" type="hidden" value="' . $id . '" />';    
    $output['action'] .= '<a href="' . $link_edit . '"><img src="' . $vnT->dir_images . '/edit.gif"  alt="Edit "></a>&nbsp;';
		$output['action'] .= $display.'&nbsp;';
    $output['action'] .= '<a href="' . $link_del . '"><img src="' . $vnT->dir_images . '/delete.gif"  alt="Delete "></a>';
    return $output;
  }
  
  /**
   * function do_Manage() 
   * Quan ly cac gioi thieu
   **/
  function do_Manage ($lang)
  {
    global $vnT, $func, $DB, $conf;
    
    
		$p = ((int) $vnT->input['p']) ? $p = $vnT->input['p'] : 1;
		$n = ($conf['record']) ? $conf['record'] : 30;
		
		$e_id = ((int) $vnT->input['e_id']) ? $e_id = $vnT->input['e_id'] : 0;
		$search = ($vnT->input['search']) ? $search = $vnT->input['search'] : "e_id";
		$keyword = ($vnT->input['keyword']) ? $keyword = $vnT->input['keyword'] : "";
		$direction = ($vnT->input['direction']) ? $direction = $vnT->input['direction'] : "DESC";
		
		$where ="  ";
	
		if(!empty($e_id)){
			$a_e_id = List_SubCat($e_id);
			$a_e_id  = substr($a_e_id,0,-1);
			if (empty($a_e_id))
				$where .=" and FIND_IN_SET('$e_id',e_id)<>0 ";
			else{
				$tmp = explode(",",$a_e_id);
				$str_= " FIND_IN_SET('$e_id',e_id)<>0 ";	
				for ($i=0;$i<count($tmp);$i++){
					$str_ .=" or FIND_IN_SET('$tmp[$i]',e_id)<>0 ";
				}
				$where .=" and (".$str_.") ";
			} 
			$ext_page .="e_id=$e_id|";	
			$ext.="&e_id=$e_id";
		}
		
		if(!empty($search)){
			$ext_page.="search=$search|";
			$ext.="&search={$search}";
		}
		
		if(!empty($keyword)){
			switch($search){
				case "e_id" : $where .=" and p.e_id = $keyword "; $search="p.e_id"; break;
				case "date_post" : $where .=" and DATE_FORMAT(FROM_UNIXTIME(date_post),'%d/%m/%Y') = '{$keyword}' "; break;
				default :$where .=" and $search like '%$keyword%' ";break;		
			}
			
			$ext_page.="keyword=$keyword|";
			$ext.="&keyword={$keyword}";
		}
		
		$search = ($search=="e_id") ? $search="p.e_id" : $search ; 
		$order_by = "order by $search $direction ";
		$ext_page=$ext_page."direction=$direction|p=$p";
		$ext.="&direction=$direction";
	
    $query = $DB->query("SELECT p.e_id
					FROM epaper p, epaper_desc pd
					WHERE p.e_id=pd.e_id 
					AND pd.lang='$lang'
					$where ");
    $totals = intval($DB->num_rows($query));    
		
    $num_pages = ceil($totals / $n);
    if ($p > $num_pages) $p = $num_pages;
    if ($p < 1) $p = 1;
    $start = ($p - 1) * $n;
    
    $nav = $func->paginate($totals, $n, $ext, $p);
    
    $table['link_action'] = $this->linkUrl . "&sub=manage";
    $table['title'] = array(
				'check_box' => "<input type=\"checkbox\" name=\"checkall\" id=\"checkall\" class=\"checkbox\" />|5%|center" , 
				'picture' => "H&#236;nh|5%|center",
				'title' => "Tên|25%|left",
				'pic_other' => "Hình phụ||center",
				'action' => "Action|15%|center"
				);
		

    $sql = "SELECT *
						FROM epaper p, epaper_desc pd
						WHERE p.e_id=pd.e_id 
						AND pd.lang='$lang'
						$where 
						$order_by LIMIT $start,$n";
    //print "sql = ".$sql."<br>";
    $reuslt = $DB->query($sql);
    if ($DB->num_rows($reuslt))
    {
      $row = $DB->get_array($result);
      for ($i = 0; $i < count($row); $i ++)
      {
        $row_info = $this->render_row($row[$i],$lang);
        $row_field[$i] = $row_info;
        $row_field[$i]['stt'] = ($i + 1);
        $row_field[$i]['row_id'] = "row_" . $row[$i]['e_id'];
        $row_field[$i]['ext'] = "";
      }
      $table['row'] = $row_field;
    }
    else
    {
      $table['row'] = array();
      $table['extra'] = "<div align=center class=font_err >" . $vnT->lang['no_have_epaper'] ."</div>";
    }
    
		$table['button'] = '<input type="button" name="btnDel" value=" '.$vnT->lang['delete'].' " class="button" onclick="del_selected(\''.$this->linkUrl.'&sub=del_all&ext='.$ext_page.'\')">';
    
    $table_list = $func->ShowTable($table);
    $data['table_list'] = $table_list;
    $data['totals'] = $totals;
		$data['listcat']=Get_Cat($e_id,$lang);
		$data['list_search']= '<select class="select" name="search" size="1" style="width: 100px;"><option value="e_id"> ID </option></select>';
 		$data['keyword'] = $keyword;
	
    $data['err'] = $err;
    $data['nav'] = $nav;
    
    /*assign the array to a template variable*/
    $this->skin->assign('data', $data);
    
    $this->skin->parse("manage");
    return $this->skin->text("manage");
  }
  
// end class
}

?>