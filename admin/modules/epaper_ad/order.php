<?php
/*================================================================================*\
|| 							Name code : order.php 		 		            	  ||
||  				Copyright @2008 by Thai Son - CMS vnTRUST                     ||
\*================================================================================*/
/**
 * @version : 2.0
 * @date upgrade : 09/01/2009 by Thai Son
 **/
if (! defined('IN_vnT'))
{
  die('Hacking attempt!');
}

$vntModule = new vntModule();
class vntModule
{
  var $output = "";
  var $skin = "";
  var $linkUrl = "";
  var $module = "epaper";
	var $action = "order";
	
  /**
   * function vntModule ()
   * Khoi tao 
   **/
  function vntModule ()
  {
    global $Template, $vnT, $func, $DB, $conf;
    require_once ("function_".$this->module.".php");
		loadSetting();
		
    $this->skin = new XiTemplate(DIR_MODULE . DS . $this->module . "_ad" . DS . "html" . DS . $this->action . ".tpl");
    $this->skin->assign('LANG', $vnT->lang);
    
    $lang = ($vnT->input['lang']) ? $lang = $vnT->input['lang'] : $func->get_lang_default();
    $this->linkUrl = "?mod=".$this->module."&act=".$this->action."&lang=" . $lang;
		
		$vnT->html->addStyleSheet( "modules/".$this->module."_ad/css/".$this->module.".css");
		
    switch ($vnT->input['sub'])
    {

      case 'edit':
        $nd['f_title'] = $vnT->lang['task_order'];
        $nd['content'] = $this->do_Edit($lang);
        break;
			case 'send_email' : 
				$nd['f_title']= $vnT->lang["send_email_order"];
				$nd['content']=$this->do_Send_Email($lang);
			  break;
      case 'del':
        $this->do_Del($lang);
        break;
      default:
        $nd['f_title'] = $vnT->lang['manage_order'];
        $nd['content'] = $this->do_Manage($lang);
        break;
    }
    $nd['menu'] = $func->getToolbar_Small($this->module,$this->action, $lang);
    $nd['row_lang'] = $func->html_lang("?mod=".$this->module."&act=".$this->action, $lang);
		
    $Template->assign("data", $nd);
    $Template->parse("box_main");
    $vnT->output .= $Template->text("box_main");
  
  }
  
  /**
   * function do_Send_Email 
   * Cap nhat 
   **/
	function do_Send_Email()
	{
		global $vnT, $func, $DB, $conf;
		
		$id = $vnT->input['id'];
		
		$data=$_POST;
		if (isset($data["btnSend"])){
			//send email
			$sent = $func->doSendMail($data['email'],$_POST['subject'],$_POST['content'], $conf['email']);
			$err="G&#7917;i email th&#224;nh c&#244;ng";
			$url = $this->linkUrl;
			$func->html_redirect($url, $err);
			
		}
		
		$sql = "select * from epaper_order where id = '{$id}' " ;
		$result = $DB->query($sql);
		if ($row = $DB->fetch_row($result)){
			$data['full_name'] = $row["d_name"];
			$data["email"] = $row["email"];
		}
		
		$data["html_content"] = $vnT->editor->doDisplay('content', $vnT->input['content'], '100%', '400', "Normal");
				
		$data["err"] = $err;
		$data["link_action"] = $this->linkUrl."&sub=send_email&id=$id";
	
		/*assign the array to a template variable*/
    $this->skin->assign('data', $data);
    
    $this->skin->parse("send_email");
    return $this->skin->text("send_email");
		
	}
  
  /**
   * function do_Edit 
   * Cap nhat 
   **/
  function do_Edit ($lang)
  {
    global $vnT, $func, $DB, $conf;
		$vnT->html->addStyleSheet( $vnT->dir_js . "/jquery_ui/themes/base/ui.all.css");
		$vnT->html->addScript($vnT->dir_js . "/jquery_ui/ui.core.js");		
		$vnT->html->addScript($vnT->dir_js . "/jquery_ui/ui.datepicker.js");  
    $vnT->html->addScriptDeclaration("
	 		$(function() {
				$('#ship_date').datepicker({
						showOn: 'button',
						buttonImage: '".$vnT->dir_images."/calendar.gif',
						buttonImageOnly: true,
						changeMonth: true,
						changeYear: true
					});					 
			});
		
		");  

		
    $id = (int)$vnT->input['id'];
    $data['date_ship'] =date("d/m/Y");
		
    if ($vnT->input['btnSave'])
    {
      $data = $_POST;			
      
      if (empty($err))
      {
				$status = $vnT->input["status"];
				$note = $vnT->input["note"];
				
				if ($vnT->input["ship_date"]!=""){
					$tmp = explode("/",$vnT->input["ship_date"]);
					$ship_date = mktime(date("H"),date("i"),0,$tmp[1],$tmp[0],$tmp[2]);
				}else{
					$ship_date = time();
				}
				
				$dataup["ship_date"] = $ship_date ;
				$dataup["status"] = $status ;
				$dataup["content"] = $note ;
				
				$ok = $DB->do_update("epaper_order",$dataup,"id=$id");
        if ($ok)
        {
					//xoa cache
          $func->clear_cache();
					
          //insert adminlog
          $func->insertlog("Edit", $_GET['act'], $id);
          
          $err = $vnT->lang["edit_success"];
          $url = $this->linkUrl . "&sub=edit&id=$id";
          $func->html_redirect($url, $err);
        } else
          $err = $func->html_err($vnT->lang["edit_failt"].$DB->debug());
      }
    
    }
    
    $sql ="select * from epaper_order where id ='{$id}' ";
		$result = $DB->query ($sql);
		if ($data = $DB->fetch_row($result))
		{
	
			// nguoi mua
			$info_customer = get_info_address($data);			
			
			
			$data['date_order'] = date("H:i, d/m/Y",$data['datesubmit']);
			
			if ($data['ship_date']=="")
				$data['text_ship_date'] ="Chưa giao hàng...";
			else
				$data['text_ship_date'] = date("H:i, d/m/Y",$data['ship_date']);
		
			if ($data['content'])
				$data['content'] = $func->HTML($data['content']) ;
			
			// chi tiet gio hang
			$data['payment_name'] = get_title_payment($data['payment'],$lang);
			 
		}	
	
		
		$data['id'] = $id;
		$data['list_status'] = list_status_order($data['status']);
		
		if ($data['ship_date'])
			$data['ship_date'] = date("d/m/Y",$data['ship_date']);
	
		//$data['link_print'] = "../?".$conf['cmd']."=mod:epaper|act:track_order|do:print|orderID:".$id;
    
    $data['err'] = $err;
    $data['link_action'] = $this->linkUrl . "&sub=edit&id=$id";
    
    /*assign the array to a template variable*/
    $this->skin->assign('data', $data);
    
    $this->skin->parse("edit");
    return $this->skin->text("edit");
  }
  
  /**
   * function do_Del 
   * Xoa 1 ... n  gioi thieu 
   **/
  function do_Del ($lang)
  {
    global $func, $DB, $conf, $vnT;

		$id = (int) $vnT->input['id'];
		$ext = $vnT->input["ext"];
		$del = 0;
		$qr = "";
			
		if ($id != 0) {
			$ids = $id;
		}
		if (isset($vnT->input["del_id"])){
			$ids = implode(',', $vnT->input["del_id"]);
		} 		
	
		$query = 'DELETE FROM epaper_order WHERE id IN ('.$ids.')' ;
		if ($ok = $DB->query($query))
		{
			$mess = $vnT->lang["del_success"] ;
		} else
			$mess = $vnT->lang["del_failt"] ;
		$ext_page = str_replace("|", "&", $ext);
		$url = $this->linkUrl . "&{$ext_page}";
		$func->html_redirect($url, $mess);
  }
  
  /**
   * function render_row 
   * list cac record
   **/
  function render_row ($row_info, $lang)
  {
    global $func, $DB, $conf, $vnT;
    $row = $row_info;
    // Xu ly tung ROW
    $id = $row['id'];
    $row_id = "row_" . $id;
    $output['check_box'] = vnT_HTML::checkbox("del_id[]", $id, 0, " ");
    $link_edit = $this->linkUrl . '&sub=edit&id=' . $id . '&ext=' . $row['ext_page'];
    $link_del = "javascript:del_item('" . $this->linkUrl . "&sub=del&id=" . $id . "&ext=" . $row['ext_page'] . "')";
    

		$link_send_email = $this->linkUrl."&sub=send_email&id=".$id;
		$output['id'] = "<a href=\"{$link_edit}\"><strong>".$row['id']."</strong></a>";;
		
		
		$type_mem = ($row['mem_id']==0) ? "Guest" : "Member" ;
	// nguoi mua
		$info_customer ="Họ tên : <b>".$row['d_name']."</b> <span class=font_err>(".$type_mem.")</span>";
		$info_customer .="<br><span class='font_small'>Email : ".$row['email'];
		$info_customer .="<br>Điện thoại : ".$row['phone'];
		$info_customer .="<br>Địa chỉ : ".$row['address'];
		if($row['city'])
		{
			$info_customer .=  ", ".get_city_name($row['city']) ;
		}
			
		
		$output['info_customer'] = $info_customer;
		
		
		$output['date_order'] = date("H:i, d/m/Y",$row['datesubmit']);
		
		$output['status'] = $vnT->lang['status_order'.$row['status'].''];
		
		if ($row['display']==1){
			$display ="<img src=\"{$vnT->dir_images}/dispay.gif\" width=15  />" ;
		}else{
			$display ="<img src=\"{$vnT->dir_images}/nodispay.gif\"  width=15 />" ;
		}
		
    $output['action'] = '<input name=h_id[]" type="hidden" value="' . $id . '" />';    
    $output['action'] .= '<a href="' . $link_edit . '"><img src="' . $vnT->dir_images . '/edit.gif"  alt="Edit "></a>&nbsp;';
		$output['action'] .= '<a href="' . $link_send_email . '"><img src="' . $vnT->dir_images . '/send_mail.gif"  alt="Send Email "></a>&nbsp;';
    $output['action'] .= '<a href="' . $link_del . '"><img src="' . $vnT->dir_images . '/delete.gif"  alt="Delete "></a>';
    return $output;
  }
  
  /**
   * function do_Manage() 
   * Quan ly cac gioi thieu
   **/
  function do_Manage ($lang)
  {
    global $vnT, $func, $DB, $conf;
    
    //update
    if ($vnT->input["do_action"])
    {
      //xoa cache
      $func->clear_cache();
      if ($vnT->input["del_id"]) $h_id = $vnT->input["del_id"];
      switch ($vnT->input["do_action"])
      {
        case "do_edit":
            if (isset($vnT->input["txt_Order"])) $arr_order = $vnT->input["txt_Order"];
            $mess .= "- " . $vnT->lang['edit_success'] . " ID: <strong>";
            $str_mess = "";
            for ($i = 0; $i < count($h_id); $i ++)
            {
              $dup['order'] = $arr_order[$h_id[$i]];
              $ok = $DB->do_update("modname", $dup, "id=" . $h_id[$i]);
              if ($ok) {
                $str_mess .= $h_id[$i] . ", ";
              }
            }
            $mess .= substr($str_mess, 0, - 2) . "</strong><br>";
            $err = $func->html_mess($mess);
          break;
        case "do_hidden":
            $mess .= "- " . $vnT->lang['hidden_success'] . " ID: <strong>";
            for ($i = 0; $i < count($h_id); $i ++)
            {
              $dup['display'] = 0;
              $ok = $DB->do_update("modname", $dup, "id=" . $h_id[$i]);
              if ($ok){
                $str_mess .= $h_id[$i] . ", ";
              }
            }
            $mess .= substr($str_mess, 0, - 2) . "</strong><br>";
            $err = $func->html_mess($mess);

          break;
        case "do_display":
            $mess .= "- " . $vnT->lang['display_success'] . "  ID: <strong>";
            for ($i = 0; $i < count($h_id); $i ++)
            {
              $dup['display'] = 1;
              $ok = $DB->do_update("modname", $dup, "id=" . $h_id[$i]);
              if ($ok){
                $str_mess .= $h_id[$i] . ", ";
              }
            }
            $mess .= substr($str_mess, 0, - 2) . "</strong><br>";
            $err = $func->html_mess($mess);
          break;
      }
    }
    
		$p = ((int) $vnT->input['p']) ? $p = $vnT->input['p'] : 1;
		$n = ($conf['record']) ? $conf['record'] : 30;
		
		$keyword	= ($vnT->input['keyword']) ? $vnT->input['keyword'] : "";
		
		$where ="where id <>0 ";
		if($keyword){
			$where .=" and order_code like '%$keyword%' ";
			$ext.="&keyword=$keyword";
		}
		
    $query = $DB->query("SELECT id FROM epaper_order $where  ");
    $totals = intval($DB->num_rows($query));    
		
    $num_pages = ceil($totals / $n);
    if ($p > $num_pages) $p = $num_pages;
    if ($p < 1) $p = 1;
    $start = ($p - 1) * $n;
    
    $nav = $func->paginate($totals, $n, $ext, $p);
    
    $table['link_action'] = $this->linkUrl . "&sub=manage";
    $table['title'] = array(
				'check_box' => "<input type=\"checkbox\" name=\"checkall\" id=\"checkall\" class=\"checkbox\" />|5%|center" , 
				'id' => "Order ID |15%|center",
				'info_customer' => "Khách hàng|30%|left",
				'date_order' => "Ngày đặt hàng |15%|center",
				'status' => "Trạng thái|10%|center",
				'action' => "Action|15%|center"
				);
    
    $sql = "SELECT * FROM epaper_order  $where ORDER BY datesubmit DESC  LIMIT $start,$n";
   // print "sql = ".$sql."<br>";
    $reuslt = $DB->query($sql);
    if ($DB->num_rows($reuslt))
    {
      $row = $DB->get_array($result);
      for ($i = 0; $i < count($row); $i ++)
      {
        $row_info = $this->render_row($row[$i],$lang);
        $row_field[$i] = $row_info;
        $row_field[$i]['stt'] = ($i + 1);
        $row_field[$i]['row_id'] = "row_" . $row[$i]['id'];
        $row_field[$i]['ext'] = "";
      }
      $table['row'] = $row_field;
    }
    else
    {
      $table['row'] = array();
      $table['extra'] = "<div align=center class=font_err >" . $vnT->lang['no_have_order'] ."</div>";
    }
    
		$table['button'] = '<input type="button" name="btnDel" value=" '.$vnT->lang['delete'].' " class="button" onclick="del_selected(\''.$this->linkUrl.'&sub=del&ext='.$ext_page.'\')">';
    
    $table_list = $func->ShowTable($table);
    $data['table_list'] = $table_list;
    $data['totals'] = $totals;
		$data['keyword'] = $keyword;
    $data['err'] = $err;
    $data['nav'] = $nav;
    
    /*assign the array to a template variable*/
    $this->skin->assign('data', $data);
    
    $this->skin->parse("manage");
    return $this->skin->text("manage");
  }
	
  // end class
}

?>