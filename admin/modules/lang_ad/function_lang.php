<?php
/*================================================================================*\
|| 							Name code : funtions_mail_temp.php 		 			               		  # ||
||  				Copyright © 2007 by Thai Son - CMS vnTRUST                          # ||
\*================================================================================*/
/**
 * @version : 1.0
 * @date upgrade : 11/12/2007 by Thai Son
 **/
if (! defined('IN_vnT')) {
  die('Hacking attempt!');
}
define('DIR_UPLOAD', '../vnt_upload/lang');

function getToolbar ($act = "lang")
{
  global $func, $DB, $conf, $vnT;
  $menu = array(
    "add" => array(
      'icon' => "i_add" , 
      'title' => "Add" , 
      'link' => "?mod=lang&act=$act&sub=add") , 
    "edit" => array(
      'icon' => "i_edit" , 
      'title' => "Edit" , 
      'link' => "javascript:alert('" . $vnT->lang['action_no_active'] . "')") , 
    "manage" => array(
      'icon' => "i_manage" , 
      'title' => "Manage" , 
      'link' => "?mod=lang&act=$act") , 
    "help" => array(
      'icon' => "i_help" , 
      'title' => "Help" , 
      'link' => "'help/index.php?mod=lang&act=$act','AdminCPHelp',1000, 600, 'yes','center'" , 
      'newwin' => 1));
  return $func->getMenu($menu);
}

//=====
function List_Lang ($did)
{
  global $func, $DB, $conf;
  $text = "<select size=1 name=\"name\" onchange=\"location.href='?mod=lang&act=lang&sub=edit_lang&name='+this.value\"  style=\"width:150px\">";
  $query = $DB->query("SELECT * FROM language ");
  while ($row = $DB->fetch_row($query)) {
    if ($row['name'] == $did)
      $text .= "<option value=\"{$row['name']}\" selected>{$row['title']}</option>";
    else
      $text .= "<option value=\"{$row['name']}\" >{$row['title']}</option>";
  }
  $text .= "</select>";
  return $text;
}

//=====
function List_Type ($did)
{
  global $func, $DB, $conf;
  $text = "<select size=1 name=\"type\" style=\"width:150px\" onchange=\"submit();\" >";
  $text .= "<option value=\"global\" selected>Global</option>";
  if ("modules" == $did)
    $text .= "<option value=\"modules\" selected>Modules</option>";
  else
    $text .= "<option value=\"modules\" >Modules</option>";
  if ("blocks" == $did)
    $text .= "<option value=\"blocks\" selected>Blocks</option>";
  else
    $text .= "<option value=\"blocks\" >Blocks</option>";
  $text .= "</select>";
  return $text;
}

//= List_Phrase
function List_Phrase ($type, $did = "global")
{
  global $func, $DB, $conf;
  $text = "<select size=1 name=\"phrase\"  style=\"width:200px\" onchange=\"submit();\" >";
  $text .= "<option value=\"\" selected>-- Select Phrase --</option>";
  $sql = "select * from lang_phrase where type='$type' order by fieldname";
  $result = $DB->query($sql);
  while ($row = $DB->fetch_row($result)) {
    $fieldname = $row['fieldname'];
    $title = $row['title'];
    if ($fieldname == $did)
      $text .= "<option value=\"{$fieldname}\" selected>{$title}</option>";
    else
      $text .= "<option value=\"{$fieldname}\" >{$title}</option>";
  }
  $text .= "</select>";
  return $text;
}
?>