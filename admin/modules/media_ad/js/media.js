/* Keypress, Click Handle */
var KEYPR = {
	isCtrl : false,
	isShift : false,
	shiftOffset : 0,
	allowKey : [ 112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122, 123 ],
	isSelectable: false,
	isFileSelectable: false,
	init : function(){
		$('body').keyup(function(e){
			if( ! $(e.target).is('.dynamic') && $.inArray( e.keyCode, KEYPR.allowKey ) == -1 ){
				e.preventDefault();
			}else{
				return;
			}

			// Ctrl key unpress
			if( e.keyCode == 17 ){
				KEYPR.isCtrl = false;
			}else if( e.keyCode == 16 ){
				KEYPR.isShift = false;
			}
		});

		$('body').keydown(function(e){
			if( ! $(e.target).is('.dynamic') && $.inArray( e.keyCode, KEYPR.allowKey ) == -1 ){
				e.preventDefault();
			}else{
				return;
			}

			// Ctrl key press
			if( e.keyCode == 17 /* Ctrl */ ){
				KEYPR.isCtrl = true;
			}else if( e.keyCode == 27 /* ESC */ ){
				// Unselect all file
				$(".imgsel").removeClass("imgsel");
				LFILE.setSelFile();

				// Hide contextmenu
				//NVCMENU.hide();

				// Reset shift offset
				KEYPR.shiftOffset = 0;
			}else if( e.keyCode == 65 /* A */ && e.ctrlKey === true ){
				// Select all file
				$(".imgcontent").addClass("imgsel");
				//LFILE.setSelFile();

				// Hide contextmenu
				//NVCMENU.hide();
			}else if( e.keyCode == 16 /* Shift */ ){
				KEYPR.isShift = true;
			}else if( e.keyCode == 46 /* Del */ ){
				// Delete file
				if( $('.imgsel').length && $("span#delete_file").attr("title") == '1' ){
					//filedelete();
				}
			}else if( e.keyCode == 88 /* X */ ){
				// Move file
				if( $('.imgsel').length && $("span#move_file").attr("title") == '1' ){
					//move();
				}
			}
		});

		// Unselect file when click on wrap area
		$('#imglist').click(function(e){
			if( KEYPR.isSelectable == false ){
				if( $(e.target).is('#imglist') ){
					$(".imgsel").removeClass("imgsel");
				}
			}

			KEYPR.isSelectable = false;
		});
	}
};

function checkNewSize() {
	var a = $("input[name=newWidth]").val(), b = $("input[name=newHeight]").val(), d = [], e = $("input[name=origWidth]").val(), g = $("input[name=origHeight]").val(), h = calSizeMax(e, g, nv_max_width, nv_max_height);
	e = calSizeMin(e, g, nv_min_width, nv_min_height);
	if (a == "" || !is_numeric(a)) {
		d = [LANG.errorEmptyX, "newWidth"];
	} else {
		if (a > h[0]) {
			d = [LANG.errorMaxX, "newWidth"];
		} else {
			if (a < e[0]) {
				d = [LANG.errorMinX, "newWidth"];
			} else {
				if (b == "" || !is_numeric(b)) {
					d = [LANG.errorEmptyY, "newHeight"];
				} else {
					if (b > h[1]) {
						d = [LANG.errorMaxY, "newHeight"];
					} else {
						if (b < e[1]) {
							d = [LANG.errorMinY, "newHeight"];
						}
					}
				}
			}
		}
	}
	$("div[title=createInfo]").find("div").remove();
	if ( typeof d[0] != "undefined") {
		$("div[title=createInfo]").prepend('<div class="red">' + d[0] + "</div>");
		$("input[name='" + d[1] + "']").select();
		return false;
	}
	a = calSize(a, b, 360, 230);
	$("img[name=myFile2]").width(a[0]).height(a[1]);
	return true;
}
function pathList(a, b) {
	var d = [];
	$("#foldertree").find("span").each(function() {
		if ($(this).attr("title") == b || $(this).attr("title") != "" && $(this).is("." + a)) {
			d[d.length] = $(this).attr("title");
		}
	});
	return d;
}

function insertvaluetofield() 
{
	if ($("input[name=CKEditorFuncNum]").val() > 0 ) 
	{			
		var a = $("input[name=CKEditorFuncNum]").val() ;
		var c = $("input[name=selFile]").val(); 
		var f =  $("span#foldervalue").attr("title") ;
		var d = ROOT_URI +"vnt_upload/"+ f + "/" + c;				
		
		window.opener.CKEDITOR.tools.callFunction(a, d);		 
		window.close();
		
	} else {
		var module = $("input[name=module]").val(); 
		var path_file =  $("span#path").attr("title"); 
		var src_file =  $("span#foldervalue").attr("title") + "/" +$("input[name=selFile]").val();		
		if(module){
			src_file = src_file.replace(path_file+"/", "");
			src_thumb = ROOT + "vnt_upload/"+ $("span#foldervalue").attr("title") + "/thumbs/" +$("input[name=selFile]").val();
			self.parent.send_to_modules(obj_return,src_file,src_thumb) ;				
		}else{
			show_pic = $("input[name=show_pic]").val(); 
			var d = ROOT_URI +"vnt_upload/"+src_file ;
			pic ='';
			if(show_pic){
				pic =	 ROOT + "vnt_upload/"+ src_file;
			}			
			self.parent.send_to_object(obj_return,d,pic) ;				
		}		
		
	}	
}
 
function download() {
	var c = $("input[name=selFile]").val();
	var e = $("img[title='" + c + "']").attr("name").split("|");
	p =  $("span#foldervalue").attr("title")  ;
	var link_file = ROOT+'vnt_upload/'+p+'/'+c;
	window.open(link_file );	
	//$("iframe#Fdownload").attr("src", remote_url + "dlimg&path=" + p + "&img=" + c);
}
function preview() {
	$("div.dynamic").text("");
	$("input.dynamic").val("");
	var a = $("input[name=selFile]").val(), e = LANG.upload_size + ": ";
	var d = $("img[title='" + a + "']").attr("name").split("|");
	b = $("span#foldervalue").attr("title") ;
	if (d[4] == "image" || d[3] == "swf") {
		var g = calSize(d[1], d[2], 360, 230);
		e += d[1] + " x " + d[2] + " pixels (" + d[5] + ")<br />";
				
		if(d[4] == "image")
		{
			 $("div#fileView").html('<img style="border:2px solid #F0F0F0;" width="' + g[0] + '" height="' + g[1] + '" src="' + ROOT + 'vnt_upload/'+ b + "/" + a + '" />') 
		}else{
			 $("div#fileView").append("<div class='flash'></div>");
		 	 $("div#fileView .flash").flash({	src : ROOT + "vnt_upload/" + b + "/" + a,		width : g[0],		height : g[1]	}, {	version : 8	});
		}
	} else {
		e += d[5] + "<br />";
		b = $("div[title='" + a + "'] div").html();
		$("div#fileView").html(b);
	}
	e += LANG.pubdate + ": " + d[7];
	$("#fileInfoAlt").html($("img[title='" + a + "']").attr("alt"));
	$("#fileInfoDetail").html(e);
	$("#fileInfoName").html(a);
	$("div#imgpreview").dialog({
		autoOpen : false,
		width : 388,
		modal : true,
		position : "center"
	}).dialog("open").dblclick(function() {
		$("div#imgpreview").dialog("close");
	});
}
 
function create() {
	$("div.dynamic").text("");
	$("input.dynamic").val("");
	var a = $("input[name=selFile]").val(), d = $("img[title='" + a + "']").attr("name");
	d = d.split("|");
	if (d[3] == "image") {
		b =  $("span#foldervalue").attr("title") ;
		$("input[name=origWidth]").val(d[0]);
		$("input[name=origHeight]").val(d[1]);
		var e = calSizeMax(d[0], d[1], nv_max_width, nv_max_height), g = calSizeMin(d[0], d[1], nv_min_width, nv_min_height);
		$("div[title=createInfo]").html("Max: " + e[0] + " x " + e[1] + ", Min: " + g[0] + " x " + g[1] + " (pixels)");
		e = calSize(d[0], d[1], 360, 230);
		$("img[name=myFile2]").width(e[0]).height(e[1]).attr("src", ROOT + b + "/" + a);
		$("#fileInfoDetail2").html(LANG.origSize + ": " + d[0] + " x " + d[1] + " pixels");
		$("#fileInfoName2").html(a);
		$("div#imgcreate").dialog({
			autoOpen : false,
			width : 650,
			modal : true,
			position : "center"
		}).dialog("open");
	}
}
function move() {	
	$("div.dynamic").text("");
	$("input.dynamic").prop("checked", false);
	$("select[name=newPath]").html("");
	var a = $("span#foldervalue").attr("title"), b = pathList("create_file", a), d, e, g = $("input[name=selFile]").val();
	for (e in b ) {
		d = a == b[e] ? ' selected="selected"' : "";
		$("select[name=newPath]").append('<option value="' + b[e] + '"' + d + ">" + b[e] + "</option>");
	}
	d = $("img[title='" + g + "']").attr("name").split("|");
	a = $("span#foldervalue").attr("title") ;
	
	$("div[title=pathFileName]").text(a + "/" + g);
	alert('move');
	
	$("div#filemove").dialog({
		autoOpen : false,
		width : 300,
		modal : true,
		position : "center"
	}).dialog("open");
}
function filerename() {
	$("div.dynamic, span.dynamic").text("");
	$("input.dynamic").val("");
	var a = $("input[name=selFile]").val();
	$("div#filerenameOrigName").text(a);
	$("input[name=filerenameNewName]").val(a.replace(/^(.+)\.([a-zA-Z0-9]+)$/, "$1"));
	$("span[title=Ext]").text("." + a.replace(/^(.+)\.([a-zA-Z0-9]+)$/, "$2"));
	$("input[name=filerenameAlt]").val($("img[title='" + a + "']").attr("alt"));
	$("div#filerename").dialog({
		autoOpen : false,
		width : 400,
		modal : true,
		position : "center"
	}).dialog("open");
}
function filedelete() {
	var a = $("input[name=selFile]").val(),  e = $("select[name=author]").val() == 1 ? "&author" : "";
	var f = $("img[title='" + a + "']").attr("name").split("|");
	var b = $("span#foldervalue").attr("title") ;
	var d = ($("select[name=imgtype]").val()) ? $("select[name=imgtype]").val() : 'image' ;
	confirm(LANG.upload_delimg_confirm + " " + a + " ?") && $.ajax({
		type : "POST",
		url : remote_url + "delimg",
		data : "path=" + b + "&file=" + a,
		success : function() {
			if ($("input[name=CKEditorFuncNum]").val()){
				$("#imglist").load("index.php?mod=media&act=editor&do=imglist&path=" + b + "&type=" + d + e + "&order=" + $("select[name=order]").val() + "&num=" + +randomNum(10));
			}else{
				$("#imglist").load(remote_url + "imglist&path=" + b + "&type=" + d + e + "&order=" + $("select[name=order]").val() + "&num=" + +randomNum(10));
			}
		}
	});
}

// Ham xu ly khi nhap chuot vao 1 file (Chuot trai, chuot giua lan chuot phai)
function fileMouseup( file, e ){

	// Khong xu ly neu jquery UI selectable dang kich hoat
	if( KEYPR.isFileSelectable == false ){

		a = $(file).attr("title");
		$("input[name=selFile]").val(a);		

		// Set shift offset
		if( e.which != 3 && ! KEYPR.isShift ){
			// Reset shift offset
			KEYPR.shiftOffset = 0;

			$.each( $('.imgcontent'), function(k, v){
				if( v == file ){
					KEYPR.shiftOffset = k;
					return false;
				}
			});
		}

		// e.which: 1: Left Mouse, 2: Center Mouse, 3: Right Mouse
		if( KEYPR.isCtrl ){
			if( $(file).is('.imgsel') && e.which != 3 ){
				$(file).removeClass('imgsel');
			}else{
				$(file).addClass('imgsel');
			}
		}else if( KEYPR.isShift && e.which != 3 ){
			var clickOffset = -1;
			$('.imgcontent').removeClass('imgsel');

			$.each( $('.imgcontent'), function(k, v){
				if( v == file ){
					clickOffset = k;
				}

				if( ( clickOffset == -1 && k >= KEYPR.shiftOffset ) || ( clickOffset != -1 && k <= KEYPR.shiftOffset ) || v == file ){
					if( ! $(v).is('.imgsel') ){
						$(v).addClass('imgsel');
					}
				}
			});
		}else{
			if( e.which != 3 || ( e.which == 3 && ! $(file).is('.imgsel') ) ){
				$('.imgsel').removeClass('imgsel');
				$(file).addClass('imgsel');
			}
		}

		//LFILE.setSelFile();

		if( e.which == 3 ){
			
			var b = $("input[name=CKEditorFuncNum]").val(), d = $("input[name=area]").val(), html = "<ul>";

			html += '<li id="select"><img style="margin-right:5px" src="' + ICON.select + '"/>' + LANG.select + '</li>'
			html += '<li id="download"><img style="margin-right:5px" src="' + ICON.download + '"/>' + LANG.download + '</li>';
			html += '<li id="filepreview"><img style="margin-right:5px" src="' + ICON.preview + '"/>' + LANG.preview + '</li>';

			if ($("span#move_file").attr("title") == "1") {
				html += '<li id="move"><img style="margin-right:5px" src="' + ICON.move + '"/>' + LANG.move + '</li>'
			}
			if ($("span#rename_file").attr("title") == "1") {
				html += '<li id="rename"><img style="margin-right:5px" src="' + ICON.rename + '"/>' + LANG.rename + '</li>'
			}
			if ($("span#delete_file").attr("title") == "1") {
				html += '<li id="filedelete"><img style="margin-right:5px" src="' + ICON.filedelete + '"/>' + LANG.upload_delfile + '</li>'
			}
			html += "</ul>";
			$("div#contextMenu").html(html)

		}

	}

	KEYPR.isFileSelectable = false;





}
function is_allowed_upload() {

	//$("input[name=upload]").parent().css("display", "block");
	//$("span#upload_file").attr("title") == "1" ? $("input[name=upload]").parent().parent().css("display", "block").next().css("display", "none") : $("input[name=upload]").parent().parent().css("display", "none").next().css("display", "block")
}
function folderClick(a) {
	var b = $(a).attr("title");
	
	if (b != $("span#foldervalue").attr("title")) {
  		
		$("span#foldervalue").attr("title", b);
		$("span#view_dir").attr("title", $(a).is(".view_dir") ? "1" : "0");
		$("span#create_dir").attr("title", $(a).is(".create_dir") ? "1" : "0");
		$("span#rename_dir").attr("title", $(a).is(".rename_dir") ? "1" : "0");
		$("span#delete_dir").attr("title", $(a).is(".delete_dir") ? "1" : "0");
		$("span#upload_file").attr("title", $(a).is(".upload_file") ? "1" : "0");
		$("span#create_file").attr("title", $(a).is(".create_file") ? "1" : "0");
		$("span#rename_file").attr("title", $(a).is(".rename_file") ? "1" : "0");
		$("span#delete_file").attr("title", $(a).is(".delete_file") ? "1" : "0");
		$("span#move_file").attr("title", $(a).is(".move_file") ? "1" : "0");
		$("span.folder").css("color", "");
		$(a).css("color", "red");
		 
		if ($(a).is(".view_dir")) {
			a = $("select[name=imgtype]").val();
			var d = $("input[name=selFile]").val(), e = $("select[name=author]").val() == 1 ? "&author" : "";
			$("div#imglist").html('<p style="padding:20px; text-align:center"><img src="' + DIR_IMAGE + '/load_bar.gif"/> please wait...</p>').load(remote_url + "imglist&path=" + b + "&imgfile=" + d + "&type=" + a + e + "&order=" + $("select[name=order]").val() + "&random=" + randomNum(10))
		} else {
			$("div#imglist").text("")
		}
		is_allowed_upload()
	}
}
function menuMouseup(a) {
	$(a).attr("title");
	$("span").attr("name", "");
	$(a).attr("name", "current");
	
	var b = "";
	//if ($(a).is(".create_dir")) {
		b += '<li id="createfolder"><img style="margin-right:5px" src="' + ICON.create + '"/>' + LANG.createfolder + '</li>'
	//}
	//if ($(a).is(".rename_dir")) {
	//	b += '<li id="renamefolder"><img style="margin-right:5px" src="' + ICON.rename + '"/>' + LANG.renamefolder + '</li>'
	//	}
	//if ($(a).is(".delete_dir")) {
	//	b += '<li id="deletefolder"><img style="margin-right:5px" src="' + ICON.filedelete + '"/>' + LANG.deletefolder + '</li>'
	//}
	if (b != "") {
		b = "<ul>" + b + "</ul>"
	}
	
	$("div#contextMenu").html(b)
}
function renamefolder() {
	var a = $("span[name=current]").attr("title").split("/");
	a = a[a.length - 1];
	$("input[name=foldername]").val(a);
	$("div#renamefolder").dialog("open")
}
function createfolder() {
	$("input[name=createfoldername]").val("");
	$("div#createfolder").dialog("open")
}
function deletefolder() {
	if (confirm(LANG.delete_folder)) {
		var a = $("span[name=current]").attr("title");
		$.ajax({
			type : "POST",
			url : remote_url + "delfolder&random=" + randomNum(10),
			data : "path=" + a,
			success : function(b) {
				b = b.split("_");
				if (b[0] == "ERROR") {
					alert(b[1])
				} else {
					b = a.split("/");
					a = "";
					for ( i = 0; i < b.length - 1; i++) {
						if (a != "") {
							a += "/"
						}
						a += b[i]
					}
					b = $("select[name=imgtype]").val();
					var d = $("select[name=author]").val() == 1 ? "&author" : "", e = $("span#path").attr("title"), g = $("input[name=selFile]").val();
					$("#imgfolder").load(remote_url + "folderlist&path=" + e + "&currentpath=" + a + "&random=" + randomNum(10));
					$("div#imglist").load(remote_url + "imglist&path=" + a + "&imgfile=" + g + "&type=" + b + d + "&order=" + $("select[name=order]").val() + "&order=" + $("select[name=order]").val() + "&random=" + randomNum(10))
				}
			}
		})
	}
}
function searchfile() {
	a = $("select[name=searchPath]").val(), q = $("input[name=q]").val();
	b = $("select[name=imgtype]").val();
	$("div#filesearch").dialog("close");
	$("#imglist").html('<p style="padding:20px; text-align:center"><img src="' + DIR_IMAGE + '/load_bar.gif"/> please wait...</p>').load(remote_url + 'imglist&path=' + a + '&type=' + b  + '&q=' + rawurlencode(q) + '&order=' + $('select[name=order]').val() + '&random=' + randomNum(10))
	return false;
}


var ICON = [];
ICON.select = ROOT + 'js/contextmenu/icons/select.png';
ICON.download = ROOT + 'js/contextmenu/icons/download.png';
ICON.preview = ROOT + 'js/contextmenu/icons/view.png';
ICON.create = ROOT + 'js/contextmenu/icons/copy.png';
ICON.move = ROOT + 'js/contextmenu/icons/move.png';
ICON.rename = ROOT + 'js/contextmenu/icons/rename.png';
ICON.filedelete = ROOT + 'js/contextmenu/icons/delete.png';
$(".vchange").change(function() {
	var a = $("span#foldervalue").attr("title"), b = $("input[name=selFile]").val(), d = $("select[name=imgtype]").val(), e = $(this).val() == 1 ? "&author" : "";
	$("#imglist").load(remote_url + "imglist&path=" + a + "&type=" + d + "&imgfile=" + b + e + "&order=" + $("select[name=order]").val() + "&random=" + randomNum(10))
});
$(".refresh a").click(function() {
	var a = $("span#foldervalue").attr("title"), b = $("select[name=imgtype]").val(), d = $("input[name=selFile]").val() , g = $("span#path").attr("title");
	/*$("#imgfolder").load(remote_url + "folderlist&path=" + g + "&currentpath=" + a + "&dirListRefresh&random=" + randomNum(10));*/
	$("#imglist").load(remote_url + "imglist&path=" + a + "&type=" + b + "&imgfile=" + d + "&refresh&order=" + $("select[name=order]").val() + "&random=" + randomNum(10));
	return false
});

$(".search a").click(function() {
	var a = $("span#foldervalue").attr("title") 
	var b = pathList("create_file", a), d, e;
	$("select[name=searchPath]").html("");
	for (e in b ) {
		d = a == b[e] ? ' selected="selected"' : "";
		$("select[name=searchPath]").append('<option value="' + b[e] + '"' + d + ">" + b[e] + "</option>")
	}
	$("div#filesearch").dialog({
		autoOpen : false,
		width : 300,
		modal : true,
		position : "center"
	}).dialog("open");
	$("input[name=q]").val("").focus();
	return false
});

$("input[name=upload]").change(function() {
	var a = $(this).val();
	f = a.replace(/.*\\(.*)/, "$1").replace(/.*\/(.*)/, "$1");
	$(this).parent().prev().html(f);
	a = a + " " + $("span#foldervalue").attr("title");
	$("input[name=currentFileUpload]").val() != a && $(this).parent().next().next().css("display", "none").next().css("display", "none")
});
$("input[name=imgurl]").change(function() {
	$(this).parent().next().next().css("display", "none").next().css("display", "none");
});

// Upload
$("#confirm").click(function() {
	var path = $("span#foldervalue").attr("title");
	var type = $("select[name=imgtype]").val() 
	
	var a = $("input[name=upload]").val() ;
	var d = $("input[name=currentFileUpload]").val();
	var e = path + "/" + a ;
	if (a != "" && d != e) 
	{
		$("input[name=upload]").parent().css("display", "none").next().css("display", "block").next().css("display", "none");
		$("input[name=upload]").upload(remote_url + "upload&random=" + randomNum(10), "path=" + path, function(res) 
		{
			$("input[name=currentFileUpload]").val(e);	
			var l = res.split("_");
			if (l[0] == "ERROR") {
				$("div#errorInfo").html(l[1]).dialog("open");
				$("input[name=upload]").parent().css("display", "block").next().css("display", "none").next().css("display", "none").next().css("display", "block")
			} else {
				$("input[name=upload]").parent().css("display", "block").next().css("display", "none").next().css("display", "block");
				$("input[name=selFile]").val(res);
				 
				$("#cfile").html(res);
				 
				$("#imglist").load(remote_url + "imglist&path=" + path + "&type=" + type+ "&order=0&imgfile=" + res, function() {
					filerename();
				});
			}
		}, "html");
	}else{ //upload url
	
		var b = $("input[name=imgurl]").val();
		var d = $("input[name=currentFileUrl]").val();
		var e =  path + " " + a ;
		if (/^(https?|ftp):\/\/(((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:)*@)?(((\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5]))|((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?)(:\d*)?)(\/((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)+(\/(([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)*)*)?)?(\?((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|[\uE000-\uF8FF]|\/|\?)*)?(\#((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|\/|\?)*)?$/i.test(b) && d != e) 
		{			
			$("input[name=imgurl]").parent().css("display", "none").next().css("display", "block").next().css("display", "none");
			$.ajax({
				type : "POST",
				url : remote_url + "upload&random=" + randomNum(10),
				data : "path=" + path + "&fileurl=" + b,
				success : function(k) {
					$("input[name=currentFileUrl]").val(e);
					var l = k.split("_");
					if (l[0] == "ERROR") {
						$("div#errorInfo").html(l[1]).dialog("open");
						$("input[name=imgurl]").parent().css("display", "block").next().css("display", "none").next().css("display", "none").next().css("display", "block")
					} else {
						$("input[name=imgurl]").parent().css("display", "block").next().css("display", "none").next().css("display", "block").next().css("display", "none");
						$("input[name=selFile]").val(k);
						$("#imglist").load(remote_url + "imglist&path=" + path + "&type=" + type + "&order=0&imgfile=" + k )
						filerename();
					}
				}
			})
		}
		
	}
	 
	 
});
$("div#errorInfo").dialog({
	autoOpen : false,
	width : 300,
	height : 180,
	modal : true,
	position : "center",
	show : "slide"
});
$("div#renamefolder").dialog({
	autoOpen : false,
	width : 250,
	height : 160,
	modal : true,
	position : "center",
	buttons : {
		Ok : function() {
			var a = $("span[name=current]").attr("title"), b = $("input[name=foldername]").val(), d = $("span#foldervalue").attr("title"), e = a.split("/");
			e = e[e.length - 1];
			if (b == "" || b == e || !nv_namecheck.test(b)) {
				alert(LANG.rename_nonamefolder);
				$("input[name=foldername]").focus();
				return false
			}
			e = $("span[name=current]").attr("class").split(" ");
			e = e[e.length - 1];
			var g = true;
			$("span." + e).each(function() {
				var h = $(this).attr("title").split("/");
				h = h[h.length - 1];
				if (b == h) {
					g = false
				}
			});
			if (!g) {
				alert(LANG.folder_exists);
				$("input[name=foldername]").focus();
				return false
			}
			$.ajax({
				type : "POST",
				url : remote_url + "renamefolder&random=" + randomNum(10),
				data : "path=" + a + "&newname=" + b,
				success : function(h) {
					var j = h.split("_");
					if (j[0] == "ERROR") {
						alert(j[1])
					} else {
						j = h.split("/");
						j = j[j.length - 1];
						var k;
						$("span[name=current]").parent().find("span").each(function() {
							k = $(this).attr("title");
							k = k.replace(a, h);
							$(this).attr("title") == d && $("span#foldervalue").attr("title", k);
							$(this).attr("title", k)
						});
						$("span[name=current]").html("&nbsp;" + j).attr("title", h)
					}
				}
			});
			$(this).dialog("close")
		}
	}
});
$("div#createfolder").dialog({
	autoOpen : false,
	width : 250,
	height : 160,
	modal : true,
	position : "center",
	buttons : {
		Ok : function() {
			var a = $("input[name=createfoldername]").val(), b = $("span[name=current]").attr("title");
			if (a == "" || !namecheck.test(a)) {
				alert(LANG.name_folder_error);
				$("input[name=createfoldername]").focus();
				return false
			}
			$.ajax({
				type : "POST",
				url : remote_url + "createfolder&random=" + randomNum(10),
				data : "path=" + b + "&newname=" + a,
				success : function(d) {
					var e = d.split("_");
					if (e[0] == "ERROR") {
						alert(e[1])
					} else {
						e = $("select[name=imgtype]").val();
						var h = $("span#path").attr("title");
						var path = h+"/"+d;
						
						$("#imgfolder").load(remote_url + "folderlist&path=" + h + "&folder=" + d + "&random=" + randomNum(10));
						$("div#imglist").load(remote_url + "imglist&path=" + path + "&type=" + e + "&order=" + $("select[name=order]").val() + "&random=" + randomNum(10))
					}
				}
			});
			$(this).dialog("close")
		}
	}
});
$("input[name=newWidth], input[name=newHeight]").keyup(function() {
	var a = $(this).attr("name"), b = $("input[name='" + a + "']").val(), d = $("input[name=origWidth]").val(), e = $("input[name=origHeight]").val(), g = calSizeMax(d, e, nv_max_width, nv_max_height);
	g = a == "newWidth" ? g[0] : g[1];
	if (!is_numeric(b) || b > g || b < 0) {
		$("input[name=newWidth]").val("");
		$("input[name=newHeight]").val("")
	} else {
		a == "newWidth" ? $("input[name=newHeight]").val(resize_byWidth(d, e, b)) : $("input[name=newWidth]").val(resize_byHeight(d, e, b))
	}
});
$("input[name=prView]").click(function() {
	checkNewSize()
});
$("input[name=newSizeOK]").click(function() {
	var a = $("input[name=newWidth]").val(), b = $("input[name=newHeight]").val(), d = $("input[name=origWidth]").val(), e = $("input[name=origHeight]").val();
	if (a == d && b == e) {
		$("div#imgcreate").dialog("close")
	} else {
		if (checkNewSize() !== false) {
			$(this).attr("disabled", "disabled");
			d = $("input[name=selFile]").val();
			var g = $("span#foldervalue").attr("title");
			$.ajax({
				type : "POST",
				url : remote_url + "createimg",
				data : "path=" + g + "&img=" + d + "&width=" + a + "&height=" + b + "&num=" + randomNum(10),
				success : function(h) {
					var j = h.split("_");
					if (j[0] == "ERROR") {
						alert(j[1]);
						$("input[name=newSizeOK]").removeAttr("disabled")
					} else {
						j = $("select[name=imgtype]").val();
						var k = $("select[name=author]").val() == 1 ? "&author" : "";
						$("input[name=selFile]").val(h);
						$("input[name=newSizeOK]").removeAttr("disabled");
						$("div#imgcreate").dialog("close");
						$("#imglist").load(remote_url + "imglist&path=" + g + "&type=" + j + "&imgfile=" + h + k + "&order=" + $("select[name=order]").val() + "&num=" + +randomNum(10))
					}
				}
			})
		}
	}
});
$("input[name=newPathOK]").click(function() {
	var a = $("span#foldervalue").attr("title"), b = $("select[name=newPath]").val(), d = $("input[name=selFile]").val(), e = $("input[name=mirrorFile]:checked").length;
	if (a == b) {
		$("div#filemove").dialog("close")
	} else {
		$(this).attr("disabled", "disabled");
		$.ajax({
			type : "POST",
			url : remote_url + "moveimg&num=" + randomNum(10),
			data : "path=" + a + "&newpath=" + b + "&file=" + d + "&mirror=" + e,
			success : function(g) {
				var h = g.split("_");
				if (h[0] == "ERROR") {
					alert(h[1]);
					$("input[name=newPathOK]").removeAttr("disabled");
				} else {
					h = $("select[name=imgtype]").val();
					var j = $("input[name=goNewPath]:checked").length, k = $("select[name=author]").val() == 1 ? "&author" : "";
					$("input[name=selFile]").val(g);
					$("input[name=newPathOK]").removeAttr("disabled");
					$("div#filemove").dialog("close");
					if (j == 1) {
						j = $("span#path").attr("title");
						$("#imgfolder").load(remote_url + "folderlist&path=" + j + "&currentpath=" + b + "&random=" + randomNum(10));
						$("#imglist").load(remote_url + "imglist&path=" + b + "&type=" + h + "&imgfile=" + g + k + "&order=" + $("select[name=order]").val() + "&num=" + +randomNum(10));
					} else {
						$("#imglist").load(remote_url + "imglist&path=" + a + "&type=" + h + "&imgfile=" + g + k + "&order=" + $("select[name=order]").val() + "&num=" + +randomNum(10));
					}
				}
			}
		});
	}
});
$("input[name=filerenameOK]").click(function() {
	var b = $("input[name=selFile]").val();
	var d = $("input[name=filerenameNewName]").val();
	var e = b.match(/^(.+)\.([a-zA-Z0-9]+)$/);
	d = $.trim(d);
	$("input[name=filerenameNewName]").val(d);
	if (d == "") {
		alert(LANG.rename_noname);
		$("input[name=filerenameNewName]").focus();
	} else {
		if (e[1] == d ) {
			$("div#filerename").dialog("close");
		} else { 
			p = $("span#foldervalue").attr("title") ;
			$(this).attr("disabled", "disabled");
			
			$.ajax({
				type : "POST",
				url : remote_url + "renameimg&num=" + randomNum(10),
				data : "path=" + p + "&file=" + b + "&newname=" + d  ,
				success : function(g) {
					var h = g.split("_");
					if (h[0] == "ERROR") {
						alert(h[1]);
						$("input[name=filerenameOK]").removeAttr("disabled");
					} else {
						h = $("select[name=imgtype]").val(); 						
						$("input[name=filerenameOK]").removeAttr("disabled");
						$("div#filerename").dialog("close");
						$("#imglist").load(remote_url + "imglist&path=" + p + "&type=" + h + "&imgfile=" + g  + "&order=" + $("select[name=order]").val() + "&num=" + randomNum(10));
					}
				}
			});
		}
	}
});
$("img[name=myFile2]").dblclick(function() {
	$("div[title=createInfo]").find("div").remove();
	var a = $("input[name=origWidth]").val(), b = $("input[name=origHeight]").val();
	c = calSize(a, b, 360, 230);
	$(this).width(c[0]).height(c[1]);
	$("input[name=newHeight]").val(b);
	$("input[name=newWidth]").val(a).select();
});



function file_preview(type,src,info) {
	$("div.dynamic").text("");
	$("input.dynamic").val(""); 
	var root = location.protocol + '//' + location.host
	var arr_info = info.split("|");
	var e = LANG.upload_size + ": ";
	
	info_wh = calSize(arr_info[1], arr_info[2], 400, 400);
	
	if(type == "image")
	{
		 $("div#fileView").html('<img style="border:2px solid #F0F0F0;" width="' + info_wh[1] + '" height="' + info_wh[2] + '" src="' + src + '" /><div style="font-size:11px">'+root+src+'</div>') 
		 e +=  arr_info[1]+" x "+ arr_info[2] + " ("+ arr_info[3]+") <br />";
	}else if (type=="flash"){
		 $("div#fileView").append("<div class='flash'></div>"+'<div style="font-size:11px">'+root+src+'</div>');
		 $("div#fileView .flash").flash({	src : src,		width : arr_info[1],		height : arr_info[2]	}, {	version : 8	});
		  e +=  arr_info[1]+" x "+ arr_info[2] + " ("+ arr_info[3]+") <br />";
	} else {
		e += arr_info[3] + "<br />"; 
		$("div#fileView").html(src);
	}
	e += LANG.pubdate + ": " + arr_info[4]; 
	$("#fileInfoDetail").html(e);
	$("#fileInfoName").html(arr_info[0]);
	$("div#imgpreview").dialog({
		autoOpen : false,
		width : 388,
		modal : true,
		position : "center"
	}).dialog("open").dblclick(function() {
		$("div#imgpreview").dialog("close");
	});
}

//-------------------------- EDITOR ---------------------------

$(".editor_refresh a").click(function() {
	var a = $("span#foldervalue").attr("title"), b = "image", d = $("input[name=selFile]").val() , g = $("span#path").attr("title");
	/*$("#imgfolder").load(remote_url + "folderlist&path=" + g + "&currentpath=" + a + "&dirListRefresh&random=" + randomNum(10));*/
	$("#imglist").load( "index.php?mod=media&act=editor&do=imglist&path=" + a + "&type=" + b + "&imgfile=" + d + "&refresh&order=" + $("select[name=order]").val() + "&random=" + randomNum(10));
	return false
});

$(".editor_search a").click(function() {
	var a = $("span#foldervalue").attr("title") 
	var b = pathList("create_file", a), d, e;
	$("select[name=searchPath]").html("");
	for (e in b ) {
		d = a == b[e] ? ' selected="selected"' : "";
		$("select[name=searchPath]").append('<option value="' + b[e] + '"' + d + ">" + b[e] + "</option>")
	}
	$("div#filesearch").dialog({
		autoOpen : false,
		width : 300,
		modal : true,
		position : "center"
	}).dialog("open");
	$("input[name=q]").val("").focus();
	return false
});

// Upload
$("#editor_confirm").click(function() {
	var path = $("span#foldervalue").attr("title");
	var type = "image";
	
	var a = $("input[name=upload]").val() ;
	var d = $("input[name=currentFileUpload]").val();
	var e = path + "/" + a ;
	if (a != "" && d != e) 
	{
		$("input[name=upload]").parent().css("display", "none").next().css("display", "block").next().css("display", "none");
		$("input[name=upload]").upload( "index.php?mod=media&act=editor&do=upload&random=" + randomNum(10), "path=" + path, function(res) 
		{
			$("input[name=currentFileUpload]").val(e);	
			var l = res.split("_");
			if (l[0] == "ERROR") {
				$("div#errorInfo").html(l[1]).dialog("open");
				$("input[name=upload]").parent().css("display", "block").next().css("display", "none").next().css("display", "none").next().css("display", "block")
			} else {
				$("input[name=upload]").parent().css("display", "block").next().css("display", "none").next().css("display", "block");
				$("input[name=selFile]").val(res);
				 
				$("#cfile").html(res);
				 
				$("#imglist").load( "index.php?mod=media&act=editor&do=imglist&path=" + path + "&type=" + type+ "&order=0&imgfile=" + res, function() {
					//filerename();
					vnTMedia.do_SelectPic( $("img[title='" + res + "']"));
				});
			}
		}, "html");
	}else{ //upload url
	
		var b = $("input[name=imgurl]").val();
		var d = $("input[name=currentFileUrl]").val();
		var e =  path + " " + a ;
		if (/^(https?|ftp):\/\/(((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:)*@)?(((\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5]))|((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?)(:\d*)?)(\/((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)+(\/(([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)*)*)?)?(\?((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|[\uE000-\uF8FF]|\/|\?)*)?(\#((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|\/|\?)*)?$/i.test(b) && d != e) 
		{			
			$("input[name=imgurl]").parent().css("display", "none").next().css("display", "block").next().css("display", "none");
			$.ajax({
				type : "POST",
				url : "index.php?mod=media&act=editor&do=upload&random=" + randomNum(10),
				data : "path=" + path + "&fileurl=" + b,
				success : function(k) {
					$("input[name=currentFileUrl]").val(e);
					var l = k.split("_");
					if (l[0] == "ERROR") {
						$("div#errorInfo").html(l[1]).dialog("open");
						$("input[name=imgurl]").parent().css("display", "block").next().css("display", "none").next().css("display", "none").next().css("display", "block")
					} else {
						$("input[name=imgurl]").parent().css("display", "block").next().css("display", "none").next().css("display", "block").next().css("display", "none");
						$("input[name=selFile]").val(k);
						$("#imglist").load( "index.php?mod=media&act=editor&do=imglist&path=" + path + "&type=" + type + "&order=0&imgfile=" + k , function() {
							//filerename();
							vnTMedia.do_SelectPic( $("img[title='" + k + "']"));
						});						 
					 
					}
				}
			})
		}
		
	}
	 
	 
});


// Xu ly keo tha chuot chon file
function fileSelecting(e, ui){
	 
	if( e.ctrlKey ){
	 
		if( $(ui.selecting).is('.imgsel') ){
			$(ui.selecting).addClass('imgtempunsel');
		}else{
			$(ui.selecting).addClass('imgtempsel');
		}
	}else if( e.shiftKey ){
	 
		$(ui.selecting).addClass('imgtempsel');
	}else{
		$(ui.selecting).removeClass('imgtempunsel').addClass('imgtempsel');
		$('#imglist .imgcontent:not(.imgtempsel)').addClass('imgtempunsel');
	}
}

// Xu ly khi thoi chon file
function fileUnselect(e, ui){
	$(ui.unselecting).removeClass('imgtempunsel imgtempsel');
}

// Xu ly khi ket thuc chon file
function fileSelectStop(e, ui){
	$('#imglist .ui-selected').removeClass('ui-selected');
	$('.imgtempsel').addClass('imgsel').removeClass('imgtempsel');
	$('.imgtempunsel').removeClass('imgsel imgtempunsel');
	 
	LFILE.setSelFile();
}


vnTMedia = {  
//	--------------------------------- main action -------------------------------- // 
	do_SelectPic:function(a){ 
		$("#imgdetail").show();
		
		$(".imgsel").removeClass("imgsel");
		$(a).addClass("imgsel");
		a = $(a).attr("title");
		$("input[name=selFile]").val(a);
 		var pic_name = a.replace(/^(.+)\.([a-zA-Z0-9]+)$/, "$1");
		var pic_ext = a.replace(/^(.+)\.([a-zA-Z0-9]+)$/, "$2")
		var src_thumb = $("img[title='" + a + "']").attr("src");
		var d = $("img[title='" + a + "']").attr("name").split("|");
		b = $("span#foldervalue").attr("title") ;	
		
		
		$("#detail-img").attr("src",  src_thumb );
		$("#imgdetail .filename").html(a);
		$("#imgdetail .uploaded").html(d[7]);
		$("#imgdetail .dimensions").html(d[1]+"x"+d[2]);
		
		$("input[name=pic_name_old]").text(a);
		$("input[name=pic_name]").val(pic_name);
		$("input[name=pic_alt]").val(pic_name);		
		$("span[title=Ext]").text("." + pic_ext);
		$("input[name=pic_width]").val(d[1]);
		$("input[name=h_fileId]").val(d[0]);	
		
	},	
	
	folderClick:function (a) {
		var b = $(a).attr("title");
		
		if (b != $("span#foldervalue").attr("title")) {
				
			$("span#foldervalue").attr("title", b);
			$("span#view_dir").attr("title", $(a).is(".view_dir") ? "1" : "0");
			$("span#create_dir").attr("title", $(a).is(".create_dir") ? "1" : "0");
			$("span#rename_dir").attr("title", $(a).is(".rename_dir") ? "1" : "0");
			$("span#delete_dir").attr("title", $(a).is(".delete_dir") ? "1" : "0");
			$("span#upload_file").attr("title", $(a).is(".upload_file") ? "1" : "0");
			$("span#create_file").attr("title", $(a).is(".create_file") ? "1" : "0");
			$("span#rename_file").attr("title", $(a).is(".rename_file") ? "1" : "0");
			$("span#delete_file").attr("title", $(a).is(".delete_file") ? "1" : "0");
			$("span#move_file").attr("title", $(a).is(".move_file") ? "1" : "0");
			$("span.folder").css("color", "");
			$(a).css("color", "red");
			 
			if ($(a).is(".view_dir")) {
	
				var d = $("input[name=selFile]").val(), e = $("select[name=author]").val() == 1 ? "&author" : "";
				$("div#imglist").html('<p style="padding:20px; text-align:center"><img src="' + DIR_IMAGE + '/load_bar.gif"/> please wait...</p>').load("index.php?mod=media&act=editor&do=imglist&path=" + b + "&imgfile=" + d + "&type=image" + e + "&order=" + $("select[name=order]").val() + "&random=" + randomNum(10))
			} else {
				$("div#imglist").text("")
			}
			is_allowed_upload()
		}
	}, 
	
	searchfile : function () {
		a = $("select[name=searchPath]").val(), q = $("input[name=q]").val();
		b = "image";
		$("div#filesearch").dialog("close");
		$("#imglist").html('<p style="padding:20px; text-align:center"><img src="' + DIR_IMAGE + '/load_bar.gif"/> please wait...</p>').load(  'index.php?mod=media&act=editor&do=imglist&path=' + a + '&type=' + b  + '&q=' + rawurlencode(q) + '&order=' + $('select[name=order]').val() + '&random=' + randomNum(10))
		return false;
	},

	insertToEditor:function () 
	{
		
		var a = $("input[name=CKEditorFuncNum]").val() ;
		var file_id = $("#h_fileId").val();
		
		if(file_id)
		{
			var pic_name = $("#pic_name").val();			
			var pic_alt = $("#pic_alt").val();			
			var pic_width = $("#pic_width").val();			
			
			var mydata =  "file_id="+file_id+"&pic_name="+encodeURIComponent(pic_name)+'&pic_alt='+ encodeURIComponent(pic_alt)+'&pic_width='+pic_width ; 
			$.ajax({
				async: true,
				type : "POST",
				dataType: 'json',
				url : "index.php?mod=media&act=editor&do=insert_img&random=" + randomNum(10),
				data : mydata,
				success : function(data) 
				{	 
					if (data.ok == 1) 
					{						 
						window.opener.CKEDITOR.tools.callFunction(a, data.link_file , function() {
							// Get the reference to a dialog window.
							var dialog = this.getDialog();
							// Check if this is the Image dialog window.
							if (dialog.getName() == 'image') {
								// Get the reference to a text field that holds the "alt" attribute.
								var element = dialog.getContentElement('info', 'txtAlt');
								// Assign the new value.
								if (element)
									element.setValue(pic_alt);
							}
						});
						
						
						//window.opener.CKEDITOR.tools.callFunction(a, data.link_file);		 
						window.close();							
					} else {
						alert(data.mess)							
					}
				}
			}) ; 
		}else{
			alert('Vui lòng chọn 1 hình')	;
		}	
		 		
		
	}, 
	
	
}; 

$("#btnInsertPost").click(function() {	 	 
	vnTMedia.insertToEditor();
});
