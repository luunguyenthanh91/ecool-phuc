<?php
/*================================================================================*\
|| 							Name code : media.php 		 		            	  ||
||  				Copyright @2008 by Thai Son - CMS vnTRUST                     ||
\*================================================================================*/
/**
 * @version : 2.0
 * @date upgrade : 09/01/2009 by Thai Son
 **/
if (! defined('IN_vnT')) {
  die('Hacking attempt!');
}
$vntModule = new vntModule();

class vntModule
{
  var $output = "";
  var $skin = "";
  var $linkUrl = "";
  var $module = "media";
	var $action = "popup_media";

  /**
   * function vntModule ()
   * Khoi tao 
   **/
  function vntModule ()
  {
    global $Template, $vnT, $func, $DB, $conf;
    require_once ("function_media.php");
    $this->skin = new XiTemplate(DIR_MODULE . DS . "media_ad" . DS . "html" . DS . "popup_media.tpl");
    $this->skin->assign('PATH_ROOT', $conf['rootpath']);
    $this->skin->assign('LANG', $vnT->lang);		
	  $this->skin->assign('CONF', $vnT->conf);
		$this->skin->assign('ROOT_URI', ROOT_URI);
    $this->skin->assign('DIR_IMAGE', $vnT->dir_images);
	  $this->skin->assign('DIR_STYLE', $vnT->dir_style );		 
    $this->skin->assign('DIR_JS', $conf['rooturl'] . "js");
    $this->skin->assign("admininfo", $vnT->admininfo);
    $this->skin->assign('DIR_IMAGE_MEDIA', DIR_IMAGE_MEDIA);

    $this->linkUrl = "?mod=" . $this->module . "&act=" . $this->action  ;
		switch ($_GET['stype'])
		{

      case "editor_gallery"	 :
        flush() ;
          echo  $this->do_EditorGallery();
        exit();
        break ;

      case "edit_gallery"	 :
        flush() ;
        echo  $this->do_EditGallery();
        exit();
        break ;

      case "browse"	 : $data['main'] = $this->do_BrowseMedia(); break ;
      case "editor"	 : $data['main'] = $this->do_EditorMedia(); break ;
			default : $data['main'] =   $this->do_PopupMedia(); break ;
		}    
		$data['obj_return'] = $_GET['obj'];
    $this->skin->assign('data', $data);		
    $this->skin->parse("html_popup");
    $output =  $this->skin->text("html_popup");
		
    flush();
    echo $output;
    exit();
  }
 
	//============ do_PopupMedia
  function do_PopupMedia ()
  {
    global $vnT, $func, $DB, $conf; 
		
		$obj_return = $_GET['obj'];
		$module = $_GET['module'] ;		
		$folder = ($_GET['folder']== $_GET['module']."/") ? $_GET['module'] : $_GET['folder'] ; 				
		$type = ($_GET['type']) ? $_GET['type'] : "file";
		
		$data['path']	 = ($module) ? $module : "File";
		$data['folder']	 = $folder;
		$data['path_img'] = $folder ;
		
		if($_GET['CKEditorFuncNum']){
			$data['CKEditorFuncNum'] = $_GET['CKEditorFuncNum'];
			$obj_return = $_GET['CKEditor'];
		}else{
			$data['CKEditorFuncNum'] = "";				
		}		
		
		$data['module'] = $module;
		$data['type'] = $type ;
    $data['max_upload'] = ini_get('upload_max_filesize');
    $data['err'] = $err; 
    $data['obj_return'] = $obj_return;
		$data['show_pic'] = (int)$_GET['show_pic'];
		$data['list_filetype'] =  vnT_HTML::selectbox("imgtype", array(
      'file' => $vnT->lang['type_file'] , 'image' => $vnT->lang['type_image'] , 'flash' => $vnT->lang['type_flash']
    ), $type); 
		
		
    $data['link_action'] = $this->linkUrl ;
    /*assign the array to a template variable*/
    $this->skin->assign('data', $data);
    $this->skin->parse("html_popup_media");
    return $this->skin->text("html_popup_media");
  }
	
	//============ do_EditorMedia
  function do_EditorMedia ()
  {
    global $vnT, $func, $DB, $conf; 
		
		$obj_return = $_GET['obj'];
		$module = $_GET['module'] ;		
		$folder = $_GET['folder']; 				
		$type = ($_GET['type']) ? $_GET['type'] : "file";
		
		$data['path']	 = ($module) ? $module : "File";
		$data['folder']	 = $folder;
		$data['path_img'] = $folder ;
		
		if($_GET['CKEditorFuncNum']){
			$data['CKEditorFuncNum'] = $_GET['CKEditorFuncNum'];
			$obj_return = $_GET['CKEditor'];
		}else{
			$data['CKEditorFuncNum'] = "";				
		}		
		
		$data['module'] = $module;
		$data['type'] = $type ;
    $data['max_upload'] = ini_get('upload_max_filesize');
    $data['err'] = $err; 
    $data['obj_return'] = $obj_return;
		$data['show_pic'] = (int)$_GET['show_pic'];

		$data['list_filetype'] =  vnT_HTML::selectbox("imgtype", array(
      'file' => $vnT->lang['type_file'] , 'image' => $vnT->lang['type_image'] , 'flash' => $vnT->lang['type_flash']
    ), $type); 
		
		
    /*assign the array to a template variable*/
    $this->skin->assign('data', $data);
    $this->skin->parse("html_editor_media");
    return $this->skin->text("html_editor_media");
  }

  //============ do_BrowseMedia
  function do_BrowseMedia ()
  {
    global $vnT, $func, $DB, $conf;
    $err ='';
    $obj_return = $_GET['obj'];
    $module = $_GET['module'] ;
    $folder = ($_GET['folder']== $_GET['module']."/") ? $_GET['module'] : $_GET['folder'] ;
    $type = ($_GET['type']) ? $_GET['type'] : "file";

    $data['path']	 = ($module) ? $module : "File";
    $data['folder']	 = $folder;
    $data['path_img'] = $folder ;


    $data['module'] = $module;
    $data['type'] = $type ;
    $data['max_upload'] = ini_get('upload_max_filesize');
    $data['err'] = $err;
    $data['obj_return'] = $obj_return;
    $data['show_pic'] = (int)$_GET['show_pic'];
    $data['list_filetype'] =  vnT_HTML::selectbox("imgtype", array(
      'file' => $vnT->lang['type_file'] , 'image' => $vnT->lang['type_image'] , 'flash' => $vnT->lang['type_flash']
    ), $type);


    $data['link_action'] = $this->linkUrl ;
    /*assign the array to a template variable*/
    $this->skin->assign('data', $data);
    $this->skin->parse("html_browse_media");
    return $this->skin->text("html_browse_media");
  }


  //============ do_EditorGallery
  function do_EditorGallery ()
  {
    global $vnT, $func, $DB, $conf;

    $err = '';

    if($_POST['do_submit']){

      /*echo "<pre>";
      print_r($_POST);
      echo "</pre>";
      die();
    */

      $arr_photo = $_POST['photo'];
      $num_photo = count($arr_photo);
      if($num_photo){
        $i = 0; $list_id = 0;
        foreach ( $arr_photo as $photo) {
          $i++;
          $tmp = @explode("|",$photo);
          $list_id .= trim($tmp[0]).",";
          $picture = trim($tmp[1]);

        }
        $list_id = substr($list_id,0,-1);

        $cot['picture'] = $picture ;
        $cot['num_photo'] = $num_photo;
        $cot['list_photo'] = $list_id ;
        $cot['date_post'] = time();
        $ok = $vnT->DB->do_insert("media_gallery",$cot);
        if($ok){
          $gid = $vnT->DB->insertid();
          $title = "Gallery photo ".$gid;
          $vnT->DB->query("UPDATE media_gallery SET title='".$title."' WHERE gid=".$gid);
          $vnT->DB->query("UPDATE media_files SET gid=".$gid." WHERE file_id in(".$list_id.") ");

          $data['js_extra'] = ' self.parent.inset_gallery('.$gid.'); ';

        }
      }
    }

    $folder_id = 3 ;
    $folder_upload = $conf['rootpath']."vnt_upload" ;
    $cur_folder = '';
    if($folder_id){
      $result = $vnT->DB->query("SELECT * FROM media_folders WHERE folder_id=".$folder_id)	;
      if($row = $vnT->DB->fetch_row($result))
      {
        $cur_folder = $row['folder_path']	;
        $folder_upload .= "/". $cur_folder ;

        if (! file_exists($folder_upload)) {
          $err = $func->html_err("Thư mục <b>{$folder_upload}</b> không tồn tại");
        }

      }else{
        $err = $func->html_err("Thư mục <b>{$folder_upload}</b> không tồn tại");
      }
    }

    $data['w_pic'] = $vnT->setting['upload_max_width'];
    $data['w_thumb'] = $vnT->setting['upload_thum_width'] ;
    $data['folder_id'] = $folder_id ;
    $data['folder_upload'] = $folder_upload ;
    $data['folderpath'] =   DIR_MEDIA ;
    $data['folder'] = $cur_folder ;
    $data['max_upload'] = ini_get('upload_max_filesize');



    $result = $vnT->DB->query("SELECT * FROM media_gallery ORDER BY date_post DESC");
    if($num = $vnT->DB->num_rows($result))
    {
      while ($row = $vnT->DB->fetch_row($result))
      {
        $link =  $this->linkUrl .'&stype=edit_gallery&gid='.$row['gid'];
        $title = $vnT->func->HTML($row['title']);
        $src = get_src_thumb($row['picture']);
        $src_big = URL_MEDIA ."/".$row['picture'];

        $text_date_post = @date("d/m/Y",$row['date_post']) ;
        $row['link'] = $link ;
        $row['title'] = $title ;
        $row['src'] = $src ;
        $row['src_big'] = $src_big ;
        $row['text_date_post'] = $text_date_post;
        $this->skin->assign('row', $row);
        $this->skin->parse("html_editor_gallery.html_item");
      }
    }



    $data['err'] = $err;
    /*assign the array to a template variable*/
    $this->skin->assign('data', $data);
    $this->skin->parse("html_editor_gallery");
    return $this->skin->text("html_editor_gallery");
  }

  //============ do_EditGallery
  function do_EditGallery ()
  {
    global $vnT, $func, $DB, $conf;

    $err = '';
    $gid = (int) $vnT->input['gid'];

    if($_POST['do_submit']){

      /*echo "<pre>";
      print_r($_POST);
      echo "</pre>";
      die();
    */
      $list_id = ''; $picture ='';
      $arr_photo_old = $_POST['photo_old'];
      $num_photo_old =  count($arr_photo_old);
      foreach ($arr_photo_old as $photo_old){
        $list_id .= $photo_old.",";
      }

      $arr_photo = $_POST['photo'];
      $num_photo_new = 0;
      if(count($arr_photo)){
        $i = 0;
        foreach ( $arr_photo as $photo) {
          $i++;
          if($photo){
            $tmp = @explode("|",$photo);
            $list_id .= trim($tmp[0]).",";
            $picture = trim($tmp[1]);

            $num_photo_new++;
          }

        }
      }
      $list_id = substr($list_id,0,-1);

      $num_photo = $num_photo_old+$num_photo_new ;
      if($num_photo){

        $title = $vnT->func->txt_HTML($_POST['title']);
        if($picture){
          $cot['picture'] = $picture ;
        }

        //$cot['title'] = $title;
        $cot['num_photo'] = $num_photo ;
        $cot['list_photo'] = $list_id ;
        $cot['date_post'] = time();
        $ok = $vnT->DB->do_update("media_gallery",$cot,"gid=".$gid);
        if($ok){
          //$err = "Cập nhật thành công";

          $vnT->DB->query("UPDATE media_files SET gid=0 WHERE gid=".$gid);
          $vnT->DB->query("UPDATE media_files SET gid=".$gid." WHERE file_id in(".$list_id.") ");

        }
      }
    }

    $folder_id = 3 ;
    $folder_upload = $conf['rootpath']."vnt_upload" ;
    $cur_folder = '';
    if($folder_id){
      $result = $vnT->DB->query("SELECT * FROM media_folders WHERE folder_id=".$folder_id)	;
      if($row = $vnT->DB->fetch_row($result))
      {
        $cur_folder = $row['folder_path']	;
        $folder_upload .= "/". $cur_folder ;

        if (! file_exists($folder_upload)) {
          $err = $func->html_err("Thư mục <b>{$folder_upload}</b> không tồn tại");
        }

      }else{
        $err = $func->html_err("Thư mục <b>{$folder_upload}</b> không tồn tại");
      }
    }




    $result = $vnT->DB->query("SELECT * FROM media_gallery WHERE gid=".$gid);
    if ($data = $vnT->DB->fetch_row($result))
    {



      $res_pic = $vnT->DB->query("SELECT * FROM media_files WHERE gid=".$gid." ORDER BY display_order ASC , file_id ASC" );
      $stt  = 0;
      while ($row_pic = $vnT->DB->fetch_row($res_pic)) {
        $stt++;
        $row_pic['stt'] = $stt ;
        $row_pic['src'] = get_src_thumb($row_pic['file_src'])  ;
        $this->skin->assign('row', $row_pic);
        $this->skin->parse("html_edit_gallery.item_pic");

      }

    }

    $data['w_pic'] = $vnT->setting['upload_max_width'];
    $data['w_thumb'] = $vnT->setting['upload_thum_width'] ;
    $data['folder_id'] = $folder_id ;
    $data['folder_upload'] = $folder_upload ;
    $data['folderpath'] =   DIR_MEDIA ;
    $data['folder'] = $cur_folder ;
    $data['max_upload'] = ini_get('upload_max_filesize');

    $data['link_manage'] = $this->linkUrl .'&stype=editor_gallery';
    $data['err'] = $err;
    $data['link_action'] = $this->linkUrl .'&stype=edit_gallery&gid='.$gid;
    /*assign the array to a template variable*/
    $this->skin->assign('data', $data);
    $this->skin->parse("html_edit_gallery");
    return $this->skin->text("html_edit_gallery");
  }




   // end class
}
?>