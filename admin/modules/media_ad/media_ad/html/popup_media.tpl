<!-- BEGIN: html_popup -->
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>[:: Admin ::]</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link rel="SHORTCUT ICON" href="vntrust.ico" type="image/x-icon" />
<link rel="icon" href="vntrust.ico" type="image/gif" >
<LINK href="{DIR_STYLE}/global.css" rel="stylesheet" type="text/css">
<LINK href="modules/media_ad/css/media.css" rel="stylesheet" type="text/css">
<LINK href="{DIR_JS}/jquery_plugins/treeview/jquery.treeview.css" rel="stylesheet" type="text/css">
<LINK href="{DIR_JS}/jquery_ui/themes/base/ui.all.css" rel="stylesheet" type="text/css">

<script type='text/javascript' src="{DIR_JS}/jquery.min.js"></script>
<script type='text/javascript' src="{DIR_JS}/jquery-migrate.min.js"></script>

<script type="text/javascript" src="{DIR_JS}/jquery_ui/ui.core.js?t=1"></script>
<script type="text/javascript" src="{DIR_JS}/jquery_ui/ui.draggable.js?t=1"></script>
<script type="text/javascript" src="{DIR_JS}/jquery_ui/ui.resizable.js?t=1"></script>
<script type="text/javascript" src="{DIR_JS}/jquery_ui/ui.selectable.js?t=1"></script>
<script type="text/javascript" src="{DIR_JS}/jquery_ui/ui.dialog.js?t=1"></script>

<script language="javascript1.2" src="{DIR_JS}/contextmenu/jquery.contextmenu.js"></script>
<script language="javascript1.2" src="{DIR_JS}/jquery_plugins/jquery.lazyload.js"></script>
<script type="text/javascript" src="{DIR_JS}/jquery_plugins/jquery.flash.js?t=1"></script>
<script language="javascript1.2" src="{DIR_JS}/jquery_plugins/treeview/jquery.treeview.js"></script>

<script language="javascript1.2" src="{DIR_JS}/jquery_plugins/jquery.upload.js?v=1.0"></script>
<script language="javascript1.2" src="{DIR_JS}/admin/js_admin.js?v=1.0"></script>

<script type="text/javascript">

	var ROOT = "{CONF.rooturl}";
	var ROOT_URI = "{ROOT_URI}";
	var DIR_IMAGE = "{DIR_IMAGE}";
	var win = window.dialogArguments || opener || parent || top;
	var obj_return = "{data.obj_return}" ;
//<![CDATA[
var LANG = [];
LANG.upload_size = "Kích thước";
LANG.pubdate = "Cập nhật";
LANG.download = "Tải về";
LANG.preview = "Xem chi tiết";
LANG.addlogo = "Thêm Logo";
LANG.select = "Chọn";
LANG.upload_createimage = "Công cụ ảnh";
LANG.move = "Di chuyển";
LANG.rename = "Đổi tên file";
LANG.upload_delfile = "Xóa file";
LANG.createfolder = "Tạo folder";
LANG.renamefolder = "Đổi tên folder";
LANG.deletefolder = "Xóa folder";
LANG.delete_folder = "Bạn có chắc muốn xóa thư mục này không. Nếu xóa thư mục này đồng nghĩa với việc toàn bộ các file trong thư mục này cũng bị xóa ?";
LANG.rename_nonamefolder = "Bạn chưa đặt tên mới cho thư mục hoặc tên thư mục không đúng quy chuẩn";
LANG.folder_exists = "Lỗi! Đã có thư mục cùng tên tồn tại";
LANG.name_folder_error = "Bạn chưa đặt tên cho thư mục hoặc tên không đúng quy chuẩn";
LANG.rename_noname = "Bạn chưa đặt tên mới cho file";
LANG.upload_delimg_confirm = "Bạn có chắc muốn xóa file";
LANG.origSize = "Kích thước gốc";
LANG.errorMinX = "Lỗi: Chiều rộng nhỏ hơn mức cho phép";
LANG.errorMaxX = "Lỗi: Chiều rộng lớn hơn mức cho phép";
LANG.errorMinY = "Lỗi: Chiều cao nhỏ hơn mức cho phép";
LANG.errorMaxY = "Lỗi: Chiều cao lớn hơn mức cho phép";
LANG.errorEmptyX = "Lỗi: Chiều rộng chưa xác định";
LANG.errorEmptyY = "Lỗi: Chiều cao chưa xác định";
var nv_max_width = '1500', nv_max_height = '1500', nv_min_width = '10', nv_min_height = '10';
var remote_url = "index.php?mod=media&act=remote&do=", nv_namecheck = /^([a-zA-Z0-9_-])+$/, array_images = ["gif", "jpg", "jpeg", "pjpeg", "png"], array_flash = ["swf", "swc", "flv"], array_archives = ["rar", "zip", "tar"], array_documents = ["doc", "xls", "chm", "pdf", "docx", "xlsx"];
//]]>
 
</script>

 
</head>
<body style=" background:url({DIR_IMAGE_MEDIA}/images/media_footer.png) #F0F0F0; margin:0px;" >
	<div class="popupContent">
  	{data.main}
  </div>

<script language="javascript1.2" src="modules/media_ad/js/media.js"></script>
</body>
</html>
<!-- END: html_popup -->

 
 
<!-- BEGIN: html_popup_media --> 
<div class="media-content">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="200" valign="top">
    <div id="imgfolder" class="imgfolder"><p style="padding:20px; text-align:center"><img alt="" src="{DIR_IMAGE}/load_bar.gif" /> please wait...</p></div>
    </td>
    <td  valign="top">
    	<div class="filebrowse">
      	<div id="imglist"><p style="padding:20px; text-align:center"><img alt="" src="{DIR_IMAGE}/load_bar.gif" /> please wait...</p></div>
      </div>
    </td>
  </tr>
</table>
<div class="clear"></div>
</div>
<div class="media-footer"> 
<div class="refresh">
		<a href="#" title="{LANG.refresh}"><img alt="{LANG.refresh}" src="{DIR_IMAGE}/refresh.png" width="16" height="16"/></a>
	</div>
	<div class="filetype">{data.list_filetype}</div>
	<div class="sortFile">
 
		<select name="order" class="select">
			<option value="0">{LANG.order0}</option>
			<option value="1">{LANG.order1}</option>
			<option value="2">{LANG.order2}</option>
		</select>
	</div>
	<div class="search">
		<a href="#" title="{LANG.search}"><img alt="{LANG.search}" src="{DIR_IMAGE}/search.png" width="16" height="16"/></a>
	</div>
	<div class="uploadForm" >
		<div style="margin-top:5px;margin-right:5px;float:left;" id="cfile">
			{LANG.upload_file}
		</div>
		<div class="upload"><input type="file" name="upload" id="myfile"/>
		</div>
		<div style="margin-top:10px;float:left;display:none"><img src="{DIR_IMAGE}/load_bar.gif"/>
		</div>
		<div style="margin-top:5px;margin-left:5px;float:left;display:none"><img src="{DIR_IMAGE}/ok.png"/>
		</div>
		<div style="margin-top:7px;margin-left:5px;margin-right:5px;float:left;display:none"><img src="{DIR_IMAGE}/error.png"/>
		</div>
		<div style="float:left;margin-left:5px;">
			{LANG.upload_otherurl}: <input type="text" name="imgurl"/>
		</div>
		<div style="margin-top:10px;margin-left:5px;margin-right:5px;float:left;display:none"><img src="{DIR_IMAGE}/load_bar.gif"/>
		</div>
		<div style="margin-top:5px;margin-left:5px;margin-right:5px;float:left;display:none"><img src="{DIR_IMAGE}/ok.png"/>
		</div>
		<div style="margin-top:7px;margin-left:5px;margin-right:5px;float:left;display:none"><img src="{DIR_IMAGE}/error.png"/>
		</div>
		<div style="float:left;"><input type="button" value="Upload" id="confirm" class="button" />
		</div>
	</div>
	<div class="notupload" style="display:none">
		{LANG.notupload}
	</div>
	<div class="clear"></div>
</div> 

<input type="hidden" name="module" value="{data.module}"/>
<input type="hidden" name="currentFileUpload" value=""/>
<input type="hidden" name="currentFileUrl" value=""/>
<input type="hidden" name="selFile" value=""/>
<input type="hidden" name="CKEditorFuncNum" value="{data.CKEditorFuncNum}"/>
<input type="hidden" name="area" value="{AREA}"/>
<input type="hidden" name="alt" value="{ALT}"/>
<input type="hidden" name="show_pic" value="{data.show_pic}"/>
<div style="display:none" id="contextMenu"></div>
<div style="display:none">
	<iframe id="Fdownload" src="" width="0" height="0" frameborder="0"></iframe>
</div>

<div id="filesearch" style="display:none;padding:10px;font-size:11px;" title="{LANG.search}">
	<form method="get" onsubmit="return searchfile();">
		{LANG.searchdir}:
		<div style="margin:10px 0 20px">
			<select name="searchPath"></select>
		</div>
		{LANG.searchkey}:
		<div style="margin:10px 0"><input name="q" type="text" class="w200 dynamic" />
		</div>
		<input style="margin-left:50px;width:100px;" type="submit" value="{LANG.search}" name="search" />
	</form>
</div>

<div id="errorInfo" style="display:none" title="{LANG.errorInfo}"></div>
<div id="imgpreview" style="overflow:auto;display:none" title="{LANG.preview}">
	<div style="text-align:center;font-size:12px;font-weight:800;margin-top:10px" id="fileInfoAlt" class="dynamic"></div>
	<div style="text-align:center;margin-top:10px" id="fileView" class="dynamic"><span class="fileView"></span></div>
	<div style="text-align:center;font-size:12px;font-weight:800;margin-top:10px" id="fileInfoName" class="dynamic"></div>
	<div style="text-align:center;font-size:11px;margin-top:10px;margin-bottom:10px" id="fileInfoDetail" class="dynamic"></div>
</div>
<div id="createfolder" style="display:none" title="{LANG.createfolder}">
	{LANG.foldername}<input type="text" name="createfoldername"/>
</div>
<div id="filerename" style="display:none;padding:10px;font-size:11px;text-align:center;" title="{LANG.rename}">
	<div id="filerenameOrigName" style="font-weight:800;margin-bottom:10px" class="dynamic"></div>
	<div style="margin-top:10px;margin-bottom:10px">
		{LANG.rename_newname}:
		<input style="width:200px;margin-left:5px" type="text" name="filerenameNewName" maxlength="255" class="dynamic" />
		<span title="Ext">Ext</span>
	</div>
	 
	<input style="width:60px;" type="button" value="OK" name="filerenameOK" />
</div>

<script type="text/javascript"> 
$(function() {
	$("#imgfolder").load("?mod=media&act=remote&do=folderlist&path={data.path}&folder={data.folder}&random=" + randomNum(10));
	$("#imglist").load("?mod=media&act=remote&do=imglist&path={data.path_img}&type={data.type}&random=" + randomNum(10))
});  
</script> 
<!-- END: html_popup_media -->






<!-- BEGIN: html_editor_media --> 
<div class="media-content">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="200" valign="top">
    <div id="imgfolder"><p style="padding:20px; text-align:center"><img alt="" src="{DIR_IMAGE}/load_bar.gif" /> please wait...</p></div>
    </td>
    <td  valign="top">
    	<div class="filebrowse">
      	<div id="imglist"><p style="padding:20px; text-align:center"><img alt="" src="{DIR_IMAGE}/load_bar.gif" /> please wait...</p></div>
      </div>
    </td>
    
    <td  width="300" valign="top">
    	<div class="filedetails" >
      	<div id="imgdetail" style="display:none;" >
        	
          <h3>Chi tiết hình</h3>
          
          <div class="attachment-info">
            <div class="thumbnail"><img id="detail-img" src=""></div>
            <div class="details">
              <div class="filename">&nbsp;</div>
              <div class="uploaded">&nbsp;</div>					
              <div class="dimensions">&nbsp;</div>				
            </div>
          </div>
    	
          <div class="file-setting" >    		
            <div class="setting"><label data-setting="title" >Tên hình</label><div class="div-text"><input type="text" name="pic_name" id="pic_name" value="Tulips-12"></div> </div>
            <div class="setting"><label data-setting="alt" >Alt text</label><div class="div-text"><input type="text" name="pic_alt" id="pic_alt" value=""> </div>
            <div class="setting"><label data-setting="size">Chiều ngang</label><div class="div-text"><input type="text" name="pic_width" id="pic_width" value="" size="5"> px </div></div>
          </div> 
                   
        </div>
        
        <div class="div-button"><input type="hidden" name="h_fileId" id="h_fileId" value="0" /><a href="#" class="button" id="btnInsertPost"><span>Chèn vào bài viết</span></a></div>
      </div>
    </td>
  </tr>
</table>
<div class="clear"></div>
</div>
<div class="media-footer"> 
<div class="editor_refresh">
		<a href="#" title="{LANG.refresh}"><img alt="{LANG.refresh}" src="{DIR_IMAGE}/refresh.png" width="16" height="16"/></a>
	</div>
	<div class="filetype"><input type="hidden" value="{data.type}" id="imgtype" name="imgtype" /></div>
	<div class="sortFile">
 
		<select name="order" class="select">
			<option value="0">{LANG.order0}</option>
			<option value="1">{LANG.order1}</option>
			<option value="2">{LANG.order2}</option>
		</select>
	</div>
	<div class="editor_search">
		<a href="#" title="{LANG.search}"><img alt="{LANG.search}" src="{DIR_IMAGE}/search.png" width="16" height="16"/></a>
	</div>
	<div class="uploadForm" >
		<div style="margin-top:5px;margin-right:5px;float:left;" id="cfile">
			{LANG.upload_file}
		</div>
		<div class="upload"><input type="file" name="upload" id="myfile"/>
		</div>
		<div style="margin-top:10px;float:left;display:none"><img src="{DIR_IMAGE}/load_bar.gif"/>
		</div>
		<div style="margin-top:5px;margin-left:5px;float:left;display:none"><img src="{DIR_IMAGE}/ok.png"/>
		</div>
		<div style="margin-top:7px;margin-left:5px;margin-right:5px;float:left;display:none"><img src="{DIR_IMAGE}/error.png"/>
		</div>
		<div style="float:left;margin-left:5px;">
			{LANG.upload_otherurl}: <input type="text" name="imgurl"/>
		</div>
		<div style="margin-top:10px;margin-left:5px;margin-right:5px;float:left;display:none"><img src="{DIR_IMAGE}/load_bar.gif"/>
		</div>
		<div style="margin-top:5px;margin-left:5px;margin-right:5px;float:left;display:none"><img src="{DIR_IMAGE}/ok.png"/>
		</div>
		<div style="margin-top:7px;margin-left:5px;margin-right:5px;float:left;display:none"><img src="{DIR_IMAGE}/error.png"/>
		</div>
		<div style="float:left;"><input type="button" value="Upload" id="editor_confirm" class="button" />
		</div>
	</div>
	<div class="notupload" style="display:none">
		{LANG.notupload}
	</div>
	<div class="clear"></div>
</div> 

<input type="hidden" name="module" value="{data.module}"/>
<input type="hidden" name="currentFileUpload" value=""/>
<input type="hidden" name="currentFileUrl" value=""/>
<input type="hidden" name="selFile" value=""/>
<input type="hidden" name="CKEditorFuncNum" value="{data.CKEditorFuncNum}"/>
<input type="hidden" name="area" value="{AREA}"/>
<input type="hidden" name="alt" value="{ALT}"/>
<input type="hidden" name="show_pic" value="{data.show_pic}"/>
<div style="display:none" id="contextMenu"></div>
<div style="display:none">
	<iframe id="Fdownload" src="" width="0" height="0" frameborder="0"></iframe>
</div>

<div id="filesearch" style="display:none;padding:10px;font-size:11px;" title="{LANG.search}">
	<form method="get" onsubmit="return vnTMedia.searchfile();">
		{LANG.searchdir}:
		<div style="margin:10px 0 20px">
			<select name="searchPath"></select>
		</div>
		{LANG.searchkey}:
		<div style="margin:10px 0"><input name="q" type="text" class="w200 dynamic" />
		</div>
		<input style="margin-left:50px;width:100px;" type="submit" value="{LANG.search}" name="search" />
	</form>
</div>

<div id="errorInfo" style="display:none" title="{LANG.errorInfo}"></div>
<div id="imgpreview" style="overflow:auto;display:none" title="{LANG.preview}">
	<div style="text-align:center;font-size:12px;font-weight:800;margin-top:10px" id="fileInfoAlt" class="dynamic"></div>
	<div style="text-align:center;margin-top:10px" id="fileView" class="dynamic"><span class="fileView"></span></div>
	<div style="text-align:center;font-size:12px;font-weight:800;margin-top:10px" id="fileInfoName" class="dynamic"></div>
	<div style="text-align:center;font-size:11px;margin-top:10px;margin-bottom:10px" id="fileInfoDetail" class="dynamic"></div>
</div>
<div id="createfolder" style="display:none" title="{LANG.createfolder}">
	{LANG.foldername}<input type="text" name="createfoldername"/>
</div>
<div id="filerename" style="display:none;padding:10px;font-size:11px;text-align:center;" title="{LANG.rename}">
	<div id="filerenameOrigName" style="font-weight:800;margin-bottom:10px" class="dynamic"></div>
	<div style="margin-top:10px;margin-bottom:10px">
		{LANG.rename_newname}:
		<input style="width:200px;margin-left:5px" type="text" name="filerenameNewName" maxlength="255" class="dynamic" />
		<span title="Ext">Ext</span>
	</div>
	 
	<input style="width:60px;" type="button" value="OK" name="filerenameOK" />
</div>

<script type="text/javascript"> 
$(function() {
	$("#imgfolder").load("?mod=media&act=editor&do=folderlist&path={data.path}&folder={data.folder}&random=" + randomNum(10));
	$("#imglist").load("?mod=media&act=editor&do=imglist&path={data.path_img}&type={data.type}&random=" + randomNum(10))
});  
</script> 
<!-- END: html_editor_media -->


