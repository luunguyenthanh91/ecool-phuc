<!-- BEGIN: html_popup -->
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>[:: Admin ::]</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <link rel="SHORTCUT ICON" href="vntrust.ico" type="image/x-icon"/>
    <link rel="icon" href="vntrust.ico" type="image/gif">
    <LINK href="{DIR_STYLE}/global.css" rel="stylesheet" type="text/css">
    <LINK href="modules/media_ad/css/media.css" rel="stylesheet" type="text/css">
    <LINK href="{DIR_JS}/jquery_plugins/treeview/jquery.treeview.css" rel="stylesheet" type="text/css">
    <LINK href="{DIR_JS}/jquery_ui/themes/base/ui.all.css" rel="stylesheet" type="text/css">

    <script type='text/javascript' src="{DIR_JS}/jquery.min.js"></script>
    <script type='text/javascript' src="{DIR_JS}/jquery-migrate.min.js"></script>

    <script type="text/javascript" src="{DIR_JS}/jquery_ui/ui.core.js?t=1"></script>
    <script type="text/javascript" src="{DIR_JS}/jquery_ui/ui.draggable.js?t=1"></script>
    <script type="text/javascript" src="{DIR_JS}/jquery_ui/ui.selectable.js?t=1"></script>
    <script language="javascript1.2" src="{DIR_JS}/jquery_plugins/jquery.lazyload.js"></script>
    <script language="javascript1.2" src="{DIR_JS}/jquery_plugins/treeview/jquery.treeview.js"></script> 

    <script type="text/javascript">
      var ROOT = "{CONF.rooturl}";
      var ROOT_URI = "{ROOT_URI}";
      var DIR_IMAGE = "{DIR_IMAGE}";
      var win = window.dialogArguments || opener || parent || top;
      var LANG = [];
      var remote_url = "?mod=media&act=remote&do=";

    </script>


</head>
<body style=" background:  #F0F0F0; margin:0px;">
<div class="popupContent">
    {data.main}
</div>

<script language="javascript1.2" src="modules/media_ad/js/gallery.js"></script>

<script type="text/javascript">
  $(function () {   vnTGallery.init();  });
</script>

</body>
</html>
<!-- END: html_popup -->


<!-- BEGIN: html_gallery -->
<div class="media-content">
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td width="200" valign="top">
                <div id="imgfolder" class="imgfolder"><p style="padding:20px; text-align:center"><img alt="" src="{DIR_IMAGE}/load_bar.gif"/> please wait...</p></div>
            </td>
            <td valign="top">
                <div class="filebrowse">
                    <div id="imglist"><p style="padding:20px; text-align:center"><img alt=""  src="{DIR_IMAGE}/load_bar.gif" />  please wait...</p></div>
                </div>
            </td>
        </tr>
    </table>
    <div class="clear"></div>
</div>

<div class="media-footer">
    <div class="footer-btn" style="float: right">
        <button class="button" type="button" onclick="vnTGallery.insertValue()"><span>Submit</span></button>
    </div>
    <div class="clear"></div>
</div>
<input type="hidden" name="obj" value="{data.obj_return}"/>
<input type="hidden" name="module" value="{data.module}"/>
<input type="hidden" name="selFile" value=""/>
<div style="display:none" id="contextMenu"></div>
<div id="errorInfo" style="display:none" title="{LANG.errorInfo}"></div>
<div id="imgpreview" style="overflow:auto;display:none" title="{LANG.preview}">
    <div style="text-align:center;font-size:12px;font-weight:800;margin-top:10px" id="fileInfoAlt"
         class="dynamic"></div>
    <div style="text-align:center;margin-top:10px" id="fileView" class="dynamic"><span class="fileView"></span></div>
    <div style="text-align:center;font-size:12px;font-weight:800;margin-top:10px" id="fileInfoName"
         class="dynamic"></div>
    <div style="text-align:center;font-size:11px;margin-top:10px;margin-bottom:10px" id="fileInfoDetail"
         class="dynamic"></div>
</div>


<script type="text/javascript">


  $(function () {
    $("#imgfolder").load("?mod=media&act=remote&do=folder_gallery&path={data.path}&folder={data.folder}&random=" + randomNum(10));
    $("#imglist").load("?mod=media&act=remote&do=gallery&path={data.path_img}&type={data.type}&random=" + randomNum(10))
  });
</script>
<!-- END: html_gallery -->
