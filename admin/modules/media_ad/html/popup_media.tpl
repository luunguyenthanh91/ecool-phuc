<!-- BEGIN: html_popup -->
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>[:: Admin ::]</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link rel="SHORTCUT ICON" href="vntrust.ico" type="image/x-icon" />
<link rel="icon" href="vntrust.ico" type="image/gif" >
<LINK href="{DIR_STYLE}/global.css" rel="stylesheet" type="text/css">
<LINK href="modules/media_ad/css/media.css" rel="stylesheet" type="text/css">
<LINK href="{DIR_JS}/jquery_plugins/treeview/jquery.treeview.css" rel="stylesheet" type="text/css">
<LINK href="{DIR_JS}/jquery_ui/themes/base/ui.all.css" rel="stylesheet" type="text/css">

<script type='text/javascript' src="{DIR_JS}/jquery.min.js"></script>
<script type='text/javascript' src="{DIR_JS}/jquery-migrate.min.js"></script>

<script type="text/javascript" src="{DIR_JS}/jquery_ui/ui.core.js?t=1"></script>
<script type="text/javascript" src="{DIR_JS}/jquery_ui/ui.draggable.js?t=1"></script>
<script type="text/javascript" src="{DIR_JS}/jquery_ui/ui.selectable.js?t=1"></script>

<script type="text/javascript" src="{DIR_JS}/jquery_ui/ui.resizable.js?t=1"></script>
<script type="text/javascript" src="{DIR_JS}/jquery_ui/ui.button.js?t=1"></script>
<script type="text/javascript" src="{DIR_JS}/jquery_ui/ui.dialog.js?t=1"></script>

<script language="javascript1.2" src="{DIR_JS}/contextmenu/jquery.contextmenu.js"></script>
<script language="javascript1.2" src="{DIR_JS}/jquery_plugins/jquery.lazyload.js"></script>
<script type="text/javascript" src="{DIR_JS}/jquery_plugins/jquery.flash.js?t=1"></script>
<script language="javascript1.2" src="{DIR_JS}/jquery_plugins/treeview/jquery.treeview.js"></script>
 
<script language="javascript1.2" src="{DIR_JS}/jquery_plugins/jquery.upload.js?v=1.0"></script>
<script language="javascript1.2" src="{DIR_JS}/admin/js_admin.js?v=1.0"></script>

<script type="text/javascript">

	var ROOT = "{CONF.rooturl}";
	var ROOT_URI = "{ROOT_URI}";
	var DIR_IMAGE = "{DIR_IMAGE}";
	var win = window.dialogArguments || opener || parent || top;
	var obj_return = "{data.obj_return}"
//<![CDATA[
var LANG = [];
LANG.upload_size = "Kích thước";
LANG.pubdate = "Cập nhật";
LANG.download = "Tải về";
LANG.preview = "Xem chi tiết";
LANG.addlogo = "Thêm Logo";
LANG.select = "Chọn";
LANG.upload_createimage = "Công cụ ảnh";
LANG.move = "Di chuyển";
LANG.rename = "Đổi tên file";
LANG.upload_delfile = "Xóa file";
LANG.createfolder = "Tạo folder";
LANG.renamefolder = "Đổi tên folder";
LANG.deletefolder = "Xóa folder";
LANG.delete_folder = "Bạn có chắc muốn xóa thư mục này không. Nếu xóa thư mục này đồng nghĩa với việc toàn bộ các file trong thư mục này cũng bị xóa ?";
LANG.rename_nonamefolder = "Bạn chưa đặt tên mới cho thư mục hoặc tên thư mục không đúng quy chuẩn";
LANG.folder_exists = "Lỗi! Đã có thư mục cùng tên tồn tại";
LANG.name_folder_error = "Bạn chưa đặt tên cho thư mục hoặc tên không đúng quy chuẩn";
LANG.rename_noname = "Bạn chưa đặt tên mới cho file";
LANG.upload_delimg_confirm = "Bạn có chắc muốn xóa file";
LANG.origSize = "Kích thước gốc";
LANG.errorMinX = "Lỗi: Chiều rộng nhỏ hơn mức cho phép";
LANG.errorMaxX = "Lỗi: Chiều rộng lớn hơn mức cho phép";
LANG.errorMinY = "Lỗi: Chiều cao nhỏ hơn mức cho phép";
LANG.errorMaxY = "Lỗi: Chiều cao lớn hơn mức cho phép";
LANG.errorEmptyX = "Lỗi: Chiều rộng chưa xác định";
LANG.errorEmptyY = "Lỗi: Chiều cao chưa xác định";
var nv_max_width = '1500', nv_max_height = '1500', nv_min_width = '10', nv_min_height = '10';
var remote_url = "index.php?mod=media&act=remote&do=", nv_namecheck = /^([a-zA-Z0-9_-])+$/, array_images = ["gif", "jpg", "jpeg", "pjpeg", "png"], array_flash = ["swf", "swc", "flv"], array_archives = ["rar", "zip", "tar"], array_documents = ["doc", "xls", "chm", "pdf", "docx", "xlsx"];
//]]>
 
</script>

 
</head>
<body style=" background:url({DIR_IMAGE_MEDIA}/images/media_footer.png) #F0F0F0; margin:0px;" >
	<div class="popupContent">
  	{data.main}
  </div>

<script language="javascript1.2" src="modules/media_ad/js/media.js"></script>  
</body>
</html>
<!-- END: html_popup -->

 
 
<!-- BEGIN: html_popup_media --> 
<div class="media-content">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="200" valign="top">
    <div id="imgfolder" class="imgfolder"><p style="padding:20px; text-align:center"><img alt="" src="{DIR_IMAGE}/load_bar.gif" /> please wait...</p></div>
    </td>
    <td  valign="top">
    	<div class="filebrowse">
      	<div id="imglist"><p style="padding:20px; text-align:center"><img alt="" src="{DIR_IMAGE}/load_bar.gif" /> please wait...</p></div>
      </div>
    </td>
  </tr>
</table>
<div class="clear"></div>
</div>
<div class="media-footer"> 
<div class="refresh">
		<a href="#" title="{LANG.refresh}"><img alt="{LANG.refresh}" src="{DIR_IMAGE}/refresh.png" width="16" height="16"/></a>
	</div>
	<div class="filetype">{data.list_filetype}</div>
	<div class="sortFile">
 
		<select name="order" class="select">
			<option value="0">{LANG.order0}</option>
			<option value="1">{LANG.order1}</option>
			<option value="2">{LANG.order2}</option>
		</select>
	</div>
	<div class="search">
		<a href="#" title="{LANG.search}"><img alt="{LANG.search}" src="{DIR_IMAGE}/search.png" width="16" height="16"/></a>
	</div>
	<div class="uploadForm" >
		<div style="margin-top:5px;margin-right:5px;float:left;" id="cfile">
			{LANG.upload_file}
		</div>
		<div class="upload"><input type="file" name="upload" id="myfile"/>
		</div>
		<div style="margin-top:10px;float:left;display:none"><img src="{DIR_IMAGE}/load_bar.gif"/>
		</div>
		<div style="margin-top:5px;margin-left:5px;float:left;display:none"><img src="{DIR_IMAGE}/ok.png"/>
		</div>
		<div style="margin-top:7px;margin-left:5px;margin-right:5px;float:left;display:none"><img src="{DIR_IMAGE}/error.png"/>
		</div>
		<div style="float:left;margin-left:5px;">
			{LANG.upload_otherurl}: <input type="text" name="imgurl"/>
		</div>
		<div style="margin-top:10px;margin-left:5px;margin-right:5px;float:left;display:none"><img src="{DIR_IMAGE}/load_bar.gif"/>
		</div>
		<div style="margin-top:5px;margin-left:5px;margin-right:5px;float:left;display:none"><img src="{DIR_IMAGE}/ok.png"/>
		</div>
		<div style="margin-top:7px;margin-left:5px;margin-right:5px;float:left;display:none"><img src="{DIR_IMAGE}/error.png"/>
		</div>
		<div style="float:left;"><input type="button" value="Upload" id="confirm" class="button" />
		</div>
	</div>
	<div class="notupload" style="display:none">
		{LANG.notupload}
	</div>
	<div class="clear"></div>
</div> 

<input type="hidden" name="module" value="{data.module}"/>
<input type="hidden" name="currentFileUpload" value=""/>
<input type="hidden" name="currentFileUrl" value=""/>
<input type="hidden" name="selFile" value=""/>
<input type="hidden" name="CKEditorFuncNum" value="{data.CKEditorFuncNum}"/>
<input type="hidden" name="area" value="{AREA}"/>
<input type="hidden" name="alt" value="{ALT}"/>
<input type="hidden" name="show_pic" value="{data.show_pic}"/>
<div style="display:none" id="contextMenu"></div>
<div style="display:none">
	<iframe id="Fdownload" src="" width="0" height="0" frameborder="0"></iframe>
</div>

<div id="filesearch" style="display:none;padding:10px;font-size:11px;" title="{LANG.search}">
	<form method="get" onsubmit="return searchfile();">
		{LANG.searchdir}:
		<div style="margin:10px 0 20px">
			<select name="searchPath"></select>
		</div>
		{LANG.searchkey}:
		<div style="margin:10px 0"><input name="q" type="text" class="w200 dynamic" />
		</div>
		<input style="margin-left:50px;width:100px;" type="submit" value="{LANG.search}" name="search" />
	</form>
</div>

<div id="errorInfo" style="display:none" title="{LANG.errorInfo}"></div>
<div id="imgpreview" style="overflow:auto;display:none" title="{LANG.preview}">
	<div style="text-align:center;font-size:12px;font-weight:800;margin-top:10px" id="fileInfoAlt" class="dynamic"></div>
	<div style="text-align:center;margin-top:10px" id="fileView" class="dynamic"><span class="fileView"></span></div>
	<div style="text-align:center;font-size:12px;font-weight:800;margin-top:10px" id="fileInfoName" class="dynamic"></div>
	<div style="text-align:center;font-size:11px;margin-top:10px;margin-bottom:10px" id="fileInfoDetail" class="dynamic"></div>
</div>
<div id="createfolder" style="display:none" title="{LANG.createfolder}">
	{LANG.foldername}<input type="text" name="createfoldername"/>
</div>
<div id="filerename" style="display:none;padding:10px;font-size:11px;text-align:center;" title="{LANG.rename}">
	<div id="filerenameOrigName" style="font-weight:800;margin-bottom:10px" class="dynamic"></div>
	<div style="margin-top:10px;margin-bottom:10px">
		{LANG.rename_newname}:
		<input style="width:200px;margin-left:5px" type="text" name="filerenameNewName" maxlength="255" class="dynamic" />
		<span title="Ext">Ext</span>
	</div>
	 
	<input style="width:60px;" type="button" value="OK" name="filerenameOK" />
</div>

<script type="text/javascript"> 
$(function() {
	$("#imgfolder").load("?mod=media&act=remote&do=folderlist&path={data.path}&folder={data.folder}&random=" + randomNum(10));
	$("#imglist").load("?mod=media&act=remote&do=imglist&path={data.path_img}&type={data.type}&random=" + randomNum(10))
});  
</script> 
<!-- END: html_popup_media -->






<!-- BEGIN: html_editor_media --> 
<div class="media-content">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="200" valign="top">
    <div id="imgfolder"><p style="padding:20px; text-align:center"><img alt="" src="{DIR_IMAGE}/load_bar.gif" /> please wait...</p></div>
    </td>
    <td  valign="top">
    	<div class="filebrowse">
      	<div id="imglist"><p style="padding:20px; text-align:center"><img alt="" src="{DIR_IMAGE}/load_bar.gif" /> please wait...</p></div>
      </div>
    </td>
    
    <td  width="300" valign="top">
    	<div class="filedetails" >
      	<div id="imgdetail" style="display:none;" >
        	
          <h3>Chi tiết hình</h3>
          
          <div class="attachment-info">
            <div class="thumbnail"><img id="detail-img" src=""></div>
            <div class="details">
              <div class="filename">&nbsp;</div>
              <div class="uploaded">&nbsp;</div>					
              <div class="dimensions">&nbsp;</div>				
            </div>
          </div>
    	
          <div class="file-setting" >    		
            <div class="setting"><label data-setting="title" >Tên hình</label><div class="div-text"><input type="text" name="pic_name" id="pic_name" value="Tulips-12"></div> </div>
            <div class="setting"><label data-setting="alt" >Alt text</label><div class="div-text"><input type="text" name="pic_alt" id="pic_alt" value=""> </div>
            <div class="setting"><label data-setting="size">Chiều ngang</label><div class="div-text"><input type="text" name="pic_width" id="pic_width" value="" size="5"> px </div></div>
          </div> 
                   
        </div>
        
        <div class="div-button"><input type="hidden" name="h_fileId" id="h_fileId" value="0" /><a href="#" class="button" id="btnInsertPost"><span>Chèn vào bài viết</span></a></div>
      </div>
    </td>
  </tr>
</table>
<div class="clear"></div>
</div>
<div class="media-footer"> 
<div class="editor_refresh">
		<a href="#" title="{LANG.refresh}"><img alt="{LANG.refresh}" src="{DIR_IMAGE}/refresh.png" width="16" height="16"/></a>
	</div>
	<div class="filetype"><input type="hidden" value="{data.type}" id="imgtype" name="imgtype" /></div>
	<div class="sortFile">
 
		<select name="order" class="select">
			<option value="0">{LANG.order0}</option>
			<option value="1">{LANG.order1}</option>
			<option value="2">{LANG.order2}</option>
		</select>
	</div>
	<div class="editor_search">
		<a href="#" title="{LANG.search}"><img alt="{LANG.search}" src="{DIR_IMAGE}/search.png" width="16" height="16"/></a>
	</div>
	<div class="uploadForm" >
		<div style="margin-top:5px;margin-right:5px;float:left;" id="cfile">
			{LANG.upload_file}
		</div>
		<div class="upload"><input type="file" name="upload" id="myfile"/>
		</div>
		<div style="margin-top:10px;float:left;display:none"><img src="{DIR_IMAGE}/load_bar.gif"/>
		</div>
		<div style="margin-top:5px;margin-left:5px;float:left;display:none"><img src="{DIR_IMAGE}/ok.png"/>
		</div>
		<div style="margin-top:7px;margin-left:5px;margin-right:5px;float:left;display:none"><img src="{DIR_IMAGE}/error.png"/>
		</div>
		<div style="float:left;margin-left:5px;">
			{LANG.upload_otherurl}: <input type="text" name="imgurl"/>
		</div>
		<div style="margin-top:10px;margin-left:5px;margin-right:5px;float:left;display:none"><img src="{DIR_IMAGE}/load_bar.gif"/>
		</div>
		<div style="margin-top:5px;margin-left:5px;margin-right:5px;float:left;display:none"><img src="{DIR_IMAGE}/ok.png"/>
		</div>
		<div style="margin-top:7px;margin-left:5px;margin-right:5px;float:left;display:none"><img src="{DIR_IMAGE}/error.png"/>
		</div>
		<div style="float:left;"><input type="button" value="Upload" id="editor_confirm" class="button" />
		</div>
	</div>
	<div class="notupload" style="display:none">
		{LANG.notupload}
	</div>
	<div class="clear"></div>
</div> 

<input type="hidden" name="module" value="{data.module}"/>
<input type="hidden" name="currentFileUpload" value=""/>
<input type="hidden" name="currentFileUrl" value=""/>
<input type="hidden" name="selFile" value=""/>
<input type="hidden" name="CKEditorFuncNum" value="{data.CKEditorFuncNum}"/>
<input type="hidden" name="area" value="{AREA}"/>
<input type="hidden" name="alt" value="{ALT}"/>
<input type="hidden" name="show_pic" value="{data.show_pic}"/>
<div style="display:none" id="contextMenu"></div>
<div style="display:none">
	<iframe id="Fdownload" src="" width="0" height="0" frameborder="0"></iframe>
</div>

<div id="filesearch" style="display:none;padding:10px;font-size:11px;" title="{LANG.search}">
	<form method="get" onsubmit="return vnTMedia.searchfile();">
		{LANG.searchdir}:
		<div style="margin:10px 0 20px">
			<select name="searchPath"></select>
		</div>
		{LANG.searchkey}:
		<div style="margin:10px 0"><input name="q" type="text" class="w200 dynamic" />
		</div>
		<input style="margin-left:50px;width:100px;" type="submit" value="{LANG.search}" name="search" />
	</form>
</div>

<div id="errorInfo" style="display:none" title="{LANG.errorInfo}"></div>
<div id="imgpreview" style="overflow:auto;display:none" title="{LANG.preview}">
	<div style="text-align:center;font-size:12px;font-weight:800;margin-top:10px" id="fileInfoAlt" class="dynamic"></div>
	<div style="text-align:center;margin-top:10px" id="fileView" class="dynamic"><span class="fileView"></span></div>
	<div style="text-align:center;font-size:12px;font-weight:800;margin-top:10px" id="fileInfoName" class="dynamic"></div>
	<div style="text-align:center;font-size:11px;margin-top:10px;margin-bottom:10px" id="fileInfoDetail" class="dynamic"></div>
</div>
<div id="createfolder" style="display:none" title="{LANG.createfolder}">
	{LANG.foldername}<input type="text" name="createfoldername"/>
</div>
<div id="filerename" style="display:none;padding:10px;font-size:11px;text-align:center;" title="{LANG.rename}">
	<div id="filerenameOrigName" style="font-weight:800;margin-bottom:10px" class="dynamic"></div>
	<div style="margin-top:10px;margin-bottom:10px">
		{LANG.rename_newname}:
		<input style="width:200px;margin-left:5px" type="text" name="filerenameNewName" maxlength="255" class="dynamic" />
		<span title="Ext">Ext</span>
	</div>
	 
	<input style="width:60px;" type="button" value="OK" name="filerenameOK" />
</div>

<script type="text/javascript"> 
$(function() {
	$("#imgfolder").load("?mod=media&act=editor&do=folderlist&path={data.path}&folder={data.folder}&random=" + randomNum(10));
	$("#imglist").load("?mod=media&act=editor&do=imglist&path={data.path_img}&type={data.type}&random=" + randomNum(10))
});  
</script> 
<!-- END: html_editor_media -->






<!-- BEGIN: html_browse_media -->
<div class="media-content">
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td width="200" valign="top">
				<div id="imgfolder" class="imgfolder"><p style="padding:20px; text-align:center"><img alt="" src="{DIR_IMAGE}/load_bar.gif" /> please wait...</p></div>
			</td>
			<td  valign="top">
				<div class="filebrowse">
					<div id="imglist"><p style="padding:20px; text-align:center"><img alt="" src="{DIR_IMAGE}/load_bar.gif" /> please wait...</p></div>
				</div>
			</td>
		</tr>
	</table>
	<div class="clear"></div>
</div>
<div class="media-footer">
	<div class="refresh">
		<a href="#" title="{LANG.refresh}"><img alt="{LANG.refresh}" src="{DIR_IMAGE}/refresh.png" width="16" height="16"/></a>
	</div>
	<div class="filetype">{data.list_filetype}</div>
	<div class="sortFile">

		<select name="order" class="select">
			<option value="0">{LANG.order0}</option>
			<option value="1">{LANG.order1}</option>
			<option value="2">{LANG.order2}</option>
		</select>
	</div>
	<div class="search">
		<a href="#" title="{LANG.search}"><img alt="{LANG.search}" src="{DIR_IMAGE}/search.png" width="16" height="16"/></a>
	</div>
	<div class="uploadForm" >
		<div style="margin-top:5px;margin-right:5px;float:left;" id="cfile">
			{LANG.upload_file}
		</div>
		<div class="upload"><input type="file" name="upload" id="myfile"/>
		</div>
		<div style="margin-top:10px;float:left;display:none"><img src="{DIR_IMAGE}/load_bar.gif"/>
		</div>
		<div style="margin-top:5px;margin-left:5px;float:left;display:none"><img src="{DIR_IMAGE}/ok.png"/>
		</div>
		<div style="margin-top:7px;margin-left:5px;margin-right:5px;float:left;display:none"><img src="{DIR_IMAGE}/error.png"/>
		</div>
		<div style="float:left;margin-left:5px;">
			{LANG.upload_otherurl}: <input type="text" name="imgurl"/>
		</div>
		<div style="margin-top:10px;margin-left:5px;margin-right:5px;float:left;display:none"><img src="{DIR_IMAGE}/load_bar.gif"/>
		</div>
		<div style="margin-top:5px;margin-left:5px;margin-right:5px;float:left;display:none"><img src="{DIR_IMAGE}/ok.png"/>
		</div>
		<div style="margin-top:7px;margin-left:5px;margin-right:5px;float:left;display:none"><img src="{DIR_IMAGE}/error.png"/>
		</div>
		<div style="float:left;"><input type="button" value="Upload" id="confirm" class="button" />
		</div>
	</div>
	<div class="notupload" style="display:none">
		{LANG.notupload}
	</div>
	<div class="clear"></div>
</div>

<input type="hidden" name="module" value="{data.module}"/>
<input type="hidden" name="currentFileUpload" value=""/>
<input type="hidden" name="currentFileUrl" value=""/>
<input type="hidden" name="selFile" value=""/>
<input type="hidden" name="CKEditorFuncNum" value="{data.CKEditorFuncNum}"/>
<input type="hidden" name="area" value="{AREA}"/>
<input type="hidden" name="alt" value="{ALT}"/>
<input type="hidden" name="show_pic" value="{data.show_pic}"/>
<div style="display:none" id="contextMenu"></div>
<div style="display:none">
	<iframe id="Fdownload" src="" width="0" height="0" frameborder="0"></iframe>
</div>

<div id="filesearch" style="display:none;padding:10px;font-size:11px;" title="{LANG.search}">
	<form method="get" onsubmit="return searchfile();">
		{LANG.searchdir}:
		<div style="margin:10px 0 20px">
			<select name="searchPath"></select>
		</div>
		{LANG.searchkey}:
		<div style="margin:10px 0"><input name="q" type="text" class="w200 dynamic" />
		</div>
		<input style="margin-left:50px;width:100px;" type="submit" value="{LANG.search}" name="search" />
	</form>
</div>

<div id="errorInfo" style="display:none" title="{LANG.errorInfo}"></div>
<div id="imgpreview" style="overflow:auto;display:none" title="{LANG.preview}">
	<div style="text-align:center;font-size:12px;font-weight:800;margin-top:10px" id="fileInfoAlt" class="dynamic"></div>
	<div style="text-align:center;margin-top:10px" id="fileView" class="dynamic"><span class="fileView"></span></div>
	<div style="text-align:center;font-size:12px;font-weight:800;margin-top:10px" id="fileInfoName" class="dynamic"></div>
	<div style="text-align:center;font-size:11px;margin-top:10px;margin-bottom:10px" id="fileInfoDetail" class="dynamic"></div>
</div>
<div id="createfolder" style="display:none" title="{LANG.createfolder}">
	{LANG.foldername}<input type="text" name="createfoldername"/>
</div>
<div id="filerename" style="display:none;padding:10px;font-size:11px;text-align:center;" title="{LANG.rename}">
	<div id="filerenameOrigName" style="font-weight:800;margin-bottom:10px" class="dynamic"></div>
	<div style="margin-top:10px;margin-bottom:10px">
		{LANG.rename_newname}:
		<input style="width:200px;margin-left:5px" type="text" name="filerenameNewName" maxlength="255" class="dynamic" />
		<span title="Ext">Ext</span>
	</div>

	<input style="width:60px;" type="button" value="OK" name="filerenameOK" />
</div>

<script type="text/javascript">
	$(function() {
		$("#imgfolder").load("?mod=media&act=remote&do=folderlist&path={data.path}&folder={data.folder}&random=" + randomNum(10));
		$("#imglist").load("?mod=media&act=remote&do=imglist&path={data.path_img}&type={data.type}&random=" + randomNum(10))
	});
</script>
<!-- END: html_browse_media -->



<!-- BEGIN: html_editor_gallery -->
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>[:: Admin ::]</title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<link rel="SHORTCUT ICON" href="vntrust.ico" type="image/x-icon" />
	<link rel="icon" href="vntrust.ico" type="image/gif" >
	<link href="{DIR_JS}/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
	<link href="{DIR_STYLE}/fonts/fontawesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />

	<LINK href="modules/media_ad/css/media.css" rel="stylesheet" type="text/css">
	<script type='text/javascript' src="{DIR_JS}/jquery.min.js"></script>
	<script type='text/javascript' src="{DIR_JS}/jquery-migrate.min.js"></script>


	<script type="text/javascript">

		var ROOT = "{CONF.rooturl}";
		var ROOT_URI = "{ROOT_URI}";
		var DIR_IMAGE = "{DIR_IMAGE}";
		var win = window.dialogArguments || opener || parent || top;
	</script>

	<LINK href="{DIR_JS}/thickbox/thickbox.css" rel="stylesheet" type="text/css">
	<link rel="stylesheet" href="{DIR_JS}/jquery-ui/jquery-ui.min.css" type="text/css" />
	<link rel="stylesheet" type="text/css" href="{DIR_JS}/plupload_236/jquery.ui.plupload/css/jquery.ui.plupload.css"  />
	<script type='text/javascript' src='{DIR_JS}/thickbox/thickbox.js?v=3.1'></script>
	<script type="text/javascript" src="{DIR_JS}/bootstrap/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="{DIR_JS}/jquery-ui/jquery-ui.min.js"></script>
	<script type="text/javascript" src="{DIR_JS}/plupload_236/plupload.full.min.js" charset="UTF-8"></script>
	<script type="text/javascript" src="{DIR_JS}/plupload_236/i18n/vi.js" charset="UTF-8"></script>
	<script type="text/javascript" src="{DIR_JS}/plupload_236/jquery.ui.plupload/jquery.ui.plupload.js" charset="UTF-8"></script>





</head>
<body   >
<div class="popupGallery">


	<script type="text/javascript">
		/* <![CDATA[*/

		function do_insertGallery(id){
			self.parent.inset_gallery(id);
		}

		function attachCallbacks(a) {

			a.bind('FileUploaded', function(up, file,re) {
				var response = $.parseJSON(re.response) ;
				$("#photo_"+file.id).val(response.id+'|'+response.picture) ;


				1 == up.total.queued && ("undefined" == typeof debug ? $("#myForm").submit() : ($("#bugcallbacks").show(), 0 < up.e.code && $("#bugcallbacks").append("<div>File: " + up.result + " <br/ > Error code: " + file.message + " <hr></div>")))

			});


		}


		$(document).ready(function(){

			$("#photo").plupload({
				// General settings
				runtimes : 'gears,flash,silverlight,browserplus,html5',
				url : 'modules/media_ad/ajax/upload.php',
				max_file_size : '{data.max_upload}',
				chunk_size: '1mb',
				multipart: true,
				multipart_params: {
					'module' : 'media_gallery',
					'folder_id': '{data.folder_id}',
					'folder_upload': '{data.folder_upload}',
					'folder': '{data.folder}',
					'w' : '{data.w_pic}',
					'thumb' :1 ,
					'w_thumb': '{data.w_thumb}'
				},
				resize : {
					width : {data.w_pic},
					height : {data.w_pic},
					quality : 90
				},

				filters : [
					{title : "Image files", extensions : "jpg,gif,png"}
				],
				rename : true,
				sortable: true,
				dragdrop: true,
				views: {
					list: true,
					thumbs: true, // Show thumbs
					active: 'thumbs'
				},
				flash_swf_url : ROOT+'js/plupload/plupload.flash.swf',
				silverlight_xap_url : ROOT+'js/plupload/plupload.silverlight.xap' ,
				preinit: attachCallbacks
			});




			{data.js_extra}

		});



		/* ]]> */
	</script>



	<div>


	<ul class="nav nav-tabs" role="tablist">
		<li role="presentation" class="active"><a href="#tab1" aria-controls="tab1" role="tab" data-toggle="tab">Tạo Album hình mới </a></li>
		<li role="presentation"><a href="#tab2" aria-controls="tab2" role="tab" data-toggle="tab">Danh sách Album hình </a></li>

	</ul>


	<div class="tab-content">
		<div role="tabpanel" class="tab-pane active" id="tab1">

			<form action="{data.link_action}" method="post" name="myForm" id="myForm"   enctype="multipart/form-data" >
				{data.err}
				<div style="padding:15px 0px;">

					<input type="hidden" name="hlistfile" id="hlistfile" value="" />
					<div id="photo">
						<p>You browser doesn't have Flash, Silverlight, Gears, BrowserPlus or HTML5 support.</p>
					</div>


					<br />

					<div class="square note">
						<ul>
							<li>Mỗi lần upload tối đa <b>50 file</b>. Giữ phím <b>Ctrl</b> để chọn thêm ảnh, phím <b>Shift</b> để chọn cùng lúc nhiều file.</li>
							<li>Dung lượng mỗi file tối đa <b>{data.max_upload}</b></li>
						</ul>
						<div class="clear"></div>
					</div>

					<div id="bugcallbacks" style="display: none "></div>
				</div>

				<input type="hidden" name="do_submit" id="do_submit" value="1" />
			</form>

		</div>
		<div role="tabpanel" class="tab-pane" id="tab2">
			<div style="padding:15px 0px;" class="ListGallery">

			<table class="table table-bordered table-hover">
				<thead>
				<tr>
					<th width="30">#ID</th>
					<th width="50">Picture</th>
					<th width="30%">Title</th>
					<th>Infomation</th>
					<th width="210">Action</th>
				</tr>
				</thead>
				<tbody>

				<!-- BEGIN: html_item -->
				<tr>
					<td align="center" >{row.gid}</td>
					<td class="img"><a href="{row.src_big}" class="thickbox" title="{row.title}" ><img src="{row.src}" alt="{row.title}" /></a></td>
					<td><a href="{row.src_big}" class="thickbox" ><b>{row.title}</b></a></td>
					<td>
						<div class="num_photo">Số hình : <span>{row.num_photo}</span></div>
						<div class="date_post">Ngày tạo : <span>{row.text_date_post}</span></div>
					</td>
					<td nowrap >
						<div class="div-action"><a class="btn btn-primary" href="javascript:void(0)" onclick="do_insertGallery({row.gid})"><i class="fa fa-paper-plane" aria-hidden="true"></i> Chèn vào bài viết </a>
						<a class="btn btn-success" href="{row.link}"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Chi tiết</a>
						</div>
					</td>
				</tr>
				<!-- END: html_item -->

				</tbody>
			</table>
			</div>
		</div>

	</div>

</div>







</div>

</body>
</html>
<!-- END: html_editor_gallery -->



<!-- BEGIN: html_edit_gallery -->
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>[:: Admin ::]</title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<link rel="SHORTCUT ICON" href="vntrust.ico" type="image/x-icon" />
	<link rel="icon" href="vntrust.ico" type="image/gif" >
	<link href="{DIR_JS}/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
	<link href="{DIR_STYLE}/fonts/fontawesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />

	<LINK href="modules/media_ad/css/media.css" rel="stylesheet" type="text/css">
	<script type='text/javascript' src="{DIR_JS}/jquery.min.js"></script>
	<script type='text/javascript' src="{DIR_JS}/jquery-migrate.min.js"></script>


	<script type="text/javascript">

		var ROOT = "{CONF.rooturl}";
		var ROOT_URI = "{ROOT_URI}";
		var DIR_IMAGE = "{DIR_IMAGE}";
		var win = window.dialogArguments || opener || parent || top;
	</script>

	<LINK href="{DIR_JS}/thickbox/thickbox.css" rel="stylesheet" type="text/css">
	<link rel="stylesheet" href="{DIR_JS}/jquery-ui/jquery-ui.min.css" type="text/css" />
	<link rel="stylesheet" type="text/css" href="{DIR_JS}/plupload_236/jquery.ui.plupload/css/jquery.ui.plupload.css"  />
	<script type='text/javascript' src='{DIR_JS}/thickbox/thickbox.js?v=3.1'></script>
	<script type="text/javascript" src="{DIR_JS}/bootstrap/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="{DIR_JS}/jquery-ui/jquery-ui.min.js"></script>
	<script type="text/javascript" src="{DIR_JS}/plupload_236/plupload.full.min.js" charset="UTF-8"></script>
	<script type="text/javascript" src="{DIR_JS}/plupload_236/i18n/vi.js" charset="UTF-8"></script>
	<script type="text/javascript" src="{DIR_JS}/plupload_236/jquery.ui.plupload/jquery.ui.plupload.js" charset="UTF-8"></script>





</head>
<body   >
<div class="popupGallery">


	<script type="text/javascript">
		/* <![CDATA[*/

		function do_insertGallery(id){
			self.parent.inset_gallery(id);
		}

		function attachCallbacks(a) {

			a.bind('FileUploaded', function(up, file,re) {
				var response = $.parseJSON(re.response) ;
				$("#photo_"+file.id).val(response.id+'|'+response.picture) ;
				1 == up.total.queued && ("undefined" == typeof debug ? $("#myForm").submit() : ($("#bugcallbacks").show(), 0 < up.e.code && $("#bugcallbacks").append("<div>File: " + up.result + " <br/ > Error code: " + file.message + " <hr></div>")))
			});


		}


		$(document).ready(function(){

			$("#photo").plupload({
				// General settings
				runtimes : 'gears,flash,silverlight,browserplus,html5',
				url : 'modules/media_ad/ajax/upload.php',
				max_file_size : '{data.max_upload}',
				chunk_size: '1mb',
				multipart: true,
				multipart_params: {
					'module' : 'media_gallery',
					'folder_id': '{data.folder_id}',
					'folder_upload': '{data.folder_upload}',
					'folder': '{data.folder}',
					'w' : '{data.w_pic}',
					'thumb' :1 ,
					'w_thumb': '{data.w_thumb}'
				},
				resize : {
					width : {data.w_pic},
					height : {data.w_pic},
					quality : 90
				},

				filters : [
					{title : "Image files", extensions : "jpg,gif,png"}
				],
				rename : true,
				sortable: true,
				dragdrop: true,
				views: {
					list: true,
					thumbs: true, // Show thumbs
					active: 'thumbs'
				},
				flash_swf_url : ROOT+'js/plupload/plupload.flash.swf',
				silverlight_xap_url : ROOT+'js/plupload/plupload.silverlight.xap' ,
				preinit: attachCallbacks
			});




			$(".list_pic_old .pic_item .remove").click(function(){
				$(this).parent().remove();
			});

			$( "#list_pic_detail .div_sort" ).sortable({
				update: function(event, ui) {
					$( "#list_pic_detail .div_sort > div" ).each(function (e) {
						$(this).attr("data_count", (e+1));
					});
				}
			});

			{data.js_extra}

		});



		/* ]]> */
	</script>


<div class="galleryEdit">
	<div class="mid-title" >
		<div class="titleL"><div class="fTitle">{data.title}</div></div>
		<div class="titleR">
			<a class="btn btn-success" href="javascript:void(0)" onclick="do_insertGallery({row.gid})"><i class="fa fa-paper-plane" aria-hidden="true"></i> Chèn vào bài viết </a>
			<a class="btn btn-default" href="{data.link_manage}"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>Quay về danh sách </a>
		</div>
	</div>
	<div class="mid-content">
	<form action="{data.link_action}" method="post" name="myForm" id="myForm"   enctype="multipart/form-data" >
	<ul class="nav nav-tabs" role="tablist">
		<li role="presentation" class="active"><a href="#tab1" aria-controls="tab1" role="tab" data-toggle="tab">Danh sách hình trong Album</a></li>
		<li role="presentation"><a href="#tab2" aria-controls="tab2" role="tab" data-toggle="tab">Upload hình mới vào Album</a></li>

	</ul>
	<div class="tab-content">

		<div role="tabpanel" class="tab-pane active" id="tab1">

				{data.err}

				<div class="block-gallery" style="padding-top: 15px;">

					<div class="div-picture">
						<div  class="note"  style="padding: 5px;" ><b>Lưu ý :</b> Kéo thả để xếp thứ tự hình</div>
						<div  id="list_pic_detail" class="list_pic_old"><div class="div_sort">
								<!-- BEGIN: item_pic -->
								<div class="pic_item" data_count="{row.stt}">
									<img src="{row.src}" width="200" />
									<input name="photo_old[]" type="hidden" value="{row.file_id}"/>
									<a href="javascript:void(0);" class="remove">&nbsp;</a>
								</div>
								<!-- END: item_pic -->

							</div><div class="clear"></div>
						</div>

					</div>
				</div>




		</div>
		<div role="tabpanel" class="tab-pane" id="tab2">

				{data.err}


				<div style="padding:15px 0px;">

					<input type="hidden" name="hlistfile" id="hlistfile" value="" />
					<div id="photo">
						<p>You browser doesn't have Flash, Silverlight, Gears, BrowserPlus or HTML5 support.</p>
					</div>


					<br />

					<div class="square note">
						<ul>
							<li>Mỗi lần upload tối đa <b>50 file</b>. Giữ phím <b>Ctrl</b> để chọn thêm ảnh, phím <b>Shift</b> để chọn cùng lúc nhiều file.</li>
							<li>Dung lượng mỗi file tối đa <b>{data.max_upload}</b></li>
						</ul>
						<div class="clear"></div>
					</div>

					<div id="bugcallbacks" style="display: none "></div>
				</div>


		</div>
	</div>

	<div class="div-button">
		<input type="hidden" name="do_submit" id="do_submit" value="1" />
		<button type="submit" class="btn btn-primary" value="Cập nhật"><span>Cập nhật Gallery</span></button>
	</div>

	</form>
	</div>


</div>
</div>

</body>
</html>
<!-- END: html_edit_gallery -->


