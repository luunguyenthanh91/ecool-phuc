<!-- BEGIN: edit -->
<script language="javascript" >
  $(document).ready(function() {
    $('#myForm').validate({
      rules: {     
        title: {
          required: true,
          minlength: 3
        }
      },
      messages: {   
        title: {
          required: "{LANG.err_text_required}",
          minlength: "{LANG.err_length} 3 {LANG.char}" 
        } 
      }
    });
  });
</script>
{data.err}
<form action="{data.link_action}" method="post" enctype="multipart/form-data" name="myForm"  id="myForm" class="validate">
  <table width="100%"  border="0" cellspacing="2" cellpadding="2" align=center class="admintable"> 
    <tr class="form-required">
      <td align="right" width="20%" class="row1">Name: </td>
      <td align="left" class="row0">
        <input name="title" type="text" id="title" size="70" maxlength="250" value="{data.title}" >
      </td>
    </tr>
    <tr>
      <td align="right" class="row1">Short name: </td>
      <td align="left" class="row0">
        <input name="short" type="text" id="short" size="70" maxlength="250" value="{data.short}">
      </td>
    </tr>
    <tr>
      <td align="right" class="row1">Nội dung : </td>
      <td align="left" class="row0">
        
        {data.html_content}
      </td>
    </tr>
    <tr class="form-required">
      <td align="right" width="20%" class="row1">Link khác: </td>
      <td align="left" class="row0">
        <input name="link" type="text" id="link" size="70" maxlength="250" value="{data.link}" >
      </td>
    </tr>
    <tr>
           <td align="right" class="row1">Tin nổi bật: </td>
            <td class="row0">{data.list_is_focus}</td>
          </tr>
    <tr>
      <td class="row1" align="right" >{LANG.picture}: </td>
      <td class="row0">
        <div id="ext_picture" class="picture">{data.pic}</div>
        <input type="hidden" name="picture" id="picture" value="{data.picture}" />
        <div id="btnU_picture" class="div_upload" {data.style_upload} >
          <div class="button2">
            <div class="image"><a title="Add an Image" class="thickbox" id="add_image" href="{data.link_upload}" >Chọn hình</a></div>
          </div>
        </div>
      </td>
    </tr>
    
    <tr class="row_title">
      <td colspan="2" class="font_title" >Search Engine Optimization : </td>
    </tr> 
    <tr>
      <td class="row1" valign="top" style="padding-top:10px;">Friendly URL :</td>
      <td class="row0"><input name="friendly_url" id="friendly_url" type="text" size="50" maxlength="250" value="{data.friendly_url}" class="textfield"><br><span class="font_err">({LANG.mess_friendly_url})</span></td>
    </tr>
    <tr>
      <td class="row1" valign="top" >Friendly Title :</td>
      <td class="row0">
        <input name="friendly_title" id="friendly_title" type="text" size="60" maxlength="250" value="{data.friendly_title}" class="textfield">
      </td>
    </tr>
    <tr>
      <td class="row1">Meta Keyword :</td>
      <td class="row0"><input name="metakey" id="metakey" type="text" size="60" maxlength="250" value="{data.metakey}" class="textfield"></td>
    </tr>
    <tr>
      <td class="row1">Meta Description : </td>
      <td class="row0"><textarea name="metadesc" id="metadesc" rows="3" cols="50" style="width:90%" class="textarea">{data.metadesc}</textarea></td>
    </tr>
    <tr>
      <td class="row1">&nbsp;</td>
      <td class="row0">
        <input type="hidden" name="do_submit" value="1" />
        <input type="submit" name="btnSubmit" value="Submit" class="button">
        <input type="reset" name="Submit2" value="Reset" class="button">            
      </td>
    </tr>
     
  </table>
</form><br>
<!-- END: edit -->

<!-- BEGIN: manage -->
<form action="{data.link_fsearch}" method="post" name="fSearch">
  <table width="100%" border="0" cellspacing="2" cellpadding="2" align="center" class="tableborder">
    <tr>
      <td align="left"><strong>{LANG.search}  :</strong> &nbsp;&nbsp;&nbsp; </td>
      <td align="left">{data.list_search} &nbsp;&nbsp;<strong>{LANG.keyword}: </strong> &nbsp;
        <input name="keyword"  value="{data.keyword}"type="text" size="20">
        <input name="btnSearch" type="submit" value=" Search " class="button">
      </td>
    </tr>
    <tr>
      <td width="15%" align="left" class="row1"><strong>{LANG.totals}:</strong> &nbsp;</td>
      <td width="85%" align="left" class="row0"><b class="font_err">{data.totals}</b></td>
    </tr>
  </table>
</form>
{data.err}<br />
{data.table_list}<br />
<table width="100%" border="0" align="center" cellspacing="1" cellpadding="1" class="bg_tab">
  <tr>
    <td height="25">{data.nav}</td>
  </tr>
</table>
<!-- END: manage -->