<?php
/*================================================================================*\
|| 							Name code : funtions_music.php 		 			      	         		  # ||
||  				Copyright © 2007 by Thai Son - CMS vnTRUST                          # ||
\*================================================================================*/
/**
 * @version : 1.0
 * @date upgrade : 11/12/2007 by Thai Son
 **/
/*=======================CHANGE LOG================================================
DATE 14/12/2007 :
	- function getToolbar 	them 2 var mod va act de xac dinh ( la`m bieng edit wa ), 
													khong su dung lang (hktrung)
													
	- define MOD_DIR_UPLOAD		thu muc upload cua module ( hktrung )
==================================================================================*/
if (!defined('IN_vnT')) {
  die('Hacking attempt!');
}
define('MOD_DIR_UPLOAD', '../vnt_upload/collection/');
define('ROOT_UPLOAD', 'vnt_upload/collection/');
define('MOD_ROOT_URL', $conf['rooturl'] . 'modules/collection/');


class Model
{
  function __construct(){
    $lang = ($_GET['lang']) ? $_GET['lang'] : "vn";
    $this->loadSetting($lang);
  }
  function loadSetting($lang = "vn"){
    global $vnT, $func, $DB, $conf;
    $setting = array();
    $result = $DB->query("SELECT * FROM collection_setting WHERE lang='$lang' ");
    $setting = $DB->fetch_row($result);
    foreach ($setting as $k => $v) {
      $vnT->setting[$k] = stripslashes($v);
    }
    $vnT->setting['arr_category'] = array();
    $res_cat= $DB->query("SELECT n.* , nd.cat_name , nd.friendly_url
            							FROM collection_category n, collection_category_desc nd 
            							WHERE n.cat_id=nd.cat_id AND display=1 AND nd.lang='$lang'  
            							ORDER BY cat_order DESC , n.cat_id ASC ");
    while ($row_cat = $DB->fetch_row($res_cat)) {
      $vnT->setting['arr_category'][$row_cat['cat_id']] = $row_cat;
    }
    unset($setting);
  }
  function Get_Cat($did = -1, $lang, $ext = ""){
    global $func, $DB, $conf;
    $text = "<select size=1 id=\"cat_id\" name=\"cat_id\" class='form-control'  {$ext} >";
    $text .= "<option value=\"\">-- Root --</option>";
    $query= $DB->query("SELECT n.*, nd.cat_name FROM collection_category n,collection_category_desc nd 
                        WHERE n.cat_id=nd.cat_id AND nd.lang='$lang' AND n.parentid=0 
                        ORDER BY cat_order ASC ,n.cat_id DESC");
    while ($cat = $DB->fetch_row($query)) {
      $cat_name = $func->HTML($cat['cat_name']);

      $selected = ($cat['cat_id'] == $did) ? " selected" : "";
      $text .= "<option value=\"{$cat['cat_id']}\" " . $selected . ">{$cat_name}</option>";
      $n = 1;
      $text .= $this->Get_Sub($did, $cat['cat_id'], $n, $lang);
    }
    $text .= "</select>";
    return $text;
  }
  function Get_Sub($did, $cid, $n, $lang){
    global $func, $DB, $conf;
    $output = "";
    $k = $n;
    $query= $DB->query("SELECT n.*, nd.cat_name FROM collection_category n,collection_category_desc nd 
                        WHERE n.cat_id=nd.cat_id AND nd.lang='$lang' AND n.parentid={$cid}
                        ORDER BY cat_order ASC ,n.cat_id DESC");
    while ($cat = $DB->fetch_row($query)) {
      $cat_name = $func->HTML($cat['cat_name']);
      $selected = ($cat['cat_id'] == $did) ? " selected" : "";
      $output .= "<option value=\"{$cat['cat_id']}\" " . $selected . ">";
      for ($i = 0; $i < $k; $i++)
        $output .= "|-- ";
      $output .= "{$cat_name}</option>";

      $n = $k + 1;
      $output .= $this->Get_Sub($did, $cat['cat_id'], $n, $lang);
    }
    return $output;
  }
  function List_Cat($did="",$lang="vn"){
    global $func,$DB,$conf,$vnT;
    if ($did)
      $arr_selected = explode(",",$did);
    else{
      $arr_selected = array('0',);
    }
    $text= "<select name=\"list_cat[]\" id=\"list_cat\"  multiple >";
    $sql="SELECT n.*,nd.cat_name FROM collection_category n, collection_category_desc nd
  				WHERE n.cat_id=nd.cat_id AND nd.lang='$lang' AND n.parentid=0 
  				ORDER BY cat_order ASC, n.cat_id DESC ";
    $result = $DB->query ($sql);
    while ($row = $DB->fetch_row($result)){
      $cat_name = $func->HTML($row['cat_name']);
      $selected="";
      if (in_array($row['cat_id'],$arr_selected)){
        $selected = "selected";
      }
      $text .= "<option value=\"{$row['cat_id']}\" {$selected} >".$cat_name."</option>";
      $n=1;
      $text.= $this->List_Sub($row['cat_id'],$n,$did,$lang);
    }
    $text.="</select>";
    return $text;
  }
  function List_Sub($cid,$n,$did="",$lang){
    global $func,$DB,$conf,$vnT;
    if ($did)
      $arr_selected = explode(",",$did);
    else{
      $arr_selected = array('0',);
    }
    $output="";
    $k=$n;
    $query= $DB->query("SELECT n.*,nd.cat_name FROM collection_category n, collection_category_desc nd
                				where n.cat_id=nd.cat_id AND nd.lang='$lang' AND parentid={$cid} 
                				ORDER BY cat_order ASC , n.cat_id DESC");
    while ($cat=$DB->fetch_row($query)){
      $cat_name = $func->HTML($cat['cat_name']);
      $selected="";
      if (in_array($cat['cat_id'],$arr_selected)){
        $selected = "selected";
      }
      $output.="<option value=\"{$cat['cat_id']}\" {$selected} >";
      for ($i=0;$i<$k;$i++) $output.= "|-- ";
      $output.="{$cat_name}</option>";

      $n=$k+1;
      $output.= $this->List_Sub($cat['cat_id'],$n,$did,$lang);
    }
    return $output;
  }
  function List_SubCat($cat_id){
    global $func, $DB, $conf;
    $output = "";
    $query = $DB->query("SELECT * FROM collection_category WHERE parentid={$cat_id}");
    while ($cat = $DB->fetch_row($query)) {
      $output .= $cat["cat_id"] . ",";
      $output .= $this->List_SubCat($cat['cat_id']);
    }
    return $output;
  }
  function get_cat_name($cat_id, $lang){
    global $vnT, $func, $DB, $conf;
    $result = $DB->query("SELECT cat_name FROM collection_category_desc WHERE cat_id=$cat_id and lang='$lang'");
    if ($row = $DB->fetch_row($result)) {
      $cat_name = $func->HTML($row['cat_name']);
    }
    return $cat_name;
  }
  function get_catCode($parentid, $cat_id){
    global $vnT, $func, $DB, $conf;
    $text = "";
    $sql = "SELECT cat_id,cat_code FROM collection_category WHERE cat_id =$parentid ";
    $result = $DB->query($sql);
    if ($row = $DB->fetch_row($result)) {
      $text = $row['cat_code'] . "_" . $cat_id;
    } else
      $text = $cat_id;
    return $text;
  }
  function create_maso($cat_id, $p_id) {
    global $vnT, $func, $DB, $conf;
    $text = "";
    $sql = "SELECT name FROM collection_category WHERE cat_id='{$cat_id}' ";
    $result = $DB->query($sql);
    if ($row = $DB->fetch_row($result)) {
      $text = ($row['name']) ? $row['name'] . "-" . $p_id : $p_id;
    } else {
      $text = $p_id;
    }
    return $text;
  }
  function get_cat_list($cat_id){
    global $vnT, $func, $DB, $conf;
    $arr_cat = array();
    $res = $DB->query("SELECT cat_id,cat_code FROM collection_category where cat_id in (" . $cat_id . ") ");
    while ($row = $DB->fetch_row($res)) {
      $tmp = @explode("_", $row['cat_code']);
      $arr_cat = array_merge($arr_cat, $tmp);
    }
    $arr_cat = array_unique($arr_cat);
    $out = @implode(",", $arr_cat);
    return $out;
  }
  function rebuild_cat_list($cat_id){
    global $vnT, $func, $DB, $conf;
    $arr_cat = array();
    $res = $DB->query("SELECT p_id,cat_id FROM collection WHERE  FIND_IN_SET('$cat_id',cat_list)<>0 ");
    while ($row = $DB->fetch_row($res)) {
      $DB->query("UPDATE collection SET cat_list='".$this->get_cat_list($row['cat_id'])."' WHERE p_id=" . $row['p_id']);
    }
    return false;
  }
  function rebuild_cat_code($parentid, $cat_id){
    global $vnT, $func, $DB, $conf;
    $cat_code = $this->get_catCode($parentid, $cat_id);
    $DB->query("UPDATE collection_category SET cat_code='" . $cat_code . "' WHERE cat_id=" . $cat_id);
    $res_chid = $DB->query("SELECT cat_id FROM collection_category WHERE  parentid=" . $cat_id);
    while ($row_chid = $DB->fetch_row($res_chid)) {
      $this->rebuild_cat_code($cat_id, $row_chid['cat_id']);
    }
    return false;
  }
  function get_price_format($price, $unit = " đ", $default = "Liên hệ"){
    global $vnT;
    if ($price) {
      $price = $vnT->func->format_number($price, '.');
      if ($unit) {
        $price .= " " . $unit;
      }
    } else {
      $price = $default;
    }
    return $price;
  }
  function get_picture($picture, $w = ""){
    global $vnT, $func, $DB, $conf;
    $out = "";
    $ext = "";
    $w_thumb = ($vnT->setting['imgthumb_width']) ? $vnT->setting['imgthumb_width'] : 100;
    if ($picture) {
      $linkhinh = "../vnt_upload/collection/" . $picture;
      $linkhinh = str_replace("//", "/", $linkhinh);
      $dir = substr($linkhinh, 0, strrpos($linkhinh, "/"));
      $pic_name = substr($linkhinh, strrpos($linkhinh, "/") + 1);
      $src = $dir . "/thumbs/" . $pic_name;
    }
    if ($w < $w_thumb) $ext = " width='$w' ";
    $out = "<img  src=\"{$src}\" {$ext} >";
    return $out;
  }
  function get_pic_input($p_id, $dir, $w = 1000, $w_thumb = 150){
    global $func, $DB, $conf, $vnT;
    $arr_out = array();
    $stt = 1;
    if ($p_id) {
      $arr_pic_old = $_POST['picture_old'];
      $arr_old_id = array();
      if (is_array($arr_pic_old)) {
        foreach ($arr_pic_old as $pic_old) {
          $tmp = @explode("|", $pic_old);
          $pic_id = trim($tmp[0]);
          $pic_src = trim($tmp[1]);
          $pic_name = $_POST['pic_name_old'][$pic_id];
          $pic_order = $_POST['pic_order_old'][$pic_id];
          $dup = array();
          $dup['pic_name'] = $pic_name;
          $dup['pic_order'] = $pic_order;
          $vnT->DB->do_update("collection_picture", $dup, "p_id=" . $p_id . " AND id=" . $pic_id);
          $stt++;
          $arr_old_id[] = $pic_id;
          $arr_out[$pic_order]['picture'] = $pic_src;
          $arr_out[$pic_order]['pic_name'] = $pic_name;
          $arr_out[$pic_order]['pic_order'] = $pic_order;
        }
        $vnT->DB->query("DELETE FROM collection_picture 
                        WHERE p_id=".$p_id." AND id not in(".@implode(",", $arr_old_id) . ")");
      }else{
        $vnT->DB->query("DELETE FROM collection_picture WHERE p_id=".$p_id);
      }
    }
    $arr_pic = $_POST['pictures'];
    if (is_array($arr_pic)) {
      foreach ($arr_pic as $k => $picture) {
        $pic_name = $_POST['pic_name'][$k];
        $pic_order = $_POST['pic_order'][$k];
        if ($p_id) {
          $cot = array();
          $cot['p_id'] = $p_id;
          $cot['picture'] = $picture;
          $cot['pic_name'] = $pic_name;
          $cot['pic_order'] = $pic_order;
          $vnT->DB->do_insert("collection_picture", $cot);
        }
        $arr_out[$pic_order]['picture'] = $picture;
        $arr_out[$pic_order]['pic_name'] = $pic_name;
        $arr_out[$pic_order]['pic_order'] = $pic_order;
      }
    }
    return $arr_out;
  }
  function List_Search($did, $ext = ""){
    global $func, $DB, $conf, $vnT;
    $arr_where = array('p_id' => $vnT->lang['id'], 'maso' => $vnT->lang['maso'], 'p_name' => $vnT->lang['collection_name'], 'price' => $vnT->lang['price'], 'date_post' => $vnT->lang['date_post'] . " (d/m/Y)", 'description' => $vnT->lang['description']);
    $text = "<select size=1 name=\"search\" id='search' class='form-control' {$ext} >";
    foreach ($arr_where as $key => $value) {
      $selected = ($did == $key) ? "selected" : "";
      $text .= "<option value=\"{$key}\" {$selected} > {$value} </option>";
    }
    $text .= "</select>";
    return $text;
  }
  function List_Status_Pro($selname, $did, $lang = "vn", $ext = ""){
    global $func, $DB, $conf;
    $text = "<select name=\"{$selname}\"  class='form-control' {$ext}   >";
    $text .= "<option value=\"0\" selected>-- Chọn trạng thái --</option>";
    $sql = "SELECT n.*, nd.title FROM collection_status n, collection_status_desc nd 
    				where n.status_id=nd.status_id AND nd.lang='$lang' AND n.display=1 
    				ORDER BY n.s_order ";
    $result = $DB->query($sql);
    while ($row = $DB->fetch_row($result)) {
      if ($row['status_id'] == $did) {
        $text .= "<option value=\"{$row['status_id']}\" selected>" . $func->HTML($row['title']) . "</option>";
      } else {
        $text .= "<option value=\"{$row['status_id']}\">" . $func->HTML($row['title']) . "</option>";
      }
    }
    $text .= "</select>";
    return $text;
  }
  function List_Status_Muti($did="",$lang="vn",$ext=""){
    global $func,$DB,$conf;
    if ($did)
      $arr_selected = explode(",",$did);
    else{
      $arr_selected = array();
    }
    $sql="SELECT n.*, nd.title FROM collection_status n, collection_status_desc nd 
  				WHERE n.status_id=nd.status_id AND nd.lang='$lang' AND n.display=1 
  				ORDER BY n.s_order ASC ";
    $result = $DB->query ($sql);
    $text='<div id="list-status" class="list-choose"><ul>';
    while ($row = $DB->fetch_row($result)){
      $id = $row['status_id'];
      $title = $func->HTML($row['title']);
      $checked = in_array($id, $arr_selected) ? "class='checked'" : "";
      $text .= "<li ".$checked." rel='".$id."' ><span>".$title."</span></li>";
    }
    $text .= "</ul>";
    $text .= '<input class="input-value" type="hidden" name="status" id="status" value="'.$did.'" />';
    $text .= "</div>";
    return $text;
  }
  function List_Display($did = 0, $ext = ""){
    global $func, $vnT, $conf;
    $text = "<select size=1 name=\"display\" id=\"display\" class='form-control' {$ext}'>";
    $text .= "<option value=\"-1\" > -- Tất cả -- </option>";
    if ($did == "0")
      $text .= "<option value=\"0\" selected> " . $vnT->lang['display_no'] . " </option>";
    else
      $text .= "<option value=\"0\" > " . $vnT->lang['display_no'] . " </option>";

    if ($did == "1")
      $text .= "<option value=\"1\" selected> " . $vnT->lang['display_yes'] . " </option>";
    else
      $text .= "<option value=\"1\"> " . $vnT->lang['display_yes'] . " </option>";
    $text .= "</select>";
    return $text;
  }
  function do_DeleteSub($ids, $lang = "vn"){
    global $func, $DB, $conf, $vnT;
    $res = $DB->query("SELECT cat_id,parentid FROM collection_category WHERE parentid in (" . $ids . ")");
    while ($row = $DB->fetch_row($res)) {
      $this->do_DeleteSub($row['cat_id'], $lang);
      $DB->query("UPDATE collection_category_desc SET display=-1 
                  WHERE cat_id = ".$row['cat_id']." AND lang='".$lang."' ");
      //insert RecycleBin
      $rb_log['module'] = "collection";
      $rb_log['action'] = "category";
      $rb_log['tbl_data'] = "collection_category_desc";
      $rb_log['name_id'] = "cat_id";
      $rb_log['item_id'] = $row['cat_id'];
      $rb_log['lang'] = $lang;
      $func->insertRecycleBin($rb_log);
    }
  }
  function get_list_attr($attr_id, $lang = 'vn'){
    global $input,$conf,$vnT,$DB,$func;   
    $arr_select = explode(",",$attr_id);
    $list_attr_check='';  
    $sql = "SELECT * FROM collection_attr p, collection_attr_desc pd
            WHERE p.attr_id = pd.attr_id AND lang = '{$lang}'
            ORDER BY display_order ASC,p.attr_id ASC";
    $result = $DB->query($sql);
    if($num = $DB->num_rows($result)){
      $i=0;$ngang=0;
      $list_attr_check .= '<div class="box_list_color"><table width="100%" border="0" cellspacing="0" cellpadding="0" ><tr>';
      while ($row = $DB->fetch_row($result)){
        $i++;
        $attr_name = $func->HTML($row['title']);
        $attr_id = $row['attr_id'];
        if (@in_array($row['attr_id'],$arr_select)){
          $checked ="checked";  
        }else{
          $checked =""; 
        }
        if($ngang==4){
          $list_attr_check .= '</tr><tr>';
          $ngang=0;
        }
        $list_attr_check .= "<td class='row2'><input {$checked} align=\"absmiddle\" type=\"checkbox\" name=\"list_attr[]\" id=\"list_attr\" value=\"{$attr_id}\"  align='absmiddle' />&nbsp;<span class='text_chose'>{$attr_name}</span></td>";
        $ngang++;
      }
      if ($ngang<5) $list_attr_check.='<td>&nbsp;</td>';
      $list_attr_check.='</tr></table></div>';
    }
    return $list_attr_check;
  }
  function List_Stock($id){
    global $func,$DB,$conf,$vnT;
    $text = "<select name=\"in_stock\" id=\"in_stock\">";
    if($id==1){
      $text .= '<option value="1" selected>Còn hàng</option>';
      $text .= '<option value="0">Hết hàng</option>';
    }else{
      $text .= '<option value="1">Còn hàng</option>';
      $text .= '<option value="0" selected>Hết hàng</option>';
    }
    $text.="</select>";
    return $text;
  }
  function get_p_name($p_id,$lang){
    global $vnT,$DB;
    $result = $DB->query("SELECT p_name FROM collection_desc WHERE p_id = {$p_id} AND lang='$lang'");
    if($row = $DB->fetch_row($result))
      return $row['p_name'];
    else
      return '';
  }
  function Status_Booking($did){
    global $vnT,$DB;
    $arr[1] = 'Cuộc hẹn mới';
    $arr[2] = 'Đã liên lạc lại';
    $arr[3] = 'Đã tư vấn';
    $text .= '<select name="status" id="status">';
    foreach ($arr as $key => $value) {
      $selected = ($key == $did) ? 'selected' : '';
      $text .= '<option value="'.$key.'">'.$value.'</option>';
    }
    $text .= '</select>';
    return $text;
  }
}
$model = new Model();
?>