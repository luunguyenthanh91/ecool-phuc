<?php
/*================================================================================*\
|| 							Name code : funtions_contact.php 		 			          	     		  # ||
||  				Copyright © 2007 by Thai Son - CMS vnTRUST                          # ||
\*================================================================================*/
/**
 * @version : 1.0
 * @date upgrade : 11/12/2007 by Thai Son
 **/
if (! defined('IN_vnT')) {
  die('Hacking attempt!');
}
define('DIR_IMAGE_MOD', 'modules/contact_ad/images');

function getToolbar ($act = "contact", $lang = "vn")
{
  global $func, $DB, $conf, $vnT;
  $menu = array(
    "add" => array(
      'icon' => "i_add" , 
      'title' => "Add" , 
      'link' => "?mod=contact&act=$act&sub=add&lang=$lang") , 
    "edit" => array(
      'icon' => "i_edit" , 
      'title' => "Edit" , 
      'link' => "javascript:alert('" . $vnT->lang['action_no_active'] . "')") , 
    "manage" => array(
      'icon' => "i_manage" , 
      'title' => "Manage" , 
      'link' => "?mod=contact&act=$act&lang=$lang") , 
    "help" => array(
      'icon' => "i_help" , 
      'title' => "Help" , 
      'link' => "'help/index.php?mod=contact&act=$act','AdminCPHelp',1000, 600, 'yes','center'" , 
      'newwin' => 1));
  return $func->getMenu($menu);
}

function getToolbar_Small ($act = "contact", $lang = "vn")
{
  global $func, $DB, $conf, $vnT;
  $menu = array(
    "manage" => array(
      'icon' => "i_manage" , 
      'title' => "Manage" , 
      'link' => "?mod=contact&act=$act") , 
    "help" => array(
      'icon' => "i_help" , 
      'title' => "Help" , 
      'link' => "'help/index.php?mod=contact&act=$act','AdminCPHelp',1000, 600, 'yes','center'" , 
      'newwin' => 1));
  return $func->getMenu($menu);
}

function List_Status ($did, $ext = "")
{
  global $func, $DB, $conf;
  $output = "<select name=status class='select' {$ext} >";
  if ($did == "0")
    $output .= " <option value=\"0\" selected> Chưa gọi lại (Mới) </option>";
  else
    $output .= " <option value=\"0\" > Chưa gọi lại (Mới) </option>";
  if ($did == "1")
    $output .= " <option value=\"1\" selected> Đã gọi lại </option>";
  else
    $output .= " <option value=\"1\" > Đã gọi lại </option>";
  $output .= "</select>";
  return $output;
}

function  get_cat_name ($table,$cat_id,$lang="vn")
{
	global $func, $DB, $conf ,$vnT;
	$text = '';
	$result = $DB->query("SELECT cat_name FROM {$table}_category_desc WHERE cat_id=$cat_id AND lang='$lang' ");
	if($row = $DB->fetch_row($result))
	{
		$text = $row['cat_name'];
	}
	return $text ;
}


function  get_p_name ($p_id,$lang="vn")
{
	global $func, $DB, $conf ,$vnT;
	$text = '';
	$result = $DB->query("SELECT p_name FROM products_desc WHERE p_id=$p_id AND lang='$lang' ");
	if($row = $DB->fetch_row($result))
	{
		$text = $row['p_name'];
	}
	return $text ;
}


function  get_service_name ($sid,$lang="vn")
{
	global $func, $DB, $conf ,$vnT;
	$text = '';
	$result = $DB->query("SELECT title FROM service_desc WHERE service_id=$sid AND lang='$lang' ");
	if($row = $DB->fetch_row($result))
	{
		$text = $row['title'];
	}
	return $text ;
}

?>