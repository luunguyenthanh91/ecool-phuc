<!-- BEGIN: edit -->
<style>
#tabnav, ul.tabnav {
	height: 23px;
	padding-left: 10px;
	background: url(modules/contact_ad/images/tab_bottom.gif) repeat-x bottom;
	margin: 0;
}

#tabnav li, ul.tabnav li {
	display: inline;
	list-style-type: none;
	margin: 0;
	padding: 0;
}

#tabnav a:link, #tabnav a:visited, ul.tabnav a:link, ul.tabnav a:visited {
	float: left;
	font-size: 10px;
	line-height: 14px;
	font-weight: 700;
	margin-right: 4px;
	border: 1px solid #ccc;
	text-decoration: none;
	color: #666;
	background: url(modules/contact_ad/images/tab_bg.gif) repeat-x;
	padding: 2px 10px;
}

#tabnav a:link.active, #tabnav a:visited.active, ul.tabnav a:link.active, ul.tabnav a:visited.active {
	border-bottom: 1px solid #fff;
	background: #fff;
	color: #000;
}

#tabnav a:hover, ul.tabnav a:hover {
	background: #fff;
}
.tabContent { padding-top:10px;}
</style>
<script language="javascript" >
$(document).ready(function() {
	$('#myForm').validate({
		rules: {			
				title: {
					required: true,
					minlength: 3
				}
	    },
	    messages: { 	
				
				title: {
						required: "{LANG.err_text_required}",
						minlength: "{LANG.err_length} 3 {LANG.char}" 
				} 
		}
	});
});
</script>

{data.err}
<form action="{data.link_action}" method="post" enctype="multipart/form-data" name="myForm" id="myForm"  class="validate">
<table width="100%"  border="0"  cellspacing="1" cellpadding="1" class="admintable">
	
 
    <tr class="form-required">
			<td class="row1" width="20%" >Title : </td>
			<td  align="left" class="row0"><input name="title" id="title" type="text" size="70" maxlength="250" value="{data.title}"></td>
		</tr>
    
    <tr >
			<td class="row1" >{LANG.company} : </td>
			<td  align="left" class="row0"><input name="company" id="company" type="text" size="70" maxlength="250" value="{data.company}"></td>
		</tr>
    <tr >
			<td class="row1" >{LANG.address} : </td>
			<td  align="left" class="row0"><input name="address" id="address" type="text" size="70" maxlength="250" value="{data.address}"></td>
		</tr>
    <tr >
			<td class="row1" >{LANG.phone} : </td>
			<td  align="left" class="row0"><input name="phone" id="phone" type="text" size="70" maxlength="250" value="{data.phone}"></td>
		</tr>
    <tr >
			<td class="row1" >Fax : </td>
			<td  align="left" class="row0"><input name="fax" id="fax" type="text" size="70" maxlength="250" value="{data.fax}"></td>
		</tr>
    <tr >
			<td class="row1" >Email: </td>
			<td  align="left" class="row0"><input name="email" id="email" type="text" size="70" maxlength="250" value="{data.email}"></td>
		</tr>
    <tr >
			<td class="row1" >Website: </td>
			<td  align="left" class="row0"><input name="website" id="website" type="text" size="70" maxlength="250" value="{data.website}"></td>
		</tr>
    <tr >
     <td class="row1">QR CODE: </td>
     <td class="row0">
     	{data.qrcode}
    </td>
    </tr>
		<tr>
			<td colspan=2  align="center" class="row1"><p><strong>{LANG.content_extra} :</strong></p>
			{data.html_content}
			</td>
		</tr>
		  <tr class="row_title" >
     <td  colspan="2" class="font_title" >Bản đồ : </td>
    </tr> 
    <tr  >
     <td  class="row0" colspan="2"  align="center">
     
     <input id="currentTab" name="currentTab" value="{data.CurrentTab}" type="hidden" />
     
     <ul id="tabnav">
				<li><a onclick="ShowTab(1)" id="tab1" class="active" href="javascript:void(0);"  ><input type="radio" name="map_type" id="map_type1" value="1" align="absmiddle" /> Google Maps</a></li>
				<li><a onclick="ShowTab(2)" id="tab2" href="javascript:void(0);" class=""><input type="radio" name="map_type" id="map_type2" value="2" align="absmiddle" /> Images Maps</a></li> 
        <li><a onclick="ShowTab(0)" id="tab0" href="javascript:void(0);" class=""><input type="radio" name="map_type" id="map_type0" value="0" align="absmiddle" /> None</a></li> 
        
			</ul>	
      
      
      <div  class="tabContent" id="div1" style="display:none" >
        <div id="googleMaps">
        	
          <script type="text/javascript" src="http://maps.google.com/maps/api/js?key={CONF.GoogleMapsAPIKey}&language=vi"></script>
          <script language="javascript" >          	 
    				var defaultPosition = new google.maps.LatLng({data.map_lat},{data.map_lng});		
						var information	= '{data.map_information}';
						var map_lng  = '{data.map_lat}';
						var map_lat = '{data.map_lng}';
          </script>
          <script language="javascript" src="{DIR_JS}/google/googlemap.js" ></script>
           
          
         
          
          <table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td><strong>Bước 1 :</strong> Điền thông tin địa chỉ của công ty vào ô địa chỉ => Click nút <strong>"Cập nhật"</strong>.<br>
     
     <table  border="0" cellspacing="2" cellpadding="2" align="center">
  <tr>
    <td><strong>Nhập địa chỉ : </strong></td>
    <td><input type="text" size="60" id="map_address" name="map_address"  value="{data.map_address}" style="height:25px; font-size:18px; color:#B84120; border:1px solid #ff0000;"  /> </td>
    <td><input type="button" value="Cập nhật" onclick="onChangeAddress()" style="height:25px; font-size:14px; color:#ff0000; "  /></td>
  </tr>
</table> 	
 </td>
  </tr>
  <tr>
    <td>
    
    <strong>Bước 2 :</strong> Kéo nút định vị <img src="http://mt.googleapis.com/vt/icon/name=icons/spotlight/spotlight-poi.png&scale=1" align="absmiddle" width="16" /> đến đúng vị trí công ty trên bản đồ , điền nội dung cần hiển thị trong khung <strong>"Thông tin vị trí"</strong> => Click nút <strong>"Lưu vị trí"</strong>.<br> 
    
     <div id="map" style="width:95%; height:500px;" ></div>
     
 <div id="tt"></div> 
          
           <input type="hidden" id="map_lat" name="map_lat" value="{data.map_lat}" />
          <input type="hidden" id="map_lng" name="map_lng" value="{data.map_lng}" />
          <input type="hidden" id="map_information" name="map_information" value="{data.map_desc}" />	 
      
         
        
    </td>
     
  </tr> 
</table>
</div>
           

    
   
              
      </div>
      
      
      <div class="tabContent"  id="div2" style="display:none" >
        <div id="uploadMaps">
        	
          {data.img_maps}
          <table width="80%" border="0" cellspacing="0" cellpadding="0">
      <tr>
         <td style="padding-right:10px;" width="70" align="right" ><strong>Picture : </strong></td>
         <td style="padding-right:10px;" ><input name="map_picture" id="map_picture" type="text"  size="50" maxlength="250" value="{data.map_picture}" style="width:98%"></td>
        <td  > <div class="button2"><div class="image"><a title="Add an Image" class="thickbox" id="add_image" href="?mod=media&act=popup_media&type=textbox&folder=Images&obj=map_picture&TB_iframe=true&width=640&height=474" >Image</a></div></div></td>
      </tr>
    </table>
        
        </div>
      </div>
      
      <div class="tabContent"  id="div0" style="display:none" >
         &nbsp;
      </div>
       
      
 <script type="text/javascript">
 		var currentTab = "{data.CurrentTab}" ;
		function ShowTab(T) {
			i = 0;
			while (document.getElementById("tab" + i) != null) {
				$("#div"+i).hide();
				$("#tab"+i).removeClass("active") ;
				 
			
				i++;
			}
	
			$("#div"+T).show();
			$("#tab"+T).addClass('active'); 
		
			$("#map_type"+T).attr('checked',true);
			$("#currentTab").val(T);
						
			if(T==1 && currentTab!=1) {				
				window.frames['ifrMpas'].location.reload(true); 
			}
			return false ;
		} 
		 

	$(document).ready(function() {

		ShowTab(currentTab);  
		
	});

	</script>
     </td>
    </tr>

		<tr align="center">
    <td class="row1" ><strong>Bước 3 :</strong> Click nút <strong>"Submit"</strong> </td>
			<td class="row0" >
				<input type="hidden" name="do_submit"	 value="1" />
				<input type="submit" name="btnSubmit" value="Submit" class="button">
				<input type="reset" name="btnReset" value="Reset" class="button">
			</td>
		</tr>
	</table>
</form>
<br>
<!-- END: edit -->

<!-- BEGIN: manage -->
<form action="{data.link_fsearch}" method="post" name="myform">
<table width="100%" border="0" cellspacing="2" cellpadding="2" align="center" class="tableborder">
  <tr>
    <td width="15%" align="left">{LANG.totals}: &nbsp;</td>
    <td width="85%" align="left"><b class="font_err">{data.totals}</b></td>
  </tr>
  </table>
</form>
{data.err}
<br />
{data.table_list}
<br />
<table width="100%"  border="0" align="center" cellspacing="1" cellpadding="1" class="bg_tab">
  <tr>
    <td  height="25">{data.nav}</td>
  </tr>
</table>
<br />
<!-- END: manage -->