<!-- BEGIN: manage -->
<form action="{data.link_action}" method="post" name="f_config" id="f_config" >
{data.err}
<table width="100%"  border="0"  cellspacing="1" cellpadding="1" class="admintable">
	<tr class="row_title" >
		<td colspan="2" ><strong class="font_title">Search Engine Optimization (SEO):</strong></td>
	</tr>
   <tr>
      <td class="row1" valign="top" >Friendly Title :</td>
      <td class="row0"><input name="friendly_title" id="friendly_title" type="text" size="70" maxlength="250" value="{data.friendly_title}" class="textfield"></td>
    </tr>
    <tr>
      <td class="row1">Meta Keyword :</td>
      <td class="row0"><input name="metakey" id="metakey" type="text" size="70" maxlength="250" value="{data.metakey}" class="textfield"></td>
    </tr>
    <tr>
      <td class="row1">Meta Description : </td>
      <td class="row0"><textarea name="metadesc" id="metadesc" rows="3" cols="50" style="width:90%" class="textarea">{data.metadesc}</textarea></td>
    </tr>
    <tr>
      <td class="row1">Rebuild Link SEO: </td>
      <td class="row0"><input type="button" name="Rebuild" value=" &nbsp; Rebuild Link &nbsp;" class="button" onclick="location.href='{data.link_rebuild}'" /></td>
    </tr>
</table> 
<br>
<table width="100%"  border="0"  cellspacing="1" cellpadding="1" class="admintable">
	<tr class="row_title" >
		<td colspan="2" ><strong class="font_title">{LANG.setting_module}</strong></td>
	</tr>
 
  
	<tr >
    <td   class="row1" width="30%"> <strong>{LANG.n_list} : </strong></td>
      <td  align="left" class="row0"><input name="n_list" type="text" size="20" maxlength="250" value="{data.n_list}" class="textfiled"></td>
  </tr>
 

  <tr>
    <td class="row1"  >{LANG.nophoto} :</td>
    <td class="row0">{data.nophoto}</td>
  </tr>

 
  <tr>
    <td class="row1"  >{LANG.imgthumb_width} :</td>
    <td class="row0"><input name="imgthumb_width" size="20" type="text" class="textfiled" value="{data.imgthumb_width}" onKeyPress="return is_num(event,'imgthumb_width')"></td>
  </tr>
  
  
  <tr>
    <td  class="row1"  >{LANG.img_width} :</td>
    <td class="row0"><input name="img_width" size="20" type="text" class="textfiled" value="{data.img_width}"  onkeypress="return is_num(event,'img_width')"></td>
  </tr>
   
   
</table>
 
<p align="center">
			<input type="hidden" name="num" value="{data.num}">
      <input type="hidden" name="do_submit" value="1">
      <input type="submit" name="btnEdit" value="Update >>" class="button">
</p>
</form>
<br />
<!-- END: manage -->