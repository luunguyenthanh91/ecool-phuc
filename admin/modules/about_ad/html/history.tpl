
<!-- BEGIN: edit -->
<link href="{DIR_JS}/metabox/seo-metabox.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="{DIR_JS}/metabox/seo-metabox.js"></script>
<script language="javascript" >
var wpseo_lang = 'en';
var wpseo_meta_desc_length = '155';
var wpseo_title = 'title';
var wpseo_content = 'content';
var wpseo_title_template = '%%title%%';
var wpseo_metadesc_template = '';
var wpseo_permalink_template = '{CONF.rooturl}%postname%.html';
var wpseo_keyword_suggest_nonce = 'a7c4d81c79'; 
			
$(document).ready(function() {
	$('#myForm').validate({
		 
		rules: {			
				title: {
					required: true,
					minlength: 3
				},
                parentid: {
                    required: true
                }
	    },
	    messages: {	    	
				title: {
						required: "{LANG.err_text_required}",
						minlength: "{LANG.err_length} 3 {LANG.char}" 
				},
            parentid: {
                required: "Chọn chủ đề !"
            }

		}
	});
	
	{data.js_preview} 
	
});
</script>

{data.err}
<form action="{data.link_action}" method="post" enctype="multipart/form-data" name="myForm" id="myForm"  class="validate">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td valign="top" style="min-width:600px;" >
    
<table width="100%"  border="0"  cellspacing="1" cellpadding="1" class="admintable">

    <tr class="form-required">
        <td class="row1" >Là con của : </td>
        <td align="left" class="row0">{data.list_cat}</td>
    </tr>

		<tr class="form-required">
			<td class="row1" >{LANG.title} : </td>
			<td  align="left" class="row0"><input name="title" id="title" type="text" size="70" maxlength="250" value="{data.title}" onkeyup="vnTMXH.setTitle(this.value)" ></td>
		</tr>
    
		
    <tr >
      <td class="row1" >{LANG.picture}: </td>
      <td class="row0"><div id="ext_picture" class="picture" >{data.pic}</div>
        <input type="hidden" name="picture"	 id="picture" value="{data.picture}" />
        <div id="btnU_picture" class="div_upload" {data.style_upload} >
          <div class="button2">
            <div class="image"><a title="Add an Image" class="thickbox" id="add_image" href="{data.link_upload}" >Chọn hình</a></div>
          </div>
        </div></td>
    </tr>
    
		<tr >
    	<td  class="row0"  colspan="2">
            <div id="tab-container-1">
                <ul id="tab-container-1-nav">
                    <li><a href="#Tab1"><span>Nội dung</span></a></li>

                </ul>
                <div class="clear"></div>
                <div class="tab" id="Tab1">
                    {data.html_content}
                </div>
            </div>

            <script type="text/javascript">
                var tabber1 = new Yetii({
                    id: 'tab-container-1',
                    active: 1
                });
            </script>
        </td>
		</tr>
    <tr>
            <td class="row1">{LANG.display}</td>
            <td align="left" class="row0">{data.list_display}</td>
        </tr>  
        
 </table>
    </td>

  </tr>
  <tr >
    <td class="row0" align="center" height="50">
      <input type="hidden" name="do_submit"	 value="1" />
      <input type="submit" name="btnAdd" value="{LANG.btn_submit}" class="button">&nbsp;
      <input type="reset" name="btnReset" value="{LANG.btn_reset}" class="button">&nbsp;
    </td>
  </tr>
</table>



</form> 
<br>
<!-- END: edit -->

<!-- BEGIN: manage -->
<form action="{data.link_fsearch}" method="post" name="myform">

<table width="100%" border="0" cellspacing="2" cellpadding="2" align="center" class="tableborder">
  <tr>
    <td width="15%" align="left"><span class="edit-class">{LANG.totals}</span>: &nbsp;</td>
    <td width="85%" align="left"><b class="font_err">{data.totals}</b></td>
  </tr>

  <tr>
  <td align="left"><span class="edit-class">{LANG.search_for}</span>  : &nbsp;&nbsp;&nbsp;  </td>
  <td align="left">{data.list_search} &nbsp;&nbsp;{LANG.keyword} : &nbsp;
    <input name="keyword"  value="{data.keyword}"type="text" size="20">
    <input name="btnSearch" type="submit" value=" Search " class="button"></td>
  </tr>
  </table>
</form>
{data.err}
<br />
{data.table_list}
<br />
<table width="100%"  border="0" align="center" cellspacing="1" cellpadding="1" class="bg_tab">
  <tr>
    <td  height="25">{data.nav}</td>
  </tr>
</table>
<br />
<!-- END: manage -->