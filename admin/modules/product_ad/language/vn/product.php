<?php

$lang = array (

//cat_product
'manage_cat_product' => "Quản lý danh mục sản phẩm",
'add_cat_product' => "Thêm danh mục sản phẩm",
'move_cat' => 'Di chuyển sản phẩm của danh mục',
'edit_cat_product' => "Cập nhật danh mục sản phẩm",
'no_have_cat_product' => "Chưa có danh mục nào",
'err_empty_cat' =>"Vui lòng điền tên danh mục",
'cat_name' => "Tên danh mục",
'cat_parent' => "Danh mục cha",
'cat_description' => "Mô tả danh mục ",
'move_success' => "Di chuyển thành công",
'move_item_success' => 'Di chuyển sản phẩm thành công',
'focus'	=>	'Focus',

//product
'manage_product' => "Quản lý sản phẩm",
'add_product' => "Thêm sản phẩm mới",
'edit_product' => "Cập nhật sản phẩm",
'edit_pic' => 'Cập nhật hình sản phẩm',
'no_have_product' => 'Chưa có sản phẩm nào',

'category' => 'Danh mục',
'search' => 'Tìm kiếm',
'id' => 'ID',
'maso' => 'Mã số',
'picture' => 'Hình ảnh',
'date_post' => 'Ngày đăng',
'keyword' => 'Từ khóa',
'order' => 'Thứ tự',
'asc' => 'Tăng dần',
'desc' => 'Giảm dần',
'maso_existed'=> "Mã sản phẩm này đã tồn tại",
'product_name' => 'Tên sản phẩm',
'buocgia' => 'Bước giá',
'add_pic' => 'Thêm hình',
'del_pic' => 'Xóa hình',
'pic_other' => 'Hình SP phụ',
'pro_status' => 'Trạng thái SP',
'price' => 'Giá SP',
'list_cat'	=>	'Các danh mục của sản phẩm',
'list_cat_chose'	=>	'Danh mục sản phẩm được chọn',
'note_maso'	=>	'Nếu không nhập mã số , Hệ thống sẽ tự tạo mã số theo chuẩn',
'select_status'	=>	'Chọn trạng thái',
'other_pic'	=>	'Hình phụ',

'other_option' => 'CÁC TÙY CHỌN KHÁC',
'date_begin' => 'Ngày bắt đầu',
'date_expire' => 'Ngày kết thúc',
'price_buy' => 'Giá mua ngay',

'del_pic_success' => 'Xóa hình thành công',
'edit_pic_success' => 'Cập nhật hình thhành công',
'no_chose_pic' => 'Vui lòng chọn hình để cập nhật',

//move
'source_category'	=>	"danh mục cần chuyển",
'des_category'	=>	"Chuyển tới danh mục",
'move_all'	=>	"Chuyển tất cả SP của danh mục",
'move_success' => "Di chuyển thành công",
'move_item_success' => 'Di chuyển SP thành công',
'no_have_cat_download' => 'Chưa có danh  mục nào',

//setting
'manage_setting_product' => 'Quản lý cấu hình cho sản phẩm',
'add_setting_product_success' => 'Thêm cấu hình sản phẩm thành công',
'add_setting_product_failt' => 'Thêm cấu hình sản phẩm thất bại ',
'edit_setting_product_success' => 'Cập nhật cấu hình sản phẩm thành công',
'setting_product'	=>	'Các chức năng module product',
'comment_product'	=>	'Đánh giá sản phẩm',
'sendmail_product'	=>	'Giới thiệu SP qua email',
'wishlist_pro'	=>	'Danh sách SP ưu thích',
'toolsearch'	=>	'Kích hoạt thanh công cụ tổ hợp tìm kiếm',
'config_product'	=>	'Cấu hình chung của module product',
'rate'	=>	'Tỉ giá ',
'n_list'	=>	'Số sản phẩm  xem theo list',
'n_grid'	=>	'Số sản phẩm  xem theo grid',
'show_nophoto'	=>	'Hiện hình <strong>nophoto</strong> khi tin không có ảnh',
'img_width_list'	=>	'Chiều ngang hình xem theo list',
'img_width_grid'	=>	'Chiều ngang hình xem theo grid',
'img_width'	=>	'Chiều ngang tối đa của hình upload',
'folder_upload'	=>	'Cấu hình thư mục upload',
'thum_size'	=>	'Tạo file thumb theo size',
'config_detail'	=>	'Cấu hình trang xem chi tiết SP',
'imgdetail_width'	=>	'Chiều ngang tối đa của hình xem chi tiết ',
'n_comment'	=>	'Số comment 1 trang',
'n_other'	=>	'Số sản phẩm cùng loại',


//comment
'detail_commen' => 'Chi tiết đánh giá',
'manage_comment' => 'Quản lý dánh giá - nhận xét',
'content_comment'=> 'Nội dung đánh giá',
'mark' => 'Điểm',
'display_no' => 'Chưa xét duyệt',
'display_yes' => 'Đã xét duyệt',
'no_have_comment' => 'Không có nhận xét nào',

//status
'add_status' => 'Thêm trạng thái mới',
'edit_status' => 'Cập nhật trạng thái ',
'manage_status' => 'Quản lý trạng thái mới',
'is_order'	=>	'Cho phép đặt hàng',
'is_edit_price'	=>	'Cho phép Edit giá',
'no_have_status'	=>	'Chưa có trạng thái nào',
'desc_status'	=>	'Nội dung khuyến mãi',

//order 
'task_order' => 'Chi tiết hóa đơn',
'send_email_order' => 'Gửi email cho khách hàng',
'manage_other' => 'Danh sách hóa đơn',
'f_info_order'	=>	'THÔNG TIN ĐƠN HÀNG',
'payment_address'	=>	'Địa chỉ thanh toán',
'shipping_address'	=>	'Địa chỉ giao hàng',
'f_info_cart'	=>	'THÔNG TIN GIỎ HÀNG',
'stt'	=>	'STT',
'p_name'	=>	'Tên sản phẩm',
'quantity'	=>	'Số lượng',
'total'	=>	'Tổng',
'total_price'	=>	'Tổng tiền',
'comment_order'	=>	'Yêu cầu của hóa đơn',
'update_order'	=>	'CẬP NHẬT ĐƠN HÀNG',
'update_status'	=>	'Cập nhật trạng thái',
'date_order'	=>	'Ngày đặt hàng',
'date_ship'	=>	'Ngày giao hàng',
'note'	=>	'Ghi chú',
'status_order0' => 'Hóa đơn mới',
'status_order1' => 'Đã xác nhận',
'status_order2' => 'Hoàn tất',
'status_order3' => 'Hủy Bỏ',
//option
'add_option' => 'Thêm trạng option mới',
'edit_option' => 'Cập nhật Option ',
'manage_option' => 'Quản lý Option mới',
'no_have_option_product' => 'Chưa có Option SP nào',
'op_name'	=>	'Tên option',

// focus_product
'edit_focus_product'	=>	'Cập nhật sản phẩm focus',
'manage_focus_product'	=>	'Quản lý sản phẩm focus', 
'no_have_product'	=> 'Chưa có product nào',
'set_product_focus'	=>	'Sét focus các product đã chọn',
'list_product_focus_of_cat'	=>	'Danh sách sản phẩm focus của danh mục',
'list_product_focus_of_main'	=>	'Danh sách sản phẩm focus cho <b class=font_err>Trang chủ</b>',
'no_have_product_focus'	=>	'Chưa có sản phẩm focus nào',
'del_focus'	=>	'Xóa các sản phẩm focus đã chọn',
'edit_order'	=>	'Cập nhật thứ tự',
'note_focus'	=> 'Để tạo tiêu điểm cho trang chủ bạn chọn ROOT',
'list_product'	=>	'DANH SÁCH CÁC sản phẩm',
'new_product'	=> 'product mới',
'set_focus_success' => 'Sét focus thành công',
'list_all_focus'	=>	'Danh sách sản phẩm focus',
'list_focus_cat' => 'Danh sách sản phẩm focus của danh mục : ',
 

'edit_product_promotion' => 'Cập nhật nội dung trạng thái',
'manage_product_promotion'	=>	'Quản lý sản phẩm Khuyến mãi / Giảm giá',


//brand
'add_brand' => 'Thêm nhà cung cấp mới' , 
'edit_brand' => 'Cập nhật nhà cung cấp ' , 
'manage_brand' => 'Quản lý nhà cung cấp ' , 
'no_have_brand' => 'Chưa có nhà cung cấp  nào' ,  

'brand'	=>	'Nhà cung cấp',
'select_brand' => '-- Chọn nhà cung cấp --',
 

);

?>