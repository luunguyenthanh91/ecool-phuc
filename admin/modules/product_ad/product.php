<?php
/*================================================================================*\
|| 							Name code : product.php 		 		            	  ||
||  				Copyright @2008 by Thai Son - CMS vnTRUST                     ||
\*================================================================================*/
/**
 * @version : 2.0
 * @date upgrade : 09/01/2009 by Thai Son
 **/
if (! defined('IN_vnT'))
{
  die('Hacking attempt!');
}
//load Model
include(dirname(__FILE__) . "/functions.php");

class vntModule extends Model
{
  var $output = "";
  var $skin = "";
  var $linkUrl = "";
  var $module = "product";
	var $action = "product";

  function vntModule (){
    global $Template, $vnT, $func, $DB, $conf;
		$this->skin = new XiTemplate(DIR_MODULE.DS.$this->module."_ad" . DS . "html" . DS . $this->action . ".tpl");
    $this->skin->assign('CONF', $vnT->conf);
		$this->skin->assign('LANG', $vnT->lang);
		$this->skin->assign("DIR_JS", $vnT->dir_js);
		$this->skin->assign('DIR_IMAGE', $vnT->dir_images);
    $lang = ($vnT->input['lang']) ? $lang = $vnT->input['lang'] : $func->get_lang_default();
    $this->linkUrl = "?mod=".$this->module."&act=".$this->action."&lang=" . $lang;
		$vnT->html->addStyleSheet( "modules/" . $this->module."_ad/css/".$this->module.".css");
		$vnT->html->addScript("modules/" . $this->module."_ad/js/".$this->module.".js");
		$vnT->html->addStyleSheet( $vnT->dir_js."/multi-select/multi-select.css");
		$vnT->html->addScript($vnT->dir_js."/multi-select/multi-select.js");
		$vnT->html->addStyleSheet( $vnT->dir_js."/jquery-ui/jquery-ui.min.css");
		$vnT->html->addScript($vnT->dir_js . "/jquery-ui/jquery-ui.min.js");
		$vnT->html->addScriptDeclaration("
	 		$(function() {
				$('.datepicker').datepicker({
					showOn: 'both',
					buttonImage: '".$vnT->dir_images."/calendar.gif',
					buttonImageOnly: true,
					changeMonth: true,
					changeYear: true
				});
			});
		");
		switch ($vnT->input['sub']){
      case 'add':
        $nd['f_title'] = $vnT->lang['add_product'];
        $nd['content'] = $this->do_Add($lang);
        break;
      case 'edit':
        $nd['f_title'] = $vnT->lang['edit_product'];
        $nd['content'] = $this->do_Edit($lang);
        break;
			case 'duplicate':
        $this->do_Duplicate($lang);
        break;
      case 'del':
        $this->do_Del($lang);
        break;
      default:
        $nd['f_title'] = $vnT->lang['manage_product'];
        $nd['content'] = $this->do_Manage($lang);
        break;
    }
    $nd['menu'] = $func->getToolbar($this->module,$this->action, $lang);
    $nd['row_lang'] = $func->html_lang("?mod=".$this->module."&act=".$this->action, $lang);
    $Template->assign("data", $nd);
    $Template->parse("box_main");
    $vnT->output .= $Template->text("box_main");
  }
  function do_Add ($lang){
    global $vnT, $func, $DB, $conf;
		$vnT->html->addStyleSheet($vnT->dir_js."/autoSuggestv14/autoSuggest.css");
		$vnT->html->addScript($vnT->dir_js."/autoSuggestv14/jquery.autoSuggest.js");
    $err = "";
    $vnT->input['price']=0;
		$vnT->input['display'] =1;
 		$rate = ($vnT->setting['rate']) ? $vnT->setting['rate'] : 1;
		$data['p_id']= 0;
		$data['ngay_begin'] = @date("d/m/Y");
		$num_pic_old = 0;
		$w = ($vnT->setting['img_width']) ? $vnT->setting['img_width'] : 750;
		$w_thumb = ($vnT->setting['img_width_grid']) ? $vnT->setting['img_width_grid'] : 100 ;
		$dir = $vnT->func->get_dir_upload_module($this->module,$vnT->setting['folder_upload']);
    if ($vnT->input['do_submit'] == 1){
			$data = $_POST;
			$cat_id = @implode(",",$vnT->input['list_cat']);
			$maso = trim($vnT->input['maso']);
			$p_name = $vnT->func->txt_HTML($_POST['p_name']);
			$price = str_replace(array(",","."),"",trim($_POST['price']));
			$price_old = str_replace(array(",","."),"",trim($_POST['price_old']));
 			$picture = $vnT->input['picture'];
 			//$list_attr = @implode(",",$vnT->input['list_attr']);
			$info_tag =  $vnT->func->get_input_tags_js ($this->module,$_POST["as_values_htag"]);
			if($vnT->input['op_value']){
				$options = serialize($vnT->input['op_value']);
			}
			if ($maso){
				$res_ck = $DB->query("SELECT p_id,maso FROM products WHERE maso='$maso' AND lang='$lang' ");
				if ($DB->num_rows($res_ck)) $err = $func->html_err($vnT->lang['maso_existed']);
			}
      // insert CSDL
      if (empty($err)) {
				$cot['cat_id'] = $cat_id;
				$cot['cat_list'] = $this->get_cat_list($cat_id);
				$cot['maso'] = $maso;
				$cot['picture'] = $picture;
				$cot['price'] =  $price;
				$cot['price_old'] = $price_old;
				$cot['discount'] = $vnT->input['discount'];
				$cot['tags'] = $info_tag['list_id'];
				$cot['tags_name'] = $info_tag['list_name'];
				$cot['onhand'] = (int) $vnT->input['onhand'];
				$cot['is_parent'] = (int) $vnT->input['is_parent'];
				$cot['in_stock'] = (int) $vnT->input['in_stock'];
				$cot['video'] = ($vnT->input['video']) ? $this->regex_url_youtube($vnT->input['video']) : '';
				$cot['status'] = $vnT->input['status'];
				$cot['link'] =  $vnT->input['link'];
        $image_1 = $vnT->input['image_1'];
        if (! empty($image_1) ) {
          $cot['image_1'] = $image_1 ;
        }

        $image_2 = $vnT->input['image_2'];
        if (! empty($image_1) ) {
          $cot['image_2'] = $image_2 ;
        }

        $image_3 = $vnT->input['image_3'];
        if (! empty($image_3) ) {
          $cot['image_3'] = $image_3 ;
        }

        $image_4 = $vnT->input['image_4'];
        if (! empty($image_4) ) {
          $cot['image_4'] = $image_4 ;
        }

        $image_5 = $vnT->input['image_5'];
        if (! empty($image_5) ) {
          $cot['image_5'] = $image_5 ;
        }
        
				//$cot['attr_id'] = $list_attr;
				$cot['date_post'] = time();
				$cot['date_update'] = time();
				$cot['adminid'] =  $vnT->admininfo['adminid'];
        $ok = $DB->do_insert("products", $cot);
        if ($ok) {
					$p_id = $DB->insertid();
					$cot_d['p_id'] = $p_id;
					$cot_d['p_name'] = $p_name;
					$cot_d['description'] = $DB->mySQLSafe($_POST['description']);
					$cot_d['element'] = $DB->mySQLSafe($_POST['element']);
					$cot_d['tinhnang'] = $DB->mySQLSafe($_POST['tinhnang']);
					$cot_d['chungnhan'] = $DB->mySQLSafe($_POST['chungnhan']);
					$cot_d['hotro'] = $DB->mySQLSafe($_POST['hotro']);
					$cot_d['short'] = $DB->mySQLSafe($_POST['short']);
 					$cot_d['options'] = $options;
					$cot_d['key_search'] =  strtolower($func->utf8_to_ascii($p_name));
					$cot_d['display'] = $vnT->input['display'];
					$cot_d['friendly_url'] = (trim($vnT->input['friendly_url'])) ? trim($vnT->input['friendly_url']) : $func->make_url($p_name);
					$cot_d['friendly_title'] = (trim($vnT->input['friendly_title'])) ? trim($vnT->input['friendly_title']) : $p_name;
					$cot_d['metakey'] = (trim($vnT->input['metakey'])) ? trim($vnT->input['metakey']) :  $p_name ;
					$cot_d['metadesc'] = (trim($vnT->input['metadesc'])) ? trim($vnT->input['metadesc']) :  $func->cut_string($func->check_html($_POST['short'],'nohtml'),200,1);
					$query_lang = $DB->query("select name from language ");
					while ( $row = $DB->fetch_row($query_lang) ) {
						$cot_d['lang'] = $row['name'];
						$DB->do_insert("products_desc",$cot_d);
					}
					if(empty($maso)){
						$dup['maso'] = $this->create_maso ($cat_id,$p_id);
 					}
					$dup['p_order'] = $p_id;
          $DB->do_update("products", $dup, "p_id=$p_id");
					$arr_picture = $this->get_pic_input(0, $dir, $w, $w_thumb);
					if(is_array($arr_picture)) {
						foreach ($arr_picture as $key => $val) {
							$cot_pic = array();
							$cot_pic['p_id'] = $p_id ;
							$cot_pic['picture'] = $val['picture'] ;
							$cot_pic['pic_name'] = $val['pic_name'] ;
							$cot_pic['pic_order'] =  $val['pic_order'] ;
							$vnT->DB->do_insert("product_picture",$cot_pic);
						}
					}
					$seo['sub'] = 'add';
					$seo['modules'] = $this->module;
					$seo['action'] = "detail";
					$seo['name_id'] = "itemID";
					$seo['item_id'] = $p_id;
					$seo['friendly_url'] = $cot_d['friendly_url'] ;
					$seo['lang'] = $lang;
					$seo['query_string'] = "mod:".$seo['modules']."|act:".$seo['action']."|".$seo['name_id'].":".$p_id;
					$res_seo = $func->build_seo_url($seo);
					if($res_seo['existed']==1){
						$DB->query("UPDATE products_desc SET friendly_url='".$res_seo['friendly_url']."' WHERE p_id=".$p_id) ;
					}
					//xoa cache
          $func->clear_cache();
          //insert adminlog
          $func->insertlog("Add", $_GET['act'], $p_id);
          $mess = $vnT->lang['add_success'];
          if(isset($_POST['btn_preview']))	{
						$url = $this->linkUrl . "&sub=edit&id=$p_id&preview=1";
						$DB->query("Update products SET display=0 WHERE p_id=$p_id ");
					}else{
						$url = $this->linkUrl . "&sub=add";
					}
          $func->html_redirect($url, $mess);
        } else {
          $err = $func->html_err($vnT->lang['add_failt'].$DB->debug());
        }
      }
    }
		$res_op = $DB->query("SELECT * FROM product_option n, product_option_desc nd
													WHERE n.op_id=nd.op_id AND nd.lang='$lang' AND n.display=1
													ORDER BY n.op_order, n.op_id DESC");
		while ($r_op = $DB->fetch_row($res_op)){
			$r_op['op_name'] = $func->HTML($r_op['op_name']);
			$this->skin->assign('row', $r_op);
			$this->skin->parse("edit.row_option");
		}
		$data['list_stock'] = $this->List_Stock($data['in_stock']);
		$data['list_cat'] = $this->List_Cat($cat_id,$lang);
		//$data['list_attr'] = $this->get_list_attr($vnT->input['attr_id'],$lang);
		$vnT->func->get_tags_js($this->module,"tags","htag",$data['tags']);
		$data['list_status'] = $this->List_Status_Muti($vnT->input['status'],$lang);
		$data['list_display'] = vnT_HTML::list_yesno ("display",$vnT->input['display']);
		$data["html_content"] = $vnT->editor->doDisplay('description', $vnT->input['description'], '100%', '450', "Default",$this->module,$dir);
		$data["html_element"] = $vnT->editor->doDisplay('element', $data['element'], '100%', '250', "Normal",$this->module,$dir);
		$data["html_tinhnang"] = $vnT->editor->doDisplay('tinhnang', $data['tinhnang'], '100%', '250', "Normal",$this->module,$dir);
		$data["html_chungnhan"] = $vnT->editor->doDisplay('chungnhan', $data['chungnhan'], '100%', '250', "Normal",$this->module,$dir);
		$data["html_hotro"] = $vnT->editor->doDisplay('hotro', $data['hotro'], '100%', '250', "Normal",$this->module,$dir);
		$data["html_short"] = $vnT->editor->doDisplay('short', $data['short'], '100%', '250', "Normal",$this->module,$dir);
		$data['link_seo'] = $conf['rooturl']."xxx.html";
		if($data['friendly_url'])
			$data['link_mxh'] = $conf['rooturl'].$data['friendly_url'].".html";
		$data['link_upload'] = '?mod=media&act=popup_media&module='.$this->module.'&folder='.$this->module.'/'.$dir.'&obj=picture&type=image&TB_iframe=true&width=900&height=474';
		$data['product_type'] = $this->product_type($vnT->input['is_parent'], "onchange='change_parent(this.value);'");
		$data['discount'] = $vnT->input['discount'] ? $vnT->input['discount'] : 0;
		$folder_upload = $conf['rootpath']."vnt_upload/product/".$dir;
		$data['folder_upload'] = '../vnt_upload/product/';
		$data['dir_upload'] =  $dir;
		$data['link_show_file'] = ROOT_URL.'vnt_upload/product';
		$data['folder_upload'] = $folder_upload ;
		$data['folderpath'] =   $conf['rootpath']."vnt_upload/product";
		$data['folder'] = $dir ;
		$data['max_upload'] = ini_get('upload_max_filesize');
		$data['w_thumb'] = $w_thumb;
		$data['w'] = $w;
		$data['folder_browse'] =  ($dir) ? $this->module."/".$dir : $this->module;
    $data['err'] = $err;
    $data['link_action'] = $this->linkUrl . "&sub=add";
    $this->skin->assign('data', $data);
    $this->skin->parse("edit");
    return $this->skin->text("edit");
  }
  function do_Edit ($lang){
    global $vnT, $func, $DB, $conf;
		$vnT->html->addStyleSheet($vnT->dir_js."/autoSuggestv14/autoSuggest.css");
		$vnT->html->addScript($vnT->dir_js."/autoSuggestv14/jquery.autoSuggest.js");
    $id = (int) $vnT->input['id'];
    $rate = ($vnT->setting['rate']) ? $vnT->setting['rate'] : 1;
		$w = ($vnT->setting['img_width']) ? $vnT->setting['img_width'] : 750 ;
		$w_thumb = ($vnT->setting['img_width_grid']) ? $vnT->setting['img_width_grid'] : 100 ;
		$dir = $vnT->func->get_dir_upload_module($this->module,$vnT->setting['folder_upload']);
    if ($vnT->input['do_submit']){
      $data = $_POST;
 			$cat_id = @implode(",",$vnT->input['list_cat']);
			$maso = trim($vnT->input['maso']);
			$p_name = $vnT->func->txt_HTML($_POST['p_name']);
			$price = str_replace(array(",","."),"",trim($_POST['price']));
			$price_old = str_replace(array(",","."),"",trim($_POST['price_old']));
			$picture = $vnT->input['picture'];
			//$list_attr = @implode(",",$vnT->input['list_attr']);
 			$info_tag =  $vnT->func->get_input_tags_js ($this->module,$_POST["as_values_htag"]);
			if($vnT->input['op_value']){
				$options = $vnT->format->txt_serialize($vnT->input['op_value']);
			}
			if ($maso){
				$res_ck = $DB->query("SELECT p_id,maso FROM products WHERE maso='$maso' AND lang='$lang' AND p_id<>$id ");
				if ($DB->num_rows($res_ck)) $err = $func->html_err($vnT->lang['maso_existed']);
			}
      if (empty($err)) {
				$cot['cat_id'] = $cat_id;
				$cot['cat_list'] = $this->get_cat_list($cat_id);
				$cot['maso'] = $maso;
				$cot['picture'] = $picture;
				$cot['price'] =  $price;
				$cot['price_old'] = $price_old;
				$cot['discount'] = $vnT->input['discount'];
				$cot['tags'] = $info_tag['list_id'];
				$cot['tags_name'] = $info_tag['list_name'];
				$cot['onhand'] = (int) $vnT->input['onhand'];
				$cot['is_parent'] = (int) $vnT->input['is_parent'];
				$cot['in_stock'] = (int) $vnT->input['in_stock'];
				$cot['video'] = ($vnT->input['video']) ? $this->regex_url_youtube($vnT->input['video']) : '';
				$cot['status'] = $vnT->input['status'];
				$cot['link'] =  $vnT->input['link'];

        $image_1 = $vnT->input['image_1'];
        if (! empty($image_1) ) {
          $cot['image_1'] = $image_1 ;
        }

        $image_2 = $vnT->input['image_2'];
        if (! empty($image_1) ) {
          $cot['image_2'] = $image_2 ;
        }

        $image_3 = $vnT->input['image_3'];
        if (! empty($image_3) ) {
          $cot['image_3'] = $image_3 ;
        }

        $image_4 = $vnT->input['image_4'];
        if (! empty($image_4) ) {
          $cot['image_4'] = $image_4 ;
        }

        $image_5 = $vnT->input['image_5'];
        if (! empty($image_5) ) {
          $cot['image_5'] = $image_5 ;
        }

				//$cot['attr_id'] = $list_attr;
				$cot['date_update'] = time();
        $ok = $DB->do_update("products", $cot, "p_id=$id");
        if ($ok) {
					$cot_d['p_name'] = $p_name;
					$cot_d['description'] = $DB->mySQLSafe($_POST['description']);
 					$cot_d['options'] =  $options;
					$cot_d['element'] = $DB->mySQLSafe($_POST['element']);
					$cot_d['tinhnang'] = $DB->mySQLSafe($_POST['tinhnang']);
					$cot_d['chungnhan'] = $DB->mySQLSafe($_POST['chungnhan']);
					$cot_d['hotro'] = $DB->mySQLSafe($_POST['hotro']);
					$cot_d['short'] = $DB->mySQLSafe($_POST['short']);
					$cot_d['key_search'] =  strtolower($func->utf8_to_ascii($p_name));
					$cot_d['display'] = $vnT->input['display'];
					$cot_d['friendly_url'] = (trim($vnT->input['friendly_url'])) ? trim($vnT->input['friendly_url']) :  $func->make_url($p_name);
					$cot_d['friendly_title'] = (trim($vnT->input['friendly_title'])) ? trim($vnT->input['friendly_title']) :  $func->utf8_to_ascii($p_name);
					$cot_d['metakey'] = (trim($vnT->input['metakey'])) ? trim($vnT->input['metakey']) :  $p_name ;
					$cot_d['metadesc'] = (trim($vnT->input['metadesc'])) ? trim($vnT->input['metadesc']) :  $func->cut_string($func->check_html($_POST['short'],'nohtml'),200,1) ;
					// echo "<pre/>";print_r(,$cot_d);die;
					$DB->do_update("products_desc",$cot_d," p_id={$id} and lang='{$lang}'");
					//update hinh
					$arr_picture = $this->get_pic_input($id, $dir, $w, $w_thumb);
					//build seo_url
					$seo['sub'] = 'edit';
					$seo['modules'] = $this->module;
					$seo['action'] = "detail";
					$seo['name_id'] = "itemID";
					$seo['item_id'] = $id;
					$seo['friendly_url'] = $cot_d['friendly_url'] ;
					$seo['lang'] = $lang;
					$seo['query_string'] = "mod:".$seo['modules']."|act:".$seo['action']."|".$seo['name_id'].":".$id;
					$res_seo = $func->build_seo_url($seo);
					if($res_seo['existed']==1){
						$DB->query("UPDATE products_desc SET friendly_url='".$res_seo['friendly_url']."' WHERE lang='".$lang."' AND p_id=".$id) ;
					}
          $func->clear_cache();
          //insert adminlog
          $func->insertlog("Edit", $_GET['act'], $id);
          $err = $vnT->lang["edit_success"];
					if(isset($_POST['btn_preview']))	{
						$url = $this->linkUrl . "&sub=edit&id=$id&preview=1";
					}else{
						$url = $this->linkUrl . "&sub=edit&id=$id";
					}
          $func->html_redirect($url, $err);
        } else
          $err = $func->html_err($vnT->lang["edit_failt"].$DB->debug());
      }
    }
    $query = $DB->query("SELECT * FROM products p, products_desc pd
												WHERE p.p_id=pd.p_id AND pd.lang='$lang' AND p.p_id=$id ");
    if ($data = $DB->fetch_row($query)){
			if($vnT->input['preview']==1)	{
				$link_preview = $conf['rooturl'];
				if($vnT->muti_lang) $link_preview .= $lang."/";
				$link_preview.= $data['friendly_url'].".html/?preview=1";
				$mess_preview = str_replace(array("{title}","{link}"),array($data['p_name'],$link_preview),$vnT->lang['mess_preview']);
				$data['js_preview'] = "tb_show('".$mess_preview."', '".$link_preview."&TB_iframe=true&width=1000&height=700',null)";
			}
			if ($data['picture']) {
        $data['pic'] = $this->get_picture($data['picture'],$w_thumb)." <a href=\"javascript:del_picture('picture')\" class=\"del\">Xóa</a>";
				$data['style_upload'] = "style='display:none' ";
      } else {
        $data['pic'] = "";
      }
			if($data['options']){
				$options = unserialize($data['options']);
			}
			$res_op = $DB->query("SELECT * FROM product_option n, product_option_desc nd
														WHERE n.op_id=nd.op_id AND nd.lang='$lang' AND n.display=1
														ORDER BY n.op_order, nd.op_id DESC ");
			while ($r_op = $DB->fetch_row($res_op)){
				$r_op['op_name'] = $func->HTML($r_op['op_name']);
				$r_op['op_value'] = $options[$r_op['op_id']];
				$this->skin->assign('row', $r_op);
				$this->skin->parse("edit.row_option");
			}
			$num_pic_old = 0;
			$res_pic = $vnT->DB->query("SELECT * FROM product_picture
																	WHERE p_id=".$id." ORDER BY pic_order ASC , id DESC" );
			if($num_pic_old = $vnT->DB->num_rows($res_pic)) {
				$pic_stt  = 0;
				while ($row_pic = $vnT->DB->fetch_row($res_pic)) {
					$pic_stt++;
					$row_pic['stt'] = $pic_stt ;
					$row_pic['file_src'] = $row_pic['picture']  ;
					$row_pic['pic'] =  $this->get_picture($row_pic['picture'],$w_thumb) ;
					$row_pic['pic_order'] = $pic_stt;
					$this->skin->assign('row', $row_pic);
					$this->skin->parse("edit.item_pic");
				}
			}
			$data['link_seo'] = $conf['rooturl']."xxx.html";
			if($data['friendly_url'])
				$data['link_mxh'] = $conf['rooturl'].$data['friendly_url'].".html";
			$data['img_mxh'] = '<div class="face-img"><img src="'.$conf['rooturl'].'image.php?image='.$conf['rooturl'].ROOT_UPLOAD.'/'.$data['picture'].'&width=120&height=120&cropratio=1:1" alt="" /></div>';
			if($data['finish']==1){
				$data['readonly'] = 'readonly';
			}
      $data['video'] = ($data['video']) ? 'https://www.youtube.com/watch?v='.$data['video'] : '';
      $data['style_onhand'] = ($data['is_parent'] > 0 ) ? 'style="display:none"' : '';
    }else{
      $mess = $vnT->lang['not_found']." ID : ".$id;
      $url = $this->linkUrl;
      $func->html_redirect($url, $mess);
    }

    if($data['image_1'])
    {

      $data['html_img_image_1'] = $this->get_picture($data['image_1'],$w_thumb);

      $data['image_1'] = $data['image_1'];
    }

    if($data['image_2'])
    {

      $data['html_img_image_2'] = $this->get_picture($data['image_2'],$w_thumb);

      $data['image_2'] = $data['image_2'];
    }

    if($data['image_3'])
    {

      $data['html_img_image_3'] = $this->get_picture($data['image_3'],$w_thumb);

      $data['image_3'] = $data['image_3'];
    }

    if($data['image_4'])
    {

      $data['html_img_image_4'] = $this->get_picture($data['image_4'],$w_thumb);

      $data['image_4'] = $data['image_4'];
    }

    if($data['image_5'])
    {

      $data['html_img_image_5'] = $this->get_picture($data['image_5'],$w_thumb);

      $data['image_5'] = $data['image_5'];
    }


    $data['list_stock'] = $this->List_Stock($data['in_stock']);
		$data['list_cat'] = $this->List_Cat($data['cat_id'],$lang);
		//$data['list_attr'] = $this->get_list_attr($data['attr_id'],$lang);
		$vnT->func->get_tags_js($this->module,"tags","htag",$data['tags']);
		$data['list_status'] = $this->List_Status_Muti($data['status'],$lang);
		$data['list_display'] = vnT_HTML::list_yesno ("display",$data['display']);
		$data["html_content"] = $vnT->editor->doDisplay('description', $data['description'], '100%', '450', "Default",$this->module,$dir);
		$data["html_element"] = $vnT->editor->doDisplay('element', $data['element'], '100%', '250', "Normal",$this->module,$dir);
		$data["html_tinhnang"] = $vnT->editor->doDisplay('tinhnang', $data['tinhnang'], '100%', '250', "Normal",$this->module,$dir);
		$data["html_chungnhan"] = $vnT->editor->doDisplay('chungnhan', $data['chungnhan'], '100%', '250', "Normal",$this->module,$dir);
		$data["html_hotro"] = $vnT->editor->doDisplay('hotro', $data['hotro'], '100%', '250', "Normal",$this->module,$dir);
		$data["html_short"] = $vnT->editor->doDisplay('short',$data['short'],'100%','250',"Normal",$this->module,$dir);
		$data['link_upload'] = '?mod=media&act=popup_media&module='.$this->module.'&folder='.$this->module.'/'.$dir.'&obj=picture&type=image&TB_iframe=true&width=900&height=474';
		$data['product_type'] = $this->product_type($data['is_parent'], "onchange='change_parent(this.value);'");

		$folder_upload = $conf['rootpath']."vnt_upload/product/".$dir;
		$data['folder_upload'] = '../vnt_upload/product/';
		$data['dir_upload'] =  $dir;
		$data['link_show_file'] = ROOT_URL.'vnt_upload/product';
		$data['folder_upload'] = $folder_upload ;
		$data['folderpath'] =   $conf['rootpath']."vnt_upload/product";
		$data['folder'] = $dir;
		$data['max_upload'] = ini_get('upload_max_filesize');
		$data['w_thumb'] = $w_thumb;
		$data['w'] = $w;
		$data['folder_browse'] =  ($dir) ? $this->module."/".$dir : $this->module;
    $data['err'] = $err;
    $data['link_action'] = $this->linkUrl."&sub=edit&id=$id";
    $this->skin->assign('data', $data);
    $this->skin->parse("edit");
    return $this->skin->text("edit");
  }
  function do_Duplicate ($lang){
    global $func, $DB, $conf, $vnT;
		$id = (int) $vnT->input['id'];
		$ext = $vnT->input["ext"];
		$del = 0;
		$qr = "";
		if ($id != 0) {
			$ids = $id;
		}
		if (isset($vnT->input["del_id"])){
			$ids = implode(',', $vnT->input["del_id"]);
		}
		$res = $DB->query("SELECT * FROM products WHERE p_id IN (" . $ids . ") ");
    if ($DB->num_rows($res)){
      while ($row = $DB->fetch_row($res)){
				//Duplicate products
        $cot = $row;
				$cot["p_id"] = "";
				$cot["views"] = 0;
				$cot["votes"] = 0;
				$cot["numvote"] = 0;
				$cot["date_post"] = time();
				$cot["date_update"] = time();
				$DB->do_insert("products", $cot);
				//End Duplicate products
				$p_id = $DB->insertid();
				if($p_id){
					$dup['maso'] = $this->create_maso ($cot['cat_id'],$p_id);
					$dup['p_order'] = $p_id;
          $DB->do_update("products", $dup, "p_id=$p_id");
					$res_d = $DB->query("SELECT * FROM products_desc WHERE p_id=".$row['p_id']." ");
					while ($row_d = $DB->fetch_row($res_d))  {
						$dup_d = $row_d;
						$dup_d["id"] = "";
						$dup_d["p_id"] = $p_id;
						$DB->do_insert("products_desc", $dup_d);
					}
					//Duplicate product_picture
					$res_pic = $DB->query("SELECT * FROM product_picture WHERE p_id=".$row['p_id']." ");
					while ($row_pic = $DB->fetch_row($res_pic))  {
						$dup_pic = $row_pic;
						$dup_pic["id"] = "";
						$dup_pic["p_id"] = $p_id;
						$DB->do_insert("product_picture", $dup_pic);
					}
					//End Duplicate product_picture


					//build seo_url
					$seo['sub'] = 'add';
					$seo['modules'] = $this->module;
					$seo['action'] = "detail";
					$seo['name_id'] = "itemID";
					$seo['item_id'] = $p_id;
					$seo['friendly_url'] = $dup_d['friendly_url']."-".time() ;
					$seo['lang'] = $lang;
					$seo['query_string'] = "mod:".$seo['modules']."|act:".$seo['action']."|".$seo['name_id'].":".$p_id;
					$res_seo = $func->build_seo_url($seo);
					if($res_seo['existed']==1){
						$DB->query("UPDATE products_desc SET friendly_url='".$res_seo['friendly_url']."' WHERE p_id=".$p_id) ;
					}

				}
      }
      $mess = $vnT->lang["duplicate_success"];
			//xoa cache
      $func->clear_cache();
    } else  {
      $mess = $vnT->lang["duplicate_failt"];
    }
		$ext_page = str_replace("|", "&", $ext);
		$url = $this->linkUrl . "&{$ext_page}";
		$func->html_redirect($url, $mess);
  }
  function do_Del ($lang){
    global $func, $DB, $conf, $vnT;
		$id = (int) $vnT->input['id'];
		$ext = $vnT->input["ext"];
		$del = 0;
		$qr = "";
		if ($id != 0) {
			$ids = $id;
		}
		if (isset($vnT->input["del_id"])){
			$ids = implode(',', $vnT->input["del_id"]);
		}
		$res = $DB->query("SELECT p_id FROM products WHERE p_id IN (" . $ids . ") ");
    if ($DB->num_rows($res)){
			$DB->query("UPDATE products_desc SET display=-1 WHERE p_id IN (" . $ids . ") AND lang='$lang'");
			//insert RecycleBin
			$rb_log['module'] = $this->module;
			$rb_log['action'] = "detail";
			$rb_log['tbl_data'] = "products";
			$rb_log['name_id'] = "p_id";
			$rb_log['item_id'] = $ids;
			//$rb_log['lang'] = $lang;
			$func->insertRecycleBin($rb_log);

			//build seo_url
      $seo['sub'] = 'del';
      $seo['modules'] = $this->module;
      $seo['action'] = "detail";
      $seo['item_id'] = $ids;
      $seo['lang'] = $lang;
      $res_seo = $func->build_seo_url($seo);

      $mess = $vnT->lang["del_success"];
			//xoa cache
      $func->clear_cache();
    } else  {
      $mess = $vnT->lang["del_failt"];
    }
		$ext_page = str_replace("|", "&", $ext);
		$url = $this->linkUrl . "&{$ext_page}";
		$func->html_redirect($url, $mess);
  }
  function render_row ($row_info, $lang){
    global $func, $DB, $conf, $vnT;
    $row = $row_info;
    // Xu ly tung ROW
    $id = $row['p_id'];
    $row_id = "row_" . $id;
    $output['check_box'] = vnT_HTML::checkbox("del_id[]", $id, 0, " ");
    $link_edit = $this->linkUrl . '&sub=edit&id=' . $id . '&ext=' . $row['ext_page'];
		$link_duplicate = $this->linkUrl . '&sub=duplicate&id=' . $id . '&ext=' . $row['ext_page'];
    $link_del = "javascript:del_item('".$this->linkUrl."&sub=del&id=".$id."&ext=" . $row['ext_page'] . "')";
    $link_pro_sub = "?mod=product&act=product_sub&p_id={$id}&lang=$lang";
    $link_belong = "?mod=product&act=belong_product&sub=edit&id={$id}&lang=$lang";
		if ($row['picture']){
			$output['picture']= $this->get_picture($row['picture'],50);
		}else $output['picture'] ="No image";
	 	$output['order'] = $row['ext'] . "<input name=\"txt_Order[{$id}]\" type=\"text\" size=\"3\"  style=\"text-align:center\" value=\"{$row['p_order']}\"  onkeypress=\"return is_num(event,'txtOrder')\" onchange='javascript:do_check($id)' />";
    $output['p_name'] = "<strong><a href='" . $link_edit . "' >" . $func->HTML($row['p_name']) . "</a></strong>";
		$output['p_name'] .= '<div style="padding:2px;"> <span class=font_err >(MS: <b >'.$row['maso'].'</b>)</div>';
		$output['price'] = "<input name=\"txtPrice[{$id}]\" type=\"text\" style=\"text-align:center\" size=\"10\"  value=\"{$row['price']}\" onchange='javascript:do_check($id)' />";
		$info = "<div style='padding:2px;'>".$vnT->lang['date_post']." : <b>".@date("d/m/Y",$row['date_post'])."</b></div>";
		$info .=  "<div style='padding:2px;'>".$vnT->lang['views']." : <strong>".(int)$row['views']."</strong></div>";
		$output['info'] = $info;
		$pic_other = "<div class='img_other' style='padding:2px 0px;width:120px;'><img src='".$vnT->dir_images."/belong.png' align='absmiddle' width='20'/> <a href='{$link_belong}'>SP liên quan</a></div>";
		if($row['is_parent'] == 1)
			$pic_other .= "<div class='img_other' style='padding:2px 0px;width:120px;'><img src='".$vnT->dir_images."/ico_sanphamdikem.gif' align='absmiddle' width='20'/> <a href='{$link_pro_sub}'>Sản phẩm con</a></div>";
		$output['pic_other'] = $pic_other;
		$link_display = $this->linkUrl.$row['ext_link'];
		if ($row['display'] == 1) {
			$display = "<a class='i-display' href='" . $link_display . "&do_hidden=$id' data-toggle='tooltip' data-placement='top'  title='" . $vnT->lang['click_do_hidden'] . "' ><i class=\"fa fa-eye\" aria-hidden=\"true\"></i></a>";
		} else {
			$display = "<a class='i-display'  href='" . $link_display . "&do_display=$id'  data-toggle='tooltip' data-placement='top'  title='" . $vnT->lang['click_do_display'] . "' ><i class=\"fa fa-eye-slash\" aria-hidden=\"true\"></i></a>";
		}
		$output['action'] = '<div class="action-buttons"><input name=h_id[]" type="hidden" value="' . $id . '" />';
		$output['action'] .= '<a href="javascript:void(0)" class="i-duplicate"  data-toggle=\'tooltip\' data-placement=\'top\' title="Nhân bản" onclick="vnTRUST.confirm_redirect(\'Bạn có chắc chưa\',\''.$link_duplicate.'\')"><i class="fa fa-files-o" aria-hidden="true"></i></a>';
		$output['action'] .= '<a href="' . $link_edit . '" class="i-edit"  data-toggle=\'tooltip\' data-placement=\'top\' title="Cập nhật"  ><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>';
		$output['action'] .= $display;
		$output['action'] .= '<a href="' . $link_del . '" class="i-del"  data-toggle=\'tooltip\' data-placement=\'top\' title="Xóa" ><i class="fa fa-trash-o" aria-hidden="true"></i></a>';
		$output['action'] .= '</div>';
		return $output;
  }
  function do_Manage ($lang){
    global $vnT, $func, $DB, $conf;
    //update
    if ($vnT->input["do_action"]){
      $func->clear_cache();
			$mess ='';
      if ($vnT->input["del_id"]) $h_id = $vnT->input["del_id"];
      switch ($vnT->input["do_action"]){
        case "do_edit":
					if (isset($vnT->input["txt_Order"])) $arr_order = $vnT->input["txt_Order"];
          if (isset($vnT->input["txtPrice"])) $arr_price = $vnT->input["txtPrice"] ; else $arr_price="";
          $mess .= "- " . $vnT->lang['edit_success'] . " ID: <strong>";
          $str_mess = "";
          for ($i = 0; $i < count($h_id); $i ++){
						$dup['p_order'] = $arr_order[$h_id[$i]];
						$dup['price'] = $arr_price[$h_id[$i]];
						$dup['date_update'] = time();
            $ok = $DB->do_update("products", $dup, "p_id=" . $h_id[$i]);
            if ($ok) {
              $str_mess .= $h_id[$i] . ", ";
            }
          }
          $mess .= substr($str_mess, 0, - 2) . "</strong><br>";
          $err = $func->html_mess($mess);
          break;
        case "do_hidden":
          $mess .= "- " . $vnT->lang['hidden_success'] . " ID: <strong>";
          for ($i = 0; $i < count($h_id); $i ++){
            $dup['display'] = 0;
            $ok = $DB->do_update("products_desc", $dup, "lang='$lang' AND p_id=" . $h_id[$i]);
            if ($ok){
              $str_mess .= $h_id[$i] . ", ";
            }
          }
          $mess .= substr($str_mess, 0, - 2) . "</strong><br>";
          $err = $func->html_mess($mess);
          break;
        case "do_display":
          $mess .= "- " . $vnT->lang['display_success'] . "  ID: <strong>";
          for ($i = 0; $i < count($h_id); $i ++){
            $dup['display'] = 1;
            $ok = $DB->do_update("products_desc", $dup, "lang='$lang' AND p_id=" . $h_id[$i]);
            if ($ok){
              $str_mess .= $h_id[$i] . ", ";
            }
          }
          $mess .= substr($str_mess, 0, - 2) . "</strong><br>";
          $err = $func->html_mess($mess);
          break;
      }
    }
		if((int)$vnT->input["do_display"]) {
			$ok = $DB->query("UPDATE products_desc SET display=1 WHERE lang='$lang' AND p_id=".$vnT->input["do_display"]);
			if($ok){
				$mess .= "- ".$vnT->lang['display_success'] . "  ID: <strong>".$vnT->input["do_display"] . "</strong><br>";
				$err = $func->html_mess($mess);
			}
      $func->clear_cache();
		}
		if((int)$vnT->input["do_hidden"]) {
			$ok = $DB->query("UPDATE products_desc SET display=0 WHERE lang='$lang' AND p_id=".$vnT->input["do_hidden"]);
			if($ok){
				$mess .= "- " . $vnT->lang['display_success']." ID: <strong>".$vnT->input["do_hidden"]."</strong><br>";
				$err = $func->html_mess($mess);
			}
      $func->clear_cache();
		}
		$p = ((int) $vnT->input['p']) ? $p = $vnT->input['p'] : 1;
		$n = ($conf['record']) ? $conf['record'] : 30;
		$cat_id = ((int) $vnT->input['cat_id']) ?   $vnT->input['cat_id'] : 0;
		$status_id = ((int) $vnT->input['status_id']) ?   $vnT->input['status_id'] : 0;
		$search = ($vnT->input['search']) ?  $vnT->input['search'] : "p_id";
		$keyword = ($vnT->input['keyword']) ?  $vnT->input['keyword'] : "";
		$direction = ($vnT->input['direction']) ?   $vnT->input['direction'] : "DESC";
		$adminid = ((int) $vnT->input['adminid']) ?   $vnT->input['adminid'] : 0;
		$date_begin = ($vnT->input['date_begin']) ?  $vnT->input['date_begin'] : "";
		$date_end = ($vnT->input['date_end']) ?  $vnT->input['date_end'] : "";
		$where ="  ";
		$ext_page='';
		$ext='';
		if(!empty($cat_id)){
			$where .=" and FIND_IN_SET('$cat_id',cat_list)<>0 ";
			$ext_page .="cat_id=$cat_id|";
			$ext.="&cat_id=$cat_id";
		}
		if($status_id){
			$where .= " and FIND_IN_SET('$status_id',status)<>0 ";
			$ext_page.="status_id=$status_id|";
			$ext.="&status_id={$status_id}";
		}
		if($date_begin || $date_end ){
			$tmp1 = @explode("/", $date_begin);
			$time_begin = @mktime(0, 0, 0, $tmp1[1], $tmp1[0], $tmp1[2]);

			$tmp2 = @explode("/", $date_end);
			$time_end = @mktime(23, 59, 59, $tmp2[1], $tmp2[0], $tmp2[2]);

			$where.=" AND (date_post BETWEEN {$time_begin} AND {$time_end} ) ";
			$ext.="&date_begin=".$date_begin."&date_end=".$date_end;
			$ext_page.= "date_begin=".$date_begin."|date_end=".$date_end."|";
		}
		if(!empty($search)){
			$ext_page.="search=$search|";
			$ext.="&search={$search}";
		}
		if(!empty($keyword)){
			switch($search){
				case "p_id" : $where .=" and  p.p_id = $keyword ";   break;
				case "date_post" : $where .=" and DATE_FORMAT(FROM_UNIXTIME(date_post),'%d/%m/%Y') = '{$keyword}' "; break;
				default :$where .=" and $search like '%$keyword%' ";break;
			}
			$ext_page.="keyword=$keyword|";
			$ext.="&keyword={$keyword}";
		}
		$sortField = ($vnT->input['sortField']) ? $vnT->input['sortField'] : "p_order";
		$sortOrder = ($vnT->input['sortOrder']) ? $vnT->input['sortOrder'] : "DESC";
		$OrderBy = " ORDER BY $sortField $sortOrder , date_post DESC ";
 		$ext_page=$ext_page."p=$p";
    $query= $DB->query("SELECT p.p_id FROM products p, products_desc pd
												WHERE p.p_id=pd.p_id AND pd.lang='$lang' AND display <> -1 $where");
    $totals = intval($DB->num_rows($query));
    $num_pages = ceil($totals / $n);
    if ($p > $num_pages) $p = $num_pages;
    if ($p < 1) $p = 1;
    $start = ($p - 1) * $n;
    $nav = $func->paginate($totals, $n, $ext, $p);
    $table['link_action'] = $this->linkUrl . "{$ext}&p=$p";
		$ext_link = $ext."&p=$p" ;
		$sortLinks = array("p_id","p_order","p_name","price","date_post" );
		$data['SortLink'] = $func->BuildSortingLinks($sortLinks,$this->linkUrl.$ext."&p=".$p, $sortField, $sortOrder);
    $table['title'] = array(
			'check_box' => "<input type=\"checkbox\" name=\"checkall\" id=\"checkall\" class=\"checkbox\" />|5%|center" ,
			'order' => $vnT->lang['order']." ".$data['SortLink']['p_order']."|7%|center",
			'picture' => $vnT->lang['picture']." |8%|center",
			'p_name' => $vnT->lang['product_name']." ".$data['SortLink']['p_name']."|27%|left",
			'price' => " Giá bán".$data['SortLink']['price']."|15%|left",
			'pic_other' => "Cập nhậ|15%|left",
			'info' =>$vnT->lang['infomartion']."|20%|left",
			'action' => "Action|15%|center"
		);
    $sql = "SELECT * FROM products p, products_desc pd
						WHERE p.p_id=pd.p_id AND pd.lang='$lang' AND display <> -1
						$where $OrderBy LIMIT $start,$n";
    $result = $DB->query($sql);
    if ($DB->num_rows($result)){
      $row = $DB->get_array($result);
      for ($i = 0; $i < count($row); $i ++){
				$row[$i]['ext_link'] = $ext_link ;
				$row[$i]['ext_page'] = $ext_page;
        $row_info = $this->render_row($row[$i],$lang);
        $row_field[$i] = $row_info;
        $row_field[$i]['stt'] = ($i + 1);
        $row_field[$i]['row_id'] = "row_" . $row[$i]['p_id'];
        $row_field[$i]['ext'] = "";
      }
      $table['row'] = $row_field;
    } else {
      $table['row'] = array();
      $table['extra'] = "<div align=center class=font_err >" . $vnT->lang['no_have_product'] ."</div>";
    }
		$table['button'] = '<input type="button" name="btnHidden" value=" '.$vnT->lang['hidden'].' " class="button" onclick="do_submit(\'do_hidden\')">&nbsp;';
		$table['button'] .= '<input type="button" name="btnDisplay" value=" '.$vnT->lang['display'].' " class="button" onclick="do_submit(\'do_display\')">&nbsp;';
		$table['button'] .= '<input type="button" name="btnEdit" value=" '.$vnT->lang['update'].' " class="button" onclick="do_submit(\'do_edit\')">&nbsp;';
		$table['button'] .= '<input type="button" name="btnDel" value=" '.$vnT->lang['delete'].' " class="button" onclick="del_selected(\''.$this->linkUrl.'&sub=del&ext='.$ext_page.'\')">';
    $table_list = $func->ShowTable($table);
    $data['table_list'] = $table_list;
    $data['totals'] = $totals;
		$data['listcat']= $this->Get_Cat($cat_id,$lang);
		$data['list_status'] = $this->List_Status_Pro("status_id",$status_id,$lang);
		$data['list_search']=$this->List_Search($search);
		$data['keyword'] = $keyword;
		$data['date_begin'] = $date_begin;
		$data['date_end'] = $date_end;
		if($totals>5000){
			$data['link_excel']="javascript:alert('Vui lòng lọc bớt dữ liệu để xuất file Excel, tránh quá tải server');" ;
			$data['link_excel_sub']="javascript:alert('Vui lòng lọc bớt dữ liệu để xuất file Excel, tránh quá tải server');" ;
		}else{
			$data['link_excel'] = $conf['rooturl']."admin/modules/product_ad/excel_pro.php?lang=$lang".$ext ;
			$data['link_excel_sub'] = $conf['rooturl']."admin/modules/product_ad/excel_sub.php?lang=$lang".$ext ;
		}
    $data['err'] = $err;
    $data['nav'] = $nav;
    /*assign the array to a template variable*/
    $this->skin->assign('data', $data);
    $this->skin->parse("manage");
    return $this->skin->text("manage");
  }
}

$vntModule = new vntModule();
?>
