<!-- BEGIN: edit -->
{data.err}
<form action="{data.link_action}" method="post" enctype="multipart/form-data" name="myForm" id="myForm" class="validate">
  <table width="100%"  border="0"  cellspacing="1" cellpadding="1" class="admintable">
    <tr class="form-required">
      <td class="row1" width="20%">Danh mục: </td>
      <td class="row0">
        {data.list_cat}
      </td>
    </tr>
    <tr class="form-required">
      <td class="row1" width="20%">Tên: </td>
      <td class="row0">
        <input name="title" id="title" type="text" size="60" maxlength="250" value="{data.title}">
      </td>
    </tr>
    <tr class="form-required">
      <td class="row1" width="20%">{LANG.maso}: </td>
      <td class="row0">
        <input name="maso" id="maso" type="text" size="20" maxlength="250" value="{data.maso}">
        <span class="font_err">({LANG.note_maso})</span>
      </td>
    </tr>
    <tr class="form-required">
      <td class="row1" width="20%">Màu sắc: </td>
      <td class="row0">
        <input name="color" id="color" type="text" size="60" maxlength="250" value="{data.color}">
      </td>
    </tr>
    <tr class="form-required">
      <td class="row1" width="20%">Thông số: </td>
      <td class="row0">
        <input name="param" id="param" type="text" size="60" maxlength="250" value="{data.param}">
      </td>
    </tr>
    <tr>
      <td class="row1">Giá gốc : </td>
      <td class="row0" align="left">
        <input name="price_old" id="price_old" type="text" size="20" maxlength="250" value="{data.price_old}" autocomplete="off" onKeyUp="mixMoney(this)" onKeyPress="numberOnly(this,event)" class="form-control"> <strong>VNĐ</strong>
        &nbsp;&nbsp; Giảm giá
        <input name="discount" id="discount" type="number" style="max-width: 50px" size="5" maxlength="250" value="{data.discount}" autocomplete="off" class="form-control" min="0" max="100"> %
      </td>
    </tr>
    <tr>
      <td class="row1" >Giá bán: </td>
      <td class="row0" align="left">
        <input name="price" id="price" type="text" size="20" maxlength="250" value="{data.price}" onKeyUp="mixMoney(this)" onKeyPress="numberOnly(this,event)" class="form-control"> <strong>VNĐ</strong>
        <div id="exp_price_text" style="color:#333333; font-weight:bold; font-size:12px">{data.price_text}</div>
      </td>
    </tr>
    <tr class="form-required">
      <td class="row1" width="20%">Tồn kho: </td>
      <td class="row0">
        <input name="onhand" id="onhand" type="number" size="20" maxlength="250" value="{data.onhand}" onKeyPress="numberOnly(this,event)" class="form-control">
      </td>
    </tr> 
    <tr>
      <td class="row1" width="20%">{LANG.picture}: </td>
      <td class="row0">
        <div id="ext_picture" class="picture" >{data.pic}</div>
        <input type="hidden" name="picture"  id="picture" value="{data.picture}" />
        <div id="btnU_picture" class="div_upload" {data.style_upload}><div class="button2"><div class="image"><a title="Add an Image" class="thickbox" id="add_image" href="{data.link_upload}">Chọn hình</a></div></div></div>
      </td>
    </tr>
    <tr>
      <td class="row1" colspan="2" ><p><strong>{LANG.description} :</strong></p>  {data.html_content}</td>
    </tr>    
    <tr>
      <td class="row0" colspan="2" align="center" height="50">
        <input type="hidden" name="color_old"	value="{data.color_default}"/>
        <input type="hidden" name="do_submit"	value="1"/>
        <input type="submit" name="btnAdd" value="{LANG.btn_submit}" class="button">&nbsp;
        <input type="reset" name="btnReset" value="{LANG.btn_reset}" class="button">&nbsp;
      </td>
    </tr>
	</table>
</form>
<br>
<!-- END: edit -->

<!-- BEGIN: manage -->
<form action="{data.link_fsearch}" method="post" name="fSearch">
<table width="80%" border="0" cellspacing="1" cellpadding="1" align="center"  bgcolor="#CCCCCC">
  <tr bgcolor="#FFFFFF">
    <td width="150" align="center" style="padding:5px;">{data.pic}</td>
    <td style="padding:5px;line-height:20px;">Mã số : <strong class="font_err">{data.maso}</strong><br />
				{LANG.title}: <strong>{data.p_name}</strong><br />
         ID : #<strong>{data.p_id}</strong><br />
    		</td>
  </tr>
</table>
<br/>
<table width="100%" border="0" cellspacing="2" cellpadding="2" align="center" class="tableborder">
  
  <tr>
    <td width="15%" align="left"><strong>{LANG.totals}:</strong> &nbsp;</td>
    <td width="85%" align="left"><b class="font_err">{data.totals}</b></td>
  </tr>
</table>
</form>
{data.err}
<br />
{data.table_list}
<br />
<table width="100%"  border="0" align="center" cellspacing="1" cellpadding="1" class="bg_tab">
  <tr>
    <td  height="25">{data.nav}</td>
  </tr>
</table>
<br />
<!-- END: manage -->