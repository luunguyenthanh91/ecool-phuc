<!-- BEGIN: html_edit -->
<br />
<table width="80%" border="0" cellspacing="1" cellpadding="1" align="center"  bgcolor="#CCCCCC">
  <tr bgcolor="#FFFFFF">
    <td width="150" align="center" style="padding:5px;">{data.pic_pro}</td>
    <td style="padding:5px;">MS : <strong class="font_err">{data.maso}</strong><br />
				Tên SP: <strong>{data.p_name}</strong><br />
        Product ID : #<strong>{data.p_id}</strong>
    		</td>
  </tr>
</table>
<br />
{data.table_list_focus}
<br/>
{data.table_list}
<br/>
<!-- END: html_edit -->


<!-- BEGIN: list_product -->
<br />
{data.err}
<form action="{data.link_fsearch}" method="post" name="myform">
<table width="100%" border="0" cellspacing="2" cellpadding="2" align="center" class="tableborder">
    <tr>
    <td align="left" width="15%"><strong>{LANG.category}:</strong> &nbsp;</td>
    <td align="left">{data.listcat}&nbsp; </td>
  </tr>

  <tr>
    <td  colspan="2"><strong>{LANG.search} :</strong> {data.list_search} <strong> {LANG.keyword} :</strong>  <input name="keyword" value="{data.keyword}" size="20" type="text" />&nbsp;<input type="submit" name="btnGo" value=" Search " class="button"></td>
  </tr>
  
  </table>
</form>

<br />
<table width="100%" border="0" cellspacing="0" cellpadding="0" style="margin-bottom:5PX;">
  <tr>
  <td height="30" style="border-bottom: 2px solid rgb(184, 65, 32);" class="font_title">{data.f_title}</td>
  </tr>
</table>
{data.table_list}
<br />
<table width="100%"  border="0" align="center" cellspacing="1" cellpadding="1" class="bg_tab">
  <tr>
    <td  height="25">{data.nav}</td>
  </tr>
</table>
<!-- END: list_product -->




<!-- BEGIN: list_focus -->
<script language="javascript">
function check_all_focus()	{
	for ( i=0;i < document.f_focus.elements.length ; i++ ){
		if (document.f_focus.elements[i].type=="checkbox" && document.f_focus.elements[i].name!="all"){
			row_id = 'rowfocus_'+document.f_focus.elements[i].value;
		}else{
			row_id="";
		}
		
		if ( document.f_focus.all.checked==true ){
			document.f_focus.elements[i].checked = true;
			if (row_id!="" ){
				getobj(row_id).className = "row_select";
			}
		}
		else{
			document.f_focus.elements[i].checked  = false;
			if (row_id!="" ){
				if (i%2==0)
					getobj(row_id).className = "row1";
				else
					getobj(row_id).className = "row";
			}
		}
		
	}
}

function do_check_focus (id){
	
	for ( i=0;i < document.f_focus.elements.length ; i++ ){
		if (document.f_focus.elements[i].type=="checkbox" && document.f_focus.elements[i].name!="all" && document.f_focus.elements[i].value == id){
			document.f_focus.elements[i].checked=true;
			getobj("rowfocus_"+id).className = "row_select";
			
			break;
		}	
	}
	
}
function selected_item_focus(){
		var name_count = document.f_focus.length;

		for (i=0;i<name_count;i++){
			if (document.f_focus.elements[i].checked){
				return true;
			}
		}
		alert('Hãy chọn ít nhất 1 record');
		return false;
	}
	
function del_focus_selected(action) {
		if (selected_item_focus()){
			question = confirm('Bạn có chắc muốn XÓA không ?')
			if (question != "0"){
				document.f_focus.action=action;
				document.f_focus.submit();
			}else{
			  alert ('Phew~');
		    }
		}
	
}

</script>

<table width="100%" border="0" cellspacing="0" cellpadding="0" style="margin-bottom:5PX;">
  <tr>
  <td height="30" style="border-bottom: 2px solid rgb(184, 65, 32);" class="font_title">Có <strong class="font_err">{data.totals}</strong> sản phẩm đi kèm</td>
  </tr>
</table>
{data.err}
<form action="{data.link_action}" method="post" name="f_focus" id="f_focus">
<table width="100%"  border="0" cellspacing="0" cellpadding="0" class="bg_tbl">
	<tr>
		<td >
		<table width="100%" cellspacing="0" cellpadding="0" border="0" class="adminlist">
    <thead>
		  <tr>
			  <th width="5%" align="center"><input type="checkbox" name="all" onclick="javascript:check_all_focus();"></th>
			  <th width="10%" align="center">{LANG.picture}</th>
			  <th width="40%" align="center">{LANG.product_name}</th>
        <th width="15%" align="center">{LANG.status}</th>
			  <th width="20%" align="center">{LANG.category} </th>
		  </tr>
      </thead>
      <tbody>
		   <!-- BEGIN: row_focus -->
      <tr class="{row.class}" id="{row.row_id}"> 
        <td align="center">{row.check_box}&nbsp;</td>
        <td align="center">{row.picture}&nbsp;</td>
        <td align="left">{row.p_name}&nbsp;</td>
        <td align="center">{row.status}&nbsp;</td>
        <td align="center">{row.cat_name}&nbsp;</td>
      </tr>
      <!-- END: row_focus -->
      
      <!-- BEGIN: row_focus_no -->
         <tr class="row0" >
           <td  colspan="6" align="center" class="font_err" >{mess}</td>
         </tr>
         <!-- END: row_focus_no -->
         
      </tbody>
		</table>
		</td>
	</tr>
	<tr>
		<td  style="padding:5px;">
		<input type="button" name="btnDel" value="Xóa các SP đi kèm đã chọn" class="button" onclick="javascript:del_focus_selected('{data.link_del}');" ></td>
	</tr>
</table>
</form>    
<br>
<table width="100%"  border="0" cellspacing="1" cellpadding="1" class="bg_tab" align=center>
      <tr  height=25>
        <td align="left" >{data.nav}</td>
      </tr>
</table>
<!-- END: list_focus -->
<!-- BEGIN: manage -->
<style>.img_belong img {width: 40px;height: 40px;}</style>
<form action="{data.link_fsearch}" method="post" name="fSearch">
<table width="100%" border="0" cellspacing="2" cellpadding="2" align="center" class="tableborder">
  <tr>
    <td width="15%" align="left"><strong>{LANG.totals}:</strong> &nbsp;</td>
    <td width="85%" align="left"><b class="font_err">{data.totals}</b></td>
  </tr>
  <tr>
	   <td><strong>{LANG.category} : </strong>  </td>
    <td>{data.listcat}</td>
  </tr>
  <tr>
    <td colspan="2"><strong>{LANG.search} :</strong> {data.list_search} <strong> {LANG.keyword} :</strong>  <input name="keyword" value="{data.keyword}" size="20" type="text" /> &nbsp;<input type="submit" name="btnGo" value=" Search " class="button"></td>
  </tr>
</table>
</form>
{data.err}
<br />
{data.table_list}
<br />
<table width="100%"  border="0" align="center" cellspacing="1" cellpadding="1" class="bg_tab">
  <tr>
    <td  height="25">{data.nav}</td>
  </tr>
</table>
<br />
<!-- END: manage -->
