<!-- BEGIN: edit -->
<link href="{DIR_JS}/metabox/seo-metabox.css" rel="stylesheet" type="text/css" />
<style>
  span.text_chose {
    position: absolute;
    margin-left: 3px;
  }
</style>
<script type="text/javascript" src="{DIR_JS}/metabox/seo-metabox.js"></script>
<script language="javascript" >
  var wpseo_lang = 'en';
  var wpseo_meta_desc_length = '155';
  var wpseo_title = 'p_name';
  var wpseo_content = 'metadesc';
  var wpseo_title_template = '%%title%%';
  var wpseo_metadesc_template = '';
  var wpseo_permalink_template = '{CONF.rooturl}%postname%.html';
  var wpseo_keyword_suggest_nonce = 'a7c4d81c79';
  $(document).ready(function() {
  	jQuery.validator.addMethod("category", function( value, element ) {
  		var result = multipleSelectOnSubmit();
  		return result;
  	}, "Vui long chon danh muc");
  	$('#myForm').validate({
  	  rules: {
  			cat_id : {
          required: true
        },
  			p_name: {
  				required: true,
  				minlength: 3
  			}
      },
      messages: {
        cat_id: {
          required: "{LANG.err_select_required}"
        },
        p_name: {
  				required: "{LANG.err_text_required}",
  				minlength: "{LANG.err_length} 3 {LANG.char}"
  			}
  	  }
  	});
    $('#list_cat').multiSelect({selectableHeader: "<div class='custom-header'>{LANG.list_cat}</div>",  selectionHeader: "<div class='custom-header'>{LANG.list_cat_chose}</div>",});
  	{data.js_preview}
  });
  function change_parent(parent){
    if(parent > 0){
      $("#ext_onhand").hide();
      $("#ext_onhand input").val(0);
    }else{
      $("#ext_onhand").show();
    }
  }
</script>
{data.err}
<form action="{data.link_action}" method="post" enctype="multipart/form-data" name="myForm" id="myForm" class="validate">
<div class="boxAdminForm">
  <div class="block-left">
    <table width="100%" border="0" cellspacing="1" cellpadding="1" class="admintable">
      <tr>
        <td class="row1">{LANG.category}</td>
        <td class="row0">{data.list_cat}</td>
      </tr>
      <tr>
        <td class="row1" width="20%">Kiểu sản phẩm: </td>
        <td class="row0">
          {data.product_type}
          <span id="ext_onhand" {data.style_onhand}>&nbsp;&nbsp;Tồn kho <input type="text" name="onhand" id="onhand" value="{data.onhand}" size="10"></span>
        </td>
      </tr>
      <tr>
        <td class="row1" width="20%">{LANG.maso}: </td>
        <td class="row0">
          <input name="maso" id="maso" type="text" size="20" maxlength="250" value="{data.maso}" class="form-control" style="width: 150px;">
          <span class="font_err">({LANG.note_maso})</span>
        </td>
      </tr>
      <tr class="form-required">
        <td class="row1" >{LANG.product_name}: </td>
        <td class="row0">
          <input name="p_name" id="p_name" type="text" size="60" maxlength="250" value="{data.p_name}" onkeyup="vnTMXH.setTitle(this.value)" class="form-control">
        </td>
      </tr>
      <tr >
        <td class="row1" width="20%">{LANG.picture}: </td>
        <td class="row0">
          <div id="ext_picture" class="picture">{data.pic}</div>
          <input type="hidden" name="picture" id="picture" value="{data.picture}" />
          <div id="btnU_picture" class="div_upload" {data.style_upload}><div class="button2"><div class="image"><a title="Add an Image" class="thickbox" id="add_image" href="{data.link_upload}" >Chọn hình</a></div></div></div>
        </td>
      </tr>
      <tr>
        <td class="row1">Youtube video : </td>
        <td class="row0" align="left">
          <input name="video" id="video" type="text" size="60" maxlength="250" value="{data.video}" class="form-control"> <span class="font_err">Ex: https://www.youtube.com/watch?v=ZCO1yk-H0Ak</span>
        </td>
      </tr>
      <tr>
        <td class="row1">Giá gốc : </td>
        <td class="row0" align="left">
          <input name="price_old" id="price_old" type="text" size="20" maxlength="250" value="{data.price_old}" autocomplete="off" onKeyUp="mixMoney(this)" onKeyPress="numberOnly(this,event)" class="form-control"> <strong>VNĐ</strong>
          &nbsp;&nbsp; Giảm giá
          <input name="discount" id="discount" type="number" style="max-width: 50px" size="5" maxlength="250" value="{data.discount}" autocomplete="off" class="form-control" min="0" max="100"> %
        </td>
      </tr>
      <tr>
        <td class="row1">Giá bán : </td>
        <td class="row0" align="left">
          <input name="price" id="price" type="text" size="20" maxlength="250" value="{data.price}" autocomplete="off" onKeyUp="mixMoney(this)" onKeyPress="numberOnly(this,event)" class="form-control"> <strong>VNĐ</strong>
        </td>
      </tr>

      <tr>
        <td class="row1">Link : </td>
        <td class="row0" align="left">
          <input name="link" id="link" type="text" size="60" maxlength="250" value="{data.link}" class="form-control">
        </td>
      </tr>
      <tr>
        <td class="row1">Tình trạng : </td>
        <td class="row0" align="left">
          {data.list_stock}
        </td>
      </tr>
      <tr>
        <td class="row1" colspan="2" >
          <div class="panel with-nav-tabs panel-default ">
            <div class="panel-heading">
              <ul class="nav nav-tabs">
                <li class="active"><a data-toggle="tab" href="#Tab1">Mô tả ngắn</a></li>
                <li><a data-toggle="tab" href="#Tab2">Mô tả chi tiết</a></li>
                <li><a data-toggle="tab" href="#Tab3">Thông số kỹ thuật</a></li>
                <li><a data-toggle="tab" href="#Tab5">Tính năng ưu việt</a></li>
                <li><a data-toggle="tab" href="#Tab6">Chứng nhận chất lượng</a></li>
                <li><a data-toggle="tab" href="#Tab7">Hỗ trợ</a></li>
                <li><a data-toggle="tab" href="#Tab4">Hình ảnh phụ</a></li>
                <li><a data-toggle="tab" href="#Tab9">Slide Footer</a></li>
              </ul>
            </div>
            <div class="panel-body">
              <div class="tab-content">
                <div id="Tab1" class="tab-pane fade in active">
                  {data.html_short}
                </div>
                <div id="Tab2" class="tab-pane fade">
                  {data.html_content}
                </div>
                <div id="Tab3" class="tab-pane fade">
                  {data.html_element}
                </div>
                <div id="Tab5" class="tab-pane fade">
                  {data.html_tinhnang}
                </div>
                <div id="Tab6" class="tab-pane fade">
                  {data.html_chungnhan}
                </div>
                <div id="Tab7" class="tab-pane fade">
                  {data.html_hotro}
                </div>
                <div id="Tab4" class="tab-pane fade ">
                  <div class="div-picture">
                    <div class="div-file">
                      <span class="note">Tối đa 8 Hình. Định dạng hình: Jpg,. Jpeg. Dung lượng không quá 2 MB / 1 hình</span>
                    </div>
                    <div id="uploader">
                      <div  id="list_pic_detail" class="list_pic_old">
                        <div class="div_sort" id="filelist">
                          <!-- BEGIN: item_pic -->
                          <div class="pic_item" data_count="{row.stt}">
                            <div class="img">{row.pic}</div>
                            <input name="picture_old[]" type="hidden" value="{row.id}|{row.picture}"/>
                            <a href="javascript:void(0);" class="remove">&nbsp;</a>
                            <input name="pic_name_old[{row.id}]" type="text" value="{row.pic_name}" placeholder="Nhập tên hình" class="form-control"/>
                            <input  name="pic_order_old[{row.id}]" id="pic_order_{row.id}" type="hidden" value="{row.pic_order}" class="pic_order"/>
                          </div>
                          <!-- END: item_pic -->
                        </div><div class="clear"></div>
                      </div>
                    </div>
                    <div class="row_input">
                      <button type="button" class="btn upimg" id="pickfiles" style="position: relative; z-index: 1;"><span>Chọn hình từ máy của bạn</span></button>
                      <button type="button" class="btn btn-browse" style="position: relative; z-index: 1;" onclick="vnTRUST.loadPopupGallery('filelist','product','{data.folder_browse}')" ><span>Chọn hình từ thư viện </span></button>
                    </div>
                  </div>
                  <script type="text/javascript" src="{DIR_JS}/plupload_236/plupload.full.min.js" charset="UTF-8"></script>
                  <script type="text/javascript">
                    // Initialize the widget when the DOM is ready
                    function callback_FileList(list_img) {
                      var arr_img = list_img.split("|");
                      var pic_len = parseInt($(".pic_item").length) ;
                      for (i in arr_img ) {
                        stt = parseInt(i)+1;
                        var picture = arr_img[i].replace("product/", "");
                        var src = ROOT+'vnt_upload/product/'+picture ;
                        var pic_order = parseInt(pic_len) + parseInt(stt) ;

                        html_pic = '<div class="pic_item" data_count="'+pic_order+'" >';
                        html_pic += '<a href="javascript:void(0);" class="remove">&nbsp;</a>';
                        html_pic += '<div class="img"><img src="'+src+'"   alt="" /></img></div>';
                        html_pic += '<input name="pictures[]" type="hidden" value="'+picture+'"/>';

                        html_pic += '<input name="pic_name[]" type="text" value="" placeholder="Nhập tên hình" class="form-control"/>';
                        html_pic += '<input class="pic_order" name="pic_order[]"   type="hidden" value="'+pic_order+'"/>';
                        html_pic += '</div>';
                        $('#filelist').append(html_pic);
                      }
                    }
                    $(document).ready(function() {
                      var uploader = new plupload.Uploader({
                        runtimes : 'gears,html5,flash,silverlight,browserplus',
                        browse_button : 'pickfiles',
                        container : 'uploader',
                        max_file_size : '{data.max_upload}',
                        url : 'modules/product_ad/ajax/upload.php',
                        autostart : true,
                        multipart: true,
                        multipart_params: {
                          'folder_upload': '{data.folder_upload}',
                          'folder': '{data.folder}',
                          'w' : '{data.w_pic}',
                          'thumb' : '1',
                          'w_thumb': '{data.w_pic}'
                        },
                        filters : [
                          {title : "Image files", extensions : "jpg,gif,png"},
                          {title : "Zip files", extensions : "zip"}
                        ],
                        resize : {width : {data.w}, height : {data.w}, quality : 90},
                        flash_swf_url : ROOT+'js/plupload_219/Moxie.swf',
                        silverlight_xap_url : ROOT+'js/plupload_219/Moxie.xap',
                        init: {
                          PostInit: function() {
                          },
                          FilesAdded: function(up, files) {
                            $.each(files, function(i, file) {
                              stt = parseInt($(".pic_item").length)+1 ;
                              html_pic = '<div id="' + file.id + '" class="pic_item"  data_count="'+stt+'">';
                              html_pic +=	'<div class="name">'+file.name + '<a href="#" id="cancel'+file.id+'" class="cancel">cancel</a></div>';
                              html_pic +=	'<div class="fileInfo"><span class="size">' + plupload.formatSize(file.size) + '</span></div>' ;
                              html_pic += '<div class="plupload_progress"><div class="plupload_progress_container"><div class="plupload_progress_bar"></div></div><span class="percentComplete"></span></div>';
                              html_pic += '</div>' ;
                              $('#filelist').append(html_pic);
                              //Fire Upload Event
                              up.refresh(); // Reposition Flash/Silverlight
                              up.start();
                              //Bind cancel click event
                              $('#cancel'+file.id).click(function(){
                                $fileItem = $('#' + file.id);
                                $fileItem.addClass("cancelled");
                                uploader.removeFile(file);
                                $(this).unbind().remove();
                              });
                            });
                          },
                          UploadProgress: function(up, file) {
                            var $fileWrapper = $('#' + file.id);
                            $fileWrapper.find(".plupload_progress").show();
                            $fileWrapper.find(".plupload_progress_bar").attr("style", "width:"+ file.percent + "%");
                            $fileWrapper.find(".percentComplete").html(file.percent+"%");
                            $fileWrapper.find('#cancel'+file.id).remove();
                          },
                          FileUploaded:  function(up, file ,re) {
                            var $fileWrapper = $('#' + file.id);
                            var response = $.parseJSON(re.response) ;
                            $("#picture_"+file.id).val(response.picture) ;
                            var src = ROOT+'vnt_upload/product/'+response.picture ;
                            var pic_order =  $('#' + file.id).attr("data_count") ;
                            html_pic = '<a href="javascript:void(0);" class="remove">&nbsp;</a>';
                            html_pic +=	'<div class="img"><img src="'+src+'"   alt="" /></img></div>';
                            html_pic += '<input id="pictures_' + file.id + '" name="pictures[]" type="hidden" value="'+response.picture+'"/>';

                            html_pic += '<input name="pic_name[]" type="text" value="" placeholder="Nhập tên hình" class="form-control"/>';
                            html_pic += '<input class="pic_order" name="pic_order[]" id="pic_order_' + file.id + '" type="hidden" value="'+pic_order+'"/>';
                            $fileWrapper.html(html_pic);
                            console.log(file);
                          },
                          UploadComplete:  function(up, file) {
                            $(".list_pic_old .pic_item .remove").click(function(){
                              $(this).parent().remove();
                            });
                            $( "#list_pic_detail .div_sort" ).sortable({
                              update: function(event, ui) {
                                $( "#list_pic_detail .div_sort > div.pic_item" ).each(function (e) {
                                    stt = (e+1) ;
                                    $(this).attr("data_count", stt);
                                    $(this).find('.pic_order').val(stt);
                                  });
                              }
                            });
                          },
                          Error: function(up, err) {
                            document.getElementById('console').innerHTML += "\nError #" + err.code + ": " + err.message;
                          }
                        }
                      });
                      uploader.init();
                      $(".list_pic_old .pic_item .remove").click(function(){
                        $(this).parent().remove();
                      });
                      $( "#list_pic_detail .div_sort" ).sortable({
                        update: function(event, ui) {
                          $( "#list_pic_detail .div_sort > div" ).each(function (e) {
                            stt = (e+1) ;
                            $(this).attr("data_count", stt);
                            $(this).find('.pic_order').val(stt);
                          });
                        }
                      });
                    });
                  </script>
                </div>




                <div id="Tab9" class="tab-pane fade ">
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                  <tr {data.style0}  >
                      <td colspan="2" class="row0" align="center">
                            {data.html_img_image_1}
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr >
                                    <td width="20%" class="row1">Banner Footer 1 :&nbsp;</td>
                                    <td align="left" class="row0">
                                      <table border="0" cellspacing="0" cellpadding="0">
                                          <tr>
                                            <td style="padding:0px;" ><input name="image_1" id="image_1" type="text"  size="60" maxlength="250" value="{data.image_1}" readonly="readonly"></td>
                                            <td style="padding:0px;" > <div class="button2"><div class="image"><a title="Add an Image" class="thickbox" id="add_image_1" href="?mod=media&act=popup_media&type=image&&module=product&folder=product&obj=image_1&TB_iframe=true&width=900&height=474" >Image</a></div></div></td>
                                          </tr>
                                      </table>
                                    </td>
                                </tr>
                              </table>

                      </td>
                  </tr>

                  <tr {data.style0}  >
                      <td colspan="2" class="row0" align="center">
                            {data.html_img_image_2}
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr >
                                    <td width="20%" class="row1">Banner Footer 2 :&nbsp;</td>
                                    <td align="left" class="row0">
                                      <table border="0" cellspacing="0" cellpadding="0">
                                          <tr>
                                            <td style="padding:0px;" ><input name="image_2" id="image_2" type="text"  size="60" maxlength="250" value="{data.image_2}" readonly="readonly"></td>
                                            <td style="padding:0px;" > <div class="button2"><div class="image"><a title="Add an Image" class="thickbox" id="add_image_2" href="?mod=media&act=popup_media&type=image&&module=product&folder=product&obj=image_2&TB_iframe=true&width=900&height=474" >Image</a></div></div></td>
                                          </tr>
                                      </table>
                                    </td>
                                </tr>
                              </table>

                      </td>
                  </tr>

                  <tr {data.style0}  >
                      <td colspan="2" class="row0" align="center">
                            {data.html_img_image_3}
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr >
                                    <td width="20%" class="row1">Banner Footer 3 :&nbsp;</td>
                                    <td align="left" class="row0">
                                      <table border="0" cellspacing="0" cellpadding="0">
                                          <tr>
                                            <td style="padding:0px;" ><input name="image_3" id="image_3" type="text"  size="60" maxlength="250" value="{data.image_3}" readonly="readonly"></td>
                                            <td style="padding:0px;" > <div class="button2"><div class="image"><a title="Add an Image" class="thickbox" id="add_image_3" href="?mod=media&act=popup_media&type=image&&module=product&folder=product&obj=image_3&TB_iframe=true&width=900&height=474" >Image</a></div></div></td>
                                          </tr>
                                      </table>
                                    </td>
                                </tr>
                              </table>

                      </td>
                  </tr>

                  <tr {data.style0}  >
                      <td colspan="2" class="row0" align="center">
                            {data.html_img_image_4}
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr >
                                    <td width="20%" class="row1">Banner Footer 4 :&nbsp;</td>
                                    <td align="left" class="row0">
                                      <table border="0" cellspacing="0" cellpadding="0">
                                          <tr>
                                            <td style="padding:0px;" ><input name="image_4" id="image_4" type="text"  size="60" maxlength="250" value="{data.image_4}" readonly="readonly"></td>
                                            <td style="padding:0px;" > <div class="button2"><div class="image"><a title="Add an Image" class="thickbox" id="add_image_4" href="?mod=media&act=popup_media&type=image&&module=product&folder=product&obj=image_4&TB_iframe=true&width=900&height=474" >Image</a></div></div></td>
                                          </tr>
                                      </table>
                                    </td>
                                </tr>
                              </table>

                      </td>
                  </tr>

                  <tr {data.style0}  >
                      <td colspan="2" class="row0" align="center">
                            {data.html_img_image_5}
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr >
                                    <td width="20%" class="row1">Banner Footer 5 :&nbsp;</td>
                                    <td align="left" class="row0">
                                      <table border="0" cellspacing="0" cellpadding="0">
                                          <tr>
                                            <td style="padding:0px;" ><input name="image_5" id="image_5" type="text"  size="60" maxlength="250" value="{data.image_5}" readonly="readonly"></td>
                                            <td style="padding:0px;" > <div class="button2"><div class="image"><a title="Add an Image" class="thickbox" id="add_image_5" href="?mod=media&act=popup_media&type=image&&module=product&folder=product&obj=image_5&TB_iframe=true&width=900&height=474" >Image</a></div></div></td>
                                          </tr>
                                      </table>
                                    </td>
                                </tr>
                              </table>

                      </td>
                  </tr>
                  </table>
                </div>


              </div>
            </div>
          </div>
        </td>
      </tr>
    </table>
  </div>
  <div class="block-right">
    <div class="desc">
      <table width="100%" border="0" cellspacing="2" cellpadding="2" align=center class="admintable desc_title">
        <tr class="row_title" >
          <td colspan="2" class="font_title"><img src="{DIR_IMAGE}/toggle_minus.png" alt="bt_add" title="Collapse" align="absmiddle"/> {LANG.other_option}</td>
        </tr>
      </table>
      <div class="desc_content">
        <table width="100%" border="0" cellspacing="2" cellpadding="2" align=center class="admintable">
          <!-- BEGIN: row_option -->
          <tr>
            <td class="row1">{row.op_name} : </td>
            <td align="left" class="row0">
              <textarea name="op_value[{row.op_id}]" cols="60" rows="1" class="textarea" style="width:95%" class="form-control">{row.op_value}</textarea>
            </td>
          </tr>
          <!-- END: row_option -->
          <tr>
            <td class="row1">{LANG.pro_status}</td>
            <td class="row0">{data.list_status}</td>
          </tr>
          <tr>
            <td class="row1" width="20%">{LANG.display}</td>
            <td class="row0">{data.list_display}</td>
          </tr>
        </table>
      </div>
    </div><br/>
    <div class="desc">
      <table width="100%"  border="0" cellspacing="2" cellpadding="2" align=center class="admintable desc_title">
        <tr class="row_title" >
          <td class="font_title"><img src="{DIR_IMAGE}/toggle_minus.png" alt="bt_add" title="Collapse" align="absmiddle"/> Search Engine Optimization :  </td>
        </tr>
      </table>
      <div class="desc_content" style="background: #ffffff">
        <div class="general">
          <h4 class="wpseo-heading" style="display: none;">General</h4>
          <table width="100%" border="0" cellspacing="1" cellpadding="1" class="admintable">
            <tr>
              <td class="row1" width="20%"><label for="yoast_wpseo_snippetpreview">Snippet Preview:</label></td>
              <td class="row0">
                <div id="wpseosnippet">
                  <a href="#" class="wpseo_title"></a><br/>
                  <a class="wpseo_url"  href="#"></a>
                  <p class="desc"><span class="content" style="color: rgb(136, 136, 136);"></span></p>
                </div>
              </td>
            </tr>
            <tr>
              <td class="row0" ><label ><strong>Friendly<br>URL :</strong></label></td>
              <td class="row0">
                <input name="friendly_url" id="friendly_url" type="text" size="50" maxlength="250" value="{data.friendly_url}" class="textfield" style="width:98%"><br>
                <span class="font_err">({LANG.mess_friendly_url})</span>
              </td>
            </tr>
            <tr>
              <td class="row1"><label for="yoast_wpseo_title">Friendly Title:</label></td>
              <td class="row0"><input type="text" class="textfield" value="{data.friendly_title}" name="friendly_title" id="friendly_title"  style="width:98%"   /><br/><p>Title display in search engines is limited to 70 chars, <span id="friendly_title-length"><span class="good">70</span></span> chars left.<br/></td>
            </tr>
            <tr>
              <td class="row1"><label for="metadesc">Meta Description:</label></td>
              <td class="row0"><textarea name="metadesc" id="metadesc" rows="3" class="textarea"  style="width:98%">{data.metadesc}</textarea><p>The <code>meta</code> description will be limited to 155 chars (because of date display), <span id="metadesc-length"><span class="good">155</span></span> chars left. </p><div id="metadesc_notice"></div></td>
            </tr>
            <tr>
              <td class="row1" ><label for="metakey">Meta Keyword:</label></td>
              <td class="row0"><input type="text" class="textfield" value="{data.metakey}" name="metakey"  id="metakey"  style="width:98%"/><br/><p></p><div style="width: 300px;" class="alignright"><p id="related_keywords_heading" style="display: none;">Related keywords:</p><div id="wpseo_tag_suggestions"></div></div>
              </td>
            </tr>
          </table>
        </div>
        <div class="results" style="padding: 10px;">
          <div id="focuskwresults">
            <div class="article_heading">
              <div class="label">Article Heading</div><span class="wrong">NO</span></div>
            <div class="page_url"><div class="label">Page URL</div><span class="wrong">NO</span></div>
            <div class="page_title"><div class="label">Page title</div><span class="wrong">NO</span></div>
            <div class="meta_desc">
              <div class="label">Meta description</div><span class="wrong">NO</span>
            </div>
            <div class="content_result">
              <div class="label">Content</div><span class="wrong">NO</span></div>
            </div>
          <div class="clear"></div>
        </div>
      </div>
    </div><br/>
    <div class="desc">
      <table width="100%" border="0" cellspacing="2" cellpadding="2" align=center class="admintable desc_title">
        <tr class="row_title" >
          <td class="font_title" ><img src="{DIR_IMAGE}/toggle_minus.png" alt="bt_add" title="Collapse" align="absmiddle"/> Xem Demo tương tác FaceBook : </td>
        </tr>
      </table>
      <div class="desc_content" style="background: #ffffff; padding: 10px;">
        <div class="divFacebook"> {data.img_mxh}
          <div class="face-info">
            <div class="title_mxh" id="title_mxh" >{data.friendly_title}</div>
            <div class="link_mxh" id="link_mxh" >{data.link_mxh}</div>
            <div class="description_mxh" id="description_mxh" >{data.metadesc}</div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td class="row0" align="center" height="50" >
      <input type="hidden" name="do_submit"	 value="1" />
      <input type="submit" name="btnAdd" value="{LANG.btn_submit}" class="button">&nbsp;
      <input type="reset" name="btnReset" value="{LANG.btn_reset}" class="button">&nbsp;
    </td>
  </tr>
</table>
</form>
<!-- END: edit -->

<!-- BEGIN: manage -->
<div class="box-search">
<form action="{data.link_fsearch}" method="post" name="fSearch">
<table width="100%" border="0" cellspacing="2" cellpadding="2" align="center" class="tableborder">
  <tr>
    <td><strong>{LANG.category} : </strong> </td>
    <td>{data.listcat} </td>
  </tr>
  <tr>
    <td class="row1"><strong>{LANG.view_day_from}:</strong>  </td>
    <td align="left" class="row0">
      <input type="text" name="date_begin" id="date_begin" value="{data.date_begin}" size="15" maxlength="10" class="form-control datepicker"/> &nbsp;&nbsp; <strong>{LANG.view_day_to} :</strong> <input type="text" name="date_end" id="date_end" value="{data.date_end}" size="15" maxlength="10"   class="form-control datepicker"   />
    </td>
  </tr>
  <tr>
  <tr>
  	<td class="row1" ><strong>{LANG.search} :</strong>   </td>
    <td class="row0" >{data.list_search} <strong> {LANG.keyword} :</strong>  <input name="keyword" value="{data.keyword}" size="20" type="text" class="form-control" />  &nbsp;<input type="submit" name="btnGo" value=" Search " class="button"></td>
  </tr>
  <tr>
    <td width="15%" align="left"><strong>{LANG.totals}:</strong> &nbsp;</td>
    <td width="85%" align="left">
      <b class="font_err">{data.totals}</b>  &nbsp; | &nbsp;<a href="{data.link_excel}" target="_blank"><img src="{DIR_IMAGE}/icon_excel.gif"  alt="excel" align="absmiddle"/> <b>Xuất ra file Excel theo điều kiện Search</b> </a>
      &nbsp; | &nbsp;<a href="{data.link_excel_sub}" target="_blank"><img src="{DIR_IMAGE}/icon_excel.gif"  alt="excel" align="absmiddle"/> <b>Xuất Excel SP con theo điều kiện Search</b> </a>
    </td>
  </tr>
</table>
</form>
</div>
{data.err}
<br />
{data.table_list}
<br />
<table width="100%"  border="0" align="center" cellspacing="1" cellpadding="1" class="bg_tab">
  <tr>
    <td  height="25">{data.nav}</td>
  </tr>
</table>
<br />
<!-- END: manage -->
