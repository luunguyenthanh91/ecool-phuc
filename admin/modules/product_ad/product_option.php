<?php
/*================================================================================*\
|| 							Name code : comment.php 		 			                 							# ||
||  				Copyright © 2007 by Thai Son - CMS vnTRUST                          # ||
\*================================================================================*/
/**
* @version : 1.0
* @date upgrade : 11/12/2007 by Thai Son
**/

if (!defined('IN_vnT')){
     die('Hacking attempt!');
}

$act=new sMain($sub);
class sMain{
var $output="";
var $skin="";
var $linkUrl = "";

function sMain($sub){
global $vnT,$func,$DB,$conf;
	//load lang
	$func->load_language("product");
	include ("function_product.php");
	require_once("skin_comment.php"); 
	$this->skin=new skin_comment();
	$this->linkUrl ="?mod=product&act=product_option";
	
	$ext = $_GET['ext'];
	if (isset ($_GET['lang'])) $lang = $_GET['lang'] ; else $lang=$func->get_lang_default();
	if (isset ($_GET['id']) && is_numeric($_GET['id'])) $id = $_GET['id'] ; else $id=0;
	if (isset ($_GET['cat_id']) && is_numeric($_GET['cat_id'])) $cat_id = $_GET['cat_id'] ;
	
	if(isset ($_POST['maso'])){
		$res = $DB->query("select p_id,cat_id from products where maso='".$_POST['maso']."' ");
		if($row_ms = $DB->fetch_row($result)){
			$id = $row_ms['p_id'];
			$cat_id = get_catid_option($row_ms['cat_id']);
		}
	}
	
	if (isset($_POST['btnSubmit'])){
		$a_tongquan = $_POST['tongquan'];
		//option tong quan
		$cot1['p_id'] = $id;
		$cot1['option_cat'] = 0;
		foreach ($a_tongquan as $key => $value){
			$a_tongquan_new[$key ] = stripslashes($value);
		}
		$cot1['option_value'] = serialize($a_tongquan_new);
		$res_ck = $DB->query("select p_id from products_options where p_id=$id and option_cat=0 ");
		if ($DB->num_rows($res_ck)){
			
			$DB->do_update ("products_options",$cot1,"p_id=$id  and option_cat=0");
		}else{
			$DB->do_insert ("products_options",$cot1);
		}
		//option khac
		if ($_POST['have_option']){
			$res_cat = $DB->query ("select * from cat_option where cat_id=$cat_id and parentid=0 order by option_order ");
			while ($row_cat = $DB->fetch_row($res_cat)){
				$option_cat = $row_cat['option_id'];
				$a_option = $_POST['txtOption_'.$option_cat.''];	
				$cot_op['p_id'] = $id;
				$cot_op['option_cat'] = $option_cat;
				if (is_array($a_option) ){
					foreach ($a_option as $key => $value){
						$a_option_new[$key ] = stripslashes($value);
					}
				}
				$cot_op['option_value'] = serialize($a_option_new);
				$res_ck = $DB->query("select p_id from products_options where p_id=$id and option_cat=$option_cat ");
				if ($DB->num_rows($res_ck)){
					$DB->do_update ("products_options",$cot_op,"p_id=$id  and option_cat=$option_cat");
				}else{
					$DB->do_insert ("products_options",$cot_op);
					
				}
				
			}
		}
		//clear cache
		$func->clear_cache("html");
		$mess = "C&#7853;p nh&#7853;t option th&#224;nh c&#244;ng";
		$ext_page = str_replace("|","&",$ext);
		$url = "?mod=product&act=product&{$ext_page}";
		flush();
			echo $func->html_redirect($url,$mess);
		exit();
	}
	
	$nd['f_title']= "Qu&#7843;n l&#253; option s&#7843;n ph&#7849;m";
	if(empty($id)){
		$nd['content']=$this->do_Search();
	}else{
		$nd['content']=$this->do_Manage($cat_id,$id,$lang);
	}
	
	$vnT->output.= $vnT->skin_acp->html_table($nd);
}
//=============
function do_Search(){
global $func,$DB,$conf,$vnT;
	$err="";
	if(isset($_POST['btnSearch'])){
		$data = $_POST;
		$err ="Không tìm thấy sản phẩm có mã ".$_POST['maso'];
	}
	$data['err'] = $err;
	$data['link_action'] = $this->linkUrl;
	return $this->html_search($data);

}

//======
function do_Manage($cat_id,$id,$lang){
global $func,$DB,$conf,$vnT,$lang_acp;
$err="";

$ext = $_GET['ext'];

if (empty($id) || empty($cat_id) ) {
	$ext_page = str_replace("|","&",$ext);
	$url = "?mod=product&act=product&{$ext_page}";
	flush();
		echo $func->html_redirect($url,"Vui l&#242;ng ch&#7885;n s&#7843;n ph&#7849;m tr&#432;&#7899;c");
	exit();
}

//tongquan
$res = $DB->query("select option_value from products_options  where p_id=$id and option_cat=0  ");

if ($row = $DB->fetch_row($res)){
	if ($row['option_value']){
		$a_tongquan = unserialize($row['option_value']);
	}
}

//option
$sql = "select * from products_options  where p_id=$id and option_cat<>0  ";
$result = $DB->query($sql);
while ($data = $DB->fetch_row($result)){
	$cat_option = $data['option_cat'];
	if ($data['option_value']){
		$a_option[$cat_option] = unserialize($data['option_value']);
	}
}

$list_tongquan="";
$list_option="";
$sql = "select * from cat_option where cat_id=$cat_id and parentid=0 and is_focus=1 order by option_order ";
$result = $DB->query ($sql);
while ($data = $DB->fetch_row($result)){
	$option_name = $func->HTML($data['option_name']);
	$option_id = $data['option_id'];
	$list_tongquan .="<tr><td width=\"20%\">{$option_name}</td>
        <td><textarea name=\"tongquan[$option_id]\" cols=\"70\" rows=\"1\">".$a_tongquan[$option_id]."</textarea></td> </tr>";
		
	$res = $DB->query ("select * from cat_option where cat_id=$cat_id and parentid=$option_id order by option_order ");
	if ($DB->num_rows($res)){
		$have_option =1;
		$list_option.= '<table width="100%" border="0" cellspacing="0" cellpadding="0">
	  <tr><td  style="border-bottom:2px solid #FF0000" height=25><strong>'.$option_name.'</strong></td></tr><tr><td>
		<table width="100%" border="0" cellspacing="1" cellpadding="1">';
		while ($row = $DB->fetch_row($res)){
			$op_id = $row['option_id'];
			$list_option.="<tr><td width=\"20%\">".$func->HTML($row['option_name'])."</td><td><textarea name=\"txtOption_{$option_id}[$op_id]\" cols=\"70\" rows=\"1\">".$a_option[$option_id][$op_id]."</textarea></td> </tr>";
		}
		$list_option.='</table></td></tr></table><br>';
	}
}

$res_p = $DB->query ("select maso,p_name from products where p_id=$id ");
if ($row_p = $DB->fetch_row($res_p)) {
 $data['maso'] = $row_p['maso'];
 $data['p_name'] = $func->HTML($row_p['p_name']);
}

$data['list_tongquan'] = $list_tongquan;
$data['list_option'] = $list_option;
$data['have_option'] =$have_option;
$data['err'] = $err;
$data['link_action'] = $this->linkUrl."&cat_id=$cat_id&id=$id&ext=".$ext;
return $this->html_manage($data);


}

//=================Skin===================
function html_search($data){
global $func,$DB,$lang_acp;
return<<<EOF
<br>

<form id="form1" name="form1" method="post" action="{$data['link_action']}">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td align="center"> Nhập mã sản phẩm <input name="maso" value="{$data['maso']}" type="text" /></td>
  </tr>
<tr>
    <td align="center" class="font_err">{$data['err']}</td>
  </tr>    
</table>
<br />

<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td align="center">
	<input name="btnSearch" type="submit" value=" Submit " class="button" />
	&nbsp;<input name="reset" type="reset"  value="Reset" class="button"/></td>
  </tr>
</table>

</form>
<br />
EOF;
}


//=================Skin===================
function html_manage($data){
global $func,$DB,$lang_acp;
return<<<EOF
<br>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td  style="border-bottom:1px dashed #CCCCCC;font-size:14px;" height="25"><strong>Mã số:</strong> <strong style="color:#ff0000">{$data['maso']}</strong> </td>
  </tr>
	<tr>
    <td  style="border-bottom:1px dashed #CCCCCC;font-size:14px;" height="25"><strong>S&#7843;n ph&#7849;m :</strong> <strong style="color:#ff0000">{$data['p_name']}</strong> </td>
  </tr>
  
</table>
<br>
<form id="form1" name="form1" method="post" action="{$data['link_action']}">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td  style="border-bottom:2px solid #FF0000" height=25><strong>T&#7892;NG QUAN :</strong></td>
  </tr>
  <tr>
    <td>
    <table width="100%" border="0" cellspacing="1" cellpadding="1">
      {$data['list_tongquan']}
    </table>
	
    
    </td>
  </tr>
</table>
<br />
{$data['list_option']}

<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td align="center">
	<input name="have_option" type="hidden" value="{$data['have_option']}" />
	<input name="btnSubmit" type="submit" value=" Submit " class="button" />
	&nbsp;<input name="reset" type="reset"  value="Reset" class="button"/></td>
  </tr>
</table>

</form>
<br />
EOF;
}


// end 
}
?>