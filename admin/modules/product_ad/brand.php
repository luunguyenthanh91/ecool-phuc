<?php
/*================================================================================*\
|| 							Name code : cat_product.php 		 		            	  ||
||  				Copyright @2008 by Thai Son - CMS vnTRUST                     ||
\*================================================================================*/
/**
 * @version : 2.0
 * @date upgrade : 09/01/2009 by Thai Son
 **/
if (! defined('IN_vnT'))
{
  die('Hacking attempt!');
}

$vntModule = new vntModule();
class vntModule
{
  var $output = "";
  var $skin = "";
  var $linkUrl = "";
  var $module = "product";
  var $action = "brand";
  
  /**
   * function vntModule ()
   * Khoi tao 
   **/
  function vntModule ()
  {
    global $Template, $vnT, $func, $DB, $conf;
    require_once ("function_" . $this->module . ".php");
    $this->skin = new XiTemplate(DIR_MODULE . DS . $this->module."_ad" . DS .'html'. DS . $this->action.".tpl");
    $this->skin->assign('LANG', $vnT->lang);
    
    $lang = ($vnT->input['lang']) ? $lang = $vnT->input['lang'] : $func->get_lang_default();
    $this->linkUrl = "?mod=" . $this->module . "&act=" . $this->action . "&lang=" . $lang;
    
     switch ($vnT->input['sub'])
    {
      case 'add':
        $nd['f_title'] = $vnT->lang['add_brand'];
        $nd['content'] = $this->do_Add($lang);
        break;
      case 'edit':
        $nd['f_title'] = $vnT->lang['edit_brand'];
        $nd['content'] = $this->do_Edit($lang);
        break;
      case 'del':
        $this->do_Del($lang);
        break;
      default:
        $nd['f_title'] = $vnT->lang['manage_brand'];
        $nd['content'] = $this->do_Manage($lang);
        break;
    }
    $nd['menu'] = $func->getToolbar($this->module, $this->action, $lang);
    $nd['row_lang'] = $func->html_lang("?mod=" . $this->module . "&act=" . $this->action, $lang);
    
    $Template->assign("data", $nd);
    $Template->parse("box_main");
    $vnT->output .= $Template->text("box_main");
  
  }
  
  /**
   * function do_Add 
   * Them  moi 
   **/
  function do_Add ($lang)
  {
    global $vnT, $func, $DB, $conf;
    $err = "";
    
    if ($vnT->input['do_submit'] == 1)
    {
      $data = $_POST;
      
      $title = $vnT->input['title'];
      $parentid = $vnT->input["brand_id"];
      
      // Check for Error
 
      //upload
      if ($vnT->input['chk_upload'] && ! empty($_FILES['image']) && $_FILES['image']['name'] != "")
      {
        $up['path'] = MOD_DIR_UPLOAD;
        $up['dir'] = "brand";
        $up['file'] = $_FILES['image'];
        $up['type'] = "hinh";
        $up['w'] = 500;
        
        $result = $vnT->File->Upload($up);
        if (empty($result['err']))
        {
          $picture = $result['link'];
          $file_type = $result['type'];
        }
        else
        {
          $err = $func->html_err($result['err']);
        }
      }
      else
      {
        if ($vnT->input['picture'])
        {
          $up['path'] = MOD_DIR_UPLOAD;
          $up['dir'] = "brand";
          $up['url'] = $vnT->input['picture'];
          $up['type'] = "hinh";
          $up['w'] = 500;
          
          $result = $vnT->File->UploadURL($up);
          if (empty($result['err']))
          {
            $picture = $result['link'];
            $file_type = $result['type'];
          }
          else
          {
            $err = $func->html_err($result['err']);
          }
        }
      } //end upload
      

      // insert CSDL
      if (empty($err))
      {
        $cot['parentid'] = $parentid;
        $cot['picture'] = $picture; 				
				$cot['date_post'] = time(); 
				
        $ok = $DB->do_insert("product_brand", $cot);
        if ($ok)
        {
          $brand_id = $DB->insertid(); 
					
 					//update content
					$cot_d['brand_id'] = $brand_id;
					$cot_d['title'] = $title ;
					$cot_d['description'] = $func->txt_HTML($_POST['description']); 					 
				
					$cot_d['friendly_url'] = (trim($vnT->input['friendly_url'])) ? trim($vnT->input['friendly_url']) :  $func->make_url($title);
				  $cot_d['friendly_title'] = (trim($vnT->input['friendly_title'])) ? trim($vnT->input['friendly_title']) :  $func->utf8_to_ascii($title);
					$cot_d['metakey'] = (trim($vnT->input['metakey'])) ? trim($vnT->input['metakey']) :  $title ;
					$cot_d['metadesc'] = (trim($vnT->input['metadesc'])) ? trim($vnT->input['metadesc']) :  $func->txt_HTML($_POST['description']) ;
				
					$query_lang = $DB->query("select name from language ");
					while ( $row = $DB->fetch_row($query_lang) ) {
						$cot_d['lang'] = $row['name'];
						$DB->do_insert("product_brand_desc",$cot_d);
					}
					
           //xoa cache				
          $func->clear_cache();
          //insert adminlog
          $func->insertlog("Add", $_GET['act'], $brand_id);
          
          $mess = $vnT->lang['add_success'];
          $url = $this->linkUrl . "&sub=add";
          $func->html_redirect($url, $mess);
        }
        else
        {
          $err = $func->html_err($vnT->lang['add_failt'] . $DB->debug());
        }
      }
    
    }
    
    $data['list_parent'] = List_Brand_Root($vnT->input['brand_id'], $lang, "");
     
    $data['err'] = $err;
    $data['link_action'] = $this->linkUrl . "&sub=add";
    
    /*assign the array to a template variable*/
    $this->skin->assign('data', $data);
    
    $this->skin->parse("edit");
    return $this->skin->text("edit");
  
  }
  
  /**
   * function do_Edit 
   * Cap nhat 
   **/
  function do_Edit ($lang)
  {
    global $vnT, $func, $DB, $conf;
    $id = (int) $vnT->input['id'];
    
    if ($vnT->input['do_submit'])
    {
      $data = $_POST;
      
      $title = $vnT->input['title'];
      $parentid = $vnT->input["brand_id"];
      
      // Check for Error
      if ($parentid == $id) $err = $func->html_err($vnT->lang['err_parentid_invalid']);
      
      //upload
      if ($vnT->input['chk_upload'] && ! empty($_FILES['image']) && $_FILES['image']['name'] != "")
      {
        $up['path'] = MOD_DIR_UPLOAD;
        $up['dir'] = "brand";
        $up['file'] = $_FILES['image'];
        $up['type'] = "hinh";
        $up['w'] = 500;
        
        $result = $vnT->File->Upload($up);
        if (empty($result['err']))
        {
          $picture = $result['link'];
          $file_type = $result['type'];
        }
        else
        {
          $err = $func->html_err($result['err']);
        }
      }
      else
      {
        if ($vnT->input['picture'])
        {
          $up['path'] = MOD_DIR_UPLOAD;
          $up['dir'] = "brand";
          $up['url'] = $vnT->input['picture'];
          $up['type'] = "hinh";
          $up['w'] = 500;
          
          $result = $vnT->File->UploadURL($up);
          if (empty($result['err']))
          {
            $picture = $result['link'];
            $file_type = $result['type'];
          }
          else
          {
            $err = $func->html_err($result['err']);
          }
        }
      } //end upload
      

      if (empty($err))
      {
        $cot['parentid'] = $parentid;
        if ($vnT->input['chk_upload'] == 1 || ! empty($picture))
        {           
          $cot['picture'] = $picture;
        }
				
        $ok = $DB->do_update("product_brand", $cot, "brand_id=$id");
        if ($ok)
        {           
					//update content
					$cot_d['title'] = $title ;
					$cot_d['description'] = $func->txt_HTML($_POST['description']);
					
					$cot_d['friendly_url'] = (trim($vnT->input['friendly_url'])) ? trim($vnT->input['friendly_url']) :  $func->make_url($title);
				  $cot_d['friendly_title'] = (trim($vnT->input['friendly_title'])) ? trim($vnT->input['friendly_title']) :  $func->utf8_to_ascii($title);
					$cot_d['metakey'] = (trim($vnT->input['metakey'])) ? trim($vnT->input['metakey']) :  $title ;
					$cot_d['metadesc'] = (trim($vnT->input['metadesc'])) ? trim($vnT->input['metadesc']) :  $func->txt_HTML($_POST['description']) ;
				
					$DB->do_update ("product_brand_desc",$cot_d,"brand_id=$id and lang='{$lang}'");
					
          //xoa cache
          $func->clear_cache();
          
          //insert adminlog
          $func->insertlog("Edit", $_GET['act'], $id);
          
          $err = $vnT->lang["edit_success"];
          $url = $this->linkUrl . "&sub=edit&id=$id";
          $func->html_redirect($url, $err);
        }
        else
          $err = $func->html_err($vnT->lang["edit_failt"] . $DB->debug());
      }
    
    }
    
    $query = $DB->query("SELECT * FROM product_brand n,  product_brand_desc nd
						WHERE  n.brand_id=nd.brand_id
						AND nd.lang='$lang'	
						AND n.brand_id=$id");
    if ($data = $DB->fetch_row($query))
    {
      if (! empty($data['picture']))
      {
        $data['pic'] = "<img src=\"" . MOD_DIR_UPLOAD . "brand/{$data['picture']}\" ><br>";
      }
    
    }
    else
    {
      $mess = $vnT->lang['not_found'] . " ID : " . $id;
      $url = $this->linkUrl;
      $func->html_redirect($url, $mess);
    }
    
    $data['list_parent'] = List_Brand_Root($data['parentid'], $lang, "");
     
    $data['err'] = $err;
    $data['link_action'] = $this->linkUrl . "&sub=edit&id=$id";
    
    /*assign the array to a template variable*/
    $this->skin->assign('data', $data);
    
    $this->skin->parse("edit");
    return $this->skin->text("edit");
  }
   
  /**
   * function do_Del 
   * Xoa 1 ... n  gioi thieu 
   **/
  function do_Del ($lang)
  {
    global $func, $DB, $conf, $vnT;
    
    $id = (int) $vnT->input['id'];
    $ext = $vnT->input["ext"];
    $del = 0;
    $qr = "";
    
    if ($id != 0)  {
      $ids = $id;
    }
    if (isset($vnT->input["del_id"]))   {
      $ids = implode(',', $vnT->input["del_id"]);
    }
    
    // Del Image
    $query = $DB->query("SELECT picture FROM product_brand WHERE brand_id IN (" . $ids . ") ");
    while ($img = $DB->fetch_row($query))
    {
      $file_pic = MOD_DIR_UPLOAD . "brand/" . $img['picture'];
      if ((file_exists($file_pic)) && (! empty($img['picture']))) unlink($file_pic);
    }
    // End del image
    

    $query = 'DELETE FROM product_brand WHERE brand_id IN (' . $ids . ')';
    if ($ok = $DB->query($query))
    {       
			$DB->query("DELETE FROM product_brand_desc WHERE brand_id IN (".$ids.")"); 
			
 			//xoa cache
      $func->clear_cache();
			
      $mess = $vnT->lang["del_success"];			
    }
    else
      $mess = $vnT->lang["del_failt"];
    $ext_page = str_replace("|", "&", $ext);
    $url = $this->linkUrl . "&{$ext_page}";
    $func->html_redirect($url, $mess);
  }
  
  /**
   * function render_row 
   * list cac record
   **/
  function render_row ($row_info, $lang)
  {
    global $func, $DB, $conf, $vnT;
    $row = $row_info;
    // Xu ly tung ROW
    $id = $row['brand_id'];
    $row_id = "row_" . $id;
    $output['row_id'] = $row_id;
    $output['check_box'] = vnT_HTML::checkbox("del_id[]", $id, 0, " ");
    $link_edit = $this->linkUrl . '&sub=edit&id=' . $id . '&ext=' . $row['ext_page'];
    $link_del = "javascript:del_item('" . $this->linkUrl . "&sub=del&id=" . $id . "&ext=" . $row['ext_page'] . "')";
    
    $output['order'] = $row['ext'] . "<input name=\"txt_Order[{$id}]\" type=\"text\" size=\"2\" maxlength=\"2\" style=\"text-align:center\" value=\"{$row['display_order']}\"  onkeypress=\"return is_num(event,'txtOrder')\" onchange='javascript:do_check($id)' />";
    
    if ($row['picture'])
    {
      $output['picture'] = "<img src=\"" . MOD_DIR_UPLOAD . "brand/" . $row['picture'] . "\" width=50 />";
    }
    else
      $output['picture'] = "No image";
    
    $text_edit = "product_brand_desc|title|id=" . $row['id'];
    $title = "<span id='edit-text-" . $id . "' onClick=\"quick_edit('edit-text-" . $id . "','$text_edit');\">" . $func->HTML($row['title']) . "</span>";
    
    if ($row['ext'])   {
      $output['title'] = $row['ext'] . "&nbsp;" . $title;		
			$output['is_focus'] = '---';			
    }  else   {
      $output['title'] = "<strong>" . $title . "</strong>";
			$output['is_focus'] = vnT_HTML::list_yesno("is_focus[{$id}]", $row['is_focus'], "onchange='javascript:do_check($id)'");
    }		
		$output['link'] = "product/brand/".$row['friendly_url']."-".$row['brand_id'].".html";	
		
    $link_display = $this->linkUrl.$row['ext_link']; 		
    if ($row['display'] == 1) {
      $display = "<a href='".$link_display."&do_hidden=$id' title='".$vnT->lang['click_do_hidden']."' ><img src=\"" . $vnT->dir_images . "/dispay.gif\" width=15  /></a>";
    } else {
      $display = "<a href='".$link_display."&do_display=$id'  title='".$vnT->lang['click_do_display']."' ><img src=\"" . $vnT->dir_images . "/nodispay.gif\"  width=15 /></a>";
    }
    
    $output['action'] = '<input name=h_id[]" type="hidden" value="' . $id . '" />';
    $output['action'] .= '<a href="' . $link_edit . '"><img src="' . $vnT->dir_images . '/edit.gif"  alt="Edit "></a>&nbsp;';
    $output['action'] .= $display . '&nbsp;';
    $output['action'] .= '<a href="' . $link_del . '"><img src="' . $vnT->dir_images . '/delete.gif"  alt="Delete "></a>';
    return $output;
  }
  
  /**
   * function do_Manage() 
   * Quan ly 
   **/
  function do_Manage ($lang)
  {
    global $vnT, $func, $DB, $conf;
    
    //update
    if ($vnT->input["do_action"])
    {
      //xoa cache
      $func->clear_cache();
      if ($vnT->input["del_id"]) $h_id = $vnT->input["del_id"];
      switch ($vnT->input["do_action"])
      {
        case "do_edit":
          if (isset($vnT->input["txt_Order"])) $arr_order = $vnT->input["txt_Order"]; 
          $mess .= "- " . $vnT->lang['edit_success'] . " ID: <strong>";
          $str_mess = "";
          for ($i = 0; $i < count($h_id); $i ++)
          {
            $dup['display_order'] = $arr_order[$h_id[$i]]; 
            $ok = $DB->do_update("product_brand", $dup, "brand_id=" . $h_id[$i]);
            if ($ok)
            {
              $str_mess .= $h_id[$i] . ", ";
            }
          }
          $mess .= substr($str_mess, 0, - 2) . "</strong><br>";
          $err = $func->html_mess($mess);
          break;
        case "do_hidden":
          $mess .= "- " . $vnT->lang['hidden_success'] . " ID: <strong>";
          for ($i = 0; $i < count($h_id); $i ++)
          {
            $dup['display'] = 0;
            $ok = $DB->do_update("product_brand", $dup, "brand_id=" . $h_id[$i]);
            if ($ok)
            {
              $str_mess .= $h_id[$i] . ", ";
            }
          }
          $mess .= substr($str_mess, 0, - 2) . "</strong><br>";
          $err = $func->html_mess($mess);
          
          break;
        case "do_display":
          $mess .= "- " . $vnT->lang['display_success'] . "  ID: <strong>";
          for ($i = 0; $i < count($h_id); $i ++)
          {
            $dup['display'] = 1;
            $ok = $DB->do_update("product_brand", $dup, "brand_id=" . $h_id[$i]);
            if ($ok)
            {
              $str_mess .= $h_id[$i] . ", ";
            }
          }
          $mess .= substr($str_mess, 0, - 2) . "</strong><br>";
          $err = $func->html_mess($mess);
          break;
      }
    }
		
		if((int)$vnT->input["do_display"]) {				
			$ok = $DB->query("Update product_brand SET display=1 WHERE brand_id=".$vnT->input["do_display"]);
			if($ok){
				$mess .= "- " . $vnT->lang['display_success'] . "  ID: <strong>".$vnT->input["do_display"] . "</strong><br>";	
				$err = $func->html_mess($mess);
			}        
			//xoa cache
      $func->clear_cache();
		}
		if((int)$vnT->input["do_hidden"]) {				
			$ok = $DB->query("Update product_brand SET display=0 WHERE brand_id=".$vnT->input["do_hidden"]);
			if($ok){
				$mess .= "- " . $vnT->lang['display_success'] . "  ID: <strong>".$vnT->input["do_hidden"] . "</strong><br>";	
				$err = $func->html_mess($mess);
			}    
			//xoa cache
      $func->clear_cache();
		}
		
    
    $p = ((int) $vnT->input['p']) ? $p = $vnT->input['p'] : 1;
    $n = ($conf['record']) ? $conf['record'] : 30;
    
    $query = $DB->query("SELECT  n.brand_id FROM product_brand n,  product_brand_desc nd
													WHERE  n.brand_id=nd.brand_id
													AND nd.lang='$lang'  
													AND n.parentid=0 
													{$where} ");
    $totals = intval($DB->num_rows($query));
    
    $num_pages = ceil($totals / $n);
    if ($p > $num_pages) $p = $num_pages;
    if ($p < 1) $p = 1;
    $start = ($p - 1) * $n;
    
    $nav = $func->paginate($totals, $n, $ext, $p);
    
    $data['link_action'] = $this->linkUrl . "&p=$p"; 
     
    $sql = "SELECT * FROM product_brand n,  product_brand_desc nd
						WHERE  n.brand_id=nd.brand_id
						AND nd.lang='$lang' 
						AND n.parentid=0 
						{$where} 
					  ORDER BY  n.display_order ASC, n.brand_id ASC  
						LIMIT $start,$n";
    //print "sql = ".$sql."<br>";
    $result = $DB->query($sql);
    if ($DB->num_rows($result))
    {
      $i = 0;
      while ($row = $DB->fetch_row($result))
      {
        $i ++;
        $row['ext'] = "";
				$row['ext_link'] = "&p=".$p ;
        $row_info = $this->render_row($row, $lang);
        $row_info['class'] = ($i % 2) ? "row1" : "row0";
        $this->skin->assign('row', $row_info);
        $this->skin->parse("manage.html_row");
        $n = 1;
        $this->Row_Sub($row['brand_id'], $n, &$i, $lang);
      }
    }
    else
    {
      $mess = $vnT->lang['no_have_cat_product'];
      $this->skin->assign('mess', $mess);
      $this->skin->parse("manage.html_row_no");
    }
    
    $data['button'] = '<input type="button" name="btnHidden" value=" ' . $vnT->lang['hidden'] . ' " class="button" onclick="do_submit(\'do_hidden\')">&nbsp;';
    $data['button'] .= '<input type="button" name="btnDisplay" value=" ' . $vnT->lang['display'] . ' " class="button" onclick="do_submit(\'do_display\')">&nbsp;';
    $data['button'] .= '<input type="button" name="btnEdit" value=" ' . $vnT->lang['update'] . ' " class="button" onclick="do_submit(\'do_edit\')">&nbsp;';
    $data['button'] .= '<input type="button" name="btnDel" value=" ' . $vnT->lang['delete'] . ' " class="button" onclick="del_selected(\'' . $this->linkUrl . '&sub=del&ext=' . $ext_page . '\')">';
    
    $data['totals'] = $totals;
    $data['err'] = $err;
    $data['nav'] = $nav;
    
    /*assign the array to a template variable*/
    $this->skin->assign('data', $data);
    
    $this->skin->parse("manage");
    return $this->skin->text("manage");
  }
  
  /**
   * function Row_Sub() 
   * 
   **/
  function Row_Sub ($cid, $n, $i, $lang)
  {
    global $func, $DB, $conf, $vnT;
    $textout = "";
    $space = "&nbsp;&nbsp;&nbsp;&nbsp;";
    $n1 = $n;
    $sql = "SELECT * FROM product_brand n,  product_brand_desc nd
						WHERE  n.brand_id=nd.brand_id
						AND nd.lang='$lang' 
						AND n.parentid='{$cid}'
						ORDER BY  n.display_order ASC,  n.brand_id DESC ";
    //	print "sql = ".$sql."<br>";
    $result = $DB->query($sql);
    while ($row = $DB->fetch_row($result))
    {
      $i ++;
			$row['ext_link'] = "&p=".$_GET['p'] ;
      $row['ext'] = "&nbsp;<img src=\"{$vnT->dir_images}/line3.gif\" align=\"absmiddle\"/>";
			$width="";
      for ($k = 1; $k < $n1; $k ++)
      {
        $width.=$space;
        $row['ext'] = $width . "&nbsp;<img src=\"{$vnT->dir_images}/line3.gif\" align=\"absmiddle\"/>";
      }
      
      $row_info = $this->render_row($row, $lang);
      $row_info['class'] = ($i % 2) ? "row1" : "row0";
      $this->skin->assign('row', $row_info);
      $this->skin->parse("manage.html_row");
      
      $n = $n1 + 1;
      $textout .= $this->Row_Sub($row['brand_id'], $n, $i, $lang);
    }
    
    return $textout;
  }
  
// end class
}

?>
