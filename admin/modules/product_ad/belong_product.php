<?php
if (! defined('IN_vnT')){
  die('Hacking attempt!');
}
//load Model
include(dirname(__FILE__) . "/functions.php");

class vntModule extends Model
{
  var $output = "";
  var $skin = "";
  var $linkUrl = "";
  var $module = "product";
	var $action = "belong_product";

  function vntModule (){
    global $Template, $vnT, $func, $DB, $conf;
    //require_once ("function_".$this->module.".php");
    $this->skin = new XiTemplate(DIR_MODULE . DS . $this->module."_ad" . DS .'html'. DS . $this->action.".tpl");
    $this->skin->assign('LANG', $vnT->lang);    
    $lang = ($vnT->input['lang']) ? $lang = $vnT->input['lang'] : $func->get_lang_default();		
		$cat_id = ((int) $vnT->input['cat_id']) ? $cat_id = $vnT->input['cat_id'] : 0;		
    $this->linkUrl = "?mod=".$this->module."&act=".$this->action."&cat_id=$cat_id&lang=" . $lang;		
    switch ($vnT->input['sub']){
      case 'edit' :
				$nd['f_title']= "Cập nhật sản phẩm liên quan";
				$nd['content']=$this->do_Edit($lang);
				break;
			case 'del' : $this->do_Del($lang); break;
			case 'del_all' : $this->do_Del_All($lang); break;
			default :
				$nd['f_title']= "Danh sách các sản phẩm ";
				$nd['content']=$this->do_Manage($lang);
				break;
    }
    $nd['menu'] = $func->getToolbar_Small($this->module, $this->action."&cat_id=$cat_id", $lang);
    $nd['row_lang'] = $func->html_lang("?mod=".$this->module."&act=".$this->action."&cat_id=$cat_id", $lang);
    $Template->assign("data", $nd);
    $Template->parse("box_main");
    $vnT->output .= $Template->text("box_main");
  }
  /**
	* function do_Del 
	* Xoa 1 ... n  gioi thieu 
	**/
  function do_Del ($lang)
  {
    global $func, $DB, $conf, $vnT;
		$pid = (int) $vnT->input['pid'];
		$cat_id = (int) $vnT->input['cat_id'];
		$arr_belong_new = array();	
		$res = $DB->query("select p_id,belong_product from products where p_id=".$pid);
		if($row = $DB->fetch_row($res))
		{
			$arr_belong	= explode(",",$row['belong_product']);																		 
		}
		$arr_del = $vnT->input["del_focus"];
		foreach ($arr_belong as $value)
		{
			if(!in_array($value,$arr_del))
			{
				$arr_belong_new[] = $value;
			}
		}
		$belong_product = @implode(",",$arr_belong_new);
		$sql = "UPDATE products SET belong_product='$belong_product' WHERE p_id=".$pid ; 
		$url = $this->linkUrl."&sub=edit&id=".$pid;
		if ($ok = $DB->query($sql))
		{			
			$mess = $vnT->lang["del_success"] ;
		} else
			$mess = $vnT->lang["del_failt"] ;
		$func->html_redirect($url, $mess);
	}
	/**
	* function do_Del_All 
	* 
	**/
  function do_Del_All ($lang)
  {
    global $func, $DB, $conf, $vnT;
    $id = (int) $vnT->input['id'];
    $ext = $vnT->input["ext"];
    $del = 0;
    $qr = "";
    if ($id != 0)
    {
      $ids = $id;
    }
    if (isset($vnT->input["del_id"]))
    {
      $ids = implode(',', $vnT->input["del_id"]);
    }
    $query = 'UPDATE products SET belong_product=0 WHERE p_id IN (' . $ids . ')';
    if ($ok = $DB->query($query))
    {
      $mess = $vnT->lang["del_success"];
    }
    else
      $mess = $vnT->lang["del_failt"];
    $ext_page = str_replace("|", "&", $ext);
    $url = $this->linkUrl . "&{$ext_page}";
    $func->html_redirect($url, $mess);
  }

  function do_Edit ($lang){
    global $vnT, $func, $DB, $conf;    
		$id = (int)  $vnT->input['id'];
		$sql = "SELECT p.*, p.maso,pd.p_name FROM products p, products_desc pd
						WHERE p.p_id=pd.p_id AND pd.lang='$lang' AND p.p_id=$id";
		$res_p = $DB->query ($sql);
		if ($row_p = $DB->fetch_row($res_p)){
			$data['pic_pro'] = $this->get_picture($row_p['picture'],80);
			$data['p_id'] = $row_p['p_id'];
			$data['p_name'] = $func->HTML($row_p['p_name']);
			$data['maso'] = $row_p['maso'];
		}
		$data['table_list_focus']  = $this->list_focus($row_p,$lang);
		$data['table_list']  = $this->list_product($row_p,$lang);
    /*assign the array to a template variable*/
    $this->skin->assign('data', $data);    
    $this->skin->parse("html_edit");
    return $this->skin->text("html_edit");
  }
  function render_row_pro ($row_info, $lang){
    global $func, $DB, $conf, $vnT;
    $row = $row_info;
    $id = $row['p_id'];
    $row_id = "row_" . $id;
    $output['row_id'] = $row_id;
		$output['check_box'] = vnT_HTML::checkbox("del_id[]", $id, 0, " ");
    $link_edit = $this->linkUrl . '&sub=edit&id=' . $id . '&ext=' . $row['ext_page'];
		$link_pro = "?mod=product&act=product&sub=edit&id={$id}&lang=$lang";
		if ($row['picture']){
			$output['picture'] = $this->get_picture($row['picture'],50);
		}else $output['picture'] ="No image";
		$output['p_name'] = "Mã số : <b class=font_err>".$row['maso']."</b><br><a href=\"{$link_pro}\">".$func->HTML($row['p_name'])."</a>";
		$output['status'] = $this->get_status_name($row['status'],$lang);
		$output['cat_name'] = $this->get_cat_name($row['cat_id'],$lang);
		
		if ($row['display']==1){
			$display ="<img src=\"{$vnT->dir_images}/dispay.gif\" width=15  />" ;
		}else{
			$display ="<img src=\"{$vnT->dir_images}/nodispay.gif\"  width=15 />" ;
		}
		
    $output['action'] = '<input name=h_id[]" type="hidden" value="' . $id . '" />';    
    $output['action'] .= '<a href="' . $link_edit . '"><img src="' . $vnT->dir_images . '/edit.gif"  alt="Edit "></a>&nbsp;';
		$output['action'] .= $display.'&nbsp;';

		return $output;
  }
	/**
	* function list_product 
	*  
	**/
	function list_product ($info,$lang)
	{
		global $func,$DB,$conf,$vnT;
		$pid = (int)  $info['p_id'];
		//update
    if ($vnT->input["do_action"] && $vnT->input["do_action"]=='set_belong' )
    {
      //xoa cache
      $func->clear_cache();
      if ($vnT->input["del_id"]) $h_id = $vnT->input["del_id"];
			$mess .= "Sét thành công SP ID: <strong>";
			$str_mess = "";
			$arr_belong = ($info['belong_product']) ? explode(",",$info['belong_product']) : array();
			for ($i = 0; $i < count($h_id); $i ++)
			{
				if(!in_array($h_id[$i],$arr_belong))
				{
					$arr_belong[] = $h_id[$i] ;
					$str_mess .= $h_id[$i] . ", ";	
				}	
			}
			$belong_product = @implode(",",$arr_belong);
			//echo $belong_product;
			$ok = $DB->query("UPDATE products SET belong_product='$belong_product' WHERE p_id=$pid ");
			if($ok)
			{
				$mess .= substr($str_mess, 0, - 2) . "</strong><br>";
			}	
			//insert log
			$func->insertlog("Set belong",$_GET['act'],$str_mess);
			$url = $this->linkUrl . "&sub=edit&id=".$pid;
			$func->html_redirect($url, $mess);
    }
	  $p = ((int) $vnT->input['p']) ? $p = $vnT->input['p'] : 1;
		$n = ($conf['record']) ? $conf['record'] : 30;
		$cat_id = ((int) $vnT->input['cat_id']) ? $cat_id = $vnT->input['cat_id'] : 0;
		$search = ($vnT->input['search']) ? $search = $vnT->input['search'] : "p_id";
		$keyword = ($vnT->input['keyword']) ? $keyword = $vnT->input['keyword'] : "";
	  $where ="";
		if($info['belong_product'])
		{
			$where .= " AND n.p_id not in ($pid,".$info['belong_product'].") ";
		}else{
			$where .= " AND n.p_id not in ($pid) ";
		}
		if(!empty($cat_id)){
			$a_cat_id = $this->List_SubCat($cat_id);
			$a_cat_id  = substr($a_cat_id,0,-1);
			if (empty($a_cat_id))
				$where .=" and FIND_IN_SET('$cat_id',cat_id)<>0 ";
			else{
				$tmp = explode(",",$a_cat_id);
				$str_= " FIND_IN_SET('$cat_id',cat_id)<>0 ";	
				for ($i=0;$i<count($tmp);$i++){
					$str_ .=" or FIND_IN_SET('$tmp[$i]',cat_id)<>0 ";
				}
				$where .=" and (".$str_.") ";
			} 
			$ext_page .="cat_id=$cat_id|";	
			$ext.="&cat_id=$cat_id";
		}
		if(!empty($search)){
			$ext_page.="search=$search|";
			$ext.="&search={$search}";
		}
		if(!empty($keyword)){
			switch($search){
				case "p_id" : $where .=" and p.p_id = $keyword "; break;
				case "date_post" : $where .=" and DATE_FORMAT(FROM_UNIXTIME(date_post),'%d/%m/%Y') = '{$keyword}' "; break;
				default :$where .=" and $search like '%$keyword%' ";break;		
			}
			$ext_page.="keyword=$keyword|";
			$ext.="&keyword={$keyword}";
		}
		$query =$DB->query("SELECT n.*, nd.p_name, nd.lang 
												FROM products n, products_desc nd 
												WHERE n.p_id=nd.p_id 
												AND nd.lang='$lang'							
												$where ");
		$totals = $DB->num_rows($query);
		$n=20;
		$num_pages = ceil($totals/$n) ;
		if ($p > $num_pages) $p=$num_pages;
		if ($p < 1 ) $p=1;
		$start = ($p-1) * $n;
		$nav = $func->paginate($totals, $n, $ext, $p);
		$table['link_action'] = $this->linkUrl."&sub=edit&id=$pid".$ext;
		$table['title'] = array (
					 'check_box' => "<input type=\"checkbox\" name=\"checkall\" id=\"checkall\" class=\"checkbox\" />|5%|center",
					 'picture' => $vnT->lang['picture']."|10%|center",
					 'p_name' => $vnT->lang['product_name']."|40%|left",
					 'status' => $vnT->lang['status']."|15%|center",
					 'cat_name' => $vnT->lang['category']."|20%|center",
					 );
		$sql = "SELECT n.*, nd.p_name, nd.lang
						FROM products n, products_desc nd
						WHERE n.p_id=nd.p_id
						AND nd.lang='$lang'
						$where
						ORDER BY date_post DESC, n.p_id DESC LIMIT $start,$n";
		//print "sql = ".$sql."<br>";
		$reuslt = $DB->query($sql);
		if ($DB->num_rows ($reuslt)){
		 	$row = $DB->get_array($result);
		 	for ($i=0;$i<count($row);$i++) {
				$row_info = $this->render_row_pro($row[$i],$lang);
				$row_field[$i]=$row_info ;
				$row_field[$i]['stt'] = ($i+1);
				$row_field[$i]['row_id'] = "row_".$row[$i]['p_id'];
				$row_field[$i]['ext'] ="";
			}
			$table['row'] = $row_field;
		}else{
			$table['row']=array();
			$table['extra']="<div align=center class=font_err >".$vnT->lang['no_have_product']."</div>";
		}
		$table['button'] = "&nbsp;&nbsp;<input type=\"button\" name=\"btnEdit\" value=\"Sét sản phẩm liên quan\" class=\"button\" onclick=\"javascript:do_submit('set_belong')\">";
		$data['table_list'] = $func->ShowTable($table);
		$data['listcat'] = $this->Get_Cat($cat_id,$lang,"onChange='submit()'");
		$data['list_search'] = $this->List_Search($search);
		$data['keyword']=$keyword;
		$data['link_fsearch'] = "";
		$data['nav']=$nav;
		$data['err']=$err;
		$data['f_title'] = 'DANH SÁCH CÁC SẢN PHẨM';
		/*assign the array to a template variable*/
		$this->skin->assign('data', $data);				
		$this->skin->parse("list_product");		
		return $this->skin->text("list_product");
		}
	/**
	* function render_row_focus 
	*  
	**/
	function render_row_focus($row_info,$lang) 
	{
		global $func,$DB,$conf,$vnT;
		$row=$row_info;
	// Xu ly tung ROW
    $id = $row['p_id'];
    $row_id = "rowfocus_" . $id;
    $output['row_id'] = $row_id;
		$output['check_box'] = vnT_HTML::checkbox("del_id[]", $id, 0, " ");
		$link_pro = "?mod=product&act=product&sub=edit&id={$id}&lang=$lang";
		$output['check_box'] = "<input type=\"checkbox\" name=\"del_focus[]\" value=\"{$id}\" class=\"checkbox\" onclick=\"select_row('{$row_id}')\">";
		if ($row['picture']){
			$output['picture'] = $this->get_picture($row['picture'],50);
		}else $output['picture'] ="No image";
		$output['price'] = $this->get_price_format($row['price']);
		$output['status'] = $this->get_status_name($row['status'],$lang);
		$output['p_name'] = "Mã số : <b class=font_err>".$row['maso']."</b><br><a href=\"{$link_pro}\">".$func->HTML($row['p_name'])."</a>";
		$output['cat_name'] = $this->get_cat_name($row['cat_id'],$lang);
		return $output;
	}
	
	function list_focus($info,$lang){
		global $func,$DB,$conf,$vnT;
		$pid = (int)  $info['p_id'];		
		if ((isset($_GET['p1'])) && (is_numeric($_GET['p1']))) $p1=$_GET['p1']; else $p1=1;
		$cat_id = ((int) $vnT->input['cat_id']) ? $cat_id = $vnT->input['cat_id'] : 0;
		$where = "";		
		$query = $DB->query("SELECT n.*, nd.p_name, nd.lang 
						FROM products n, products_desc nd 
						WHERE n.p_id=nd.p_id 
						AND nd.lang='$lang'
						AND n.p_id in (".$info['belong_product'].") 
						$where ");
		$totals = $DB->num_rows($query);
		$n=5;
		$num_pages = ceil($totals/$n) ;
		if ($p1 > $num_pages) $p=$num_pages;
		if ($p1 < 1 ) $p1=1;
		$start = ($p1-1) * $n ; 
		
		$nav = "<div align=left>&nbsp;<span class=\"pagelink\">{$num_pages} Page(s) :</span>&nbsp;";
		for ($i=1; $i<$num_pages+1; $i++ ) {
			if ($i==$p1) $nav.="<span class=\"pagecur\">{$i}</span>&nbsp";
			else $nav.="<a href='".$this->linkUrl."&sub=edit&id=$pid&p1={$i}'><span class=\"pagelink\">{$i}</span></a>&nbsp ";
		} 
		$nav .= "</div>";
		$html_row = "";
		$sql .= " SELECT n.*, nd.p_name, nd.lang FROM products n, products_desc nd 
							WHERE n.p_id=nd.p_id AND nd.lang='$lang' AND n.p_id in (".$info['belong_product'].") $where 
							ORDER BY n.date_post , n.p_id DESC limit $start,$n";
		$result = $DB->query($sql);	
		if ($DB->num_rows($result)){
			while ($row=$DB->fetch_row($result)){
				$row['ext'] = "";
				$row['cid'] = $cat_id;
				$row_info = $this->render_row_focus($row,$lang);
				$this->skin->assign('row', $row_info);			
				$this->skin->parse("list_focus.row_focus");
			}
		}else{
			$this->skin->assign('mess', $vnT->lang['no_have_focus']);
			$this->skin->parse("list_focus.row_focus_no");
		}
		$data['html_row'] = $html_row ;	
		$data['totals'] = $totals;
		$data['nav'] = $nav;
		$data['err'] = $err;
		$data['link_del']=$this->linkUrl."&sub=del&pid=".$pid;
		$data['link_action'] = $this->linkUrl;
		$data['f_title'] = $f_title;
		//$text_out .= $this->skin->html_list_focus($data);
		/*assign the array to a template variable*/
		$this->skin->assign('data', $data);			
		$this->skin->parse("list_focus");
		$text_out .= $this->skin->text("list_focus");		
		return $text_out;
	}
  function render_row ($row_info, $lang){
    global $func, $DB, $conf, $vnT;
    $row = $row_info;
    // Xu ly tung ROW
    $id = $row['p_id'];
    $carRoot = $this->get_cat_root($row['cat_id']);
    $row_id = "row_" . $id;
    $output['check_box'] = vnT_HTML::checkbox("del_id[]", $id, 0, " ");
		$link_add = $this->linkUrl."&sub=edit&id={$id}";
		$link_del = "javascript:del_item('".$this->linkUrl."&sub=del_all&id={$id}&ext={$row['ext_page']}')";
    if ($row['picture']){
      $output['picture'] = $this->get_picture($row['picture'], 50);
    }
    else
      $output['picture'] = "No image";
    $output['p_name'] = "MS: <b class=font_err>{$row['maso']}</b> (ID: #".$row['p_id'].")";  
		$output['p_name'] .="<p><a href=\"?mod=product&act=product&sub=edit&lang=$lang&id=".$row['p_id']."\"><strong>".$func->HTML($row['p_name'])."</strong></a></p>";		
		$belong_product ='<table border="0" cellspacing="2" cellpadding="2"><tr>';
		$res = $DB->query("SELECT p.* ,pd.p_name
											FROM products p, products_desc pd
											WHERE p.p_id=pd.p_id 
											AND pd.lang='$lang'
											AND p.p_id in (".$row['belong_product'].")											
											ORDER BY p.date_post DESC, p.p_id DESC");
		if($num = $DB->num_rows($res)){
			$j=0;
			while ($r = $DB->fetch_row($res)){
				$j++;
				$link_pro = '?mod=product&act=product&sub=edit&id='.$r['p_id'].'&lang='.$lang;
				$belong_product.="<td align='center' style='border:1px solid #ccc;padding:2px;'><span style='font-size:10px;'>Serial: <b class=font_err>".$r['maso']."</b><table border=0 cellspacing=0 cellpadding=0>
					<tr>
						<td class='img_belong'><a href='{$link_pro}'>".$this->get_picture($r['picture'],40)."</a></td>
					</tr>
				</table></td>";
				if ($j%5==0) { $belong_product .= "</tr><tr>"; }
			}
		}else{
			$belong_product.='<td align="center">Chưa có trang sức liên quan nào</td>';
		}
		$belong_product.='</tr></table>';
		$output['belong_product'] =$belong_product;
    $output['action'] = '<input name=h_id[]" type="hidden" value="' . $id . '" />';
    $output['action'] .= '<a href="'.$link_add.'"><img src="'.$vnT->dir_images.'/edit.gif" alt="Add"></a>&nbsp;';
    $output['action'] .= '<a href="'.$link_del.'"><img src="'.$vnT->dir_images.'/delete.gif" alt="Delete "></a>';
    return $output;
  }

  function do_Manage ($lang){
    global $vnT, $func, $DB, $conf;
    $p = ((int) $vnT->input['p']) ? $p = $vnT->input['p'] : 1;
    $n = ($conf['record']) ? $conf['record'] : 30;
    $cat_id = ((int) $vnT->input['cat_id']) ? $cat_id = $vnT->input['cat_id'] : 0;
		$search = ($vnT->input['search']) ? $search = $vnT->input['search'] : "p_id";
    $keyword = ($vnT->input['keyword']) ? $keyword = $vnT->input['keyword'] : "";
    $where = " ";
    if (! empty($cat_id)){
      $a_cat_id = List_SubCat($cat_id);
      $a_cat_id = substr($a_cat_id, 0, - 1);
      if (empty($a_cat_id))
        $where .= " and FIND_IN_SET('$cat_id',cat_id)<>0 ";
      else{
        $tmp = explode(",", $a_cat_id);
        $str_ = " FIND_IN_SET('$cat_id',cat_id)<>0 ";
        for ($i = 0; $i < count($tmp); $i ++){
          $str_ .= " or FIND_IN_SET('$tmp[$i]',cat_id)<>0 ";
        }
        $where .= " and (" . $str_ . ") ";
      }
      $ext_page .= "cat_id=$cat_id|";
      $ext .= "&cat_id=$cat_id";
    }
    if (! empty($search)){
      $ext_page .= "search=$search|";
      $ext .= "&search={$search}";
    }
    if (! empty($keyword)){
      switch ($search){
        case "p_id":
        	$where .= " and p.p_id = $keyword ";
        	break;
        case "date_post":
        	$where .= " and DATE_FORMAT(FROM_UNIXTIME(date_post),'%d/%m/%Y') = '{$keyword}' ";
        	break;
        default:
        	$where .= " and $search like '%$keyword%' ";
        	break;
      }
      $ext_page .= "keyword=$keyword|";
      $ext .= "&keyword={$keyword}";
    }
    $query = $DB->query("SELECT p.p_id FROM products p, products_desc pd
												WHERE p.p_id=pd.p_id AND pd.lang='$lang' $where ");
    $totals = intval($DB->num_rows($query));
    $num_pages = ceil($totals / $n);
    if ($p > $num_pages) $p = $num_pages;
    if ($p < 1) $p = 1;
    $start = ($p - 1) * $n;
    
    $nav = $func->paginate($totals, $n, $ext, $p);
    
    $table['link_action'] = $this->linkUrl . "$ext";
    $table['title'] = array(
      'check_box' => "<input type=\"checkbox\" name=\"checkall\" id=\"checkall\" class=\"checkbox\" />|5%|center" , 
      'picture' => "H&#236;nh|10%|center" , 
      'p_name' => $vnT->lang['product_name'] . "|35%|left" , 
      'belong_product' => "Các trang sức liên quan|50%|center",
      'action' => "Action|10%|center"
    );
    $sql = "SELECT * FROM products p, products_desc pd
						WHERE p.p_id=pd.p_id AND pd.lang='$lang' $where
						ORDER BY p.date_post DESC, p.p_id DESC LIMIT $start,$n";
    $reuslt = $DB->query($sql);
    if ($DB->num_rows($reuslt)){
      $row = $DB->get_array($result);
      for ($i = 0; $i < count($row); $i ++){
        $row_info = $this->render_row($row[$i], $lang);
        $row_field[$i] = $row_info;
        $row_field[$i]['stt'] = ($i + 1);
        $row_field[$i]['row_id'] = "row_" . $row[$i]['p_id'];
        $row_field[$i]['ext'] = "";
      }
      $table['row'] = $row_field;
    } else {
      $table['row'] = array();
      $table['extra'] = "<div align=center class=font_err >" . $vnT->lang['no_have_product'] . "</div>";
    }
    $table['button'] .= '<input type="button" name="btnDel" value=" Xóa danh sách liên quan" class="button" onclick="del_selected(\'' . $this->linkUrl . '&sub=del_all&ext=' . $ext_page . '\')">';
    $table_list = $func->ShowTable($table);
    $data['table_list'] = $table_list;
    $data['totals'] = $totals;
    $data['listcat'] = $this->Get_Cat($cat_id, $lang);
		$data['list_search'] = $this->List_Search($search);
    $data['keyword'] = $keyword;
    $data['err'] = $err;
    $data['nav'] = $nav;
    $this->skin->assign('data', $data);
    $this->skin->parse("manage");
    return $this->skin->text("manage");
  }
}
$vntModule = new vntModule();
?>
