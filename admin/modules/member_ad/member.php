<?php
/*================================================================================*\
|| 							Name code : member.php 		 		            	  ||
||  				Copyright @2008 by Thai Son - CMS vnTRUST                     ||
\*================================================================================*/
/**
 * @version : 2.0
 * @date upgrade : 09/01/2009 by Thai Son
 **/
if (! defined('IN_vnT')) {
  die('Hacking attempt!');
}
$vntModule = new vntModule();

class vntModule
{
  var $output = "";
  var $skin = "";
  var $linkUrl = "";
  var $module = "member";
  var $action = "member";

  function vntModule (){
    global $Template, $vnT, $func, $DB, $conf;
    require_once ("function_" . $this->module . ".php");
		loadSetting();
    $this->skin = new XiTemplate(DIR_MODULE.DS. $this->module . "_ad" . DS . "html" . DS . $this->action . ".tpl");
    $this->skin->assign('LANG', $vnT->lang);
    $this->skin->assign('DIR_IMAGE', $vnT->dir_images);
    $vnT->html->addScript("modules/" . $this->module . "_ad/js/" . $this->module . ".js");
    $lang = ($vnT->input['lang']) ? $lang = $vnT->input['lang'] : $func->get_lang_default();
    $this->linkUrl = "?mod=" . $this->module . "&act=" . $this->action . "&lang=" . $lang;
    switch ($vnT->input['sub']) {
      case 'add':
        $nd['f_title'] = $vnT->lang['add_member'];
        $nd['content'] = $this->do_Add($lang);
      break;
      case 'edit':
        $nd['f_title'] = $vnT->lang['edit_member'];
        $nd['content'] = $this->do_Edit($lang);
      break;
      case 'send_mail':
        $nd['f_title'] = "Gừi email cho thành viên";
        $nd['content'] = $this->do_Send_Mail($lang);
      break;
      case 'del':
        $this->do_Del($lang);
      break;
      case 'ban':
        $this->do_Ban($lang);
      break;
      case 'unban':
        $this->do_UnBan($lang);
      break;
      case 'active':
        $this->do_Active($lang);
      break;
      default:
        $nd['f_title'] = $vnT->lang['manage_member'];
        $nd['content'] = $this->do_Manage($lang);
      break;
    }
    $nd['menu'] = $func->getToolbar($this->module, $this->action, $lang);
    $nd['row_lang'] = $func->html_lang("?mod=" . $this->module . "&act=" . $this->action, $lang);
    $Template->assign("data", $nd);
    $Template->parse("box_main");
    $vnT->output .= $Template->text("box_main");
  }
  function do_Add ($lang){
    global $vnT, $func, $DB, $conf;
    $err = "";
    $data['city'] = 2;
		$data['gender']=1;
    if ($vnT->input['do_submit'] == 1) {
      $data = $_POST;
      //check username
      $username = $vnT->input['username'];
      if ($DB->do_check_exist("members", " username='$username' "))
        $err = $func->html_err($vnT->lang['username_existed']);
        //check email
      $email = $vnT->input['email'];
      if ($DB->do_check_exist("members", "email='$email' "))
        $err = $func->html_err($vnT->lang['email_existed']);
      $password = $func->md10($vnT->input['password']);
      $birthday = $vnT->input["day"] . "/" . $vnT->input["month"] . "/" . $vnT->input["year"];
     
		 //upload picture			
      if (empty($err)) {
        if (! empty($_FILES['image']) && ($_FILES['image']['name'] != "")) {
          $up['path'] = MOD_DIR_UPLOAD;
          $up['dir'] = "avatar";
          $up['file'] = $_FILES['image'];
          $up['type'] = "hinh";
          $up['w'] = 150;
          $result = $vnT->File->Upload($up);
          if (empty($result['err'])) {
            $picture = $result['link'];
          } else {
            $err = $func->html_err($result['err']);
          }
        }
      }
      // insert CSDL
      if (empty($err)) {
				$full_address = $vnT->input['address'];
				if($vnT->input['state']) $full_address .= ", ".get_state_name($vnT->input['state']) ;
				if($vnT->input['city'])  $full_address .= ", ".get_city_name($vnT->input['city']) ; 
				
        $cot = array(
          "mem_group" => $vnT->input['mem_group'] , 
					"username" => $vnT->input['username'] ,           
          "email" => $vnT->input['email'] ,
          "password" => $password ,
          "gender" => $vnT->input['gender'] ,  
					"full_name" => $vnT->input['full_name']  , 
          "birthday" => $birthday , 
          "address" => $vnT->input['address'] , 
          "city" => $vnT->input['city'] , 
          "state" => $vnT->input['state'] ,  
          "country" => $vnT->input['country'] ,  
					"zipcode" => $vnT->input['zipcode'] , 
					
          "phone" => $vnT->input['phone'] , 					
          "mobile" => $vnT->input['mobile'] , 
          "cmnd" => $vnT->input['cmnd'] , 
          "yahoo_id" => $vnT->input['yahoo_id'] , 
          "skype" => $vnT->input['skype'] , 
          "website" => $vnT->input['website'] , 
          "hide_email" => $vnT->input['hide_email'] , 
          "newsletter" => $vnT->input['newsletter'] , 
          "date_join" => time() , 
          "activate_code" => md5(uniqid(microtime())) , 
          "m_status" => 1);
					
			  $cot['avatar'] = $picture;
					
        $ok = $DB->do_insert("members", $cot);
        if ($ok) {
          //xoa cache
          $func->clear_cache();
          //insert adminlog
          $func->insertlog("Add", $_GET['act'], $DB->insertid());
          //update maillist
          if ($vnT->input['newsletter'] == 1) {
            $err_mail = 0;
            $check = $DB->query("select * from listmail where email='" . $vnT->input['email'] . "'");
            if ($DB->num_rows($check))
              $err_mail = 1;
            if ($err_mail == 0) {
              $cot1['email'] = $_POST['email'];
              $cot1['datesubmit'] = time();
              $DB->do_insert("listmail", $cot1);
            }
          }
          $mess = $vnT->lang['add_success'];
          $url = $this->linkUrl . "&sub=add";
          $func->html_redirect($url, $mess);
        } else {
          $err = $func->html_err($vnT->lang['add_failt'] . $DB->debug());
        }
      }
    }
    $data['list_mem_group'] = List_Mem_Group($data['mem_group'], 0);
    $data['list_day'] = ListDay($data['day']);
    $data['list_month'] = ListMonth($data['month']);
    $data['list_year'] = ListYear($data['year']);
    $data['list_gender'] = List_Gender($data['gender']) ; 
		
		$data['list_city'] = List_City("VN" ,$data['city'], "style='width:350px;'  onChange=\"LoadAjax('list_state','city='+this.value,'ext_state');\" "); 
 			 
		$data['list_state'] =  List_State ($data['city'],$data['state'],"  style='width:350px;' "); 
			
    $data['list_hide_email'] = vnT_HTML::list_yesno("hide_email", $data['hide_email']);
    $data['list_newsletter'] = vnT_HTML::list_yesno("newsletter", $data['newsletter']);
    $data['err'] = $err;
    $data['link_action'] = $this->linkUrl . "&sub=add";
    /*assign the array to a template variable*/
    $this->skin->assign('data', $data);
    $this->skin->parse("add");
    return $this->skin->text("add");
  }
  function do_Edit ($lang){
    global $vnT, $func, $DB, $conf;
    $id = (int) $vnT->input['id'];
    if ($vnT->input['do_submit']) {
      $data = $_POST;
      $birthday = $vnT->input["day"] . "/" . $vnT->input["month"] . "/" . $vnT->input["year"];
      // Check for Error
      $username = $vnT->input['username'];
      $res_chk = $DB->query("SELECT * FROM members  WHERE  username='$username' and mem_id<>$id ");
      if ($check = $DB->fetch_row($res_chk))
        $err = $func->html_err($vnT->lang['username_existed']);
        //upload picture
      if ($_POST['chk_upload'] == 1) {
        if (! empty($_FILES['image']) && ($_FILES['image']['name'] != "")) {
          $up['path'] = MOD_DIR_UPLOAD;
          $up['dir'] = "avatar";
          $up['file'] = $_FILES['image'];
          $up['type'] = "hinh";
          $up['w'] = 150;
          $result = $vnT->File->Upload($up);
          if (empty($result['err'])) {
            $picture = $result['link'];
          } else {
            $err = $func->html_err($result['err']);
          }
        }
      }
      if (empty($err)) {
				
				$full_address = $vnT->input['address'];
				if($vnT->input['state']) $full_address .= ", ".get_state_name($vnT->input['state']) ;
				if($vnT->input['city'])  $full_address .= ", ".get_city_name($vnT->input['city']) ; 
				
        $cot = array(
          "mem_group" => $vnT->input['mem_group'] , 
					"username" => $vnT->input['username'] ,           
          "email" => $vnT->input['email'] , 
          "gender" => $vnT->input['gender'] ,  
					"full_name" => $vnT->input['full_name']  , 
          "birthday" => $birthday , 
          "address" => $vnT->input['address'] , 
          "city" => $vnT->input['city'] , 
          "state" => $vnT->input['state'] ,  
          "country" => $vnT->input['country'] , 
					"zipcode" => $vnT->input['zipcode'] ,
          "phone" => $vnT->input['phone'] , 
					
          "mobile" => $vnT->input['mobile'] , 
          "cmnd" => $vnT->input['cmnd'] , 
          "yahoo_id" => $vnT->input['yahoo_id'] , 
          "skype" => $vnT->input['skype'] , 
          "website" => $vnT->input['website'] , 
          "hide_email" => $vnT->input['hide_email'] , 
          "newsletter" => $vnT->input['newsletter']);
				
        if (! empty($vnT->input['password'])) {
          $cot['password'] = $func->md10(trim($vnT->input['password']));
        }
        
				if ($_POST['chk_upload'] == 1) {
          $img_q = $DB->query("SELECT avatar FROM members WHERE mem_id=$id ");
          if ($img = $DB->fetch_row($img_q)) {
            $file_pic = MOD_DIR_UPLOAD . "avatar/" . $img['avatar'];
            if ((file_exists($file_pic)) && (! empty($img['avatar'])))
              unlink($file_pic);
          }
          $cot['avatar'] = $picture;
        }
				$cot['mem_point'] = (int) $vnT->input['mem_point'];
        $ok = $DB->do_update("members", $cot, "mem_id=$id");
        if ($ok) {
          /* gui email thong bao
					if ($_POST['note']){
						//send email
						$subject = "Thong bao den thanh vien tu ".$_SERVER['HTTP_HOST'];
						$message = $func->HTML($_POST['note']) ;
						$sent = $func->doSendMail($_POST['email'],$subject,$message, $conf['email']);
					}
					*/
          //update maillist
          if ($vnT->input['newsletter'] == 1) {
            $err_mail = 0;
            $check = $DB->query("select * from listmail where email='" . $vnT->input['email'] . "'");
            if ($DB->num_rows($check))
              $err_mail = 1;
            if ($err_mail == 0) {
              $cot1['email'] = $_POST['email'];
              $cot1['datesubmit'] = time();
              $DB->do_insert("listmail", $cot1);
            }
          }
          //xoa cache
          $func->clear_cache();
          //insert adminlog
          $func->insertlog("Edit", $_GET['act'], $id);
          $err = $vnT->lang["edit_success"];
          $url = $this->linkUrl . "&sub=edit&id=$id";
          $func->html_redirect($url, $err);
        } else
          $err = $func->html_err($vnT->lang["edit_failt"] . $DB->debug());
      }
    }
    $query = $DB->query("SELECT * FROM members WHERE mem_id=$id");
    if ($data = $DB->fetch_row($query)) 
		{
      $data['list_mem_group'] = List_Mem_Group($data['mem_group'], 0);
      $data['list_gender'] = List_Gender($data['gender']) ;
      //picture
      if (! empty($data['avatar'])) {
        $data['pic'] = "<img src=\"" . MOD_DIR_UPLOAD . "avatar/{$data['avatar']}\" ><br>";
        $data['pic'] .= "<input name=\"chk_upload\" type=\"radio\" value=\"1\"> Upload Image &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
        $data['pic'] .= "<input name=\"chk_upload\" type=\"radio\" value=\"0\" checked> Keep Image <br>";
      } else
        $data['pic'] = "<input name=\"chk_upload\" type=\"hidden\" value=\"1\">";
        //birthday
      $tmp = explode("/", $data['birthday']);
      $data['list_day'] = ListDay($tmp[0]);
      $data['list_month'] = ListMonth($tmp[1]);
      $data['list_year'] = ListYear($tmp[2]);
      $data['list_hide_email'] = vnT_HTML::list_yesno("hide_email", $data['hide_email']);
      $data['list_newsletter'] = vnT_HTML::list_yesno("newsletter", $data['newsletter']);
      switch ($data['m_status']) 
			{
        case 0: $data['status_name'] = "<span class=font_err>".$vnT->lang['not_active']."</span>";   break;
        case 1: $data['status_name'] = $vnT->lang['active']; break;
        case 2: $data['status_name'] = "<span class=font_err>".$vnT->lang['banned']."</span>";  break;
      }
			
      $data['date_join'] = date("H:i - d/m/Y ", $data['date_join']);
      if (! empty($data['last_login']))
        $data['date_last_login'] = date("H:i - d/m/Y ", $data['last_login']);
      else
        $data['date_last_login'] = $vnT->lang['no_login'];
				
      $data['list_city'] = List_City("VN" ,$data['city'], "style='width:350px;'  onChange=\"LoadAjax('list_state','city='+this.value,'ext_state');\" "); 
 			 
			$data['list_state'] =  List_State ($data['city'],$data['state'],"  style='width:350px;' "); 
		
    } else {
      $mess = $vnT->lang['not_found'] . " ID : " . $id;
      $url = $this->linkUrl;
      $func->html_redirect($url, $mess);
    }
    $data['link_ban'] = $this->linkUrl . "&sub=ban&id=$id";
    $data['link_unban'] = $this->linkUrl . "&sub=unban&id=$id";
    $data['link_del'] = $this->linkUrl . "&sub=del&id=$id";
    $data['err'] = $err;
    $data['link_action'] = $this->linkUrl . "&sub=edit&id=$id";
    /*assign the array to a template variable*/
    $this->skin->assign('data', $data);
    $this->skin->parse("edit");
    return $this->skin->text("edit");
  }
  function do_Active ($lang){
    global $func, $DB, $conf, $vnT;
    $id = (int) $vnT->input['id'];
    $query = "UPDATE members SET m_status =1 WHERE mem_id=" . $id;
    if ($ok = $DB->query($query)) {
      //insert adminlog
      $func->insertlog("Active", $_GET['act'], $id);
      $mess = $vnT->lang['mess_active_success'];
    }
    $url = $this->linkUrl;
    $func->html_redirect($url, $mess);
  }
  function do_Ban ($lang){
    global $func, $DB, $conf, $vnT;
    $id = (int) $vnT->input['id'];
    $query = "UPDATE members SET m_status =2 WHERE mem_id=" . $id;
    if ($ok = $DB->query($query)) {
      //insert adminlog
      $func->insertlog("Ban", $_GET['act'], $id);
      $mess = $vnT->lang['mess_ban_success'];
    }
    $url = $this->linkUrl;
    $func->html_redirect($url, $mess);
  }
  function do_UnBan ($lang){
    global $func, $DB, $conf, $vnT;
    $id = (int) $vnT->input['id'];
    $query = "UPDATE members SET m_status =1 WHERE mem_id=" . $id;
    if ($ok = $DB->query($query)) {
      //insert adminlog
      $func->insertlog("Unban", $_GET['act'], $id);
      $mess = $vnT->lang['mess_unban_success'];
    }
    $url = $this->linkUrl;
    $func->html_redirect($url, $mess);
  }
  function do_Send_Mail ($lang){
    global $vnT, $func, $DB, $conf;
    if (isset($vnT->input["list_id"])) {
      $ids = $vnT->input["list_id"];
    }
    $res = $DB->query("select email from members where mem_id IN (" . $ids . ")");
    if ($num = $DB->num_rows($res)) {
      $i = 0;
      while ($row = $DB->fetch_row($res)) {
        $i ++;
        $email .= $row["email"] . " ";
        if ($i < $num)
          $email .= ",";
      }
    }
    if ($vnT->input['do_submit']) {
      $data = $_POST;
      $message = $_POST['content'];
      $result = $DB->query("select email from members where mem_id IN (" . $ids . ")");
      $num = 0;
      $flag = 0;
      if ($num = $DB->num_rows($result)) {
        while ($row = $DB->fetch_row($result)) {
          $mail = $row['email'];
          $sent = $func->doSendMail($mail, $vnT->input['subject'], $message, $vnT->conf['email']);
          if ($sent) {
            $flag = 1;
          } else {
            $flag = 0;
          }
        }
      }
      if ($flag) {
        //insert adminlog
        $func->insertlog("Send mail", $_GET['act'], $id);
        //insert adminlog
        $func->insertlog("Edit", $_GET['act'], $id);
        $err = "Đã gửi email tới {$num} thành viên ";
        $url = $this->linkUrl; //. "&sub=send_mail&id=$id";
        $func->html_redirect($url, $err);
      } else {
        $err = "Quá trình gửi mail thất bại ";
        $url = $this->linkUrl; //. "&sub=send_mail&id=$id";
        $func->html_redirect($url, $err);
      }
    }
    $data['send_for'] = "<b>Send mail to :</b> " . $email;
    $data["html_content"] = $vnT->editor->doDisplay('content', $vnT->input['content'], '100%', '500', "Normal");
    $data['err'] = $err;
    $data['link_action'] = $this->linkUrl . "&sub=send_mail&list_id=$ids";
    /*assign the array to a template variable*/
    $this->skin->assign('data', $data);
    $this->skin->parse("send_mail");
    return $this->skin->text("send_mail");
  }
  function do_Del ($lang){
    global $func, $DB, $conf, $vnT;
    $id = (int) $vnT->input['id'];
    $ext = $vnT->input["ext"];
    $del = 0;
    $qr = "";
    if ($id != 0) {
      $ids = $id;
    }
    if (isset($vnT->input["del_id"])) {
      $ids = implode(',', $vnT->input["del_id"]);
    }
    $query = 'UPDATE members SET m_status = -1 WHERE mem_id IN (' . $ids . ')';
    if ($ok = $DB->query($query)) {
      //insert RecycleBin
      $rb_log['module'] = $this->module;
      $rb_log['action'] = $this->action;
      $rb_log['tbl_data'] = "members";
      $rb_log['name_id'] = "mem_id";
      $rb_log['item_id'] = $ids;
      // $func->insertRecycleBin($rb_log);
      $mess = $vnT->lang["del_success"];
    } else
      $mess = $vnT->lang["del_failt"];
    $ext_page = str_replace("|", "&", $ext);
    $url = $this->linkUrl . "&{$ext_page}";
    $func->html_redirect($url, $mess);
  }
  function render_row ($row_info, $lang){
    global $func, $DB, $conf, $vnT;
    $row = $row_info;
    // Xu ly tung ROW
    $id = $row['mem_id'];
    $row_id = "row_" . $id;
    $output['check_box'] = vnT_HTML::checkbox("del_id[]", $id, 0, " ");
    $link_edit = $this->linkUrl . '&sub=edit&id=' . $id . '&ext=' . $row['ext_page'];
    $link_del = "javascript:del_item('" . $this->linkUrl . "&sub=del&id=" . $id . "&ext=" . $row['ext_page'] . "')";
    $link_order = $this->linkUrl . "&sub=order_history&id={$id}";
    $link_send = $this->linkUrl . '&sub=send_mail&id=' . $id;
    $link_money= "?mod=member&act=money_statistics&search=mem_id&keyword=".$id;

    switch ($row['m_status']) {
      case 0:
        $text_status = "&nbsp;<span class='font_err'><a href='" . $this->linkUrl . "&sub=active&id=$id'>[ Not active ]</a></span>";
      break;
      case 2:
        $text_status = "&nbsp;<span class='font_err'><a href='" . $this->linkUrl . "&sub=unban&id=$id'>[ Ban ]</a></span>";
      break;
      default:
        $text_status = "";
      break;
    }
		
    $avatar = "";
    if ($row['avatar']) {
      $avatar = "<table  border=0 cellspacing=1 cellpadding=1 align=left><tr><td><img src=\"" . MOD_DIR_UPLOAD . "/avatar/{$row['avatar']}\" width=50 ></td></tr></table>";
    } else {
      $avatar = "";
    }
    $output['username'] = "<a href=\"{$link_edit}\"><strong>" . $row['username'] . "</strong></a> " . $text_status;
    $output['member'] = $avatar . "MemID: <b class=font_err>" . $row['mem_id'] . "</b>";
    $output['member'] .= "<br>Họ tên : <strong >" . $func->HTML($row['full_name']) . "</strong>";
    $output['member'] .= "<br>Email : <strong>" . $row['email'] . "</strong> <a href='" . $link_send . "'><img src=\"{$vnT->dir_images}/send_mail.gif\" width=15  alt=\"Send\"/></a>";


    //order
    $res = $DB->query("select order_id from order_sum where mem_id=" . $row['mem_id']);
    if ($num_order = $DB->num_rows($res)) {
      $output['order_history'] = "<a href=\"{$link_order}\"><img src=\"modules/member_ad/images/order_history.gif\"  alt=\"Order\" align=absmiddle ></a> <b class=font_err>(" . (int) $num_order . ")</b>";
    } else {
      $output['order_history'] = "Chưa có";
    }
     
		
		$link_display = $this->linkUrl.$row['ext_link']; 		
    if ($row['m_status'] == 1) {
      $display = "<a href='".$link_display."&do_hidden=$id' title='".$vnT->lang['click_do_hidden']."' ><img src=\"" . $vnT->dir_images . "/dispay.gif\" width=15  /></a>";
    } else {
      $display = "<a href='".$link_display."&do_display=$id'  title='".$vnT->lang['click_do_display']."' ><img src=\"" . $vnT->dir_images . "/nodispay.gif\"  width=15 /></a>";
    }
		 
    $output['is_focus'] = vnT_HTML::list_yesno("is_focus[$id]", $row['is_focus'], "onchange='javascript:do_check($id)'");


    $info =  "<div style='padding:2px;'>Loại TV : <strong>".$vnT->setting['g_name'][$row['mem_group']]."</strong></div>";
    /*$info .=  "<div style='padding:2px;'>Tiền : <strong class='red'>".get_format_money($row['mem_point'])."</strong> <a href=\"{$link_money}\">[<img src=\"".$vnT->dir_images."/but_view.gif\" width=22  alt=\"Gold\" align=absmiddle >Chi tiết]</a> | <a href=\"?mod=member&act=money&sub=add&id=".$id."\" title='Nap tiền cho thành viên'  target='_blank'>[<img src=\"modules/member_ad/images/ads_new.gif\"   alt=\"Addd\" align=absmiddle >Nạp tiền]</a></div>";*/

		$info .=  "<div style='padding:2px;'>".$vnT->lang['date_join']." : <strong>".@date("d/m/Y", $row['date_join'])."</strong></div>";
		//$data_join = ($row['last_login'] == 0) ? "Chưa đăng nhập" : "<b>".@date("d/m/Y",$row['last_login'])."</b>";
		//$info .= "<div style='padding:2px;'>".$vnT->lang['date_last_login']." : ".$data_join."</div>";
 		
		$output['info'] = $info ;
		
    $output['action'] = '<input name=h_id[]" type="hidden" value="' . $id . '" />';
    $output['action'] .= '<a href="' . $link_edit . '"><img src="' . $vnT->dir_images . '/edit.gif"  alt="Edit "></a>&nbsp;';
    $output['action'] .= $display . '&nbsp;';
    $output['action'] .= '<a href="' . $link_del . '"><img src="' . $vnT->dir_images . '/delete.gif"  alt="Delete "></a>';
    return $output;
  }
  function do_Manage ($lang){
    global $vnT, $func, $DB, $conf;
    //update
    if ($vnT->input["do_action"]) {
      //xoa cache
      $func->clear_cache();
      $mess ='';
      if ($vnT->input["del_id"])     $h_id = $vnT->input["del_id"];
      switch ($vnT->input["do_action"]) {
        case "do_edit":
				if (isset($vnT->input["is_focus"])) $is_focus = $vnT->input["is_focus"];
				  $mess .= "- " . $vnT->lang['edit_success'] . " ID: <strong>";
          $str_mess = "";
          for ($i = 0; $i < count($h_id); $i ++)
          {
            $dup['is_focus'] = $is_focus[$h_id[$i]];
            $ok = $DB->do_update("members", $dup, "mem_id=" . $h_id[$i]);
            if ($ok)
            {
              $str_mess .= $h_id[$i] . ", ";
            } else
            {
              $mess .= "- " . $vnT->lang['edit_failt'] . " ID: <strong>" . $h_id[$i] . "</strong><br>";
            }
          }
          $mess .= substr($str_mess, 0, - 2) . "</strong><br>";
          $err = $func->html_mess($mess);
        break;
        case "do_hidden":
          $mess .= "- " . $vnT->lang['hidden_success'] . " ID: <strong>";
          for ($i = 0; $i < count($h_id); $i ++) {
            $dup['m_status'] = 2;
            $ok = $DB->do_update("members", $dup, "mem_id=" . $h_id[$i]);
            if ($ok) {
              $str_mess .= $h_id[$i] . ", ";
            }
          }
          $mess .= substr($str_mess, 0, - 2) . "</strong><br>";
          $err = $func->html_mess($mess);
        break;
        case "do_display":
          $mess .= "- " . $vnT->lang['display_success'] . "  ID: <strong>";
          for ($i = 0; $i < count($h_id); $i ++) {
            $dup['m_status'] = 1;
            $ok = $DB->do_update("members", $dup, "mem_id=" . $h_id[$i]);
            if ($ok) {
              $str_mess .= $h_id[$i] . ", ";
            }
          }
          $mess .= substr($str_mess, 0, - 2) . "</strong><br>";
          $err = $func->html_mess($mess);
        break;
      }
    }
		if((int)$vnT->input["do_display"]) {				
			$ok = $DB->query("Update members SET m_status=1 WHERE mem_id=".$vnT->input["do_display"]);
			if($ok){
				$mess .= "- " . $vnT->lang['display_success'] . "  ID: <strong>".$vnT->input["do_display"] . "</strong><br>";	
				$err = $func->html_mess($mess);
			}        
			//xoa cache
      $func->clear_cache();
		}
		if((int)$vnT->input["do_hidden"]) {				
			$ok = $DB->query("Update members SET m_status=0 WHERE mem_id=".$vnT->input["do_hidden"]);
			if($ok){
				$mess .= "- " . $vnT->lang['display_success'] . "  ID: <strong>".$vnT->input["do_hidden"] . "</strong><br>";	
				$err = $func->html_mess($mess);
			}    
			//xoa cache
      $func->clear_cache();
		}
    $p = ((int) $vnT->input['p']) ? $vnT->input['p'] : 1;
    $n = ($conf['record']) ? $conf['record'] : 30;
    $m_status = (isset($vnT->input['m_status'])) ? $vnT->input['m_status'] : "-1";
    $mem_group = (isset($vnT->input['mem_group'])) ? $vnT->input['mem_group'] : 0;
    $search = (isset($vnT->input['search'])) ? $vnT->input['search'] : "mem_id";
    $keyword = (isset($vnT->input['keyword'])) ? $vnT->input['keyword'] : "";
    $where = "";
    $ext_page ="";
    $ext = "";
    if ($mem_group) {
      $where .= " and mem_group=$mem_group ";
      $ext .= "&mem_group={$mem_group}";
      $ext_page .= "mem_group=$mem_group|";
    }
    if ($m_status != "-1") {
      $where .= " and m_status=$m_status ";
      $ext .= "&m_status={$m_status}";
      $ext_page .= "m_status=$m_status|";
    }
    if (! empty($keyword)) {
      switch ($search) {
        case "mem_id":
          $where .= " and mem_id=$keyword ";
        break;
        case "date_join":
          $where .= " and DATE_FORMAT(FROM_UNIXTIME(date_join),'%d/%m/%Y') = '$keyword' ";
        break;
        case "birthday":
          $where .= " and substring( birthday, 1, 5 ) = '$keyword' ";
        break;
        default:
          $where .= " and $search like '%$keyword%' ";
        break;
      }
      $ext .= "&search={$search}&keyword={$keyword}";
      $ext_page .= "search=$search|keyword=$keyword|";
    }
    $ext_page .= "p=$p";
    $query = $DB->query("SELECT mem_id FROM members  WHERE m_status <> -1 $where ");
    $totals = intval($DB->num_rows($query));
    $num_pages = ceil($totals / $n);
    if ($p > $num_pages)
      $p = $num_pages;
    if ($p < 1)
      $p = 1;
    $start = ($p - 1) * $n;
    $nav = $func->paginate($totals, $n, $ext, $p);
    $table['link_action'] = $this->linkUrl . "{$ext}&p=$p"; 
		$ext_link = $ext."&p=$p" ;
		
    $table['title'] = array(
      'check_box' => "<input type=\"checkbox\" name=\"checkall\" id=\"checkall\" class=\"checkbox\" />|5%|center" , 
      'username' => "Tài khoản |15%|left" , 
      'member' => "Thông tin | 30%|left" ,
      'info' => $vnT->lang['infomartion'] . "| |left" ,
      'is_focus' => "Focus|10%|center" ,
      'action' => "Action|10%|center"
    );
		$sql = "SELECT * FROM members  
					  WHERE m_status <> -1 ORDER BY mem_id DESC LIMIT $start,$n";
    $reuslt = $DB->query($sql);
    if ($DB->num_rows($reuslt)) {
      $row = $DB->get_array($result);
      for ($i = 0; $i < count($row); $i ++) {
        $row[$i]['ext_link'] = $ext_link ;
				$row[$i]['ext_page'] = $ext_page;
        $row_info = $this->render_row($row[$i], $lang);
        $row_field[$i] = $row_info;
        $row_field[$i]['stt'] = ($i + 1);
        $row_field[$i]['row_id'] = "row_" . $row[$i]['mem_id'];
        $row_field[$i]['ext'] = "";
      }
      $table['row'] = $row_field;
    } else {
      $table['row'] = array();
      $table['extra'] = "<div align=center class=font_err >" . $vnT->lang['no_have_member'] . "</div>";
    }
    $table['button'] = '<input type="button" name="btnHidden" value=" ' . $vnT->lang['hidden'] . ' " class="button" onclick="do_submit(\'do_hidden\')">&nbsp;';
    $table['button'] .= '<input type="button" name="btnDisplay" value=" ' . $vnT->lang['display'] . ' " class="button" onclick="do_submit(\'do_display\')">&nbsp;';
    $table['button'] .= '<input type="button" name="btnEdit" value=" ' . $vnT->lang['update'] . ' " class="button" onclick="do_submit(\'do_edit\')">&nbsp;';
    $table['button'] .= '<input type="button" name="btnDel" value=" ' . $vnT->lang['delete'] . ' " class="button" onclick="del_selected(\'' . $this->linkUrl . '&sub=del&ext=' . $ext_page . '\')">';
    $table['button'] .= '<input type="button" name="sendMailList" value="Send email list" class="button" onclick="do_send_maillist()">&nbsp;';
    $table_list = $func->ShowTable($table);
    $data['table_list'] = $table_list;
    $data['totals'] = $totals;
    $data['list_mem_group'] = List_Mem_Group($mem_group, 1);
    $data['list_status'] = vnT_HTML::selectbox("m_status", array(
      '-1' => '--- '.$vnT->lang['all'].' ---' , 
      '0' => $vnT->lang['not_active'] , 
      '1' => $vnT->lang['active'] , 
      '2' => $vnT->lang['banned']), $m_status, "", "onChange='submit();'");
    $data['keyword'] = $keyword;
    $data['list_search'] = List_Search($search);
    $data['err'] = $err;
    $data['nav'] = $nav;

    $data['link_excel'] = $conf['rooturl']."admin/modules/member_ad/excel_member.php?lang=$lang".$ext ;

    /*assign the array to a template variable*/
    $this->skin->assign('data', $data);
    $this->skin->parse("manage");
    return $this->skin->text("manage");
  }
}
?>