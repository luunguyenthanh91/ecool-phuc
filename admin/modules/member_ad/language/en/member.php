<?php
$lang = array(
  //Quản lý thanh vien
  'manage_member' => "Manage member" , 
  'add_member' => "Add new member" , 
  'edit_member' => "Edit member" , 
  'no_have_member' => 'No have member' , 
  'manage_mem_group' => "Manage member group" , 
  'add_mem_group' => "Add member group" , 
  'edit_mem_group' => "Edit member group" , 
  'username_existed' => 'Username existed' , 
  'email_existed' => 'Email existed' , 
  'group_name' => 'Group name' , 
  'group_member' => 'Group member' , 
  'info_member' => 'INFORMATION MEMBER' , 
  'status_member' => 'STATUS ACCOUNT' , 
  'note_password' => 'Enter the password to change your password (if left blank will retain the old password) ' , 
  'date_join' => 'Date create' , 
  'date_last_login' => 'Last login' , 
  'num_login' => 'Num login' , 
  'announce_for_member' => 'Announce ' , 
  'banned_account' => 'Banned Account' , 
  'active_account' => 'Active Account' , 
  'del_account' => 'Delete Account' , 
  'not_active' => 'No active' , 
  'active' => 'Active' , 
  'banned' => 'Banned' , 
  'no_login' => 'No login' , 
  'mess_active_success' => 'Active account successfull' , 
  'mess_ban_success' => 'Banned account successfull' , 
  'mess_unban_success' => 'UnBan account successfull' , 
  'manage_member_log' => 'Member statistics' , 
  'detail_member_log' => 'Detail log' , 
  'no_have_member_log' => 'No have member logs' , 
	'option' => 'OPTION' , 
  //--
  'account_information' => 'Account Information ' , 
  'account_profile' => 'PERSONAL INFORMATION' , 
  'account_contact' => 'Contact INFORMATION' , 
  'info_yellowppage' => 'INFORMATION Trade Directory' , 
  'y_title' => 'CSTM' , 
  'city' => 'City' , 
  'category' => 'Category' , 
  'state' => 'State' , 
  'fax' => 'Fax' , 
  'zipcode' => 'Zipcode' , 
  'map' => 'Map' , 
  'website' => 'Website' , 
  'info_classifieds' => 'Information Classifield' , 
  'select_category' => 'Select category' , 
  'check_same' => 'Information Classifield same information Trade directory',
	//seting
  'manage_setting' => 'Member configuration' , 
  'w_avatar' => 'Image width Avatar' , 
  'active_email' => 'Activation Email' , 
  'insert_maillist' => 'Add the registration email to maillist' , 
  'term_of_register' => 'Terms of registration' , 
  'active_method' => 'Activate account',
	'group_default' => 'The default group when registering ' , 
	'active_method_0'	=>	'No need to activate',
	'active_method_1'	=>	'Activation by Email',
	'active_method_2'	=>	'Activation by administrator',
	'mess_join'	=>	'Member Benefits',
	
	);
?>