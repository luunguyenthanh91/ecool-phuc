<?php
$lang = array(
  //admin
  'add_admin' => "Thêm Admin" , 
  'edit_admin' => "Cập nhật admin" , 
  'manage_admin' => "Quản lý admin" , 
  'all_permission' => 'Tất cả quyền' , 
  //admin_group
  'add_admin_group' => "Thêm Nhóm Admin mới" , 
  'eidt_admin_group' => "Cập nhật nhóm admin" , 
  'manage_admin_group' => "Quản lý nhóm admin" , 
  'no_have_admin_group' => 'Chưa có nhóm admin nào' , 
  //adminlog
  'manage_adminlog' => 'Quản lý họat động của Admin' , 
  'group_name' => 'Tên nhóm' , 
  'group' => 'Nhóm' , 
  'permission' => 'Quyền hạn' , 
  'click_to_view_permission' => 'Click vào đây để xem quyền' , 
  'group_admin' => 'Nhóm quản trị (Admin)');
?>