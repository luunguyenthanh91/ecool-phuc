<?php
/*================================================================================*\
|| 							Name code : order.php 		 		            	  ||
||  				Copyright @2008 by Thai Son - CMS vnTRUST                     ||
\*================================================================================*/
/**
 * @version : 2.0
 * @date upgrade : 09/01/2009 by Thai Son
 **/
if (! defined('IN_vnT')) {
  die('Hacking attempt!');
}
$vntModule = new vntModule();

class vntModule
{
  var $output = "";
  var $skin = "";
  var $linkUrl = "";
  var $module = "service";
  var $action = "register";

  /**
   * function vntModule ()
   * Khoi tao 
   **/
  function vntModule ()
  {
    global $Template, $vnT, $func, $DB, $conf;
    require_once ("function_" . $this->module . ".php");
    $this->skin = new XiTemplate(DIR_MODULE . DS . $this->module . "_ad" . DS . "html" . DS . $this->action . ".tpl");
    $this->skin->assign('LANG', $vnT->lang);
    $lang = ($vnT->input['lang']) ? $lang = $vnT->input['lang'] : $func->get_lang_default();
    $this->linkUrl = "?mod=" . $this->module . "&act=" . $this->action . "&lang=" . $lang;
    $vnT->html->addStyleSheet("modules/" . $this->module . "_ad/css/" . $this->module . ".css");
    switch ($vnT->input['sub']) {
      case 'edit':
        $nd['f_title'] = 'Chi tiết đăng ký';
        $nd['content'] = $this->do_Edit($lang);
      break;
      case 'del':
        $this->do_Del($lang);
      break;
      default:
        $nd['f_title'] = 'Quản lý đăng ký online ';
        $nd['content'] = $this->do_Manage($lang);
      break;
    }
    $nd['menu'] = $func->getToolbar_Small($this->module, $this->action, $lang);
    $nd['row_lang'] = $func->html_lang("?mod=" . $this->module . "&act=" . $this->action, $lang);
    $Template->assign("data", $nd);
    $Template->parse("box_main");
    $vnT->output .= $Template->text("box_main");
  }

  /**
   * function do_Edit 
   *   
   **/
  function do_Edit ($lang)
  {
    global $vnT, $func, $DB, $conf, $a_order_status;
     global $vnT, $func, $DB, $conf;
    $vnT->html->addStyleSheet( $vnT->dir_js . "/jquery_ui/themes/base/ui.all.css");
		$vnT->html->addScript($vnT->dir_js . "/jquery_ui/ui.core.js");		
		$vnT->html->addScript($vnT->dir_js . "/jquery_ui/ui.datepicker.js");  
    $vnT->html->addScriptDeclaration("
	 		$(function() {
				$('#date_update').datepicker({
					changeMonth: true,
					changeYear: true
				});

			});
		
		");
		
    $id = $vnT->input['id'];
    $msg = "";
    if (isset($_POST["btnSave"])) {
      if ($_POST["date_update"] != "") {
        $tmp = explode("/", $_POST["date_update"]);
        $date_update = mktime(date("H"), date("i"), 0, $tmp[1], $tmp[0], $tmp[2]);
      } else {
        $date_update = time();
      }
      $status = $_POST["status"];
      $dataup["date_update"] = $date_update;
      $dataup["status"] = $status;
      $ok = $DB->do_update("service_register", $dataup, "id='{$id}'");
      if ($ok) {
        $err = "Modify Successful";
        $url = $this->linkUrl . "&sub=edit&id=$id";
        $func->html_redirect($url, $err);
      } else
        $err = $vnT->lang["edit_failt"];
    }
    $sql = "select * from service_register where id ='{$id}' ";
    $result = $DB->query($sql);
    if ($data = $DB->fetch_row($result)) {
		 
			
      $data['date_post'] = date("H:i, d/m/Y", $data['date_post']);
			$data['type_name'] = $vnT->setting['type'][$data['type']];
			$data['note'] = $func->HTML($data['note']);
			 
			$data['product'] = get_p_name($data['p_id'],$lang);
			$data['company'] =  $vnT->setting['company'][$data['company']];
			$data['gender'] = $vnT->setting['gender'][$data['gender']];
			
			$data['city'] = get_city_name($data['city']);
 			$data['service_by'] = $vnT->setting['service_by'][$data['service_by']];
			
			if($data['type'] == "maintenance")
			{
				$data['option_more']	 = '<tr>
				<td class="row0">Số KM bảo dưỡng  : </td>
				<td class="row1">'.$vnT->setting['km'][$data['time_buy']].'</td>
			</tr>
      <tr>
				<td class="row0">Thời gian đến bảo dưỡng:  </td>
				<td class="row1">'.$data['service_time'].'</td>
			</tr>';
			
			}else{
				$data['option_more']	 = '<tr>
				<td class="row0">Dự định mua xe trong  : </td>
				<td class="row1">'.$vnT->setting['time_buy'][$data['time_buy']].'</td>
			</tr>
      <tr>
				<td class="row0">Thời gian có thể liên lạc:  </td>
				<td class="row1">'.$data['service_time'].'</td>
			</tr>';
			}
			
    }
    
		$data['date_update'] = ($data['date_update']) ? date("d/m/Y", $data['date_update']) : date("d/m/Y");
    $data['id'] = $id;
    $data['list_status'] = List_Status($data['status']);
    $data['link_action'] = $this->linkUrl . "&sub=edit&id=$id";
    /*assign the array to a template variable*/
    $this->skin->assign('data', $data);
    $this->skin->parse("edit");
    return $this->skin->text("edit");
  }

  /**
   * function do_Task 
   *   
   **/
  function do_Del ($lang)
  {
    global $func, $DB, $conf, $vnT;
    global $func, $DB, $conf, $vnT;
    $id = (int) $vnT->input['id'];
    $ext = $vnT->input["ext"];
    $del = 0;
    $qr = "";
    if ($id != 0) {
      $ids = $id;
    }
    if (isset($vnT->input["del_id"])) {
      $ids = implode(',', $vnT->input["del_id"]);
    }
    $query = 'DELETE FROM service_register WHERE id IN (' . $ids . ')';
    if ($ok = $DB->query($query)) {
      $mess = $vnT->lang["del_success"];
    } else
      $mess = $vnT->lang["del_failt"];
    $ext_page = str_replace("|", "&", $ext);
    $url = $this->linkUrl . "&{$ext_page}";
    $func->html_redirect($url, $mess);
  }

  /**
   * function render_row 
   * list cac record
   **/
  function render_row ($row_info, $lang)
  {
    global $func, $DB, $conf, $vnT;
    $row = $row_info;
    // Xu ly tung ROW
    $id = $row['id'];
    $row_id = "row_" . $id;
    $output['check_box'] = vnT_HTML::checkbox("del_id[]", $id, 0, " ");
    //$link_edit = $this->linkUrl . '&sub=edit&id=' . $func->NDK_encode($id) . '&ext=' . $row['ext_page'];
    $link_edit = $this->linkUrl . "&sub=edit&id={$id}";
    $link_del = "javascript:del_item('" . $this->linkUrl . "&sub=del&id=" . $id . "&ext=" . $row['ext_page'] . "')";
    $link_send_email = $this->linkUrl . "&sub=send_email&id=" . $id;

    //$output['member'] = $func->HTML($row['d_name']) ;
    $output['date_post'] = date("H:i, d/m/Y", $row['date_post']);
		$output['date_post'] .= "<div style='padding:3px 0px;'>Reg ID: <strong class=font_err >#" . $row['id'] . "</strong></div>" ;
 
  	$customer = '<table width="100%" border="0" cellspacing="0" cellpadding="0" class="tblInfo">';
		$customer .= '<tr><td width="50" nowrap="nowrap"><strong>Họ tên </strong></td><td width=5 align="center">:</td><td ><strong>' . $func->HTML($row['name']) . '</strong></td></tr>';
		$customer .= '<tr><td nowrap="nowrap"><strong>Email </strong></td><td width=5 align="center">:</td><td >' . $func->HTML($row['email']) . '</td></tr>';
		$customer .= '<tr><td nowrap="nowrap"><strong>ĐT </strong></td><td width=5 align="center">:</td><td >' . $func->HTML($row['phone']) . '</td></tr>';
		if($row['company'])
			$customer .= '<tr><td nowrap="nowrap"><strong>Cty </strong></td><td width=5 align="center">:</td><td >' . $func->HTML($row['company']) . '</td></tr>';
		if($row['position'])
			$customer .= '<tr><td nowrap="nowrap"><strong>Chức vụ </strong></td><td width=5 align="center">:</td><td >' . $func->HTML($row['position']) . '</td></tr>';
		
		$customer .= '</table>';
		$output['customer'] = $customer ;
		
    
    if($row['cat_news_id']){
      $cat_name = $this->get_cat_name($row['cat_news_id'],"vn") ;
      $info = "<div style='padding:2px;'>Danh mục: <strong>".$cat_name."</strong></div>";  
    }

    if($row['list_news_id']){
      $arrNews = explode(",", $row['list_news_id']);
      foreach($arrNews as $value){
        $name = $this->get_news_name($value,"vn") ;
        $info .= "<div style='padding:2px;'>-- <i>".$name."</i></div>";
      }     
    }

    if($row['note']){
      $info .= '<div style="padding:2px;">'.$row['note'].'</div>';  
    }
		
    
		
		$output['info'] = $info;
		
    $output['date_order'] = date("H:i, d/m/Y", $row['date_order']);
    switch ($row['status']) {
      case "0":
        $output['status'] = "<b class=red>Đăng ký mới</b>";
      break;
      case "1":
        $output['status'] =  "<b class=blue>Đã xác nhận</b>";
      break;
      case "2":
        $output['status'] = "Hủy bỏ";
      break;
    }
    if ($row['display'] == 1) {
      $display = "<img src=\"{$vnT->dir_images}/dispay.gif\" width=15  />";
    } else {
      $display = "<img src=\"{$vnT->dir_images}/nodispay.gif\"  width=15 />";
    }
    $output['action'] = '<input name=h_id[]" type="hidden" value="' . $id . '" />';
    //$output['action'] .= '<a href="' . $link_edit . '"><img src="' . $vnT->dir_images . '/but_view.gif"  alt="View " width=22></a>&nbsp;';
    $output['action'] .= '<a href="' . $link_del . '"><img src="' . $vnT->dir_images . '/delete.gif"  alt="Delete "></a>';
    return $output;
  }

  /**
   * function do_Manage() 
   * Quan ly 
   **/
  function do_Manage ($lang)
  {
    global $vnT, $func, $DB, $conf;
		$vnT->html->addStyleSheet( $vnT->dir_js . "/jquery_ui/themes/base/ui.all.css");
		$vnT->html->addScript($vnT->dir_js . "/jquery_ui/ui.core.js");		
		$vnT->html->addScript($vnT->dir_js . "/jquery_ui/ui.datepicker.js");  
    $vnT->html->addScriptDeclaration("
	 		$(function() {
				$('#date_begin').datepicker({
						showOn: 'button',
						buttonImage: '".$vnT->dir_images."/calendar.gif',
						buttonImageOnly: true,
						changeMonth: true,
						changeYear: true
					});
					$('#date_end').datepicker({
						showOn: 'button',
						buttonImage: '".$vnT->dir_images."/calendar.gif',
						buttonImageOnly: true,
						changeMonth: true,
						changeYear: true
					});

			});
		
		"); 
		
    //update
    if ($vnT->input["do_action"]) {
      //xoa cache
      $func->clear_cache();
      if ($vnT->input["del_id"])
        $h_id = $vnT->input["del_id"];
      switch ($vnT->input["do_action"]) {
        
        case "do_status0":
          $mess .= "- Đã xét đăng ký mới ID: <strong>";
          for ($i = 0; $i < count($h_id); $i ++) {
            $dup['status'] = 0;
            $ok = $DB->do_update("service_register", $dup, "id=" . $h_id[$i]);
            if ($ok) {
              $str_mess .= $h_id[$i] . ", ";
            }
          }
          $mess .= substr($str_mess, 0, - 2) . "</strong><br>";
          $err = $func->html_mess($mess);
        break;
        case "do_status1":
          $mess .= "- Xét đã xác nhận ID: <strong>";
          for ($i = 0; $i < count($h_id); $i ++) {
            $dup['status'] = 1;
            $ok = $DB->do_update("service_register", $dup, "id=" . $h_id[$i]);
            if ($ok) {
              $str_mess .= $h_id[$i] . ", ";
            }
          }
          $mess .= substr($str_mess, 0, - 2) . "</strong><br>";
          $err = $func->html_mess($mess);
        break;
				case "do_status2":
          $mess .= "- Xét hủy bỏ ID: <strong>";
          for ($i = 0; $i < count($h_id); $i ++) {
            $dup['status'] = 2;
            $ok = $DB->do_update("service_register", $dup, "id=" . $h_id[$i]);
            if ($ok) {
              $str_mess .= $h_id[$i] . ", ";
            }
          }
          $mess .= substr($str_mess, 0, - 2) . "</strong><br>";
          $err = $func->html_mess($mess);
        break;
      }
    }
    $p = ((int) $vnT->input['p']) ? $p = $vnT->input['p'] : 1;
    $n = ($conf['record']) ? $conf['record'] : 30;
     
		$status = (isset($vnT->input['status'])) ? $vnT->input['status'] : "-1";
		$search = ($vnT->input['search']) ? $vnT->input['search'] : "id";
    $keyword = ($vnT->input['keyword']) ? $vnT->input['keyword'] : "";
 		$date_begin = ($vnT->input['date_begin']) ?  $vnT->input['date_begin'] : "";
		$date_end = ($vnT->input['date_end']) ?  $vnT->input['date_end'] : "";
		
		$where ="";
		if($date_begin || $date_end )
		{
			$tmp1 = @explode("/", $date_begin);
			$time_begin = @mktime(0, 0, 0, $tmp1[1], $tmp1[0], $tmp1[2]);
			
			$tmp2 = @explode("/", $date_end);
			$time_end = @mktime(23, 59, 59, $tmp2[1], $tmp2[0], $tmp2[2]);
			
			$where.=" AND (date_post BETWEEN {$time_begin} AND {$time_end} ) ";
			$ext.="&date_begin=".$date_begin."&date_end=".$date_end;
			$ext_page.= "date_begin=".$date_begin."|date_end=".$date_end."|";
		}
		
    if (! empty($keyword)) {
      if ($search == "date_post") {
        $where .= " and DATE_FORMAT(FROM_UNIXTIME(date_post),'%d/%m/%Y') = '{$keyword}' ";
      } else {
        $where .= " and $search like '%$keyword%' ";
      }
      $ext_page .= "keyword=$keyword|";
      $ext .= "&search={$search}&keyword={$keyword}";
    }
		
		if($status!=-1)
		{
			$where .=" AND status=".$status;
			$ext_page .= "status=$status|";
      $ext = "&status=$status";
		}
 		 
    $query = $DB->query("SELECT * FROM service_register WHERE id<>0  $where ");
    $totals = intval($DB->num_rows($query));
    $num_pages = ceil($totals / $n);
    if ($p > $num_pages)
      $p = $num_pages;
    if ($p < 1)
      $p = 1;
    $start = ($p - 1) * $n;
    $nav = $func->paginate($totals, $n, $ext, $p);
    $table['link_action'] = $this->linkUrl . $ext;
    $table['title'] = array(
      'check_box' => "<input type=\"checkbox\" name=\"checkall\" id=\"checkall\" class=\"checkbox\" />|5%|center" , 
      'date_post' => "Ngày đăng ký |12%|center" , 
			'customer' => "Học viên|30%|left" , 
      'info' => "Thông tin đăng ký|35%|left" ,       
      'status' => "Trạng thái|13%|center" , 
      'action' => "Action|10%|center");
    $sql = "SELECT * FROM service_register  WHERE id<>0 $where ORDER BY  date_post DESC  LIMIT $start,$n";
    //print "sql = ".$sql."<br>";
    $reuslt = $DB->query($sql);
    if ($DB->num_rows($reuslt)) {
      $row = $DB->get_array($result);
      for ($i = 0; $i < count($row); $i ++) {
        $row_info = $this->render_row($row[$i], $lang);
        $row_field[$i] = $row_info;
        $row_field[$i]['stt'] = ($i + 1);
        $row_field[$i]['row_id'] = "row_" . $row[$i]['id'];
        $row_field[$i]['ext'] = "";
      }
      $table['row'] = $row_field;
    } else {
      $table['row'] = array();
      $table['extra'] = "<div align=center class=font_err > Chưa có đăng ký nào</div>";
    }
		
		 $table['button'] = '<input type="button" name="btnStatus0" value=" Xét đăng ký mới " class="button" onclick="do_submit(\'do_status0\')">&nbsp;';
		 $table['button'] .= '<input type="button" name="btnStatus1" value=" Xét đã xác nhận " class="button" onclick="do_submit(\'do_status1\')">&nbsp;';
		 $table['button'] .= '<input type="button" name="btnStatus2" value=" Xét Hủy bỏ " class="button" onclick="do_submit(\'do_status2\')">&nbsp;';
		 
    $table['button'] .= '<input type="button" name="btnDel" value=" ' . $vnT->lang['delete'] . ' " class="button" onclick="del_selected(\'' . $this->linkUrl . '&sub=del&ext=' . $ext_page . '\')">';
		
    $table_list = $func->ShowTable($table);
    $data['table_list'] = $table_list;
    $data['totals'] = $totals;
    $data['keyword'] = $keyword;
		//$data['list_search'] = List_Search_Register($search);
		$data['date_begin'] = $date_begin;
		$data['date_end'] = $date_end;
		//$data['list_status'] = List_Status_Register($status,"onChange='submit()'");
    $data['err'] = $err;
    $data['nav'] = $nav;
    /*assign the array to a template variable*/
    $this->skin->assign('data', $data);
    $this->skin->parse("manage");
    return $this->skin->text("manage");
  }
  // end class

  function get_cat_name ($cat_id, $lang)
{
  global $vnT, $func, $DB, $conf;
  $result = $DB->query("select cat_name from news_category_desc  where cat_id=$cat_id and lang='$lang'");
  if ($row = $DB->fetch_row($result)) {
    $cat_name = $func->HTML($row['cat_name']);
  }
  return $cat_name;
}
  function get_news_name ($id, $lang)
{
  global $vnT, $func, $DB, $conf;
  $result = $DB->query("select title from news_desc  where $id=newsid and lang='$lang'");
  if ($row = $DB->fetch_row($result)) {
    $cat_name = $func->HTML($row['title']);
  }
  return $cat_name;
}


}
?>