<?php
/*================================================================================*\
|| 							Name code : service.php 		 		            	  ||
||  				Copyright @2008 by Thai Son - CMS vnTRUST                     ||
\*================================================================================*/
/**
 * @version : 2.0
 * @date upgrade : 09/01/2009 by Thai Son
 **/
if (! defined('IN_vnT')) {
  die('Hacking attempt!');
}
$vntModule = new vntModule();

class vntModule
{
  var $output = "";
  var $skin = "";
  var $linkUrl = "";
  var $module = "service";
  var $action = "service";
  var $setting = array();

  /**
   * function vntModule ()
   * Khoi tao 
   **/
  function vntModule ()
  {
    global $Template, $vnT, $func, $DB, $conf;
    require_once ("function_" . $this->module . ".php");
    
    $this->skin = new XiTemplate(DIR_MODULE . DS . $this->module . "_ad" . DS . "html" . DS . $this->action . ".tpl");
    $this->skin->assign('LANG', $vnT->lang);
		$this->skin->assign("DIR_JS", $vnT->dir_js);
		$this->skin->assign("DIR_IMAGE", $vnT->dir_images);
    $lang = ($vnT->input['lang']) ? $lang = $vnT->input['lang'] : $func->get_lang_default();
    $this->linkUrl = "?mod=" . $this->module . "&act=" . $this->action . "&lang=" . $lang;
		
		loadSetting($lang);
		$nd['menu'] = $func->getToolbar($this->module, $this->action, $lang);
    $nd['row_lang'] = $func->html_lang("?mod=" . $this->module . "&act=" . $this->action, $lang);
		
    switch ($vnT->input['sub']) {
      case 'add':
        $nd['f_title'] = $vnT->lang['add_service'];
        $nd['content'] = $this->do_Add($lang);
      break;
      case 'edit':
        $nd['f_title'] = $vnT->lang['edit_service'];
        $nd['content'] = $this->do_Edit($lang);
      break;
      case 'form_contact':
        $this->skin->assign("DIR_IMAGE", $vnT->dir_images);
        $this->skin->assign("DIR_STYLE", $vnT->dir_style);
        $this->skin->assign("DIR_JS", $vnT->dir_js);
        $this->skin->assign("CONF", $conf);
        flush();
        echo $this->do_FormContact($lang);
        exit();
      break;
      case 'del':
        $this->do_Del($lang);
      break;
      default:
        $nd['f_title'] = $vnT->lang['manage_service'];
        $nd['content'] = $this->do_Manage($lang);
      break;
    }
    
    $Template->assign("data", $nd);
    $Template->parse("box_main");
    $vnT->output .= $Template->text("box_main");
  }

  /**
   * function do_Add 
   * Them  moi 
   **/
  function do_Add ($lang)
  {
    global $vnT, $func, $DB, $conf;
    $err = "";
    $data['display'] = 1;
    $data['gio'] = date("H:i");
    $data['ngay'] = date("d/m/Y");
		$data['ref'] = 1;
		$dir = $vnT->func->get_dir_upload_module($this->module,$vnT->setting['folder_upload']);
    $w_thumb = ($vnT->setting['imgthumb_width']) ? $vnT->setting['imgthumb_width'] : 100;
    if ($vnT->input['do_submit'] == 1) {
      $data = $_POST;
      $cat_id = $vnT->input['cat_id'];
      $title = $vnT->input['title'];
      $gio = explode(":", $vnT->input['gio']);
      $ngay = explode("/", $vnT->input['ngay']);
      $date_post = mktime($gio[0], $gio[1], 0, $ngay[1], $ngay[0], $ngay[2]);
			$picture =  $vnT->input['picture'];
			if (empty($err)) {
        // lay hinh
        if ($vnT->setting['get_image']) {
          $tmp_content = replace_img($data['content'], $dir);
          $content = $tmp_content['result'];
          if (empty($picture)) {
            if ($tmp_content['url'])
              $picture = $tmp_content['url'];
          }
        } else {
          $content = $_POST['content'];
        }
      } //end if err		
      // insert CSDL
      if (empty($err)) {
        $cot = array(
          'cat_id' => $cat_id , 
          'parentid' => $vnT->input['service_id'] , 
          'picture' => $picture , 
          'date_post' => $date_post , 
          'date_update' => time()
					);
				
 					$cot['adminid'] =  $vnT->admininfo['adminid']; 
					
        $kq = $DB->do_insert("service", $cot);
        if ($kq) {
          
          $service_id = $DB->insertid(); 			
					
					$cot_d['service_id'] = $service_id;
					$cot_d['title'] = $title;
					$cot_d['short'] = $DB->mySQLSafe($_POST['short']);
					$cot_d['content'] = $DB->mySQLSafe($_POST['content']); 
					//SEO
					$cot_d['friendly_url'] = (trim($vnT->input['friendly_url'])) ? trim($vnT->input['friendly_url']) :  $func->make_url($_POST['title']);
					$cot_d['friendly_title'] = (trim($vnT->input['friendly_title'])) ? trim($vnT->input['friendly_title']) :  $title ;
					$cot_d['metakey'] = (trim($vnT->input['metakey'])) ? $vnT->input['metakey'] : $title;
					$cot_d['metadesc'] = (trim($vnT->input['metadesc'])) ? $vnT->input['metadesc'] : $func->cut_string($func->check_html($_POST['short'],'nohtml'),200,1); 						 
					
					$cot_d['display'] = $vnT->input['display'];
					
				  $query_lang = $DB->query("select name from language ");
          while ($row_lang = $DB->fetch_row($query_lang))
          {
            $cot_d['lang'] = $row_lang['name'];
            $DB->do_insert("service_desc", $cot_d);
          }
          
					//build seo_url
					$seo['sub'] = 'add';
					$seo['modules'] = $this->module;
					$seo['action'] = "detail";
					$seo['name_id'] = "itemID";
					$seo['item_id'] = $service_id;
					$seo['friendly_url'] = $cot_d['friendly_url'] ;
					$seo['lang'] = $lang;					
					$seo['query_string'] = "mod:".$seo['modules']."|act:".$seo['action']."|".$seo['name_id'].":".$service_id;
					$res_seo = $func->build_seo_url($seo);
					if($res_seo['existed']==1){
						$DB->query("UPDATE service_desc SET friendly_url='".$res_seo['friendly_url']."' WHERE service_id=".$service_id) ; 
					}
					
					//xoa cache
          $func->clear_cache();
					
          //insert adminlog
          $func->insertlog("Add", $_GET['act'], $fid);
          $mess = $vnT->lang['add_success'];
          if(isset($_POST['btn_preview']))	{
						$url = $this->linkUrl . "&sub=edit&id=$service_id&preview=1";
						$DB->query("Update service_desc SET display=0 WHERE service_id=$service_id ");
					}else{
						$url = $this->linkUrl . "&sub=add";						
					}
          $func->html_redirect($url, $mess);
        } else {
          $err = $func->html_err($vnT->lang['add_failt'] . $DB->debug());
        }
      }
    }
    $data['list_display'] = vnT_HTML::list_yesno("display", $data['display']);
    $data['list_service'] = list_service($data['service_id'], $lang);
		
		$data["html_short"] = $vnT->editor->doDisplay('short', $vnT->input['short'], '100%', '200', "Normal",$this->module,$dir); 
		$data["html_content"] = $vnT->editor->doDisplay('content', $vnT->input['content'], '100%', '500', "Default",$this->module,$dir);
		$data['link_upload'] = '?mod=media&act=popup_media&module='.$this->module.'&folder='.$this->module.'/'.$dir.'&obj=picture&type=image&TB_iframe=true&width=900&height=474';
			 
		$data['link_seo'] = $conf['rooturl']."xxx.html";
		if($data['friendly_url'])
			$data['link_mxh'] = $conf['rooturl'].$data['friendly_url'].".html";
				
    $data['err'] = $err;
    $data['link_action'] = $this->linkUrl . "&sub=add";
    /*assign the array to a template variable*/
    $this->skin->assign('data', $data);
    $this->skin->parse("edit");
    return $this->skin->text("edit");
  }

  /**
   * function do_Edit 
   * Cap nhat 
   **/
  function do_Edit ($lang)
  {
    global $vnT, $func, $DB, $conf;
    $id = (int) $vnT->input['id'];
    $w_thumb = ($vnT->setting['imgthumb_width']) ? $vnT->setting['imgthumb_width'] : 100;
		$dir = $vnT->func->get_dir_upload_module($this->module,$vnT->setting['folder_upload']);
    if ($vnT->input['do_submit']) {
      $data = $_POST;
      $cat_id = $vnT->input['cat_id'];
      $title = $vnT->input['title'];
      $gio = explode(":", $vnT->input['gio']);
      $ngay = explode("/", $vnT->input['ngay']);
      $date_post = mktime($gio[0], $gio[1], 0, $ngay[1], $ngay[0], $ngay[2]);
			 $picture =  $vnT->input['picture'];
      //check
      if ($vnT->input['service_id'] == $id)   $err = $func->html_err("Dịch vụ cha không hợp lệ");
     	 //upload
      if (empty($err)) {
         
        // lay hinh
        if ($vnT->setting['get_image']) 
				{
          $tmp_content = replace_img($data['content'], $dir);
          $content = $tmp_content['result'];
          if (empty($picture)) {
            if ($tmp_content['url'])
              $picture = $tmp_content['url'];
          }
        } else {
          $content = $_POST['content'];
        }
      } //end if err
      if (empty($err)) {
        $cot = array(
          'cat_id' => $cat_id ,   
					'picture' => $picture ,
          'parentid' => $vnT->input['service_id'] );
				
       
				$cot['date_post'] = $date_post;
				$cot['date_update'] = time();
				
        $kq = $DB->do_update("service", $cot, "service_id=$id");
        if ($kq) {
					
          $cot_d['title'] = $title;
	 				$cot_d['short'] = $DB->mySQLSafe($_POST['short']);
					$cot_d['content'] = $DB->mySQLSafe($content); 
					//SEO
					$cot_d['friendly_url'] = (trim($vnT->input['friendly_url'])) ? trim($vnT->input['friendly_url']) :  $func->make_url($_POST['title']);
					$cot_d['friendly_title'] = (trim($vnT->input['friendly_title'])) ? trim($vnT->input['friendly_title']) :  $title ;
					$cot_d['metakey'] = (trim($vnT->input['metakey'])) ? $vnT->input['metakey'] : $title;
					$cot_d['metadesc'] = (trim($vnT->input['metadesc'])) ? $vnT->input['metadesc'] : $func->cut_string($func->check_html($_POST['short'],'nohtml'),200,1); 						 
					
					$cot_d['display'] = $vnT->input['display'];
					$DB->do_update("service_desc", $cot_d, "service_id=$id and lang='{$lang}'");
					
					//build seo_url
					$seo['sub'] = 'edit';
					$seo['modules'] = $this->module;
					$seo['action'] = "detail";
					$seo['name_id'] = "itemID";
					$seo['item_id'] = $id;
					$seo['friendly_url'] = $cot_d['friendly_url'] ;
					$seo['lang'] = $lang;					
					$seo['query_string'] = "mod:".$seo['modules']."|act:".$seo['action']."|".$seo['name_id'].":".$id;
					$res_seo = $func->build_seo_url($seo);
					if($res_seo['existed']==1){
						$DB->query("UPDATE service_desc SET friendly_url='".$res_seo['friendly_url']."' WHERE lang='".$lang."' AND service_id=".$id) ;
					}
					
          //xoa cache
          $func->clear_cache();
          //insert adminlog
          $func->insertlog("Edit", $_GET['act'], $id);
          $err = $vnT->lang["edit_success"];
          
					if(isset($_POST['btn_preview']))	{
						$url = $this->linkUrl . "&sub=edit&id=$id&preview=1";
					}else{
						$url = $this->linkUrl . "&sub=edit&id=$id";
					} 
          $func->html_redirect($url, $err);
        } else
          $err = $func->html_err($vnT->lang["edit_failt"] . $DB->debug());
      }
    }
    $query = $DB->query("SELECT * FROM service  n , service_desc nd
												 WHERE n.service_id = nd.service_id  
												 AND lang='$lang' 
												 AND  n.service_id=$id ");
    if ($data = $DB->fetch_row($query)) 
		{
			$seo_name = ($lang=="vn") ? 'dich-vu' : 'service';		 
			
			if($vnT->input['preview']==1)	{
				 
				$link_preview = $conf['rooturl'].$seo_name."/".$data['service_id']."/".$data['friendly_url'].".html/?preview=1";
				$mess_preview = str_replace(array("{title}","{link}"),array($data['title'],$link_preview),$vnT->lang['mess_preview']);
				$data['js_preview'] = "tb_show('".$mess_preview."', '".$link_preview."&TB_iframe=true&width=1000&height=700',null)";
			}
			//dir
      if ($data['picture']) {
				$dir = substr($data['picture'], 0, strrpos($data['picture'], "/"));				
        $data['pic'] = get_pic_thumb($data['picture'], $w_thum) . "  <a href=\"javascript:del_picture('picture')\" class=\"del\">Xóa</a>";
				$data['style_upload'] = "style='display:none' ";
      } else {
        $data['pic'] = "";
      }
      $data['title'] = $func->txt_unHTML($data['title']);
      $data['picturedes'] = $func->txt_unHTML($data['picturedes']);
			
      $data['gio'] = date("H:i", $data['date_post']);
      $data['ngay'] = date("d/m/Y", $data['date_post']);
 			
			$data['checked_pic_social_network'] = ($data['pic_social_network']==1) ? " checked ='checked' " : "";
			
			$data['link_seo'] = $conf['rooturl']."xxx.html";
			if($data['friendly_url'])
				$data['link_mxh'] = $conf['rooturl'].$data['friendly_url'].".html";
			
			$data['img_mxh'] = '<div class="face-img"><img src="'.$conf['rooturl'].'image.php?image='.$conf['rooturl'].ROOT_UPLOAD.'/'.$data['picture'].'&width=120&height=120&cropratio=1:1" alt="" /></div>';
			
			
      $data['list_display'] = vnT_HTML::list_yesno("display", $data['display']);
    } else {
      $mess = $vnT->lang['not_found'] . " ID : " . $id;
      $url = $this->linkUrl;
      $func->html_redirect($url, $mess);
    }
    $data['list_service'] = list_service($data['parentid'], $lang);
    $data["html_short"] = $vnT->editor->doDisplay('short', $data['short'], '100%', '200', "Normal",$this->module,$dir); 
		$data["html_content"] = $vnT->editor->doDisplay('content', $data['content'], '100%', '500', "Default",$this->module,$dir);
		$data['link_upload'] = '?mod=media&act=popup_media&type=modules&module='.$this->module.'&folder='.$this->module.'/'.$dir.'&obj=picture&TB_iframe=true&width=900&height=474';

    $data['err'] = $err;
    $data['link_action'] = $this->linkUrl . "&sub=edit&id=$id";
    /*assign the array to a template variable*/
    $this->skin->assign('data', $data);
    $this->skin->parse("edit");
    return $this->skin->text("edit");
  }

  /**
   * function do_Del 
   * Xoa 1 ... n  gioi thieu 
   **/
  function do_Del ($lang)
  {
    global $func, $DB, $conf, $vnT;
    $id = (int) $vnT->input['id'];
    $ext = $vnT->input["ext"];
    $del = 0;
    $qr = "";
    if ($id != 0) {
      $ids = $id;
    }
    if (isset($vnT->input["del_id"])) {
      $ids = implode(',', $vnT->input["del_id"]);
    }
		
    $res = $DB->query("SELECT * FROM service WHERE service_id IN (" . $ids . ") ");
    if ($DB->num_rows($res))
    {			
      while ($row = $DB->fetch_row($res))  {
        $res_d = $DB->query("SELECT id FROM service_desc WHERE service_id=".$row['service_id']." AND lang<>'".$lang."' ");
				if(!$DB->num_rows($res_d))
				{
					$DB->query("DELETE FROM service WHERE  service_id=".$row['service_id'] );  
				}	
				$DB->query("DELETE FROM service_desc WHERE  service_id=".$row['service_id']." AND lang='".$lang."' ");	
      }
      $mess = $vnT->lang["del_success"];
			//xoa cache
      $func->clear_cache();
    } else  {
      $mess = $vnT->lang["del_failt"];
    }  
		
    $ext_page = str_replace("|", "&", $ext);
    $url = $this->linkUrl . "&{$ext_page}";
    $func->html_redirect($url, $mess);
  }

  /**
   * function render_row 
   * list cac record
   **/
  function render_row ($row_info, $lang)
  {
    global $func, $DB, $conf, $vnT;
    $row = $row_info;
    // Xu ly tung ROW
    $id = $row['service_id'];
    $row_id = "row_" . $id;
    $output['check_box'] = "<input type=\"checkbox\" name=\"del_id[]\" value=\"{$id}\" class=\"checkbox\" onclick=\"select_row('{$row_id}')\">";
    $link_edit = $this->linkUrl . "&sub=edit&id={$id}";
    $link_del = "javascript:del_item('" . $this->linkUrl . "&sub=del&id={$id}')";
    if (empty($row['picture']))
      $picture = "<i>No Image</i>";
    else {
      $url_view = "zoom.php?image=" . MOD_DIR_UPLOAD . "{$row['picture']}&title=Zoom";
      $picture = "<a href=\"#Zoom\" onClick=\"openPopUp('{$url_view}', 'Zoom', 700, 600, 'yes')\"><img src=\"" . MOD_DIR_UPLOAD . "{$row['picture']}\" width=50></a>";
    }
    $output['picture'] = $picture;
		
    $output['is_focus'] = vnT_HTML::list_yesno("is_focus[{$id}]", $row['is_focus'], "onchange='javascript:do_check($id)'");
    $title = "<a href='" . $link_edit . "'><b>".$func->HTML($row['title'])."</b></a>";
		if ($row['is_sub']) {
      $ext = '&nbsp;<img src="' . $vnT->dir_images . '/line3.gif" align="absmiddle" />&nbsp;';
      $title = $ext . $title ;
      $output['is_focus'] = "---";
    }
		
    $output['order'] = $ext . "<input name=\"txt_Order[{$id}]\" type=\"text\" size=\"2\" maxlength=\"2\" style=\"text-align:center\" value=\"{$row['s_order']}\"  onchange='javascript:do_check($id)' />";
    $output['title'] = $title;		
    
		$output['link'] = $row['friendly_url'].".html";
		
 		$info = "<div style='padding:2px;'>".$vnT->lang['date_post']." : <b>".@date("d/m/Y",$row['date_post'])."</b></div>";
 		$info .=  "<div style='padding:2px;'>".$vnT->lang['views']." : <strong>".(int)$row['views']."</strong></div>";
		$output['info'] = $info ;
		
    $output['date_post'] = date("d/m/Y", $row['date_post']);
		
    $link_display = $this->linkUrl.$row['ext_link']; 		
    if ($row['display'] == 1) {
      $display = "<a href='".$link_display."&do_hidden=$id' title='".$vnT->lang['click_do_hidden']."' ><img src=\"" . $vnT->dir_images . "/dispay.gif\" width=15  /></a>";
    } else {
      $display = "<a href='".$link_display."&do_display=$id'  title='".$vnT->lang['click_do_display']."' ><img src=\"" . $vnT->dir_images . "/nodispay.gif\"  width=15 /></a>";
    }
		
    $output['action'] = '<input name=h_id[]" type="hidden" value="' . $id . '" />';
    $output['action'] .= '<a href="' . $link_edit . '"><img src="' . $vnT->dir_images . '/edit.gif"  alt="Edit "></a>&nbsp;';
    $output['action'] .= $display . '&nbsp;';
    $output['action'] .= '<a href="' . $link_del . '"><img src="' . $vnT->dir_images . '/delete.gif"  alt="Delete "></a>';
    return $output;
  }

  /**
   * function do_Manage() 
   * Quan ly cac gioi thieu
   **/
  function do_Manage ($lang)
  {
    global $vnT, $func, $DB, $conf;
		$vnT->html->addStyleSheet( $vnT->dir_js . "/jquery_ui/themes/base/ui.all.css");
		$vnT->html->addScript($vnT->dir_js . "/jquery_ui/ui.core.js");		
		$vnT->html->addScript($vnT->dir_js . "/jquery_ui/ui.datepicker.js");  
    $vnT->html->addScriptDeclaration("
	 		$(function() {
				$('#date_begin').datepicker({
						showOn: 'button',
						buttonImage: '".$vnT->dir_images."/calendar.gif',
						buttonImageOnly: true,
						changeMonth: true,
						changeYear: true
					});
					$('#date_end').datepicker({
						showOn: 'button',
						buttonImage: '".$vnT->dir_images."/calendar.gif',
						buttonImageOnly: true,
						changeMonth: true,
						changeYear: true
					});

			});
		
		"); 
		
    //update
    if ($vnT->input["do_action"]) {
      //xoa cache
      $func->clear_cache();
      if ($vnT->input["del_id"])     $h_id = $vnT->input["del_id"];
      switch ($vnT->input["do_action"]) {
        case "do_edit":
          if (isset($vnT->input["txt_Order"]))        $arr_order = $vnT->input["txt_Order"];
          //if (isset($vnT->input["have_form"]))       $have_form = $vnT->input["have_form"];
          if (isset($vnT->input["is_focus"]))         $is_focus = $vnT->input["is_focus"];
          $mess .= "- " . $vnT->lang['edit_success'] . " ID: <strong>";
          $str_mess = "";
          for ($i = 0; $i < count($h_id); $i ++) {
            $dup['s_order'] = $arr_order[$h_id[$i]];
            //$dup['have_form'] = $have_form[$h_id[$i]];
            $dup['is_focus'] = $is_focus[$h_id[$i]];
            $ok = $DB->do_update("service", $dup, "service_id=" . $h_id[$i]);
            if ($ok) {
              $str_mess .= $h_id[$i] . ", ";
            }
          }
          $mess .= substr($str_mess, 0, - 2) . "</strong><br>";
          $err = $func->html_mess($mess);
          ;
        break;
        case "do_hidden":
          $mess .= "- " . $vnT->lang['hidden_success'] . " ID: <strong>";
          for ($i = 0; $i < count($h_id); $i ++) {
            $dup['display'] = 0;
            $ok = $DB->do_update("service_desc", $dup, "service_id={$h_id[$i]} AND lang='$lang' ");
            if ($ok) {
              $str_mess .= $h_id[$i] . ", ";
            }
          }
          $mess .= substr($str_mess, 0, - 2) . "</strong><br>";
          $err = $func->html_mess($mess);
        break;
        case "do_display":
          $mess .= "- " . $vnT->lang['display_success'] . "  ID: <strong>";
          for ($i = 0; $i < count($h_id); $i ++) {
            $dup['display'] = 1;
            $ok = $DB->do_update("service_desc", $dup, "service_id={$h_id[$i]}  AND lang='$lang' ");
            if ($ok) {
              $str_mess .= $h_id[$i] . ", ";
            }
          }
          $mess .= substr($str_mess, 0, - 2) . "</strong><br>";
          $err = $func->html_mess($mess);
        break;
      }
    }
		
		if((int)$vnT->input["do_display"]) {				
			$ok = $DB->query("Update service_desc SET display=1 WHERE   lang='$lang' AND service_id=".$vnT->input["do_display"]);
			if($ok){
				$mess .= "- " . $vnT->lang['display_success'] . "  ID: <strong>".$vnT->input["do_display"] . "</strong><br>";	
				$err = $func->html_mess($mess);
			}        
			//xoa cache
      $func->clear_cache();
		}
		if((int)$vnT->input["do_hidden"]) {				
			$ok = $DB->query("Update service_desc SET display=0 WHERE   lang='$lang' AND  service_id=".$vnT->input["do_hidden"]);
			if($ok){
				$mess .= "- " . $vnT->lang['display_success'] . "  ID: <strong>".$vnT->input["do_hidden"] . "</strong><br>";	
				$err = $func->html_mess($mess);
			}    
			//xoa cache
      $func->clear_cache();
		}
		
		$search = ($vnT->input['search']) ? $vnT->input['search'] : "service_id";
    $keyword = ($vnT->input['keyword']) ? $vnT->input['keyword'] : "";
		$adminid = ((int) $vnT->input['adminid']) ?   $vnT->input['adminid'] : 0;
		$date_begin = ($vnT->input['date_begin']) ?  $vnT->input['date_begin'] : "";
		$date_end = ($vnT->input['date_end']) ?  $vnT->input['date_end'] : "";
		
    $where = "";
		$ext = "";
		if(!empty($adminid)){
			$where .=" and adminid=$adminid ";
			$ext_page.="adminid=$adminid|";
			$ext.="&adminid={$adminid}";
		}
		
		if($date_begin || $date_end )
		{
			$tmp1 = @explode("/", $date_begin);
			$time_begin = @mktime(0, 0, 0, $tmp1[1], $tmp1[0], $tmp1[2]);
			
			$tmp2 = @explode("/", $date_end);
			$time_end = @mktime(23, 59, 59, $tmp2[1], $tmp2[0], $tmp2[2]);
			
			$where.=" AND (date_post BETWEEN {$time_begin} AND {$time_end} ) ";
			$ext.="&date_begin=".$date_begin."&date_end=".$date_end;
			$ext_page.= "date_begin=".$date_begin."|date_end=".$date_end."|";
		}
		
    if(!empty($keyword)){
			switch($search){
				case "service_id" : $where .=" and  n.service_id = $keyword ";   break;
				case "date_post" : $where .=" and DATE_FORMAT(FROM_UNIXTIME(date_post),'%d/%m/%Y') = '{$keyword}' "; break;
				default :$where .=" and $search like '%$keyword%' ";break;		
			}
			
			$ext_page.="keyword=$keyword|keyword=$keyword|";
			$ext.="&search={$search}&keyword={$keyword}";
		}
		
    
    $p = ((int) $vnT->input['p']) ? $p = $vnT->input['p'] : 1;
    $n = ($conf['record']) ? $conf['record'] : 30;
    $query = $DB->query("SELECT n.service_id
						FROM service n , service_desc nd
						WHERE n.service_id=nd.service_id
						AND lang='$lang'
						AND parentid=0
						$where  ");
    $totals = intval($DB->num_rows($query));
    $num_pages = ceil($totals / $n);
    if ($p > $num_pages) $p = $num_pages; 
		if ($p < 1)   $p = 1;
    $start = ($p - 1) * $n;
    $nav = $func->paginate($totals, $n, $ext, $p);
    $table['link_action'] = $this->linkUrl . "{$ext}&p=$p";
		$ext_link = $ext."&p=$p" ;
		
    $table['title'] = array(
      'check_box' => "<input type=\"checkbox\" name=\"all\" class=\"checkbox\"  />|5%|center" , 
      'order' => $vnT->lang['order'] . "|10%|center" , 
      'picture' => $vnT->lang['picture'] . "|13%|center" , 
      'title' => $vnT->lang['title'] . "||left" , 
			'link' => "Link|20%|left" , 
			'info' =>$vnT->lang['infomartion']. "|15%|left", 
      'action' => "Action|12%|center");
		
   $sql = "SELECT *
						FROM service n , service_desc nd
						WHERE n.service_id=nd.service_id
						AND lang='$lang'
						AND parentid=0 
						$where 
						ORDER BY s_order ASC, date_post DESC LIMIT $start,$n";
    $reuslt = $DB->query($sql);
    if ($DB->num_rows($reuslt))
    {
      $row = $DB->get_array($result);
      $stt = 0;
      for ($i = 0; $i < count($row); $i ++)
      {
				$row[$i]['ext_link'] = $ext_link ;
				$row[$i]['ext_page'] = $ext_page;
        $row_info = $this->render_row($row[$i], $lang);
        $row_field[$stt] = $row_info;
        $row_field[$stt]['stt'] = ($i + 1);
        $row_field[$stt]['row_id'] = "row_" . $row[$i]['service_id'];
        $row_field[$stt]['ext'] = "";
        //check sub
        $sql_sub = "SELECT * 
						FROM service n , service_desc nd
						WHERE n.service_id=nd.service_id
						AND  lang='$lang'
						AND parentid=".$row[$i]['service_id']." 
						$where 
						ORDER BY  s_order ASC,  date_post DESC"; 
        $res_sub = $DB->query($sql_sub);
        if ($DB->num_rows($res_sub))
        {
          $row_sub = $DB->get_array($res_sub);
          for ($j = 0; $j < count($row_sub); $j ++)
          {
						$row_sub[$j]['ext_link'] = $ext_link ;
				    $row_sub[$j]['ext_page'] = $ext_page;
            $row_sub[$j]['is_sub'] = 1;
            $stt ++;
            $row_info = $this->render_row($row_sub[$j], $lang);
            $row_field[$stt] = $row_info;
            $row_field[$stt]['stt'] = ($j + 1);
            $row_field[$stt]['row_id'] = "row_" . $row_sub[$j]['service_id'];
            $row_field[$stt]['ext'] = "";            
         
				  } // end for sub
        } // end if sub
        $stt ++;
      } // end for
      $table['row'] = $row_field;
    } else  {
       $table['row'] = array();
      $table['extra'] = "<div align=center class=font_err >" . $vnT->lang['no_have_service'] ."</div>";
    }
		
    $table['button'] = '<input type="button" name="btnHidden" value=" ' . $vnT->lang['hidden'] . ' " class="button" onclick="do_submit(\'do_hidden\')">&nbsp;';
    $table['button'] .= '<input type="button" name="btnDisplay" value=" ' . $vnT->lang['display'] . ' " class="button" onclick="do_submit(\'do_display\')">&nbsp;';
    $table['button'] .= '<input type="button" name="btnEdit" value=" ' . $vnT->lang['update'] . ' " class="button" onclick="do_submit(\'do_edit\')">&nbsp;';
    $table['button'] .= '<input type="button" name="btnDel" value=" ' . $vnT->lang['delete'] . ' " class="button" onclick="del_selected(\'' . $this->linkUrl . '&sub=del&ext=' . $ext_page . '\')">';
    $table_list = $func->ShowTable($table);
    $data['table_list'] = $table_list;
    $data['totals'] = $totals;
    $data['link_fsearch'] = $this->linkUrl;
		
		$data['date_begin'] = $date_begin;
		$data['date_end'] = $date_end;
    $data['list_search'] = List_Search($search);
 		
    $data['keyword'] = $keyword;
		
    $data['err'] = $err;
    $data['nav'] = $nav;
    /*assign the array to a template variable*/
    $this->skin->assign('data', $data);
    $this->skin->parse("manage");
    return $this->skin->text("manage");
  }

  /**
   * function do_FormContact() 
   * 
   **/
  function do_FormContact ($lang)
  {
    global $vnT, $func, $DB, $conf;
    $id = (int) $vnT->input['service_id'];
    $is_insert = 1;
    if ($vnT->input['do_submit']) {
      $dup['form_contact'] = $DB->mySQLSafe($_POST['form_contact']);
      $ok = $DB->do_update("service_desc", $dup, "service_id=$id and lang='$lang'");
      if ($ok) {
        //xoa cache
        $func->clear_cache();
        //insert adminlog
        $func->insertlog("Edit Form", $_GET['act'], $id);
        $err = $func->html_mess($vnT->lang["edit_success"]);
      } else {
        $err = $func->html_err($vnT->lang["edit_failt"]);
      }
    }
    $result = $DB->query("SELECT * FROM service 
												 WHERE  lang='$lang' 
												 AND  service_id=$id ");
    if ($data = $DB->fetch_row($result)) {
      $data["name"] = "Form liên lạc của <b class='font_err'>" . $func->HTML($data['title']) . "</b>";
    }
    $data["html_content"] = $vnT->editor->doDisplay('form_contact', $data['form_contact'], '100%', '400', "Normal");
    $data['err'] = $err;
    $data['link_action'] = $this->linkUrl . "&sub=form_contact&service_id=" . $id;
    /*assign the array to a template variable*/
    $this->skin->assign('data', $data);
    $this->skin->parse("form_contact");
    return $this->skin->text("form_contact");
  }
  // end class
}
?>
