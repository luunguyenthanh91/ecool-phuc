<!-- BEGIN: edit -->
<script language=javascript>
$(document).ready(function() {
	$('#myForm').validate({
		rules: {			
				city: {
					required: true
				},
				
				name: {
					required: true,
					minlength: 3
				},
				code: {
					required: true 
				}
	    },
	    messages: {
	    	city: {
						required: "{LANG.err_select_required}"
				} ,
				name: {
						required: "{LANG.err_text_required}",
						minlength: "{LANG.err_length} 3 {LANG.char}" 
				} ,
				code: {
						required: "{LANG.err_text_required}" 
				} 
		}
	});
});
</script>
{data.err}
<form action="{data.link_action}" method="post" name="myForm" id="myForm"  class="validate">
 <table width="100%"  border="0" cellspacing="2" cellpadding="2"  class="admintable">

      <tr class="form-required" >
            <td  class="row1" width="30%" > Thành phố : </td>
            <td class="row0" >{data.list_city} </td>
		  </tr>
      
 
			
       <tr class="form-required" >
            <td  class="row1"> Mã quận huyện : </td>
            <td class="row0"><input name="code" id="code" type="text"  size="10" maxlength="250" value="{data.code}" /> 
            </td>
		  </tr>      
         
          <tr class="form-required"  >
            <td  class="row1">Tên quận huyện : </td>
            <td class="row0"><input name="name" type="text" id="name" size="50" maxlength="250" value="{data.name}" /></td>
          </tr>
          

          <tr align="center">
            <td  class="row1">&nbsp;</td>
						<td class="row0" >
						<input type="hidden" name="h_code" value="{data.code}" />
            <input type="hidden" name="do_submit" value="1" />
            <input type="submit" name="btnSubmit" value="Submit" class="button" / >
            <input type="reset" name="Submit2" value="Reset" class="button" />           
             </td></tr>
        </table>
    </form>
<br>
<!-- END: edit -->

<!-- BEGIN: manage -->
{data.err}
<form action="" method="post">

<br />
<table width="100%"  border="0" align="center" cellspacing="2" cellpadding="2" class="tableborder">
	
  <tr>
      <td align="right" width="20%"> <strong>Thành phố : </strong></td>
      <td align="left">{data.list_city} </td>
  </tr>
  
  
   <tr>
		<td align="right" ><strong>Tìm theo :</strong></td>
	  <td >{data.list_search} <strong> Từ khóa :</strong> <input name="keyword"  value="{data.keyword}"type="text"> <input name="btnSearch" type="submit" value=" Search ! "></td>
  </tr>
  <tr>
    <td align="right" ><strong>Trạng thái  :</strong></td>
    <td >{data.list_display}</td>
  </tr>
  <tr>
  	<td align="right"> <strong>Tổng số  : </strong> </td>
    <td ><span class="font_err"><strong>{data.totals}</strong></span></td>
  </tr>
</table>
</form>
<br />
{data.table_list}
<table width="100%"  border="0" align="center" cellspacing="1" cellpadding="1">
  <tr>
    <td  height="30">{data.nav}</td>
  </tr>
</table>
<br />
<!-- END: manage -->