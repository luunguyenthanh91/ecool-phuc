<?php
/*================================================================================*\
|| 							Name code : order.php 		 		            	  ||
||  				Copyright @2008 by Thai Son - CMS vnTRUST                     ||
\*================================================================================*/
/**
 * @version : 2.0
 * @date upgrade : 09/01/2009 by Thai Son
 **/
if (! defined('IN_vnT')) {
  die('Hacking attempt!');
}
$vntModule = new vntModule();

class vntModule
{
  var $output = "";
  var $skin = "";
  var $linkUrl = "";
  var $module = "member";
  var $action = "upgrade";

  /**
   * function vntModule ()
   * Khoi tao 
   **/
  function vntModule ()
  {
    global $Template, $vnT, $func, $DB, $conf;
    require_once ("function_" . $this->module . ".php");
		loadSetting();
    $this->skin = new XiTemplate(DIR_MODULE . DS . $this->module . "_ad" . DS . "html" . DS . $this->action . ".tpl");
    $this->skin->assign('LANG', $vnT->lang);
    $lang = ($vnT->input['lang']) ? $lang = $vnT->input['lang'] : $func->get_lang_default();
    $this->linkUrl = "?mod=" . $this->module . "&act=" . $this->action . "&lang=" . $lang;
    $vnT->html->addStyleSheet("modules/" . $this->module . "_ad/css/" . $this->module . ".css");
    switch ($vnT->input['sub']) {
      
      case 'del':
        $this->do_Del($lang);
      break;
      default:
        $nd['f_title'] = 'Quản lý nâng cấp thành viên';
        $nd['content'] = $this->do_Manage($lang);
      break;
    }
    $nd['menu'] = $func->getToolbar_Small($this->module, $this->action, $lang);
    $nd['row_lang'] = $func->html_lang("?mod=" . $this->module . "&act=" . $this->action, $lang);
    $Template->assign("data", $nd);
    $Template->parse("box_main");
    $vnT->output .= $Template->text("box_main");
  }

   

  /**
   * function do_Task 
   *   
   **/
  function do_Del ($lang)
  {
    global $func, $DB, $conf, $vnT;
    global $func, $DB, $conf, $vnT;
    $id = (int) $vnT->input['id'];
    $ext = $vnT->input["ext"];
    $del = 0;
    $qr = "";
    if ($id != 0) {
      $ids = $id;
    }
    if (isset($vnT->input["del_id"])) {
      $ids = implode(',', $vnT->input["del_id"]);
    }
    $query = 'DELETE FROM member_upgrade WHERE id IN (' . $ids . ')';
    if ($ok = $DB->query($query)) {
      $mess = $vnT->lang["del_success"];
    } else
      $mess = $vnT->lang["del_failt"];
    $ext_page = str_replace("|", "&", $ext);
    $url = $this->linkUrl . "&{$ext_page}";
    $func->html_redirect($url, $mess);
  }

  /**
   * function render_row 
   * list cac record
   **/
  function render_row ($row_info, $lang)
  {
    global $func, $DB, $conf, $vnT;
    $row = $row_info;
    // Xu ly tung ROW
    $id = $row['id'];
    $row_id = "row_" . $id;
    $output['check_box'] = vnT_HTML::checkbox("del_id[]", $id, 0, " ");
    
    $link_del = "javascript:del_item('" . $this->linkUrl . "&sub=del&id=" . $id . "&ext=" . $row['ext_page'] . "')";
    $output['id'] = "<a href=\"{$link_edit}\"><strong>" . $row['id'] . "</strong></a>";
    ;
    
		$res_m = $DB->query("SELECT mem_id,username,full_name,email FROM members WHERE mem_id=".$row['mem_id']);
		if($row_m = $DB->fetch_row($res_m))
		{
			$link_member =  '?mod=member&act=member&sub=edit&id=' . $row['mem_id'];
			$link_send =  '?mod=member&act=member&sub=send_mail&id=' . $row['mem_id'];
			
			$member  = "User : <a href='".$link_member."' target='_blank' ><strong>".$row['username']. "</strong></a> <b class=font_err>[ID: ".$row['mem_id']."]</b>";
  		$member .= "<br>Nhóm hiện tại : <strong >" . $vnT->setting['g_name'][$row['mem_group']]  . "</strong>";
			$member .= "<br>Họ tên : <strong >" . $func->HTML($row['full_name']) . "</strong>";
  	  $member .=  "<br>Email : <strong>" . $row['email'] . "</strong> <a href='" . $link_send . "' target='_blank' ><img src=\"{$vnT->dir_images}/send_mail.gif\" width=15  alt=\"Send\"/></a>";
		
		}
		
		$output['member'] = $member ;
    
    $output['info_upgrade'] = $vnT->setting['g_name'][$row['g_current']] ."<br> => <br>". $vnT->setting['g_name'][$row['g_upgrade']] ;    
    $output['note']  = $func->HTML($row['note']);
		 
		$output['info_update'] = "Ngày đăng ký: " .date("d/m/Y", $row['date_post']);
		$output['info_update'] .= ($row['status']==1) ? "<div style='padding:2px 0px'>Trạng thái: <b class=blue >Đã nâng cấp</b></div>"  : "<div style='padding:2px'>Trạng thái: <b class=red >Chờ xét duyệt</b></div>";
		$date_update = ($row['date_update']) ? date("d/m/Y", $row['date_update']) : " --- " ;
		$output['info_update'] .= "<div style='padding:2px 0px'>Ngày cập nhật : " .$date_update."</div>";
   
	 
	  if ($row['display'] == 1) {
      $display = "<img src=\"{$vnT->dir_images}/dispay.gif\" width=15  />";
    } else {
      $display = "<img src=\"{$vnT->dir_images}/nodispay.gif\"  width=15 />";
    }
    $output['action'] = '<input name=h_id[]" type="hidden" value="' . $id . '" />';
    $output['action'] .= '<a href="' . $link_view . '" target="_blank" ><img src="' . $vnT->dir_images . '/but_view.gif"  alt="View " width=22></a>&nbsp;';
    $output['action'] .= '<a href="' . $link_del . '"><img src="' . $vnT->dir_images . '/delete.gif"  alt="Delete "></a>';
    return $output;
  }

  /**
   * function do_Manage() 
   * Quan ly 
   **/
  function do_Manage ($lang)
  {
    global $vnT, $func, $DB, $conf;
    //update
    if ($vnT->input["do_action"]) {
      //xoa cache
      $func->clear_cache();
      if ($vnT->input["del_id"])     $h_id = $vnT->input["del_id"];
      switch ($vnT->input["do_action"]) {
        case "do_edit":
					$mess .= "- Nâng cấp thành công Accout ID: <strong>";
          for ($i = 0; $i < count($h_id); $i ++) { 
						$res_check = $DB->query("SELECT * FROM member_upgrade WHERE id=".$h_id[$i]);						
						if($row_ck = $DB->fetch_row($res_check))
						{							
							$DB->query("Update members SET mem_group=".$row_ck['g_upgrade']." WHERE mem_id=".$row_ck['mem_id']);							
							$str_mess .= $row_ck['mem_id'] . ", ";							
						}
            $dup['status'] = 1;
						$dup['date_update'] = time();
						$dup['adminid '] = $vnT->admininfo['adminid'];
            $ok = $DB->do_update("member_upgrade", $dup, "id=" . $h_id[$i]);
             
          }
          $mess .= substr($str_mess, 0, - 2) . "</strong><br>";
          $err = $func->html_mess($mess);
        break;
        case "do_hidden":
          $mess .= "- " . $vnT->lang['hidden_success'] . " ID: <strong>";
          for ($i = 0; $i < count($h_id); $i ++) {
            $dup['display'] = 0;
            $ok = $DB->do_update("member_upgrade", $dup, "id=" . $h_id[$i]);
            if ($ok) {
              $str_mess .= $h_id[$i] . ", ";
            }
          }
          $mess .= substr($str_mess, 0, - 2) . "</strong><br>";
          $err = $func->html_mess($mess);
        break;
        case "do_display":
          $mess .= "- " . $vnT->lang['display_success'] . "  ID: <strong>";
          for ($i = 0; $i < count($h_id); $i ++) {
            $dup['display'] = 1;
            $ok = $DB->do_update("member_upgrade", $dup, "id=" . $h_id[$i]);
            if ($ok) {
              $str_mess .= $h_id[$i] . ", ";
            }
          }
          $mess .= substr($str_mess, 0, - 2) . "</strong><br>";
          $err = $func->html_mess($mess);
        break;
      }
    }
    $p = ((int) $vnT->input['p']) ? $p = $vnT->input['p'] : 1;
    $n = ($conf['record']) ? $conf['record'] : 30;
    $search = (isset($vnT->input['search'])) ? $vnT->input['search'] : "mem_id";
    $keyword = (isset($vnT->input['keyword'])) ? $vnT->input['keyword'] : "";
		$status = (isset($vnT->input['status'])) ? $vnT->input['status'] : "-1";
		
		if($status!="-1")
		{
			$where .= " AND u.status=$status ";
			$ext .= "&status={$status} ";
      $ext_page .= "status=$status|";
		}
		if (! empty($keyword)) {
      switch ($search) {
        case "mem_id":
          $where .= " and u.mem_id=$keyword ";
        break;         
        default:
          $where .= " and $search like '%$keyword%' ";
        break;
      }
      $ext .= "&search={$search}&keyword={$keyword}";
      $ext_page .= "search=$search|keyword=$keyword|";
    }
		
    $query = $DB->query("SELECT u.mem_id
						FROM member_upgrade u, members m 
						WHERE m.mem_id=u.mem_id		$where   ");
    $totals = intval($DB->num_rows($query));
    $num_pages = ceil($totals / $n);
    if ($p > $num_pages)
      $p = $num_pages;
    if ($p < 1)
      $p = 1;
    $start = ($p - 1) * $n;
    $nav = $func->paginate($totals, $n, $ext, $p);
    $table['link_action'] = $this->linkUrl .$ext;
    $table['title'] = array(
      'check_box' => "<input type=\"checkbox\" name=\"checkall\" id=\"checkall\" class=\"checkbox\" />|5%|center" , 
      'member' => "Thành viên|30%|left" , 
      'info_upgrade' => "Thông tin Nâng cấp|20%|center" , 
			'note' => "Ghi chú|25%|left" ,  
			'info_update' => "Thông tin cập nhật|20%|left" , 
      );
    $sql = "SELECT u.*, m.username, m.full_name, m.email ,m.mem_group, m.phone,m.company
						FROM member_upgrade u, members m 
						WHERE m.mem_id=u.mem_id		
						$where
						ORDER BY  id DESC  LIMIT $start,$n";
    //print "sql = ".$sql."<br>";
    $reuslt = $DB->query($sql);
    if ($DB->num_rows($reuslt)) {
      $row = $DB->get_array($result);
      for ($i = 0; $i < count($row); $i ++) {
        $row_info = $this->render_row($row[$i], $lang);
        $row_field[$i] = $row_info;
        $row_field[$i]['stt'] = ($i + 1);
        $row_field[$i]['row_id'] = "row_" . $row[$i]['id'];
        $row_field[$i]['ext'] = "";
      }
      $table['row'] = $row_field;
    } else {
      $table['row'] = array();
      $table['extra'] = "<div align=center class=font_err >Chưa có yêu cầu nâng cấp nào</div>";
    }
		
	  $table['button'] = '<input type="button" name="btnEdit" value=" Nâng cấp thành viên đã chọn " class="button" onclick="do_submit(\'do_edit\')">&nbsp;';
		
    $table['button'] .= '<input type="button" name="btnDel" value=" ' . $vnT->lang['delete'] . ' " class="button" onclick="del_selected(\'' . $this->linkUrl . '&sub=del&ext=' . $ext_page . '\')">';
    $table_list = $func->ShowTable($table);
    $data['table_list'] = $table_list;
    $data['totals'] = $totals; 
		$data['keyword'] = $keyword;
    $data['list_search'] = List_Search($search);
		$data['list_status'] = vnT_HTML::selectbox("status", array(
      '-1' => '-- Tất cả--' , '0' => 'Chờ xét duyệt' , '1' => 'Đã nâng cấp'
    ), $status,"","onChange='submit();'");
		
    $data['err'] = $err;
    $data['nav'] = $nav;
    /*assign the array to a template variable*/
    $this->skin->assign('data', $data);
    $this->skin->parse("manage");
    return $this->skin->text("manage");
  }
  // end class
}
?>