<?php
session_start();
define('IN_vnT',1);
define('DS', DIRECTORY_SEPARATOR);

require_once("../../../_config.php");
include($conf['rootpath']."includes/class_db.php");
$DB = new DB;
//Functions
include ($conf['rootpath'] . 'includes/class_functions.php');
include($conf['rootpath'] . 'includes/admin.class.php');
$func  = new Func_Admin;
$conf = $func->fetchDbConfig($conf);

if(empty($_SESSION['admin_session']))	{
	die("Ban khong co quyen truy cap trang nay") ;
}



	require_once($conf['rootpath'].'libraries/excel/PHPExcel.php');
  // HTTP headers
	$file_name = 'member_'.time() ;
	// Create new PHPExcel object
	$objPHPExcel = new PHPExcel();

	// Set properties
	$objPHPExcel->getProperties()->setCreator("TRUSTvn")
		->setLastModifiedBy("Maarten Balliauw")
		->setTitle("Office 2007 XLSX Test Document")
		->setSubject("Office 2007 XLSX Test Document")
		->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
		->setKeywords("office 2007 openxml php")
		->setCategory("Test result file");

	// Rename sheet
	$objPHPExcel->getActiveSheet()->setTitle('THONG KE KHACH HANG');


		//group
		$arr_group = array();
		$res_g = $DB->query("SELECT * FROM mem_group WHERE display=1 ORDER BY g_order ASC , g_id ASC ");
		while($row_g = $DB->fetch_row($res_g) )
		{
			$arr_group[$row_g['g_id']] = $func->HTML($row_g['g_name']) ;
		}

		$lang = ($_GET['lang']) ? $_GET['lang'] : "vn";
		$m_status = ($_GET['m_status']) ? $_GET['m_status'] : "-1";
		$mem_group = ($_GET['mem_group']) ? $_GET['mem_group'] : 0;
	  $keyword = ($_GET['keyword']) ? $_GET['keyword'] : "";
    $search = ($_GET['search']) ? $_GET['search'] : "mem_id";
		$date_begin = ($_GET['date_begin']) ?  $_GET['date_begin'] : "";
		$date_end = ($_GET['date_end']) ?  $_GET['date_end'] : "";


		$where = "";

		if ($mem_group) {
			$where .= " and mem_group=$mem_group ";
		}
		if ($m_status != "-1") {
			$where .= " and m_status=$m_status ";
		}

		if($date_begin || $date_end )
		{
			$tmp1 = @explode("/", $date_begin);
			$time_begin = @mktime(0, 0, 0, $tmp1[1], $tmp1[0], $tmp1[2]);

			$tmp2 = @explode("/", $date_end);
			$time_end = @mktime(23, 59, 59, $tmp2[1], $tmp2[0], $tmp2[2]);

			$where.=" AND (date_order BETWEEN {$time_begin} AND {$time_end} ) ";
		}

		if (! empty($keyword)) {
			switch ($search) {
				case "mem_id":
					$where .= " and mem_id=$keyword ";
					break;
				case "date_join":
					$where .= " and DATE_FORMAT(FROM_UNIXTIME(date_join),'%d/%m/%Y') = '$keyword' ";
					break;
				case "birthday":
					$where .= " and substring( birthday, 1, 5 ) = '$keyword' ";
					break;
				default:
					$where .= " and $search like '%$keyword%' ";
					break;
			}
		}



	
	$sql = "SELECT * FROM members  WHERE m_status <> -1 $where ORDER BY  mem_id DESC  ";
 
	// 
	$reuslt = $DB->query($sql);
	if ($num = $DB->num_rows($reuslt))
	{

		// Add some data
		$objPHPExcel->setActiveSheetIndex(0)
			->setCellValue('A1', 'Mem ID')
			->setCellValue('B1', 'Tài khoản')
			->setCellValue('C1', 'Họ tên')
			->setCellValue('D1', 'Email')
			->setCellValue('E1', 'Điện thoại')
			->setCellValue('F1', 'Địa chỉ')
			->setCellValue('G1', 'Sinh nhật')
			->setCellValue('H1', 'Loại TV')
			->setCellValue('I1', 'Tiền TK')
			->setCellValue('J1', 'Ngày đăng ký');

		$dong =2;
		$stt=0;
		while($row = $DB->fetch_row($reuslt))
		{
			$stt++;
			$mem_id = $row['mem_id'] ;
			$username = $row['username'] ;

			$full_name = $func->HTML($row['full_name']) ;

			$phone =  $row['phone'];
			$email =  $row['email'];
			$address = $func->HTML($row['address']) ;
			$group_name = $arr_group[$row['mem_group']];
			$num_account = $row['num_account'];
			$birthday = $row['birthday'];
			$mem_point = (int)$row['mem_point'];
			$date_join = @date("H:i, d/m/Y",$row['date_join'])  ;


			$objPHPExcel->setActiveSheetIndex(0)
				->setCellValue('A'.$dong, $mem_id)
				->setCellValue('B'.$dong, $username)
				->setCellValue('C'.$dong, $full_name)
				->setCellValue('D'.$dong, $email)
				->setCellValue('E'.$dong, $phone)
				->setCellValue('F'.$dong, $address)
				->setCellValue('G'.$dong, $birthday)
				->setCellValue('H'.$dong, $group_name)
				->setCellValue('I'.$dong, $mem_point)
				->setCellValue('J'.$dong, $date_join);
					
			$dong++;
		}
	}

$objPHPExcel->getActiveSheet()->getStyle('I2:I'.$dong)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);

$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(10);
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(30);
$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(30);
$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(40);
$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(15);


$objPHPExcel->getActiveSheet()->getStyle('A1:J1')->getFont()->setBold(true);
$DB->close();

// Redirect output to a client's web browser (Excel5)
header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename="'.$file_name.'.xls"');
header('Cache-Control: max-age=0');

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output');

exit;

?>