<!-- BEGIN: add -->
<script language="javascript" >
$(document).ready(function() {

	jQuery.validator.addMethod("mem_group", function( value, element ) {
		if(value=='' || value==0)
		{
			return false;
		}else{
			return true ;
		}	
	}, "{LANG.err_select_required}");
	
	$('#myForm').validate({
		rules: {	
				mem_group : "mem_group",		
				username: {
					required: true,
					minlength: 3
				},
				password: {
					required: true,
					minlength: 3
				},
				email: {
					required: true,
					email: true
				} 
				
	    },
	    messages: {	    	
				username: {
						required: "{LANG.err_text_required}",
						minlength: "{LANG.err_length} 3 {LANG.char}" 
				},
				password: {
						required: "{LANG.err_text_required}",
						minlength: "{LANG.err_length} 3 {LANG.char}" 
				},
				email: {
						required: "{LANG.err_text_required}",
						email: "{LANG.err_email_invalid}" 
				} 
				
		}
	});
});
</script>
{data.err}
<form action="{data.link_action}" method="post" name="myForm" id="myForm" enctype="multipart/form-data" class="validate">
 
  <table width="100%"  border="0"  cellspacing="1" cellpadding="1" class="admintable">
		 <tr class="form-required" >
     <td class="row1" width="20%">{LANG.group_member}: </td>
     <td class="row0">{data.list_mem_group}</td>
    </tr>

    
    <tr class="form-required">
     <td class="row1" width="20%">{LANG.username}: </td>
     <td class="row0"><input name="username" id="username" type="text" size="50" maxlength="250" value="{data.username}" class="textfiled"></td>
    </tr>
    <tr class="form-required">
     <td class="row1" >{LANG.password}: </td>
     <td class="row0"><input name="password" id="password" type="password" size="50" maxlength="250" class="textfiled"></td>
    </tr>
    <tr class="form-required">
     <td class="row1" >Email: </td>
     <td class="row0"><input name="email" id="email" type="text" size="50" maxlength="250" value="{data.email}" class="textfiled"></td>
    </tr>
    
    <tr class="form-required">
     <td class="row1" >{LANG.full_name}: </td>
     <td class="row0"><input name="full_name" id="full_name" type="text" size="50" maxlength="250" value="{data.full_name}" class="textfiled">  </td>
    </tr>
    <tr >
     <td class="row1" >{LANG.gender}: </td>
     <td class="row0">{data.list_gender}</td>
    </tr>
    <tr >
     <td class="row1" >{LANG.birthday}: </td>
     <td class="row0">{data.list_day} / {data.list_month}/{data.list_year}</td>
    </tr>
    <tr >
     <td class="row1" >Avatar: </td>
     <td class="row0"><input name="image" type="file" id="image"  size="30" maxlength="250"/></td>
    </tr>
    <tr >
     <td class="row1" >{LANG.address}: </td>
     <td class="row0"><input name="address" type="text" size="50" maxlength="250" value="{data.address}" class="textfiled"></td>
    </tr>
    <tr >
     <td class="row1" >{LANG.city}: </td>
     <td class="row0">{data.list_city}
     <input name="country" id="country" value="VN"  type="hidden" />
     </td>
    </tr> 
    <tr >
     <td class="row1" >{LANG.state}: </td>
     <td class="row0"><span id="ext_state">{data.list_state}</span></td>
    </tr>
    
    
     <tr >
     <td class="row1" >{LANG.phone}: </td>
     <td class="row0"><input name="phone" type="text" size="50" maxlength="250" value="{data.phone}" class="textfiled"></td>
    </tr>
 
		
			
		<tr align="center">
    <td class="row1" >&nbsp; </td>
			<td class="row0" >
      
				<input type="hidden" name="do_submit"	 value="1" />
				<input type="submit" name="btnAdd" value="Submit" class="button">
				<input type="reset" name="btnReset" value="Reset" class="button">
			</td>
		</tr>
	</table>
</form>


<br>
<!-- END: add -->

<!-- BEGIN: edit -->
<script language="javascript" >
$(document).ready(function() {

	jQuery.validator.addMethod("mem_group", function( value, element ) {
		if(value=='' || value==0)
		{
			return false;
		}else{
			return true ;
		}	
	}, "{LANG.err_select_required}");
	
	$('#myForm').validate({
		rules: {	
				mem_group : "mem_group",		
				username: {
					required: true,
					minlength: 3
				},
				 
				email: {
					required: true,
					email: true
				} 
				
	    },
	    messages: {	    	
				username: {
						required: "{LANG.err_text_required}",
						minlength: "{LANG.err_length} 3 {LANG.char}" 
				},
				 
				email: {
						required: "{LANG.err_text_required}",
						email: "{LANG.err_email_invalid}" 
				} 
				
		}
	});
});
</script>
{data.err}
<form action="{data.link_action}" method="post" name="myForm" id="myForm" enctype="multipart/form-data" class="validate">

<fieldset>
  <legend class="font_err">&nbsp;<strong>{LANG.info_member}</strong>&nbsp;</legend>
  <table width="100%"  border="0"  cellspacing="1" cellpadding="1" class="admintable">
		 <tr class="form-required" >
     <td class="row1" >{LANG.group_member}: </td>
     <td class="row0">{data.list_mem_group}</td>
    </tr>

    <tr class="form-required">
     <td class="row1" width="20%">{LANG.username}: </td>
     <td class="row0"><input name="username" id="username" type="text" size="50" maxlength="250" value="{data.username}" class="textfiled"></td>
    </tr>
    <tr class="form-required">
     <td class="row1" >Email: </td>
     <td class="row0"><input name="email" id="email" type="text" size="50" maxlength="250" value="{data.email}" class="textfiled"></td>
    </tr>
    <tr >
     <td class="row1" >{LANG.password}: </td>
     <td class="row0"><input name="password" type="password" size="50" maxlength="250"  class="textfiled"><br><span class="font_err">{LANG.note_password}</span></td>
    </tr>
    <tr class="form-required">
     <td class="row1" >{LANG.full_name}: </td>
     <td class="row0"><input name="full_name" id="full_name" type="text" size="50" maxlength="250" value="{data.full_name}" class="textfiled"></td>
    </tr>
    <tr >
     <td class="row1" >{LANG.gender}: </td>
     <td class="row0">{data.list_gender}</td>
    </tr>
    <tr >
     <td class="row1" >{LANG.birthday}: </td>
     <td class="row0">{data.list_day} / {data.list_month}/{data.list_year}</td>
    </tr>
    <tr >
     <td class="row1" >Avatar: </td>
     <td class="row0">{data.pic}<input name="image" type="file" id="image"  size="30" maxlength="250"/></td>
    </tr>
    <tr >
     <td class="row1" >{LANG.address}: </td>
     <td class="row0"><input name="address" type="text" size="50" maxlength="250" value="{data.address}" class="textfiled"></td>
    </tr>
    <tr >
     <td class="row1" >{LANG.city}: </td>
     <td class="row0">{data.list_city}
     <input name="country" id="country" value="VN"  type="hidden" />
     </td>
    </tr> 
    <tr >
     <td class="row1" >{LANG.state}: </td>
     <td class="row0"><span id="ext_state">{data.list_state}</span></td>
    </tr>
    
    
     <tr >
     <td class="row1" >{LANG.phone}: </td>
     <td class="row0"><input name="phone" type="text" size="50" maxlength="250" value="{data.phone}" class="textfiled"></td>
    </tr>

	 <tr >
     <td class="row1"  >Tiền Nạp: </td>
     <td class="row0"><input name="mem_point" id="mem_point" type="text" size="10" maxlength="250" value="{data.mem_point}" class="textfiled"> <strong>VNĐ</strong><br><span class="font_err">(Lưu ý : Nếu chỉnh sửa số tiền này thì phần <a href="?mod=member&act=money&search=username&keyword={data.username}" style="text-decoration:underline;">thống kê tiền của thành viên này</a> sẽ không còn chính xác .)</span></td>
    </tr>

	</table>
</fieldset>

<br />

<fieldset>
  <legend class="font_err">&nbsp;<strong>{LANG.status_member}</strong>&nbsp;</legend>
   <table width="100%"  border="0"  cellspacing="1" cellpadding="1" class="admintable">

    <tr class="row1">
     <td  ><strong>{LANG.date_join}:</strong> {data.date_join}</td>
    </tr>
    <tr class="row0">
     <td  ><strong>{LANG.num_login}:</strong> {data.num_login}</td>
    </tr>
    <tr class="row1">
     <td  ><strong>{LANG.date_last_login}:</strong> {data.date_last_login}</td>
    </tr>
    <tr class="row0">
     <td  ><strong>{LANG.status}:</strong> {data.status_name}</td>
    </tr>
 
  </table>
</fieldset>
<br />

<fieldset>
  <legend class="font_err">&nbsp;<strong>{LANG.option}</strong>&nbsp;</legend>
   <table width="100%"  border="0"  cellspacing="1" cellpadding="1" class="admintable">

    <tr>
      <td align="center" class="row1">{LANG.announce_for_member}: </td>
      <td colspan="2" class="row0"><textarea name="note" cols="60" rows="5" class="textarea">{data.note}</textarea></td>
    </tr>
    <tr >
     <td align="center"  class="tableborder" width="33%"><a href="{data.link_ban}"><strong>{LANG.banned_account}</strong></a></td>
      <td align="center"  class="tableborder" ><a href="{data.link_unban}"><strong>{LANG.active_account}</strong></a> </td>
      <td align="center"  class="tableborder"  width="33%"><a href="{data.link_del}"><strong>{LANG.del_account}</strong></a> </td>
    </tr>
 
  </table>
</fieldset>

  <p align="center">
	  <input type="hidden" name="do_submit"	 value="1" />
		<input name="btnUpdate" type="submit"  value="{LANG.update}" class="button "/>
	 </p>
</form>

<br>
<!-- END: edit -->


<!-- BEGIN: send_mail -->
{data.err}
<form action="{data.link_action}" method="post" enctype="multipart/form-data" name="myForm"  class="validate">
<table width="100%"  border="0"  cellspacing="1" cellpadding="1" class="admintable">
	<tr>
      <td colspan="2"  align="center" class="row0" > {data.send_for} </td>
    </tr>
	<tr class="form-required" >
			<td class="row1" width="20%" >Subject : </td>
			<td  align="left" class="row0"><input type="text" name="subject" size="60" value="{data.subject}" ></td>
		</tr>
	<tr>
  <tr >
 	 <td class="row1" width="25%" >Content : </td>
		<td  class="row1" >{data.html_content} </td>
		</tr>
	<tr>

		<tr align="center">
    <td class="row1" >&nbsp; </td>
			<td class="row0" >
				<input type="hidden" name="do_submit"	 value="1" />
				<input type="submit" name="btnSubmit" value="Submit" class="button">
				<input type="reset" name="btnReset" value="Reset" class="button">
			</td>
		</tr>
	</table>
</form>
<br>
<!-- END: send_mail -->

<!-- BEGIN: manage -->
<form action="{data.link_fsearch}" method="post" name="fSearch">
<table width="100%" border="0" cellspacing="2" cellpadding="2" align="center" class="tableborder">
  
  <tr>
    <td  align="left">{LANG.group_member}</td>
    <td  align="left">{data.list_mem_group} &nbsp; <strong>Trạng thái kích hoạt: </strong> {data.list_status}</td>
  </tr>
 
  <tr>
  <td align="left">{LANG.search_for} : &nbsp;&nbsp;&nbsp;  </td>
  <td align="left">{data.list_search} &nbsp;&nbsp; {LANG.keyword}  : &nbsp;
    <input name="keyword"  value="{data.keyword}"type="text" class="textfiled">
    <input name="btnSearch" type="submit" value=" Search " class="button"></td>
  </tr>
  <tr>
    <td width="20%" align="left">{LANG.totals}  :</td>
    <td width="80%" align="left"><b class='font_err'>{data.totals}</b> &nbsp; | &nbsp;<a href="{data.link_excel}" target="_blank"><img src="{DIR_IMAGE}/icon_excel.gif"  alt="excel" align="absmiddle"/> <b>Xuất ra file Excel theo điều kiện Search</b> </a></td>
  </tr>
  
  </table>
</form>
{data.err}
<br />
{data.table_list}
<br />
<table width="100%"  border="0" align="center" cellspacing="1" cellpadding="1" class="bg_tab">
  <tr>
    <td  height="25">{data.nav}</td>
  </tr>
</table>
<br />
<!-- END: manage -->


<!-- BEGIN: order_history -->
<form action="{data.link_fsearch}" method="post" name="fSearch">
<table width="100%" border="0" cellspacing="2" cellpadding="2" class="tableborder" align=center>
  <tr>
    <td >
			<strong class="font_err">Thông tin khách hàng</strong>
	</td>
	</tr>
  <tr>
    <td >
{data.info_customer}
	</td>
	</tr>
  <tr>
    <td >
			<strong class="font_err">Thông tin mua hàng</strong>
	</td>
	</tr>
  <tr>
    <td >
<table width="100%" border="0" cellspacing="2" cellpadding="2" >
  <tr>
    <td width="45%"><strong>Tổng số hóa đơn </strong> : <strong class="font_err">{data.totals}</strong></td>
	  <td >Tổng giá trị đã mua : <strong class="font_err">{data.total_price}</strong></td>
  </tr>
  
	<tr>
    <td ><strong>Trạng thái </strong>: {data.list_status}</td>
	  <td >Tổng giá trị đang mua : <strong class="font_err">{data.total_price_wait}</strong></td>
  </tr>

  </table>
  </td>
  </tr>

  </table>
</form>
{data.err}
<br />
{data.table_list}
<br />
<table width="100%"  border="0" align="center" cellspacing="1" cellpadding="1" class="bg_tab">
  <tr>
    <td  height="25">{data.nav}</td>
  </tr>
</table>
<br />
<!-- END: order_history -->