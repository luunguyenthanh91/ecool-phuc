<!-- BEGIN: edit -->
<script language="javascript" >
$(document).ready(function() {
	$('#myForm').validate({
		rules: {
				 		
				g_name: {
					required: true,
					minlength: 3
				} 				
	    },
	    messages: {	 						   	
				g_name: {
						required: "{LANG.err_text_required}",
						minlength: "{LANG.err_length} 3 {LANG.char}" 
				}
				
		}
	});
});
 
</script>
{data.err}
<form action="{data.link_action}" method="post" enctype="multipart/form-data" name="myForm" id="myForm"  class="validate">
  <table width="100%"  border="0"  cellspacing="1" cellpadding="1" class="admintable">

    <tr class="form-required">
     <td class="row1" width="20%" nowrap="nowrap">{LANG.group_name}: </td>
     <td class="row0"><input name="g_name" id="g_name" type="text" size="50" maxlength="250" value="{data.g_name}"></td>
    </tr>
		<tr >
      
  
    <tr >
     <td class="row1" width="20%">{LANG.description}: </td>
     <td class="row0"><textarea name="description" cols="60" rows="10" class="textarea">{data.description}</textarea></td>
    </tr>
    
    <tr>
		<td class="row1">{LANG.group_default}  : </td>
		<td  class="row0">{data.list_default}</td>
	</tr>
			
		<tr align="center">
    <td class="row1" >&nbsp; </td>
			<td class="row0" >
				<input type="hidden" name="do_submit"	 value="1" />
				<input type="submit" name="btnAdd" value="Submit" class="button">
				<input type="reset" name="Submit2" value="Reset" class="button">
			</td>
		</tr>
	</table>
</form>


<br>
<!-- END: edit -->

<!-- BEGIN: manage -->
<form action="{data.link_fsearch}" method="post" name="fSearch">
<table width="100%" border="0" cellspacing="2" cellpadding="2" align="center" class="tableborder">
  <tr>
    <td width="15%" align="left">{LANG.totals}: &nbsp;</td>
    <td width="85%" align="left"><b class="font_err">{data.totals}</b></td>
  </tr>
</table>
</form>
{data.err}
<br />
{data.table_list}
<br />
<table width="100%"  border="0" align="center" cellspacing="1" cellpadding="1" class="bg_tab">
  <tr>
    <td  height="25">{data.nav}</td>
  </tr>
</table>
<br />
<!-- END: manage -->