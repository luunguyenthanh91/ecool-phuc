<?php
/*================================================================================*\
|| 							Name code : gallery_setting.php 		 		            	  ||
||  				Copyright @2008 by Thai Son - CMS vnTRUST                     ||
\*================================================================================*/
/**
 * @version : 2.0
 * @date upgrade : 09/01/2009 by Thai Son
 **/
if (! defined('IN_vnT'))
{
  die('Hacking attempt!');
}

$vntModule = new vntModule();
class vntModule
{
  var $output = "";
  var $skin = "";
  var $linkUrl = "";
  var $module = "estore";
	var $action = "setting";
	
  /**
   * function vntModule ()
   * Khoi tao 
   **/
  function vntModule ()
  {
    global $Template, $vnT, $func, $DB, $conf;
    require_once ("function_".$this->module.".php");

    $lang = ($vnT->input['lang']) ? $lang = $vnT->input['lang'] : $func->get_lang_default();
    $this->linkUrl = "?mod=".$this->module."&act=".$this->action."&lang=" . $lang;
		
    switch ($vnT->input['sub'])
    {      
      case 'edit':
	   		 $nd['content']=$this->do_Edit($lang);
      break;
			default:
        $nd['f_title'] = $vnT->lang['manage_setting_estore'];
        $nd['content'] = $this->do_Manage($lang);
        break;
    }
    $nd['menu'] = $func->getToolbar_Small($this->module,$this->action, $lang);
		
    $Template->assign("data", $nd);
    $Template->parse("box_main");
    $vnT->output .= $Template->text("box_main");
  
  }    

	/**
   * function do_Edit 
   * Cap nhat gioi thieu 
   **/
  function do_Edit ($lang)
  {
    global $vnT, $func, $DB, $conf;

    if ($vnT->input['do_submit']) {
      $data = $_POST;
			$res_check = $DB->query("select * from estore_setting where id=1 ");
			$row = $DB->fetch_row($res_check);
			foreach ($row as $key => $value){
				if ($key!='id'){
					$dup[$key] = $vnT->input[$key];
				}
			}
			$ok = $DB->do_update("estore_setting",$dup,"id=1");
			
			//xoa cache
      $func->clear_cache();
			//insert adminlog
      $func->insertlog("Setting", $_GET['act'], $id);
			$err = $vnT->lang["edit_setting_estore_success"];
			
		}
		$url = $this->linkUrl;
		$func->html_redirect($url, $err);
  }
	
  /**
   * function do_Manage() 
   * Quan ly cac gioi thieu
   **/
  function do_Manage ($lang)
  {
    global $vnT, $func, $DB, $conf;    
		
		$result = $DB->query("select * from estore_setting where id=1");
		$data = $DB->fetch_row($result) ;
		
		$data['nophoto'] = vnT_HTML::list_yesno("nophoto",$data['nophoto']);
		$data['active_bqt'] = vnT_HTML::list_yesno("active_bqt",$data['active_bqt']);		
		$data['comment']	= vnT_HTML::selectbox("comment", array(
																											0 => $vnT->lang['no'],
																											1 => 'Cho phép tất cả',
																											2 => 'Chỉ cho phép thành viên'
																										), $data['comment'])  ;
			
		
		$data['link_action'] = $this->linkUrl."&sub=edit";
		return $this->html_manage($data) ;
  }

//========== SKIN ======================	
function html_manage($data){
global $func,$DB,$conf,$vnT;
return<<<EOF
{$data['err']}
<form action="{$data['link_action']}" method="post" name="f_setting" id="f_setting" >
 <table width="100%"  border="0"  cellspacing="1" cellpadding="1" class="admintable">
 	<tr height=20 class="row_title" > 
   <td colspan="2" ><strong>{$vnT->lang['setting_module_estore']}</strong></td>
  </tr>
	
	<tr >
    <td width="30%" align="right" class="row1"> <strong>{$vnT->lang['active_bqt']} : </strong></td>
    <td  align="left" class="row0">{$data['active_bqt']}</td>
  </tr>
	
	<tr >
    <td width="20%" align="right" class="row1"> <strong>{$vnT->lang['nophoto']} : </strong></td>
    <td  align="left" class="row0">{$data['nophoto']}</td>
  </tr>
	
	<tr >
    <td width="20%" align="right" class="row1"> <strong>{$vnT->lang['imgthumb_width']} : </strong></td>
    <td  align="left" class="row0"><input name="imgthumb_width" type="text" size="20" maxlength="250" value="{$data['imgthumb_width']}" class="textfiled"></td>
  </tr>
	
	<tr >
    <td width="20%" align="right" class="row1"> <strong>{$vnT->lang['imgdetail_width']} : </strong></td>
    <td  align="left" class="row0"><input name="imgdetail_width" type="text" size="20" maxlength="250" value="{$data['imgdetail_width']}" class="textfiled"></td>
  </tr>
	
  
  
	<tr >
    <td width="20%" align="right" class="row1"> <strong>{$vnT->lang['n_list']} : </strong></td>
    <td  align="left" class="row0"><input name="n_list" type="text" size="20" maxlength="250" value="{$data['n_list']}" class="textfiled"></td>
  </tr>
	
	<tr >
    <td width="20%" align="right" class="row1"> <strong>{$vnT->lang['comment']} : </strong></td>
    <td  align="left" class="row0">{$data['comment']}</td>
  </tr>
	
	<tr >
    <td width="20%" align="right" class="row1"> <strong>{$vnT->lang['n_comment']} : </strong></td>
    <td  align="left" class="row0"><input name="n_comment" type="text" size="20" maxlength="250" value="{$data['n_comment']}" class="textfiled"></td>
  </tr>
	
   
  </table>
		
<p align="center">
<input type="hidden" name="do_submit" value="1">
<input name="btnEdit" type="submit" value="Submit" class="button"></p>

</form>
<br />
EOF;
}


  // end class
}

?>
