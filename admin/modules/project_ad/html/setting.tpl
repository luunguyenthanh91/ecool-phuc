<!-- BEGIN: manage -->
<form action="{data.link_action}" method="post" name="f_config" id="f_config" >
{data.err}
  <table width="100%" border="0" cellspacing="1" cellpadding="1" class="admintable">
  	<tr class="row_title" >
  		<td colspan="2"><strong class="font_title">Search Engine Optimization (SEO):</strong></td>
  	</tr>
    <tr>
      <td class="row1" valign="top">Modules Slogan :</td>
      <td class="row0">
        <input name="slogan" id="slogan" type="text" size="70" maxlength="250" value="{data.slogan}" class="textfield">
      </td>
    </tr>
    <tr>
      <td class="row1" valign="top" >Friendly Title :</td>
      <td class="row0"><input name="friendly_title" id="friendly_title" type="text" size="70" maxlength="250" value="{data.friendly_title}" class="textfield"></td>
    </tr>
    <tr>
      <td class="row1">Meta Keyword :</td>
      <td class="row0">
        <input name="metakey" id="metakey" type="text" size="70" maxlength="250" value="{data.metakey}" class="textfield">
      </td>
    </tr>
    <tr>
      <td class="row1">Meta Description : </td>
      <td class="row0"><textarea name="metadesc" id="metadesc" rows="3" cols="50" style="width:90%" class="textarea">{data.metadesc}</textarea></td>
    </tr>
    <tr>
      <td class="row1">Rebuild Link SEO: </td>
      <td class="row0">
        <input type="button" name="Rebuild" value=" &nbsp; Rebuild Link &nbsp;" class="button" onclick="location.href='{data.link_rebuild}'"/>
      </td>
    </tr>
  </table><br>
<table width="100%" border="0" cellspacing="1" cellpadding="1" class="admintable">
	<tr class="row_title">
		<td colspan="2"><strong class="font_title">{LANG.config_project}</strong></td>
	</tr>
  <tr>
    <td class="row1" width="35%">{LANG.n_grid} :</td>
    <td class="row0"><input name="n_grid" size="20" type="text" class="textfiled" value="{data.n_grid}" onKeyPress="return is_num(event,'n_grid')"></td>
  </tr>
  <tr>
    <td class="row1">{LANG.show_nophoto}:</td>
    <td class="row0">{data.nophoto}</td>
  </tr>
	<tr>
    <td class="row1"  >Hình Nophoto : </td>
    <td class="row0">
      <div id="ext_pic_nophoto" class="picture" >{data.image_nophoto}</div>
      <input type="hidden" name="pic_nophoto"	 id="pic_nophoto" value="{data.pic_nophoto}" />
      <div id="btnU_pic_nophoto" class="div_upload" {data.style_upload}>
        <div class="button2">
          <div class="image">
            <a title="Add an Image" class="thickbox" id="add_image" href="{data.link_upload}" >Chọn hình</a>
          </div>
        </div>
      </div>
    </td>
  </tr> 
  <tr>
    <td class="row1">{LANG.img_width_grid}:</td>
    <td class="row0"><input name="img_width_grid" size="20" type="text" class="textfiled" value="{data.img_width_grid}" onKeyPress="return is_num(event,'img_width_grid')"></td>
  </tr>
  <tr>
    <td  class="row1">{LANG.img_width} :</td>
    <td class="row0"><input name="img_width" size="20" type="text" class="textfiled" value="{data.img_width}"  onkeypress="return is_num(event,'img_width')"></td>
  </tr>
  <tr>
    <td class="row1" >{LANG.folder_upload} :</td>
    <td class="row0">{data.folder_upload}</td>
  </tr>
   
</table>
<br>
<table width="100%"  border="0"  cellspacing="1" cellpadding="1" class="admintable">
	<tr class="row_title" >
		<td colspan="2" ><strong class="font_title">{LANG.config_detail}</strong></td>
	</tr>

	<tr>
    <td width="35%" class="row1" >{LANG.imgdetail_width} :</td>
    <td class="row0"><input name="imgdetail_width" size="20" type="text" class="textfiled" value="{data.imgdetail_width}"  onkeypress="return is_num(event,'imgdetail_width')"></td>
  </tr>
  <tr>
    <td class="row1">{LANG.n_comment} :</td>
    <td class="row0"><input name="n_comment" size="20" type="text" class="textfiled" value="{data.n_comment}" onKeyPress="return is_num(event,'n_comment')"></td>
  </tr>

  <tr>
    <td class="row1">{LANG.n_other} :</td>
    <td class="row0"><input name="n_other" size="20" type="text" class="textfiled" value="{data.n_other}" onKeyPress="return is_num(event,'n_other')"></td>
  </tr>

</table>
<p align="center">
			<input type="hidden" name="num" value="{data.num}">
      <input type="hidden" name="do_submit" value="1">
      <input type="submit" name="btnEdit" value="Update >>" class="button">
</p>
</form>
<br />
<!-- END: manage -->