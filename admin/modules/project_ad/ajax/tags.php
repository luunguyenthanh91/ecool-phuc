<?php
	define('IN_vnT',1);
	define('DS', DIRECTORY_SEPARATOR);
	
	require_once("../../../../_config.php"); 
	include($conf['rootpath']."includes/class_db.php"); 
	$DB = new DB;	
	//Functions
	include ($conf['rootpath'] . 'includes/class_functions.php');
	include($conf['rootpath'] . 'includes/admin.class.php');
	$func  = new Func_Admin;
	$conf = $func->fetchDbConfig($conf);

	require_once ($conf['rootpath']."includes" . DS . 'class.XiTemplate.php');
	
	define('DIR_SKIN', $conf['rootpath']."admin" . DS . 'skins' . DS . $conf['skin_acp']);
	define('DIR_IMAGE', $conf['rooturl']."admin/skins/". $conf['skin_acp']."/images");
	$Template = new XiTemplate(DIR_SKIN . DS . 'global.tpl');
	
	$lang = ($_POST['lang']) ? $_POST['lang'] : "vn";
	
	switch ($_POST['do']){
		case "edit" : $jsout = do_Edit($lang) ;break;
		case "submit_edit" : $jsout = do_SubmitEdit($lang) ;break;
  	default  : $jsout = "Error" ;break;
	}
	function  do_Edit ($lang){
		global $vnT,$DB,$func,$conf;
		$tag_id = (int)$_POST['tag_id'] ;
		$sql = "SELECT * FROM project_tags WHERE tag_id=".$tag_id;
    $result = $DB->query($sql);
		$jsout = "";
    if ($data = $DB->fetch_row ($result)){
			$jsout = "<td align=\"center\" >&nbsp;</td>
             <td valign=\"top\" ><input name=\"name_".$tag_id."\" id=\"name_".$tag_id."\" type=\"text\" style=\"width:50%;\" value=\"".$data["name"]."\"  /></td>						
            <td align=\"center\"   ><input name=\"btnEdit\" type=\"button\" class=\"button\" value=\" Edit \" onClick=\"do_submitEdit(".$tag_id.",'".$lang."')\" /></td>";
 		}
		return $jsout;
	}
	function do_SubmitEdit ($lang){
		global $vnT,$DB,$func,$conf;
		$tag_id = (int)$_POST['tag_id'];
		if($tag_id){
			$cot['name'] = trim($_POST['name']);		 
			$cot['name_compare'] = $func->get_keyword($cot['name']);
			$cot['name_search'] = strtolower($func->utf8_to_ascii($cot['name']));
 			$cot['date_post'] = time(); 
			$DB->do_update("project_tags",$cot,"tag_id=$tag_id");		 
		}
		$sql = "SELECT * FROM project_tags WHERE tag_id=".$tag_id;
    $result = $DB->query($sql);
    if ($row = $DB->fetch_row ($result)){
			$id = $row['tag_id'];
			$link_del = "javascript:del_item('?mod=news&act=tag&sub=del&id=" . $id . "')";
			$data['name'] = $row['name'];
			$data['rate'] = $row['rate'];
			$link_display = $linkUrl.$row['ext_link']; 		
			if ($row['display'] == 1) {
				$display = "<a href='".$link_display."&do_hidden=$id' title='".$vnT->lang['click_do_hidden']."' ><img src=\"skins/default/images/dispay.gif\" width=15  /></a>";
			} else {
				$display = "<a href='".$link_display."&do_display=$id'  title='".$vnT->lang['click_do_display']."' ><img src=\"skins/default/images/nodispay.gif\"  width=15 /></a>";
			}
			$data['action'] = '<input name="h_id[]" type="hidden" value="' . $id . '" />';    
			$data['action'] .= "<a href=\"#Edit\" onClick=\"do_Edit(".$id.",'".$lang."')\" ><img src=\"skins/default/images/edit.gif\"  alt=\"Edit \"></a>&nbsp;";
			$data['action'] .= $display . '&nbsp;';
			$data['action'] .= '<a href="'.$link_del.'"><img src="skins/default/images/delete.gif"  alt="Delete "></a>';
		}
		$jsout = '<td align="center" ><input class="checkbox" type="checkbox" value="'.$id.'" name="del_id[]"></td>
 							<td >'.$data['name'].'</td>
							<td align="center" >'.$data['action'].'</td>';
		return $jsout;
	}
	flush();
	echo $jsout;
	exit();
?>