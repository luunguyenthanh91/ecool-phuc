<!-- BEGIN: manage -->
<form action="{data.link_fsearch}" method="post" name="fSearch">
  <table width="100%" border="0" cellspacing="2" cellpadding="2" align="center" class="tableborder">
  	<tr>
    <td  align="left">Tên tag: &nbsp;</td>
    <td  align="left"><input name="keyword" id="keyword"  value="{data.keyword}"type="text"> <input name="btnSearch" type="submit" value=" Search ! "></td>
  </tr>
    <tr>
      <td width="15%" align="left">{LANG.totals}: &nbsp;</td>
      <td width="85%" align="left"><b class="font_err">{data.totals}</b></td>
    </tr>
  </table>
</form>
{data.err} <br />
<form id="manage" name="manage" method="post" action="{data.link_action}">
  <table width="100%" border="0" cellspacing="0" cellpadding="0" class="bg_tbl">
    <tr>
      <td><table  border="0" cellspacing="2" cellpadding="2">
        <tr>
          <td>{data.button}</td>
        </tr>
      </table></td>
    </tr>
    <tr>
      <td>
      <table cellspacing="1" class="adminlist">
        <thead>
          <tr height="25">    
           	<th width="5%" align="center" ><input type="checkbox" name="checkall" id="checkall" class="checkbox" /></th>
             <th width="70%" align="center" >{LANG.name}</th>
            <th width="15%"  align="center" >Action</th>
          </tr>
        </thead>        
        <tbody>
  
       	  <tr class="row0" id="ext_tags" style="display:none" >
            <td align="center" >&nbsp;</td>
             <td valign="top" ><input name="name" id="name" type="text" style="width:50%;" value="{data.name}" /></td>
            <td align="center"   ><input name="btnAdd" type="submit" class="button" value=" Thêm " /></td>
          </tr>
       
          <!-- BEGIN: html_row -->
          <tr class="{row.class}" id="{row.row_id}">
            <td align="center" >{row.check_box}</td> 
            <td >{row.name}</td>
            <td align="center" >{row.action}</td>
          </tr>
          <!-- END: html_row -->
          <!-- BEGIN: html_row_no -->
          <tr class="row0" >
            <td  colspan="11" align="center" class="font_err" >{mess}</td>
          </tr>
          <!-- END: html_row_no -->
        </tbody>
      </table>
      </td>
    </tr>
    <tr>
      <td ><table  border="0" cellspacing="2" cellpadding="2">
          <tr>
            <td>{data.button}</td>
          </tr>
        </table></td>
    </tr>
  </table>
  <input type="hidden" name="do_action" id="do_action" value="" >
</form>
<br />
<table width="100%"  border="0" align="center" cellspacing="1" cellpadding="1" class="bg_tab">
  <tr>
    <td  height="25">{data.nav}</td>
  </tr>
</table>
<script>
function do_Edit (tag_id,lang)
{
	hideobj('ext_tags') ;		
	var ext_display = 'row_'+tag_id;
	$.ajax({
		 type: "POST",
		 url: 'modules/news_ad/ajax/tags.php',
		 data: "do=edit&tag_id="+tag_id+"&lang="+lang,
		 success: function(html){
				$("#"+ext_display).html(html);
		 }
	 });	
	
}

function do_submitEdit (tag_id,lang)
{

	var ext_display = 'row_'+tag_id;
	name = $('#name_'+tag_id).val(); 	
	var text_data = '&name='+name+'&lang='+lang;
	
	$.ajax({
		 type: "POST",
		 url: 'modules/news_ad/ajax/tags.php',
		 data: "do=submit_edit&tag_id="+tag_id+text_data,
		 success: function(html){
				$("#"+ext_display).html(html);
		 }
	 });	
	
}
</script>
<!-- END: manage -->