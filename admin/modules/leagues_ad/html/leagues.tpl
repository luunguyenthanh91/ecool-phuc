<!-- BEGIN: edit -->
<link href="{DIR_JS}/metabox/seo-metabox.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="{DIR_JS}/metabox/seo-metabox.js"></script>
<script language="javascript" >
var wpseo_lang = 'en';
var wpseo_meta_desc_length = '155';
var wpseo_title = 'title';
var wpseo_content = 'content';
var wpseo_title_template = '%%title%%';
var wpseo_metadesc_template = '';
var wpseo_permalink_template = '{CONF.rooturl}%postname%.html';
var wpseo_keyword_suggest_nonce = 'a7c4d81c79'; 
$(document).ready(function() {
	$('#myForm').validate({
		rules: {					
			cat_id: {
					required: true
				},	
				team1: {
					required: true,
				},
        team2: {
          required: true,
          
        },
        cauthu_ghiban_team1: {
          required: true,
          
        },
        cauthu_ghiban_team2: {
          required: true,
          
        },
        // time_ghiban_team1: {
        //   required: true,
          
        // },
        // time_ghiban_team2: {
        //   required: true,
          
        // }
        
	    },
	    messages: {	    	
				cat_id: {
						required: "{LANG.err_select_required}" 
				},
				team1: {
						required: "{LANG.err_text_required}"						
				},
        team2: {
            required: "{LANG.err_text_required}"            
        },
        cauthu_ghiban_team1: {
            required: "{LANG.err_text_required}"            
        },
        cauthu_ghiban_team2: {
            required: "{LANG.err_text_required}"            
        }, 
        // time_ghiban_team1: {
        //     required: "{LANG.err_text_required}"            
        // },
        // time_ghiban_team2: {
        //     required: "{LANG.err_text_required}"            
        // }
		}
	});
	
	{data.js_preview} 
	
});

</script>

<script type="text/javascript">
  $(document).ready(function() {
    $('.team1').select2({ width: '31%' });
    $('.team2').select2({ width: '31%' });
    $('.player1').select2({ width: '40%' });
    $('.player2').select2({ width: '40%' });
});

</script>

{data.err}
<form action="{data.link_action}" method="post" enctype="multipart/form-data" name="myForm" id="myForm" class="validate" >
  <div class="boxAdminForm">
      <div class="block-left">
          <table width="100%"  border="0" cellspacing="2" cellpadding="2" align=center class="admintable">
              <tr >
                  <td width="15%" class="row1" nowrap="nowrap">Chọn giải đấu&nbsp;: </td>
                  <td  align="left" class="row0">{data.listcat}</td>
              </tr>
              
              <tr>
                <td class="row1" valign="top">Đội A :</td>
                <td class="row0">
                    <select class="team1" name="team1">
                      {data.list_team1}
                    </select>
                </td>
                
              </tr>

              <tr>
                <td class="row1" valign="top">Đội B :</td>
                <td class="row0">
                  <select class="team2" name="team2">
                      {data.list_team2}
                    </select>
                </td>

              </tr>
              <tr >
                        <td class="row1" nowrap="nowrap">Tỷ số</td>
                        <td align="left" class="row0">
                          <table>
                            <tr>
                              <td style="text-align: center;">
                                <input name="tyso_team1" style="text-align: center;" type="text" size="7" placeholder="Đội A" maxlength="10" value="{data.tyso_team1}"><br><small>Đội A</small> 
                              </td>
<td> &nbsp;&nbsp;</td>
                              <td style="text-align: center;">
                                <input name="tyso_team2" style="text-align: center;" type="text" size="7" placeholder="Đội B" maxlength="10" value="{data.tyso_team2}"><br><small>Đội B </small>
                              </td>
                            </tr>
                          </table>
                         
                           
                          
                        </td>
                    </tr>
              
          </table>


      </div>
      <br/>
      

            <div class="desc_content">
                <table width="100%"  border="0" cellspacing="2" cellpadding="2" align=center class="admintable">
                    
                    <tr>
                        <td  class="row1" nowrap="nowrap">Nơi thi đấu</td>
                        <td align="left" class="row0"><input name="location" type="text" id="location" size="54" maxlength="250" value="{data.location}" ></td>
                    </tr>
                    <tr>
                        <td class="row1" nowrap="nowrap">Ngày thi đấu</td>
                        <td align="left" class="row0"><input name="gio" type="text" size="5" maxlength="5" value="{data.gio}">
                            &nbsp;,&nbsp;
                            <input  id="ngay" name="ngay" type="text" size="10" maxlength="10" value="{data.ngay}"></td>
                    </tr>
                    <tr>
                        <td class="row1">{LANG.display_leagues}</td>
                        <td align="left" class="row0">{data.list_display}</td>
                    </tr>

                    <!-- <tr>
                        <td class="row1">Hiển thị bên phải</td>
                        <td align="left" class="row0">{data.list_show_home}</td>
                    </tr> -->
                    <!-- <tr>
                        <td class="row1">Hot</td>
                        <td align="left" class="row0">{data.list_hot}</td>
                    </tr> -->
                </table>
            </div>


            <br/>

<!--BEGIN Chi tiết trận đấu-->

<script>
$(document).ready(function() {
        var max_fields    = 30;
        //Team 1
        var list1         = $("#list1");
        var add_button1   = $("#btn1");
        //Team 2
        var list2         = $("#list2");
        var add_button2   = $("#btn2");

        // Action Add team 1
        var x = 1;
        $(add_button1).click(function(e){
            e.preventDefault();
            if(x < max_fields){
                x++;
                $(list1).append('<li><label>Cầu thủ ghi bàn : </label><select class="player1" name="cauthu_ghiban_team1[]">{data.list_player1}</select><label>&nbsp;&nbsp;&nbsp;Phút : </label><input name="time_ghiban_team1[]" id="time_ghiban_team1" maxlength="250" value="" class="textfield" style="width:5%" > <a href="#" class="delete" style="color:red">Xóa</a></li>'); //add input box
                $('.player1').select2({ width: '40%' });
            }
            else
            {
            alert('You Reached the limits');
            }
        });
        // Action Add team 2
        $(add_button2).click(function(e){
            e.preventDefault();
            if(x < max_fields){
                x++;
                $(list2).append('<li><label>Cầu thủ ghi bàn : </label><select class="player2" name="cauthu_ghiban_team2[]">{data.list_player2}</select><label>&nbsp;&nbsp;&nbsp;Phút : </label><input name="time_ghiban_team2[]" id="time_ghiban_team2"  maxlength="250" value="" class="textfield" style="width:5%" > <a href="#" class="delete" style="color:red">Xóa</a></li>'); //add input box
                  $('.player2').select2({ width: '40%' });
            }
            else
            {
            alert('You Reached the limits');
            }
        });

        //Action delete team 1
        $(list1).on("click",".delete", function(e){
            e.preventDefault(); $(this).parent('li').remove(); x--;
        });
        //Action delete team 2
        $(list2).on("click",".delete", function(e){
            e.preventDefault(); $(this).parent('li').remove(); x--;
        })
    });
</script>

            <div class="block-right" {data.display}>

               <div class="desc">
                    <table width="100%"  border="0" cellspacing="2" cellpadding="2" align=center class="admintable desc_title">
                        <tr class="row_title" >
                            <td  class="font_title" ><img src="{DIR_IMAGE}/toggle_minus.png" alt="bt_add" title="Collapse" align="absmiddle"/> Chi tiết trận đấu :  </td>
                        </tr>

                    </table>
                  <div class="desc_content" style="background: #ffffff">

                      <div class="general">
                          <h4 class="wpseo-heading" style="display: none;">General</h4>
                          <table width="100%"  border="0"  cellspacing="1" cellpadding="1" class="admintable">
                              
                              <tr style="text-align: center">
                                <td class="row0" ><b>{data.name_team1.name} &nbsp;<button id="btn1" type="button"><img src="https://cdn3.iconfinder.com/data/icons/tango-icon-library/48/list-add-16.png" title="Thêm bàn thắng" width="13" /></button></b></td>
                                <td class="row0" ><b>{data.name_team2.name} &nbsp;<button id="btn2" type="button"><img src="https://cdn3.iconfinder.com/data/icons/tango-icon-library/48/list-add-16.png" title="Thêm bàn thắng" width="13" /></button></b></td>
                              </tr>

                              <tr style="text-align: center">
                                  <td  class="row0" >
                                    <ul id="list1">
                                      {data.banthang_team1}
                                    </ul>
                                  </td>

                                  <td  class="row0" >
                                    <ul id="list2">
                                      {data.banthang_team2}
                                    </ul>
                                  </td>
                              </tr>

                          </table>
                      </div>
                      
                  </div>
                </div> 
              <br/>
            </div>

  <!--END Chi tiết trận đấu-->



    </div>
  </div>
  <table width="100%" border="0" cellspacing="0" cellpadding="0">

    <tr >
      <td class="row0" colspan="2"  align="center" height="50"><input type="hidden" name="do_submit"	 value="1" />
        <input type="submit" name="btnAdd" value="{LANG.btn_submit}" class="button">
        &nbsp;
        <input type="reset" name="btnReset" value="{LANG.btn_reset}" class="button">
        &nbsp; </td>
    </tr>
  </table>
</form>
<br>
<!-- END: edit --> 

<!-- BEGIN: manage -->
<form action="{data.link_fsearch}" method="post" name="myform">
  <table width="100%" border="0" cellspacing="2" cellpadding="2" align="center" class="tableborder">
    <tr>
      <td align="left" class="row1"><strong>Giải đấu: </strong></td>
      <td align="left" class="row0">{data.list_cat}</td>
    </tr>
    <tr>
      <td class="row1" ><strong>{LANG.view_day_from}:</strong></td>
      <td align="left" class="row0"><input type="text" name="date_begin" id="date_begin" value="{data.date_begin}" size="15" maxlength="10"    />
        &nbsp;&nbsp; <strong>{LANG.view_day_to} :</strong>
        <input type="text" name="date_end" id="date_end" value="{data.date_end}" size="15" maxlength="10"   /></td>
    </tr>
    <tr>
    <tr>
      <td align="left"><strong>{LANG.search}  :</strong> &nbsp;&nbsp;&nbsp; </td>
      <td align="left">{data.list_search} &nbsp;&nbsp;<strong>{LANG.keyword} :</strong> &nbsp;
        <input name="keyword"  value="{data.keyword}"type="text" size="20">
        <input name="btnSearch" type="submit" value=" Search " class="button"></td>
    </tr>
    <tr>
      <td width="15%" align="left" class="row1"><strong>{LANG.totals}:</strong> &nbsp;</td>
      <td width="85%" align="left" class="row0"><b class="font_err">{data.totals}</b></td>
    </tr>
  </table>
</form>
{data.err}
<br />
{data.table_list}
<br />
<table width="100%"  border="0" align="center" cellspacing="1" cellpadding="1" class="bg_tab">
  <tr>
    <td  height="25">{data.nav}</td>
  </tr>
</table>
<br />
<!-- END: manage --> 
