<!-- BEGIN: list_focus -->
<script language="javascript">
function check_all_focus()	{
	for ( i=0;i < document.f_focus.elements.length ; i++ ){
		if (document.f_focus.elements[i].type=="checkbox" && document.f_focus.elements[i].name!="all"){
			row_id = 'rowfocus_'+document.f_focus.elements[i].value;
		}else{
			row_id="";
		}
		
		if ( document.f_focus.all.checked==true ){
			document.f_focus.elements[i].checked = true;
			if (row_id!="" ){
				getobj(row_id).className = "row_select";
			}
		}
		else{
			document.f_focus.elements[i].checked  = false;
			if (row_id!="" ){
				if (i%2==0)
					getobj(row_id).className = "row1";
				else
					getobj(row_id).className = "row";
			}
		}
		
	}
}

function do_check_focus (id){
	
	for ( i=0;i < document.f_focus.elements.length ; i++ ){
		if (document.f_focus.elements[i].type=="checkbox" && document.f_focus.elements[i].name!="all" && document.f_focus.elements[i].value == id){
			document.f_focus.elements[i].checked=true;
			getobj("rowfocus_"+id).className = "row_select";
			
			break;
		}	
	}
	
}
function selected_item_focus(){
		var name_count = document.f_focus.length;

		for (i=0;i<name_count;i++){
			if (document.f_focus.elements[i].checked){
				return true;
			}
		}
		alert('Hãy chọn ít nhất 1 record');
		return false;
	}
	
function del_focus_selected(action) {
		if (selected_item_focus()){
			question = confirm('Bạn có chắc muốn XÓA không ?')
			if (question != "0"){
				document.f_focus.action=action;
				document.f_focus.submit();
			}else{
			  alert ('Phew~');
		    }
		}
	
}

</script>
<table width="100%" border="0" cellspacing="2" cellpadding="2" align="center" class="tableborder">
  <tr>
    <td align="left" width="20%">{LANG.totals} &nbsp; : </td>
    <td align="left" class="font_err"><strong>{data.totals}</strong></td>
  </tr>
 </table>
 <br />
<table width="100%" border="0" cellspacing="0" cellpadding="0" style="margin-bottom:5PX;">
  <tr>
  <td height="30" style="border-bottom: 2px solid rgb(184, 65, 32);" class="font_title">{data.f_title}</td>
  </tr>
</table>
{data.err}
<form action="{data.link_action}" method="post" name="f_focus" id="f_focus">
<table width="100%"  border="0" cellspacing="0" cellpadding="0" class="bg_tbl">
	<tr>
		<td >
		<table width="100%" cellspacing="0" cellpadding="0" border="0" class="adminlist">
    <thead>
		  <tr>
			  <th width="5%" align="center"><input type="checkbox" name="all" onclick="javascript:check_all_focus();"></th>
				<th width="10%">{LANG.order}</th>
			  <th width="15%" align="center">{LANG.picture}</th>
			  <th width="45%" align="center">{LANG.title}</th>
			  <th width="30%" align="center">Tin của gian hàng </th>
		  </tr>
      </thead>
      <tbody>
		   <!-- BEGIN: row_focus -->
      <tr class="{row.class}" id="{row.row_id}"> 
        <td align="center">{row.check_box}&nbsp;</td>
        <td align="center">{row.order}&nbsp;</td>
        <td align="center">{row.picture}&nbsp;</td>
        <td align="left">{row.title}&nbsp;</td>
        <td align="center">{row.estore}&nbsp;</td>
      </tr>
      <!-- END: row_focus -->
      
      <!-- BEGIN: row_focus_no -->
         <tr class="row0" >
           <td  colspan="5" align="center" class="font_err" >{mess}</td>
         </tr>
         <!-- END: row_focus_no -->
         
      </tbody>
		</table>
		</td>
	</tr>
	<tr>
		<td  style="padding:5px;">
		<input type="submit" name="btnEdit" value="Cập nhật thứ tự" class="button"  >
		<input type="button" name="btnDel" value="{LANG.del_focus}" class="button" onclick="javascript:del_focus_selected('{data.link_del}');" ></td>
	</tr>
</table>
</form>    
<br>
<table width="100%"  border="0" cellspacing="1" cellpadding="1" class="bg_tab" align=center>
      <tr  height=25>
        <td align="left" >{data.nav}</td>
      </tr>
</table>
<br />

<!-- END: list_focus -->
 

<!-- BEGIN: manage -->
<form action="{data.link_fsearch}" method="post" name="myform">
<table width="100%" border="0" cellspacing="2" cellpadding="2" align="center" class="tableborder">
     
  
   
  <tr>
    <td class="row1" ><strong>{LANG.view_day_from}:</strong>  </td>
    <td align="left" class="row0"> 
    <input type="text" name="date_begin" id="date_begin" value="{data.date_begin}" size="15" maxlength="10"    /> &nbsp;&nbsp; <strong>{LANG.view_day_to} :</strong> <input type="text" name="date_end" id="date_end" value="{data.date_end}" size="15" maxlength="10"   /> </td>
  </tr>
  <tr>
  <tr>
      <td class="row1"><strong>Tin của gian hàng :</strong></td>
      <td class="row0"><input type="text" style="width:95%" id="estore_name" name="estore_name" value="{data.estore_name}" autocomplete="off" onmouseup="getLoad_Estore();" onkeyup="getLoad_Estore();"  ><input type="hidden" id="estore_id" name="estore_id" value="{data.estore_id}"> </td>
    </tr>

  <tr>
  <td align="left"><strong>{LANG.search}  :</strong> &nbsp;&nbsp;&nbsp;  </td>
  <td align="left">{data.list_search} &nbsp;&nbsp;<strong>{LANG.keyword} :</strong> &nbsp;
    <input name="keyword"  value="{data.keyword}"type="text" size="20">
    <input name="btnSearch" type="submit" value=" Search " class="button"></td>
  </tr>
   <tr>
    <td width="15%" align="left" class="row1"><strong>{LANG.totals}:</strong> &nbsp;</td>
    <td width="85%" align="left" class="row0"><b class="font_err">{data.totals}</b></td>
  </tr>
  </table>
</form>
{data.err}
<br />
{data.table_list}
<br />
<table width="100%"  border="0" align="center" cellspacing="1" cellpadding="1" class="bg_tab">
  <tr>
    <td  height="25">{data.nav}</td>
  </tr>
</table>
  <script type="text/javascript">
  
function getLoad_Estore() {
	document.getElementById('estore_id').value =0 ;
	var options = {
		script:"modules/estore_ad/ajax/ajax_list.php?json=true&do=estore&lang=vn&",
		varname:"input",
		json:true,
		shownoresults:false,
		maxresults:6,
		callback: function (obj) { 
			document.getElementById('estore_id').value = obj.id;		
		}
	};
	var as_json = new bsn.AutoSuggest('estore_name', options);
}

</script> 
<br />
<!-- END: manage -->
 