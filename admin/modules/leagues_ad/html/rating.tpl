<!-- BEGIN: edit -->
<link href="//code.jquery.com/ui/1.11.1/themes/smoothness/jquery-ui.css" rel="stylesheet">
<script src="//code.jquery.com/ui/1.11.1/jquery-ui.js"></script>
<script type="text/javascript">
    $(function() {
      $( "tbody" ).sortable();
    });
</script>
<script language=javascript>
$(document).ready(function() {
	$('#myForm').validate({
		rules: {			
				name: {
					required: true 
				},
				title: {
					required: true,
					minlength: 3
				}
	    },
	    messages: {
	    	
				name: {
						required: "{LANG.err_text_required}"
				} ,
				title: {
						required: "{LANG.err_text_required}",
						minlength: "{LANG.err_length} 3 {LANG.char}" 
				} 
		}
	});
    
});
</script>

<script type="text/javascript">
  $(document).ready(function() {
    $('.team').select2({ width: '100%' });   
});

</script>

{data.err}
<form action="{data.link_action}" method="post" enctype="multipart/form-data" name="myForm" id="myForm"  class="validate">
  <table width="100%"  border="0"  cellspacing="1" cellpadding="1" class="admintable">
		<tr>
          <td width="15%" class="row1" nowrap="nowrap">Chọn giải đấu&nbsp;: </td>
          <td align="left" class="row0">{data.listcat}</td>
        </tr>
    <!-- BEGIN: select_lang -->
		<tr >
			<td class="row1" >{LANG.language} : </td>
			<td  align="left" class="row0"><input name="rLang" id="rLang" type="radio" value="0" /> {LANG.only_this_lang} <input name="rLang" id="rLang" type="radio" value="1" checked="checked" /> {LANG.all_lang}</td>
		</tr>
    <!-- END: select_lang -->
    
    <tr class="form-required">
     <td class="row1" width="20%">{LANG.name}: </td>
     <td class="row0"><input name="name" id="name" type="text" size="50" maxlength="250" value="{data.name}"></td>
    </tr>
    
	<tr class="form-required">
     <td class="row1" width="20%">{LANG.title}: </td>
     <td class="row0"><input name="title" id="title" type="text" size="50" maxlength="250" value="{data.title}"></td>
    </tr>
    
    <!-- <tr >
     <td class="row1" width="20%">{LANG.picture}: </td>
     <td class="row0">
     	{data.pic}
      <div class="ext_upload">
      <input name="chk_upload" id="chk_upload" type="radio" value="0" checked> 
      Insert URL's image &nbsp; <input name="picture" type="text" size="50" maxlength="250" onchange="do_ChoseUpload('ext_upload',0);" > <br>
      <input name="chk_upload" id="chk_upload" type="radio" value="1"> Upload Picture &nbsp;&nbsp;&nbsp;
      <input name="image" type="file" id="image" size="30" maxlength="250" onclick="do_ChoseUpload('ext_upload',1);" >
      </div>
    </td>
    </tr>     -->
 	<tr>
 		<table width="100%"  border="0" cellspacing="2" cellpadding="2" align=center >
            <tr class="row_title" >
                <td  class="font_title" > Chi tiết : &nbsp;<button id="btn" type="button"><img src="https://cdn3.iconfinder.com/data/icons/tango-icon-library/48/list-add-16.png" title="Thêm đội" width="13" /> Thêm</button> </td>
            </tr>
        </table>

        <table width="100%"  border="0"  cellspacing="1" cellpadding="1" class="admintable" >
            <tr style="text-align: center;">
             	<td width="23%">Đội bóng</td>
             	<td width="9%"><b>PTS</b><br/> <small>Điểm</small></td>
             	<td width="9%"><b>PLD</b><br/> <small>Số trận</small></td>
             	<td width="9%"><b>W</b><br/> <small>Trận thắng</small></td>
             	<td width="9%"><b>D</b><br/> <small>Hòa</small></td>
             	<td width="9%"><b>L</b><br/> <small>Trận thua</small></td>
             	<td width="9%"><b>GF</b><br/> <small>Số bàn thắng</small></td>
             	<td width="9%"><b>GA</b><br/> <small>Số bàn thua</small></td>
             	<td width="9%"><b>GD</b><br/><small> Hiệu số bàn thắng bại</small></td>
             	<td width="5%"></td>
             </tr> 
             <tbody id="list">
             {data.list_xephang}
             </tbody>
             <!-- <tr style="text-align: center;" id="list_sub">
             	<td>
             		<select class="team" name="team">
                      {data.list_team}
                    </select>
             	</td>
             	<td class="row2">
             		<input type="text" name="pts[]" placeholder="PTS">
             	</td>
             	<td class="row2">
             		<input type="text" name="pld[]" placeholder="PLD">
             	</td>
             	<td class="row2">
             		<input type="text" name="w[]" placeholder="W">
             	</td>
             	<td class="row2">
             		<input type="text" name="d[]" placeholder="D">
             	</td>
             	<td class="row2">
             		<input type="text" name="l[]" placeholder="L">
             	</td>
             	<td class="row2">
             		<input type="text" name="gf[]" placeholder="GF">
             	</td>
             	<td class="row2">
             		<input type="text" name="ga[]" placeholder="GA">
             	</td>
             	<td class="row2">
             		<input type="text" name="gd[]" placeholder="GD">
             	</td>
             	<td class="row2">
             		<a href="#" class="delete" style="color:red">Xóa</a>
             	</td>
             </tr>   -->              

        </table>
 	</tr>
    <br/>
			
		<tr align="center">
    		<td class="row1" >&nbsp; </td>
			<td class="row0" >
				<input type="hidden" name="do_submit"	 value="1" />
				<input type="submit" name="btnAdd" value="Submit" class="button">
				<input type="reset" name="btnReset" value="Reset" class="button">
			</td>
		</tr>
	</table>
</form>
<br>

<style type="text/css">
	table.admintable tbody tr.row2 	{width: 9%; background: #fff; border-top: 1px solid #fff; }
	table.admintable tbody tr td.row2 	{ background: #fff; border-top: 1px solid #fff; }
	.row2 input{text-align: center;width: 100px }

 
    table.admintable tbody tr td {
        cursor: move;
    }
 
</style>
<script>
$(document).ready(function() {
        var max_fields    = 30;
        //list
        var list         = $("#list");
        var add_button   = $("#btn");

        // Action Add team 1
        var x = 1;
        $(add_button).click(function(e){
            e.preventDefault();
            if(x < max_fields){
                x++;
                $(list).append('<tr style="text-align: center;" id="list_sub"><td><select class="team" name="team[]" required>{data.list_team}</select></td><td class="row2"><input type="text" name="pts[]" placeholder="PTS"></td><td class="row2"><input type="text" name="pld[]" placeholder="PLD"></td><td class="row2"><input type="text" name="w[]" placeholder="W"></td><td class="row2"><input type="text" name="d[]" placeholder="D"></td><td class="row2"><input type="text" name="l[]" placeholder="L"></td><td class="row2"><input type="text" name="gf[]" placeholder="GF"></td><td class="row2"><input type="text" name="ga[]" placeholder="GA"></td><td class="row2"><input type="text" name="gd[]" placeholder="GD"></td><td class="row2"><a href="#" class="delete" style="color:red">Xóa</a></td></tr>');
             	$('.team').select2({ width: '100%' });   	

            }
            else
            {
            alert('You Reached the limits');
            }
        });

        //Action delete team 1
        $(list).on("click",".delete", function(e){
            e.preventDefault(); 
            $(this).closest('tr').remove(); 
            x--;
        })
        
    });
</script>
<!-- END: edit -->

<!-- BEGIN: manage -->
<form action="{data.link_fsearch}" method="post" name="fSearch">
<table width="100%" border="0" cellspacing="2" cellpadding="2" align="center" class="tableborder">
  <tr>
    <td width="15%" align="left">{LANG.totals}: &nbsp;</td>
    <td width="85%" align="left"><b class="font_err">{data.totals}</b></td>
  </tr>
</table>
</form>
{data.err}
<br />
{data.table_list}
<br />
<table width="100%"  border="0" align="center" cellspacing="1" cellpadding="1" class="bg_tab">
  <tr>
    <td  height="25">{data.nav}</td>
  </tr>
</table>
<br />


<!-- END: manage -->