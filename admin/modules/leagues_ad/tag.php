
<?php
/*================================================================================*\
|| 							Name code : keywords.php 		 		            	  ||
||  				Copyright @2008 by Thai Son - CMS vnTRUST                     ||
\*================================================================================*/
/**
 * @version : 2.0
 * @date upgrade : 09/01/2009 by Thai Son
 **/
if (! defined('IN_vnT')) {
  die('Hacking attempt!');
}
$vntModule = new vntModule();

class vntModule
{
  var $output = "";
  var $skin = "";
  var $linkUrl = "";
  var $module = "news";
  var $action = "tag";

  /**
   * function vntModule ()
   * Khoi tao 
   **/
  function vntModule ()
  {
    global $Template, $vnT, $func, $DB, $conf;
    require_once ("function_" . $this->module . ".php");		
    $this->skin = new XiTemplate(DIR_MODULE . DS . $this->module . "_ad" . DS . "html" . DS . $this->action . ".tpl");
    $this->skin->assign('LANG', $vnT->lang);
    $lang = ($vnT->input['lang']) ? $vnT->input['lang'] : $func->get_lang_default();
    $this->linkUrl = "?mod=" . $this->module . "&act=" . $this->action . "&lang=" . $lang;
		$vnT->html->addScript("modules/".$this->module."_ad/js/" . $this->action . ".js");
    switch ($vnT->input['sub'])
    {
      
      case 'del':
        $this->do_Del($lang);
        break;
      default:
        $nd['f_title'] = "Quản lý Tags";
        $nd['content'] = $this->do_Manage($lang);
        break;
    }
    $nd['menu'] = $func->getToolbar_Small($this->module, $this->action, $lang);
   // $nd['row_lang'] = $func->html_lang("?mod=" . $this->module . "&act=" . $this->action, $lang);
    $Template->assign("data", $nd);
    $Template->parse("box_main");
    $vnT->output .= $Template->text("box_main");
  }

 
  /**
   * function do_Del 
   * Xoa 1 ... n  gioi thieu 
   **/
  function do_Del ($lang)
  {
    global $func, $DB, $conf, $vnT;

		$id = (int) $vnT->input['id'];
		$ext = $vnT->input["ext"];
		$del = 0;
		$qr = "";
			
		if ($id != 0) {
			$ids = $id;
		}
		if (isset($vnT->input["del_id"])){
			$ids = implode(',', $vnT->input["del_id"]);
		} 		
		$query = 'DELETE FROM news_tags WHERE tag_id IN ('.$ids.')' ;
		if ($ok = $DB->query($query))
		{ 
			$mess = $vnT->lang["del_success"] ;
		} else
			$mess = $vnT->lang["del_failt"] ;
		$ext_page = str_replace("|", "&", $ext);
		$url = $this->linkUrl . "&{$ext_page}";
		$func->html_redirect($url, $mess);
  }

  /**
   * function render_row 
   * list cac record
   **/
  function render_row ($row_info, $lang)
  {
    global $func, $DB, $conf, $vnT;
    $row = $row_info;
    // Xu ly tung ROW
    $id = $row['tag_id'];
    $row_id = "row_" . $id;
		$output['row_id'] =$row_id;
    $output['check_box'] = vnT_HTML::checkbox("del_id[]", $id, 0, " ");
    $link_edit = $this->linkUrl . '&sub=edit&id=' . $id . '&ext=' . $row['ext_page'];
    $link_del = "javascript:del_item('" . $this->linkUrl . "&sub=del&id=" . $id . "&ext=" . $row['ext_page'] . "')";
    
		$output['order'] = "<input name=\"txt_Order[{$id}]\" type=\"text\" size=\"2\" maxlength=\"2\" style=\"text-align:center\" value=\"{$row['is_order']}\"  onkeypress=\"return is_num(event,'txtOrder')\" onchange='javascript:do_check($id)' />";
		
		$output['name'] = $row['name'];
				 
		
		$link_display = $this->linkUrl.$row['ext_link']; 		
    if ($row['display'] == 1) {
      $display = "<a href='".$link_display."&do_hidden=$id' title='".$vnT->lang['click_do_hidden']."' ><img src=\"" . $vnT->dir_images . "/dispay.gif\" width=15  /></a>";
    } else {
      $display = "<a href='".$link_display."&do_display=$id'  title='".$vnT->lang['click_do_display']."' ><img src=\"" . $vnT->dir_images . "/nodispay.gif\"  width=15 /></a>";
    }
				
    $output['action'] = '<input name="h_id[]" type="hidden" value="' . $id . '" />';    
    $output['action'] .= "<a href=\"#Edit\" onClick=\"do_Edit(".$id.",'".$lang."')\" ><img src=\"" . $vnT->dir_images . "/edit.gif\"  alt=\"Edit \"></a>&nbsp;";
		$output['action'] .= $display . '&nbsp;';
    $output['action'] .= '<a href="' . $link_del . '"><img src="' . $vnT->dir_images . '/delete.gif"  alt="Delete "></a>';
    return $output;
  }

  /**
   * function do_Manage() 
   * Quan ly cac gioi thieu
   **/
  function do_Manage ($lang)
  {
    global $vnT, $func, $DB, $conf;
    $vnT->html->addStyleSheet( $vnT->dir_js."/jquery_ui/themes/base/ui.all.css");
    $vnT->html->addScript($vnT->dir_js."/jquery_ui/ui.core.js");
    $vnT->html->addScript($vnT->dir_js."/jquery_ui/ui.datepicker.js");
	
		if(isset($_POST['btnAdd']))
		{
			$data = $_POST;
			$cot['name'] = trim($vnT->input['name']);		 
			$cot['name_compare'] = $func->get_keyword($cot['name']);
			$cot['name_search'] = strtolower($func->utf8_to_ascii($cot['name']));
 			$cot['date_post'] = time(); 
			$ok = $DB->do_insert("news_tags",$cot);
			if($ok)
			{
				$tag_id = $DB->insertid();				 
				 //xoa cache
				$func->clear_cache();
				//insert adminlog
				$func->insertlog("Add", $_GET['act'], $tag_id);
				
				$mess = $vnT->lang["add_success"] ;
				$url = $this->linkUrl   ;
				$func->html_redirect($url, $mess);
			}else{
				$err = $vnT->lang["add_failt"] .$DB->debug() ;
			}
			
		}
		
    //update
    if ($vnT->input["do_action"])
    {
       //xoa cache
      $func->clear_cache();
      if ($vnT->input["del_id"]) $h_id = $vnT->input["del_id"];
      switch ($vnT->input["do_action"])
      {
        case "do_edit":
            if (isset($vnT->input["txt_Order"])) $arr_order = $vnT->input["txt_Order"];
             $mess .= "- " . $vnT->lang['edit_success'] . " ID: <strong>";
            $str_mess = "";
            for ($i = 0; $i < count($h_id); $i ++)
            {
              $dup['is_order'] = $arr_order[$h_id[$i]];
               $ok = $DB->do_update("news_tags", $dup, "tag_id=" . $h_id[$i]);
              if ($ok) {
                $str_mess .= $h_id[$i] . ", ";
              }
            }
            $mess .= substr($str_mess, 0, - 2) . "</strong><br>";
            $err = $func->html_mess($mess);
          break;
        case "do_hidden":
            $mess .= "- " . $vnT->lang['hidden_success'] . " ID: <strong>";
            for ($i = 0; $i < count($h_id); $i ++)
            {
              $dup['display'] = 0;
              $ok = $DB->do_update("news_tags", $dup, "tag_id=" . $h_id[$i]);
              if ($ok){
                $str_mess .= $h_id[$i] . ", ";
              }
            }
            $mess .= substr($str_mess, 0, - 2) . "</strong><br>";
            $err = $func->html_mess($mess);

          break;
        case "do_display":
            $mess .= "- " . $vnT->lang['display_success'] . "  ID: <strong>";
            for ($i = 0; $i < count($h_id); $i ++)
            {
              $dup['display'] = 1;
              $ok = $DB->do_update("news_tags", $dup, "tag_id=" . $h_id[$i]);
              if ($ok){
                $str_mess .= $h_id[$i] . ", ";
              }
            }
            $mess .= substr($str_mess, 0, - 2) . "</strong><br>";
            $err = $func->html_mess($mess);
          break;
      }
    }
		
		if((int)$vnT->input["do_display"]) {				
			$ok = $DB->query("Update news_tags SET display=1 WHERE  tag_id=".$vnT->input["do_display"]);
			if($ok){
				$mess .= "- " . $vnT->lang['display_success'] . "  ID: <strong>".$vnT->input["do_display"] . "</strong><br>";	
				$err = $func->html_mess($mess);
			}        
			//xoa cache
      $func->clear_cache();
		}
		if((int)$vnT->input["do_hidden"]) {				
			$ok = $DB->query("Update news_tags SET display=0 WHERE  tag_id=".$vnT->input["do_hidden"]);
			if($ok){
				$mess .= "- " . $vnT->lang['display_success'] . "  ID: <strong>".$vnT->input["do_hidden"] . "</strong><br>";	
				$err = $func->html_mess($mess);
			}    
			//xoa cache
      $func->clear_cache();
		}
    
		$p = ((int) $vnT->input['p']) ? $p = $vnT->input['p'] : 1;
		$n = ($conf['record']) ? $conf['record'] : 30;
		
		$where= '';
		$keyword = ($vnT->input['keyword']) ? $vnT->input['keyword'] : "";				
		if(!empty($keyword)){
			$where =" where name like '%$keyword%' ";
		}
		
    $query = $DB->query("SELECT tag_id FROM news_tags {$where} ");
    $totals = intval($DB->num_rows($query));    
		
    $num_pages = ceil($totals / $n);
    if ($p > $num_pages) $p = $num_pages;
    if ($p < 1) $p = 1;
    $start = ($p - 1) * $n;
    
    $nav = $func->paginate($totals, $n, $ext, $p);
    
    $data['link_action'] = $this->linkUrl . "&p=$p";
    
    $sql = "SELECT * FROM news_tags {$where} ORDER BY  name ASC  LIMIT $start,$n";
    //print "sql = ".$sql."<br>";
    $result = $DB->query($sql);
    if ($DB->num_rows ($result))
		{
	  	$i=0;
			while ($row = $DB->fetch_row($result))
			{
				$i++;
				$row['ext'] = "";
				$row_info = $this->render_row($row,$lang);
				$row_info['class'] = ($i%2) ? "row1" : "row0";
				$this->skin->assign('row', $row_info);
				$this->skin->parse("manage.html_row");

			}
    }else{
			$mess = "";
			$this->skin->assign('mess', $mess);
			$this->skin->parse("manage.html_row_no");
		}
    
 		$data['button'] = '&nbsp;<input type="button" name="btnAdd" value=" Thêm vào danh sách " class="button" onclick="showobj(\'ext_tags\')">';
		$data['button'] .= '<input type="button" name="btnHidden" value=" ' . $vnT->lang['hidden'] . ' " class="button" onclick="do_submit(\'do_hidden\')">&nbsp;';
    $data['button'] .= '<input type="button" name="btnDisplay" value=" ' . $vnT->lang['display'] . ' " class="button" onclick="do_submit(\'do_display\')">&nbsp;';
    $data['button'] .= '<input type="button" name="btnEdit" value=" ' . $vnT->lang['update'] . ' " class="button" onclick="do_submit(\'do_edit\')">&nbsp;';
    $data['button'] .= '<input type="button" name="btnDel" value=" ' . $vnT->lang['delete'] . ' " class="button" onclick="del_selected(\'' . $this->linkUrl . '&sub=del&ext=' . $ext_page . '\')">';
    
    $data['totals'] = $totals;
    $data['err'] = $err;
    $data['nav'] = $nav; 
		$data['keyword'] = $keyword;
		
    $this->skin->assign('data', $data);
    
    $this->skin->parse("manage");
    return $this->skin->text("manage");
  }
	
  // end class
}
?>