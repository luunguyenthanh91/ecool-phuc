<?php
/*================================================================================*\
|| 						           Name code : focus_news.php 		 		            	  			||
||  				     Copyright @2008 by Thai Son - CMS vnTRUST                  			||
\*================================================================================*/
/**
 * @version : 2.0
 * @date upgrade : 09/01/2009 by Thai Son
 **/
if (! defined('IN_vnT')) {
  die('Hacking attempt!');
}
$vntModule = new vntModule();

class vntModule
{
  var $output = "";
  var $skin = "";
  var $linkUrl = "";
  var $module = "news";
  var $action = "focus_news";

  /**
   * function vntModule ()
   * Khoi tao 
   **/
  function vntModule ()
  {
    global $Template, $vnT, $func, $DB, $conf;
    require_once ("function_" . $this->module . ".php");
    $this->skin = new XiTemplate(DIR_MODULE . DS . $this->module . "_ad" . DS . "html" . DS . $this->action . ".tpl");
    $this->skin->assign('LANG', $vnT->lang);
    $lang = ($vnT->input['lang']) ? $lang = $vnT->input['lang'] : $func->get_lang_default();
    $this->linkUrl = "?mod=" . $this->module . "&act=" . $this->action . "&lang=" . $lang;
    switch ($vnT->input['sub']) {
      case 'edit':
        $this->do_Edit($lang);
      break;
      case 'del':
        $this->do_Del($lang);
      break;
      default:
        {
          $nd['f_title'] = $vnT->lang['manage_focus_news'];
          $nd['content'] = $this->do_Manage($lang);
        }
        ;
      break;
    }
    $nd['menu'] = $func->getToolbar_Small($this->module, $this->action, $lang);
    $nd['row_lang'] = $func->html_lang("?mod=" . $this->module . "&act=" . $this->action, $lang);
    $Template->assign("data", $nd);
    $Template->parse("box_main");
    $vnT->output .= $Template->text("box_main");
  }

  /**
   * function do_Edit 
   * Cap nhat 
   **/
  function do_Edit ($lang)
  {
    global $vnT, $func, $DB, $conf;
    $id = (int) $vnT->input['id'];
    $cat_id = (int) $vnT->input['cat_id'];
    if (isset($vnT->input["focus_id"])) {
      $ids = implode(',', $vnT->input["focus_id"]);
    }
    // list 
    if ($cat_id) {
      $sql = 'UPDATE news SET focus_cat=1 WHERE newsid IN (' . $ids . ')';
      $url = $this->linkUrl . "&cat_id=$cat_id";
    } else {
      $sql = 'UPDATE news SET focus_main=1 WHERE newsid IN (' . $ids . ')';
      $url = $this->linkUrl;
    }
    $ok = $DB->query($sql);
    if ($ok) {
      //xoa cache
      $func->clear_cache();
      //insert adminlog
      $func->insertlog("Set focus", $_GET['act'], $ids);
      $mess = $vnT->lang['set_focus_success'];
    } else {
      $mess = $vnT->lang['edit_failt'];
    }
    $func->html_redirect($url, $mess);
  }

  /**
   * function do_Del 
   * Xoa 1 ... n  gioi thieu 
   **/
  function do_Del ($lang)
  {
    global $func, $DB, $conf, $vnT;
    $cat_id = (int) $vnT->input['cat_id'];
    if (isset($vnT->input["del_focus"])) {
      $ids = implode(',', $vnT->input["del_focus"]);
    }
    if ($cat_id) {
      $sql = 'UPDATE news SET focus_cat=0 WHERE newsid IN (' . $ids . ')';
      $url = $this->linkUrl . "&cat_id=$cat_id";
    } else {
      $sql = 'UPDATE news SET focus_main=0 WHERE newsid IN (' . $ids . ')';
      $url = $this->linkUrl;
    }
    if ($ok = $DB->query($sql)) {
      $mess = $vnT->lang["del_success"];
			//xoa cache
      $func->clear_cache();
    } else
      $mess = $vnT->lang["del_failt"];
    $func->html_redirect($url, $mess);
  }

  /**
   * function do_Manage() 
   * Quan ly cac gioi thieu
   **/
  function do_Manage ($lang)
  {
    global $vnT, $func, $DB, $conf;
    $cat_id = (int) $vnT->input['cat_id'];
    $data['table_list_focus'] = $this->list_focus_news($cat_id, $lang);
    $data['link_fsearch'] = $this->linkUrl;
    $data['table_list'] = $this->list_news($cat_id, $lang);
    $data['listcat'] = Get_Cat($cat_id, $lang, "onChange='submit();'");
    /*assign the array to a template variable*/
    $this->skin->assign('data', $data);
    $this->skin->parse("manage");
    return $this->skin->text("manage");
  }

  /**
   * function render_row 
   * list cac record
   **/
  function render_row ($row_info, $lang)
  {
    global $func, $DB, $conf, $vnT;
    $row = $row_info;
    // Xu ly tung ROW	
    $id = $row['newsid'];
    $row_id = "row_" . $id;
    $output['check_box'] = "<input type=\"checkbox\" name=\"focus_id[]\" value=\"{$id}\" class=\"checkbox\" onclick=\"select_row('{$row_id}')\">";
    $link_edit = "?mod=news&act=news&sub=edit&id={$id}&lang=$lang";
    if (empty($row['picture']))
      $picture = "<i>No Image</i>";
    else {
      $picture = get_pic_thumb($row['picture'], 50);
    }
    $output['title'] = "<a href=\"{$link_edit}\">" . $func->HTML($row['title']) . "</a>";
    $output['cat_name'] = get_cat_name($row['cat_id'], $lang);
    $output['picture'] = $picture;
    return $output;
  }

  /**
   * function list_news 
   *  
   **/
  function list_news ($cat_id, $lang)
  {
    global $func, $DB, $conf, $vnT;
    $p = ((int) $vnT->input['p']) ? $vnT->input['p'] : 1;
		$search = ($vnT->input['search']) ? $vnT->input['search'] : "newsid";
    $keyword = ($vnT->input['keyword']) ? $vnT->input['keyword'] : "";
		
    $where = " ";
		
    $ext = "";
    if (! empty($cat_id)) 
		{
      $subcat = List_SubCat($cat_id);
      if (! empty($subcat)) {
        $subcat = substr($subcat, 0, - 1);
        $a_cat_id = $cat_id . "," . $subcat;
        $a_cat_id = str_replace(",", "','", $a_cat_id);
        $where .= " and cat_id in ('" . $a_cat_id . "') ";
      } else {
        $where .= " and cat_id = $cat_id ";
      }
			
			$where .= " and focus_cat = 0 ";	
			
      $ext_page .= "cat_id=$cat_id|";
      $ext = "&cat_id=$cat_id";
			 
    }else{
			$where .= " and focus_main = 0 ";	
		}
		
		if(!empty($keyword)){
			switch($search){
				case "newsid" : $where .=" and  n.newsid = $keyword ";   break;
				case "date_post" : $where .=" and DATE_FORMAT(FROM_UNIXTIME(date_post),'%d/%m/%Y') = '{$keyword}' "; break;
				default :$where .=" and $search like '%$keyword%' ";break;		
			}
			
			$ext_page.="keyword=$keyword|keyword=$keyword|";
			$ext.="&search={$search}&keyword={$keyword}";
		}
		
    $query = $DB->query("SELECT n.newsid 
												FROM news n, news_desc nd
												WHERE n.newsid=nd.newsid
												AND lang='$lang' 
												$where " );
    $totals = $DB->num_rows($query);
    $n = 20;
    $num_pages = ceil($totals / $n);
    if ($p > $num_pages)     $p = $num_pages;
    if ($p < 1)    $p = 1;
    $start = ($p - 1) * $n;
    $nav = $func->paginate($totals, $n, $ext, $p);
    $table['link_action'] = $this->linkUrl . "&sub=edit&cat_id=$cat_id";
    $table['title'] = array(
      'check_box' => "<input type=\"checkbox\" name=\"checkall\" id=\"checkall\" class=\"checkbox\" />|5%|center" , 
      'picture' => $vnT->lang['picture'] . "|10%|center" , 
      'title' => $vnT->lang['title'] . "|40%|left" , 
      'cat_name' => $vnT->lang['category'] . "|30%|center");
    $sql = "SELECT * 
						FROM news n, news_desc nd
						WHERE n.newsid=nd.newsid
						AND lang='$lang'   
						$where 							
						ORDER BY display_order DESC, date_post DESC LIMIT $start,$n";
    //print "sql = ".$sql."<br>";
    $result = $DB->query($sql);
    if ($DB->num_rows($result)) {
      $row = $DB->get_array($result);
      for ($i = 0; $i < count($row); $i ++) {
        $row_info = $this->render_row($row[$i], $lang);
        $row_field[$i] = $row_info;
        $row_field[$i]['stt'] = ($i + 1);
        $row_field[$i]['row_id'] = "row_" . $row[$i]['newsid'];
        $row_field[$i]['ext'] = "";
      }
      $table['row'] = $row_field;
    } else {
      $table['row'] = array();
      $table['extra'] = "<div align=center class=font_err >" . $vnT->lang['no_have_news'] . "</div>";
    }
    $table['button'] = "&nbsp;&nbsp;<input type=\"button\" name=\"btnEdit\" value=\"" . $vnT->lang['set_focus_news'] . "\" class=\"button\" onclick=\"javascript:do_submit('do_edit')\">";
    $data['table_list'] = $func->ShowTable($table);
    $data['listcat'] = Get_Cat($cat_id, $lang);
    $data['list_search'] = List_Search($search);
    $data['keyword'] = $keyword;
    $data['link_fsearch'] = "";
    $data['nav'] = $nav; 
    /*assign the array to a template variable*/
    $this->skin->assign('data', $data);
    $this->skin->parse("list_news");
    return $this->skin->text("list_news");
  }

  /**
   * function render_row_focus 
   *  
   **/
  function render_row_focus ($row_info, $lang)
  {
    global $func, $DB, $conf, $vnT;
    $row = $row_info;
    // Xu ly tung ROW
    $id = $row['newsid'];
    $row_id = "rowfocus_" . $id;
    $output['row_id'] = $row_id;
    $output['check_box'] = "<input type=\"checkbox\" name=\"del_focus[]\" value=\"{$id}\" class=\"checkbox\" onclick=\"select_row('{$row_id}')\">";
    $link_edit = "?mod=news&sub=edit&lang=$lang&id={$id}&lang=$lang";
    if ($row['cid'])
      $order = (int) $row['focus_cat_order'];
    else
      $order = (int) $row['focus_order'];
    $output['order'] = "<input name=\"txtOrder[{$id}]\" type=\"text\" size=\"2\" maxlength=\"2\" style=\"text-align:center\" value=\"{$order}\"  onkeypress=\"return is_num(event,'txtOrder')\" onchange='javascript:do_check_focus($id)' />";
    if (empty($row['picture']))
      $picture = "<i>No Image</i>";
    else {
      $picture = get_pic_thumb($row['picture'], 50);
    }
    $output['title'] = "<a href=\"{$link_edit}\">" . $func->HTML($row['title']) . "</a>";
    $output['cat_name'] = get_cat_name($row['cat_id'], $lang);
    $output['picture'] = $picture;
    return $output;
  }

  /**
   * function list_focus_news 
   *  
   **/
  function list_focus_news ($cat_id, $lang)
  {
    global $func, $DB, $conf, $vnT;
    //update
    if (isset($vnT->input["btnEdit"])) {
      //xoa cache
      $func->clear_cache();
      if (isset($vnT->input["del_focus"]))
        $h_id = $vnT->input["del_focus"];
      $mess .= "- " . $vnT->lang['edit_success'] . " ID: <strong>";
      $str_mess = "";
      if (isset($vnT->input["txtOrder"]))
        $arr_order = $vnT->input["txtOrder"];
      for ($i = 0; $i < count($h_id); $i ++) {
        if ($cat_id) {
          $dup['focus_cat_order'] = $arr_order[$h_id[$i]];
        } else {
          $dup['focus_order'] = $arr_order[$h_id[$i]];
        }
        $ok = $DB->do_update("news", $dup, "newsid={$h_id[$i]}");
        if ($ok) {
          $str_mess .= $h_id[$i] . ", ";
        }
      }
      $mess .= substr($str_mess, 0, - 2) . "</strong><br>";
      $err = $func->html_mess($mess);
      //insert adminlog
      $func->insertlog("Update Order", $_GET['act'], $str_mess);
    }
    if ((isset($_GET['p1'])) && (is_numeric($_GET['p1'])))
      $p1 = $_GET['p1'];
    else
      $p1 = 1;
    if ($cat_id) {
      $where = " And focus_cat=1  ";
      $subcat = List_SubCat($cat_id);
      if (! empty($subcat)) {
        $subcat = substr($subcat, 0, - 1);
        $a_cat_id = $cat_id . "," . $subcat;
        $a_cat_id = str_replace(",", "','", $a_cat_id);
        $where .= " and cat_id in ('" . $a_cat_id . "') ";
      } else {
        $where .= " and cat_id = $cat_id ";
      }
      $where .= " order by focus_cat_order ASC , date_post DESC";
      $title = $vnT->lang['list_focus_cat'] . " <b class='font_err'>" . get_cat_name($cat_id, $lang) . "</b>";
    } else {
      $where = " and display=1 and focus_main=1 order by focus_order  ASC , date_post DESC ";
      $title = $vnT->lang['list_focus_root'];
    }
    $query = $DB->query("SELECT n.newsid 
						FROM news n, news_desc nd
						WHERE n.newsid=nd.newsid
						AND lang='$lang' 
						$where  ");
    $totals = $DB->num_rows($query);
    $n = 9;
    $num_pages = ceil($totals / $n);
    if ($p1 > $num_pages)
      $p = $num_pages;
    if ($p1 < 1)
      $p1 = 1;
    $start = ($p1 - 1) * $n;
    $nav = "<div align=left>&nbsp;<span class=\"pagelink\">{$num_pages} Page(s) :</span>&nbsp;";
    for ($i = 1; $i < $num_pages + 1; $i ++) {
      if ($i == $p1)
        $nav .= "<span class=\"pagecur\">{$i}</span>&nbsp";
      else
        $nav .= "<a href='" . $this->linkUrl . "&p1={$i}'><span class=\"pagelink\">{$i}</span></a>&nbsp ";
    }
    $nav .= "</div>";
    $html_row = "";
    $sql .= " SELECT * 
						FROM news n, news_desc nd
						WHERE n.newsid=nd.newsid
						AND lang='$lang' 
						$where   limit $start,$n";
    //print "sql = ".$sql."<br>";
    $result = $DB->query($sql);
    if ($DB->num_rows($result)) {
      while ($row = $DB->fetch_row($result)) {
        $row['ext'] = "";
        $row['cid'] = $cat_id;
        $row_info = $this->render_row_focus($row, $lang);
        /*assign the array to a template variable*/
        $this->skin->assign('row', $row_info);
        $this->skin->parse("list_focus_news.row_focus");
      }
    } else {
      $this->skin->assign('mess', $vnT->lang['no_have_focus']);
      $this->skin->parse("list_focus_news.row_focus_no");
    }
    $data['html_row'] = $html_row;
    $data['totals'] = $totals;
    $data['nav'] = $nav;
    $data['err'] = $err;
    $data['link_del'] = $this->linkUrl . "&sub=del&cat_id=$cat_id";
    $data['link_action'] = $this->linkUrl . "&cat_id=$cat_id";
    $data['f_title'] = $title;
    //$text_out .= $this->skin->html_list_focus_news($data);
    /*assign the array to a template variable*/
    $this->skin->assign('data', $data);
    $this->skin->parse("list_focus_news");
    $text_out .= $this->skin->text("list_focus_news");
    return $text_out;
  }
  // end class
}
?>
