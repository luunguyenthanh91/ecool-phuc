<?php
	session_start();
	require_once("../../../_config.php"); 
	require_once("../../../includes/class_db.php"); 
	$DB = new DB;
	
	function md10($txt) {  // MD10 Encode by NDK
		$txt = md5($txt);
		$txt = base64_encode($txt);
		$txt = md5($txt);
		return $txt;
	}
	
	$admin_user = $_POST["auth_user"] ;
	$admin_pass = $_POST["auth_pass"] ;
	$folder_upload	= $_POST["folder_upload"] ;
	
	//check 
	$sql = "select * from admin WHERE username='$admin_user' AND password='$admin_pass' "; 
	$result = $DB->query($sql);
	if (!$DB->num_rows($result)) {
		die('You do not have permission to upload files.');
	}else{
		if (!isset($_FILES["resume_file"]) || !is_uploaded_file($_FILES["resume_file"]["tmp_name"]) || $_FILES["resume_file"]["error"] != 0) {
			echo "There was a problem with the upload";
			exit(0);
		} else {
			
			$image = $_FILES["resume_file"];
			$image['name'] = str_replace("%20", "_",$image['name']);
			$image['name'] = str_replace(" ", "_", $image['name']);
			$image['name'] = str_replace("&amp;", "_", $image['name']);
			$image['name'] = str_replace("&", "_", $image['name']);
			$image['name'] = str_replace(";", "_", $image['name']);
			
			$type = strtolower(substr($image['name'],strrpos($image['name'],".")+1));
			$src_name = substr( $image['name'],0,strrpos($image['name'],".") );
			
			$link_file = $folder_upload."/".$image['name'];
			if (file_exists($link_file)){
					$link_file = $folder_upload."/".$src_name."_".time().".".$type; 	
			}
			
			$ok = copy($_FILES["resume_file"]["tmp_name"],$link_file);
			if($ok)
			{
				echo $link_file;
			}
		}
	}

	$DB->close();

?>