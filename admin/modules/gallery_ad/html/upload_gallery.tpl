<!-- BEGIN: upload_directory -->
<form action="{data.link_action}" method="post" name="myForm">
<table width="100%" border="0" cellspacing="2" cellpadding="2" align="center" class="tableborder">
  <tr>
    <td width="15%" align="left">{LANG.totals}: &nbsp;</td>
    <td width="85%" align="left"><b class="font_err">{data.totals}</b></td>
  </tr>
  <tr>
    <td width="15%" align="left">Folder: &nbsp;</td>
    <td width="85%" align="left"><b class="font_err">{data.list_folder}</b></td>
  </tr>  
  <tr>
    <td width="15%" align="left">{LANG.cat_name}: &nbsp;</td>
    <td width="85%" align="left"><b class="font_err">{data.list_cat}</b></td>
  </tr>  
</table>
</form>
{data.err}
<br />
{data.table_list}
<br />
<table width="100%"  border="0" align="center" cellspacing="1" cellpadding="1" class="bg_tab">
  <tr>
    <td  height="25">{data.nav}</td>
  </tr>
</table>
<br />
<!-- END: upload_directory -->

<!-- BEGIN: upload_multi -->

<form action="{data.link_action}" method="post" name="myForm">
	<table width="100%"  border="0"  cellspacing="1" cellpadding="1" class="admintable">
    <tr>
      <td width="20%" align="right" class="row1"> <strong>Chọn danh mục (Album) :</strong> </td>
      <td class="row0">{data.list_cat}</td>
    </tr>
    <tr  >
      <td class="row1">&nbsp;  </td>
      <td width="48%" class="row1"> <input type="submit" value="Submit" class="button" name="btnNum"/> </td>
    </tr>
  </table>
</form>	
<!-- END: upload_multi -->

<!-- BEGIN: upload_multi_add --> 
 
<script language=javascript>
$(document).ready(function() {
	$('#myForm').validate({
		rules: {			
				cat_id: {
					required: true 
				} 
	    },
	    messages: {
	    	cat_id : {
						required: "{LANG.err_select_required}"
				} 
		}
	});
});

 


function attachCallbacks(a) {
	a.bind("Error", function (a, e) { 
		$('#bugcallbacks').append("<div>Error: " + e.code +   ", Message: " + e.message +   (e.file ? ", File: " + e.file.name : "") + "</div>"   );	 
     a.refresh(); // Reposition Flash/Silverlight
		$('#bugcallbacks').show();
	});
	a.bind("FileUploaded", function (b, e, c) { 	 
		1 == a.total.queued && ("undefined" == typeof debug ? $("#photoalbum_form").submit() : ($("#bugcallbacks").show(), 0 < b.e.code && $("#bugcallbacks").append("<div>File: " + b.result + " <br/ > Error code: " + e.message + " <hr></div>")))
	})	
}

// Convert divs to queue widgets when the DOM is ready
$(function() {
	
	$("#uploader").pluploadQueue({
		// General settings
		runtimes : 'gears,flash,silverlight,browserplus,html5',
		url : 'modules/gallery_ad/ajax/upload.php',
		max_file_size : '{data.max_upload}',
		chunk_size : '1mb', 
		rename : true,
		multipart: true,
		multipart_params: {
			'cat_id': '{data.cat_id}',  
			'folder_upload': '{data.folder_upload}',  
			'folder': '{data.folder}', 
			'w' : '{data.w_pic}',	
			'crop' : '0',	
			'thumb' : '1',		
			'w_thumb': '{data.w_pic}'
		},		

		// Resize images on clientside if we can
		resize : {width : 1000, height : 1000, quality : 90}, 
		// Specify what files to browse for
		filters : [
			{title : "Image files", extensions : "jpg,gif,png"}
		],

		flash_swf_url : ROOT+'js/plupload/plupload.flash.swf',
		silverlight_xap_url : ROOT+'js/plupload/plupload.silverlight.xap' ,
		preinit: attachCallbacks
	});
 	
	
});
</script>

<style>

#uploader { width:800px; margin:0px auto;} 
.square.note {
    background: none repeat scroll 0 0 #ECECEC;
    border: 1px solid #CCCCCC;
    color: #333333;
    padding: 5px 10px 10px; 
}

</style> 
{data.err}
 <form action="{data.link_action}" method="post" name="photoalbum_form" id="photoalbum_form"   enctype="multipart/form-data" >

  <table width="100%"  border="0"  cellspacing="1" cellpadding="1" class="admintable">
		<tr >
       <td class="row1" width="20%" align="right">{LANG.cat_name} (Album) : </td>
       <td class="row0"><b class="font_err">{data.cat_name}</b></td>
    </tr>	
    
 
 <tr >
 
       <td class="row0" colspan="2"> 
       
       <div style="padding:10px 30px;">
      
	<input type="hidden" name="hlistfile" id="hlistfile" value="" />
	<div id="uploader">
		<p>You browser doesn't have Flash, Silverlight, Gears, BrowserPlus or HTML5 support.</p>
	</div>
  
 
<br />

<div class="square note">
  <ul>
  <li>Mỗi lần upload tối đa <b>50 ảnh</b>. Giữ phím <b>Ctrl</b> để chọn thêm ảnh, phím <b>Shift</b> để chọn cùng lúc nhiều ảnh.</li>
      <li>Dung lượng mỗi ảnh tối đa <b>{data.max_upload}</b></li>
      <li>Định dạng ảnh cho phép: <b>.jpg</b>, <b>.jpeg</b>, <b>.gif</b> hoặc <b>.png</b></li>
  </ul>
    <div class="clear"></div>
</div>			

<div id="bugcallbacks" style="display: none "></div>
       </div>
        </td>
    </tr>
     

	</table>
</form>


<br>
<!-- END: upload_multi_add -->