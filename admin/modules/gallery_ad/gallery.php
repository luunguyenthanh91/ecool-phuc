<?php
/*================================================================================*\
|| 							Name code : gallery.php 		 		            	  ||
||  				Copyright @2008 by Thai Son - CMS vnTRUST                     ||
\*================================================================================*/
/**
 * @version : 2.0
 * @date upgrade : 09/01/2009 by Thai Son
 **/
if (! defined('IN_vnT')) {
  die('Hacking attempt!');
}
$vntModule = new vntModule();

class vntModule
{
  var $output = "";
  var $skin = "";
  var $linkUrl = "";
  var $module = "gallery";
  var $action = "gallery";

  /**
   * function vntModule ()
   * Khoi tao 
   **/
  function vntModule ()
  {
    global $Template, $vnT, $func, $DB, $conf;
    require_once ("function_" . $this->module . ".php");    
   
	  $this->skin = new XiTemplate(DIR_MODULE . DS . $this->module . "_ad" . DS . "html" . DS . $this->action . ".tpl");
    $this->skin->assign('LANG', $vnT->lang);
		$this->skin->assign('PATH_ROOT', $conf['rootpath']);
    $this->skin->assign('LANG', $vnT->lang);
    $this->skin->assign('DIR_IMAGE', $vnT->dir_images);
    $this->skin->assign('DIR_JS', $conf['rooturl'] . "js");
    $this->skin->assign("admininfo", $vnT->admininfo); 
    $lang = ($vnT->input['lang']) ? $lang = $vnT->input['lang'] : $func->get_lang_default();
    $this->linkUrl = "?mod=" . $this->module . "&act=" . $this->action . "&lang=" . $lang;
 		
		loadSetting($lang);
		
		$vnT->html->addScript("modules/" . $this->module."_ad/js/".$this->module.".js");		
 		
		
    switch ($vnT->input['sub']) {
      case 'add':
        $nd['f_title'] = $vnT->lang['add_gallery'];
        $nd['content'] = $this->do_Add($lang);
      break;
      case 'edit':
        $nd['f_title'] = $vnT->lang['edit_gallery'];
        $nd['content'] = $this->do_Edit($lang);
      break;
			case 'photo':
        $nd['f_title'] = "Cập nhật hình cho Album ";
        $nd['content'] = $this->do_Photo($lang);
      break;
			
      case 'del':
        $this->do_Del($lang);
      break;
      default:
        $nd['f_title'] = $vnT->lang['manage_gallery'];
        $nd['content'] = $this->do_Manage($lang);
      break;
    }
    $nd['menu'] = $func->getToolbar($this->module, $this->action, $lang);
    $nd['row_lang'] = $func->html_lang("?mod=" . $this->module . "&act=" . $this->action, $lang);
    $Template->assign("data", $nd);
    $Template->parse("box_main");
    $vnT->output .= $Template->text("box_main");
  }

  /**
   * function do_Add 
   * Them  moi 
   **/
  function do_Add ($lang)
  {
    global $vnT, $func, $DB, $conf;
    $err = "";
    $vnT->input['is_display'] = 1;
		$data['type']=0;
		$data['file_width']=600;
		$data['file_height']=500;
		$w_pic = ($vnT->setting['img_width']) ? $vnT->setting['img_width'] : 2000;
    $w_thumb = ($vnT->setting['imgthumb_width']) ? $vnT->setting['imgthumb_width'] : 200;
		$dir = $vnT->func->get_dir_upload_module($this->module,$vnT->setting['folder_upload']);
		
    if ($vnT->input['do_submit'] == 1) {
      $data = $_POST;
      $title = $vnT->input['title'];
      $cat_id = $vnT->input['cat_id']; 
			$picture = $vnT->input['picture']; 


			
			
      // insert CSDL
      if (empty($err)) {
        $cot['cat_id'] = $vnT->input['cat_id'];
        $cot['picture'] = $picture;
				$cot['adminid'] =  $vnT->admininfo['adminid'];				
				$cot['date_post'] = time();
        $cot['date_update'] = time();
        $cot['display'] = $vnT->input['display'];
        $ok = $DB->do_insert("gallery", $cot);
        if ($ok) {
          $gid = $DB->insertid();
        

					$cot_d['gid'] = $gid;
					$cot_d['title'] = $title;
					$cot_d['description'] = $DB->mySQLSafe($_POST['description']);


					//SEO
					$cot_d['friendly_url'] = (trim($vnT->input['friendly_url'])) ? trim($vnT->input['friendly_url']) :  $func->make_url($title);
					$cot_d['friendly_title'] = (trim($vnT->input['friendly_title'])) ? trim($vnT->input['friendly_title']) : $title ." - ". $func->utf8_to_ascii($title);
					$cot_d['metakey'] = $vnT->input['metakey'];
					$cot_d['metadesc'] = $vnT->input['metadesc'];

					
					$query_lang = $DB->query("select name from language ");
						while ( $row_lang = $DB->fetch_row($query_lang) ) {
							$cot_d['lang'] = $row_lang['name'];
							$DB->do_insert("gallery_desc",$cot_d);
						}
					
          //xoa cache
          $func->clear_cache();
          //insert adminlog
          $func->insertlog("Add", $_GET['act'], $gid);
          $mess = $vnT->lang['add_success'];
          $url = $this->linkUrl . "&sub=add";
          $func->html_redirect($url, $mess);
        } else {
          $err = $func->html_err($vnT->lang['add_failt'] . $DB->debug());
        }
      }
    }

    $data["html_description"] = $vnT->editor->doDisplay('description', $vnT->input['description'], '100%', '500', "Default");
    $data['list_display'] = vnT_HTML::list_yesno("display", $vnT->input['is_display']);
    $data['list_cat'] = Get_Cat($data['cat_id'], $lang);
 		 
		$data['link_upload'] = '?mod=media&act=popup_media&module='.$this->module.'&folder='.$this->module.'/'.$dir.'&obj=picture&type=image&TB_iframe=true&width=900&height=474';
	  $data['err'] = $err;
    $data['link_action'] = $this->linkUrl . "&sub=add";
    /*assign the array to a template variable*/
    $this->skin->assign('data', $data);
    $this->skin->parse("edit");
    return $this->skin->text("edit");
  }

  /**
   * function do_Edit 
   * Cap nhat 
   **/
  function do_Edit ($lang)
  {
    global $vnT, $func, $DB, $conf;
    $id = (int) $vnT->input['id'];
    $has_pic = 0;
    $w_pic = ($vnT->setting['img_width']) ? $vnT->setting['img_width'] : 2000;
    $w_thumb = ($vnT->setting['imgthumb_width']) ? $vnT->setting['imgthumb_width'] : 200;
		$dir = $vnT->func->get_dir_upload_module($this->module,$vnT->setting['folder_upload']);
		
    if ($vnT->input['do_submit']) {
			$data = $_POST;

      $title = $vnT->input['title'];      
      $cat_id = $vnT->input['cat_id'];
			
			$picture = $vnT->input['picture'];
			

			
      if (empty($err)) {
        $cot['cat_id'] = $cat_id;
        $cot['picture'] = $picture;
				
				$cot['date_update'] = time();
        $cot['display'] = $vnT->input['display'];
        $ok = $DB->do_update("gallery", $cot, "gid=$id");
        if ($ok) {
					$cot_d['title'] = $title;
 					$cot_d['description'] = $DB->mySQLSafe($_POST['description']);


				//SEO
					$cot_d['friendly_url'] = (trim($vnT->input['friendly_url'])) ? trim($vnT->input['friendly_url']) :  $func->make_url($title);
					$cot_d['friendly_title'] = (trim($vnT->input['friendly_title'])) ? trim($vnT->input['friendly_title']) : $title ." - ". $func->utf8_to_ascii($title);
					$cot['metakey'] = $vnT->input['metakey'];
					$cot_d['metadesc'] = $vnT->input['metadesc'];
					$DB->do_update("gallery_desc",$cot_d," gid={$id} and lang='{$lang}'");
					
          //xoa cache
          $func->clear_cache();
          //insert adminlog
          $func->insertlog("Edit", $_GET['act'], $id);
          $err = $vnT->lang["edit_success"];
          $ext_page = str_replace("|", "&", $ext);
          $url = $this->linkUrl . "&{$ext_page}";
          $func->html_redirect($url, $err);
        } else
          $err = $func->html_err($vnT->lang["edit_failt"] . $DB->debug());
      }
    }
   
	 $query = $DB->query("SELECT *
													FROM gallery g, gallery_desc gd
													WHERE  g.gid=gd.gid
													AND lang='$lang'
													AND g.gid=$id");
    if ($data = $DB->fetch_row($query)) {
      if ($data['picture']) {
        $data['pic'] = get_pic_gallery($data['picture'],$w_thumb) . "  <a href=\"javascript:del_picture('picture')\" class=\"del\">Xóa</a>";
				$data['style_upload'] = "style='display:none' ";
      } else {
        $data['pic'] = "";
      }
			
      $data["html_description"] = $vnT->editor->doDisplay('description', $data['description'], '100%', '500', "Default");
      $data['list_display'] = vnT_HTML::list_yesno("display", $data['display']);
      $data['list_cat'] = Get_Cat( $data['cat_id'], $lang);
  		$data['link_upload'] = '?mod=media&act=popup_media&module='.$this->module.'&folder='.$this->module.'/'.$dir.'&obj=picture&type=image&TB_iframe=true&width=900&height=474';
    } else {
      $mess = $vnT->lang['not_found'] . " ID : " . $id;
      $url = $this->linkUrl;
      $func->html_redirect($url, $mess);
    }
    $data['err'] = $err;
    $data['link_action'] = $this->linkUrl . "&sub=edit&id=$id";
    /*assign the array to a template variable*/
    $this->skin->assign('data', $data);
    $this->skin->parse("edit");
    return $this->skin->text("edit");
  }
	
	/**
   * function do_Photo    
   **/
  function do_Photo ($lang)
  {
    global $vnT, $func, $DB, $conf;

    $vnT->html->addStyleSheet($vnT->dir_js . "/plupload/jquery.plupload.queue/css/jquery.plupload.queue.css");
    $vnT->html->addScript($vnT->dir_js . "/plupload/browserplus-min.js");
    $vnT->html->addScript($vnT->dir_js . "/plupload/plupload.full.js");
    $vnT->html->addScript($vnT->dir_js . "/plupload/i18n/vi.js");
    $vnT->html->addScript($vnT->dir_js . "/plupload/jquery.plupload.queue/jquery.plupload.queue.js");


    $dir = $vnT->func->get_dir_upload_module($this->module,$vnT->setting['folder_upload']);
    $folder_upload = $conf['rootpath']."vnt_upload/gallery/".$dir ;
    $w_pic = ($vnT->setting['img_width']) ? $vnT->setting['img_width'] : 2000;
    $w_thumb = ($vnT->setting['imgthumb_width']) ? $vnT->setting['imgthumb_width'] : 200;

    $err = "";		
		$id = (int)$vnT->input['id'];

    if((int)$vnT->input['del_id'])
    {
      $img_id = (int)$vnT->input['del_id'];
      $ok = $DB->query("DELETE FROM gallery_images WHERE gid=".$id." AND img_id=".$img_id) ;
      if($ok){
        $DB->query("DELETE FROM gallery_images_desc WHERE  img_id=".$img_id) ;
        $err = $vnT->func->html_mess($vnT->lang["del_success"]);
      }else{
        $err = $vnT->func->html_err($vnT->lang["del_failt"]);
      }

      //xoa cache
      $func->clear_cache();
      //insert adminlog
      $func->insertlog("Del Photo",$_GET['act'],$img_id);

    }

    if($vnT->input['btnUpdate'])
    {
      $arr_order = $vnT->input['txt_Order'];
      // up hinh phu
      foreach ($arr_order as $key => $value)
      {
        $DB->query("UPDATE gallery_images SET img_order=".(int)$value." WHERE img_id=".$key);
      }

      //xoa cache
      $func->clear_cache();
      //insert adminlog
      $func->insertlog("Edit Order",$_GET['act'],$id);

      $err = "Cập nhật thứ tự thành công";
      $url = $this->linkUrl."&sub=photo&id=".$id;
      $func->html_redirect($url,$err);
    }


		$res_ck = $DB->query("SELECT n.* ,nd.title FROM gallery n , gallery_desc nd WHERE n.gid=nd.gid AND lang='$lang' AND n.gid=".$id);
		if($row_ck = $DB->fetch_row($res_ck))
		{

      $data['title'] = $row_ck['title'];
      $data['gid'] = $row_ck['gid'];



      $result = $DB->query("SELECT n.*, nd.title FROM gallery_images n, gallery_images_desc nd WHERE n.img_id=nd.img_id AND lang='".$lang."' AND gid=".$row_ck['gid']." ORDER BY img_order ASC ,date_post DESC");
      if($num_old = $DB->num_rows($result))
      {
        $list_pic_old="";
        $i=0;
        while ($row=$DB->fetch_row($query))
        {
          $i++;
          $pic =  get_pic_gallery($row['picture'], 170);
          $list_pic_old .= "<td align=\"center\" valign='bottom'><table border=\"0\" bgcolor=\"#CCCCCC\" cellspacing=\"1\" cellpadding=\"1\" > <tr><td align=\"center\" bgcolor=\"#FFFFFF\" >".$pic."</td> </tr><tr>
				<td align=\"center\"><a href=\"".$this->linkUrl."&sub=photo&id=".$id."&del_id={$row['img_id']}\"><img src=\"{$vnT->dir_images}/delete.gif\" align=absmiddle width=20 /> Delete</a></td>
				</tr></table> STT: <input name='txt_Order[".$row['img_id']."]' id='txt_Order' type='text' size='3' value='".(int)$row['img_order']."' style='text-align:center'  /></td>";
          if ($i%5==0) { $list_pic_old .= "</tr><tr>"; }

        }

      }else{
        $list_pic_old = "<td class=font_err align=center>Chưa có hình  nào</td>";
      }

      $data['list_pic_old']	= $list_pic_old;



 			$data['w_pic'] = $w_pic;
			$data['w_thumb'] = $w_thumb;
			$data['folder_upload'] = $folder_upload ;
			$data['folderpath'] =   $conf['rootpath']."vnt_upload/gallery" ;
			$data['folder'] = $dir ;
			$data['max_upload'] = ini_get('upload_max_filesize');
		
		}else{
			$mess = $vnT->lang['not_found'] . " Gallery : " . $id;
      $url = $this->linkUrl;
      $func->html_redirect($url, $mess);	
		}

    $data['err']  = $err;
    $data['link_action'] = $this->linkUrl . "&sub=photo&id=".$id;
    /*assign the array to a template variable*/
    $this->skin->assign('data', $data);
    $this->skin->parse("photo");
    return $this->skin->text("photo");
  }
 
  /**
   * function do_Del 
   * Xoa 1 ... n  gioi thieu 
   **/

function do_Del ($lang)
  {
    global $func, $DB, $conf, $vnT;

		$id = (int) $vnT->input['id'];
		$ext = $vnT->input["ext"];
		$del = 0;
		$qr = "";
			
		if ($id != 0) {
			$ids = $id;
		}
		if (isset($vnT->input["del_id"])){
			$ids = implode(',', $vnT->input["del_id"]);
		} 		
 		 
		$res = $DB->query("SELECT * FROM gallery WHERE gid IN (" . $ids . ") ");
    if ($DB->num_rows($res))
    {			
      while ($row = $DB->fetch_row($res))  {
        $res_d = $DB->query("SELECT id FROM gallery_desc WHERE gid=".$row['gid']." AND lang<>'".$lang."' ");
				if(!$DB->num_rows($res_d))
				{
					$DB->query("DELETE FROM gallery WHERE  gid=".$row['gid']."  ");
				}	
				$DB->query("DELETE FROM gallery_desc WHERE  gid=".$row['gid']." AND lang='".$lang."' ");	
      }
      $mess = $vnT->lang["del_success"];
			//xoa cache
      $func->clear_cache();
    } else  {
      $mess = $vnT->lang["del_failt"];
    } 
		 
		$ext_page = str_replace("|", "&", $ext);
		$url = $this->linkUrl . "&{$ext_page}";
		$func->html_redirect($url, $mess);
  }
  /**
   * function render_row 
   * list cac record
   **/
  function render_row ($row_info, $lang)
  {
    global $func, $DB, $conf, $vnT;
    $row = $row_info;
    // Xu ly tung ROW
    $id = $row['gid'];
    $row_id = "row_" . $id;
    $output['check_box'] = vnT_HTML::checkbox("del_id[]", $id, 0, " ");
    $link_edit = $this->linkUrl . '&sub=edit&id=' . $id . '&ext=' . $row['ext_page'];
    $link_del = "javascript:del_item('" . $this->linkUrl . "&sub=del&id=" . $id . "&ext=" . $row['ext_page'] . "')";
    $link_photo = $this->linkUrl ."&sub=photo&id=".$id."&lang=".$lang;


    $output['order'] = "<input name=\"txt_Order[{$id}]\" type=\"text\" size=\"2\" maxlength=\"2\" style=\"text-align:center\" value=\"{$row['display_order']}\"  onkeypress=\"return is_num(event,'txtOrder')\" onchange='javascript:do_check($id)' />";
		
		$output['picture'] = get_pic_gallery($row['picture'], 60);
		
    $output['title'] .= "<strong><a href='" . $link_edit . "' >" . $func->HTML($row['title']) . "</a></strong>";   
 				
		$output['cat_name'] ='<b >'.get_cat_name($row['cat_id'],$lang).'</b>';
		
		$poster = "<a href='?mod=admin&act=admin&sub=edit&id=".$row['adminid']."'>".get_admin($row['adminid'])."</a>";
		$info =  "<div style='padding:2px;'>".$vnT->lang['poster']." : <strong>".$poster."</strong></div>";
		$info .= "<div style='padding:2px;'>".$vnT->lang['date_post']." : <b>".@date("d/m/Y",$row['date_post'])."</b></div>";
 		$info .=  "<div style='padding:2px;'>".$vnT->lang['views']." : <strong>".(int)$row['views']."</strong></div>";
		$output['info'] = $info ;

    $output['photo'] = '<a href="' . $link_photo . '" title="Cập nhật hình" ><img src="modules/gallery_ad/images/ico_hinhanh.gif"  alt="Photo "></a>';
		
    $output['num_view'] = (int) $row["num_view"];
    $link_display = $this->linkUrl.$row['ext_link']; 		
    if ($row['display'] == 1) {
      $display = "<a href='".$link_display."&do_hidden=$id' title='".$vnT->lang['click_do_hidden']."' ><img src=\"" . $vnT->dir_images . "/dispay.gif\" width=15  /></a>";
    } else {
      $display = "<a href='".$link_display."&do_display=$id'  title='".$vnT->lang['click_do_display']."' ><img src=\"" . $vnT->dir_images . "/nodispay.gif\"  width=15 /></a>";
    }
    $output['action'] = '<input name=h_id[]" type="hidden" value="' . $id . '" />';
    $output['action'] .= '<a href="' . $link_edit . '"><img src="' . $vnT->dir_images . '/edit.gif"  alt="Edit "></a>&nbsp;';
    $output['action'] .= $display . '&nbsp;';
    $output['action'] .= '<a href="' . $link_del . '"><img src="' . $vnT->dir_images . '/delete.gif"  alt="Delete "></a>';
    return $output;
  }

  /**
   * function do_Manage() 
   * Quan ly cac gioi thieu
   **/
  function do_Manage ($lang)
  {
    global $vnT, $func, $DB, $conf;
		$vnT->html->addStyleSheet( $vnT->dir_js . "/jquery_ui/themes/base/ui.all.css");
		$vnT->html->addScript($vnT->dir_js . "/jquery_ui/ui.core.js");		
		$vnT->html->addScript($vnT->dir_js . "/jquery_ui/ui.datepicker.js");  
    $vnT->html->addScriptDeclaration("
	 		$(function() {
				$('.dates').datepicker({
						showOn: 'button',
						buttonImage: '".$vnT->dir_images."/calendar.gif',
						buttonImageOnly: true,
						changeMonth: true,
						changeYear: true
					});					 

			});
		
		"); 
		
    //update
    if ($vnT->input["do_action"]) {
      //xoa cache
      $func->clear_cache();
      if ($vnT->input["del_id"])
        $h_id = $vnT->input["del_id"];
      switch ($vnT->input["do_action"]) {
        case "do_edit":
          if (isset($vnT->input["txt_Order"]))   $arr_order = $vnT->input["txt_Order"];
          $mess .= "- " . $vnT->lang['edit_success'] . " ID: <strong>";
          $str_mess = "";
          for ($i = 0; $i < count($h_id); $i ++) {
            $dup['display_order'] = $arr_order[$h_id[$i]];
            $ok = $DB->do_update("gallery", $dup, "gid=" . $h_id[$i]);
            if ($ok) {
              $str_mess .= $h_id[$i] . ", ";
            }
          }
          $mess .= substr($str_mess, 0, - 2) . "</strong><br>";
          $err = $func->html_mess($mess);
        break;
        case "do_hidden":
          $mess .= "- " . $vnT->lang['hidden_success'] . " ID: <strong>";
          for ($i = 0; $i < count($h_id); $i ++) {
            $dup['display'] = 0;
            $ok = $DB->do_update("gallery", $dup, "gid=" . $h_id[$i]);
            if ($ok) {
              $str_mess .= $h_id[$i] . ", ";
            }
          }
          $mess .= substr($str_mess, 0, - 2) . "</strong><br>";
          $err = $func->html_mess($mess);
        break;
        case "do_display":
          $mess .= "- " . $vnT->lang['display_success'] . "  ID: <strong>";
          for ($i = 0; $i < count($h_id); $i ++) {
            $dup['display'] = 1;
            $ok = $DB->do_update("gallery", $dup, "gid=" . $h_id[$i]);
            if ($ok) {
              $str_mess .= $h_id[$i] . ", ";
            }
          }
          $mess .= substr($str_mess, 0, - 2) . "</strong><br>";
          $err = $func->html_mess($mess);
        break;
      }
    }
		if((int)$vnT->input["do_display"]) {				
			$ok = $DB->query("Update gallery SET display=1 WHERE gid=".$vnT->input["do_display"]);
			if($ok){
				$mess .= "- " . $vnT->lang['display_success'] . "  ID: <strong>".$vnT->input["do_display"] . "</strong><br>";	
				$err = $func->html_mess($mess);
			}        
			//xoa cache
      $func->clear_cache();
		}
		if((int)$vnT->input["do_hidden"]) {				
			$ok = $DB->query("Update gallery SET display=0 WHERE gid=".$vnT->input["do_hidden"]);
			if($ok){
				$mess .= "- " . $vnT->lang['display_success'] . "  ID: <strong>".$vnT->input["do_hidden"] . "</strong><br>";	
				$err = $func->html_mess($mess);
			}    
			//xoa cache
      $func->clear_cache();
		}
		
    $p = ((int) $vnT->input['p']) ? $p = $vnT->input['p'] : 1;
    $n = ($conf['record']) ? $conf['record'] : 30;
    $where = "";
    $ext_page = "";
    $ext = "";

    $cat_id = ((int) $vnT->input['cat_id']) ?  $vnT->input['cat_id'] : 0;
    $search = ($vnT->input['search']) ? $vnT->input['search'] : "g.gid";
    $keyword = ($vnT->input['keyword']) ? $vnT->input['keyword'] : "";
		$adminid = ((int) $vnT->input['adminid']) ?   $vnT->input['adminid'] : 0;
		$date_begin = ($vnT->input['date_begin']) ?  $vnT->input['date_begin'] : "";
		$date_end = ($vnT->input['date_end']) ?  $vnT->input['date_end'] : "";
    
	  if (! empty($cat_id)) {
      $a_cat_id = List_SubCat($cat_id);
      $a_cat_id = substr($a_cat_id, 0, - 1);
      if (empty($a_cat_id))
        $where .= " and FIND_IN_SET('$cat_id',cat_id)<>0 ";
      else {
        $tmp = explode(",", $a_cat_id);
        $str_ = " FIND_IN_SET('$cat_id',cat_id)<>0 ";
        for ($i = 0; $i < count($tmp); $i ++) {
          $str_ .= " or FIND_IN_SET('$tmp[$i]',cat_id)<>0 ";
        }
        $where .= " and (" . $str_ . ") ";
      }
      $ext_page .= "cat_id=$cat_id|";
      $ext .= "&cat_id=$cat_id";
    }
		
		if(!empty($adminid)){
			$where .=" and adminid=$adminid ";
			$ext_page.="adminid=$adminid|";
			$ext.="&adminid={$adminid}";
		}
		
		if($date_begin || $date_end )
		{
			$tmp1 = @explode("/", $date_begin);
			$time_begin = @mktime(0, 0, 0, $tmp1[1], $tmp1[0], $tmp1[2]);
			
			$tmp2 = @explode("/", $date_end);
			$time_end = @mktime(23, 59, 59, $tmp2[1], $tmp2[0], $tmp2[2]);
			
			$where.=" AND (date_post BETWEEN {$time_begin} AND {$time_end} ) ";
			$ext.="&date_begin=".$date_begin."&date_end=".$date_end;
			$ext_page.= "date_begin=".$date_begin."|date_end=".$date_end."|";
		}
		
    if (! empty($search)) {
      $ext_page .= "search=$search|";
      $ext .= "&search={$search}";
    }
    if (! empty($keyword)) {
      switch ($search) {
        case "gid": $where .= " and gid = $keyword ";   break;
        case "date_post": $where .= " and DATE_FORMAT(FROM_UNIXTIME(date_post),'%d/%m/%Y') = '{$keyword}' ";  break;
        default: $where .= " and $search like '%$keyword%' ";  break;
      }
      $ext_page .= "keyword=$keyword|";
      $ext .= "&keyword={$keyword}";
    }

    $ext_page = $ext_page . "|p=$p";

    //echo $search ;
    $query = $DB->query("SELECT g.gid
													FROM gallery g, gallery_desc gd
													WHERE g.gid=gd.gid
													AND lang='$lang' 
													{$where} ");
    $totals = intval($DB->num_rows($query));
    $num_pages = ceil($totals / $n);
    if ($p > $num_pages)    $p = $num_pages;
    if ($p < 1)    $p = 1;
    $start = ($p - 1) * $n;
    $nav = $func->paginate($totals, $n, $ext, $p);
    $table['link_action'] = $this->linkUrl . "{$ext}&p=$p";
		$ext_link = $ext."&p=$p" ;
    $table['title'] = array(
      'check_box' => "<input type=\"checkbox\" name=\"checkall\" id=\"checkall\" class=\"checkbox\" />|5%|center" , 
      'order' => $vnT->lang['order'] . " |10%|center" , 
			'picture' => $vnT->lang['picture'] . " |10%|center" , 
      'title' => $vnT->lang['title_file'] . " |30%|left" , 
			'photo' =>  "Photo |10%|center" ,
      'info' =>$vnT->lang['infomartion']. "| |left",
      'action' => "Action|12%|center");
    $sql = "SELECT *
						FROM gallery g, gallery_desc gd
						WHERE  
						g.gid=gd.gid AND lang='$lang'
						{$where}
						ORDER BY display_order DESC, date_post DESC  
						LIMIT $start,$n";
    //    print "sql = ".$sql."<br>";
    $reuslt = $DB->query($sql);
    if ($DB->num_rows($reuslt)) {
      $row = $DB->get_array($result);
      for ($i = 0; $i < count($row); $i ++) {
				$row[$i]['ext_link'] = $ext_link ;
				$row[$i]['ext_page'] = $ext_page;
        $row_info = $this->render_row($row[$i], $lang);
        $row_field[$i] = $row_info;
        $row_field[$i]['stt'] = ($i + 1);
        $row_field[$i]['row_id'] = "row_" . $row[$i]['gid'];
        $row_field[$i]['ext'] = "";
      }
      $table['row'] = $row_field;
    } else {
      $table['row'] = array();
      $table['extra'] = "<div align=center class=font_err >" . $vnT->lang['no_have_gallery'] . "</div>";
    }
    $table['button'] = '<input type="button" name="btnHidden" value=" ' . $vnT->lang['hidden'] . ' " class="button" onclick="do_submit(\'do_hidden\')">&nbsp;';
    $table['button'] .= '<input type="button" name="btnDisplay" value=" ' . $vnT->lang['display'] . ' " class="button" onclick="do_submit(\'do_display\')">&nbsp;';
    $table['button'] .= '<input type="button" name="btnEdit" value=" ' . $vnT->lang['update'] . ' " class="button" onclick="do_submit(\'do_edit\')">&nbsp;';
    $table['button'] .= '<input type="button" name="btnDel" value=" ' . $vnT->lang['delete'] . ' " class="button" onclick="del_selected(\'' . $this->linkUrl . '&sub=del&ext=' . $ext_page . '\')">';
    $table_list = $func->ShowTable($table);
    $data['table_list'] = $table_list;
    $data['totals'] = $totals;
    $data['keyword'] = $keyword;
    $data['list_cat'] = Get_Cat($cat_id, $lang);
    $data['date_begin'] = $date_begin;
		$data['date_end'] = $date_end;
    $data['list_search'] = List_Search($search);
		if($vnT->admininfo['adminid']==1)
		{
			$data['row_other'] = '<tr>
				<td > <strong> Admin post : </strong></td>
				<td> '.list_admin($adminid).'</td>
			</tr>';
		}
		
    $data['keyword'] = $keyword; 
		
    $data['err'] = $err;
    $data['nav'] = $nav;
    /*assign the array to a template variable*/
    $this->skin->assign('data', $data);
    $this->skin->parse("manage");
    return $this->skin->text("manage");
  }
  // end class
}
?>