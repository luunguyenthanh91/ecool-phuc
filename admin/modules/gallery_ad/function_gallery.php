<?php
/*================================================================================*\
|| 							Name code : funtions_gallery.php 		 			          	     		  # ||
||  				Copyright © 2008 by Thai Son - CMS vnTRUST                          # ||
\*================================================================================*/
/**
 * @version : 2.0
 * @date upgrade : 03/02/2008 by Thai Son
 **/
if (! defined('IN_vnT')) {
  die('Hacking attempt!');
}
define('MOD_DIR_UPLOAD', '../vnt_upload/gallery/');
define('MOD_ROOT_URL', $conf['rooturl'] . 'modules/gallery/');
define('MOD_DIR_TMP', '../vnt_upload/tmp/');
$vnT->func->include_libraries('vntrust.base.object');
$vnT->func->include_libraries('vntrust.filesystem.path');
$vnT->func->include_libraries('vntrust.filesystem.folder');
$vnT->func->include_libraries('vntrust.filesystem.file');

/*-------------- loadSetting --------------------*/
function loadSetting ($lang="vn")
{
  global $vnT, $func, $DB, $conf;
  $setting = array();
  $result = $DB->query("select * from gallery_setting WHERE lang='$lang'");
  $setting = $DB->fetch_row($result);
  foreach ($setting as $k => $v) {
    $vnT->setting[$k] = stripslashes($v);
  }
  unset($setting);
}


//-----------------  get_cat_name
function get_cat_name ($cat_id, $lang)
{
  global $vnT, $func, $DB, $conf;
  $text = "";
  $sql = "select cat_name from gallery_category_desc where cat_id =$cat_id and lang='{$lang}' ";
  $result = $DB->query($sql);
  if ($row = $DB->fetch_row($result)) {
    $text = $func->HTML($row['cat_name']);
  }
  return $text;
}


//-----------------  get_catCode
function get_catCode ($parentid, $cat_id)
{
  global $vnT, $func, $DB, $conf;
  $text = "";
  $sql = "select cat_id,cat_code from gallery_category where cat_id =$parentid ";
  $result = $DB->query($sql);
  if ($row = $DB->fetch_row($result)) {
    $text = $row['cat_code'] . "_" . $cat_id;
  } else
    $text = $cat_id;
  return $text;
}


/*** Ham Get_Cat ****/
function Get_Cat ($did = -1, $lang, $ext = "")
{
  global $func, $DB, $conf;
  $text = "<select size=1 id=\"cat_id\" name=\"cat_id\" {$ext} >";
  $text .= "<option value=\"\">-- Root --</option>";
  $query = $DB->query("SELECT * FROM gallery_category g, gallery_category_desc gd
					WHERE g.cat_id=gd.cat_id
					AND lang='$lang' 
					AND parentid=0 
					order by cat_order DESC , g.cat_id DESC");
  while ($cat = $DB->fetch_row($query)) {
    $cat_name = $func->HTML($cat['cat_name']);
    if ($cat['cat_id'] == $did)
      $text .= "<option value=\"{$cat['cat_id']}\" selected>{$cat_name}</option>";
    else
      $text .= "<option value=\"{$cat['cat_id']}\" >{$cat_name}</option>";
    $n = 1;
    $text .= Get_Sub($did, $cat['cat_id'], $n, $lang);
  }
  $text .= "</select>";
  return $text;
}

/*** Ham Get_Sub   */
function Get_Sub ($did, $cid, $n, $lang)
{
  global $func, $DB, $conf;
  $output = "";
  $k = $n;
  $query = $DB->query("SELECT * FROM gallery_category g, gallery_category_desc gd
					WHERE g.cat_id=gd.cat_id AND lang='$lang' 
					AND parentid={$cid} 
					order by  cat_order DESC , g.cat_id DESC");
  while ($cat = $DB->fetch_row($query)) {
    $cat_name = $func->HTML($cat['cat_name']);
    if ($cat['cat_id'] == $did) {
      $output .= "<option value=\"{$cat['cat_id']}\" selected>";
      for ($i = 0; $i < $k; $i ++)
        $output .= "|-- ";
      $output .= "{$cat_name}</option>";
    } else {
      $output .= "<option value=\"{$cat['cat_id']}\" >";
      for ($i = 0; $i < $k; $i ++)
        $output .= "|-- ";
      $output .= "{$cat_name}</option>";
    }
    $n = $k + 1;
    $output .= Get_Sub($did, $cat['cat_id'], $n, $lang);
  }
  return $output;
}


/**
 * Ham List_SubCat 
 *
 * @param	$cat_id : cat_id cua category
 *
 * @return	$output : 1 chuoi subcat (vd: 2,4,6,8,)  co dau , cuoi cung
 */
function List_SubCat ($cat_id)
{
  global $func, $DB, $conf;
  $output = "";
  $query = $DB->query("SELECT * FROM gallery_category WHERE parentid={$cat_id}");
  while ($cat = $DB->fetch_row($query)) {
    $output .= $cat["cat_id"] . ",";
    $output .= List_SubCat($cat['cat_id']);
  }
  return $output;
}

/*** Ham Get_Category ****/
function Get_Category ($selname, $did = -1, $lang = "vn", $ext = "")
{
  global $func, $DB, $conf;
  $text = "<select size=1 id=\"{$selname}\" name=\"{$selname}\" {$ext} class='select'>";
  $text .= "<option value=\"\">-- Root --</option>";
  $query = $DB->query("SELECT * FROM gallery_category g, gallery_category_desc gd
					WHERE g.cat_id=gd.cat_id AND lang='$lang' 
					AND  parentid=0 
					order by cat_order DESC , g.cat_id DESC");
  while ($cat = $DB->fetch_row($query)) {
    $cat_name = $func->HTML($cat['cat_name']);
    if ($cat['cat_id'] == $did)
      $text .= "<option value=\"{$cat['cat_id']}\" selected>{$cat_name}</option>";
    else
      $text .= "<option value=\"{$cat['cat_id']}\" >{$cat_name}</option>";
    $n = 1;
    $text .= Get_Sub_Category($selname, $did, $cat['cat_id'], $n,$lang);
  }
  $text .= "</select>";
  return $text;
}

/*** Ham Get_Sub   */
function Get_Sub_Category ($selname, $did, $cid, $n,$lang)
{
  global $func, $DB, $conf;
  $output = "";
  $k = $n;
	
 
  $query = $DB->query("SELECT * FROM gallery_category g, gallery_category_desc gd
					WHERE  g.cat_id=gd.cat_id AND lang='$lang' 
					AND  parentid={$cid}
					order by cat_order DESC , g.cat_id DESC");
  while ($cat = $DB->fetch_row($query)) {
    $cat_name = $func->HTML($cat['cat_name']);
    if ($cat['cat_id'] == $did) {
      $output .= "<option value=\"{$cat['cat_id']}\" selected>";
      for ($i = 0; $i < $k; $i ++)
        $output .= "|-- ";
      $output .= "{$cat_name}</option>";
    } else {
      $output .= "<option value=\"{$cat['cat_id']}\" >";
      for ($i = 0; $i < $k; $i ++)
        $output .= "|-- ";
      $output .= "{$cat_name}</option>";
    }
    $n = $k + 1;
    $output .= Get_Sub_Category($selname, $did, $cat['cat_id'], $n,$lang);
  }
  return $output;
}



//-----------------  get_pic_gallery
function get_pic_gallery ($picture, $w = 150)
{
  global $vnT, $func, $DB, $conf;
  $out = "";
  $w_thumb = ($vnT->setting['imgthumb_width']) ? $vnT->setting['imgthumb_width'] : 100;
  if ($picture) {
    $linkhinh = "../vnt_upload/gallery/" . $picture;
    $linkhinh = str_replace("//", "/", $linkhinh);
    $dir = substr($linkhinh, 0, strrpos($linkhinh, "/"));
    $pic_name = substr($linkhinh, strrpos($linkhinh, "/") + 1);
    $src = $dir . "/thumbs/" . $pic_name; 
  }
  if ($w < $w_thumb) $ext = " width='$w' ";
  $out = "<img  src=\"{$src}\" {$ext} >";
	
  return $out;
}

//======================= List_Search =======================
function List_Search ($did,$ext="")
{
  global $func, $DB, $conf, $vnT;
	$arr_where = array('gid'=> $vnT->lang['id'],'title'=>  $vnT->lang['title'], 'date_post' => $vnT->lang['date_post'],'views'=> $vnT->lang['view_num'] );
	
	$text= "<select size=1 name=\"search\" id='search' class='select' {$ext} >";
	foreach ($arr_where as $key => $value)
	{
		$selected = ($did==$key) ? "selected"	: "";
		$text.="<option value=\"{$key}\" {$selected} > {$value} </option>";
	}	
	$text.="</select>";	
	 
  return $text;
}
//------list_admin
function list_admin ($did,$ext=""){
	global $func,$DB,$conf,$vnT;

	$text= "<select name=\"adminid\" id=\"adminid\"   class='select' {$ext} >";
	$text .= "<option value=\"\" selected>-- Chọn Admin --</option>";
	$sql="SELECT adminid,username	FROM admin order by  username ASC ";
	
	$result = $DB->query ($sql);
	$i=0;
	while ($row = $DB->fetch_row($result))
	{
		$i++;
		$username = $func->HTML($row['username']);
		$selected = ($did==$row['adminid']) ? " selected " : "";
		$text .= "<option value=\"{$row['adminid']}\" ".$selected." >  ".$username."  </option>";
	}
	
	$text.="</select>";
	return $text;
}

//------get_admin
function get_admin($adminid) {
global $vnT,$func,$DB,$conf;
	$text = "Admin";
	$sql ="select username from admin where adminid =$adminid  ";
	$result =$DB->query ($sql);
	if ($row = $DB->fetch_row($result)){
		$text = $func->HTML($row['username']);
	}
	return $text;
}



function do_Rebuild_Comment ($id)
{
  global $vnT, $func, $DB, $conf;

  $result = $DB->query("SELECT cid FROM gallery_comment WHERE id=".$id." AND display=1");
  $num = $DB->num_rows($result);

  $DB->query("UPDATE gallery SET num_comment=".$num." WHERE gid=".$id) ;

}


?>