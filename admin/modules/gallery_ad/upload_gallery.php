<?php
/*================================================================================*\
|| 							Name code : modname.php 		 		            	  ||
||  				Copyright @2008 by Thai Son - CMS vnTRUST                     ||
\*================================================================================*/
/**
 * @version : 2.0
 * @date upgrade : 09/01/2009 by Thai Son
 **/
if (! defined('IN_vnT')) {
  die('Hacking attempt!');
}
$vntModule = new vntModule();

class vntModule
{
  var $output = "";
  var $skin = "";
  var $linkUrl = "";
  var $module = "gallery";
  var $action = "upload_gallery";

  /**
   * function vntModule ()
   * Khoi tao 
   **/
  function vntModule ()
  {
    global $Template, $vnT, $func, $DB, $conf;
    require_once ("function_" . $this->module . ".php");
    $this->skin = new XiTemplate(DIR_MODULE . DS . $this->module . "_ad" . DS . "html" . DS . $this->action . ".tpl");
    $this->skin->assign('LANG', $vnT->lang); 
		$this->skin->assign('PATH_ROOT', $conf['rootpath']);
    $this->skin->assign('LANG', $vnT->lang);
    $this->skin->assign('DIR_IMAGE', $vnT->dir_images);
    $this->skin->assign('DIR_JS', $conf['rooturl'] . "js");
    $this->skin->assign("admininfo", $vnT->admininfo);
    $lang = ($vnT->input['lang']) ? $lang = $vnT->input['lang'] : $func->get_lang_default();
    $this->linkUrl = "?mod=" . $this->module . "&act=" . $this->action . "&lang=" . $lang;
		loadSetting($lang);
		
    switch ($vnT->input['sub']) {
      case 'add_directory':
        $nd['content'] = $this->do_Add_Directory($lang);
      break;
      case 'directory':
        $nd['f_title'] = $vnT->lang['title_directory_upload'];
        $nd['content'] = $this->do_Upload_Directory($lang);
      break;
      case 'add_multi':
				$nd['f_title'] = $vnT->lang['title_multi_upload'];
        $nd['content'] = $this->do_Add_Multi($lang);
      break;
      default:
        $nd['f_title'] = $vnT->lang['title_multi_upload'];
        $nd['content'] = $this->do_Upload_Multi($lang);
      break;
    }
    $nd['menu'] = $func->getToolbar($this->module, $this->action, $lang);
    $nd['row_lang'] = $func->html_lang("?mod=" . $this->module . "&act=" . $this->action, $lang);
    $Template->assign("data", $nd);
    $Template->parse("box_main");
    $vnT->output .= $Template->text("box_main");
  }

  function do_Add_Directory ($lang)
  {
    global $func, $DB, $conf, $vnT;
    if ($vnT->input['do_action'] == "add_directory") {
      if ($vnT->input["del_id"])
        $h_id = $vnT->input["del_id"];
      if ($vnT->input['cat_id'])
        $cat_id = $vnT->input['cat_id'];
      else
        $err = $vnT->lang['empty_category'];
      if (empty($err)) {
        if (isset($vnT->input["file"]))        $arr_file = $vnT->input["file"];
        if (isset($vnT->input["file_name"]))    $arr_file_name = $vnT->input["file_name"];
        if (isset($vnT->input["file_size"]))    $arr_file_size = $vnT->input["file_size"];
        if (isset($vnT->input["file_ext"]))    $arr_file_ext = $vnT->input["file_ext"];
          // tao thu muc hinh upload
        $dir = get_dir_upload();
        $w_pic = 1500;
        $w_thumb = ($vnT->setting['imgthumb_width']) ? $vnT->setting['imgthumb_width'] : 200;
        $arr_pic = array();
        for ($i = 0; $i < count($h_id); $i ++) {
          $file = $arr_file[$h_id[$i]];
          $arr_pic[$h_id[$i]] =  $file;
        }
         
        if (is_array($arr_pic)) {
          $up['path'] = MOD_DIR_UPLOAD;
          $up['dir'] = $dir;
          $up['file'] = $arr_pic;
          $up['type'] = "hinh";
          $up['w'] = $w_pic;
          $up['thum'] = 1;
          $up['change_name_thum'] = $vnT->setting['thum_size'];
          $up['w_thum'] = $w_thumb; 
				
          $result = $vnT->File->Muti_UploadURL($up);
          if (empty($result['err'])) {
            //for result
            foreach ($result['link'] as $key => $value) {
              $title = $arr_file_name[$key];
              $picture = $value;
              //thu muc
              if ($dir && $value) {
                $picture = $dir . "/" . $picture;
              }
              $cot['cat_id'] = $cat_id;
              $cot['picture'] = $picture;
              $cot['file_type'] = $result['type'][$key];
              $cot['file_size'] = $result['size'][$key];
							
							//SEO
					
              $cot['date_post'] = time();
              $ok = $DB->do_insert("gallery_images", $cot);
              if ($ok) {
                $gid = $DB->insertid();
                //update content
                $cot_d['gid'] = $gid;
                $cot_d['title'] = $title;
								//SEO
								$cot_d['friendly_url'] =  $func->make_url($title);
								$cot_d['friendly_title'] = $title ." - ". $func->utf8_to_ascii($title);
							
                $query_lang = $DB->query("select name from language ");
                while ($row = $DB->fetch_row($query_lang)) {
                  $cot_d['lang'] = $row['name'];
                  $DB->do_insert("gallery_images_desc", $cot_d);
                }								 
								
								$maso = create_maso ($cat_id,$gid);
								$DB->query("UPDATE gallery_images SET maso='$maso' WHERE gid=$gid ");
						     
                //xoa cua
                $des_dir_file = $arr_file[$key];
                if (@file_exists($des_dir_file))   @unlink($des_dir_file);
								
                $err .= "&raquo;&nbsp;" . $vnT->lang['add_success'] . " {$title}<br>";
              } else {
                $err .= "&raquo;&nbsp;" . $vnT->lang['add_failt'] . " {$title}<br>";
              }
            }
          } else {
            $err = $func->html_err($result['err']);
          }
        }
      } else {
        $err = $func->html_err($vnT->lang["empty_category"]);
      }
      $url = $this->linkUrl . "&sub=directory";
      $func->html_redirect($url, $err);
    } else {
      $err = $vnT->lang['action_no_active'];
      $url = $this->linkUrl . "&sub=directory";
      $func->html_redirect($url, $err);
    }
  }

  function render_row_directory ($row_info, $lang)
  {
    global $func, $DB, $conf, $vnT;
    $row = $row_info;
    $id = $row['file_id'];
    $file = $row['file'];
		$f = $row['folder'];
    $output['check_box'] = vnT_HTML::checkbox("del_id[]", $id, 0, " ");
    $output["picture"] = "<img src=\"{$f}/{$file}\" width=\"100\">";

		$file_name = str_replace(array("-","_","+")," ",$row['file_name']);
		$output['title'] = "<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
													<tr>
														<td>
															<input name=\"file_name[{$id}]\" type=\"text\" size=\"70\" value=\"{$file_name}\" onchange='javascript:do_check($id)' />															
														</td>
													</tr>
													<tr>
														<td>{$vnT->lang['file']}: {$row['file']}<input name=\"file[{$id}]\" type=\"hidden\" value=\"{$f}/{$row['file']}\" /></td>
													</tr>
													<tr>
														<td>{$vnT->lang['file_size']}: {$row['file_size']}<input name=\"file_size[{$id}]\" type=\"hidden\" value=\"{$row['file_size']}\" /></td>
													</tr>
													<tr>
														<td>{$vnT->lang['file_type']}: {$row['file_ext']}<input name=\"file_ext[{$id}]\" type=\"hidden\" value=\"{$row['file_ext']}\" /></td>
													</tr>
												</table>";
    return $output;
  }

  /**
   * function do_Add 
   * Them  moi 
   **/
  function do_Upload_Directory ($lang)
  {
    global $vnT, $func, $DB, $conf;
    $vnT->html->addScript("modules/gallery_ad/js/gallery.js");
    $p = ((int) $vnT->input['p']) ? $p = $vnT->input['p'] : 1;
    $n = ($conf['record']) ? $conf['record'] : 30;
		$f = ($vnT->input['folder']) ? $vnT->input['folder'] : MOD_DIR_TMP;
		
		
    $table['link_action'] = $this->linkUrl . "&sub=add_directory";
    $table['title'] = array(
      'check_box' => "<input type=\"checkbox\" name=\"checkall\" id=\"checkall\" class=\"checkbox\" />|5%|center" , 
      'picture' => $vnT->lang['picture'] . " |20%|center" , 
      'title' => $vnT->lang['title_file'] . " |75%|left");
    $sql_pic = "select url from gallery_images";
    $result_pic = $DB->query($sql_pic);
    $arr_pic_exist = $DB->fetch_row($result_pic);
    $totals = getNumFile(MOD_DIR_UPLOAD, $arr_pic_exist);
    $num_pages = ceil($totals / $n);
    if ($p > $num_pages)    $p = $num_pages;
    if ($p < 1)     $p = 1;
    $start = ($p - 1) * $n;
    $nav = $func->paginate($totals, $n, $ext, $p, $class = "pagelink");
    $picture = showPic($f, $start, $n, $arr_pic_exist);
		
    if (count($picture)) {
      $row = $picture;
      for ($i = 0; $i < count($row); $i ++) {
				$row[$i]['folder'] = $f;
        $row_info = $this->render_row_directory($row[$i], $lang);
        $row_field[$i] = $row_info;
        $row_field[$i]['stt'] = ($i + 1);
        $row_field[$i]['row_id'] = "row_" . $row[$i]['file_id'];
        $row_field[$i]['ext'] = "";
      }
      $table['row'] = $row_field;
    } else {
      $table['row'] = array();
      $table['extra'] = "<div align=center class=font_err >" . $vnT->lang['no_have_gallery'] . "</div>";
    }
    $table['button'] .= '<input type="button" name="btnEdit" value=" ' . $vnT->lang['update'] . ' " class="button" onclick="do_submit_gallery(\'add_directory\')">&nbsp;';
    $table_list = $func->ShowTable($table);
    $data['table_list'] = $table_list;
    $data['totals'] = $totals;
    $data['list_cat'] = Get_Cat($vnT->input['cat_id'], $lang);
		$data['list_folder'] =getListFolder($_POST['folder']);
    $data['err'] = $err;
    $data['nav'] = $nav;
    /*assign the array to a template variable*/
    $this->skin->assign('data', $data);
    $this->skin->parse("upload_directory");
    return $this->skin->text("upload_directory");
  }

  /**
   * function do_Add_Multi 
   * Them   
   **/
  function do_Add_Multi ($lang)
  {
    global $vnT, $func, $DB, $conf;
    $vnT->html->addStyleSheet($vnT->dir_js . "/plupload/jquery.plupload.queue/css/jquery.plupload.queue.css");
    $vnT->html->addScript($vnT->dir_js . "/plupload/browserplus-min.js");
    $vnT->html->addScript($vnT->dir_js . "/plupload/plupload.full.js");
    $vnT->html->addScript($vnT->dir_js . "/plupload/i18n/vi.js");
    $vnT->html->addScript($vnT->dir_js . "/plupload/jquery.plupload.queue/jquery.plupload.queue.js");
		
		$dir = get_dir_upload();
		$folder_upload = $conf['rootpath']."vnt_upload/gallery/".$dir ;
		$w_pic = ($vnT->setting['img_width']) ? $vnT->setting['img_width'] : 2000;
    $w_thumb = ($vnT->setting['imgthumb_width']) ? $vnT->setting['imgthumb_width'] : 200;

    $err = "";		
		$cat_id = (int)$vnT->input['cat_id'];
		$res_cat = $DB->query("SELECT n.cat_id,nd.cat_name FROM gallery_category n , gallery_category_desc nd WHERE n.cat_id=nd.cat_id AND lang='$lang' AND n.cat_id=$cat_id");
		if($row_cat = $DB->fetch_row($res_cat))
		{
			$data['cat_name'] = $func->HTML($row_cat['cat_name']);
			$data['cat_id'] = $cat_id;
			
 			$data['w_pic'] = $w_pic;
			$data['w_thumb'] = $w_thumb;
			$data['folder_upload'] = $folder_upload ;
			$data['folderpath'] =   $conf['rootpath']."vnt_upload/gallery" ;
			$data['folder'] = $dir ;
			$data['max_upload'] = ini_get('upload_max_filesize');
		
		}else{
			$mess = $vnT->lang['not_found'] . " catID : " . $cat_id;
      $url = $this->linkUrl;
      $func->html_redirect($url, $mess);	
		}
     
    $data['link_action'] = "?mod=gallery&act=gallery&cat_id=".$cat_id; 
		
    /*assign the array to a template variable*/
    $this->skin->assign('data', $data);
    $this->skin->parse("upload_multi_add");
    return $this->skin->text("upload_multi_add");
  }

  /**
   * function do_Upload_Multi() 
   * Quan ly nhieu hinh thu vien
   **/
  function do_Upload_Multi ($lang)
  {
    global $vnT, $func, $DB, $conf;
    $data['list_cat'] = Get_Cat($vnT->input['cat_id'], $lang);  
    $data['link_action'] = $this->linkUrl . "&sub=add_multi";
    /*assign the array to a template variable*/
    $this->skin->assign('data', $data);
    $this->skin->parse("upload_multi");
    return $this->skin->text("upload_multi");
  }
  // end class
}
?>