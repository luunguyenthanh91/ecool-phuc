<?php
/*================================================================================*\
|| 							Name code : funtions_music.php 		 			      	         		  # ||
||  				Copyright © 2007 by Thai Son - CMS vnTRUST                          # ||
\*================================================================================*/
/**
 * @version : 1.0
 * @date upgrade : 11/12/2007 by Thai Son
 **/
/*=======================CHANGE LOG================================================
DATE 14/12/2007 :
	- function getToolbar 	them 2 var mod va act de xac dinh ( la`m bieng edit wa ), 
													khong su dung lang (hktrung)
													
	- define MOD_DIR_UPLOAD		thu muc upload cua module ( hktrung )
==================================================================================*/
if (!defined('IN_vnT')) {
  die('Hacking attempt!');
}
define('MOD_DIR_UPLOAD', '../vnt_upload/product/');
define('ROOT_UPLOAD', 'vnt_upload/product/');
define('MOD_ROOT_URL', $conf['rooturl'] . 'modules/product/');

class Model
{
  function __construct(){
    $lang = ($_GET['lang']) ? $_GET['lang'] : "vn";
    $this->loadSetting($lang);
  }
  /*-------------- loadSetting --------------------*/
  function loadSetting($lang = "vn"){
    global $vnT, $func, $DB, $conf;
    $setting = array();
    $result = $DB->query("select * from product_setting WHERE lang='$lang' ");
    $setting = $DB->fetch_row($result);
    foreach ($setting as $k => $v) {
      $vnT->setting[$k] = stripslashes($v);
    }
    unset($setting);
    $res_s = $DB->query("SELECT * FROM order_status WHERE display=1 ORDER BY s_order ASC");
    while($row_s = $DB->fetch_row($res_s)){
      if($row_s['is_default']==1)	 {
        $vnT->setting['status_default']	= $row_s['status_id'];
      }
      if($row_s['is_confirm']==1)  {
        $vnT->setting['status_confirm'] = $row_s['status_id'];
      }
      if($row_s['is_shipping']==1)	 {
        $vnT->setting['status_shipping']	= $row_s['status_id'];
      }
      if($row_s['is_complete']==1)	 {
        $vnT->setting['status_complete']	= $row_s['status_id'];
      }
      if($row_s['is_cancel']==1)	 {
        $vnT->setting['status_cancel']	= $row_s['status_id'];
      }
      if($row_s['is_customer']==1)	 {
        $vnT->setting['status_customer']	= $row_s['status_id'];
      }
      $vnT->setting['status'][$row_s['status_id']] = "<b style='color:#".$row_s['color']."'>".$func->fetch_content($row_s['title'], $lang)."</b>";
    }
    $res_ad = $DB->query("SELECT adminid,username FROM admin  ORDER BY adminid ASC") ;
    while($row_ad = $vnT->DB->fetch_row($res_ad)) {
      $vnT->setting['arr_admin'][$row_ad['adminid']] = $row_ad['username'];
    }
    $res_city = $DB->query("SELECT id,name FROM iso_cities ORDER BY c_order ASC , name ASC");
    while ($row_city = $DB->fetch_row($res_city))  {
      $vnT->setting['arr_city'][$row_city['id']] = $row_city['name'];
    }
    $res_s = $DB->query("SELECT id,name FROM iso_states ORDER BY city ASC, s_order  ASC, name ASC");
    while ($row_s = $DB->fetch_row($res_s))  {
      $vnT->setting['arr_state'][$row_s['id']] = $row_s['name'];
    }
  }
  function get_price_pro ($price, $default = "0"){
    global $func, $DB, $conf, $vnT;
    if ($price) {
      $price = $func->format_number($price) . " đ";
    } else {
      $price = $default;
    }
    return $price;
  }
  function get_info_address ($data, $type = ""){
    global $DB, $input, $func, $vnT, $conf;
    if ($type == "shipping") {
      $text = $vnT->lang['full_name'] . " : <b>" . $data['c_name'] . "</b>";
      $text .= "<br>" . $vnT->lang['address'] . " : " . $data['c_address'];
      if ($data['c_state'])
        $text .= ", " . $vnT->setting['arr_state'][$data['c_state']];
      if ($data['c_city'])
        $text .=  ", " . $vnT->setting['arr_city'][$data['c_city']];
      //$text .= "<br> " . $vnT->lang['country'] . " : " . get_country_name($data['c_country']);
      if ($data['c_phone'])
        $text .= "<br>" . $vnT->lang['phone'] . " : " . $data['c_phone'];
      if ($data['c_mobile'])
        $text .= "<br>" . $vnT->lang['mobile'] . " : " . $data['c_mobile'];
    } else {
      $type_mem = ($data['mem_id'] == 0) ? "Khách vãng lai" : "Thành viên";
      $text = $vnT->lang['full_name'] . " : <b>" . $data['d_name'] . "</b> <span class=font_err>(" . $type_mem . ")</span>";
      if ($data['d_company'])
        $text .= "<br>" . $vnT->lang['company'] . " : " . $data['d_company'];
      $text .= "<br>" . $vnT->lang['address'] . " : " . $data['d_address'];
      if ($data['d_state'])
        $text .= ", " . $vnT->setting['arr_state'][$data['d_state']];
      if ($data['d_city'])
        $text .= ", " . $vnT->setting['arr_city'][$data['d_city']];
      //$text .= "<br> " . $vnT->lang['country'] . " : " . get_country_name($data['c_country']);

      if ($data['d_zipcode'])
        $text .= ", " . $data['d_zipcode'];
      $text .= "<br>" . $vnT->lang['phone'] . " : " . $data['d_phone'];
      if ($data['d_mobile'])
        $text .= "<br>" . $vnT->lang['mobile'] . " : " . $data['d_mobile'];
      if ($data['d_fax'])
        $text .= "<br>Fax : " . $data['d_fax'];
      if ($data['d_email'])
        $text .= "<br>E-mail : " . $data['d_email'];
    }
    return $text;
  }



  /*-------------- List_Option_City --------------------*/
  function List_Option_City ($country="VN" ,$did = "", $default = "" )
  {
    global $vnT, $conf, $DB, $func;

    $text = "<option value='' >" . $default . "</option>";
    $sql = "SELECT * FROM iso_cities where display=1 AND country= '".$country."'  order by c_order ASC , name ASC  ";
    $result = $DB->query($sql);
    while ($row = $DB->fetch_row($result)) {
      $selected = ($did == $row['id']) ? " selected" : '';
      $text .= "<option value='".$row['id']."'  ".$selected." >" . $func->HTML($row['name']) . "</option>";
    }
    return $text;
  }


  /*-------------- List_Option_State --------------------*/
  function List_Option_State ($city, $did = "", $default = "" )
  {
    global $vnT, $conf, $DB, $func;

    $text = "<option value='' >" . $default . "</option>";
    $sql = "SELECT * FROM iso_states where display=1 and city=".$city."  order by s_order ASC , name ASC  ";
    $result = $DB->query($sql);
    while ($row = $DB->fetch_row($result)) {
      $selected = ($did == $row['id']) ? " selected" : '';
      $text .= "<option value='".$row['id']."'  ".$selected." >" . $func->HTML($row['name']) . "</option>";
    }
    return $text;
  }


  /*-------------- list_status_order --------------------*/
  function list_status_order ($did, $lang = "vn", $ext = "")
  {
    global $func, $DB, $conf, $vnT;
    $text = "<select size=1 name=\"status\" id=\"status\" {$ext} >";
    $text .= "<option value=\"0\">" . $vnT->lang['select_status'] . "</option>";
    if(count($vnT->setting['status'])){
      foreach ($vnT->setting['status'] as $key => $val) {
        $selected  = ($key==$did) ? " selected" : '';
        $text .= "<option value='".$key."'  ".$selected.">" . $val . "</option>";
      }
    }
    $text .= "</select>";
    return $text;
  }

  /*-------------- get_method_name --------------------*/
  function get_method_name ($table, $name, $lang = "en")
  {
    global $DB, $input, $func, $vnT, $conf;
    $text = "";
    $reuslt = $DB->query("select title from {$table} where name='{$name}' ");
    if ($row = $DB->fetch_row($result)) {
      $text = $func->fetch_content($row['title'], $lang);
    }
    return $text;
  }


  /*-------------- List_State_Muti --------------------*/
  function List_State_Muti($city="2",$did="", $ext=""){
    global $func,$DB,$conf;
    if ($did)
      $arr_selected = explode(",",$did);
    else{
      $arr_selected = array();
    }

    $sql="SELECT * FROM iso_states where display=1 and city=".$city."  order by s_order  ASC, name ASC  ";
    $result = $DB->query ($sql);

    $text='<div id="StateSelect" class="ISSelect">';
    $text.='<ul>';
    $i=0;$ngang=0;
    while ($row = $DB->fetch_row($result))
    {
      $i++;
      if (in_array($row['id'],$arr_selected)){
        $checked ="checked";
        $class= "class='select'";
      }else{
        $checked ="";
        $class ="";
      }

      $name = $func->HTML($row['name']);
      $text.='<li '.$class.' ><input name="ckState[]" id="ckState" type="checkbox" value="'.$row['id'].'" '.$checked.'  />&nbsp;'.$name.'</li>';

    }

    $text.='</ul></div>';

    return $text;
  }

  /*-------------- List_Search --------------------*/
  function List_Search ($did, $ext = "")
  {
    global $func, $DB, $conf, $vnT;
    $arr_where = array('order_code'=> 'Mã đơn hàng','tracking_no'=> 'Mã Vận Chuyển','mem_id'=> 'ID thành viên','d_name'=>  "Họ tên khách hàng",'d_email'=> 'Email', 'd_phone' => 'Điện thoại', 'd_address' => 'Địa chỉ', 'date_order' => "Ngày đặt hàng (d/m/Y)" );

    $text= "<select size=1 name=\"search\" id='search' class='select' {$ext} >";
    foreach ($arr_where as $key => $value)
    {
      $selected = ($did==$key) ? "selected"	: "";
      $text.="<option value=\"{$key}\" {$selected} > {$value} </option>";
    }
    $text.="</select>";

    return $text;
  }







  /*-------------- do_ProcessUpdateCart --------------------*/
  function do_ProcessUpdateCart ($id)
  {
    global $func,$DB,$conf,$vnT;
    $out = array();
    $out['ok'] = 0;

    $total_cart =   $vnT->input['total_cart'];
    $s_price =  $vnT->input['s_price'];
    $promotion_price =  $vnT->input['promotion_price'];
    $discount =  $vnT->input['discount'];


    $ok_update = 1;

    $total_price = 0;

    if ($ok_update) {

      $total_price = $total_cart ;
      $total_price += $s_price ;

      if($promotion_price){
        $total_price -= $promotion_price ;
      }
      if($discount){
        $total_price -= $discount ;
      }


      $cot['s_price'] = $s_price;
      $cot['discount'] = $discount;
      $cot['total_price'] = $total_price;

      $ok = $DB->do_update("order_sum", $cot, "order_id=" . $id);
      if ($ok) {
        $mess = "Cập nhật giỏ thàng thành công";
        $out['ok'] = 1 ;
      }
    } else {
      $mess = "Giỏ hàng phải có ít nhất 1 sản phẩm";
    }
    $out['mess'] = $mess ;
    return $out;
  }


  /*-------------- do_ProcessSendMailOrder --------------------*/
  function do_ProcessSendMailOrder ($id)
  {
    global $func,$DB,$conf,$vnT;
    $out = array();

    $res_order = $DB->query("SELECT * FROM order_sum	WHERE  order_id=".$id );
    if($row_order = $DB->fetch_row($res_order))
    {
      //send Email
      $total_price = $this->get_price_pro ($row_order['total_price']) ;
      $status_name = $vnT->setting['status'][$row_order['status']] ;
      $date_order = @date("d/m/Y",$row_order['date_order']);


      $link_view = $conf['rooturl']."member/your_order.html/?do=detail&id=".$row_order['order_id'] ;
      $subject = "Thông báo đơn hàng #". $row_order['order_code']." được thay đổi";
      $content_email = $func->load_MailTemp("email_updateOrder");
      $qu_find = array(
        '{full_name}',
        '{order_code}',
        '{status_name}',
        '{total_price}',
        '{date_order}',
        '{note}',
        '{link_view}'
      );

      $qu_replace = array(
        $row_order['d_name'],
        $row_order['order_code'],
        $status_name,
        $total_price,
        $date_order,
        $row_order['note'],
        $link_view
      );


      $message = str_replace($qu_find, $qu_replace, $content_email);


      $send = $vnT->func->doSendMail($row_order['d_email'],$subject,$message,$vnT->conf['email']);


      if($row_order['mem_id']>0){
        $cot_m['mail_from'] = 0;
        $cot_m['from_name'] = "Admin";
        $cot_m['mail_to'] = $row_order['mem_id'];
        $cot_m['to_name'] =  $row_order['d_name'];
        $cot_m['mail_subject'] = $subject;
        $cot_m['mail_content'] = $message;
        $cot_m['mail_date'] = time();
        $DB->do_insert("mailbox",$cot_m);
      }

    }

    $out['mess'] = $mess ;
    return $out;
  }





//============ Email ==========
  function email_ShowCart($order_id, $lang){
    global $vnT,$func,$conf,$DB;
    $html_row_cart = "";
    $sql_cart = "select * from order_detail where order_id = '{$order_id}'";
    $result_cart = $DB->query($sql_cart);
    $x = 0;
    while ($row_cart = $DB->fetch_row($result_cart)) {
      $p_id = $row_cart["p_id"];
      $x++;
      $row_cart['stt'] = $x;

      $link = $vnT->conf["rooturl"].$row_cart['item_link'] ;

      $src_pic = ($row_cart["item_picture"]) ? $vnT->func->get_src_modules ($row_cart["item_picture"], 100, 0,0) : $conf['rooturl']."modules/product/images/nophoto.gif" ;

      $pic = "<a href='{$link}'><img src='".$src_pic."' alt='' /></a>";
      $row_cart['pic'] = $pic;
      $p_name = '<div style="color: #F90000;font-weight: bold;text-transform: uppercase;">'.$row_cart['text_save'].'</div>';
      $p_name .= "<a href='" . $link . "'><strong class='p_name'>" . $row_cart['item_title'] . "</strong></a>";
      $row_cart['p_name'] = '<table width="100%" border="0" cellspacing="0" cellpadding="0">
									<tr>
										<td>' . $p_name . '<br>#' . $row_cart["item_maso"] . '</td>
									</tr>
								</table>';

      $total = ($row_cart['price'] * $row_cart['quantity']);
      $price = $row_cart['price'];
      $row_cart['price'] = $this->get_price_pro($price, 0);
      $row_cart['total'] = $this->get_price_pro($total, 0);

      $html_row_cart .= $this->email_row_cart($row_cart);
    }

    return $this->email_cart($html_row_cart);
  }

  function email_cart($row_cart){
    global $input,$conf,$vnT ;
    return<<<EOF
<table width="100%" border="0"  cellpadding="5" cellspacing="1" bgcolor="#cccccc">
	<tr bgcolor="#ffffff" >
		<td width="5%" align="center"><b>{$vnT->lang['stt']}</b> </td>
		<td width="10%" align="center"><b>{$vnT->lang['picture']}</b> </td>
		<td class="row_title_cart" align="center"><b>{$vnT->lang['p_name']}</b> </td> 
		<td width="10%" align="center"><b>{$vnT->lang['price']}</b></td>
		<td width="10%" align="center"><b>{$vnT->lang['quantity']}</b> </td>
		<td width="10%" align="center"><b>{$vnT->lang['total']}</b> </td>
	</tr> 
	{$row_cart}
</table>
EOF;
  }

  function email_row_cart($data){
    global $vnT,$conf;
    return<<<EOF
<tr bgcolor="#ffffff">
	<td align="center" >{$data['stt']}</td>
	<td align="center">{$data['pic']}</td>
	<td align="left">{$data['p_name']}</td> 
	<td align="right" >{$data['price']}</td>
	<td align="center" >{$data['quantity']}</td>
	<td align="right" >{$data['total']}</td>
</tr>
EOF;
  }
  function do_Send_Email_Order ($order_id, $lang="vn"){
    global $vnT, $func, $DB, $conf;
    $result = $DB->query("SELECT * FROM order_sum where order_id=".$order_id);
    if($data = $DB->fetch_row($result)) {
      //send email
      $arr_c_address["address"] = $data["c_address"];
      $arr_c_address["ward"] = $data["c_ward"];
      $arr_c_address["state"] = $data["c_state"];
      $arr_c_address["city"] = $data["c_city"];
      // $data["c_full_address"] = $this->get_full_address($arr_c_address);
      $email = $data['d_email'];
      $data['basket_total'] = $this->get_price_pro($data['total_cart']);
      $data['discount_price'] = $this->get_price_pro($data['discount']);
      $data['shipping_price'] = $this->get_price_pro($data['s_price']);
      $data['total_price'] = $this->get_price_pro($data['total_price']);
      $content_email = '';
      $title_email = '' ;

      $result = $DB->query("SELECT title,content FROM mail_template
                            WHERE name ='OrderAdmintoGuest' AND lang='".$lang."' ");
      if ($row = $DB->fetch_row($result)){
        $content_email = $row['content'];
        $title_email = $row['title'];
      }
      $qu_find = array(
        '{domain}',
        '{order_order}',
        '{date_order}',
        '{time_order}',
        '{order_status}',
        '{order_note}',
        '{c_full_address}',
        '{c_phone}',
        '{d_email}',
        '{shipping_method}',
        '{ship_date}',
        '{table_cart}',
        '{payment_method}',
        '{basket_total}',
        '{discount_price}',
        '{shipping_price}',
        '{total_price}'
      );
      $qu_replace = array(
        strtoupper($_SERVER['HTTP_HOST']),
        $data["order_code"] ,
        @date("d/m/Y",$data["date_order"]),
        @date("h:ia",$data["date_order"]),
        $vnT->setting['status'][$data['status']],
        $vnT->func->HTML($data['note']),
        $data["c_full_address"],
        $data["c_phone"],
        $data["d_email"],
        $data["shipping_name"],
        @date("d/m/Y",$data["ship_date"]),
        $this->email_ShowCart($order_id, $lang),
        $data['payment_name'],
        $data['basket_total'],
        "-".$data['discount_price'],
        $data['shipping_price'],
        $data['total_price'],
      );
      $content_email = str_replace($qu_find, $qu_replace,$content_email);
      $subject = str_replace("{host}",$_SERVER['HTTP_HOST'],$title_email);
      //--- gui den khach hang
      $vnT->func->doSendMail($email,$subject,$content_email,$vnT->conf['email']);
    }
    return true;
  }
  function do_update_onhand($status = 1,$order_id){
    global $vnT,$DB,$conf;
    $result = $DB->query("SELECT * FROM order_detail WHERE order_id = {$order_id}");
    if($num = $DB->num_rows($result)){
      while ($row = $DB->fetch_row($result)){
        $item_id = (int) $row['item_id'];
        $subid = (int) $row['subid'];
        $onhand = (int) $row['quantity'];
        if($status == 2){
          if($subid){
            $DB->query("UPDATE products_sub SET onhand = onhand + $onhand 
                        WHERE parentid = {$item_id} AND subid = {$subid}");
          }else{
            $DB->query("UPDATE products SET onhand = onhand + $onhand WHERE p_id = {$item_id}");
          }
        }else{
          if($subid){
            $DB->query("UPDATE products_sub SET onhand = onhand - $onhand 
                        WHERE parentid = {$item_id} AND subid = {$subid}");
          }else{
            $DB->query("UPDATE products SET onhand = onhand - $onhand WHERE p_id = {$item_id}");
          }
        }
      }
    }
    $onhand_status = ($status == 1) ? 2 : 1 ;
    $DB->query("UPDATE order_sum SET onhand_status = $onhand_status WHERE order_id = {$order_id}");
  }
}

$model = new Model();
?>