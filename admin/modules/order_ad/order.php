<?php
/*================================================================================*\
|| 							Name code : order.php 		 		            	  ||
||  				Copyright @2008 by Thai Son - CMS vnTRUST                     ||
\*================================================================================*/
/**
 * @version : 2.0
 * @date upgrade : 09/01/2009 by Thai Son
 **/
if (! defined('IN_vnT')) {
  die('Hacking attempt!');
}
//load Model
include(dirname(__FILE__) . "/functions.php");

class vntModule extends Model
{
  var $output = "";
  var $skin = "";
  var $linkUrl = "";
  var $module = "order";
  var $action = "order";

  function vntModule (){
    global $Template, $vnT, $func, $DB, $conf;
    $this->skin = new XiTemplate(DIR_MODULE.DS.$this->module . "_ad" . DS . "html" . DS . $this->action . ".tpl");
    $this->skin->assign('LANG', $vnT->lang);
		$this->skin->assign('DIR_MOD', "modules/" .$this->module . "_ad");
		$this->skin->assign('DIR_IMAGE', $vnT->dir_images);
    $lang = ($vnT->input['lang']) ? $lang = $vnT->input['lang'] : $func->get_lang_default();
    $this->linkUrl = "?mod=" . $this->module . "&act=" . $this->action . "&lang=" . $lang;
    $vnT->html->addStyleSheet("modules/" . $this->module . "_ad/css/" . $this->module . ".css");
		$vnT->html->addScript("modules/" . $this->module . "_ad/js/" . $this->module . ".js"); 
		$vnT->html->addStyleSheet($vnT->dir_js."/jquery_alerts/jquery.alerts.css");
		$vnT->html->addScript($vnT->dir_js."/jquery_alerts/jquery.alerts.js"); 
    switch ($vnT->input['sub']) {
      case 'edit':
        $nd['f_title'] = $vnT->lang['task_order'];
        $nd['content'] = $this->do_Edit($lang);
        break;
      case 'del':
        $this->do_Del($lang);
        break;
			case 'print_order':
				flush();
        echo $this->do_Print_Order($lang);
        exit();
        break;
      default:
        $nd['f_title'] = $vnT->lang['manage_order'];
        $nd['content'] = $this->do_Manage($lang);
        break;
    }
    $nd['menu'] = $func->getToolbar_Small($this->module, $this->action, $lang);
    $nd['row_lang'] = $func->html_lang("?mod=" . $this->module . "&act=" . $this->action, $lang);
    $Template->assign("data", $nd);
    $Template->parse("box_main");
    $vnT->output .= $Template->text("box_main");
  }
  function do_Edit ($lang){
    global $vnT, $func, $DB, $conf;
		$vnT->html->addStyleSheet( $vnT->dir_js . "/jquery_ui/themes/base/ui.all.css");
		$vnT->html->addScript($vnT->dir_js . "/jquery_ui/ui.core.js");		
		$vnT->html->addScript($vnT->dir_js . "/jquery_ui/ui.datepicker.js");  
    $vnT->html->addScriptDeclaration("
	 		$(function() {
				$('#ship_date').datepicker({
					changeMonth: true,
					changeYear: true
				});
			});
		");
    $status_complete = $vnT->setting['status_complete'];
    $status_confirm = $vnT->setting['status_confirm'];
    $status_cancel = $vnT->setting['status_cancel'];
    $status_customer = $vnT->setting['status_customer'];
    $data['date_ship'] = date("d/m/Y");
    $msg = "";
    $id = $vnT->input['id'];
    $data['date_ship'] = date("d/m/Y");
    if ($vnT->input['btnUpdate']) {
      $res_up = $this->do_ProcessUpdateCart($id) ;
      if($res_up['ok']){
        $link_ref =  $this->linkUrl . "&sub=edit&id=".$id;
        $func->html_redirect($link_ref, $res_up['mess']);
      }else{
        $err = $vnT->func->html_err($res_up['mess']);
      }
    }
    if ($vnT->input['btnSave']) {
      $data = $_POST;
      if (empty($err)) {
        $status = $vnT->input["status"];
        $note = $DB->mySQLSafe($_POST['note']);
        $ship_date = time();
        $dataup["ship_date"] = $ship_date;
        $dataup["status"] = $status;
        $dataup["note"] = $note;
        $ok = $DB->do_update("order_sum", $dataup, "order_id='{$id}'");
        if ($ok) {
          //insert history
          $cot_h['order_id'] = $id;
          $cot_h['status'] = $status;
          $cot_h['status_old'] = $vnT->input['status_old'];
          $cot_h['note'] = $note;
          $cot_h['adminid'] = $vnT->admininfo['adminid'] ;
          $cot_h['admin_name'] = $vnT->admininfo['username'] ;
          $cot_h['date_submit'] = time();
          $DB->do_insert("order_history",$cot_h);
          if ($status == $status_confirm && $vnT->input['onhand_status'] == 1) {
            $this->do_update_onhand(1,$id);
          }
          if (($status == $status_cancel && $vnT->input['onhand_status'] == 2) || ($status == $status_customer && $vnT->input['onhand_status'] == 2)) {
            $this->do_update_onhand(2,$id);
          }
          if ($status == $status_complete ) {
            if( $vnT->input['onhand_status'] == 1){
              $this->do_update_onhand(1,$id);
            }
            //update ton kho
          }
          //gui email
          if($vnT->input['is_send_email']){
            $res_send = $this->do_Send_Email_Order ($id,$lang);
          }
          //xoa cache
          $func->clear_cache();
          //insert adminlog
          $func->insertlog("Edit", $_GET['act'], $id);
          $err = $vnT->lang["edit_success"];
          $url = $this->linkUrl . "&sub=edit&id=$id";
          $func->html_redirect($url, $err);
        } else
          $err = $func->html_err($vnT->lang["edit_failt"] . $DB->debug());
      }
    }
    $sql = "SELECT * FROM order_sum WHERE order_id ='{$id}' ";
    $result = $DB->query($sql);
    if ($data = $DB->fetch_row($result)) {
			// chi tiet gio hang
      $html_row_cart = "";
      $sql_cart = "SELECT * FROM order_detail WHERE order_id = '{$id}'";
      $result_cart = $DB->query($sql_cart);
      $x = 0; $w =65 ;
      while ($row_cart = $DB->fetch_row($result_cart)){
				$row_cart['stt'] = ($x+1);
        $quantity = $row_cart['quantity'];
				$item_type = $row_cart["item_type"];
				$item_id = $row_cart["item_id"];
				$link = $row_cart["item_link"];
				$src_pic = ($row_cart["item_picture"]) ? $vnT->func->get_src_modules ($row_cart["item_picture"], $w, 0,0) : ROOT_URL."modules/product/images/nophoto.gif";
        $item_title =	"<a href='".$link."' target='_blank' ><strong >".$row_cart['item_title']."</strong></a>" ;
        $item_title .= '<div style="padding-top: 2px;">MS: <strong>'.$row_cart['item_maso'].'</strong></div>';
        $total = ($row_cart['price'] * $row_cart['quantity']);
			  //show
				$row_cart['pic'] = "<a href='{$link}' target='_blank'><img src=\"{$src_pic}\" width=\"{$w}\" alt='".$row_cart['item_title']."' /></a>";
				$row_cart['item_title'] = $item_title ;
        $row_cart['price'] = $this->get_price_pro($row_cart['price'], 0);
        $row_cart['total'] = $this->get_price_pro($total, 0);
        $this->skin->assign('row', $row_cart);
        $this->skin->parse("edit.row_cart");
        $x ++;
      }
      $data['other_price'] ='';
      if ($data['promotion_price']) {
        $data['other_price'] .= '<tr >
               <td class="row_total_cart"  colspan="5"  align="right"><b>Dùng khuyến mãi <span class="font_err">('.$data['promotion_code'].')</span> : </b> </td>
               <td  class="row_total_cart"   align="right" style="padding-right:5px" ><strong>-' . $this->get_price_pro($data['promotion_price'], "0") . '</strong><input type="hidden" name="promotion_price" value="'.$data['promotion_price'].'"/> </td>
							</tr>';
      }
      if ($vnT->setting['status_default'] == $data['status']) {
        //$data['s_price'] = '<input type="text" name="s_price" id="s_price" value="' . $data['s_price'] . '" size="10" style="text-align:right;width:100px" onkeypress="return is_num(event,\'s_price\')" /> đ';
        //$data['discount'] = '<input type="text" name="discount" id="discount" value="' . $data['discount'] . '" size="10" style="text-align:right;width:100px" onkeypress="return is_num(event,\'discount\')" /> đ';
       //  $data['other_price'] .= '<tr>
       //         <td class="row_total_cart" colspan="5" align="right"><b>Giảm giá thêm: </b> </td>
       //         <td class="row_total_cart" align="right" style="padding-right:5px"><strong>-'.$data['discount'].'</strong></td>
							// </tr>';
        $data['btn_update_cart'] = '<div align="center" style="padding: 5px;">
                                  <input type="hidden" name="mem_id" value="'.$data['mem_id'].'"/>
                                  <input type="hidden" name="mem_point" value="'.$data['mem_point_earned'].'"/>
                                  <input type="submit" name="btnUpdate" id="btnUpdate" value=" Cập nhật giỏ hàng "  class="button" style="height: 40px; line-height: 30px; font-size: 18px;">
                                  </div>';
        $data['btn_update_cart'] .= '<p align="center"><span class="font_err"><b>Lưu ý :</b> Sau khi thay đổi thông tin nhớ click <b>"Cập nhật giỏ hàng"</b> để lưu lại giỏ hàng và tính toán lại tổng tiền</span></p>';
      } else {
        $data['s_price'] = $this->get_price_pro($data['s_price']);
        $discount = $data['promotion_price'] + $data['ctv_price'] ;
      }
      if ($data['promotion_code'] != "") {
        $data['info_promotion'] = '<p style="padding-top: 10px"><b>Dùng mã khuyến mãi .</b> CODE : <b>' . $data['promotion_code'].'</b></p>';
      }
      if($data['ctv_code']) {
        $data['info_ctv'] = '<p style="padding-top: 10px"><b>Dùng Mã CTV .</b> CODE : <b>'.$data['ctv_code'].'</b></p>';
      }
      $data['eligible_for_sale'] = str_replace("{point}", "<b class=font_err>" . $data['mem_point_earned'] . "</b>", $vnT->lang['mess_eligible_for_sale']);
      $info_customer = $this->get_info_address($data);
      // nguoi nhan
      $info_receiver = $this->get_info_address($data, "shipping");
      $data['date_order'] = date("H:i, d/m/Y", $data['date_order']);
      if ($data['ship_date'] == "")
        $data['text_ship_date'] = $vnT->lang['watting_shipping'];
      else
        $data['text_ship_date'] = date("H:i, d/m/Y", $data['ship_date']);
      $data['info_customer'] = $info_customer;
      $data['info_receiver'] = $info_receiver;
      $data['shipping_name'] = $this->get_method_name("shipping_method", $data['shipping_method'], $data['lang']);
      $data['payment_name'] = $this->get_method_name("payment_method", $data['payment_method'], $data['lang']);
      $data['comment'] = $vnT->func->HTML($data['comment']);
      $info_bill ='';
      if($data['bill']){
        $info_bill .= '<p style="padding-top: 10px"><b>Xuất hóa đơn công ty :</b>';
        $info_bill .= '<br>Tên công ty : '.$data['bill_company'];
        $info_bill .= '<br>Địa chỉ : '.$data['bill_address'];
        $info_bill .= '<br>MST : '.$data['bill_mst'];
        $info_bill .= '</p>';
      }
      $data['info_bill'] = $info_bill ;
      $data['text_total_cart'] = $this->get_price_pro($data['total_cart'], '0 đ');
      $data['text_total_price'] = $this->get_price_pro($data['total_price'], '0 đ');
    }
    //history
    $res_h = $DB->query("SELECT * FROM order_history WHERE order_id=".$id." ORDER BY date_submit DESC ");
    if($num_h= $DB->num_rows($res_h)){
      while ($row_h= $DB->fetch_row($res_h)){
        $row_h['date_submit'] = @date("H:i, d/m/Y",$row_h['date_submit']);
        $row_h['text_status'] = $vnT->setting['status'][$row_h['status']];
        $this->skin->assign('row', $row_h);
        $this->skin->parse("edit.html_history");
      }
    }else{
      $this->skin->assign('row', $row_h);
      $this->skin->parse("edit.html_history_no");
    }
    $data['order_id'] = $id;
    $data['list_status'] = $this->list_status_order($data['status'],$lang);
		$data['text_status'] = $vnT->setting['status'][$data['status']];	
    if ($data['ship_date'])
      $data['ship_date'] = date("d/m/Y", $data['ship_date']);
		$data['link_print'] = $this->linkUrl ."&sub=print_order&id=" . $id;
    if ($data['status'] == $status_complete ) {
      $data['text_status'] = $vnT->setting['status'][$data['status']];
      $this->skin->assign('data', $data);
      $this->skin->parse("edit.html_form_finished");
    } else {
      $data['html_note'] = $vnT->editor->doDisplay('note', $data['note'], '100%', '200', "Normal");
      $this->skin->assign('data', $data);
      $this->skin->parse("edit.html_form_update");
    }
    $data['err'] = $err;
    $data['link_action'] = $this->linkUrl . "&sub=edit&id=$id";
    $this->skin->assign('data', $data);
    $this->skin->parse("edit");
    return $this->skin->text("edit");
  }
  function do_Del ($lang){
    global $func, $DB, $conf, $vnT;
    $id = (int) $vnT->input['id'];
    $ext = $vnT->input["ext"];
    $del = 0;
    $qr = "";
    $status_complete = $vnT->setting['status_complete'] ;
    $status_customer = $vnT->setting['status_customer'];
    if ($id != 0) {
      $ids = $id;
    }
    if (isset($vnT->input["del_id"])) {
      $ids = implode(',', $vnT->input["del_id"]);
    }
    $query = 'DELETE FROM order_sum WHERE order_id IN (' . $ids . ') AND status<>'.$status_complete;
    if ($ok = $DB->query($query)) {
      $DB->query("DELETE FROM order_detail WHERE order_id IN (" . $ids . ")");
      $mess = $vnT->lang["del_success"];
    } else
      $mess = $vnT->lang["del_failt"];
    $ext_page = str_replace("|", "&", $ext);
    $url = $this->linkUrl . "&{$ext_page}";
    $func->html_redirect($url, $mess);
  }
  function render_row ($row_info, $lang){
    global $func, $DB, $conf, $vnT;
    $row = $row_info;
    // Xu ly tung ROW  
    $id = $row['order_id'];
    $row_id = "row_" . $id;
    $output['row_id'] = $row_id; 
    if ($row['status'] == $vnT->setting['status_complete']) {
      $output['check_box'] = "";
    } else {
      $output['check_box'] = vnT_HTML::checkbox("del_id[]", $id, 0, " ");
    }
    $link_edit = $this->linkUrl . "&sub=edit&id=" . $id . '&ext=' . $row['ext_page'];
    $link_del = "javascript:del_item('" . $this->linkUrl . "&sub=del&id=" . $id . "&ext=" . $row['ext_page'] . "')";
    $link_send_email = $this->linkUrl . "&sub=send_email&id=" . $id;
    $output['order_code'] = "<a href=\"{$link_edit}\"><strong>" . $row['order_code'] . "</strong></a>";
    $type_mem = ($row['mem_id'] == 0) ? "Guest" : "Mem ID: ".$row['mem_id'];
    // nguoi mua
    $info_customer = $this->get_info_address($row,"payment",1) ;
    $info_receiver = $this->get_info_address($row,"shipping",1) ;
    $output['info_customer'] = $info_customer;
    $output['info_receiver'] = $info_receiver;
    $output['date_order'] = date("H:i, d/m/Y", $row['date_order']);
    $output['date_order'] .=  "<div style='padding:5px 0px;'>PTTT: <b>".$row['payment_method']."</b></div>";
    //$output['date_order'] .=  "<div style='padding:5px 0px;'>PTVC: <b>".$row['shipping_method']."</b></div>";
    if ($row['total_price']) {
      $total_price = $this->get_price_pro($row['total_price'], $row['lang']);
    } else {
      $total_price = "<div align=center>Call</div>";
    }
    $output['total_price'] = $total_price;
    $output['status'] = $vnT->setting['status'][$row['status']];
		$output['messages'] = "<a title=\"Xem tin nhắn đơn hàng ".$row['order_code']."\" class=\"thickbox\"  href='".$this->linkUrl."&sub=message&orderId=" . $id ."&TB_iframe=true&width=850&height=400'  >".(int)$row['nummessages']." Tin nhắn</a>";
		$output['messages'] .= ((int)$row['newmessages']) ? "<br><b class='font_err'>(".(int)$row['newmessages']." New)</b>" : "<br>(".(int)$row['newmessages']." New)";															
    if ($row['status'] == $vnT->setting['status_complete'] ) {
      $output['status'] = $vnT->setting['status'][$row['status']] ;
      $delete = "";
    } else {
      $delete = "<a href=\"{$link_del}\"><img src=\"" . $vnT->dir_images . "/delete.gif\"  alt=\"Delete \"></a>";
    }
    $output['action'] = "
			<input name=\"h_id[]\" type=\"hidden\" value=\"{$id}\" />
			<a href=\"{$link_edit}\"><img src=\"" . $vnT->dir_images . "/but_view.gif\" width=22 alt=\"View \"></a>&nbsp;	 
			{$delete}";
    return $output;
  }
  function do_Manage ($lang){
    global $vnT, $func, $DB, $conf;
		$vnT->html->addStyleSheet( $vnT->dir_js . "/jquery_ui/themes/base/ui.all.css");
		$vnT->html->addScript($vnT->dir_js . "/jquery_ui/ui.core.js");		
		$vnT->html->addScript($vnT->dir_js . "/jquery_ui/ui.datepicker.js");  
    $vnT->html->addScriptDeclaration("
	 		$(function() {
				$('.dates').datepicker({
					showOn: 'button',
					buttonImage: '".$vnT->dir_images."/calendar.gif',
					buttonImageOnly: true,
					changeMonth: true,
					changeYear: true
				})
			});
		"); 
    //update
    if ($vnT->input["do_action"]) {
      //xoa cache
      $func->clear_cache();
      if ($vnT->input["del_id"])
        $h_id = $vnT->input["del_id"];
      switch ($vnT->input["do_action"]) {
        case "do_edit":
          if (isset($vnT->input["txt_Order"]))
            $arr_order = $vnT->input["txt_Order"];
          $mess .= "- " . $vnT->lang['edit_success'] . " ID: <strong>";
          $str_mess = "";
          for ($i = 0; $i < count($h_id); $i ++) {
            $dup['order'] = $arr_order[$h_id[$i]];
            $ok = $DB->do_update("modname", $dup, "id=" . $h_id[$i]);
            if ($ok) {
              $str_mess .= $h_id[$i] . ", ";
            }
          }
          $mess .= substr($str_mess, 0, - 2) . "</strong><br>";
          $err = $func->html_mess($mess);
        break;
        case "do_hidden":
          $mess .= "- " . $vnT->lang['hidden_success'] . " ID: <strong>";
          for ($i = 0; $i < count($h_id); $i ++) {
            $dup['display'] = 0;
            $ok = $DB->do_update("modname", $dup, "id=" . $h_id[$i]);
            if ($ok) {
              $str_mess .= $h_id[$i] . ", ";
            }
          }
          $mess .= substr($str_mess, 0, - 2) . "</strong><br>";
          $err = $func->html_mess($mess);
        break;
        case "do_display":
          $mess .= "- " . $vnT->lang['display_success'] . "  ID: <strong>";
          for ($i = 0; $i < count($h_id); $i ++) {
            $dup['display'] = 1;
            $ok = $DB->do_update("modname", $dup, "id=" . $h_id[$i]);
            if ($ok) {
              $str_mess .= $h_id[$i] . ", ";
            }
          }
          $mess .= substr($str_mess, 0, - 2) . "</strong><br>";
          $err = $func->html_mess($mess);
        break;
      }
    }
    $p = ((int) $vnT->input['p']) ? $p = $vnT->input['p'] : 1;
    $n = ($conf['record']) ? $conf['record'] : 30;
    $keyword = ($vnT->input['keyword']) ? trim($vnT->input['keyword']) : "";
    $status = ($vnT->input['status']) ? $vnT->input['status'] : 0;
    $payment_method = (isset($vnT->input['payment_method'])) ? $vnT->input['payment_method'] : "";
    $city = (isset($vnT->input['city'])) ? $vnT->input['city'] : "";
    $state = (isset($vnT->input['state'])) ? $vnT->input['state'] : "";
    $type_mem = (isset($vnT->input['type_mem'])) ? $vnT->input['type_mem'] : "-1"; 
		$date_begin = ($vnT->input['date_begin']) ?  $vnT->input['date_begin'] : "";
		$date_end = ($vnT->input['date_end']) ?  $vnT->input['date_end'] : "";
    $search = ($vnT->input['search']) ? $vnT->input['search'] : "order_code";
		
    $where = "";
    $ext = '';
    $ext_page = '';
    if ($type_mem != "-1") {
      if ($type_mem == 0) {
        $where .= " and mem_id=0 ";
      } else {
        $where .= " and mem_id<>0 ";
      }
      $ext .= "&type_mem={$type_mem}";
    }
		if($date_begin || $date_end ){
			$tmp1 = @explode("/", $date_begin);
			$time_begin = @mktime(0, 0, 0, $tmp1[1], $tmp1[0], $tmp1[2]);
			
			$tmp2 = @explode("/", $date_end);
			$time_end = @mktime(23, 59, 59, $tmp2[1], $tmp2[0], $tmp2[2]);
			
			$where.=" AND (date_order BETWEEN {$time_begin} AND {$time_end} ) ";
			$ext.="&date_begin=".$date_begin."&date_end=".$date_end;
			$ext_page.= "date_begin=".$date_begin."|date_end=".$date_end."|";
		}
    if ($keyword) {
			switch ($search){
				case "mem_id" : $where .= " AND mem_id =$keyword "; break ;
				case "date_order" : $where.=" AND DATE_FORMAT(FROM_UNIXTIME(date_order),'%d/%m/%Y') = '{$keyword}' "; break ;
				default :	$where .= " AND $search like '%$keyword%' "; break ;
			}
      $ext_page .= "search=$search|keyword=$keyword|";
      $ext .= "&search={$search}&keyword={$keyword}";
    }
    if ($status) {
      $where .= " and status=" . $status;
      $ext .= "&status=$status";
    }
    if($payment_method) {
      $where .= " and payment_method='" . $payment_method."'";
      $ext .= "&payment_method=".$payment_method;
    }
    if($city) {
      $where .= " and c_city='" . $city."'";
      $ext .= "&city=".$city;
    }
    if($state) {
      $where .= " and c_state='" . $state."'";
      $ext .= "&state=".$state;
    }
    $query = $DB->query("SELECT order_id FROM order_sum 	WHERE order_id>0  $where  ");
    $totals = intval($DB->num_rows($query));
    $num_pages = ceil($totals / $n);
    if ($p > $num_pages)
      $p = $num_pages;
    if ($p < 1)
      $p = 1;
    $start = ($p - 1) * $n;
    $nav = $func->paginate($totals, $n, $ext, $p);
    $table['link_action'] = $this->linkUrl . "&p=$p" . $ext;
    $table['title'] = array(
      'check_box' => "<input type=\"checkbox\" name=\"checkall\" id=\"checkall\" class=\"checkbox\" />|5%|center" , 
      'order_code' => "Order ID |13%|center" ,
      'info_customer' => $vnT->lang['customer'] . "||left" ,
      'date_order' => $vnT->lang['date_order'] . " |13%|center" , 
      'total_price' => $vnT->lang['total_price'] . "|10%|center" , 
      'status' => $vnT->lang['status'] . "|12%|center" ,
      'action' => "Action|8%|center");
    $sql = "SELECT * FROM order_sum 
						WHERE order_id>0 $where 
						ORDER BY  date_order DESC , order_id DESC LIMIT $start,$n";
    $result = $DB->query($sql);
    if ($DB->num_rows($result)) {
      $row = $DB->get_array($result);
      for ($i = 0; $i < count($row); $i ++) {
        $row_info = $this->render_row($row[$i], $lang);
        $row_field[$i] = $row_info;
        $row_field[$i]['stt'] = ($i + 1);
        $row_field[$i]['row_id'] = "row_" . $row[$i]['order_id'];
        $row_field[$i]['ext'] = "";
      }
      $table['row'] = $row_field;
    } else {
      $table['row'] = array();
      $table['extra'] = "<div align=center class=font_err>".$vnT->lang['no_have_order']."</div>";
    }
    $table['button'] = '<input type="button" name="btnDel" value=" '.$vnT->lang['delete'].' " class="button" onclick="del_selected(\'' . $this->linkUrl . '&sub=del&ext=' . $ext_page . '\')">';
    $table_list = $func->ShowTable($table);
    $data['table_list'] = $table_list;
    $data['totals'] = $totals;
    $data['keyword'] = $keyword;
    $data['list_status'] = $this->list_status_order($status, $lang );
    $data['list_city'] = $this->List_Option_City("VN",$city,"Tỉnh/Thành" );
    $data['list_state'] = $this->List_Option_State($city,$state,"Quận/Huyện");
    $data['list_type'] = vnT_HTML::selectbox("type_mem", array(  '1' => 'Thành viên (Member)' ,  '0' => 'Khách vãng lai (Guest)'), $type_mem );
    $data['list_search'] = $this->List_Search($search);
		$data['date_begin'] = $date_begin;
		$data['date_end'] = $date_end;
    $data['link_fsearch'] = $this->linkUrl;
    $data['err'] = $err;
    $data['nav'] = $nav;
		$data['link_excel'] = $conf['rooturl']."admin/modules/order_ad/excel_order.php?lang=$lang".$ext ;
    $this->skin->assign('data', $data);
    $this->skin->parse("manage");
    return $this->skin->text("manage");
  }
  function do_Print_Order ($lang){
    global $vnT, $func, $DB, $conf;
    $id = (int)$vnT->input['id'];
    $sql = "SELECT * FROM order_sum where order_id ='{$id}' ";
    $result = $DB->query($sql);
    if ($data = $DB->fetch_row($result)) {
      // nguoi mua
      $info_customer = $this->get_info_address($data);
      // nguoi nhan
      $info_receiver = $this->get_info_address($data, "shipping");
        // chi tiet gio hang
      $html_row_cart = "";
      $sql_cart = "select * from order_detail where order_id = '{$id}'";
      $result_cart = $DB->query($sql_cart);
      $x = 0;  $stt_both=1; $w =65 ;
      while ($row_cart = $DB->fetch_row($result_cart)) 
			{ 
        $row_cart['stt'] = $x + 1;
				$total = ($row_cart['price'] * $row_cart['quantity']);
        $item_type = $row_cart["item_type"] ;
				$item_id = $row_cart["item_id"] ;
				$link = $row_cart["item_link"];
				$src_pic = ($row_cart["item_picture"]) ? $vnT->func->get_src_modules ($row_cart["item_picture"], $w, 0,0) : ROOT_URL."modules/product/images/nophoto.gif" ;
 			
        $item_title =	"<a href='".$link."' target='_blank' ><strong >".$row_cart['item_title']."</strong></a>" ;

        if($row_cart['color'] > 0)
          $item_title .= '<div style="padding-top: 2px;">Màu sắc: <strong>'.$row_cart['text_color']. '</strong></div>';

        if($row_cart['size'] > 0)
          $item_title .= '<div style="padding-top: 2px;">Kích thước: <strong>'.$row_cart['text_size']. '</strong></div>';


        $row_cart['pic'] = "<a href='{$link}' target='_blank'><img src=\"{$src_pic}\" width=\"{$w}\" alt='".$row_cart['item_title']."' /></a>";
				$row_cart['item_title'] = $item_title ;	
				
        $row_cart['price'] = $this->get_price_pro($row_cart['price'], 0);

			  
        $row_cart['total'] = $this->get_price_pro($total, 0);
 				$row_cart['class'] = "row_item_cart";			 
			
        $this->skin->assign('row', $row_cart);
        $this->skin->parse("html_print.row_cart");
				
        $x ++;
      }
      //====
      
			
			
			$data['info_customer'] = $info_customer;
      $data['info_receiver'] = $info_receiver;
      $data['shipping_name'] = $this->get_method_name("shipping_method", $data['shipping_method'], $data['lang']);
      $data['payment_name'] = $this->get_method_name("payment_method", $data['payment_method'], $data['lang']);
      $info_bill ='';
      if($data['bill']){
        $info_bill .= '<p style="padding-top: 10px"><b>Xuất hóa đơn công ty :</b>';
        $info_bill .= '<br>Tên công ty : '.$data['bill_company'];
        $info_bill .= '<br>Địa chỉ : '.$data['bill_address'];
        $info_bill .= '<br>MST : '.$data['bill_mst'];
        $info_bill .= '</p>';
      }
      $data['info_bill'] = $info_bill ;


      if ($data['promotion_code'] != "") {
        $data['info_promotion'] = '<p><b>Dùng mã khuyến mãi .</b> CODE : <b>' . $data['promotion_code'].'</b></p>';
      }



      $data['other_price'] ='';

      if ($data['promotion_price']) {
        $data['other_price'] .= '<tr  height="20">
               <td class="row_total_cart"  colspan="5"  align="right"><b>Khuyến mãi Coupon : </b> </td>
               <td  class="row_total_cart"   align="right" style="padding-right:5px" ><strong>-' . $this->get_price_pro($data['promotion_price'], "0") . '</strong></td>
							</tr>';
      }

      if ($data['discount']) {
        $data['other_price'] .= '<tr  height="20">
               <td class="row_total_cart"  colspan="5"  align="right"><b>Giảm giá thêm : </b> </td>
               <td  class="row_total_cart"   align="right" style="padding-right:5px" ><strong>-' . $this->get_price_pro($data['discount'], "0") . '</strong></td>
							</tr>';
      }
 			
    }
		
		
		$data['total_cart'] = $this->get_price_pro($data['total_cart']);
    $data['s_price'] = $this->get_price_pro($data['s_price'], 0);
    $data['total_price'] = $this->get_price_pro($data['total_price']);
		$data['eligible_for_sale'] = str_replace("{point}", "<b class=font_err>" . $data['mem_point_earned'] . "</b>", $vnT->lang['mess_eligible_for_sale']);
		
		$data['text_status'] = $vnT->setting['status'][$data['status']];
    $data['date_order'] = date("H:i, d/m/Y", $data['date_order']);
		
		if ($data['ship_date']) {
			$data['text_ship_date'] = '<tr >
        <td ><strong>'.$vnT->lang['date_ship'].': </strong> '.@date("d/m/Y", $data['ship_date']).'</td> 
      </tr>';		
		} 
		
		if ($data['comment']) {			
			$data['text_comment'] = "<b>".$vnT->lang['comment_order'].":</b><br>". $func->HTML($data['comment']);
		}
			
    /*assign the array to a template variable*/
    $this->skin->assign('data', $data);
    $this->skin->parse("html_print");
    $content = $this->skin->text("html_print");
		
		$qu_find = array(
				'{order_code}',
				'{date_order}',
				'{content}',
			);
		$qu_replace = array(
				$data["order_code"] ,
				$data['date_order'],
				$content
			);
			
		$textout = str_replace($qu_find, $qu_replace,$func->load_SiteDoc("header_footer_order"));
		
		return $textout ;
  }
}
$vntModule = new vntModule();
?>