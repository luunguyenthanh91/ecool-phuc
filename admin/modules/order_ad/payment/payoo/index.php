


<?php
if (isset($msg)) {
  echo stripslashes($msg);
}
?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td width="250"><a href="https://www.payoo.com.vn"><img
			src="modules/order_ad/payment/payoo/logo.gif" alt="" border="0"
			title="" /></a></td>
		<td valign="top">
		<p style="line-height: 18px;">Ví điện tử Payoo là một tài khoản điện
		tử, có chức năng như một chiếc Ví tiền trong thế giới internet nhằm hỗ
		trợ người dùng mua – bán – giao dịch tại các trang web thương mại điện
		tử và tại các cộng đồng mạng có hoạt động thanh toán hoặc trả phí.</p>
		<p style="line-height: 18px;">Nói cách khác, Ví điện tử Payoo là một
		“Chiếc Ví thật trong thế giới Số” cho phép các công dân thời đại
		internet có thể giao dịch – mua bán – trao đổi trực tuyến một cách
		tiện lợi và an toàn.</p>
		</td>
	</tr>
</table>





<form action="<?=$link_action?>" method="post" name="f_config">
<h3 class="font_title">Configuration Settings</h3>
<table width="100%" cellspacing="1" cellpadding="1" border="0"
	class="admintable">

	<tr>

		<td width="30%" class="row1">Username of business account</td>

		<td class="row0"><input type="text" name="module[BusinessUsername]"
			value="<?php
  echo $module['BusinessUsername'];
  ?>" class="textbox"
			size="60" /></td>
	</tr>
	<tr>
		<td class="row1">Shop ID</td>

		<td class="row0"><input type="text" name="module[ShopID]"
			value="<?php
  echo $module['ShopID'];
  ?>" class="textbox" size="60" /></td>
	</tr>
	<tr>
		<td class="row1">Shop Title</td>

		<td class="row0"><input type="text" name="module[ShopTitle]"
			value="<?php
  echo $module['ShopTitle'];
  ?>" class="textbox" size="60" /></td>
	</tr>
	<tr>
		<td class="row1">Shop Domain</td>

		<td class="row0"><input type="text" name="module[ShopDomain]"
			value="<?php
  echo $module['ShopDomain'];
  ?>" class="textbox"
			size="60" /></td>
	</tr>
	<tr>
		<td class="row1">Shipping Days (Số ngày giao hàng)</td>

		<td class="row0"><input type="text" name="module[ShippingDays]"
			value="<?php
  echo $module['ShippingDays'];
  ?>" class="textbox"
			size="60" /></td>
	</tr>

	<tr>
		<td class="row1">Payoo Payment Url</td>

		<td class="row0"><input type="text" name="module[PayooPaymentUrl]"
			value="<?php
  echo $module['PayooPaymentUrl'];
  ?>" class="textbox"
			size="60" /></td>
	</tr>
	<tr>
		<td colspan="2"><b class="font_title">Callerservices Config</b></td>
	</tr>
	<tr>
		<td class="row1">Business API Url</td>

		<td class="row0"><input type="text" name="module[BusinessAPIUrl]"
			value="<?php
  echo $module['BusinessAPIUrl'];
  ?>" class="textbox"
			size="60" /></td>
	</tr>
	<tr>
		<td class="row1">API Username</td>

		<td class="row0"><input type="text" name="module[APIUsername]"
			value="<?php
  echo $module['APIUsername'];
  ?>" class="textbox"
			size="60" /></td>
	</tr>
	<tr>
		<td class="row1">API Password</td>

		<td class="row0"><input type="text" name="module[APIPassword]"
			value="<?php
  echo $module['APIPassword'];
  ?>" class="textbox"
			size="60" /></td>
	</tr>
	<tr>
		<td class="row1">API Signature</td>

		<td class="row0"><input type="text" name="module[APISignature]"
			value="<?php
  echo $module['APISignature'];
  ?>" class="textbox"
			size="60" /></td>
	</tr>


	<tr>
		<td class="row1">&nbsp;</td>
		<td class="row0"><input type="submit" name="btnConfig"
			value="Edit Config" class="button" /></td>
	</tr>
</table>

</form>
