<!-- BEGIN: edit -->
{data.err}
<form action="{data.link_action}" method="post" enctype="multipart/form-data" name="myForm"  class="validate">
  <table width="100%"  border="0"  cellspacing="1" cellpadding="1" class="admintable">



      <tr class="form-required">
     <td class="row1" width="20%">{LANG.name}: </td>
     <td class="row0"><input name="name" type="text" size="50" maxlength="250" value="{data.name}"></td>
    </tr>


		<tr class="form-required">
     <td class="row1" width="20%">{LANG.title}: </td>
     <td class="row0"><input name="title" type="text" size="50" maxlength="250" value="{data.title}"></td>
    </tr>

      <tr >
          <td class="row1" width="20%">{LANG.picture}: </td>
          <td class="row0">
              <div id="ext_picture" class="picture" >{data.pic}</div>
              <input type="hidden" name="picture"	 id="picture" value="{data.logo}" />
              <div id="btnU_picture" class="div_upload" {data.style_upload} ><div class="button2"><div class="image"><a title="Add an Image" class="thickbox" id="add_image" href="?mod=media&act=popup_media&module=product&folder=product&obj=picture&type=image&TB_iframe=true&width=900&height=474" >Chọn hình</a></div></div></div>
          </td>
      </tr>


      <tr >
     <td class="row1" width="20%">Mô tả ngắn: </td>
     <td class="row0"><textarea name="short" id="metadesc" rows="3" cols="50" style="width:95%" class="textarea">{data.short}</textarea></td>
    </tr>
    
    <tr >
     <td class="row1" width="20%">{LANG.description}: </td>
     <td class="row0">{data.html_content}</td>
    </tr>
    <tr>
    		<td class="row1" width="20%">Thông báo thanh toán thành công : </td>
     		<td class="row0">{data.html_content_success}</td>
    </tr>
    <tr>
    		<td class="row1" width="20%">Thông báo thanh toán thất bại : <br /><br /><font class="font_err" style="font-weight:normal">(Chỉ áp dụng cho Thanh toán trực tuyến)</font></td>
     		<td class="row0">{data.html_content_failed}</td>
    </tr>
    
    <tr >
     <td class="row1" width="20%">{LANG.display}: </td>
     <td class="row0">{data.list_display}</td>
    </tr>
			
		<tr align="center">
    <td class="row1" >&nbsp; </td>
			<td class="row0" >
				<input type="hidden" name="do_submit"	 value="1" />
				<input type="submit" name="btnAdd" value="Submit" class="button">
				<input type="reset" name="btnReset" value="Reset" class="button">
			</td>
		</tr>
	</table>
</form>


<br>
<!-- END: edit -->

<!-- BEGIN: manage -->
<form action="{data.link_fsearch}" method="post" name="fSearch">
<table width="100%" border="0" cellspacing="2" cellpadding="2" align="center" class="tableborder">
  <tr>
    <td width="15%" align="left">{LANG.totals}: &nbsp;</td>
    <td width="85%" align="left"><b class="font_err">{data.totals}</b></td>
  </tr>
</table>
</form>
{data.err}
<br />
{data.table_list}
<br />
<table width="100%"  border="0" align="center" cellspacing="1" cellpadding="1" class="bg_tab">
  <tr>
    <td  height="25">{data.nav}</td>
  </tr>
</table>
<br />
<!-- END: manage -->