<!-- BEGIN: edit -->
<div class="OrderInfo">
  <div class="block-info">
    <table width="100%" border="0" cellspacing="4" cellpadding="4" align="center">
      <tr>
        <td width="33%">Order no : <strong class="order_code">{data.order_code}</strong></td>
        <td align="center">
          <p><strong>Trạng thái</strong> : <span style="font-size:18px;border:1px solid #ccc; padding:5px 10px; display:inline-block">{data.text_status}</span></p>
        </td>
        <td align="right" width="30%"><p><strong>Ngày đặt hàng:</strong> {data.date_order}</p></td>
      </tr>
    </table>
  </div>
  <div class="block-info order-box">
    <div class="order-title"><span>{LANG.f_info_order}</span></div>
    <table width="100%" border="0" cellspacing="2" cellpadding="2">
      <tr>
        <td valign="top"><strong>{LANG.payment_address} :</strong> <br/>{data.info_customer}</td>
        <td valign="top"><strong>{LANG.shipping_address} :</strong> <br/>{data.info_receiver}</td>
      </tr>
      <tr>
        <td valign="top" width="50%">
          <p><strong>{LANG.payment_method} :</strong><br/>{data.payment_name}</p>
          {data.info_bill}
          {data.info_promotion}
          {data.info_ctv}
        </td>
        <td valign="top">
          <p><strong>{LANG.shipping_method} :</strong><br>{data.shipping_name}</p>
        </td>
      </tr>
    </table>
    <div style="padding: 10px 2px;" class="box-comment">
      <strong>{LANG.comment_order} </strong> : {data.comment}
    </div>
  </div>
  <div class="block-info order-box">
    <div class="order-title"><span>{LANG.f_info_cart}</span></div>
    <form id="fCart" name="fCart" method="post" action="{data.link_action}">
      <table width="100%"  class="table table-sm table-bordered table-hover">
        <tr>
          <td width="5%" class="row_title_cart" align="center"><b>{LANG.stt}</b></td>
          <td width="10%" class="row_title_cart" align="center"><b>{LANG.picture}</b></td>
          <td width="30%" class="row_title_cart" align="center"><b>{LANG.p_name}</b></td>
          <td width="20%" class="row_title_cart" align="center"><b>{LANG.price}</b></td>
          <td width="10%" class="row_title_cart" align="center"><b>{LANG.quantity}</b></td>
          <td width="15%" class="row_title_cart" align="center"><b>{LANG.total}</b></td>
        </tr>
        <!-- BEGIN: row_cart -->
        <tr>
          <td class="row_item_cart" align="center">{row.stt}</td>
          <td class="row_item_cart" align="center">{row.pic}</td>
          <td class="row_item_cart" align="left">{row.item_title}</td>
          <td class="row_item_cart" align="right">{row.price}</td>
          <td class="row_item_cart" align="center">{row.quantity}</td>
          <td class="row_item_cart" align="right">{row.total}</td>
        </tr>
        <!-- END: row_cart -->
        <tr>
          <td class="row_total_cart" colspan="5" align="right"><b>{LANG.total_price}: </b></td>
          <td class="row_total_cart" align="right" style="padding-right:5px"><b>{data.text_total_price}</b></td>
        </tr>
      </table>
    </form>
  </div>
  <div class="block-info order-box">
    <div class="order-title"><span>LỊCH SỬ ĐƠN HÀNG</span></div>
    <table class="table table-sm table-bordered table-hover tblRemote" width="100%" cellspacing="1" cellpadding="1" border="0">
      <thead>
        <tr>
          <th width="15%" align="center"><b>Thời gian</b></th>
          <th width="15%" align="center"><b>Trạng thái</b></th>
          <th><b>Nội dung</b></th>
          <th width="15%" align="center"><b>Người cập nhật</b></th>
        </tr>
      </thead>
      <tbody>
        <!-- BEGIN: html_history -->
        <tr>
          <td align="center">{row.date_submit}</td>
          <td align="center">{row.text_status}</td>
          <td >{row.note}</td>
          <td align="center">{row.admin_name}</td>
        </tr>
        <!-- END: html_history -->
        <!-- BEGIN: html_history_no-->
        <tr>
          <td colspan="4" class="font_err"  align="center">Chưa có thông tin</td>
        </tr>
        <!-- END: html_history_no -->
      </tbody>
    </table>
  </div>
  {data.err}
  <!-- BEGIN: html_form_update -->
  <div class="block-info order-box">
    <div class="order-title"><span>Cập nhật tiến độ của đơn hàng</span></div>
    <form action="{data.link_action}" method="post" name="myForm">
      <table width="100%" border="0" cellspacing="1" cellpadding="1" class="admintable">
        <tr>
          <td class="row1"><strong>Trạng thái: </strong></td>
          <td class="row0">{data.list_status} &nbsp;&nbsp; </td>
        </tr>
        <tr>
          <td class="row1"><strong>{LANG.note} : </strong></td>
          <td class="row0">{data.html_note}</td>
        </tr>
        <tr>
          <td class="row1">&nbsp;</td>
          <td class="row0">
            <input type="hidden" name="status_old" value="{data.status}" />
            <input type="hidden" name="onhand_status" value="{data.onhand_status}" />
            <input type="hidden" name="mem_id" value="{data.mem_id}"/>
            <input type="hidden" name="mem_point" value="{data.mem_point_earned}"/>
            <input type="submit" name="btnSave" value=" Cập nhật " class="button"> &nbsp;&nbsp;
            <input type="button" name="btnPrint" value=" In Hóa Đơn " onClick="javascript:openPopUp('{data.link_print}','Print',800,600, 'Yes')" class="button">
          </td>
        </tr>
      </table>
    </form>
  </div>
  <!-- END: html_form_update -->
  <!-- BEGIN: html_form_finished -->
  <div class="block-info order-box">
    <div class="order-title"><span>{LANG.update_order}</span></div>
    <form action="{data.link_action}" method="post" name="myForm">
      <table width="100%" border="0" cellspacing="1" cellpadding="1" class="admintable">
        <tr>
          <td align="right"><strong>Trạng thái đơn hàng: </strong></td>
          <td width="70%" align="left"><b class="font_err">{data.text_status}</b></td>
        </tr>
        <tr>
          <td align="right"><strong>Ghi chú : </strong></td>
          <td align="left">{data.note}</td>
        </tr>
      </table>
    </form>
  </div>
  <!-- END: html_form_finished -->
</div>
<!-- END: edit -->

<!-- BEGIN: manage -->
<form action="{data.link_fsearch}" method="post" name="fSearch">
  <table width="100%" border="0" cellspacing="2" cellpadding="2" align="center" class="tableborder">
    <tr>
      <td class="row1"><strong>Đặt hàng từ ngày :</strong></td>
      <td align="left" class="row0">
        <input type="text" name="date_begin" id="date_begin" value="{data.date_begin}" size="15" maxlength="10" class="dates"/> &nbsp;&nbsp; <strong>đến :</strong> <input type="text" name="date_end" id="date_end" value="{data.date_end}"  size="15" maxlength="10" class="dates" /> &nbsp;&nbsp;
        <strong>{LANG.type_customer} :</strong> {data.list_type}
      </td>
    </tr>
    <tr>
      <td><strong>{LANG.status} :</strong></td>
      <td>
        {data.list_status} &nbsp;&nbsp; <strong>Tỉnh thành :</strong> <select class="form-control load_state" name="city" id="city" data-state="state">{data.list_city}</select> &nbsp;&nbsp; <strong>Quận huyện :</strong> <select class="form-control" name="state" id="state">{data.list_state}</select>
      </td>
    </tr>
    <tr>
      <td><strong>{LANG.search_for} :</strong></td>
      <td>
        {data.list_search} <strong> {LANG.keyword} :</strong>
        <input name="keyword" value="{data.keyword}"  type="text">
        <input name="btnSearch" type="submit" value=" Search ! " class="button">
      </td>
    </tr>
    <tr>
      <td width="15%"><strong>{LANG.totals}:</strong> &nbsp;</td>
      <td width="85%">
        <b class="font_err">{data.totals}</b> &nbsp; | &nbsp;<a href="{data.link_excel}"  target="_blank"><img  src="{DIR_IMAGE}/icon_excel.gif" alt="excel" align="absmiddle"/> <b>Xuất ra file Excel theo  điều kiện Search</b> </a>
      </td>
    </tr>
  </table>
</form>
<div id="ajax_mess"></div>
{data.err}<br/>
{data.table_list}<br/>
<table width="100%" border="0" align="center" cellspacing="1" cellpadding="1" class="bg_tab">
  <tr>
    <td height="25">{data.nav}</td>
  </tr>
</table><br/>
<!-- END: manage -->

<!-- BEGIN: html_print -->
<html>
<link href="{DIR_MOD}/css/print.css" rel="stylesheet" type="text/css"/>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
<title>PRINT YOUR ORDER</title>
<body style="padding:5px;" onLoad="window.print();">


<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
    <tr>
        <td>
            <fieldset>
                <legend class="font_err">&nbsp;<strong>{LANG.f_info_order}</strong>&nbsp;</legend>
                <table width="100%" border="0" cellspacing="2" cellpadding="2">

                    <tr>
                        <td valign="top"><strong>{LANG.payment_address} :</strong> <br/>
                            {data.info_customer}</td>
                        <td valign="top"><strong>{LANG.shipping_address} :</strong> <br/>
                            {data.info_receiver}</td>
                    </tr>
                    <tr>
                        <td valign="top" width="50%">
                            <p><strong>{LANG.payment_method} :</strong><br/>{data.payment_name}</p>

                            {data.info_bill}
                            {data.info_promotion}
                            {data.info_ctv}

                        </td>
                        <td valign="top">
                            <p><strong>{LANG.shipping_method} :</strong><br>{data.shipping_name}</p>
                        </td>
                    </tr>

                </table>
            </fieldset>
        </td>
    </tr>
</table>
<br>
<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
    <tr>
        <td>
            <fieldset>
                <legend class="font_err">&nbsp;<strong>{LANG.f_info_cart}</strong>&nbsp;</legend>
                <br/>
                <table width="100%" border="0" cellpadding="2" cellspacing="1" class="border_cart">
                    <tr>
                        <td width="5%" class="row_title_cart" align="center"><b>{LANG.stt}</b></td>
                        <td width="10%" class="row_title_cart" align="center"><b>{LANG.picture}</b></td>
                        <td width="30%" class="row_title_cart" align="center"><b>{LANG.p_name}</b></td>

                        <td width="20%" class="row_title_cart" align="center"><b>{LANG.price}</b></td>
                        <td width="10%" class="row_title_cart" align="center"><b>{LANG.quantity}</b></td>
                        <td width="15%" class="row_title_cart" align="center"><b>{LANG.total}</b></td>
                    </tr>

                    <!-- BEGIN: row_cart -->
                    <tr height=25>
                        <td class="row_item_cart" align="center">{row.stt}</td>
                        <td class="row_item_cart" align="center">{row.pic}</td>
                        <td class="row_item_cart" align="left">{row.item_title}</td>
                        <td class="row_item_cart" align="right">{row.price}</td>
                        <td class="row_item_cart" align="center">{row.quantity}</td>
                        <td class="row_item_cart" align="right">{row.total}</td>
                    </tr>
                    <!-- END: row_cart -->

                    <tr height="20">
                        <td class="row_total_cart" colspan="5" align="right"><b>{LANG.basket_total}: </b></td>
                        <td class="row_total_cart" align="right" style="padding-right:5px"><b>{data.total_cart}</b></td>
                    </tr>
                    <tr height="20">
                        <td class="row_total_cart" colspan="5" align="right"><b>{LANG.s_price}: </b></td>
                        <td class="row_total_cart" align="right" style="padding-right:5px"><b>{data.s_price}</b></td>
                    </tr>
                    {data.other_price}
                    <tr height="20">
                        <td class="row_total_cart" colspan="5" align="right"><b>{LANG.total_price}: </b></td>
                        <td class="row_total_cart" align="right" style="padding-right:5px"><b>{data.total_price}</b>
                        </td>
                    </tr>

                </table>
            </fieldset>
        </td>
    </tr>
</table>
<br>
<table width="100%" border="0" cellspacing="1" cellpadding="1" align="center">
    <tr>
        <td><strong>{LANG.comment_order}: </strong>
            <p>{data.comment}</p></td>
    </tr>
    <tr>
        <td><strong>Trạng thái đơn hàng: </strong> <b class="font_err">{data.text_status}</b></td>
    </tr>


    {data.text_ship_date}
</table>


</body>
</html>

<!-- END: html_print -->