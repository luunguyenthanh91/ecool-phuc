<!-- BEGIN: edit -->
<table width="80%" border="0" cellspacing="1" cellpadding="1" align="center"  bgcolor="#CCCCCC">
    <tr bgcolor="#FFFFFF">
        <td width="150" align="center" style="padding:5px;">{data.pic_pro}</td>
        <td style="padding:5px;">Serial : <strong class="font_err">{data.maso}</strong><br />
            Tên SP: <strong>{data.p_name}</strong><br />
            Collection ID : #<strong>{data.p_id}</strong>
        </td>
    </tr>
</table>
<br />

{data.err}
<form action="{data.link_action}" method="post" enctype="multipart/form-data" name="myForm"  class="validate">
  <table width="100%"  border="0"  cellspacing="1" cellpadding="1" class="admintable">

    <tr  >
     <td class="row1" width="20%">Họ tên: </td>
     <td class="row0"><input name="name" type="text" size="50" maxlength="250" value="{data.name}"></td>
    </tr>
		
    <tr  >
     <td class="row1"  >Email: </td>
     <td class="row0"><input name="email" type="text" size="50" maxlength="250" value="{data.email}">  </td>
    </tr>
     
 
    <tr  >
     <td class="row1"  >Nội dung : </td>
     <td class="row0"> <textarea name="content" id="content" rows="3" cols="50" style="width:90%" class="textarea">{data.content}</textarea></td>
    </tr>
 
    <tr  >
     <td class="row1"  >Trạng thái : </td>
     <td class="row0">{data.list_display} <input type="hidden" name="display_old" id="display_old" value="{data.display}" /></td>
    </tr>
    
    <tr class="f_title">
    	<td colspan="2"><strong>Trả lời comment</strong></td>
    </tr>
    
     <tr  >
     <td class="row1" width="20%">Họ tên: </td>
     <td class="row0"><input name="name_rep" type="text" size="50" maxlength="250" value="{data.name_rep}"></td>
    </tr>
		
    <tr  >
     <td class="row1"  >Email: </td>
     <td class="row0"><input name="email_rep" type="text" size="50" maxlength="250" value="{data.email_rep}">  </td>
    </tr>
     
 
    <tr  >
     <td class="row1"  >Nội dung : </td>
     <td class="row0"> <textarea name="content_rep" id="content_rep" rows="3" cols="50" style="width:90%" class="textarea"></textarea></td>
    </tr>
    
     
			
		<tr align="center">
    <td class="row1" >&nbsp; </td>
			<td class="row0" >
      	
        <input type="hidden" name="p_id" id="p_id" value="{data.p_id}" />
      	<input type="hidden" name="numvote" id="numvote" value="{data.numvote}" />
       	<input type="hidden" name="votes" id="votes" value="{data.votes}" />
				<input type="hidden" name="do_submit"	 value="1" />
				<input type="submit" name="btnAdd" value="Submit" class="button">
				<input type="reset" name="Submit2" value="Reset" class="button">
			</td>
		</tr>
	</table>
</form>


<br>
<!-- END: edit -->

<!-- BEGIN: manage -->
<form action="{data.link_fsearch}" method="post" name="fSearch">
<table width="100%" border="0" cellspacing="2" cellpadding="2" align="center" class="tableborder">
 
  <tr>
    <td > <strong>Tiêu đề tin :</strong> </td>
    <td ><input type="text" style="width:95%" id="keyword" name="keyword" value="{data.keyword}" autocomplete="off"    ><input type="hidden" id="p_id" name="p_id" value="{data.p_id}"></td>
  </tr>
  <tr>
    <td ><strong>{LANG.status}:</strong> &nbsp;</td>
    <td >{data.list_display} &nbsp;  <input name="btnSearch" type="submit" value=" Search ! " class="button"></td>
  </tr>
   <tr>
    <td width="15%" align="left"><strong>{LANG.totals}:</strong> &nbsp;</td>
    <td width="85%" align="left"><b class="font_err">{data.totals}</b></td>
  </tr>
</table>
</form>
{data.err}
<br />
{data.table_list}
<br />
<table width="100%"  border="0" align="center" cellspacing="1" cellpadding="1" class="bg_tab">
  <tr>
    <td  height="25">{data.nav}</td>
  </tr>
</table>
<br />

<!-- END: manage -->

<!-- BEGIN: sub_comment -->
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>[:: Admin ::]</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<LINK href="{DIR_STYLE}/global.css" rel="stylesheet" type="text/css">
<!--[if gte IE 6]>
<link rel='stylesheet' href="{DIR_STYLE}/ie.css" type='text/css' media='all' />
<![endif]-->
<link href="{DIR_STYLE}/style_tooltips.css" rel="stylesheet" type="text/css" />
<link rel='stylesheet' href='{DIR_JS}/thickbox/thickbox.css' type='text/css' media='all' />
{EXT_STYLE}
<script language="javascript" >
	var ROOT = "{CONF.rooturl}";
	var DIR_IMAGE = "{DIR_IMAGE}";
	var lang_js = new Array(); 
		lang_js["please_chose_item"]   	= "{LANG.please_chose_item}'";
		lang_js["are_you_sure_del"]   	= "{LANG.are_you_sure_del}";
</script>
    <script type='text/javascript' src="{DIR_JS}/jquery.min.js"></script>
    <script type='text/javascript' src="{DIR_JS}/jquery-migrate.min.js"></script>
<script language="javascript1.2" src="{DIR_JS}/admin/js_admin_2017.js?v=1.0"></script>

<script type='text/javascript' src='{DIR_JS}/thickbox/thickbox.js'></script> 
<script id='ext_javascript'></script>
{EXT_HEAD}

</head>
<body style="margin:10px;" > 
<form action="{data.link_action}" method="post"  name="myForm"  class="validate">
  <table width="100%"  border="0"  cellspacing="1" cellpadding="1" class="admintable">	
    <tr class="form-required">
     <td class="row1" width="20%">Danh sách ý kiến cho : </td>
     <td class="row0">{data.comment_name}</td>
    </tr>              
		 
	</table>
</form>
{data.err}
<br />
{data.table_list}
<br />
 
</body>
</html> 
<!-- END: sub_comment -->
