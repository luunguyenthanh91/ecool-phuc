<!-- BEGIN: edit -->
<form action="{data.link_action}" method="post" enctype="multipart/form-data" name="myForm"  class="validate">
  <table width="100%" border="0" cellcollectioncing="1" cellpadding="1" class="admintable">
    <tr>
      <td valign=top width=50% class="row0" colcollectionn="2" colspan="2">
        <table width="100%" border="0" cellcollectioncing="0" cellpadding="0">
          <tr>
            <td width="20%" class="row1"><strong>{LANG.full_name} </strong>:</td>
            <td class="row0">{data.name}</td>
          </tr>
          <tr>
            <td class="row1"><strong>{LANG.phone} </strong>:</td>
            <td class="row0">{data.phone}</td>
          </tr> 
          <tr>
            <td class="row1"><strong>Email</strong>:</td>
            <td class="row0">{data.email}</td>
          </tr>
          <tr>
            <td class="row1"><strong>Địa chỉ</strong>:</td>
            <td class="row0">{data.address}</td>
          </tr>
          <tr>
            <td class="row1"><strong>Loại công trình</strong>:</td>
            <td class="row0">{data.project_name}</td>
          </tr>
          <tr>
            <td class="row1"><strong>Loại sản phẩm</strong>:</td>
            <td class="row0">{data.product_name}</td>
          </tr>
          <tr>
            <td class="row1"><strong>Phong cách</strong>:</td>
            <td class="row0">{data.style_name}</td>
          </tr>
          <tr>
            <td class="row1"><strong>Chất liệu</strong>:</td>
            <td class="row0">{data.material_name}</td>
          </tr>
          <tr>
            <td class="row1"><strong>Màu sắc</strong>:</td>
            <td class="row0">{data.color}</td>
          </tr>
          <tr>
            <td class="row1"><strong>Diện tích</strong>:</td>
            <td class="row0">{data.area}</td>
          </tr>
          <tr>
            <td class="row1"><strong>Yêu cầu</strong>:</td>
            <td class="row0">{data.note}</td>
          </tr>
          <tr>
            <td class="row1"><strong>Nội dung</strong>:</td>
            <td class="row0">{data.comment}</td>
          </tr>
          <tr>
            <td class="row1"><strong>Trạng thái</strong>:</td>
            <td class="row0">{data.Status_Booking}</td>
          </tr>
        </table>
      </td>
    </tr>
    <tr>
      <td class="row1" width="10%"></td>
      <td class="row0" >
        <input type="hidden" name="do_submit"  value="1" />
        <input type="submit" name="btnAdd" value="Submit" class="button">
        <input type="reset" name="Submit2" value="Reset" class="button">
      </td>
    </tr>
  </table>
</form>
<!-- END: edit -->

<!-- BEGIN: manage -->
<form action="{data.link_fsearch}" method="post" name="myform">
  <table width="100%" border="0" cellcollectioncing="2" cellpadding="2" align="center" class="tableborder">
    <tr>
      <td width="20%" align="left">{LANG.totals}: &nbsp;</td>
      <td align="left"><b class="font_err">{data.totals}</b></td>
    </tr>
  </table>
</form>
{data.err}<br />
{data.table_list}<br/>
<table width="100%" border="0" align="center" cellcollectioncing="1" cellpadding="1" class="bg_tab">
  <tr>
    <td height="25">{data.nav}</td>
  </tr>
</table><br />
<!-- END: manage -->