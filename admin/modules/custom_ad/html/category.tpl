<!-- BEGIN: edit -->
{data.err}
<form action="{data.link_action}" method="post" enctype="multipart/form-data" name="myForm"  class="validate">
  <table width="100%" border="0" cellspacing="1" cellpadding="1" class="admintable">
    <tr>
      <td class="row1">{LANG.cat_parent}: </td>
      <td align="left" class="row0">{data.listcat}</td>
    </tr>
    <tr class="form-required">
      <td class="row1" width="20%">{LANG.cat_name}: </td>
      <td class="row0">
        <input name="cat_name" type="text" size="50" maxlength="250" value="{data.cat_name}" class="textfield">
      </td>
    </tr>
    <tr class="form-required">
      <td class="row1" width="20%">Slogan: </td>
      <td class="row0">
        <input name="slogan" type="text" size="50" maxlength="250" value="{data.slogan}" class="textfield">
      </td>
    </tr>
    <tr>
      <td class="row1" width="20%">{LANG.picture}: </td>
      <td class="row0">
     	  <div id="ext_picture" class="picture" >{data.pic}</div>
        <input type="hidden" name="picture" id="picture" value="{data.picture}" />
        <div id="btnU_picture" class="div_upload" {data.style_upload}>
          <div class="button2">
            <div class="image">
              <a title="Add an Image" class="thickbox" id="add_image" href="{data.link_upload}" >Chọn hình</a>
            </div>
          </div>
        </div>
      </td>
    </tr>
    <tr>
      <td class="row1">{LANG.description}: </td>
      <td class="row0">
        <textarea name="description" id="description" rows="4" cols="50" style="width:90%" class="textarea">{data.description}</textarea>
      </td>
    </tr>
    <tr class="row_title" >
      <td colspan="2" class="font_title">Search Engine Optimization : </td>
    </tr>
    <tr>
      <td class="row1" valign="top" style="padding-top:10px;">Friendly URL :</td>
      <td class="row0">
        <input name="friendly_url" id="friendly_url" type="text" size="50" maxlength="250" value="{data.friendly_url}" class="textfield"><br><span class="font_err">({LANG.mess_friendly_url})</span>
      </td>
    </tr>
    <tr>
      <td class="row1" valign="top" >Friendly Title :</td>
      <td class="row0">
        <input name="friendly_title" id="friendly_title" type="text" size="60" maxlength="250" value="{data.friendly_title}" class="textfield">
      </td>
    </tr>
    <tr>
      <td class="row1">Meta Keyword :</td>
      <td class="row0"><input name="metakey" id="metakey" type="text" size="60" maxlength="250" value="{data.metakey}" class="textfield"></td>
    </tr>
    <tr>
      <td class="row1">Meta Description : </td>
      <td class="row0">
        <textarea name="metadesc" id="metadesc" rows="3" cols="50" style="width:90%" class="textarea">{data.metadesc}</textarea>
      </td>
    </tr>
		<tr align="center">
      <td class="row1" >&nbsp; </td>
			<td class="row0" >
				<input type="hidden" name="do_submit"	 value="1" />
				<input type="submit" name="btnAdd" value="Submit" class="button">
				<input type="reset" name="Submit2" value="Reset" class="button">
			</td>
		</tr>
	</table>
</form>
<!-- END: edit -->

<!-- BEGIN: move -->
<form action="{data.link_action}" method="post" name="myForm"  class="validate">
  <table width="100%"  border="0"  cellspacing="1" cellpadding="1" class="admintable">
    <tr>
      <td width="48%" class="row1"> {LANG.source_category} </td>
      <td class="row0" align="center">&nbsp;</td>
      <td width="48%" class="row1"> {LANG.des_category} </td>
    </tr>
    <tr class="form-required">
      <td width="48%" class="row1">{data.list_cat} </td>
      <td class="row0" align="center"><strong>=></strong></td>
      <td width="48%" class="row1">{data.list_cat_chose} </td>
    </tr>
    <tr>
      <td width="48%" class="row1"><div id="ext_pro1" class="font_err"></div></td>
      <td class="row0" align="center">&nbsp;</td>
      <td width="48%" class="row1"><div id="ext_pro2" class="font_err"></div></td>
    </tr>
    <tr>
      <td class="row0" colspan="3" align="center">
        <input type="submit" name="btnAll" value="{LANG.move_all}" class="button"/>
      </td>
    </tr>
  </table><br/>
</form>
<div id="ext_listpro"></div>
<!-- END: move -->

<!-- BEGIN: manage -->
<form action="{data.link_fsearch}" method="post" name="fSearch">
  <table width="100%" border="0" cellspacing="2" cellpadding="2" align="center" class="tableborder">
    <tr>
      <td width="15%" align="left">{LANG.totals}: &nbsp;</td>
      <td width="85%" align="left"><b class="font_err">{data.totals}</b></td>
    </tr>
  </table>
</form>
{data.err} <br />
<div class="box-manage">
  <form id="manage" name="manage" method="post" action="{data.link_action}">

    <div class="nav-action nav-top">{data.button}</div>
    <div class="table-list">
      <table  class="table table-sm table-bordered table-hover " id="table_list" >
        <thead>
          <tr height="25">
            <th width="5%" align="center" ><input type="checkbox" value="all" class="checkbox" name="checkall" id="checkall"/></th>            <th width="10%" align="center" >{LANG.order}</th>
            <th width="10%" align="center" >{LANG.picture}</th>                
            <th width="25%" align="left" >{LANG.cat_name}</th>
            <th   >Link</th>
            <th width="10%" align="center" >Focus</th>
            <th width="15%"  align="center" >Action</th>
          </tr>
        </thead>        
        <tbody>
          <!-- BEGIN: html_row -->
          <tr class="{row.class}" id="{row.row_id}">
            <td align="center" >{row.check_box}</td>
            <td align="center" >{row.order}</td>
            <td align="center" >{row.picture}</td> 
            <td align="left" >{row.cat_name}</td>
            <td align="left" >{row.link_cat}</td>
            <td align="center" >{row.is_focus}</td>
            <td align="center" >{row.action}</td>
          </tr>
          <!-- END: html_row -->
          <!-- BEGIN: html_row_no -->
          <tr class="row0" >
            <td  colspan="6" align="center" class="font_err" >{mess}</td>
          </tr>
          <!-- END: html_row_no -->
        </tbody>
      </table>
    </div>
    <div class="nav-action nav-bottom">{data.button}</div>

    <input type="hidden" name="do_action" id="do_action" value="" >
  </form>
</div>
<table width="100%"  border="0" align="center" cellspacing="1" cellpadding="1" class="bg_tab">
  <tr>
    <td  height="25">{data.nav}</td>
  </tr>
</table>
<br />
<!-- END: manage -->