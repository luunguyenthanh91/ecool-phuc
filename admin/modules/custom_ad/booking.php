<?php
/*================================================================================*\
||              Name code : cat_custom.php                        ||
||          Copyright @2008 by Thai Son - CMS vnTRUST                     ||
\*================================================================================*/
/**
 * @version : 2.0
 * @date upgrade : 09/01/2009 by Thai Son
 **/
if (! defined('IN_vnT'))
{
  die('Hacking attempt!');
}

//load Model
include(dirname(__FILE__) . "/functions.php");

class vntModule extends Model
{
  var $output = "";
  var $skin = "";
  var $linkUrl = "";
  var $module = "custom";
  var $action = "booking";

  function vntModule (){
    global $Template, $vnT, $func, $DB, $conf;
    $this->skin = new XiTemplate(DIR_MODULE. DS . $this->module . "_ad" . DS . "html" . DS . $this->action . ".tpl");
    $this->skin->assign('LANG', $vnT->lang);
    $this->skin->assign('DIR_JS', $vnT->dir_js);
		$this->skin->assign('DIR_IMAGE', $vnT->dir_images);		
		$this->skin->assign('DIR_IMAGE_MOD', DIR_IMAGE_MOD);
    $lang = ($vnT->input['lang']) ? $lang = $vnT->input['lang'] : $func->get_lang_default();
    $this->linkUrl = "?mod=" . $this->module . "&act=" . $this->action . "&lang=" . $lang;
    switch ($vnT->input['sub']) {
      case 'edit':
        $nd['f_title'] = 'Chi tiết đơn đăng ký';
        $nd['content'] = $this->do_Edit($lang);
      break;
      case 'del':
        $this->do_Del($lang);
      break;
      case 'markUnread':
        $this->do_markUnread($lang);
      break;
      default:
        $nd['f_title'] = 'Quản lý đơn đăng ký';
        $nd['content'] = $this->do_Manage($lang);
      break;
    }
    $nd['menu'] = $func->getToolbar_Small($this->module, $this->action, $lang);
    $Template->assign("data", $nd);
    $Template->parse("box_main");
    $vnT->output .= $Template->text("box_main");
  }
  function do_Edit ($lang){
    global $vnT, $func, $DB, $conf; 
    $id = (int) $vnT->input['id'];
    if ($vnT->input['do_submit'] == 1){
      $data = $_POST;
      $status = (int) $vnT->input['status'];
      if (empty($err)){
        $cot['status'] = $status;
        $cot['date_post'] = time();
        $ok = $DB->do_update("booking", $cot, " id = {$id}");
        if ($ok){
          $func->clear_cache();
          $func->insertlog("Edit", $_GET['act'], $branch_id);
          $mess = $vnT->lang['edit_success'];
          $url = $this->linkUrl;
          $func->html_redirect($url, $mess);
        } else {
          $err = $func->html_err($vnT->lang['add_failt'] . $DB->debug());
        }
      }
    }
    $sql = "SELECT * FROM custom_booking WHERE id=$id";
    $result = $DB->query($sql);
    if ($data = $DB->fetch_row($result)) {
      if ($data['status'] == 0)
        $DB->query("UPDATE booking SET status=1 WHERE id={$id}");
      $data['date_booking'] = $data['book_time'].', '.$data['book_date'];
      $data['Status_Booking'] = $this->Status_Booking($data['status']);
      $data['project_name'] = $this->get_attr_name($data['project'],$lang);
      $data['product_name'] = $this->get_attr_name($data['product'],$lang);
      $data['style_name'] = $this->get_attr_name($data['style'],$lang);
      $data['material_name'] = $this->get_attr_name($data['material'],$lang);
    }
    if ($data['type'] == 1) $data['style'] = "style=\"display:none\"";
    $data["html_content"] = $vnT->editor->doDisplay('re_content', $_POST['re_content'], '100%', '500', "Default");
    $data['link_action'] = $this->linkUrl . "&sub=edit&id=$id";
    $data['err'] = $err;
    /*assign the array to a template variable*/
    $this->skin->assign('data', $data);
    $this->skin->parse("edit");
    return $this->skin->text("edit");
  }
  function do_markUnread (){
    global $func, $DB, $conf, $vnT;
    $id = (int) $vnT->input['id'];
    $DB->query("update contact set status=0 where id={$id}");
    $mess = "Đã cập nhật trạng thái !";
    $url = $this->linkUrl;
    $func->html_redirect($url, $mess);
  }
  function do_Del ($lang){
    global $func, $DB, $conf, $vnT;
    $id = (int) $vnT->input['id'];
    $ext = $vnT->input["ext"];
    $del = 0;
    $qr = "";
    if ($id != 0) {
      $ids = $id;
    }
    if (isset($vnT->input["del_id"])) {
      $ids = implode(',', $vnT->input["del_id"]);
    }
    $query = 'DELETE FROM contact WHERE id IN (' . $ids . ')';
    if ($ok = $DB->query($query)) {
      $mess = $vnT->lang["del_success"];
    } else
      $mess = $vnT->lang["del_failt"];
    $ext_page = str_replace("|", "&", $ext);
    $url = $this->linkUrl . "&{$ext_page}";
    $func->html_redirect($url, $mess);
  }
  function render_row ($row_info, $lang){
    global $func, $DB, $conf, $vnT;
    $row = $row_info;
    // Xu ly tung ROW
    $id = $row['id'];
    $row_id = "row_" . $id;
    $output['check_box'] = vnT_HTML::checkbox("del_id[]", $id, 0, " ");
    $link_edit = $this->linkUrl . '&sub=edit&id=' . $id . '&ext=' . $row['ext_page'];
    $link_del = "javascript:del_item('" . $this->linkUrl . "&sub=del&id=" . $id . "&ext=" . $row['ext_page'] . "')";
    $output['name'] = "<a href=\"{$link_edit}\"><strong>" . $row['name'] . "</strong></a>";
    $output['email'] = $func->HTML($row['email']);
    $output['datesubmit'] = date("H:i, d/m/Y", $row['date_post']);
    $info = 'Email: '.$row['email'];
    $info.= '<br/>Phone: '.$row['phone'];
    $output['date_booking'] = $info;
    $output['subject'] = "Thông tin liên lạc của khách hàng";
    switch ($row['status']) {
      case 1:
        $output['status'] = "<customn style='color:#f00;'>Khảo sát mới</customn>";
      break;
      case 2:
        $output['status'] = "<customn style='color:blue;'>Đã liên lạc lại</customn>";
      break;
      case 3:
        $output['status'] = "<customn style='color:green;'>Đã tư vấn</customn>";
      break;
      default:
        $output['status'] = "Unknow";
      break;
    }
    $output['action'] = '<input name=h_id[]" type="hidden" value="' . $id . '" />';
    $output['action'] .= '<a href="' . $link_edit . '"><img src="' . $vnT->dir_images . '/edit.gif"  alt="Edit "></a>&nbsp;';
    $output['action'] .= '<a href="'.$link_del.'"><img src="'.$vnT->dir_images . '/delete.gif"  alt="Delete "></a>';
    return $output;
  }
  function do_Manage ($lang){
    global $vnT, $func, $DB, $conf;
    if ($vnT->input["do_action"]) {
      $func->clear_cache();
      if ($vnT->input["del_id"])
        $h_id = $vnT->input["del_id"];
      switch ($vnT->input["do_action"]) {
        case "do_hidden":
          $mess .= "- " . $vnT->lang['hidden_success'] . " ID: <strong>";
          for ($i = 0; $i < count($h_id); $i ++) {
            $dup['status'] = 0;
            $ok = $DB->do_update("contact", $dup, "id=" . $h_id[$i]);
            if ($ok) {
              $str_mess .= $h_id[$i] . ", ";
            }
          }
          $mess .= substr($str_mess, 0, - 2) . "</strong><br>";
          $err = $func->html_mess($mess);
        break;
        case "do_display":
          $mess .= "- " . $vnT->lang['display_success'] . "  ID: <strong>";
          for ($i = 0; $i < count($h_id); $i ++) {
            $dup['status'] = 1;
            $ok = $DB->do_update("contact", $dup, "id=" . $h_id[$i]);
            if ($ok) {
              $str_mess .= $h_id[$i] . ", ";
            }
          }
          $mess .= substr($str_mess, 0, - 2) . "</strong><br>";
          $err = $func->html_mess($mess);
        break;
      }
    }
    $p = ((int) $vnT->input['p']) ? $p = $vnT->input['p'] : 1;
    $n = ($conf['record']) ? $conf['record'] : 30;
    $query = $DB->query("SELECT id FROM custom_booking  ");
    $totals = intval($DB->num_rows($query));
    $num_pages = ceil($totals / $n);
    if ($p > $num_pages)
      $p = $num_pages;
    if ($p < 1)
      $p = 1;
    $start = ($p - 1) * $n;
    $nav = $func->paginate($totals, $n, $ext, $p);
    $table['link_action'] = $this->linkUrl . "&sub=manage";
    $table['title'] = array(
      'check_box' => "<input type=\"checkbox\" name=\"checkall\" id=\"checkall\" class=\"checkbox\" />|5%|center" , 
      'name' => $vnT->lang['full_name'] . " |20%|left" , 
      'date_booking' => "Thông tin |20%|left" , 
      'datesubmit' => "Ngày gửi|15%|center" , 
      'status' => $vnT->lang['status'] . "|15%|center" ,
      'action' => "Action|10%|center");
    $sql = "SELECT * FROM custom_booking ORDER BY id DESC LIMIT $start,$n";
    //print "sql = ".$sql."<br>";
    $result = $DB->query($sql);
    if ($DB->num_rows($result)) {
      $row = $DB->get_array($result);
      for ($i = 0; $i < count($row); $i ++) {
        $row_info = $this->render_row($row[$i], $lang);
        $row_field[$i] = $row_info;
        $row_field[$i]['stt'] = ($i + 1);
        $row_field[$i]['row_id'] = "row_" . $row[$i]['id'];
        $row_field[$i]['ext'] = "";
      }
      $table['row'] = $row_field;
    } else {
      $table['row'] = array();
      $table['extra'] = "<div align=center class=font_err >" . $vnT->lang['no_have_contact'] . "</div>";
    }
    $table['button'] = '<input type="button" name="btnHidden" value=" ' . $vnT->lang['hidden'] . ' " class="button" onclick="do_submit(\'do_hidden\')">&nbsp;';
    $table['button'] .= '<input type="button" name="btnDisplay" value=" ' . $vnT->lang['display'] . ' " class="button" onclick="do_submit(\'do_display\')">&nbsp;';
    $table['button'] .= '<input type="button" name="btnDel" value=" ' . $vnT->lang['delete'] . ' " class="button" onclick="del_selected(\'' . $this->linkUrl . '&sub=del&ext=' . $ext_page . '\')">';
    $table_list = $func->ShowTable($table);
    $data['table_list'] = $table_list;
    $data['totals'] = $totals;
    $data['err'] = $err;
    $data['nav'] = $nav;
    $this->skin->assign('data', $data);
    $this->skin->parse("manage");
    return $this->skin->text("manage");
  }
}
$vntModule = new vntModule();
?>

