	/************************************************************************************************************
	(C) www.dhtmlgoodies.com, April 2006
	
	This is a script from www.dhtmlgoodies.com. You will find this and a lot of other scripts at our website.	
	
	Terms of use:
	You are free to use this script as long as the copyright message is kept intact. However, you may not
	redistribute, sell or repost it without our permission.
	
	Thank you!
	
	www.dhtmlgoodies.com
	Alf Magne Kalleland
	
	************************************************************************************************************/	

	var ajaxBox_offsetX = 0;
	var ajaxBox_offsetY = 0;
	var ajax_list_externalFile = 'modules/product_ad/ajax/ajax_list_customer.php';	// Path to external file
	var minimumLettersBeforeLookup = 1;	// Number of letters entered before a lookup is performed.
	
	var ajax_list_objects = new Array();
	var ajax_list_cachedLists = new Array();
	var ajax_list_activeInput = false;
	var ajax_list_activeItem;
	var ajax_list_optionDivFirstItem = false;
	var ajax_list_currentLetters = new Array();
	var ajax_optionDiv = false;
	var ajax_optionDiv_iframe = false;

	var ajax_list_MSIE = false;
	if(navigator.userAgent.indexOf('MSIE')>=0 && navigator.userAgent.indexOf('Opera')<0)ajax_list_MSIE=true;
	
	var currentListIndex = 0;
	
	function ajax_getTopPos(inputObj)
	{
		
	  var returnValue = inputObj.offsetTop;
	  while((inputObj = inputObj.offsetParent) != null){
	  	returnValue += inputObj.offsetTop;
	  }
	  return returnValue;
	}
	function ajax_list_cancelEvent()
	{
		return false;
	}
	
	function ajax_getLeftPos(inputObj)
	{
	  var returnValue = inputObj.offsetLeft;
	  while((inputObj = inputObj.offsetParent) != null)returnValue += inputObj.offsetLeft;
	  
	  return returnValue;
	}
	
	function ajax_option_setValue(e,inputObj)
	{
		if(!inputObj)inputObj=this;
		var tmpValue = inputObj.innerHTML;
		if(ajax_list_MSIE)tmpValue = inputObj.innerText;else tmpValue = inputObj.textContent;
		if(!tmpValue)tmpValue = inputObj.innerHTML;
		ajax_list_activeInput.value = tmpValue;
		if(document.getElementById(ajax_list_activeInput.name + '_hidden'))document.getElementById(ajax_list_activeInput.name + '_hidden').value = inputObj.id; 
		ajax_options_hide();
	}
	
	function ajax_options_hide()
	{
		if(ajax_optionDiv)ajax_optionDiv.style.display='none';	
		if(ajax_optionDiv_iframe)ajax_optionDiv_iframe.style.display='none';
	}

	function ajax_options_rollOverActiveItem(item,fromKeyBoard)
	{
		if(ajax_list_activeItem)ajax_list_activeItem.className='optionDiv';
		item.className='optionDivSelected';
		ajax_list_activeItem = item;
		
		if(fromKeyBoard){
			if(ajax_list_activeItem.offsetTop>ajax_optionDiv.offsetHeight){
				ajax_optionDiv.scrollTop = ajax_list_activeItem.offsetTop - ajax_optionDiv.offsetHeight + ajax_list_activeItem.offsetHeight + 2 ;
			}
			if(ajax_list_activeItem.offsetTop<ajax_optionDiv.scrollTop)
			{
				ajax_optionDiv.scrollTop = 0;	
			}
		}
	}
	
	function ajax_option_list_buildList(letters,paramToExternalFile)
	{
		
		ajax_optionDiv.innerHTML = '';
		ajax_list_activeItem = false;
		if(ajax_list_cachedLists[paramToExternalFile][letters.toLowerCase()].length<=1){
			ajax_options_hide();
			return;			
		}
		
		
		
		ajax_list_optionDivFirstItem = false;
		var optionsAdded = false;
		for(var no=0;no<ajax_list_cachedLists[paramToExternalFile][letters.toLowerCase()].length;no++){
			if(ajax_list_cachedLists[paramToExternalFile][letters.toLowerCase()][no].length==0)continue;
			optionsAdded = true;
			var div = document.createElement('DIV');
			var items = ajax_list_cachedLists[paramToExternalFile][letters.toLowerCase()][no].split(/###/gi);
			
			if(ajax_list_cachedLists[paramToExternalFile][letters.toLowerCase()].length==1 && ajax_list_activeInput.value == items[0]){
				ajax_options_hide();
				return;						
			}
			
			
			div.innerHTML = items[items.length-1];
			div.id = items[0];
			div.className='optionDiv';
			div.onmouseover = function(){ ajax_options_rollOverActiveItem(this,false) }
			div.onclick = ajax_option_setValue;
			if(!ajax_list_optionDivFirstItem)ajax_list_optionDivFirstItem = div;
			ajax_optionDiv.appendChild(div);
		}	
		if(optionsAdded){
			ajax_optionDiv.style.display='block';
			if(ajax_optionDiv_iframe)ajax_optionDiv_iframe.style.display='';
			ajax_options_rollOverActiveItem(ajax_list_optionDivFirstItem,true);
		}
					
	}
	
	function ajax_option_list_showContent(ajaxIndex,inputObj,paramToExternalFile,whichIndex)
	{
		if(whichIndex!=currentListIndex)return;
		var letters = inputObj.value;
		var content = ajax_list_objects[ajaxIndex].response;
		var elements = content.split('|');
		ajax_list_cachedLists[paramToExternalFile][letters.toLowerCase()] = elements;
		ajax_option_list_buildList(letters,paramToExternalFile);
		
	}
	
	function ajax_option_resize(inputObj)
	{
		ajax_optionDiv.style.top = (ajax_getTopPos(inputObj) + inputObj.offsetHeight + ajaxBox_offsetY) + 'px';
		ajax_optionDiv.style.left = (ajax_getLeftPos(inputObj) + ajaxBox_offsetX) + 'px';
		if(ajax_optionDiv_iframe){
			ajax_optionDiv_iframe.style.left = ajax_optionDiv.style.left;
			ajax_optionDiv_iframe.style.top = ajax_optionDiv.style.top;			
		}		
		
	}
	
	function ajax_showOptions(inputObj,paramToExternalFile,e)
	{
		
		if(e.keyCode==13 || e.keyCode==9)return;
		if(ajax_list_currentLetters[inputObj.name]==inputObj.value)return;
		if(!ajax_list_cachedLists[paramToExternalFile])ajax_list_cachedLists[paramToExternalFile] = new Array();
		ajax_list_currentLetters[inputObj.name] = inputObj.value;		
		if(!ajax_optionDiv){
			ajax_optionDiv = document.createElement('DIV');
			ajax_optionDiv.id = 'ajax_listOfOptions';	
			document.body.appendChild(ajax_optionDiv);
			
			if(ajax_list_MSIE){
				ajax_optionDiv_iframe = document.createElement('IFRAME');
				ajax_optionDiv_iframe.border='0';
				ajax_optionDiv_iframe.style.width = ajax_optionDiv.clientWidth + 'px';
				ajax_optionDiv_iframe.style.height = ajax_optionDiv.clientHeight + 'px';
				ajax_optionDiv_iframe.id = 'ajax_listOfOptions_iframe';
				
				document.body.appendChild(ajax_optionDiv_iframe);
			}
			
			var allInputs = document.getElementsByTagName('INPUT');
			for(var no=0;no<allInputs.length;no++){
				if(!allInputs[no].onkeyup)allInputs[no].onfocus = ajax_options_hide;
			}			
			var allSelects = document.getElementsByTagName('SELECT');
			for(var no=0;no<allSelects.length;no++){
				allSelects[no].onfocus = ajax_options_hide;
			}

			var oldonkeydown=document.body.onkeydown;
			if(typeof oldonkeydown!='function'){
				document.body.onkeydown=ajax_option_keyNavigation;
			}else{
				document.body.onkeydown=function(){
					oldonkeydown();
				ajax_option_keyNavigation() ;}
			}
			var oldonresize=document.body.onresize;
			if(typeof oldonresize!='function'){
				document.body.onresize=function() {ajax_option_resize(inputObj); };
			}else{
				document.body.onresize=function(){oldonresize();
				ajax_option_resize(inputObj) ;}
			}
				
		}
		
		if(inputObj.value.length<minimumLettersBeforeLookup){
			ajax_options_hide();
			return;
		}
				

		ajax_optionDiv.style.top = (ajax_getTopPos(inputObj) + inputObj.offsetHeight + ajaxBox_offsetY) + 'px';
		ajax_optionDiv.style.left = (ajax_getLeftPos(inputObj) + ajaxBox_offsetX) + 'px';
		if(ajax_optionDiv_iframe){
			ajax_optionDiv_iframe.style.left = ajax_optionDiv.style.left;
			ajax_optionDiv_iframe.style.top = ajax_optionDiv.style.top;			
		}
		
		ajax_list_activeInput = inputObj;
		ajax_optionDiv.onselectstart =  ajax_list_cancelEvent;
		currentListIndex++;
		if(ajax_list_cachedLists[paramToExternalFile][inputObj.value.toLowerCase()]){
			ajax_option_list_buildList(inputObj.value,paramToExternalFile,currentListIndex);			
		}else{
			var tmpIndex=currentListIndex/1;
			ajax_optionDiv.innerHTML = '';
			var ajaxIndex = ajax_list_objects.length;
			ajax_list_objects[ajaxIndex] = new sack();
			var tmp_value = vietdecode_JS(inputObj.value);
			var url = ajax_list_externalFile + '?' + paramToExternalFile + '=1&letters=' + tmp_value.replace(" ","+");
			ajax_list_objects[ajaxIndex].requestFile = url;	// Specifying which file to get
			ajax_list_objects[ajaxIndex].onCompletion = function(){ ajax_option_list_showContent(ajaxIndex,inputObj,paramToExternalFile,tmpIndex); };	// Specify function that will be executed after file has been found
			ajax_list_objects[ajaxIndex].runAJAX();		// Execute AJAX function		
		}
		
			
	}
	
	function ajax_option_keyNavigation(e)
	{
		if(document.all)e = event;
		
		if(!ajax_optionDiv)return;
		if(ajax_optionDiv.style.display=='none')return;
		
		if(e.keyCode==38){	// Up arrow
			if(!ajax_list_activeItem)return;
			if(ajax_list_activeItem && !ajax_list_activeItem.previousSibling)return;
			ajax_options_rollOverActiveItem(ajax_list_activeItem.previousSibling,true);
		}
		
		if(e.keyCode==40){	// Down arrow
			if(!ajax_list_activeItem){
				ajax_options_rollOverActiveItem(ajax_list_optionDivFirstItem,true);
			}else{
				if(!ajax_list_activeItem.nextSibling)return;
				ajax_options_rollOverActiveItem(ajax_list_activeItem.nextSibling,true);
			}
		}
		
		if(e.keyCode==13 || e.keyCode==9){	// Enter key or tab key
			if(ajax_list_activeItem && ajax_list_activeItem.className=='optionDivSelected')ajax_option_setValue(false,ajax_list_activeItem);
			if(e.keyCode==13)return false; else return true;
		}
		if(e.keyCode==27){	// Escape key
			ajax_options_hide();			
		}
	}
	
	
	document.documentElement.onclick = autoHideList;
	
	function autoHideList(e)
	{
		if(document.all)e = event;
		
		if (e.target) source = e.target;
			else if (e.srcElement) source = e.srcElement;
			if (source.nodeType == 3) // defeat Safari bug
				source = source.parentNode;		
		if(source.tagName.toLowerCase()!='input' && source.tagName.toLowerCase()!='textarea')ajax_options_hide();
		
	}
	
	function vietdecode_JS(value) {		
		value = value.replace("ấ", "a");
		value = value.replace("ầ", "a");
		value = value.replace("ẩ", "a");
		value = value.replace("ẫ", "a");
		value = value.replace("ậ", "a");
		
		value = value.replace("Ấ", "A");
		value = value.replace("Ầ", "A");
		value = value.replace("Ẩ", "A");
		value = value.replace("Ẫ", "A");
		value = value.replace("Ậ", "A");
		
		value = value.replace("ắ", "a");
		value = value.replace("ằ", "a");
		value = value.replace("ẳ", "a");
		value = value.replace("ẵ", "a");
		value = value.replace("ặ", "a");
		
		value = value.replace("Ắ", "A");
		value = value.replace("Ằ", "A");
		value = value.replace("Ẳ", "A");
		value = value.replace("Ẵ", "A");
		value = value.replace("Ặ", "A");
		
		value = value.replace("á", "a");
		value = value.replace("à", "a");
		value = value.replace("ả", "a");
		value = value.replace("ã", "a");
		value = value.replace("ạ", "a");
		value = value.replace("â", "a");
		value = value.replace("ă", "a");
		
		value = value.replace("Á", "A");
		value = value.replace("À", "A");
		value = value.replace("Ả", "A");
		value = value.replace("Ã", "A");
		value = value.replace("Ạ", "A");
		value = value.replace("Â", "A");
		value = value.replace("Ă", "A");
		
		value = value.replace("ế", "e");
		value = value.replace("ề", "e");
		value = value.replace("ể", "e");
		value = value.replace("ễ", "e");
		value = value.replace("ệ", "e");
		
		value = value.replace("Ế", "E");
		value = value.replace("Ề", "E");
		value = value.replace("Ể", "E");
		value = value.replace("Ễ", "E");
		value = value.replace("Ệ", "E");
		
		value = value.replace("é", "e");
		value = value.replace("è", "e");
		value = value.replace("ẻ", "e");
		value = value.replace("ẽ", "e");
		value = value.replace("ẹ", "e");
		value = value.replace("ê", "e");
		
		value = value.replace("É", "E");
		value = value.replace("È", "E");
		value = value.replace("Ẻ", "E");
		value = value.replace("Ẽ", "E");
		value = value.replace("Ẹ", "E");
		value = value.replace("Ê", "E");
		
		value = value.replace("í", "i");
		value = value.replace("ì", "i");
		value = value.replace("ỉ", "i");
		value = value.replace("ĩ", "i");
		value = value.replace("ị", "i");
		
		value = value.replace("Í", "I");
		value = value.replace("Ì", "I");
		value = value.replace("Ỉ", "I");
		value = value.replace("Ĩ", "I");
		value = value.replace("Ị", "I");
		
		value = value.replace("ố", "o");
		value = value.replace("ồ", "o");
		value = value.replace("ổ", "o");
		value = value.replace("ỗ", "o");
		value = value.replace("ộ", "o");
		
		value = value.replace("Ố", "O");
		value = value.replace("Ồ", "O");
		value = value.replace("Ổ", "O");
		value = value.replace("Ô", "O");
		value = value.replace("Ộ", "O");
		
		value = value.replace("ớ", "o");
		value = value.replace("ờ", "o");
		value = value.replace("ở", "o");
		value = value.replace("ỡ", "o");
		value = value.replace("ợ", "o");
		
		value = value.replace("Ớ", "O");
		value = value.replace("Ờ", "O");
		value = value.replace("Ở", "O");
		value = value.replace("Ỡ", "O");
		value = value.replace("Ợ", "O");
		
		value = value.replace("ứ", "u");
		value = value.replace("ừ", "u");
		value = value.replace("ử", "u");
		value = value.replace("ữ", "u");
		value = value.replace("ự", "u");
		
		value = value.replace("Ứ", "U");
		value = value.replace("Ừ", "U");
		value = value.replace("Ử", "U");
		value = value.replace("Ữ", "U");
		value = value.replace("Ự", "U");
		
		value = value.replace("ý", "y");
		value = value.replace("ỳ", "y");
		value = value.replace("ỷ", "y");
		value = value.replace("ỹ", "y");
		value = value.replace("ỵ", "y");
		
		value = value.replace("Ý", "Y");
		value = value.replace("Ỳ", "Y");
		value = value.replace("Ỷ", "Y");
		value = value.replace("Ỹ", "Y");
		value = value.replace("Ỵ", "Y");
		
		value = value.replace("Đ", "D");
		value = value.replace("đ", "d");
		
		value = value.replace("ó", "o");
		value = value.replace("ò", "o");
		value = value.replace("ỏ", "o");
		value = value.replace("õ", "o");
		value = value.replace("ọ", "o");
		value = value.replace("ô", "o");
		value = value.replace("ơ", "o");
		
		value = value.replace("Ó", "O");
		value = value.replace("Ò", "O");
		value = value.replace("Ỏ", "O");
		value = value.replace("Õ", "O");
		value = value.replace("Ọ", "O");
		value = value.replace("Ô", "O");
		value = value.replace("Ơ", "O");
		
		value = value.replace("ú", "u");
		value = value.replace("ù", "u");
		value = value.replace("ủ", "u");
		value = value.replace("ũ", "u");
		value = value.replace("ụ", "u");
		value = value.replace("ư", "u");
		
		value = value.replace("Ú", "U");
		value = value.replace("Ù", "U");
		value = value.replace("Ủ", "U");
		value = value.replace("Ũ", "U");
		value = value.replace("Ụ", "U");
		value = value.replace("Ư", "U");
		
		return value;
	}