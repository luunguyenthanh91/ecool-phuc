function do_submit_move(action) {
	document.manage.do_action.value=action;
	cat_id = document.getElementById('cat_id').value;
	action_form = document.manage.action;
	if (selected_item()){
		action_form = action_form+'&cat_id='+cat_id;
		document.manage.action = action_form;
		document.manage.submit();
	}
}
function do_movecat(action){
  cat_chose = getobj('cat_chose').value;
	if (cat_chose==0){
		alert('Vui lòng chọn danh mục cần chuyển đến');	
	}else{
		document.manage.do_action.value=cat_chose;
		if (selected_item()){
			document.manage.submit();
		}
	}
}
function LoadAjax(doAction,str_value,ext_display) {
	var arrValue = str_value.split(",");
	var key='do';
	var value =doAction ;
	for (i=0;i<arrValue.length;i++){
		tmp = arrValue[i].split("=");
		key=key+','+tmp[0];
		value=value+','+tmp[1];
	}
	url = 'modules/product_ad/ajax/ajax_product.php';
	sendReq(url,key ,value,ext_display);
	return false;
}
function checkall()	{
	for ( i=0;i < document.manage.elements.length ; i++ ){
		if (document.manage.elements[i].type=="checkbox" && document.manage.elements[i].name!="all"){
			row_id = 'row_'+document.manage.elements[i].value;
		}else{
			row_id="";
		}
		if ( document.manage.all.checked==true ){
			document.manage.elements[i].checked = true;
			if (row_id!="" ){
				getobj(row_id).className = "row_select";
			}
		}
		else{
			document.manage.elements[i].checked  = false;
			if (row_id!="" ){
				if (i%2==0)
					getobj(row_id).className = "row1";
				else
					getobj(row_id).className = "row";
			}
		}
	}
}
function format_number (num) {
	num = num.toString().replace(/\$|\,/g,'');
	if(isNaN(num))
		num = "0";
	sign = (num == (num = Math.abs(num)));
	num = Math.round(num*100+0.50000000001);
	num = Math.round(num/100).toString();
	for (var i = 0; i < Math.floor((num.length-(1+i))/3); i++)
		num = num.substring(0,num.length-(4*i+3))+','+ num.substring(num.length-(4*i+3));
	return (((sign)?'':'-') + num);
}


function numberOnly (myfield, e){
	var key,keychar;
	if (window.event){key = window.event.keyCode}
	else if (e){key = e.which}
	else{return true}
	keychar = String.fromCharCode(key);
	if ((key==null) || (key==0) || (key==8) || (key==9) || (key==13) || (key==27)){return true}
	else if (("0123456789").indexOf(keychar) > -1){return true}
	return false;
}

function numberFormat ( number, decimals, dec_point, thousands_sep )
{
	var n = number, prec = decimals;
	n = !isFinite(+n) ? 0 : +n;
	prec = !isFinite(+prec) ? 0 : Math.abs(prec);
	var sep = (typeof thousands_sep == "undefined") ? '.' : thousands_sep;
	var dec = (typeof dec_point == "undefined") ? ',' : dec_point;
	var s = (prec > 0) ? n.toFixed(prec) : Math.round(n).toFixed(prec); //fix for IE parseFloat(0.55).toFixed(0) = 0;
	var abs = Math.abs(n).toFixed(prec);
	var _, i;
	if (abs >= 1000) {
		_ = abs.split(/\D/);
		i = _[0].length % 3 || 3;
		_[0] = s.slice(0,i + (n < 0)) +
			_[0].slice(i).replace(/(\d{3})/g, sep+'$1');
		s = _.join(dec);
	} else {
		s = s.replace(',', dec);
	}
	return s;
}


function mixMoney(myfield){
	var thousands_sep = ',';
	myfield.value = numberFormat(parseInt(myfield.value.replace(/,/gi, '')),0,'',thousands_sep);
}

(function (jQuery) {
	jQuery.fn.clickoutside = function (callback) {
		var outside = 1,
			self = $(this);
		self.cb = callback;
		this.click(function () {
			outside = 0
		});
		$(document).click(function (event) {
			if (event.button == 0) {
				outside && self.cb();
				outside = 1
			}
		});
		return $(this)
	}
})(jQuery);




vnTProduct = {

	loadProBelong:function(belong_product){

		var mydata =  "belong_product="+belong_product  ;
		$.ajax({
			async: true,
			url:  "modules/product_ad/remote.php?do=belong" ,
			type: 'POST',
			data: mydata ,
			success: function (html) {
				$("#ext_belong").html(html) ;
			}
		})
	},

	callback_Product:function(id,title,maso,src){
		var ok_show = 1 ;
		$(".h_pro").each( function() {
			var did =  $(this).attr( "value" );
			if(id == did) {
				ok_show = 0 ;
				return false;
			}
		});

		if(ok_show){
			var html = '<tr id="pro_'+id+'"><td align="center" >'+id+'<input type="hidden" value="'+id+'" name="h_pro[]" class="h_pro"></td><td align="center"><img src="'+src+'"  /></td><td ><b>'+title+'</b><br>MS : '+maso+'</td> <td align="center" ><a title="Xem chi tiết " target="_blank" href="?mod=product&act=product&sub=edit&id='+id+'"><img width="15" src="'+DIR_IMAGE+'/view.gif"></a>&nbsp;<a onclick="vnTProduct.del_Item(\'pro\','+id+')" title="Xóa" href="javascript:void(0)"><img alt="Delete " src="'+DIR_IMAGE+'/delete.gif"></a></td></tr>';
			$("#item_pro").prepend(html);
			$("#pro_"+id).fadeOut('slow').fadeIn('slow');
			/*tb_remove();*/
		}else{
			alert('Phụ kiện này đã tồn tại ')	;
		}

	},
	del_Item:function(type,id){

		if (confirm('Bạn có chắc muốn xóa ?')) {
			$("#"+type+"_"+id).remove();
		}
		else {
			alert ('Phew~');
		}
	},

	init:function () {

		$(".boxDropDown a").click(function () {
			$(this).next().slideToggle(0, function () {
				if ($(this).is(":hidden")) {
					$(this).prev().removeClass("active")
				} else {
					$(this).prev().addClass("active")
				}
			})
		});

		$(".boxDropDown a").clickoutside(function () {
			$(this).next().hide();
		});


		$(".list-choose").find("li").click(function(){
			var obj = $(this).closest('.list-choose') ;
			data = [];
			if($(this).hasClass("checked")) {
				$(this).removeAttr("class");
			} else {
				$(this).addClass("checked");
			}
			obj.find('li').each(function(i, e){
				if($(this).hasClass("checked")) {
					id = $(this).attr('rel');
					data.push(id);
				}
			});
			data.join();
			obj.find('.input-value').val(data);
			
		});

	}
};

$(document).ready(function () {
	vnTProduct.init();
});