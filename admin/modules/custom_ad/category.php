<?php
/*================================================================================*\
|| 							Name code : cat_custom.php 		 		            	  ||
||  				Copyright @2008 by Thai Son - CMS vnTRUST                     ||
\*================================================================================*/
/**
 * @version : 2.0
 * @date upgrade : 09/01/2009 by Thai Son
 **/
if (! defined('IN_vnT')){
  die('Hacking attempt!');
}
//load Model
include(dirname(__FILE__) . "/functions.php");

class vntModule extends Model
{
  var $output = "";
  var $skin = "";
  var $linkUrl = "";
  var $module = "custom";
  var $action = "category";

  function vntModule (){
    global $Template, $vnT, $func, $DB, $conf;
    $this->skin = new XiTemplate(DIR_MODULE . DS . $this->module."_ad" . DS .'html'. DS . $this->action.".tpl");
    $this->skin->assign('LANG', $vnT->lang);
    $this->skin->assign('DIR_IMAGE', $vnT->dir_images);
    $lang = ($vnT->input['lang']) ? $lang = $vnT->input['lang'] : $func->get_lang_default();
    $this->linkUrl = "?mod=" . $this->module . "&act=" . $this->action . "&lang=" . $lang;
    switch ($vnT->input['sub']) {
      case 'add':
        $nd['f_title'] = 'Thêm danh mục';
        $nd['content'] = $this->do_Add($lang);
        break;
      case 'edit':
        $nd['f_title'] = 'Cập nhật danh mục';
        $nd['content'] = $this->do_Edit($lang);
        break;
      case 'move':
        $nd['f_title'] = $vnT->lang["move_cat"];
        $nd['content'] = $this->do_Move($lang);
        break;
      case 'del':
        $this->do_Del($lang);
        break;
      default:
        $nd['f_title'] = $vnT->lang['manage_cat_custom'];
        $nd['content'] = $this->do_Manage($lang);
        break;
    }
    $nd['menu'] = $func->getToolbar($this->module, $this->action, $lang);
    $nd['row_lang'] = $func->html_lang("?mod=" . $this->module . "&act=" . $this->action, $lang);
    $Template->assign("data", $nd);
    $Template->parse("box_main");
    $vnT->output .= $Template->text("box_main");
  }
  function do_Add ($lang){
    global $vnT, $func, $DB, $conf;
    $err = "";
    $dir = $vnT->func->get_dir_upload_module($this->module,$vnT->setting['folder_upload']);
    if ($vnT->input['do_submit'] == 1){
      $data = $_POST;
      $cat_name = $vnT->input['cat_name'];
      $parentid = $vnT->input["cat_id"];
      $picture =  $vnT->input['picture'];
      // insert CSDL
      if (empty($err)) {
        $cot['parentid'] = $parentid;
        $cot['picture'] = $picture;
        $ok = $DB->do_insert("custom_category", $cot);
        if ($ok) {
          $cat_id = $DB->insertid();
          //update cat content
          $cot_d['cat_id'] = $cat_id;
          $cot_d['cat_name'] = $cat_name;
          $cot_d['slogan'] = $vnT->input['slogan'];
          $cot_d['description'] = $DB->mySQLSafe($_POST['description']);
          $cot_d['friendly_url'] = (trim($vnT->input['friendly_url'])) ? trim($vnT->input['friendly_url']) :  $func->make_url($_POST['cat_name']);
          $cot_d['friendly_title'] = (trim($vnT->input['friendly_title'])) ? trim($vnT->input['friendly_title']) :  $func->utf8_to_ascii($cat_name);
          $cot_d['metakey'] = (trim($vnT->input['metakey'])) ? trim($vnT->input['metakey']) :  $cat_name ;
          $cot_d['metadesc'] = (trim($vnT->input['metadesc'])) ? trim($vnT->input['metadesc']) :  $func->txt_HTML($_POST['description']);
          $query_lang = $DB->query("select name from language ");
          while ($row_lang = $DB->fetch_row($query_lang)){
            $cot_d['lang'] = $row_lang['name'];
            $DB->do_insert("custom_category_desc", $cot_d);
          }
          //update catcode				
          $dup['cat_code'] = $this->get_catCode($parentid, $cat_id);
          $DB->do_update("custom_category", $dup, "cat_id=$cat_id");
          //build seo_url
          $seo['sub'] = 'add';
          $seo['modules'] = $this->module;
          $seo['action'] = $this->action;
          $seo['name_id'] = "catID";
          $seo['item_id'] = $cat_id;
          $seo['friendly_url'] = $cot_d['friendly_url'] ;
          $seo['query_string'] = "mod:".$this->module."|act:".$this->action."|catID:".$cat_id;
          $seo['lang'] = $lang;
          $res_seo = $func->build_seo_url($seo);
          if($res_seo['existed']==1){
            $DB->query("UPDATE custom_category_desc SET friendly_url='".$res_seo['friendly_url']."' 
                        WHERE cat_id=".$cat_id);
          }
          //xoa cache				
          $func->clear_cache();
          //insert adminlog
          $func->insertlog("Add", $_GET['act'], $cat_id);
          $mess = $vnT->lang['add_success'];
          $url = $this->linkUrl . "&sub=add";
          $func->html_redirect($url, $mess);
        } else {
          $err = $func->html_err($vnT->lang['add_failt'] . $DB->debug());
        }
      }
    }
    $data['listcat'] = $this->Get_Cat($vnT->input['cat_id'], $lang, "");
    $data["html_content"] = $vnT->editor->doDisplay('description', $vnT->input['description'], '100%', '300', "Normal");
    $data['link_upload'] = '?mod=media&act=popup_media&module='.$this->module.'&folder='.$this->module.'/'.$dir.'&obj=picture&type=image&TB_iframe=true&width=900&height=474';
    $data['err'] = $err;
    $data['link_action'] = $this->linkUrl . "&sub=add";
    /*assign the array to a template variable*/
    $this->skin->assign('data', $data);
    $this->skin->parse("edit");
    return $this->skin->text("edit");
  }
  function do_Edit ($lang){
    global $vnT, $func, $DB, $conf;
    $id = (int) $vnT->input['id'];
    $dir = $vnT->func->get_dir_upload_module($this->module,$vnT->setting['folder_upload']);
    if ($vnT->input['do_submit']){
      $data = $_POST;
      $cat_name = $vnT->input['cat_name'];
      $parentid = $vnT->input["cat_id"];
      $picture =  $vnT->input['picture'];
      // Check for Error
      if ($parentid == $id) $err = $func->html_err("Danh mục cha không hợp lệ");
      if (empty($err)){
        $cot['parentid'] = $parentid;
        $cot['picture'] = $picture;
        $ok = $DB->do_update("custom_category", $cot, "cat_id=$id");
        if ($ok){
          //update cat content
          $cot_d['cat_name'] = $cat_name;
          $cot_d['slogan'] = $vnT->input['slogan'];
          $cot_d['description'] = $DB->mySQLSafe($_POST['description']);
          $cot_d['friendly_url'] = (trim($vnT->input['friendly_url'])) ? trim($vnT->input['friendly_url']) :  $func->make_url($_POST['cat_name']);
          $cot_d['friendly_title'] = (trim($vnT->input['friendly_title'])) ? trim($vnT->input['friendly_title']) :  $func->utf8_to_ascii($cat_name);
          $cot_d['metakey'] = (trim($vnT->input['metakey'])) ? trim($vnT->input['metakey']) :  $cat_name ;
          $cot_d['metadesc'] = (trim($vnT->input['metadesc'])) ? trim($vnT->input['metadesc']) :  $func->txt_HTML($_POST['description']);
          $DB->do_update("custom_category_desc", $cot_d, "cat_id=$id and lang='{$lang}'");
          //update catcode
          $this->rebuild_cat_code($parentid,$id);
          //rebuild cat_list
          $this->rebuild_cat_list($id);
          //build seo_url
          $seo['sub'] = 'edit';
          $seo['modules'] = $this->module;
          $seo['action'] = $this->action;
          $seo['name_id'] = "catID";
          $seo['item_id'] = $id;
          $seo['friendly_url'] = $cot_d['friendly_url'] ;
          $seo['query_string'] = "mod:".$this->module."|act:".$this->action."|catID:".$id;
          $seo['lang'] = $lang;
          $res_seo = $func->build_seo_url($seo);
          if($res_seo['existed']==1){
            $DB->query("UPDATE custom_category_desc SET friendly_url='".$res_seo['friendly_url']."' 
                        WHERE lang='".$lang."' AND cat_id=".$id) ;
          }
          $func->clear_cache();
          $func->insertlog("Edit", $_GET['act'], $id);
          $err = $vnT->lang["edit_success"];
          $url = $this->linkUrl . "&sub=edit&id=$id";
          $func->html_redirect($url, $err);
        } else
          $err = $func->html_err($vnT->lang["edit_failt"] . $DB->debug());
      }
    }
    $query = $DB->query("SELECT * FROM custom_category n , custom_category_desc nd 
												WHERE n.cat_id=nd.cat_id AND nd.lang='$lang' AND n.cat_id=$id");
    if ($data = $DB->fetch_row($query)) {
      if ($data['picture']) {
        $data['pic'] = $this->get_picture($data['picture'],100) . "  <a href=\"javascript:del_picture('picture')\" class=\"del\">Xóa</a>";
        $data['style_upload'] = "style='display:none' ";
      } else {
        $data['pic'] = "";
      }
    } else {
      $mess = $vnT->lang['not_found'] . " ID : " . $id;
      $url = $this->linkUrl;
      $func->html_redirect($url, $mess);
    }
    $data['listcat'] = $this->Get_Cat($data['parentid'], $lang, "");
    $data["html_content"] = $vnT->editor->doDisplay('description', $data['description'], '100%', '300', "Normal");
    $data['link_upload'] = '?mod=media&act=popup_media&module='.$this->module.'&folder='.$this->module.'/'.$dir.'&obj=picture&type=image&TB_iframe=true&width=900&height=474';
    $data['err'] = $err;
    $data['link_action'] = $this->linkUrl . "&sub=edit&id=$id";
    $this->skin->assign('data', $data);
    $this->skin->parse("edit");
    return $this->skin->text("edit");
  }
  function do_Del ($lang){
    global $func, $DB, $conf, $vnT;
    $id = (int) $vnT->input['id'];
    $ext = $vnT->input["ext"];
    $del = 0;
    $qr = "";
    if ($id != 0){
      $ids = $id;
    }
    if (isset($vnT->input["del_id"])) {
      $ids = implode(',', $vnT->input["del_id"]);
    }
    $res = $DB->query("SELECT cat_id FROM custom_category WHERE cat_id IN (" . $ids . ") ");
    if ($DB->num_rows($res)){
      $DB->query("UPDATE custom_category_desc SET display=-1 
                  WHERE cat_id IN (" . $ids . ") AND lang='".$lang."' ");
      $this->do_DeleteSub($ids,$lang); 
      //insert RecycleBin
      $rb_log['module'] = $this->module;
      $rb_log['action'] = $this->action;
      $rb_log['tbl_data'] = "custom_category_desc";
      $rb_log['name_id'] = "cat_id";
      $rb_log['item_id'] = $ids;
      $rb_log['lang'] = $lang;
      $func->insertRecycleBin($rb_log);
      //build seo_url
      $seo['sub'] = 'del';
      $seo['modules'] = $this->module;
      $seo['action'] = $this->action;
      $seo['item_id'] = $ids;
      $seo['lang'] = $lang;
      $res_seo = $func->build_seo_url($seo);
      $mess = $vnT->lang["del_success"];
      //xoa cache
      $func->clear_cache();
    } else {
      $mess = $vnT->lang["del_failt"];
    }
    $ext_page = str_replace("|", "&", $ext);
    $url = $this->linkUrl . "&{$ext_page}";
    $func->html_redirect($url, $mess);
  }
  function render_row ($row_info, $lang){
    global $func, $DB, $conf, $vnT;
    $row = $row_info;
    $id = $row['cat_id'];
    $row_id = "row_" . $id;
    $output['row_id'] = $row_id;
    $output['check_box'] = vnT_HTML::checkbox("del_id[]", $id, 0, " ");
    $link_edit = $this->linkUrl . '&sub=edit&id=' . $id . '&ext=' . $row['ext_page'];
    $link_del = "javascript:del_item('" . $this->linkUrl . "&sub=del&id=" . $id . "&ext=" . $row['ext_page'] . "')";
    $output['order'] = $row['ext']."<input name=\"txt_Order[{$id}]\" type=\"text\" size=\"2\" maxlength=\"2\" style=\"text-align:center\" value=\"{$row['cat_order']}\"  onkeypress=\"return is_num(event,'txtOrder')\" onchange='javascript:do_check($id)' />";
    if ($row['picture'])   {
      $output['picture'] =  $this->get_picture($row['picture'],50);
    } else {
      $output['picture'] = "No image";
    }
    $cat_name = $func->HTML($row['cat_name']) ;
    if ($row['ext']) {
      $output['cat_name'] = $row['ext'] . "&nbsp;<a href='" . $link_edit . "'>" . $cat_name."</a>";
      $output['is_focus'] = '---';
    } else {
      $output['cat_name'] = "<a href='" . $link_edit . "'><strong>" . $cat_name . "</strong></a>";
      $output['is_focus'] = vnT_HTML::list_yesno("is_focus[{$id}]", $row['is_focus'], "onchange='javascript:do_check($id)'");
    }
    $output['link_cat'] =  $row['friendly_url'].".html";
    $link_display = $this->linkUrl.$row['ext_link'];
    if ($row['display'] == 1) {
      $display = "<a href='".$link_display."&do_hidden=$id' title='".$vnT->lang['click_do_hidden']."' ><img src=\"" . $vnT->dir_images . "/dispay.gif\" width=15  /></a>";
    } else {
      $display = "<a href='".$link_display."&do_display=$id'  title='".$vnT->lang['click_do_display']."' ><img src=\"" . $vnT->dir_images . "/nodispay.gif\"  width=15 /></a>";
    }
    $output['action'] = '<input name=h_id[]" type="hidden" value="' . $id . '" />';
    $output['action'] .= '<a href="'.$link_edit.'"><img src="'.$vnT->dir_images.'/edit.gif" alt="Edit"></a>&nbsp;';
    $output['action'] .= $display . '&nbsp;';
    $output['action'] .= '<a href="'.$link_del.'"><img src="'.$vnT->dir_images.'/delete.gif" alt="Delete "></a>';
    return $output;
  }
  function do_Manage ($lang){
    global $vnT, $func, $DB, $conf;
    //update
    if ($vnT->input["do_action"]){
      $func->clear_cache();
      if ($vnT->input["del_id"]) $h_id = $vnT->input["del_id"];
      switch ($vnT->input["do_action"]) {
        case "do_edit":
          if (isset($vnT->input["txt_Order"])) $arr_order = $vnT->input["txt_Order"];
          if (isset($vnT->input["is_focus"])) $arr_focus = $vnT->input["is_focus"];
          $mess .= "- " . $vnT->lang['edit_success'] . " ID: <strong>";
          $str_mess = "";
          for ($i = 0; $i < count($h_id); $i ++) {
            $dup['cat_order'] = $arr_order[$h_id[$i]];
            $dup['is_focus'] = $arr_focus[$h_id[$i]];
            $ok = $DB->do_update("custom_category", $dup, "cat_id=" . $h_id[$i]);
            if ($ok) {
              $str_mess .= $h_id[$i] . ", ";
            }
          }
          $mess .= substr($str_mess, 0, - 2) . "</strong><br>";
          $err = $func->html_mess($mess);
          break;
        case "do_hidden":
          $mess .= "- " . $vnT->lang['hidden_success'] . " ID: <strong>";
          for ($i = 0; $i < count($h_id); $i ++) {
            $dup['display'] = 0;
            $ok = $DB->do_update("custom_category_desc", $dup, "lang='$lang' AND cat_id=" . $h_id[$i]);
            if ($ok) {
              $str_mess .= $h_id[$i] . ", ";
            }
          }
          $mess .= substr($str_mess, 0, - 2) . "</strong><br>";
          $err = $func->html_mess($mess);

          break;
        case "do_display":
          $mess .= "- " . $vnT->lang['display_success'] . "  ID: <strong>";
          for ($i = 0; $i < count($h_id); $i ++) {
            $dup['display'] = 1;
            $ok = $DB->do_update("custom_category_desc", $dup, "lang='$lang' AND cat_id=" . $h_id[$i]);
            if ($ok) {
              $str_mess .= $h_id[$i] . ", ";
            }
          }
          $mess .= substr($str_mess, 0, - 2) . "</strong><br>";
          $err = $func->html_mess($mess);
          break;
      }
    }
    if((int)$vnT->input["do_display"]) {
      $ok = $DB->query("UPDATE custom_category_desc SET display=1 
                        WHERE lang='$lang' AND cat_id=".$vnT->input["do_display"]);
      if($ok){
        $mess .= "- ".$vnT->lang['display_success']."  ID: <strong>".$vnT->input["do_display"] . "</strong><br>";
        $err = $func->html_mess($mess);
      }
      $func->clear_cache();
    }
    if((int)$vnT->input["do_hidden"]) {
      $ok = $DB->query("UPDATE custom_category_desc SET display=0 
                        WHERE lang='$lang' AND cat_id=".$vnT->input["do_hidden"]);
      if($ok){
        $mess .= "- ".$vnT->lang['hidden_success'] . "  ID: <strong>".$vnT->input["do_hidden"] . "</strong><br>";
        $err = $func->html_mess($mess);
      }
      $func->clear_cache();
    }
    $p = ((int) $vnT->input['p']) ? $p = $vnT->input['p'] : 1;
    $n = ($conf['record']) ? $conf['record'] : 30;

    $query = $DB->query("SELECT n.cat_id FROM custom_category n , custom_category_desc nd 
												WHERE n.cat_id=nd.cat_id AND nd.lang='$lang' AND display <> -1 AND n.parentid=0 ");
    $totals = intval($DB->num_rows($query));

    $num_pages = ceil($totals / $n);
    if ($p > $num_pages) $p = $num_pages;
    if ($p < 1) $p = 1;
    $start = ($p - 1) * $n;

    $nav = $func->paginate($totals, $n, $ext, $p);

    $data['link_action'] = $this->linkUrl . "&p=$p";

    $sql = "SELECT * FROM custom_category n , custom_category_desc nd 
						WHERE n.cat_id=nd.cat_id AND nd.lang='$lang' AND display <> -1 AND n.parentid=0
					  ORDER BY cat_order ASC, n.date_post DESC LIMIT $start,$n";
    $result = $DB->query($sql);
    if ($DB->num_rows($result)){
      $i = 0;
      while ($row = $DB->fetch_row($result)){
        $i ++;
        $row['ext'] = "";
        $row['ext_link'] = "&p=".$p ;
        $row_info = $this->render_row($row, $lang);
        $row_info['class'] = ($i % 2) ? "row1" : "row0";
        $this->skin->assign('row', $row_info);
        $this->skin->parse("manage.html_row");
        $n = 1;
        $this->Row_Sub($row['cat_id'], $n, $i, $lang);
      }
    } else {
      $mess = $vnT->lang['no_have_cat_custom'];
      $this->skin->assign('mess', $mess);
      $this->skin->parse("manage.html_row_no");
    }
    $data['button'] = '<input type="button" name="btnHidden" value=" ' . $vnT->lang['hidden'] . ' " class="button" onclick="do_submit(\'do_hidden\')">&nbsp;';
    $data['button'] .= '<input type="button" name="btnDisplay" value=" ' . $vnT->lang['display'] . ' " class="button" onclick="do_submit(\'do_display\')">&nbsp;';
    $data['button'] .= '<input type="button" name="btnEdit" value=" ' . $vnT->lang['update'] . ' " class="button" onclick="do_submit(\'do_edit\')">&nbsp;';
    $data['button'] .= '<input type="button" name="btnDel" value=" ' . $vnT->lang['delete'] . ' " class="button" onclick="del_selected(\'' . $this->linkUrl . '&sub=del&ext=' . $ext_page . '\')">';
    $data['totals'] = $totals;
    $data['err'] = $err;
    $data['nav'] = $nav;
    $this->skin->assign('data', $data);
    $this->skin->parse("manage");
    return $this->skin->text("manage");
  }
  function Row_Sub ($cid, $n, $i, $lang){
    global $func, $DB, $conf, $vnT;
    $textout = "";
    $space = "&nbsp;&nbsp;&nbsp;&nbsp;";
    $n1 = $n;
    $sql = "SELECT * FROM custom_category n ,custom_category_desc nd 
						WHERE n.cat_id=nd.cat_id AND nd.lang='$lang' AND display <> -1
						AND n.parentid='{$cid}' ORDER BY n.cat_order ASC, n.cat_id ASC ";
    $result = $DB->query($sql);
    while ($row = $DB->fetch_row($result)){
      $i ++;
      $row['ext_link'] = "&p=".$_GET['p'] ;
      $row['ext'] = "&nbsp;<img src=\"{$vnT->dir_images}/line3.gif\" align=\"absmiddle\"/>";
      $width="";
      for ($k = 1; $k < $n1; $k ++){
        $width.=$space;
        $row['ext'] = $width . "&nbsp;<img src=\"{$vnT->dir_images}/line3.gif\" align=\"absmiddle\"/>";
      }

      $row_info = $this->render_row($row, $lang);
      $row_info['class'] = ($i % 2) ? "row1" : "row0";
      $this->skin->assign('row', $row_info);
      $this->skin->parse("manage.html_row");

      $n = $n1 + 1;
      $textout .= $this->Row_Sub($row['cat_id'], $n, $i, $lang);
    }

    return $textout;
  }
}

$vntModule = new vntModule();
?>