<?php
/*================================================================================*\
|| 							Name code : attr.php 		 		            	  ||
||  				Copyright @2008 by Thai Son - CMS vnTRUST                     ||
\*================================================================================*/
/**
 * @version : 2.0
 * @date upgrade : 09/01/2009 by Thai Son
 **/
if (! defined('IN_vnT'))
{
  die('Hacking attempt!');
}

//load Model
include(dirname(__FILE__) . "/functions.php");

class vntModule extends Model
{
  var $output = "";
  var $skin = "";
  var $linkUrl = "";
  var $module = "custom";
	var $action = "attr";

  function vntModule (){
    global $Template, $vnT, $func, $DB, $conf;
    //require_once ("function_".$this->module.".php");
    $this->skin = new XiTemplate(DIR_MODULE.DS.$this->module. "_ad" . DS . "html" . DS . $this->action . ".tpl");
    $this->skin->assign('LANG', $vnT->lang);
    $lang = ($vnT->input['lang']) ? $lang = $vnT->input['lang'] : $func->get_lang_default();
    $this->linkUrl = "?mod=".$this->module."&act=".$this->action."&lang=" . $lang;
		//loadSetting($lang);		
		$nd['menu'] = $func->getToolbar($this->module, $this->action, $lang);
    $nd['row_lang'] = $func->html_lang("?mod=" . $this->module . "&act=" . $this->action, $lang);
		
    switch ($vnT->input['sub']){
      case 'add':
        $nd['f_title'] = 'Thêm loại sản phẩm';
        $nd['content'] = $this->do_Add($lang);
        break;
      case 'edit':
        $nd['f_title'] = 'Chỉnh sửa loại sản phẩm';
        $nd['content'] = $this->do_Edit($lang);
        break;
      case 'del':
        $this->do_Del($lang);
        break;
      default:
        $nd['f_title'] = 'Quản lý loại sản phẩm';
        $nd['content'] = $this->do_Manage($lang);
        break;
    } 		
    $Template->assign("data", $nd);
    $Template->parse("box_main");
    $vnT->output .= $Template->text("box_main");
  }
  function do_Add ($lang){
    global $vnT, $func, $DB, $conf;
    $err = "";
	  $dir = $vnT->func->get_dir_upload_module($this->module,$vnT->setting['folder_upload']);
    if ($vnT->input['do_submit'] == 1){
			$data = $_POST;
 			$title = $vnT->input['title'];
			$picture = $vnT->input['picture'];
			// Check for Error
			$res_chk = $DB->query("SELECT attr_id FROM custom_attr_desc WHERE title='{$title}' AND lang='$lang' ");
			if ($check=$DB->fetch_row($res_chk)) $err=$func->html_err("Title existed");
      // insert CSDL
      if (empty($err)){
 				$cot['picture'] = $picture;
        $cot['attr_type'] = $vnT->input['attr_type'];
				$cot['date_post'] = time();
        $ok = $DB->do_insert("custom_attr", $cot);
        if ($ok){
					$attr_id = $DB->insertid();
 					//update content
					$cot_d['attr_id'] = $attr_id;
					$cot_d['title'] = $title ;
					$cot_d['description'] = $func->txt_HTML($_POST['description']);
					$query_lang = $DB->query("select name from language ");
					while ( $row = $DB->fetch_row($query_lang) ) {
						$cot_d['lang'] = $row['name'];
						$DB->do_insert("custom_attr_desc",$cot_d);
					}
					//xoa cache
          $func->clear_cache();
          //insert adminlog
          $func->insertlog("Add", $_GET['act'], $attr_id);
					
          $mess = $vnT->lang['add_success'];
          $url = $this->linkUrl . "&sub=add";
          $func->html_redirect($url, $mess);
        } else {
          $err = $func->html_err($vnT->lang['add_failt'].$DB->debug());
        }
      }
    }
		$data['link_upload'] = '?mod=media&act=popup_media&module='.$this->module.'&folder='.$this->module.'/'.$dir.'&obj=picture&type=image&TB_iframe=true&width=900&height=474';
		$data['List_Attr'] = $this->List_Attr_Type($vnT->input['attr_type']);
    $data['err'] = $err;
    $data['link_action'] = $this->linkUrl . "&sub=add";
    $this->skin->assign('data', $data);
    $this->skin->parse("edit");
    return $this->skin->text("edit");
  }
  function do_Edit ($lang){
    global $vnT, $func, $DB, $conf;
    $id = (int) $vnT->input['id'];
    $dir = $vnT->func->get_dir_upload_module($this->module,$vnT->setting['folder_upload']);	
    if ($vnT->input['do_submit']){
      $data = $_POST;
      $title = $vnT->input['title'];
			$picture = $vnT->input['picture'];
			// Check for Error
			$res_chk = $DB->query("SELECT attr_id FROM custom_attr_desc
                            WHERE title='{$title}' AND lang='$lang' and attr_id<>$id ");
			if ($check=$DB->fetch_row($res_chk)) $err=$func->html_err("Name existed");
      if (empty($err)){
 				$cot['picture']	= $picture;
        $cot['attr_type'] = $vnT->input['attr_type'];
				$cot['date_update']	= time();				 				
				// more here ...
        $ok = $DB->do_update("custom_attr", $cot, "attr_id=$id");
        if ($ok){
					$cot_d['title'] = $title;
					$cot['description'] = $func->txt_HTML($_POST['description']);
					$DB->do_update ("custom_attr_desc",$cot_d,"attr_id=$id and lang='{$lang}'");
 					//xoa cache
          $func->clear_cache(); 
          //insert adminlog
          $func->insertlog("Edit", $_GET['act'], $id);
          
          $err = $vnT->lang["edit_success"];
          $url = $this->linkUrl . "&sub=edit&id=$id";
          $func->html_redirect($url, $err);
        } else
          $err = $func->html_err($vnT->lang["edit_failt"].$DB->debug());
      }
    }
    $query= $DB->query("SELECT * FROM custom_attr n, custom_attr_desc nd
			                  WHERE n.attr_id=nd.attr_id AND nd.lang='$lang' AND n.attr_id=$id");
    if ($data = $DB->fetch_row($query)){
			if ($data['picture']) { 	
        $data['pic'] = get_pic_custom($data['picture'],100)." <a href=\"javascript:del_picture('picture')\" class=\"del\">Xóa</a>";
				$data['style_upload'] = "style='display:none' ";
      } else {
        $data['pic'] = "";
      }
			$cot['description'] = $func->txt_HTML($_POST['description']);
    }else{
      $mess = $vnT->lang['not_found'] . " ID : " . $id;
      $url = $this->linkUrl;
      $func->html_redirect($url, $mess);
    }
    $data['List_Attr'] = $this->List_Attr_Type($data['attr_type']);
 		$data['link_upload'] = '?mod=media&act=popup_media&module='.$this->module.'&folder='.$this->module.'/'.$dir.'&obj=picture&type=image&TB_iframe=true&width=900&height=474';
    $data['err'] = $err;
    $data['link_action'] = $this->linkUrl . "&sub=edit&id=$id";
    
    /*assign the array to a template variable*/
    $this->skin->assign('data', $data);
    
    $this->skin->parse("edit");
    return $this->skin->text("edit");
  }
  function do_Del ($lang){
    global $func, $DB, $conf, $vnT;
		$id = (int) $vnT->input['id'];
		$ext = $vnT->input["ext"];
		$del = 0;
		$qr = "";
		if ($id != 0) {
			$ids = $id;
		}
		if (isset($vnT->input["del_id"])){
			$ids = implode(',', $vnT->input["del_id"]);
		}
		$query = 'DELETE FROM custom_attr WHERE attr_id IN ('.$ids.')' ;
		if ($ok = $DB->query($query)){
			$DB->query("DELETE FROM custom_attr_desc WHERE attr_id IN (".$ids.")"); 
			$mess = $vnT->lang["del_success"] ;
			//xoa cache
      $func->clear_cache();
		} else
			$mess = $vnT->lang["del_failt"] ;
		$ext_page = str_replace("|", "&", $ext);
		$url = $this->linkUrl . "&{$ext_page}";
		$func->html_redirect($url, $mess);
  }
  function render_row ($row_info, $lang){
    global $func, $DB, $conf, $vnT;
    $row = $row_info;
    // Xu ly tung ROW
    $id = $row['attr_id'];
    $row_id = "row_" . $id;
    $output['check_box'] = vnT_HTML::checkbox("del_id[]", $id, 0, " ");
    $link_edit = $this->linkUrl . '&sub=edit&id=' . $id . '&ext=' . $row['ext_page'];
    $link_del = "javascript:del_item('" . $this->linkUrl . "&sub=del&id=" . $id . "&ext=" . $row['ext_page'] . "')";
    
		$output['order'] = "<input name=\"txt_Order[{$id}]\" type=\"text\" size=\"2\" maxlength=\"2\" style=\"text-align:center\" value=\"{$row['display_order']}\" onkeypress=\"return is_num(event,'txtOrder')\" onchange='javascript:do_check($id)' />";
		
		if ($row['picture']){
			$output['picture']= get_pic_custom($row['picture'],50) ;
		}else $output['picture'] ="No image";
		$output['title'] = "<a href=\"{$link_edit}\"><strong>".$func->HTML($row['title'])."</strong></a>";
		$output['link'] = "san-pham/attr/".$id."/".$func->make_url($row['title']).".html";
		//$output['focus'] = vnT_HTML::list_yesno("focus[{$id}]",$row['focus'],"onchange='javascript:do_check($id)'");
    //$output['num_custom']  = get_num_custom(" AND attr_id=$id ",$lang) ;
    $arr= array('project' => 'Công trình','product' => 'Loại sản phẩm','style' => 'Phong cách','material' => 'Chất liệu');
 		$output['attr_type'] = $arr[$row['attr_type']];
		$link_display = $this->linkUrl.$row['ext_link']; 		
    if ($row['display'] == 1) {
      $display = "<a href='".$link_display."&do_hidden=$id' title='".$vnT->lang['click_do_hidden']."' ><img src=\"" . $vnT->dir_images . "/dispay.gif\" width=15/></a>";
    } else {
      $display = "<a href='".$link_display."&do_display=$id' title='".$vnT->lang['click_do_display']."' ><img src=\"" . $vnT->dir_images . "/nodispay.gif\" width=15/></a>";
    }
    $output['action'] = '<input name=h_id[]" type="hidden" value="' . $id . '" />';    
    $output['action'] .= '<a href="' . $link_edit . '"><img src="' . $vnT->dir_images . '/edit.gif"  alt="Edit "></a>&nbsp;';
		$output['action'] .= $display.'&nbsp;';
    $output['action'] .= '<a href="' . $link_del . '"><img src="' . $vnT->dir_images . '/delete.gif"  alt="Delete "></a>';
    return $output;
  }
  function do_Manage ($lang){
    global $vnT, $func, $DB, $conf;
    //update
    if ($vnT->input["do_action"]){
      //xoa cache
      $func->clear_cache();
      if ($vnT->input["del_id"]) $h_id = $vnT->input["del_id"];
      switch ($vnT->input["do_action"]){
        case "do_edit":
          if (isset($vnT->input["txt_Order"])) $arr_order = $vnT->input["txt_Order"];
					if (isset($vnT->input["focus"])) $arr_focus = $vnT->input["focus"];
          $mess .= "- " . $vnT->lang['edit_success'] . " ID: <strong>";
          $str_mess = "";
          for ($i = 0; $i < count($h_id); $i ++){
            $dup['display_order'] = $arr_order[$h_id[$i]];
						$dup['focus'] = $arr_focus[$h_id[$i]];
            $ok = $DB->do_update("custom_attr", $dup, "attr_id=" . $h_id[$i]);
            if ($ok) {
              $str_mess .= $h_id[$i] . ", ";
            }
          }
          $mess .= substr($str_mess, 0, - 2) . "</strong><br>";
          $err = $func->html_mess($mess);
          break;
        case "do_hidden":
          $mess .= "- " . $vnT->lang['hidden_success'] . " ID: <strong>";
          for ($i = 0; $i < count($h_id); $i ++){
            $dup['display'] = 0;
            $ok = $DB->do_update("custom_attr", $dup, "attr_id=" . $h_id[$i]);
            if ($ok){
              $str_mess .= $h_id[$i] . ", ";
            }
          }
          $mess .= substr($str_mess, 0, - 2) . "</strong><br>";
          $err = $func->html_mess($mess);
          break;
        case "do_display":
            $mess .= "- " . $vnT->lang['display_success'] . "  ID: <strong>";
            for ($i = 0; $i < count($h_id); $i ++)
            {
              $dup['display'] = 1;
              $ok = $DB->do_update("custom_attr", $dup, "attr_id=" . $h_id[$i]);
              if ($ok){
                $str_mess .= $h_id[$i] . ", ";
              }
            }
            $mess .= substr($str_mess, 0, - 2) . "</strong><br>";
            $err = $func->html_mess($mess);
          break;
      }
    }
		if((int)$vnT->input["do_display"]) {
			$ok = $DB->query("UPDATE custom_attr SET display=1 WHERE attr_id=".$vnT->input["do_display"]);
			if($ok){
				$mess .= "- ".$vnT->lang['display_success']." ID: <strong>".$vnT->input["do_display"] . "</strong><br>";
				$err = $func->html_mess($mess);
			}
      $func->clear_cache();
		}
		if((int)$vnT->input["do_hidden"]) {
			$ok = $DB->query("UPDATE custom_attr SET display=0 WHERE attr_id=".$vnT->input["do_hidden"]);
			if($ok){
				$mess .= "- ".$vnT->lang['display_success']."  ID: <strong>".$vnT->input["do_hidden"] . "</strong><br>";
				$err = $func->html_mess($mess);
			}
      $func->clear_cache();
		}
		$p = ((int) $vnT->input['p']) ? $p = $vnT->input['p'] : 1;
		$n = ($conf['record']) ? $conf['record'] : 30;
		$keyword	= ($vnT->input['keyword']) ? $vnT->input['keyword'] : "";		
		$where ="";
    $attr_type = $vnT->input['attr_type'] ? $vnT->input['attr_type'] : '';
    if($attr_type){
      $where .=" AND attr_type = '$attr_type' ";
      $ext.="&attr_type=$attr_type";
    }
		if($keyword){
			$where .=" AND title like '%$keyword%' ";
			$ext.="&keyword=$keyword";
		}
    $query = $DB->query("SELECT  n.attr_id FROM custom_attr n,  custom_attr_desc nd
													WHERE  n.attr_id=nd.attr_id
													AND nd.lang='$lang'  {$where}  ");
    $totals = intval($DB->num_rows($query));    
		
    $num_pages = ceil($totals / $n);
    if ($p > $num_pages) $p = $num_pages;
    if ($p < 1) $p = 1;
    $start = ($p - 1) * $n;
    
    $nav = $func->paginate($totals, $n, $ext, $p);
    
    $table['link_action'] = $this->linkUrl . "{$ext}&p=$p";
		$ext_link = $ext."&p=$p" ;
    $table['title'] = array(
			'check_box' => "<input type=\"checkbox\" name=\"checkall\" id=\"checkall\" class=\"checkbox\" />|5%|center" , 
			'order' => $vnT->lang['order'] . "|10%|center" ,
			'title' => $vnT->lang['title']."| |left",  
			'attr_type' => " Kiểu |20%|center",
			'action' => "Action|15%|center",
		);
    $sql = "SELECT * FROM custom_attr n, custom_attr_desc nd
						WHERE n.attr_id=nd.attr_id AND nd.lang='$lang' {$where} 
						ORDER BY  display_order ASC , title ASC LIMIT $start,$n";
    $reuslt = $DB->query($sql);
    if ($DB->num_rows($reuslt)){
      $row = $DB->get_array($result);
      for ($i = 0; $i < count($row); $i ++){
				$row[$i]['ext_link'] = $ext_link ;
				$row[$i]['ext_page'] = $ext_page;
        $row_info = $this->render_row($row[$i],$lang);
        $row_field[$i] = $row_info;
        $row_field[$i]['stt'] = ($i + 1);
        $row_field[$i]['row_id'] = "row_" . $row[$i]['attr_id'];
        $row_field[$i]['ext'] = "";
      }
      $table['row'] = $row_field;
    }else{
      $table['row'] = array();
      $table['extra'] = "<div align=center class=font_err >" . $vnT->lang['no_have_attr'] ."</div>";
    }
		$table['button'] = '<input type="button" name="btnHidden" value=" '.$vnT->lang['hidden'].' " class="button" onclick="do_submit(\'do_hidden\')">&nbsp;';
		$table['button'] .= '<input type="button" name="btnDisplay" value=" '.$vnT->lang['display'].' " class="button" onclick="do_submit(\'do_display\')">&nbsp;';
		$table['button'] .= '<input type="button" name="btnEdit" value=" '.$vnT->lang['update'].' " class="button" onclick="do_submit(\'do_edit\')">&nbsp;';
		$table['button'] .= '<input type="button" name="btnDel" value=" '.$vnT->lang['delete'].' " class="button" onclick="del_selected(\''.$this->linkUrl.'&sub=del&ext='.$ext_page.'\')">';
    
    $table_list = $func->ShowTable($table);
    $data['table_list'] = $table_list;
    $data['totals'] = $totals;
		$data['keyword'] = $keyword;
    $data['err'] = $err;
    $data['nav'] = $nav;
    $data['List_Attr'] = $this->List_Attr_Type($attr_type," onchange='submit();'");
    $this->skin->assign('data', $data);
    $this->skin->parse("manage");
    return $this->skin->text("manage");
  }
}
$vntModule = new vntModule();
?>