<?php
/*================================================================================*\
|| 							Name code : advertise.php 		 		            	  ||
||  				Copyright @2008 by Thai Son - CMS vnTRUST                     ||
\*================================================================================*/
/**
 * @version : 2.0
 * @date upgrade : 09/01/2009 by Thai Son
 **/
if (! defined('IN_vnT')) {
  die('Hacking attempt!');
}
$vntModule = new vntModule();

class vntModule
{
  var $output = "";
  var $skin = "";
  var $linkUrl = "";
  var $module = "advertise";
  var $action = "custom";

  /**
   * function vntModule ()
   * Khoi tao 
   **/
  function vntModule ()
  {
    global $Template, $vnT, $func, $DB, $conf;
    require_once ("function_" . $this->module . ".php");
    $this->skin = new XiTemplate(DIR_MODULE . DS . $this->module . "_ad" . DS . "html" . DS . $this->action . ".tpl");
    $this->skin->assign('LANG', $vnT->lang);
    $this->skin->assign('DIR_IMAGE', $vnT->dir_images);
    $lang = ($vnT->input['lang']) ? $lang = $vnT->input['lang'] : $func->get_lang_default();
    $this->linkUrl = "?mod=" . $this->module . "&act=" . $this->action . "&lang=" . $lang;
    $pos = $vnT->input['pos'];
    switch ($vnT->input['sub']) {
      case 'add':
        $nd['f_title'] = $vnT->lang['add_advertise'];
        $nd['content'] = $this->do_Add($pos, $lang);
      break;
      case 'edit':
        $nd['f_title'] = $vnT->lang['edit_advertise'];
        $nd['content'] = $this->do_Edit($pos, $lang);
      break;
      case 'del':
        $this->do_Del($pos, $lang);
      break;
      default:
        $nd['f_title'] = $vnT->lang['manage_advertise'];
        $nd['content'] = $this->do_Manage($pos, $lang);
      break;
    }
    $nd['menu'] = $func->getToolbar($this->module, $this->action, $lang);
    $nd['row_lang'] = $func->html_lang("?mod=" . $this->module . "&act=" . $this->action, $lang);
    $Template->assign("data", $nd);
    $Template->parse("box_main");
    $vnT->output .= $Template->text("box_main");
  }

  /**
   * function do_Add 
   * Them gioi thieu moi 
   **/
  function do_Add ($pos, $lang)
  {
    global $vnT, $func, $DB, $conf;
    $err = "";
    $data['l_link'] = "http://";
    $err = "";
    $data['date_add'] = date("d/m/Y");
    $data['date_expire'] = date("d/m/Y", (time() + 30 * 24 * 3600));
    $data['display'] = 1;
    if ($vnT->input['do_submit'] == 1) {
      $data = $_POST;
      if ($vnT->input['date_add']) {
        $tmp = explode("/", $data['date_add']);
        $date_add = mktime(0, 0, 0, $tmp[1], $tmp[0], $tmp[2]);
      }
      if ($vnT->input['date_expire']) {
        $tmp = explode("/", $data['date_expire']);
        $date_expire = mktime(0, 0, 0, $tmp[1], $tmp[0], $tmp[2]);
      }
      //upload
      if ($vnT->input['chk_upload'] && ! empty($_FILES['image']) && $_FILES['image']['name'] != "") {
        $up['path'] = MOD_DIR_UPLOAD;
        $up['dir'] = "";
        $up['file'] = $_FILES['image'];
        $up['type'] = "hinh";
        $up['w'] = 1000;
        $result = $vnT->File->Upload($up);
        if (empty($result['err'])) {
          $l_url = $result['link'];
          $type = $result['type'];
        } else {
          $err = $func->html_err($result['err']);
        }
      } else {
        if ($vnT->input['picture']) {
          $up['path'] = MOD_DIR_UPLOAD;
          $up['dir'] = "";
          $up['url'] = $vnT->input['picture'];
          $up['type'] = "hinh";
          $up['w'] = 1000;
          $result = $vnT->File->UploadURL($up);
          if (empty($result['err'])) {
            $l_url = $result['link'];
            $type = $result['type'];
          } else {
            $err = $func->html_err($result['err']);
          }
        }
        if (empty($l_url))
          $err = $func->html_err("Vui lòng nhập logo ");
      } //end upload
      $res_lang = $DB->query("select * from language ");
      while ($row_lang = $DB->fetch_row($res_lang)) {
        $name = $row_lang['name'];
        $arr_title[$name] = $vnT->input['title'];
        $arr_img[$name] = $l_url;
      }
      // insert CSDL
      if (empty($err)) {
        $cot = array(
          "title" => serialize($arr_title) , 
          "img" => serialize($arr_img) , 
          "link" => $vnT->input['l_link'] , 
          "height" => $vnT->input['height'] , 
          "width" => $vnT->input['width'] , 
          "pos" => $pos , 
          "date_add" => $date_add , 
          "date_expire" => $date_expire , 
          "target" => $vnT->input['target'] , 
          "date_add" => $date_add , 
          "display" => $vnT->input['display'] , 
          "lang" => $lang);
        $ok = $DB->do_insert("advertise_custom", $cot);
        if ($ok) {
          //xoa cache
          $func->clear_cache();
          //insert adminlog
          $func->insertlog("Add", $_GET['act'], $DB->insertid());
          $mess = $vnT->lang['add_success'];
          $url = $this->linkUrl . "&sub=add&pos={$pos}";
          $func->html_redirect($url, $mess);
        } else {
          $err = $func->html_err($vnT->lang['add_failt'] . $DB->debug());
        }
      }
    }
    $data['vitri'] = List_Pos_Custom($pos);
    $data['list_target'] = List_Target($data['target']);
    $data['list_display'] = vnT_HTML::list_yesno("display", $data['display']);
    $data['err'] = $err;
    $data['link_action'] = $this->linkUrl . "&sub=add&pos=$pos";
    /*assign the array to a template variable*/
    $this->skin->assign('data', $data);
    $this->skin->parse("edit");
    return $this->skin->text("edit");
  }

  /**
   * function do_Edit 
   * Cap nhat admin
   **/
  function do_Edit ($pos, $lang)
  {
    global $vnT, $func, $DB, $conf;
    $id = (int) $vnT->input['id'];
    if ($vnT->input['do_submit']) {
      $data = $_POST;
      $l_url = $vnT->input['l_url'];
      if ($vnT->input['date_add']) {
        $tmp = explode("/", $data['date_add']);
        $date_add = mktime(0, 0, 0, $tmp[1], $tmp[0], $tmp[2]);
      }
      if ($vnT->input['date_expire']) {
        $tmp = explode("/", $data['date_expire']);
        $date_expire = mktime(0, 0, 0, $tmp[1], $tmp[0], $tmp[2]);
      }
      //upload
      if ($vnT->input['chk_upload'] && ! empty($_FILES['image']) && $_FILES['image']['name'] != "") {
        $up['path'] = MOD_DIR_UPLOAD;
        $up['dir'] = "";
        $up['file'] = $_FILES['image'];
        $up['type'] = "hinh";
        $up['w'] = 1000;
        $result = $vnT->File->Upload($up);
        if (empty($result['err'])) {
          $l_url = $result['link'];
          $type = $result['type'];
        } else {
          $err = $func->html_err($result['err']);
        }
      } else {
        if ($vnT->input['picture']) {
          $up['path'] = MOD_DIR_UPLOAD;
          $up['dir'] = "";
          $up['url'] = $vnT->input['picture'];
          $up['type'] = "hinh";
          $up['w'] = 1000;
          $result = $vnT->File->UploadURL($up);
          if (empty($result['err'])) {
            $l_url = $result['link'];
            $type = $result['type'];
          } else {
            $err = $func->html_err($result['err']);
          }
        }
      } //end upload
      if (empty($err)) {
        $cot = array(
          "title" => $func->update_content("advertise", "title", "l_id=$id ", $lang, $vnT->input['title']) , 
          "link" => $vnT->input['l_link'] , 
          "height" => $vnT->input['height'] , 
          "width" => $vnT->input['width'] , 
          "pos" => $pos , 
          "date_add" => $date_add , 
          "date_expire" => $date_expire , 
          "target" => $vnT->input['target'] , 
          "date_add" => $date_add , 
          "display" => $vnT->input['display'] , 
          "lang" => $lang);
        if (! empty($l_url)) {
          $cot['img'] = $func->update_content("advertise_custom", "img", "l_id=$id ", $lang, $l_url);
          $cot['type'] = $type;
        }
        $ok = $DB->do_update("advertise_custom", $cot, "l_id=$id");
        if ($ok) {
          //xoa cache
          $func->clear_cache();
          //insert adminlog
          $func->insertlog("Edit", $_GET['act'], $id);
          $err = $vnT->lang["edit_success"];
          $url = $this->linkUrl . "&sub=manage&pos=$pos";
          $func->html_redirect($url, $err);
        } else
          $err = $func->html_err($vnT->lang["edit_failt"] . $DB->debug());
      }
    }
    $query = $DB->query("SELECT * FROM advertise_custom WHERE l_id={$id} ");
    if ($data = $DB->fetch_row($query)) {
      $data['title'] = $func->fetch_content($data['title'], $lang);
      $data['l_link'] = $data['link'];
      $img = $func->fetch_content($data['img'], $lang);
      $src = MOD_DIR_UPLOAD . "/" . $img;
      if ($img) {
        if ($data['type'] == "swf") {
          $data['html_img'] = '<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=7,0,19,0"  height="' . $data['height'] . '">
				<param name="movie" value="' . $src . '" />
				<param name="quality" value="high" />
				<embed src="' . $src . '" quality="high" pluginspage="http://www.macromedia.com/go/getflashplayer" type="application/x-shockwave-flash"  height="' . $data['height'] . '"></embed>
			</object>';
        } else
          $data['html_img'] = "<img src=\"{$src}\" />";
      }
      $data['date_add'] = ($data['date_add']) ? @date("d/m/Y", $data['date_add']) : "";
      $data['date_expire'] = ($data['date_expire']) ? @date("d/m/Y", $data['date_expire']) : "";
      $data['list_target'] = List_Target($data['target']);
      $data['vitri'] = List_Pos_Custom($pos);
      $data['list_display'] = vnT_HTML::list_yesno("display", $data['display']);
    } else {
      $mess = $vnT->lang['not_found'] . " ID : " . $id;
      $url = $this->linkUrl;
      $func->html_redirect($url, $mess);
    }
    $data['err'] = $err;
    $data['link_action'] = $this->linkUrl . "&sub=edit&pos=$pos&id=$id";
    /*assign the array to a template variable*/
    $this->skin->assign('data', $data);
    $this->skin->parse("edit");
    return $this->skin->text("edit");
  }

  /**
   * function do_Del 
   * Xoa 1 ... n  gioi thieu 
   **/
  function do_Del ($pos, $lang)
  {
    global $func, $DB, $conf, $vnT;
    $id = (int) $vnT->input['id'];
    $ext = $vnT->input["ext"];
    $del = 0;
    $qr = "";
    if ($id != 0) {
      $ids = $id;
    }
    if (isset($vnT->input["del_id"])) {
      $ids = implode(',', $vnT->input["del_id"]);
    }
    $query = 'DELETE FROM advertise_custom WHERE l_id IN (' . $ids . ')';
    if ($ok = $DB->query($query)) {
      $mess = $vnT->lang["del_success"];
    } else
      $mess = $vnT->lang["del_failt"];
    $ext_page = str_replace("|", "&", $ext);
    $url = $this->linkUrl . "&sub=manage&pos=$pos&{$ext_page}";
    $func->html_redirect($url, $mess);
  }

  /**
   * function render_row 
   * list cac record
   **/
  function render_row ($row_info, $lang)
  {
    global $func, $DB, $conf, $vnT;
    $row = $row_info;
    // Xu ly tung ROW
    $id = $row['l_id'];
    $row_id = "row_" . $id;
    $pos = $row['pos'];
    $output['check_box'] = vnT_HTML::checkbox("del_id[]", $id, 0, " ");
    $link_edit = $this->linkUrl . '&sub=edit&pos=' . $pos . '&id=' . $id . '&ext=' . $row['ext_page'];
    $link_del = "javascript:del_item('" . $this->linkUrl . "&sub=del&pos=" . $pos . "&id=" . $id . "&ext=" . $row['ext_page'] . "')";
    $output['order'] = "<input name=\"txt_Order[{$id}]\" type=\"text\" size=\"2\" maxlength=\"2\" style=\"text-align:center\" value=\"{$row['l_order']}\"  onkeypress=\"return is_num(event,'txtOrder')\" onchange='javascript:do_check($id)' />";
    $img = $func->fetch_content($row['img'], $lang);
    if ($img) {
      $src = MOD_DIR_UPLOAD . "/" . $img;
      if ($row['type'] == "swf") {
        $picture = '<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=7,0,19,0"  height="' . $row['height'] . '">
					<param name="movie" value="' . $src . '" />
					<param name="quality" value="high" />
					<embed src="' . $src . '" quality="high" pluginspage="http://www.macromedia.com/go/getflashplayer" type="application/x-shockwave-flash"  height="' . $row['height'] . '"></embed>
				</object>';
      } else {
        $picture = "<img src=\"" . $src . "\">";
      }
    } else
      $picture = "No image";
    $output['picture'] = $func->fetch_content($row['title'], $lang) . "<br>" . $picture . "<br>" . $row['link'];
    $output['num_click'] = $row['num_click'] . "&nbsp;l&#7847;n";
    if ($row['display'] == 1) {
      $display = "<img src=\"{$vnT->dir_images}/dispay.gif\" width=15  />";
    } else {
      $display = "<img src=\"{$vnT->dir_images}/nodispay.gif\"  width=15 />";
    }
    $output['action'] = '<input name=h_id[]" type="hidden" value="' . $id . '" />';
    $output['action'] .= '<a href="' . $link_edit . '"><img src="' . $vnT->dir_images . '/edit.gif"  alt="Edit "></a>&nbsp;';
    $output['action'] .= $display . '&nbsp;';
    $output['action'] .= '<a href="' . $link_del . '"><img src="' . $vnT->dir_images . '/delete.gif"  alt="Delete "></a>';
    return $output;
  }

  /**
   * function do_Manage() 
   * Quan ly cac gioi thieu
   **/
  function do_Manage ($pos, $lang)
  {
    global $vnT, $func, $DB, $conf;
    //update
    if ($vnT->input["do_action"]) {
      //xoa cache
      $func->clear_cache();
      if ($vnT->input["del_id"])
        $h_id = $vnT->input["del_id"];
      switch ($vnT->input["do_action"]) {
        case "do_edit":
          if (isset($vnT->input["txt_Order"]))
            $arr_order = $vnT->input["txt_Order"];
          $mess .= "- " . $vnT->lang['edit_success'] . " ID: <strong>";
          $str_mess = "";
          for ($i = 0; $i < count($h_id); $i ++) {
            $dup['l_order'] = $arr_order[$h_id[$i]];
            $ok = $DB->do_update("advertise_custom", $dup, "l_id=" . $h_id[$i]);
            if ($ok) {
              $str_mess .= $h_id[$i] . ", ";
            }
          }
          $mess .= substr($str_mess, 0, - 2) . "</strong><br>";
          $err = $func->html_mess($mess);
        break;
        case "do_hidden":
          $mess .= "- " . $vnT->lang['hidden_success'] . " ID: <strong>";
          for ($i = 0; $i < count($h_id); $i ++) {
            $dup['display'] = 0;
            $ok = $DB->do_update("advertise_custom", $dup, "l_id=" . $h_id[$i]);
            if ($ok) {
              $str_mess .= $h_id[$i] . ", ";
            }
          }
          $mess .= substr($str_mess, 0, - 2) . "</strong><br>";
          $err = $func->html_mess($mess);
        break;
        case "do_display":
          $mess .= "- " . $vnT->lang['display_success'] . "  ID: <strong>";
          for ($i = 0; $i < count($h_id); $i ++) {
            $dup['display'] = 1;
            $ok = $DB->do_update("advertise_custom", $dup, "l_id=" . $h_id[$i]);
            if ($ok) {
              $str_mess .= $h_id[$i] . ", ";
            }
          }
          $mess .= substr($str_mess, 0, - 2) . "</strong><br>";
          $err = $func->html_mess($mess);
        break;
      }
    }
    $p = ((int) $vnT->input['p']) ? $p = $vnT->input['p'] : 1;
    $n = ($conf['record']) ? $conf['record'] : 30;
    $where = "";
    if ($pos) {
      $where = " where  pos ='{$pos}'";
      $ext = "&pos=$pos";
    }
    $query = $DB->query("SELECT l_id FROM advertise_custom $where ");
    $totals = intval($DB->num_rows($query));
    $num_pages = ceil($totals / $n);
    if ($p > $num_pages)
      $p = $num_pages;
    if ($p < 1)
      $p = 1;
    $start = ($p - 1) * $n;
    $nav = $func->paginate($totals, $n, $ext, $p);
    $table['link_action'] = $this->linkUrl . "{$ext}&p=$p";
    $table['title'] = array(
      'check_box' => "<input type=\"checkbox\" name=\"checkall\" id=\"checkall\" class=\"checkbox\" />|5%|center" , 
      'order' => $vnT->lang['order'] . "|10%|center" , 
      'picture' => $vnT->lang['logo'] . " |60%|center" , 
      'num_click' => "Click |7%|center" , 
      'action' => "Action|15%|center");
    $sql = "SELECT * FROM advertise_custom $where ORDER BY l_order LIMIT $start,$n";
    //print "sql = ".$sql."<br>";
    $result = $DB->query($sql);
    if ($DB->num_rows($result)) {
      $row = $DB->get_array($result);
      for ($i = 0; $i < count($row); $i ++) {
        $row_info = $this->render_row($row[$i], $lang);
        $row_field[$i] = $row_info;
        $row_field[$i]['stt'] = ($i + 1);
        $row_field[$i]['row_id'] = "row_" . $row[$i]['l_id'];
        $row_field[$i]['ext'] = "";
      }
      $table['row'] = $row_field;
    } else {
      $table['row'] = array();
      $table['extra'] = "<div align=center class=font_err >" . $vnT->lang['no_have_advertise'] . "</div>";
    }
    $table['button'] = '<input type="button" name="btnHidden" value=" ' . $vnT->lang['hidden'] . ' " class="button" onclick="do_submit(\'do_hidden\')">&nbsp;';
    $table['button'] .= '<input type="button" name="btnDisplay" value=" ' . $vnT->lang['display'] . ' " class="button" onclick="do_submit(\'do_display\')">&nbsp;';
    $table['button'] .= '<input type="button" name="btnEdit" value=" ' . $vnT->lang['update'] . ' " class="button" onclick="do_submit(\'do_edit\')">&nbsp;';
    $table['button'] .= '<input type="button" name="btnDel" value=" ' . $vnT->lang['delete'] . ' " class="button" onclick="del_selected(\'' . $this->linkUrl . '&sub=del&pos=$pos&ext=' . $ext_page . '\')">';
    $table_list = $func->ShowTable($table);
    $data['table_list'] = $table_list;
    $data['list_pos'] = List_Pos_Custom($pos, "onChange='submit();'");
    $data['totals'] = $totals;
    $data['err'] = $err;
    $data['nav'] = $nav;
    /*assign the array to a template variable*/
    $this->skin->assign('data', $data);
    $this->skin->parse("manage");
    return $this->skin->text("manage");
  }
  // end class
}
?>