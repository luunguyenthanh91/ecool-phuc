<?php
define('IN_vnT', 1);
define('DS', DIRECTORY_SEPARATOR);
require_once ("../../../../_config.php");
include ($conf['rootpath'] . "includes/class_db.php");
$DB = new DB();
//Functions
include ($conf['rootpath'] . 'includes/class_functions.php');
include($conf['rootpath'] . 'includes/admin.class.php');
$func  = new Func_Admin;
$conf = $func->fetchDbConfig($conf);

require_once ($conf['rootpath'] . "includes" . DS . 'class.XiTemplate.php');
define('DIR_SKIN', $conf['rootpath'] . "admin" . DS . 'skins' . DS . $conf['skin_acp']);
define('DIR_IMAGE', $conf['rooturl'] . "admin/skins/" . $conf['skin_acp'] . "/images");
$Template = new XiTemplate(DIR_SKIN . DS . 'global.tpl');
$vnT->lang_name = ($_GET['lang']) ? $_GET['lang'] : "vn";
$vnT->dir_images = $conf['rooturl'] . "admin/skins/" . $conf['skin_acp'] . "/images" ;
 
 
switch ($_GET['do']) {
  case "GetList":    $jsout = ajax_list();  break;
  case "GetNum":    $jsout = ajax_num();  break;
  case "GetInfo":    $jsout = ajax_info();  break; 
  default:    $jsout = "Error";  break;
}

function List_SubCat ($cat_id)
{
  global $func, $DB, $conf;
  $output = "";
  $query = $DB->query("SELECT * FROM video_category WHERE parentid={$cat_id}");
  while ($cat = $DB->fetch_row($query)) {
    $output .= $cat["cat_id"] . ",";
    $output .= List_SubCat($cat['cat_id']);
  }
  return $output;
}


//
function render_row ($row_info, $lang)
{
  global $conf, $func, $DB, $conf, $vnT;
  $row = $row_info;
  // Xu ly tung ROW
  $id = $row['video_id'];
  $row_id = "row_" . $id;
  $output['check_box'] = "<input type=\"checkbox\" name=\"del_id[]\" value=\"{$id}\" class=\"checkbox\" onclick=\"select_row('{$row_id}')\">";
  $link_edit = "?mod=video&act=video&sub=edit&id={$id}&ext={$row['ext_page']}&lang=$lang";
  if (empty($row['picture']))
    $picture = "<i>No Image</i>";
  else {
    $url_view = "zoom.php?image=" . $conf['rooturl'] . "vnt_upload/video/{$row['picture']}&title=Zoom";
    $picture = "<a href=\"#Zoom\" onClick=\"openPopUp('{$url_view}', 'Zoom', 700, 600, 'yes')\"><img src=\"" . DIR_IMAGE . "/photo.gif\"></a>";
  }
  $output['video_name'] = "<a href=\"{$link_edit}\">" . $func->HTML($row['video_name']) . "</a>";
  $output['date_post'] = date("d/m/Y", $row['date_post']);
  $output['picture'] = $picture;
  return $output;
}

//GetList
function ajax_list ()
{
  global $Template, $vnT, $DB, $func, $conf, $lang;
  $n = 30;
  if ((isset($_GET['p'])) && (is_numeric($_GET['p'])))
    $p = $_GET['p'];
  else
    $p = 1;
  $cat_id = (int) $_GET['cat_id'];
  $where = " ";
  if ($cat_id) {
    $subcat = List_SubCat($cat_id);
    $subcat = substr($subcat, 0, - 1);
    if (empty($subcat))
      $where .= " and FIND_IN_SET('$cat_id',cat_id)<>0 ";
    else {
      $tmp = explode(",", $subcat);
      $str_ = " FIND_IN_SET('$cat_id',cat_id)<>0 ";
      for ($i = 0; $i < count($tmp); $i ++) {
        $str_ .= " or FIND_IN_SET('$tmp[$i]',cat_id)<>0 ";
      }
      $where .= " and (" . $str_ . ") ";
    }
  } else {
    $where .= " and cat_id=0 ";
  }
  $query = $DB->query("SELECT video_id
					FROM videos 
					WHERE lang='$vnT->lang_name'
					$where   ");
  $totals = $DB->num_rows($query);
  $num_pages = ceil($totals / $n);
  if ($p > $num_pages)
    $p = $num_pages;
  if ($p < 1)
    $p = 1;
  $start = ($p - 1) * $n;
  $object = "LoadAjax('GetList','ext_list','cat_id={$cat_id}&lang=$lang',";

  $nav = $func->paginate_js($totals, $n, $p, $object );
  $table['link_action'] = "?mod=video&act=category&lang=$vnT->lang_name&sub=move&cat_chose=" . $cat_chose;
  $table['title'] = array(
    'check_box' => "<input type=\"checkbox\" name=\"all\" class=\"checkbox\" onclick=\"javascript:checkall();\" />|5%|center" , 
    'picture' => "Hình|10%|center" , 
    'video_name' => "Tiêu đề |50%|left" , 
    'date_post' => "Ngày đăng |15%|center");
  $sql = "SELECT *
					FROM videos
					WHERE lang='$vnT->lang_name'
					$where 
					ORDER BY date_post DESC, video_id DESC 
					LIMIT $start,$n";
  //print "sql = ".$sql."<br>";
  $reuslt = $DB->query($sql);
  if ($DB->num_rows($reuslt)) {
    $row = $DB->get_array($result);
    for ($i = 0; $i < count($row); $i ++) {
      $row_info = render_row($row[$i], $lang);
      $row_field[$i] = $row_info;
      $row_field[$i]['stt'] = ($i + 1);
      $row_field[$i]['row_id'] = "row_" . $row[$i]['video_id'];
      $row_field[$i]['ext'] = "";
    }
    $table['row'] = $row_field;
  } else {
    $table['row'] = array();
    $table['extra'] = "<div align=center class=font_err >Not found</div>";
  }
  $button .= "&nbsp;&nbsp;<input type=\"button\" name=\"btnDel\" value=\" Chuyển Item đã chọn \" class=\"button\" onclick=\"javascript:do_movecat('move')\">";
  $table['button'] = $button;
  $textout = $func->ShowTable($table);
  $textout .= $nav;
  return $textout;
}

function ajax_num ()
{
  global $DB, $func, $conf, $lang;
  $cat_id = (int) $_GET['cat_id'];
  $where = " where video_id<>0 ";
  if ($cat_id) {
    $subcat = List_SubCat($cat_id);
    $subcat = substr($subcat, 0, - 1);
    if (empty($subcat))
      $where .= " and FIND_IN_SET('$cat_id',cat_id)<>0 ";
    else {
      $tmp = explode(",", $subcat);
      $str_ = " FIND_IN_SET('$cat_id',cat_id)<>0 ";
      for ($i = 0; $i < count($tmp); $i ++) {
        $str_ .= " or FIND_IN_SET('$tmp[$i]',cat_id)<>0 ";
      }
      $where .= " and (" . $str_ . ") ";
    }
  }
  ;
  $sql = "select video_id from videos $where ";
  $res = $DB->query($sql);
  $num = $DB->num_rows($res);
  $textout = "Danh mục này hiện có :<b>$num</b> Item";
  return $textout;
}

//GetInfo
function ajax_info ()
{
  global $DB, $func, $conf, $lang;
  $textout = "";
  return $textout;
}
  
flush();
echo $jsout;
exit();
?>
