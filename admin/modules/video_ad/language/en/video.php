<?php
$lang = array(
  //cat_video
  'manage_cat_video' => "video management topics",
  'add_cat_video' => "Add a video theme",
  'move_cat' => 'Move the video of the theme',
  'edit_cat_video' => "Update theme video",
  'no_have_cat_video' => "No subject",
  'err_empty_cat' => 'Please enter the name of the subject', 
  'cat_name' => "Category",
  'cat_parent' => "Category parent",
  'cat_description' => "topic description",
  'move_success' => 'Move succeeded',
  'move_item_success' => 'Move video success',
  'focus' => 'Focus',
  // Video
  'manage_video' => "Manage Videos",
  'add_video' => "Add new video",
  'edit_video' => "update video video",
  'edit_pic' => 'Update the video',
  'no_have_video' => 'No video',
	'category'	=>	'Category',
	'video_type'	=>	'Type video',
  // Setting
  'manage_setting' => 'Manage configuration for video',
  'edit_setting_success' => 'Update profile video success',
  'setting_module' => 'General Configuration for video module',
  'imgthumb_width' => 'Thumbnail size',
  'media_width' => 'width of the Meida',
  'media_height' => 'Height of Meida',
  'n_list' => 'Number of video view as list',
  'n_grid' => 'Number of video view grid',
  'n_main' => 'The number of videos in each category in the Home',
  'nophoto' => 'Show the <strong> nophoto </strong> to believe there is no photo');
?>