<?php
$lang = array(
  //cat_video
  'manage_cat_video' => "Quản lý chủ đề video" , 
  'add_cat_video' => "Thêm chủ đề video" , 
  'move_cat' => 'Di chuyển video của chủ đề' , 
  'edit_cat_video' => "Cập nhật chủ đề video" , 
  'no_have_cat_video' => "Chưa có chủ đề nào" , 
  'err_empty_cat' => "Vui lòng điền tên chủ đề" , 
  'cat_name' => "Tên danh mục" , 
  'cat_parent' => "Danh mục cha" , 
  'cat_description' => "Mô tả chủ đề " , 
  'move_success' => "Di chuyển thành công" , 
  'move_item_success' => 'Di chuyển video thành công' , 
  'focus' => 'Focus' , 
  //video
  'manage_video' => "Quản lý video" , 
  'add_video' => "Thêm video mới" , 
  'edit_video' => "Cập nhật video video" , 
  'edit_pic' => 'Cập nhật hình video' , 
  'no_have_video' => 'Chưa có video nào' , 
	'category'	=>	'Danh mục',
	'video_type'	=>	'Loại video',
  //setting
  'manage_setting' => 'Quản lý cấu hình cho video' , 
  'edit_setting_success' => 'Cập nhật cấu hình video thành công' , 
  'setting_module' => 'Cấu hình chung cho module video' ,  
  'imgthumb_width' => 'Kích thước hình nhỏ' , 
  'media_width' => 'Chiều ngang của Meida' , 
  'media_height' => 'Chiều cao của Meida' , 
  'n_list' => 'Số video xem theo list' , 
  'n_grid' => 'Số video xem theo grid' , 
  'n_main' => 'Số video trong từng category ở Home' , 
  'nophoto' => 'Hiện hình <strong>nophoto</strong> khi tin không có ảnh');
?>