<?php
/*================================================================================*\
|| 							Name code : video.php 		 		            	  ||
||  				Copyright @2008 by Thai Son - CMS vnTRUST                     ||
\*================================================================================*/
/**
 * @version : 2.0
 * @date upgrade : 09/01/2009 by Thai Son
 **/
if (! defined('IN_vnT')) {
  die('Hacking attempt!');
}
$vntModule = new vntModule();

class vntModule
{
  var $output = "";
  var $skin = "";
  var $linkUrl = "";
  var $module = "video";
  var $action = "video";
 
  /**
   * function vntModule ()
   * Khoi tao 
   **/
  function vntModule ()
  {
    global $Template, $vnT, $func, $DB, $conf;
    require_once ("function_" . $this->module . ".php");   
		 
    $this->skin = new XiTemplate(DIR_MODULE . DS . $this->module . "_ad" . DS . "html" . DS . $this->action . ".tpl");
    $this->skin->assign('LANG', $vnT->lang);
		$this->skin->assign('DIR_JS', $vnT->dir_js);
    $vnT->html->addScript("modules/" . $this->module . "_ad" . "/js/" . $this->module  . ".js");
    $lang = ($vnT->input['lang']) ? $lang = $vnT->input['lang'] : $func->get_lang_default();
    $this->linkUrl = "?mod=" . $this->module . "&act=" . $this->action . "&lang=" . $lang;
		loadSetting($lang);
		
		$vnT->html->addStyleSheet($vnT->dir_js . "/swfupload_user/css/default.css");
    $vnT->html->addScript($vnT->dir_js . "/swfupload_user/js/swfupload.js");
    $vnT->html->addScript($vnT->dir_js . "/swfupload_user/forms/js/fileprogress.js");
    $vnT->html->addScript($vnT->dir_js . "/swfupload_user/forms/js/handlers.js");
		
		$vnT->html->addScript($vnT->dir_js."/swfobject.js");
		
    switch ($vnT->input['sub']) {
      case 'add':
        $nd['f_title'] = $vnT->lang['add_video'];
        $nd['content'] = $this->do_Add($lang);
      break;
      case 'edit':
        $nd['f_title'] = $vnT->lang['edit_video'];
        $nd['content'] = $this->do_Edit($lang);
      break;
      case 'del':
        $this->do_Del($lang);
      break;
      default:
        $nd['f_title'] = $vnT->lang['manage_video'];
        $nd['content'] = $this->do_Manage($lang);
      break;
    }
    $nd['menu'] = $func->getToolbar($this->module, $this->action, $lang);
    $nd['row_lang'] = $func->html_lang("?mod=" . $this->module . "&act=" . $this->action, $lang);
    $Template->assign("data", $nd);
    $Template->parse("box_main");
    $vnT->output .= $Template->text("box_main");
  }

  /**
   * function do_Add 
   * Them  moi 
   **/
  function do_Add ($lang)
  {
    global $vnT, $func, $DB, $conf;
    $err = "";
    $vnT->input['display'] = 1;
    $vnT->input['is_focus'] = 0;
    $data['cat_id'] = $vnT->input['cat_id'];
    $w_thumb = ($vnT->setting['imgthumb_width']) ? $vnT->setting['imgthumb_width'] : 100;
    $w_detail = ($vnT->setting['media_width']) ? $vnT->setting['media_width'] : 500;
		$media_w = ($vnT->setting['media_width']) ? $vnT->setting['media_width'] : 700;
		$media_h = ($vnT->setting['media_height']) ? $vnT->setting['media_height'] : 525;
		
		$cur_folder = $vnT->func->create_folder(MOD_DIR_UPLOAD);
		$folder_upload_root = $conf['rootpath']."vnt_upload/video" ;
		$folder_upload = $folder_upload_root;
		
		if($cur_folder!="")
		{
			$folder_upload .= "/". $cur_folder ;
			if (! file_exists($folder_upload)) {
         $err = $func->html_err("Thư mục <b>{$folder_upload}</b> không tồn tại");
      }
		}
		
		$data['folder_upload'] = $folder_upload ;
		$data['folderpath'] =   MOD_DIR_UPLOAD ;
		$data['adminid'] =  $vnT->admininfo['adminid'];
		
    if ($vnT->input['do_submit'] == 1) {
      $video_name = $func->txt_HTML($_POST['video_name']);
      $video_type = $_POST['video_type'];
			
			$op_view_video = $_POST["op_view_video"];
			
      if ($video_type == "embed") 
			{
        $video_url = $func->txt_HTML($_POST['video_url']);
        $type_file = "object";
      } 
			elseif ($video_type == "file") 
			{
        $video_file = str_replace($folder_upload_root."/","",$_POST["hidFileID"]);
				$type_file = strtolower(substr($video_file, strrpos($video_file, ".") + 1));
      }
			elseif ($video_type == "url_youtube") 
			{
        $url_youtube = trim($_POST['url_youtube']);
        $type_file = strtolower(substr($url_youtube, strrpos($url_youtube, ".") + 1));
				
				$code_img = get_code_img($url_youtube);
				
				
				$video_url = make_iframe_youtube ($code_img, $op_view_video, $media_w, $media_h);
				$type_file = "object";
      }
			else
			{
        $video_url = trim($_POST['video_url']);
        $type_file = strtolower(substr($video_url, strrpos($video_url, ".") + 1));
      }
			
      // Check for Error
      /*$res_chk = $DB->query("SELECT * FROM videos  WHERE video_name='{$video_name}' ");
      if ($check = $DB->fetch_row($res_chk))
        $err = $func->html_err(str_replace("<title>", "Tên video", $vnT->lang['title_exist']));*/
				
			$dir = $cur_folder ;
      if ($vnT->input['chk_upload'] && ! empty($_FILES['image']) && $_FILES['image']['name'] != "") {
        $up['path'] = MOD_DIR_UPLOAD;
        $up['dir'] = $dir;
        $up['file'] = $_FILES['image'];
        $up['type'] = "hinh";
        $up['w'] = $w_detail;
        $up['thum'] = 1;
        $up['w_thum'] = $w_thumb;
        $result = $vnT->File->Upload($up);
        if (empty($result['err'])) {
          $picture = $dir."/".$result['link'];
        } else {
          $err = $func->html_err($result['err']);
        }
      } else {
        if ($vnT->input['picture']) {
          $up['path'] = MOD_DIR_UPLOAD;
          $up['dir'] = $dir;
          $up['url'] = $vnT->input['picture'];
          $up['w'] = $w_detail;
          $up['thum'] = 1;
          $up['w_thum'] = $w_thumb;
          $result = $vnT->File->UploadURL($up);
          if (empty($result['err'])) {
            $picture = $dir."/".$result['link'];
          } else {
            $err = $func->html_err($result['err']);
          }
        }
      }
			
			// lay hinh
			if (empty($picture) && ($video_type == "url_youtube" || $video_type == "embed")) {
				if($video_type == "url_youtube")
				{
					$tmp_pic = get_img_youtube($url_youtube, $dir, $video_type);
				}
				else
				{
					$tmp_pic = get_img_youtube($video_url, $dir, $video_type);
				}
				
				if ($tmp_pic)
					$picture = $tmp_pic;
					
			}
			
      // insert CSDL
      if (empty($err)) {
        $cot['cat_id'] = $vnT->input['cat_id'];
				$cot['url_youtube'] = $url_youtube;
				$cot['op_view_video '] = serialize($op_view_video) ;
        $cot['video_url'] = $video_url;
				if($_POST["chk_upload_file"]>0)
					$cot['video_file'] = $video_file;
					
				$cot['video_type'] = $video_type;
				$cot['type_file'] = $type_file;
				$cot['status'] = $vnT->input['status'];
        $cot['picture'] = $picture;
				$cot['date_post'] = time();
				$cot['date_update'] = time();
				$cot['adminid'] =  $vnT->admininfo['adminid'];
				
				/*echo "<pre>";
				print_r($cot);
				echo "</pre>";
				die();*/
        $ok = $DB->do_insert("videos", $cot);
        if ($ok) 
				{
          
					$video_id = $DB->insertid();
					$cot_d['video_id'] = $video_id;
					$cot_d['video_name'] = $video_name;
        	$cot_d['description'] = $DB->mySQLSafe($_POST['description']);
        	$cot_d['display'] = $vnT->input['display'];
          $cot_d['is_focus'] = $vnT->input['is_focus'];
          
				//SEO
					$cot_d['friendly_url'] = (trim($vnT->input['friendly_url'])) ? trim($vnT->input['friendly_url']) :  $func->make_url($video_name);
					$cot_d['friendly_title'] = (trim($vnT->input['friendly_title'])) ? trim($vnT->input['friendly_title']) : $video_name ." - ". $func->utf8_to_ascii($video_name);
					$cot_d['metakey'] = $vnT->input['metakey'];
					$cot_d['metadesc'] = $vnT->input['metadesc'];	
					
					$query_lang = $DB->query("select name from language ");
					while ( $row = $DB->fetch_row($query_lang) ) {
						$cot_d['lang'] = $row['name'];
						$DB->do_insert("videos_desc",$cot_d);
					}
					//xoa cache
          $func->clear_cache();
          //insert adminlog
          $func->insertlog("Add", $_GET['act'], $DB->insertid());
          $mess = $vnT->lang['add_success'];
          $url = $this->linkUrl . "&sub=add" ;
          $func->html_redirect($url, $mess);
        } else {
          $err = $func->html_err($vnT->lang['add_failt'] . $DB->debug());
        }
      }
    }
    $data['video_type'] = vnT_HTML::selectbox("video_type", array(
      'url' => 'URL' , 
			'url_youtube' => 'URL Youtube' , 
      'embed' => 'Embed',
			'file' => 'File'), 'url_youtube');
		$data['video_view'] = '<div class="ext_upload_file"><input name="chk_upload_file" id="chk_upload_file1" class="chk_upload_file" type="radio" value="1" checked style="display:none;" ></div>';
		
		$data['op_view_video'] = op_view_video($data['op_view_video']);
    $data['list_cat'] = Get_Cat($data['cat_id'], $lang, "");
    $data['list_status'] = List_Status("status", $data['status'], $lang);
    $data["html_content"] = $vnT->editor->doDisplay('description', $vnT->input['description'], '100%', '300', "Default");
    $data['err'] = $err;
    $data['list_display'] = vnT_HTML::list_yesno("display", $vnT->input['display']);
    $data['list_is_focus'] = vnT_HTML::list_yesno("is_focus", $vnT->input['is_focus']);
    $data['link_action'] = $this->linkUrl . "&sub=add";
    /*assign the array to a template variable*/
    $this->skin->assign('data', $data);
    $this->skin->parse("edit");
    return $this->skin->text("edit");
  }

  /**
   * function do_Edit 
   * Cap nhat 
   **/
  function do_Edit ($lang)
  {
    global $vnT, $func, $DB, $conf;
    $id = (int) $vnT->input['id'];
    $ext = $vnT->input['ext'];
    $w_thumb = ($vnT->setting['imgthumb_width']) ? $vnT->setting['imgthumb_width'] : 100;
    $w_detail = ($vnT->setting['imgdetail_width']) ? $vnT->setting['imgdetail_width'] : 500;
		$media_w = ($vnT->setting['media_width']) ? $vnT->setting['media_width'] : 700;
		$media_h = ($vnT->setting['media_height']) ? $vnT->setting['media_height'] : 525;
		
		$cur_folder = $vnT->func->create_folder(MOD_DIR_UPLOAD);
		$folder_upload_root = $conf['rootpath']."vnt_upload/video" ;
		$folder_upload = $folder_upload_root;
		
		if($cur_folder!="")
		{
			$folder_upload .= "/". $cur_folder ;
			if (! file_exists($folder_upload)) {
         $err = $func->html_err("Thư mục <b>{$folder_upload}</b> không tồn tại");
      }
		}
		
		$data['folder_upload'] = $folder_upload ;
		$data['folderpath'] =   MOD_DIR_UPLOAD ;
		$data['adminid'] =  $vnT->admininfo['adminid'];
		
    if ($vnT->input['do_submit']) {
      $data = $_POST;
      $video_name = $func->txt_HTML($_POST['video_name']);
      $video_type = $_POST['video_type'];
			
			$op_view_video = $_POST["op_view_video"];
			
      if ($video_type == "embed") 
			{
        $video_url = $func->txt_HTML($_POST['video_url']);
        $type_file = "object";
      } 
			elseif ($video_type == "file") 
			{
        $video_file = str_replace($folder_upload_root."/","",$_POST["hidFileID"]);
				$type_file = strtolower(substr($video_file, strrpos($video_file, ".") + 1));
      }
			elseif ($video_type == "url_youtube") 
			{
         $url_youtube = trim($_POST['url_youtube']);
        $type_file = strtolower(substr($url_youtube, strrpos($url_youtube, ".") + 1));
				
				$code_img = get_code_img($url_youtube);
				
				$video_url = make_iframe_youtube ($code_img, $op_view_video, $media_w, $media_h);
				$type_file = "object";
      }
			else
			{
        $video_url = trim($_POST['video_url']);
        $type_file = strtolower(substr($video_url, strrpos($video_url, ".") + 1));
      }
			
      // Check for Error
      /*$res_chk = $DB->query("SELECT * FROM videos  WHERE video_name='{$video_name}' and video_id<>$id ");
      if ($check = $DB->fetch_row($res_chk))
        $err = $func->html_err(str_replace("<title>", "Tên video", $vnT->lang['title_exist']));*/
				
			$dir = $cur_folder;
      if ($vnT->input['chk_upload'] && ! empty($_FILES['image']) && $_FILES['image']['name'] != "") {
        $up['path'] = MOD_DIR_UPLOAD;
        $up['dir'] = $dir;
        $up['file'] = $_FILES['image'];
        $up['type'] = "hinh";
        $up['w'] = $w_detail;
        $up['thum'] = 1;
        $up['w_thum'] = $w_thumb;
        $result = $vnT->File->Upload($up);
        if (empty($result['err'])) {
          $picture = $dir."/".$result['link'];
        } else {
          $err = $func->html_err($result['err']);
        }
      } else {
        if ($vnT->input['picture']) {
          $up['path'] = MOD_DIR_UPLOAD;
          $up['dir'] = $dir;
          $up['url'] = $vnT->input['picture'];
          $up['w'] = $w_detail;
          $up['thum'] = 1;
          $up['w_thum'] = $w_thumb;
          $result = $vnT->File->UploadURL($up);
          if (empty($result['err'])) {
            $picture = $dir."/".$result['link'];
          } else {
            $err = $func->html_err($result['err']);
          }
        }
      }
			
			// lay hinh
			if (empty($picture) && $vnT->input["have_pic"] == 0 && ($video_type == "url_youtube" || $video_type == "embed")) {
				if($video_type == "url_youtube")
				{
					$tmp_pic = get_img_youtube($url_youtube, $dir, $video_type);
				}
				else
				{
					$tmp_pic = get_img_youtube($video_url, $dir, $video_type);
				}
				
				if ($tmp_pic)
					$picture = $tmp_pic;
			}
			
      if (empty($err)) {
        $cot['cat_id'] = $vnT->input['cat_id'];
        $cot['url_youtube'] = $url_youtube;
				$cot['op_view_video '] = serialize($op_view_video) ;
				
        $cot['video_url'] = $video_url;
				if($_POST["chk_upload_file"]>0)
				{
					$cot['video_file'] = $video_file;
				}
				if($_POST["chk_upload_file"]>0 || $video_type != "file")
					$cot['type_file'] = $type_file;
				
        $cot['video_type'] = $video_type;
        if($picture)
				{
					$cot['picture'] = $picture;
				}
        $cot['status'] = $vnT->input['status'];
				
				$cot['date_update'] = time();
				
        // more here ...
        $ok = $DB->do_update("videos", $cot, "video_id=$id");
        if ($ok) 
				{
					$cot_d['video_name'] = $video_name;
					$cot_d['description'] = $DB->mySQLSafe($_POST['description']);
					$cot_d['display'] = $vnT->input['display'];
          $cot_d['is_focus'] = $vnT->input['is_focus'];
					//SEO
					$cot_d['friendly_url'] = (trim($vnT->input['friendly_url'])) ? trim($vnT->input['friendly_url']) :  $func->make_url($video_name);
					$cot_d['friendly_title'] = (trim($vnT->input['friendly_title'])) ? trim($vnT->input['friendly_title']) : $video_name ." - ". $func->utf8_to_ascii($video_name);
					$cot_d['metakey'] = $vnT->input['metakey'];
					$cot_d['metadesc'] = $vnT->input['metadesc'];
					$DB->do_update("videos_desc",$cot_d," video_id={$id} and lang='{$lang}'");
					
          //xoa cache
          $func->clear_cache();
          //insert adminlog
          $func->insertlog("Edit", $_GET['act'], $id);
          $err = $vnT->lang["edit_success"];
          $ext_page = str_replace("|", "&", $ext);
          $url = $this->linkUrl . "&{$ext_page}";
          $func->html_redirect($url, $err);
        } else
          $err = $func->html_err($vnT->lang["edit_failt"] . $DB->debug());
      }
    }
		$query = $DB->query("SELECT *
													FROM videos p, videos_desc pd
													WHERE p.video_id=pd.video_id 
													AND pd.lang='$lang'
													AND p.video_id=$id ");
    if ($data = $DB->fetch_row($query)) {
      if (! empty($data['picture'])) {
        $data['pic'] = get_pic_video($data['picture'], $w_thumb) . "<br>";
      } else
        $data['pic'] = "";
			
			if (! empty($data['video_file'])) {
        $data['video_view'] = playFLV ($vnT->conf["rooturl"]."vnt_upload/video/".$data['video_file'], $media_w, $media_h ,$vnT->conf["rooturl"]."vnt_upload/video/".$data['picture']) . "<br /><br />";
				$data['video_view'] .= '<div class="ext_upload_file">
            <input name="chk_upload_file" id="chk_upload_file0" class="chk_upload_file" type="radio" value="0" checked> Không đổi file &nbsp; 
            <input name="chk_upload_file" id="chk_upload_file1" class="chk_upload_file" type="radio" value="1" > Đổi file &nbsp;&nbsp;&nbsp;
          </div>';
      } else
        $data['video_view'] = '<div class="ext_upload_file"><input name="chk_upload_file" id="chk_upload_file1" class="chk_upload_file" type="radio" value="1" checked style="display:none;" ></div>';	
			
			if($data['video_type'] == "url_youtube")
			{
				$data["show_embed"] = '<tr><td class="row1"><strong>Mã nhúng :</strong> </td>
				<td class="row0">'.$data['video_url'].'</td></tr>';	
			}
			
      $data['video_type'] = vnT_HTML::selectbox("video_type", array(
				'url' => 'URL' , 
				'url_youtube' => 'URL Youtube' , 
				'embed' => 'Embed',
				'file' => 'File'), $data['video_type']);
			
			
			
			
			$data['op_view_video'] = unserialize($data['op_view_video']);			 
			 
			$data['op_view_video'] = op_view_video($data['op_view_video']);
      $data['list_cat'] = Get_Cat($data['cat_id'], $lang, "");
      $data['list_status'] = List_Status("status", $data['status'], $lang);
      $data['list_display'] = vnT_HTML::list_yesno("display", $data['display']);
      $data['list_is_focus'] = vnT_HTML::list_yesno("is_focus", $data['is_focus']);
      $data["html_content"] = $vnT->editor->doDisplay('description', $data['description'], '100%', '300', "Default");
			 
    } else {
      $mess = $vnT->lang['not_found'] . " ID : " . $id;
      $url = $this->linkUrl;
      $func->html_redirect($url, $mess);
    }
    $data['err'] = $err;
    $data['link_action'] = $this->linkUrl . "&sub=edit&id=$id&ext={$ext}";
    /*assign the array to a template variable*/
    $this->skin->assign('data', $data);
    $this->skin->parse("edit");
    return $this->skin->text("edit");
  }

  /**
   * function do_Del 
   * Xoa 1 ... n  gioi thieu 
   **/

function do_Del ($lang)
  {
    global $func, $DB, $conf, $vnT;

		$id = (int) $vnT->input['id'];
		$ext = $vnT->input["ext"];
		$del = 0;
		$qr = "";
			
		if ($id != 0) {
			$ids = $id;
		}
		if (isset($vnT->input["del_id"])){
			$ids = implode(',', $vnT->input["del_id"]);
		} 		
 		 
		$res = $DB->query("SELECT * FROM videos WHERE video_id IN (" . $ids . ") ");
    if ($DB->num_rows($res))
    {			
      while ($row = $DB->fetch_row($res))  {
        $res_d = $DB->query("SELECT id FROM videos_desc WHERE video_id=".$row['video_id']." AND lang<>'".$lang."' ");
				if(!$DB->num_rows($res_d))
				{
					$DB->query("DELETE FROM videos WHERE  video_id=".$row['video_id']."  ");
					}	
				$DB->query("DELETE FROM videos_desc WHERE  video_id=".$row['video_id']." AND lang='".$lang."' ");	
      }
      $mess = $vnT->lang["del_success"];
			//xoa cache
      $func->clear_cache();
    } else  {
      $mess = $vnT->lang["del_failt"];
    } 
		 
		$ext_page = str_replace("|", "&", $ext);
		$url = $this->linkUrl . "&{$ext_page}";
		$func->html_redirect($url, $mess);
  }
  /**
   * function render_row 
   * list cac record
   **/
  function render_row ($row_info, $lang)
  {
    global $func, $DB, $conf, $vnT;
    $row = $row_info;
    // Xu ly tung ROW
    $id = $row['video_id'];
    $row_id = "row_" . $id;
    $output['check_box'] = vnT_HTML::checkbox("del_id[]", $id, 0, " ");
    $link_edit = $this->linkUrl . '&sub=edit&id=' . $id . '&ext=' . $row['ext_page'];
    $link_del = "javascript:del_item('" . $this->linkUrl . "&sub=del&id=" . $id . "&ext=" . $row['ext_page'] . "')";
    $output['order'] = "<input name=\"txt_Order[{$id}]\" type=\"text\" size=\"2\" maxlength=\"2\" style=\"text-align:center\" value=\"{$row['video_order']}\"  onkeypress=\"return is_num(event,'txtOrder')\" onchange='javascript:do_check($id)' />";
		
		$output['is_focus'] = vnT_HTML::list_yesno("is_focus[{$id}]", $row['is_focus'], "onchange='javascript:do_check($id)'");
    $title = "<a href='" . $link_edit . "'><b>".$func->HTML($row['title'])."</b></a>";
		if ($row['is_sub']) {
      $ext = '&nbsp;<img src="' . $vnT->dir_images . '/line3.gif" align="absmiddle" />&nbsp;';
      $title = $ext . $title ;
      $output['is_focus'] = "---";
    }
		
		
    $output['name'] = "<a href=\"{$link_edit}\">" . $row['name'] . "</a>";
    if ($row['picture']) {
      $output['picture'] = get_pic_video($row['picture'], 75);
    } else
      $output['picture'] = "No image";
    $output['video_name'] = "<strong><a href=\"{$link_edit}\">" . $func->HTML($row['video_name']) . "</a></strong>";
    $output['video_url'] = $row['video_url'];
    //$output['status'] = List_Status("status[$id]", $row['status'], $lang, " style='width:100%' onchange='javascript:do_check($id)' ");
    $output['cat_name'] = get_cat_name($row['cat_id'], $lang);
    
		$poster = "<a href='?mod=admin&act=admin&sub=edit&id=".$row['adminid']."'>".get_admin($row['adminid'])."</a>";
		$info =  "<div style='padding:2px;'>".$vnT->lang['poster']." : <strong>".$poster."</strong></div>";
		$info .= "<div style='padding:2px;'>".$vnT->lang['date_post']." : <b>".@date("d/m/Y",$row['date_post'])."</b></div>";
 		$info .=  "<div style='padding:2px;'>".$vnT->lang['views']." : <strong>".(int)$row['view_num']."</strong></div>";
		$output['info'] = $info ;
		
    $link_display = $this->linkUrl.$row['ext_link']; 		
    if ($row['display'] == 1) {
      $display = "<a href='".$link_display."&do_hidden=$id' title='".$vnT->lang['click_do_hidden']."' ><img src=\"" . $vnT->dir_images . "/dispay.gif\" width=15  /></a>";
    } else {
      $display = "<a href='".$link_display."&do_display=$id'  title='".$vnT->lang['click_do_display']."' ><img src=\"" . $vnT->dir_images . "/nodispay.gif\"  width=15 /></a>";
    }


		
    $output['action'] = '<input name=h_id[]" type="hidden" value="' . $id . '" />';
    $output['action'] .= '<a href="' . $link_edit . '"><img src="' . $vnT->dir_images . '/edit.gif"  alt="Edit "></a>&nbsp;';
    $output['action'] .= $display . '&nbsp;';
    $output['action'] .= '<a href="' . $link_del . '"><img src="' . $vnT->dir_images . '/delete.gif"  alt="Delete "></a>';
    return $output;
  }

  /**
   * function do_Manage() 
   * Quan ly cac gioi thieu
   **/
  function do_Manage ($lang)
  {
    global $vnT, $func, $DB, $conf;
		$vnT->html->addStyleSheet( $vnT->dir_js . "/jquery_ui/themes/base/ui.all.css");
		$vnT->html->addScript($vnT->dir_js . "/jquery_ui/ui.core.js");		
		$vnT->html->addScript($vnT->dir_js . "/jquery_ui/ui.datepicker.js");  
    $vnT->html->addScriptDeclaration("
	 		$(function() {
				$('#date_begin').datepicker({
						showOn: 'button',
						buttonImage: '".$vnT->dir_images."/calendar.gif',
						buttonImageOnly: true,
						changeMonth: true,
						changeYear: true
					});
					$('#date_end').datepicker({
						showOn: 'button',
						buttonImage: '".$vnT->dir_images."/calendar.gif',
						buttonImageOnly: true,
						changeMonth: true,
						changeYear: true
					});

			});
		
		"); 
    //update
    if ($vnT->input["do_action"]) {
      //xoa cache
      $func->clear_cache();
      if ($vnT->input["del_id"])
        $h_id = $vnT->input["del_id"];
      switch ($vnT->input["do_action"]) {
        case "do_edit":
          if (isset($vnT->input["txt_Order"]))   $arr_order = $vnT->input["txt_Order"];  
          $mess .= "- " . $vnT->lang['edit_success'] . " ID: <strong>";
          $str_mess = "";
          for ($i = 0; $i < count($h_id); $i ++) {
            $dup['video_order'] = $arr_order[$h_id[$i]]; 
            $ok = $DB->do_update("videos", $dup, "video_id=" . $h_id[$i]);
            if ($ok) {
              $str_mess .= $h_id[$i] . ", ";
            }
          }
          $mess .= substr($str_mess, 0, - 2) . "</strong><br>";
          $err = $func->html_mess($mess);
        break;
        case "do_hidden":
          $mess .= "- " . $vnT->lang['hidden_success'] . " ID: <strong>";
					for ($i = 0; $i < count($h_id); $i ++)
            {
              $dup['display'] = 0;
              $ok = $DB->do_update("videos_desc", $dup, "lang='$lang' AND video_id=" . $h_id[$i]);
              if ($ok){
                $str_mess .= $h_id[$i] . ", ";
              }
            }
            $mess .= substr($str_mess, 0, - 2) . "</strong><br>";
            $err = $func->html_mess($mess);
        break;
        case "do_display":
          $mess .= "- " . $vnT->lang['display_success'] . "  ID: <strong>";
					for ($i = 0; $i < count($h_id); $i ++)
            {
              $dup['display'] = 1;
              $ok = $DB->do_update("videos_desc", $dup, "lang='$lang' AND video_id=" . $h_id[$i]);
              if ($ok){
                $str_mess .= $h_id[$i] . ", ";
              }
            }
          $mess .= substr($str_mess, 0, - 2) . "</strong><br>";
          $err = $func->html_mess($mess);
        break;
      }
    }
		
			if((int)$vnT->input["do_display"]) {				
			$ok = $DB->query("Update videos_desc SET display=1 WHERE lang='$lang' AND video_id=".$vnT->input["do_display"]);
			if($ok){
				$mess .= "- " . $vnT->lang['display_success'] . "  ID: <strong>".$vnT->input["do_display"] . "</strong><br>";	
				$err = $func->html_mess($mess);
			}     
			//xoa cache
      $func->clear_cache();
		}
		
		if((int)$vnT->input["do_hidden"]) {				
			$ok = $DB->query("Update videos_desc SET display=0 WHERE lang='$lang' AND video_id=".$vnT->input["do_hidden"]);
			if($ok){
				$mess .= "- " . $vnT->lang['display_success'] . "  ID: <strong>".$vnT->input["do_hidden"] . "</strong><br>";	
				$err = $func->html_mess($mess);
			}    
		
			//xoa cache
      $func->clear_cache();
		}
		
    $p = ((int) $vnT->input['p']) ? $p = $vnT->input['p'] : 1;
    $n = ($conf['record']) ? $conf['record'] : 30;
    $cat_id = ((int) $vnT->input['cat_id']) ? $vnT->input['cat_id'] : 0;
    $keyword = ($vnT->input['keyword']) ? $vnT->input['keyword'] : "";
		$adminid = ((int) $vnT->input['adminid']) ?   $vnT->input['adminid'] : 0;
		$date_begin = ($vnT->input['date_begin']) ?  $vnT->input['date_begin'] : "";
		$date_end = ($vnT->input['date_end']) ?  $vnT->input['date_end'] : "";
    $ext_page = "";
    $where = "";
    if ($cat_id) {
      $a_cat_id = List_SubCat($cat_id);
      $a_cat_id = substr($a_cat_id, 0, - 1);
      if (empty($a_cat_id))
        $where .= " and FIND_IN_SET('$cat_id',cat_id)<>0 ";
      else {
        $tmp = explode(",", $a_cat_id);
        $str_ = " FIND_IN_SET('$cat_id',cat_id)<>0 ";
        for ($i = 0; $i < count($tmp); $i ++) {
          $str_ .= " or FIND_IN_SET('$tmp[$i]',cat_id)<>0 ";
        }
        $where .= " and (" . $str_ . ") ";
      }
      $ext_page .= "cat_id=$cat_id|";
      $ext .= "&cat_id=$cat_id";
    }
		if(!empty($adminid)){
			$where .=" and adminid=$adminid ";
			$ext_page.="adminid=$adminid|";
			$ext.="&adminid={$adminid}";
		}
		
		if($date_begin || $date_end )
		{
			$tmp1 = @explode("/", $date_begin);
			$time_begin = @mktime(0, 0, 0, $tmp1[1], $tmp1[0], $tmp1[2]);
			
			$tmp2 = @explode("/", $date_end);
			$time_end = @mktime(23, 59, 59, $tmp2[1], $tmp2[0], $tmp2[2]);
			
			$where.=" AND (date_post BETWEEN {$time_begin} AND {$time_end} ) ";
			$ext.="&date_begin=".$date_begin."&date_end=".$date_end;
			$ext_page.= "date_begin=".$date_begin."|date_end=".$date_end."|";
		}
		
    if (! empty($keyword)) {
      $where .= " AND video_name like '%$keyword%' ";
      $ext_page .= "keyword=$keyword|";
      $ext .= "&keyword={$keyword}";
    }
		$query = $DB->query("SELECT p.video_id
													FROM videos p, videos_desc pd
													WHERE p.video_id=pd.video_id 
													AND pd.lang='$lang'
													$where");
    $totals = intval($DB->num_rows($query));
    $num_pages = ceil($totals / $n);
    if ($p > $num_pages)
      $p = $num_pages;
    if ($p < 1)
      $p = 1;
    $start = ($p - 1) * $n;
    $nav = $func->paginate($totals, $n, $ext, $p);
    $table['link_action'] = $this->linkUrl . "{$ext}&p=$p";
		$ext_link = $ext."&p=$p" ;
    $table['title'] = array(
      'check_box' => "<input type=\"checkbox\" name=\"checkall\" id=\"checkall\" class=\"checkbox\" />|5%|center" , 
      'order' => $vnT->lang['order'] . "|8%|center" , 
      'picture' => $vnT->lang['picture'] . "|10%|center" , 
      'video_name' => $vnT->lang['title'] . "|40%|left" , 
      'cat_name' => "Danh mục ". "|20%|left",
 			'info' =>$vnT->lang['infomartion']. "|20%|left",      
       'action' => "Action|10%|center");
		
		$sql = "SELECT *
						FROM videos p, videos_desc pd
						WHERE p.video_id=pd.video_id 
						AND pd.lang='$lang'
						$where 
						ORDER BY video_order DESC ,p.video_id DESC  LIMIT $start,$n";
    //print "sql = ".$sql."<br>";
    $reuslt = $DB->query($sql);
    if ($DB->num_rows($reuslt)) {
      $row = $DB->get_array($result);
      for ($i = 0; $i < count($row); $i ++) {
				$row[$i]['ext_link'] = $ext_link ;
				$row[$i]['ext_page'] = $ext_page; 
        $row_info = $this->render_row($row[$i], $lang);
        $row_field[$i] = $row_info;
        $row_field[$i]['stt'] = ($i + 1);
        $row_field[$i]['row_id'] = "row_" . $row[$i]['video_id'];
        $row_field[$i]['ext'] = "";
      }
      $table['row'] = $row_field;
    } else {
      $table['row'] = array();
      $table['extra'] = "<div align=center class=font_err >" . $vnT->lang['no_have_video'] . "</div>";
    }
    $table['button'] = '<input type="button" name="btnHidden" value=" ' . $vnT->lang['hidden'] . ' " class="button" onclick="do_submit(\'do_hidden\')">&nbsp;';
    $table['button'] .= '<input type="button" name="btnDisplay" value=" ' . $vnT->lang['display'] . ' " class="button" onclick="do_submit(\'do_display\')">&nbsp;';
    $table['button'] .= '<input type="button" name="btnEdit" value=" ' . $vnT->lang['update'] . ' " class="button" onclick="do_submit(\'do_edit\')">&nbsp;';
    $table['button'] .= '<input type="button" name="btnDel" value=" ' . $vnT->lang['delete'] . ' " class="button" onclick="del_selected(\'' . $this->linkUrl . '&sub=del&ext=' . $ext_page . '\')">';
    $table_list = $func->ShowTable($table);
    $data['table_list'] = $table_list;
    $data['keyword'] = $keyword;
    $data['list_cat'] = Get_Cat($cat_id, $lang);
		$data['date_begin'] = $date_begin;
		$data['date_end'] = $date_end;
    $data['list_search'] = List_Search($search);
		if($vnT->admininfo['adminid']==1)
		{
			$data['list_admin'] = ' &nbsp; <strong> Admin post : </strong>  '.list_admin($adminid) ;
		}
		
    $data['keyword'] = $keyword;
    $data['totals'] = $totals;
    $data['err'] = $err;
    $data['nav'] = $nav;
    /*assign the array to a template variable*/
    $this->skin->assign('data', $data);
    $this->skin->parse("manage");
    return $this->skin->text("manage");
  }
  // end class
}
?>
