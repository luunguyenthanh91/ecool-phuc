function do_submit_move(action) {
	
	document.manage.do_action.value=action;
	cat_id = document.getElementById('cat_id').value;
	action_form = document.manage.action;
	if (selected_item()){
		action_form = action_form+'&cat_id='+cat_id;
		
		document.manage.action = action_form;
		document.manage.submit();
	}
	
}

function do_movecat(action){
	  cat_chose = getobj('cat_chose').value;
		if (cat_chose==0){
			alert('Vui lòng chọn danh mục cần chuyển đến');	
		}else{
			document.manage.do_action.value=cat_chose;
			if (selected_item()){
				document.manage.submit();
			}
		}
} 



/*--------------- LoadAjaxProduct ----------------*/
function LoadAjax(doAction,ext_display,str_param,p) {
//	alert( 'test' );

	$.ajax({
		 type: "GET",
		 url: "modules/video_ad/ajax/ajax_video.php",
		 data: "do="+doAction+'&'+str_param+'&p='+p ,
		 success: function(html){
			  $("#"+ext_display).html(html);
		 }
	 });
	
	return false;
}

function checkall()	{
	for ( i=0;i < document.manage.elements.length ; i++ ){
		if (document.manage.elements[i].type=="checkbox" && document.manage.elements[i].name!="all"){
			row_id = 'row_'+document.manage.elements[i].value;
		}else{
			row_id="";
		}
		
		if ( document.manage.all.checked==true ){
			document.manage.elements[i].checked = true;
			if (row_id!="" ){
				getobj(row_id).className = "row_select";
			}
		}
		else{
			document.manage.elements[i].checked  = false;
			if (row_id!="" ){
				if (i%2==0)
					getobj(row_id).className = "row1";
				else
					getobj(row_id).className = "row";
			}
		}
		
	}
}

function do_chage_video_type(select_id)
{
	$("select#"+select_id).change(function () {
		$("select#"+select_id+" option:selected").each(function () {
			if($(this).val()=="file" && $(".ext_upload_file #"+$(this).attr("id")+":checked").val()==1)
			{
				$(".btn_submit_f #btnSubmit").css({"display" : ""});	
				$(".btn_submit_f #btnSubmit_1").css({"display" : "none"
				});	
			}
			else
			{
				$(".btn_submit_f #btnSubmit").css({"display" : "none"});	
				$(".btn_submit_f #btnSubmit_1").css({"display" : ""
				});	
			}
		});
	})
	.trigger('change');
}

function do_chage_submit_buttun()
{
	var select_id = "video_type";
	$("select#"+select_id).change(function () {
		$("select#"+select_id+" option:selected").each(function () {
			if($(this).val()=="file")
			{
				$("#div_file_video").css({"display" : ""});	
				$("#div_url_video").css({"display" : "none"});
				$("#div_url_youtube").css({"display" : "none"});
				if(get_chk_upload_file()==1)
				{
					do_display_submit_buttun(1);
				}
				else
				{
					do_display_submit_buttun(0);
				}
			}
			else if($(this).val()=="url_youtube")
			{
				$("#div_file_video").css({"display" : "none"});	
				$("#div_url_video").css({"display" : "none"});	
				$("#div_url_youtube").css({"display" : ""});
			}
			else
			{
				$("#div_file_video").css({"display" : "none"});	
				$("#div_url_video").css({"display" : ""});
				$("#div_url_youtube").css({"display" : "none"});
				do_display_submit_buttun(0);
			}
		});
	})
	.trigger('change');
	
	$(".ext_upload_file .chk_upload_file").each(function() {
			if($(".ext_upload_file #"+$(this).attr("id")+":checked").val()==0)
			{
				do_display_submit_buttun(0);
			}
			else if($(".ext_upload_file #"+$(this).attr("id")+":checked").val()==1)
			{
				if(get_select_selected(select_id)=="file")
				{
					do_display_submit_buttun(1);
					
				}
				else
				{
					do_display_submit_buttun(0);
				}
			}
	});
	$(".ext_upload_file .chk_upload_file").click(function () {
		$(".ext_upload_file .chk_upload_file").each(function() {
				if($(".ext_upload_file #"+$(this).attr("id")+":checked").val()==0)
				{
					do_display_submit_buttun(0);
				}
				else if($(".ext_upload_file #"+$(this).attr("id")+":checked").val()==1)
				{
					if(get_select_selected(select_id)=="file")
					{
						do_display_submit_buttun(1);
					}
					else
					{
						do_display_submit_buttun(0);
					}
				}
		});
	});
}

function do_display_submit_buttun(display)
{
	if(display==1)
	{
		$(".btn_submit_f #btnSubmit").css({"display" : ""});
		$(".btn_submit_f #btnSubmit_1").css({"display" : "none"});
	}
	else
	{
		$(".btn_submit_f #btnSubmit").css({"display" : "none"});	
		$(".btn_submit_f #btnSubmit_1").css({"display" : ""});
	}
}

function do_display_div_video(display)
{
	if(display==1)
	{
		$("#div_file_video").css({"display" : ""});
		$(".btn_submit_f #btnSubmit_1").css({"display" : "none"});
	}
	else
	{
		$(".btn_submit_f #btnSubmit").css({"display" : "none"});	
		$(".btn_submit_f #btnSubmit_1").css({"display" : ""});
	}
}

function get_select_selected(select_id)
{
	var value = "";
	$("select#"+select_id+" option:selected").each(function () {
		value = $(this).val();
	});
	return value;
}

function get_chk_upload_file()
{
	var value = "";
	$(".ext_upload_file .chk_upload_file").each(function() {
		value = $(".ext_upload_file #"+$(this).attr("id")+":checked").val();
	});
	return value;
}
