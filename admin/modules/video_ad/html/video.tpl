<!-- BEGIN: edit -->
<script language=javascript>
$(document).ready(function() {
	$('#myForm').validate({
		rules: {			
				cat_id: {
					required: true
				},
				video_name: {
					required: true,
					minlength: 3
				}
	    },
	    messages: {
	    	cat_id: {
						required: "{LANG.err_select_required}"
				},
				video_name: {
						required: "{LANG.err_text_required}",
						minlength: "{LANG.err_length} 3 {LANG.char}" 
				} 
		}
	});
	do_chage_submit_buttun();
});

var swfu;

window.onload = function () {
	swfu = new SWFUpload({
		// Backend settings
		upload_url: "{DIR_JS}/swfupload_user/upload_video.php",
		file_post_name: "resume_file",
		post_params : {
			"adminid" : "{data.adminid}",
			"folderpath" : "{data.folderpath}",
			"folder_upload" : "{data.folder_upload}"
		},

		// Flash file settings
		file_size_limit : "10 MB",
		file_types : "*.flv;*.mp4",			// or you could use something like: "*.doc;*.wpd;*.pdf",
		file_types_description : "*.FLV, *.MP4 Files",
		file_upload_limit : "0",
		file_queue_limit : "1",

		// Event handler settings
		swfupload_loaded_handler : swfUploadLoaded,
		
		file_dialog_start_handler: fileDialogStart,
		file_queued_handler : fileQueued,
		file_queue_error_handler : fileQueueError,
		file_dialog_complete_handler : fileDialogComplete,
		
		//upload_start_handler : uploadStart,	// I could do some client/JavaScript validation here, but I don't need to.
		upload_progress_handler : uploadProgress,
		upload_error_handler : uploadError,
		upload_success_handler : uploadSuccess,
		upload_complete_handler : uploadComplete,

		// Button Settings
		button_image_url : "{DIR_JS}/swfupload_user/images/XPButtonUploadText_61x22.png",
		button_placeholder_id : "spanButtonPlaceholder",
		button_width: 61,
		button_height: 22,
		
		// Flash Settings
		flash_url : "{DIR_JS}/swfupload_user/swfupload.swf",

		custom_settings : {
			progress_target : "fsUploadProgress",
			upload_successful : false
		},
		
		// Debug settings
		debug: false
	});

};

</script>
{data.err}
<form action="{data.link_action}" method="post" enctype="multipart/form-data" name="myForm" id="myForm"  class="validate">
  <table width="100%"  border="0"  cellspacing="1" cellpadding="1" class="admintable">
    <tr class="form-required">
      <td width="20%" class="row1" nowrap="nowrap">{LANG.category}&nbsp;: </td>
      <td  align="left" class="row0">{data.list_cat}</td>
    </tr>	
    <tr class="form-required">
     <td class="row1">{LANG.title}: </td>
     <td class="row0"><input name="video_name" id="video_name" type="text" size="50" maxlength="250" value="{data.video_name}"></td>
    </tr>
    
    <tr>
      <td class="row1"> {LANG.picture} : </td>
      <td class="row0">
      {data.pic}<br>
      <div class="ext_upload">
      <input name="chk_upload" id="chk_upload" type="radio" value="0" checked> 
      Insert URL's image &nbsp; <input name="picture" type="text" size="50" maxlength="250" onchange="do_ChoseUpload('ext_upload',0);" > <br>
      <input name="chk_upload" id="chk_upload" type="radio" value="1"> Upload Picture &nbsp;&nbsp;&nbsp;
      <input name="image" type="file" id="image" size="30" maxlength="250" onclick="do_ChoseUpload('ext_upload',1);" >
      </div>
      </td>
    </tr>
    
    <td class="row1"><strong> {LANG.video_type} :</strong> </td>
		<td class="row0">
      {data.video_type}
      <div class="font_err">Lưu ý có tích hợp lấy hình từ <strong>youtube.com</strong> (Trong trường hợp không tải hình ở ô trên):</div>
      <div class="font_err">- Nếu chọn dạng URL Youtube: ở ô URL's Video dán <strong>đường dẫn</strong> của video. VD: http://www.youtube.com/watch?v=-SEXK0EhK0Y (Hệ thống sẽ tự động chuyển về dạng embed)</div>
      <div class="font_err">- Nếu chọn dạng Embed: ở ô URL's Video dán <strong>embed</strong> của video. VD: &lt;iframe width=&quot;650&quot; height=&quot;405&quot; src=&quot;http://www.youtube.com/embed/-SEXK0EhK0Y&quot; frameborder=&quot;0&quot; allowfullscreen&gt;&lt;/iframe&gt;</div>
    </td>
	</tr>
  
  <tr id="div_file_video">
      <td class="row1"><strong>File's Video :</strong> </td>
      <td  class="row0">
        <div class="fieldset">
        	{data.video_view}
      
          <div>
            <input type="text" id="txtFileName" disabled="true" style="border: solid 1px; background-color: #FFFFFF;" />
            <span id="spanButtonPlaceholder"></span>
            (10 MB max)
          </div>
          <div class="flash" id="fsUploadProgress">
            <!-- This is where the file progress gets shown.  SWFUpload doesn't update the UI directly.
                  The Handlers (in handlers.js) process the upload events and make the UI updates -->
          </div>
          <input type="hidden" name="hidFileID" id="hidFileID" value="" />
          <!-- This is where the file ID is stored after SWFUpload uploads the file and gets the ID back from upload.php -->
        </div>
      </td>
    </tr>
  
    <tr id="div_url_video">
      <td class="row1"><strong>URL's Video :</strong> </td>
      <td  class="row0"><textarea name="video_url" cols="" rows="3" style="width:100%" class="textarea">{data.video_url}</textarea></td>
    </tr>
    <tr id="div_url_youtube">
      <td class="row1" valign="top"><strong>URL's Video Youtube:</strong> </td>
      <td  class="row0">
        <input name="url_youtube" id="url_youtube" type="text" size="50" maxlength="250" value="{data.url_youtube}">
        <div>VD: http://www.youtube.com/watch?v=-SEXK0EhK0Y</div>
        <br />
        <table width="100%" border="0" cellpadding="0" cellspacing="0">
           <tr class="row_title" >
           <td  colspan="2" class="font_title" >Cài đặt giao diện hiển thị của video youtube: </td>
          </tr> 
          {data.op_view_video}
        </table>
      </td>
    </tr>
    {data.show_embed}
		
    <tr>
     <td class="row1">{LANG.description}: </td>
     <td class="row0">{data.html_content}</td>
    </tr>
    
    <tr>
     <td class="row1" >{LANG.display}: </td>
     <td class="row0">{data.list_display}</td>
    </tr>

    <tr>
     <td class="row1" >Video focus: </td>
     <td class="row0">{data.list_is_focus}</td>
    </tr>
    
		 <tr class="row_title" >
       <td  colspan="2" class="font_title" >Search Engine Optimization : </td>
      </tr> 
 
     <tr>
      <td class="row1" valign="top" style="padding-top:10px;">Friendly URL :</td>
      <td class="row0"><input name="friendly_url" id="friendly_url" type="text" size="50" maxlength="250" value="{data.friendly_url}" class="textfield"><br><span class="font_err">({LANG.mess_friendly_url})</span></td>
    </tr>
     <tr>
      <td class="row1" valign="top" >Friendly Title :</td>
      <td class="row0"><input name="friendly_title" id="friendly_title" type="text" size="60" maxlength="250" value="{data.friendly_title}" class="textfield"></td>
    </tr>
    <tr>
      <td class="row1">Meta Keyword :</td>
      <td class="row0"><input name="metakey" id="metakey" type="text" size="60" maxlength="250" value="{data.metakey}" class="textfield"></td>
    </tr>
    <tr>
      <td class="row1">Meta Description : </td>
      <td class="row0"><textarea name="metadesc" id="metadesc" rows="3" cols="50" style="width:90%" class="textarea">{data.metadesc}</textarea></td>
    </tr>	
		<tr align="center">
    <td class="row1" >&nbsp; </td>
			<td class="row0 btn_submit_f" >
				<input type="hidden" name="do_submit"	 value="1" />
				<input type="submit" name="btnAdd" value="Submit" class="button" id="btnSubmit_1">
        <input type="submit" name="btnAdd" value="Submit" class="button" id="btnSubmit">
				<input type="reset" name="Submit2" value="Reset" class="button">
			</td>
		</tr>
	</table>
</form>
 
<br>
<!-- END: edit -->


<!-- BEGIN: manage -->
<form action="{data.link_fsearch}" method="post" name="myform">
<table width="100%" border="0" cellspacing="2" cellpadding="2" align="center" class="tableborder">
 
  
   
  <tr>
    <td class="row1" ><strong>{LANG.view_day_from}:</strong>  </td>
    <td align="left" class="row0"> 
    <input type="text" name="date_begin" id="date_begin" value="{data.date_begin}" size="15" maxlength="10"    /> &nbsp;&nbsp; <strong>{LANG.view_day_to} :</strong> <input type="text" name="date_end" id="date_end" value="{data.date_end}" size="15" maxlength="10"   />   </td>
  </tr>
  <tr>
  
  <tr>
  <td align="left"><strong>{LANG.keyword}  :</strong> &nbsp;&nbsp;&nbsp;  </td>
  <td align="left">  </strong><input name="keyword"  value="{data.keyword}"type="text" size="20">
    <input name="btnSearch" type="submit" value=" Search " class="button"></td>
  </tr>
   <tr>
    <td width="15%" align="left" class="row1"><strong>{LANG.totals}:</strong> &nbsp;</td>
    <td width="85%" align="left" class="row0"><b class="font_err">{data.totals}</b></td>
  </tr>
  </table>
</form>

{data.err}
<br />
{data.table_list}
<br />
<table width="100%"  border="0" align="center" cellspacing="1" cellpadding="1" class="bg_tab">
  <tr>
    <td  height="25">{data.nav}</td>
  </tr>
</table>
 
<br />
<!-- END: manage -->