<!-- BEGIN: edit -->
<script language=javascript>

$(document).ready(function() {
	 
	$('#myForm').validate({
		rules: {			
				 
				cat_name: {
					required: true,
					minlength: 3
				}
				
	    },
	    messages: {
	    	
				cat_name: {
						required: "{LANG.err_text_required}",
						minlength: "{LANG.err_length} 3 {LANG.char}" 
				} 
		}
		
	});
}); 
 
	
</script> 
{data.err}
<form action="{data.link_action}" method="post" enctype="multipart/form-data" name="myForm"  class="validate">
  <table width="100%"  border="0"  cellspacing="1" cellpadding="1" class="admintable">
    <tr>
        <td   width="24%" class="row1"> {LANG.cat_parent}  : </td>
        <td  align="left" class="row0">{data.listcat}</td>
    </tr>
    
    <tr class="form-required">
     <td class="row1" width="20%">{LANG.cat_name}: </td>
     <td class="row0"><input name="cat_name" type="text" size="50" maxlength="250" value="{data.cat_name}" class="textfield"></td>
    </tr>
     
    <tr >
     <td class="row1" width="20%">{LANG.picture}: </td>
     <td class="row0">
     	{data.pic}
     <div class="ext_upload">
      <input name="chk_upload" id="chk_upload" type="radio" value="0" checked> 
      Insert URL's image &nbsp; <input name="picture" type="text" size="50" maxlength="250" onchange="do_ChoseUpload('ext_upload',0);" > <br>
      <input name="chk_upload" id="chk_upload" type="radio" value="1"> Upload Picture &nbsp;&nbsp;&nbsp;
      <input name="image" type="file" id="image" size="30" maxlength="250" onclick="do_ChoseUpload('ext_upload',1);" >
      </div>
    </td>
    </tr>    
 
     
    <tr class="row_title" >
     <td  colspan="2" class="font_title" >Search Engine Optimization : </td>
    </tr> 
 
     <tr>
      <td class="row1" valign="top" style="padding-top:10px;">Friendly URL :</td>
      <td class="row0"><input name="friendly_url" id="friendly_url" type="text" size="50" maxlength="250" value="{data.friendly_url}" class="textfield"><br><span class="font_err">({LANG.mess_friendly_url})</span></td>
    </tr>
     <tr>
      <td class="row1" valign="top" >Friendly Title :</td>
      <td class="row0"><input name="friendly_title" id="friendly_title" type="text" size="60" maxlength="250" value="{data.friendly_title}" class="textfield"></td>
    </tr>
    <tr>
      <td class="row1">Meta Keyword :</td>
      <td class="row0"><input name="metakey" id="metakey" type="text" size="60" maxlength="250" value="{data.metakey}" class="textfield"></td>
    </tr>
    <tr>
      <td class="row1">Meta Description : </td>
      <td class="row0"><textarea name="metadesc" id="metadesc" rows="3" cols="50" style="width:90%" class="textarea">{data.metadesc}</textarea></td>
    </tr>
   
			
		<tr align="center">
    <td class="row1" >&nbsp; </td>
			<td class="row0" >
				<input type="hidden" name="do_submit"	 value="1" />
				<input type="submit" name="btnAdd" value="Submit" class="button">
				<input type="reset" name="Submit2" value="Reset" class="button">
			</td>
		</tr>
	</table>
</form>


<br>
<!-- END: edit -->

<!-- BEGIN: move -->
<script language=javascript>
	function checkform(f) {			
	
		var cat1 = f.cat1.value;
		if (cat1 == 0) {
			alert('Vui chọn danh mục chuyển ');
			f.cat1.focus();
			return false;
		}
		
		var cat_chose = f.cat_chose.value;
		if (cat_chose == 0) {
			alert('Vui chọn danh mục muốn chuyển tới ');
			f.cat_chose.focus();
			return false;
		}
		
		return true;
	}
</script>
<form action="{data.link_action}" method="post" name="myForm"  class="validate" onSubmit="return checkform(this);" >
  <table width="100%"  border="0"  cellspacing="1" cellpadding="1" class="admintable">
    <tr>
      <td width="48%" class="row1"> {LANG.source_category} </td>
      <td class="row0" align="center">&nbsp;</td>
      <td width="48%" class="row1"> {LANG.des_category} </td>
    </tr>
    <tr class="form-required">
      <td width="48%" class="row1"> {data.list_cat} </td>
      <td class="row0" align="center"><strong>=></strong></td>
      <td width="48%" class="row1"> {data.list_cat_chose} </td>
    </tr>
    <tr>
      <td width="48%" class="row1"><div id="ext_cat1" class="font_err"></div></td>
      <td class="row0" align="center">&nbsp;</td>
      <td width="48%" class="row1"><div id="ext_cat2" class="font_err"></div></td>
    </tr>
    <tr>
      <td class="row0" colspan="3" align="center"><input type="submit" name="btnAll" value="{LANG.move_all}" class="button" />
      </td>
    </tr>
  </table>
  <br />
</form>
<div id="ext_list"></div>

<!-- END: move -->

<!-- BEGIN: manage -->
<form action="{data.link_fsearch}" method="post" name="fSearch">
  <table width="100%" border="0" cellspacing="2" cellpadding="2" align="center" class="tableborder">
    <tr>
      <td width="15%" align="left">{LANG.totals}: &nbsp;</td>
      <td width="85%" align="left"><b class="font_err">{data.totals}</b></td>
    </tr>
  </table>
</form>
{data.err} <br />
<form id="manage" name="manage" method="post" action="{data.link_action}">
  <table width="100%" border="0" cellspacing="0" cellpadding="0" class="bg_tbl">
    <tr>
      <td><table  border="0" cellspacing="2" cellpadding="2">
        <tr>
          <td width="40" align="center"><img src="{DIR_IMAGE}/arr_top.gif" width="17" height="17"></td>
          <td>{data.button}</td>
        </tr>
      </table></td>
    </tr>
    <tr>
      <td>
      <table cellspacing="1" class="adminlist">
        <thead>
          <tr height="25">
            <th width="5%" align="center" ><input type="checkbox" value="all" class="checkbox" name="checkall" id="checkall"/></th>            <th width="10%" align="center" >{LANG.order}</th>
            <th width="10%" align="center" >{LANG.picture}</th>            
            <th width="30%" align="left" >{LANG.cat_name}</th>
            <th width="30%" align="center" >Link</th>            
            <th width="10%" align="center" >Focus</th>            
            <th width="15%"  align="center" >Action</th>
          </tr>
        </thead>        
        <tbody>
          <!-- BEGIN: html_row -->
          <tr class="{row.class}" id="{row.row_id}">
            <td align="center" >{row.check_box}</td>
            <td align="center" >{row.order}</td>
            <td align="center" >{row.picture}</td>
            <td align="left" >{row.cat_name}</td>
            <td align="center" >{row.link_cat}</td>
            <td align="center" >{row.is_focus}</td>
            <td align="center" >{row.action}</td>
          </tr>
          <!-- END: html_row -->
          <!-- BEGIN: html_row_no -->
          <tr class="row0" >
            <td  colspan="7" align="center" class="font_err" >{mess}</td>
          </tr>
          <!-- END: html_row_no -->
        </tbody>
      </table>
      </td>
    </tr>
    <tr>
      <td ><table  border="0" cellspacing="2" cellpadding="2">
          <tr>
            <td width="40" align="center"><img src="{DIR_IMAGE}/arr_bottom.gif" ></td>
            <td>{data.button}</td>
          </tr>
        </table></td>
    </tr>
  </table>
  <input type="hidden" name="do_action" id="do_action" value="" >
</form>
<br />
<table width="100%"  border="0" align="center" cellspacing="1" cellpadding="1" class="bg_tab">
  <tr>
    <td  height="25">{data.nav}</td>
  </tr>
</table>
<br />
<!-- END: manage -->