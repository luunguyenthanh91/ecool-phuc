<!-- BEGIN: manage -->
{data.err}
<form action="{data.link_action}" method="post" name="f_setting" id="f_setting" >
<table width="100%"  border="0"  cellspacing="1" cellpadding="1" class="admintable">
	<tr class="row_title" >
		<td colspan="2" ><strong class="font_title">Search Engine Optimization (SEO):</strong></td>
	</tr>
   <tr>
      <td class="row1" valign="top" >Friendly Title :</td>
      <td class="row0"><input name="friendly_title" id="friendly_title" type="text" size="70" maxlength="250" value="{data.friendly_title}" class="textfield"></td>
    </tr>
    <tr>
      <td class="row1">Meta Keyword :</td>
      <td class="row0"><input name="metakey" id="metakey" type="text" size="70" maxlength="250" value="{data.metakey}" class="textfield"></td>
    </tr>
    <tr>
      <td class="row1">Meta Description : </td>
      <td class="row0"><textarea name="metadesc" id="metadesc" rows="3" cols="50" style="width:90%" class="textarea">{data.metadesc}</textarea></td>
    </tr>
</table>
<br>

<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center"  class="admintable">
	<tr class="row_title" >
		<td colspan="2" ><strong class="font_title">{LANG.setting_module_video}</strong></td>
	</tr>  
  
		<tr >
    <td width="30%" align="right" class="row1"> <strong>{LANG.nophoto} : </strong></td>
    <td  align="left" class="row0">{data.nophoto}</td>
  </tr>
	
	<tr >
    <td width="20%" align="right" class="row1"> <strong>{LANG.imgthumb_width} : </strong></td>
    <td  align="left" class="row0"><input name="imgthumb_width" type="text" size="20" maxlength="250" value="{data.imgthumb_width}" class="textfiled"></td>
  </tr>
	
	
	
	<tr >
    <td width="20%" align="right" class="row1"> <strong>{LANG.media_width} : </strong></td>
    <td  align="left" class="row0"><input name="media_width" type="text" size="20" maxlength="250" value="{data.media_width}" class="textfiled"></td>
  </tr>
	
	<tr >
    <td width="20%" align="right" class="row1"> <strong>{LANG.media_height} : </strong></td>
    <td  align="left" class="row0"><input name="media_height" type="text" size="20" maxlength="250" value="{data.media_height}" class="textfiled"></td>
  </tr>
	
	<tr >
    <td width="20%" align="right" class="row1"> <strong>{LANG.n_main} : </strong></td>
    <td  align="left" class="row0"><input name="n_main" type="text" size="20" maxlength="250" value="{data.n_main}" class="textfiled"></td>
  </tr>
  
  
	<tr >
    <td width="20%" align="right" class="row1"> <strong>{LANG.n_list} : </strong></td>
    <td  align="left" class="row0"><input name="n_list" type="text" size="20" maxlength="250" value="{data.n_list}" class="textfiled"></td>
  </tr>
	
	<tr >
    <td width="20%" align="right" class="row1"> <strong>{LANG.n_grid} : </strong></td>
    <td  align="left" class="row0"><input name="n_grid" type="text" size="20" maxlength="250" value="{data.n_grid}" class="textfiled"></td>
  </tr>
  
  
</table>
 
 
 

<p align="center">
<input type="hidden" name="do_submit" value="1">
<input name="btnEdit" type="submit" value="Submit" class="button">
</p>
</form>
<br />
<!-- END: manage -->

