<?php
/*================================================================================*\
|| 							Name code : funtions_dealer.php 		 			          	     		  # ||
||  				Copyright © 2007 by Thai Son - CMS vnTRUST                          # ||
\*================================================================================*/
/**
 * @version : 1.0
 * @date upgrade : 11/12/2007 by Thai Son
 **/
if (! defined('IN_vnT')) {
  die('Hacking attempt!');
}
define('MOD_DIR_UPLOAD', '../vnt_upload/dealer/');
define('MOD_ROOT_URL', $conf['rooturl'] . 'modules/dealer/');

function loadSetting ($lang="vn")
{
  global $vnT, $func, $DB, $conf;
  $setting = array();
  $result = $DB->query("select * from dealer_setting WHERE lang='$lang'");
  $setting = $DB->fetch_row($result);
  foreach ($setting as $k => $v) {
    $vnT->setting[$k] = stripslashes($v);
  }
  unset($setting);
}

//-----------------  get_catCode
function get_catCode($parentid,$cat_id) {
	global $vnT,$func,$DB,$conf;
	$text="";
	$sql ="select cat_id,cat_code from dealer_category where cat_id =$parentid ";
	$result =$DB->query ($sql);
	if ($row = $DB->fetch_row($result)){
		$text = $row['cat_code']."_".$cat_id;
	}else $text = $cat_id;
	
	return $text;
}

/*** Ham Get_Cat ****/
function Get_Cat($did=-1,$lang,$ext=""){
global $func,$DB,$conf;
	
	$text= "<select size=1 id=\"cat_id\" name=\"cat_id\" {$ext} >";
	$text.="<option value=\"0\">-- Root --</option>";
	$query = $DB->query("SELECT n.*,nd.cat_name FROM dealer_category n, dealer_category_desc nd
				where n.cat_id=nd.cat_id
				AND nd.lang='$lang' 
				AND n.parentid=0 
				ORDER BY n.cat_order ASC, n.cat_id DESC ");

	while ($cat=$DB->fetch_row($query)) {
		$cat_name = $func->HTML($cat['cat_name']);
		if ($cat['cat_id']==$did)
			$text.="<option value=\"{$cat['cat_id']}\" selected style='font-weight:bold;'>{$cat_name}</option>";
		else
			$text.="<option value=\"{$cat['cat_id']}\" style='font-weight:bold;' >{$cat_name}</option>";
		$n=1;
		$text.=Get_Sub($did,$cat['cat_id'],$n,$lang);
	}
	$text.="</select>";
	return $text;
}
/*** Ham Get_Sub   */
function Get_Sub($did,$cid,$n,$lang){
global $func,$DB,$conf;

	$output="";
	$k=$n;
	$query = $DB->query("SELECT n.*,nd.cat_name FROM dealer_category n, dealer_category_desc nd
				where n.cat_id=nd.cat_id
				AND nd.lang='$lang' 
				AND n.parentid=$cid
				ORDER BY n.cat_order ASC, n.cat_id DESC");
	while ($cat=$DB->fetch_row($query)) {
		$cat_name = $func->HTML($cat['cat_name']);
		
		if ($cat['cat_id']==$did)	{
			$output.="<option value=\"{$cat['cat_id']}\" selected>";
			for ($i=0;$i<$k;$i++) $output.= "|-- ";
			$output.="{$cat_name}</option>";
		}else{	
			$output.="<option value=\"{$cat['cat_id']}\" >";
			for ($i=0;$i<$k;$i++) $output.= "|-- ";
			$output.="{$cat_name}</option>";
		}
		$n=$k+1;
		$output.=Get_Sub($did,$cat['cat_id'],$n,$lang);
	}
	return $output;
}




//=============================List_Cat
function List_Cat_Root($did="",$ext){
global $func,$DB,$conf;
	
	$text= "<select name=\"cat_id\" id=\"cat_id\" {$ext}   >";
	$text.="<option value=\"\" selected> Chọn danh mục  </option>";
	$sql="SELECT n.*,nd.cat_name FROM dealer_category n, dealer_category_desc nd
				where n.cat_id=nd.cat_id
				AND nd.lang='$lang' 
				AND n.parentid=0 
				ORDER BY n.cat_order ASC, n.cat_id DESC ";
	$result = $DB->query ($sql);
	while ($row = $DB->fetch_row($result)){
		$cat_name = $func->HTML($row['cat_name'],$lang);
		if ($row['cat_id']==$did){
			$text .= "<option value=\"{$row['cat_id']}\" selected>".$cat_name."</option>";
		} else{
			$text .= "<option value=\"{$row['cat_id']}\">".$cat_name."</option>";
		}
	}
	
	$text.="</select>";
	return $text;
}

/*** Ham Get_Category ****/
function Get_Category ($selname, $did = -1, $lang, $ext = "")
{
  global $func, $DB, $conf;
  
  $text = "<select size=1 id=\"{$selname}\" name=\"{$selname}\" {$ext} class='select'>";
  
  $text .= "<option value=\"0\">-- Root --</option>";
  $query = $DB->query("SELECT n.*,nd.cat_name FROM dealer_category n, dealer_category_desc nd
				where n.cat_id=nd.cat_id
				AND nd.lang='$lang' 
				AND n.parentid=0 
				order by cat_order");
  while ($cat = $DB->fetch_row($query))
  {
    $cat_name = $func->HTML($cat['cat_name']);
    if ($cat['cat_id'] == $did)
      $text .= "<option value=\"{$cat['cat_id']}\" selected>{$cat_name}</option>";
    else
      $text .= "<option value=\"{$cat['cat_id']}\" >{$cat_name}</option>";
    $n = 1;
    $text .= Get_Sub_Category($selname, $did, $cat['cat_id'], $n, $lang);
  }
  $text .= "</select>";
  return $text;
}
/*** Ham Get_Sub   */
function Get_Sub_Category ($selname, $did, $cid, $n, $lang)
{
  global $func, $DB, $conf;
  
  $output = "";
  $k = $n;
  $query = $DB->query("SELECT n.*,nd.cat_name FROM dealer_category n, dealer_category_desc nd
				where n.cat_id=nd.cat_id
				AND nd.lang='$lang' 
				AND n.parentid=$cid 
				order by cat_order");
  while ($cat = $DB->fetch_row($query))
  {
    $cat_name = $func->HTML($cat['cat_name']);
    if ($cat['cat_id'] == $did)
    {
      $output .= "<option value=\"{$cat['cat_id']}\" selected>";
      for ($i = 0; $i < $k; $i ++)
        $output .= "|-- ";
      $output .= "{$cat_name}</option>";
    }
    else
    {
      $output .= "<option value=\"{$cat['cat_id']}\" >";
      for ($i = 0; $i < $k; $i ++)
        $output .= "|-- ";
      $output .= "{$cat_name}</option>";
    }
    $n = $k + 1;
    $output .= Get_Sub_Category($selname, $did, $cat['cat_id'], $n, $lang);
  }
  return $output;
} 

/**
 * Ham List_SubCat 
 *
 * @param	$cat_id : cat_id cua category
 *
 * @return	$output : 1 chuoi subcat (vd: 2,4,6,8,)  co dau , cuoi cung
 */
function List_SubCat ($cat_id)
{
  global $func, $DB, $conf;
  $output = "";
  $query = $DB->query("SELECT * FROM dealer_category WHERE parentid={$cat_id}");
  while ($cat = $DB->fetch_row($query)) {
    $output .= $cat["cat_id"] . ",";
    $output .= List_SubCat($cat['cat_id']);
  }
  return $output;
}
function get_cat_name ($cat_id, $lang){
  global $vnT, $func, $DB, $conf;
  $result = $DB->query("select cat_name from dealer_category_desc  where cat_id=$cat_id and lang='$lang'");
  if ($row = $DB->fetch_row($result)) {
    $cat_name = $func->HTML($row['cat_name']);
  }
  return $cat_name;
}
function List_Search ($did,$ext=""){
  global $func, $DB, $conf, $vnT;
	$arr_where = array('did'=> ' ID','title'=>  $vnT->lang['title'],'address'=> $vnT->lang['address'],'phone'=> $vnT->lang['phone'],'date_post' => $vnT->lang['date_post'] );
	$text= "<select size=1 name=\"search\" id='search' class='select' {$ext} >";
	foreach ($arr_where as $key => $value){
		$selected = ($did==$key) ? "selected"	: "";
		$text.="<option value=\"{$key}\" {$selected} > {$value} </option>";
	}	
	$text.="</select>";	
	 
  return $text;
}
function List_City ($country, $did = "", $ext = ""){
  global $vnT, $conf, $DB, $func;
  $text = "<select name=\"city\" id=\"city\" class='select'  {$ext}   >";
  $text .= "<option value=\"\" selected>-- Chọn thành phố --</option>";
  $sql = "SELECT * FROM iso_cities where display=1 and country='$country'  order by c_order ASC , name ASC  ";
  $result = $DB->query($sql);
  while ($row = $DB->fetch_row($result)) {
    if ($row['id'] == $did) {
      $text .= "<option value=\"{$row['id']}\" selected>" . $func->HTML($row['name']) . "</option>";
    } else {
      $text .= "<option value=\"{$row['id']}\">" . $func->HTML($row['name']) . "</option>";
    }
  }
  $text .= "</select>";
  return $text;
}
function get_city_name ($code){
  global $vnT, $func, $DB, $conf;
  $text = $code;
  $sql = "select name from iso_cities where id='$code' ";
  $result = $DB->query($sql);
  if ($row = $DB->fetch_row($result)) {
    $text = $func->HTML($row['name']);
  }
  return $text;
}
function List_State ($city, $did = "", $ext = ""){
  global $vnT, $conf, $DB, $func;
  $sql = "SELECT * FROM iso_states where display=1 and city={$city}  order by s_order ASC , name ASC ";
  $result = $DB->query($sql);
  if ($num = $DB->num_rows($result)) {
    $text = "<select name=\"state\" id=\"state\" class='select'  {$ext}   >";
    $text .= "<option value=\"\" selected>-- Chọn quận huyện --</option>";
    while ($row = $DB->fetch_row($result)) {
      if ($row['id'] == $did) {
        $text .= "<option value=\"{$row['id']}\" selected>" . $func->HTML($row['name']) . "</option>";
      } else {
        $text .= "<option value=\"{$row['id']}\">" . $func->HTML($row['name']) . "</option>";
      }
    }
    $text .= "</select>";
  } else {
    $text = '<input type="text" size="50" class="textfiled" value="' . $did . '" name="state" id="state" />';
  }
  return $text;
}
function get_state_name ($code){
  global $func, $DB, $conf, $vnT;
  $text = $code;
  $result = $DB->query("SELECT name FROM iso_states WHERE id='$code' ");
  if ($row = $DB->fetch_row($result)) {
    $text = $func->HTML($row['name']);
  }
  return $text;
}
function get_text_address ($info){
  global $vnT, $input;
  $text = $info['address'];
  if ($info['state'])
    $text .= ", " . get_state_name($info['state']);
  if ($info['city'])
    $text .= ", " . get_city_name($info['city']);
  return $text;
}
function List_Status ($did){
  global $func, $DB, $conf;
  $text = "<select size=1 name=\"status\">";
  if ($did == "0")
    $text .= "<option value=\"0\" selected> Liên hệ mới </option>";
  else
    $text .= "<option value=\"0\" > Liên hệ mới </option>";
  if ($did == "1")
    $text .= "<option value=\"1\" selected> Đã xác nhận </option>";
  else
    $text .= "<option value=\"1\"> Đã xác nhận </option>";
  if ($did == "2")
    $text .= "<option value=\"2\" selected> Hủy bỏ </option>";
  else
    $text .= "<option value=\"2\"> Hủy bỏ </option>";
  $text .= "</select>";
  return $text;
}
function list_admin ($did,$ext=""){
	global $func,$DB,$conf,$vnT;
	$text= "<select name=\"adminid\" id=\"adminid\"   class='select' {$ext} >";
	$text .= "<option value=\"\" selected>-- Chọn Admin --</option>";
	$sql="SELECT adminid,username	FROM admin order by  username ASC ";
	$result = $DB->query ($sql);
	$i=0;
	while ($row = $DB->fetch_row($result)){
		$i++;
		$username = $func->HTML($row['username']);
		$selected = ($did==$row['adminid']) ? " selected " : "";
		$text .= "<option value=\"{$row['adminid']}\" ".$selected." >  ".$username."  </option>";
	}
	$text.="</select>";
	return $text;
}
function get_admin($adminid) {
	global $vnT,$func,$DB,$conf;
	$text = "Admin";
	$sql ="select username from admin where adminid =$adminid  ";
	$result =$DB->query ($sql);
	if ($row = $DB->fetch_row($result)){
		$text = $func->HTML($row['username']);
	}
	return $text;
}


//------get_dir_upload
function get_dir_upload() {
global $vnT,$func,$DB,$conf;
	
	$dir="";
	//check parentid
	$res_ck = $vnT->DB->query("SELECT folder_id FROM media_folders WHERE folder_name='dealer'");
	if($row_ck = $DB->fetch_row($res_ck))
	{
		$parentid  = (int)$res_ck['folder_id'];
	}else{
		$cot['parentid']	=0;
		$cot['folder_path']	= "dealer";
		$cot['folder_name']	=	"dealer";
		$cot['date_create']	=	time();
		$cot['folder_type'] = "module";
		$vnT->DB->do_insert("media_folders",$cot);
		$parentid  = $vnT->DB->insertid();
	}
	
	// tao thu muc  
	switch ($vnT->setting['folder_upload'])
	{
		case 1 : 
		$dir = date("m_Y");
		$res_dir = $func->vnt_mkdir(MOD_DIR_UPLOAD , $dir,1);
		if($res_dir[0]==1)
		{
			$cot['parentid'] = $parentid;
			$cot['folder_path'] = "dealer/".$dir;
			$cot['folder_name'] = $dir;
			$cot['date_create'] = time();
			$DB->do_insert("media_folders",$cot);	
		}
		
		
		break ;
		case 2 : 
			$dir = date("d_m_Y");
			$res_dir = $func->vnt_mkdir(MOD_DIR_UPLOAD , $dir,1);
			if($res_dir[0]==1)
			{	
				$cot['parentid'] = $parentid;
				$cot['folder_path'] = "dealer/".$dir;
				$cot['folder_name'] = $dir;
				$cot['date_create'] = time();
				$DB->do_insert("media_folders",$cot);
			}
		break ;
		case 3 : 	
			$dir_thang = date("m_Y");
			$res_dir = $func->vnt_mkdir(MOD_DIR_UPLOAD , $dir_thang);
			if($res_dir[0]==1)
			{	
				$cot['parentid'] = $parentid;
				$cot['folder_path'] = "dealer/".$dir_thang;
				$cot['folder_name'] = $dir_thang;
				$cot['date_create'] = time();
				$DB->do_insert("media_folders",$cot);
				$parent_thang = $DB->insertid();	
				
				$dir_day = date("d") ;
				$res_dir1 = $func->vnt_mkdir(MOD_DIR_UPLOAD."/".$dir_thang , $dir_day,1);
				if($res_dir1[0]==1)
				{
					$cot['parentid'] = $parent_thang;
					$cot['folder_path'] = "dealer/".$dir_thang."/".$dir_day;
					$cot['folder_name'] = $dir_day;
					$cot['date_create'] = time();
					$DB->do_insert("media_folders",$cot);	
				}
			}
				
			$dir = $dir_thang."/".$dir_day;				
		 
		break ;
	} // end switch
	
	return $dir;
}
//-----------------  get_pic_thumb
function get_pic_thumb ($picture, $w = "")
{
  global $conf, $vnT;
  $out = "";
  if (! empty($picture)) {
    $out = "";
    $ext = "";
    $w_thumb = ($vnT->setting['imgthumb_width']) ? $vnT->setting['imgthumb_width'] : 100;
    $linkhinh = "../vnt_upload/dealer/" . $picture;
    $linkhinh = str_replace("//", "/", $linkhinh);
    $dir = substr($linkhinh, 0, strrpos($linkhinh, "/"));
    $pic_name = substr($linkhinh, strrpos($linkhinh, "/") + 1);
    if ($vnT->setting['thum_size']) {
      $src = $dir . "/thumbs/{$w_thumb}_" . $pic_name;
    } else {
      $src = $dir . "/thumbs/" . $pic_name;
    }
    if ($w < $w_thumb)
      $ext = " width='$w' ";
    $out = "<img  src=\"{$src}\" {$ext} >";
  } else {
    $out = "No Picture";
  }
  return $out;
}




///List_Utilities
function List_Utilities($did,$lang="vn")
{
	global $input,$conf,$vnT,$DB,$func;
	$list = '';
	$arr_checked = @explode(",",$did);

	$sql = "select * from dealer_utilities where display=1 order by display_order ASC, uid ASC ";
	//echo $sql;
	$result = $DB->query($sql);
	if($num = $DB->num_rows($result))
	{

		$list ='<table width="100%" border="0" cellspacing="2" cellpadding="2" align="center"><tr>';
		$i=0;$ngang=0;
		while ($row = $DB->fetch_row($result))
		{
			$title = $func->fetch_content($row['title'],$lang);
			$uid = $row['uid'];

			$checked ="";
			if (in_array($uid,$arr_checked)){
				$checked ="checked='checked'";
			}

			if($i%4==0){
				$list.='</tr><tr><td width="25%" ><input name="chUtilities[]" id="chOption" type="checkbox" value="'.$uid.'" '.$checked.'  />&nbsp;'.$title ;
				$ngang=0;
			}else{
				$list.='<td width="25%"><input name="chUtilities[]" id="chOption" type="checkbox" value="'.$uid.'" '.$checked.' />&nbsp;'.$title ;
			}
			$i++;$ngang++;

		}

		if ($ngang<4) $list.='<td>&nbsp;</td>';
		$list.='</tr></table>';


	}
	return $list;
}



?>