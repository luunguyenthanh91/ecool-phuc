<!-- BEGIN: manage -->
<form action="{data.link_action}" method="post" name="f_config" id="f_config" >
{data.err}
<table width="100%"  border="0"  cellspacing="1" cellpadding="1" class="admintable">
	<tr class="row_title" >
		<td colspan="2" ><strong class="font_title">Search Engine Optimization (SEO):</strong></td>
	</tr>
    <tr>
      <td class="row1" valign="top" >Modules slogan :</td>
      <td class="row0"><input name="slogan" id="slogan" type="text" size="70" maxlength="250" value="{data.slogan}" class="textfield"></td>
    </tr>
    <tr>
      <td class="row1" valign="top" >Friendly Title :</td>
      <td class="row0"><input name="friendly_title" id="friendly_title" type="text" size="70" maxlength="250" value="{data.friendly_title}" class="textfield"></td>
    </tr>
    <tr>
      <td class="row1">Meta Keyword :</td>
      <td class="row0"><input name="metakey" id="metakey" type="text" size="70" maxlength="250" value="{data.metakey}" class="textfield"></td>
    </tr>
    <tr>
      <td class="row1">Meta Description : </td>
      <td class="row0"><textarea name="metadesc" id="metadesc" rows="3" cols="50" style="width:90%" class="textarea">{data.metadesc}</textarea></td>
    </tr>
</table> 
<br>
<table width="100%"  border="0"  cellspacing="1" cellpadding="1" class="admintable">
	<tr class="row_title" >
		<td colspan="2" ><strong class="font_title">{LANG.setting_module}</strong></td>
	</tr>
	  
   
	<tr >
    <td  align="right" class="row1"> <strong>{LANG.n_main} : </strong></td>
    <td  align="left" class="row0"><input name="n_main" type="text" size="20" maxlength="250" value="{data.n_main}" class="textfiled"></td>
  </tr>
	
	<tr >
    <td  align="right" class="row1" width="30%"> <strong>{LANG.n_list} : </strong></td>
      <td  align="left" class="row0"><input name="n_list" type="text" size="20" maxlength="250" value="{data.n_list}" class="textfiled"></td>
  </tr>
 

  <tr>
    <td class="row1"  align="right">{LANG.nophoto} :</td>
    <td class="row0">{data.nophoto}</td>
  </tr>
  <tr >
   <td class="row1"  align="right" >Hình Nophoto : </td>
   <td class="row0">
    <div id="ext_pic_nophoto" class="picture" >{data.image_nophoto}</div>
        <input type="hidden" name="pic_nophoto"	 id="pic_nophoto" value="{data.pic_nophoto}" />
        <div id="btnU_pic_nophoto" class="div_upload" {data.style_upload} ><div class="button2"><div class="image"><a title="Add an Image" class="thickbox" id="add_image" href="{data.link_upload}" >Chọn hình</a></div></div></div>
  </td>
  </tr> 
 <tr>
    <td class="row1" align="right" >{LANG.folder_upload} :</td>
    <td class="row0">{data.folder_upload}</td>
  </tr>	
 
  <tr>
    <td class="row1"  align="right">{LANG.imgthumb_width} :</td>
    <td class="row0"><input name="imgthumb_width" size="20" type="text" class="textfiled" value="{data.imgthumb_width}" onKeyPress="return is_num(event,'imgthumb_width')"></td>
  </tr>
  <tr>
    <td  class="row1" align="right"> <strong>{LANG.detail_width_max} : </strong></td>
    <td class="row0"><input name="imgdetail_width" size="20" type="text" class="textfiled" value="{data.imgdetail_width}"  onkeypress="return is_num(event,'imgdetail_width')"></td>
  </tr>
  
  <tr>
    <td  class="row1"  align="right">{LANG.img_width} :</td>
    <td class="row0"><input name="img_width" size="20" type="text" class="textfiled" value="{data.img_width}"  onkeypress="return is_num(event,'img_width')"></td>
  </tr>
   
   
</table>
 
<p align="center">
			<input type="hidden" name="num" value="{data.num}">
      <input type="hidden" name="do_submit" value="1">
      <input type="submit" name="btnEdit" value="Update >>" class="button">
</p>
</form>
<br />
<!-- END: manage -->