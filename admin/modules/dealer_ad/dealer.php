<?php
/*================================================================================*\
|| 							Name code : dealer.php 		 		            	  ||
||  				Copyright @2008 by Thai Son - CMS vnTRUST                     ||
\*================================================================================*/
/**
 * @version : 2.0
 * @date upgrade : 09/01/2009 by Thai Son
 **/
if (! defined('IN_vnT')) {
  die('Hacking attempt!');
}
$vntModule = new vntModule();

class vntModule
{
  var $output = "";
  var $skin = "";
  var $linkUrl = "";
  var $module = "dealer";
  var $action = "dealer";

  function vntModule (){
    global $Template, $vnT, $func, $DB, $conf;
    require_once ("function_" . $this->module . ".php");
    $this->skin = new XiTemplate(DIR_MODULE.DS.$this->module."_ad" . DS . "html" . DS . $this->action . ".tpl");
    $this->skin->assign('LANG', $vnT->lang); 
    $this->skin->assign('CONF', $vnT->conf);
		$this->skin->assign('DIR_JS', $vnT->dir_js);
    $vnT->html->addScript("modules/" . $this->module . "_ad/js/" . $this->module . ".js");
    $lang = ($vnT->input['lang']) ? $lang = $vnT->input['lang'] : $func->get_lang_default();
    $this->linkUrl = "?mod=" . $this->module . "&act=" . $this->action . "&lang=" . $lang;
		loadSetting($lang);
    switch ($vnT->input['sub']) {
      case 'add':
        $nd['f_title'] = $vnT->lang['add_dealer'];
        $nd['content'] = $this->do_Add($lang);
      break;
      case 'edit':
        $nd['f_title'] = $vnT->lang['edit_dealer'];
        $nd['content'] = $this->do_Edit($lang);
      break;
      case 'del':
        $this->do_Del($lang);
      break;
      default:
        $nd['f_title'] = $vnT->lang['manage_dealer'];
        $nd['content'] = $this->do_Manage($lang);
      break;
    }
    $nd['menu'] = $func->getToolbar($this->module, $this->action, $lang);
    $nd['row_lang'] = $func->html_lang("?mod=" . $this->module . "&act=" . $this->action, $lang);
    $Template->assign("data", $nd);
    $Template->parse("box_main");
    $vnT->output .= $Template->text("box_main");
  }
  function do_Add ($lang){
    global $vnT, $func, $DB, $conf;
    $err = "";
		$w = ($vnT->setting['img_width']) ? $vnT->setting['img_width'] : 500;
    $w_thum = ($vnT->setting['imgthumb_width']) ? $vnT->setting['imgthumb_width'] : 100;
		$dir = $vnT->func->get_dir_upload_module($this->module,$vnT->setting['folder_upload']);
    if ($vnT->input['do_submit'] == 1) {
		  $data = $_POST;
			$cat_id = $vnT->input['cat_id'];
      $title = $vnT->input['title'];
			//$full_address = get_text_address ($vnT->input);
      $utilities = @implode(",",$_POST['chUtilities']);
			$picture =  $vnT->input['picture'];
      // Check for Error
      $res_chk = $DB->query("SELECT * FROM dealer_desc  WHERE title='{$title}' AND lang='$lang' ");
      if ($check = $DB->fetch_row($res_chk))  $err = $func->html_err("Name existed");
      // insert CSDL
      if (empty($err)) {
				$cot['cat_id'] = $cat_id ;	
        $cot['picture'] = $picture;
        
        $cot['link360'] = $vnT->input['link360'];
        
        $cot['date_post'] = time();
				$cot['date_update'] = time();
				$cot['adminid'] = $vnT->admininfo['adminid'];
        $ok = $DB->do_insert("dealer", $cot);
        if ($ok) {
          $did = $DB->insertid();
					$cot_d['did'] = $did;
					$cot_d['title'] = $title; 
					$cot_d['short'] = $vnT->input['short'];
					$cot_d['description'] = $DB->mySQLSafe($_POST['description']);
          $cot_d['work_time'] = $vnT->input['work_time'];
					//SEO
					$cot_d['friendly_url'] = (trim($vnT->input['friendly_url'])) ? trim($vnT->input['friendly_url']) :  $func->make_url($_POST['title']);
					$cot_d['friendly_title'] = (trim($vnT->input['friendly_title'])) ? trim($vnT->input['friendly_title']) :  $title ." | ".$func->utf8_to_ascii($title);
					$cot_d['metakey'] =  $vnT->input['metakey'];
					$cot_d['metadesc'] =  $vnT->input['metadesc'];
					$query_lang = $DB->query("select name from language ");
					while ( $row_lang = $DB->fetch_row($query_lang) ) {
						$cot_d['lang'] = $row_lang['name'];
						$DB->do_insert("dealer_desc",$cot_d);
					}
          //build seo_url
          $seo['sub'] = 'add';
          $seo['modules'] = $this->module;
          $seo['action'] = $this->module;
          $seo['name_id'] = "itemID";
          $seo['item_id'] = $did;
          $seo['friendly_url'] = $cot_d['friendly_url'];
          $seo['lang'] = $lang;
          $seo['query_string'] = "mod:".$seo['modules']."|act:".$seo['action']."|".$seo['name_id'].":".$did;
          $res_seo = $func->build_seo_url($seo);
          if ($res_seo['existed'] == 1) {
            $DB->query("UPDATE dealer_desc SET friendly_url='".$res_seo['friendly_url']."' WHERE did=".$did);
          }
 				  //xoa cache
          $func->clear_cache();
          //insert adminlog
          $func->insertlog("Add", $_GET['act'], $DB->insertid());
          $mess = $vnT->lang['add_success'];
          $url = $this->linkUrl . "&sub=add";
          $func->html_redirect($url, $mess);
        } else {
          $err = $func->html_err($vnT->lang['add_failt'] . $DB->debug());
        }
      }
    }
    $data['err'] = $err;
    $data['link_action'] = $this->linkUrl . "&sub=add";
		$data['listcat'] = Get_Cat($data['cat_id'], $lang);
		$data["html_content"] = $vnT->editor->doDisplay('description', $vnT->input['description'], '100%', '500', "Default",$this->module,$dir);
		$data['link_upload'] = '?mod=media&act=popup_media&module='.$this->module.'&folder='.$this->module.'/'.$dir.'&obj=picture&type=image&TB_iframe=true&width=900&height=474';
	  $data['map_lat'] = "10.804866895605";
    $data['map_lng'] = "106.64199984239";
    if (empty($data['city'])) $data['city'] = "02";		
    $data['list_city'] = List_City("VN", $data['city']," onChange=\"cityChange(this.value,'".$lang."')\"  style='width:350px;' ");
		$data['list_state'] = List_State($data['city'],$data['state'], " style='width:350px;' ");
    $data['list_utilities'] = List_Utilities ($data['utilities'],$lang);
    $data['maps'] = '<iframe height="500" frameborder="0" style="width:780px;" scrolling="no" border="false" noresize="" src="modules/dealer_ad/popup/edit_map.php" marginheight="0" marginwidth="0"/></iframe>';
		$this->skin->assign('data', $data);
    $this->skin->parse("edit");
    return $this->skin->text("edit");
  }
  function do_Edit ($lang){
    global $vnT, $func, $DB, $conf;
    $id = (int) $vnT->input['id'];
		$w_pic = ($vnT->setting['img_width']) ? $vnT->setting['img_width'] : 2000;
    $w_thumb = ($vnT->setting['imgthumb_width']) ? $vnT->setting['imgthumb_width'] : 200;
		$dir = $vnT->func->get_dir_upload_module($this->module,$vnT->setting['folder_upload']);
    if ($vnT->input['do_submit']) {
      $data = $_POST;
			$cat_id = $vnT->input['cat_id'];
      $title = $vnT->input['title'];
			$full_address = get_text_address ($vnT->input);
      $utilities = @implode(",",$_POST['chUtilities']);
			// Check for Error 
      $res_chk = $DB->query("SELECT * FROM dealer_desc  WHERE title='{$title}' and lang='$lang' and did<>$id" );
      if ($check = $DB->fetch_row($res_chk))   $err = $func->html_err("Name existed");
      if (empty($err)){
				$cot['cat_id'] = $cat_id ;
        $cot['picture'] = $vnT->input['picture'];
				
        $cot['link360'] = $vnT->input['link360'];
        $cot_d['short'] = $vnT->input['short'];
        $cot['date_update'] = time();
        $ok = $DB->do_update("dealer", $cot, "did=$id");
        if ($ok) {
					$cot_d['title'] = $title; 
				
					$cot_d['description'] = $DB->mySQLSafe($_POST['description']);
          $cot_d['work_time'] = $vnT->input['work_time'];
					//SEO
					$cot_d['friendly_url'] = (trim($vnT->input['friendly_url'])) ? trim($vnT->input['friendly_url']) :  $func->make_url($_POST['title']);
					$cot_d['friendly_title'] = (trim($vnT->input['friendly_title'])) ? trim($vnT->input['friendly_title']) :  $title ." | ".$func->utf8_to_ascii($title);
					$cot_d['metakey'] =  $vnT->input['metakey'];
					$cot_d['metadesc'] =  $vnT->input['metadesc'];
					$DB->do_update("dealer_desc",$cot_d," did={$id} and lang='{$lang}'");
          //build seo_url
          $seo['sub'] = 'edit';
          $seo['modules'] = $this->module;
          $seo['action'] = $this->module;
          $seo['name_id'] = "itemID";
          $seo['item_id'] = $id;
          $seo['friendly_url'] = $cot_d['friendly_url'];
          $seo['lang'] = $lang;
          $seo['query_string'] = "mod:".$seo['modules']."|act:".$seo['action']."|".$seo['name_id'] . ":" . $id;
          $res_seo = $func->build_seo_url($seo);
          if ($res_seo['existed'] == 1) {
            $DB->query("UPDATE dealer_desc SET friendly_url='".$res_seo['friendly_url']."' WHERE lang='".$lang."' AND did=" . $id);
          }
          //xoa cache
          $func->clear_cache();
          //insert adminlog
          $func->insertlog("Edit", $_GET['act'], $id);
          $err = $vnT->lang["edit_success"];
          $url = $this->linkUrl . "&sub=edit&id=$id";
          $func->html_redirect($url, $err);
        } else
          $err = $func->html_err($vnT->lang["edit_failt"] . $DB->debug());
      }
    }
    $sql = "SELECT * FROM dealer n, dealer_desc nd WHERE n.did=nd.did AND lang='$lang' AND n.did=$id ";
    $query = $DB->query($sql);
    if ($data = $DB->fetch_row($query)) {
      if ($data['picture']) {
        $data['pic'] = "<img src=\"".MOD_DIR_UPLOAD.$data['picture']."\" width=100 /><a href=\"javascript:del_picture('picture')\" class=\"del\">Xóa</a>";
        $data['style_upload'] = "style='display:none' ";
      } else {
        $data['pic'] = "";
      }
			$data['map_information'] = str_replace("\r\n", "<br>", $data['map_desc']);
			$data['listcat'] = Get_Cat($data['cat_id'], $lang);
    } else {
      $mess = $vnT->lang['not_found']." ID : ".$id;
      $url = $this->linkUrl;
      $func->html_redirect($url, $mess);
    }
		$data['list_city'] = List_City("VN", $data['city'], " onChange=\"cityChange(this.value,'".$lang."')\"  style='width:350px;' ");
		$data['list_state'] = List_State($data['city'],$data['state'], "   style='width:350px;' ");
		$data['list_utilities'] = List_Utilities ($data['utilities'],$lang);
    $data['maps'] = '<iframe height="500" frameborder="0" style="width:780px;" scrolling="no" border="false" noresize="" src="modules/dealer_ad/popup/edit_map.php?id=' . $id . '" marginheight="0" marginwidth="0"/></iframe>';
    $data['err'] = $err;
    $data['link_action'] = $this->linkUrl . "&sub=edit&id=$id";
		$data["html_content"] = $vnT->editor->doDisplay('description', $data['description'], '100%', '500', "Default",$this->module,$dir);
		$data['link_upload'] = '?mod=media&act=popup_media&type=modules&module='.$this->module.'&folder='.$this->module.'/'.$dir.'&obj=picture&TB_iframe=true&width=900&height=474';
    $this->skin->assign('data', $data);
    $this->skin->parse("edit");
    return $this->skin->text("edit");
  }
  function do_Del ($lang){
    global $func, $DB, $conf, $vnT;
		$id = (int) $vnT->input['id'];
		$ext = $vnT->input["ext"];
		$del = 0;
		$qr = "";
		if ($id != 0) {
			$ids = $id;
		}
		if (isset($vnT->input["del_id"])){
			$ids = implode(',', $vnT->input["del_id"]);
		}
		$res = $DB->query("SELECT * FROM dealer WHERE did IN (" . $ids . ") ");
    if ($DB->num_rows($res)){			
      while ($row = $DB->fetch_row($res))  {
        $res_d = $DB->query("SELECT id FROM dealer_desc WHERE did=".$row['did']." AND lang<>'".$lang."' ");
				if(!$DB->num_rows($res_d)){
					$DB->query("DELETE FROM dealer WHERE did=".$row['did']."  "); 
				}	
				$DB->query("DELETE FROM dealer_desc WHERE did=".$row['did']." AND lang='".$lang."' ");	
      }
      //build seo_url
      $seo['sub'] = 'del';
      $seo['modules'] = $this->module;
      $seo['action'] = $this->module;
      $seo['item_id'] = $ids;
      $seo['lang'] = $lang;
      $res_seo = $func->build_seo_url($seo);
      $mess = $vnT->lang["del_success"];
			//xoa cache
      $func->clear_cache();
    } else  {
      $mess = $vnT->lang["del_failt"];
    }
		$ext_page = str_replace("|", "&", $ext);
		$url = $this->linkUrl . "&{$ext_page}";
		$func->html_redirect($url, $mess);
  }
  function render_row ($row_info, $lang){
    global $func, $DB, $conf, $vnT;
    $row = $row_info;
    // Xu ly tung ROW
    $id = $row['did'];
    $row_id = "row_" . $id;
    $output['check_box'] = vnT_HTML::checkbox("del_id[]", $id, 0, " ");
    $link_edit = $this->linkUrl . '&sub=edit&id=' . $id . '&ext=' . $row['ext_page'];
    $link_del = "javascript:del_item('" . $this->linkUrl . "&sub=del&id=" . $id . "&ext=" . $row['ext_page'] . "')";
    $link_pic = "?mod=dealer&act=pic_dealer&id={$id}&lang=$lang&sub=edit";
    if ($row['picture']) {
      $output['picture'] = "<img src=\"" . MOD_DIR_UPLOAD . $row['picture'] . "\" width=50 />";
    } else
      $output['picture'] = "No image";
    $output['title'] = "<a href=\"{$link_edit}\"><strong>" . $func->HTML($row['title']) . "</strong></a>";
    $output['date_post'] = date("d/m/Y", $row['date_post']);
    $output['order'] = "<input name=\"txt_Order[{$id}]\" type=\"text\" size=\"2\" maxlength=\"2\" style=\"text-align:center\" value=\"{$row['d_order']}\"  onkeypress=\"return is_num(event,'txtOrder')\" onchange='javascript:do_check($id)' />";
		$link_display = $this->linkUrl.$row['ext_link']; 		
    if ($row['display'] == 1) {
      $display = "<a href='".$link_display."&do_hidden=$id' title='".$vnT->lang['click_do_hidden']."' ><img src=\"" . $vnT->dir_images . "/dispay.gif\" width=15  /></a>";
    } else {
      $display = "<a href='".$link_display."&do_display=$id'  title='".$vnT->lang['click_do_display']."' ><img src=\"" . $vnT->dir_images . "/nodispay.gif\"  width=15 /></a>";
    }
    $output['is_focus'] = vnT_HTML::list_yesno("is_focus[{$id}]",$row['is_focus'], "onchange='javascript:do_check($id)'");
    $output['pic_other'] = '<a href="'.$link_pic.'"><img src="'.$vnT->dir_images.'/ico_hinhanh.gif"alt="Picture "></a>';
    $output['info'] = "<strong>".$vnT->lang['address']." :</strong> " . get_text_address($row);
    $output['info'] .= "<br><strong>".$vnT->lang['phone']." :</strong> " . $row['phone'];
    $output['action'] = '<input name=h_id[]" type="hidden" value="' . $id . '" />';
    $output['action'] .= '<a href="' . $link_edit . '"><img src="' . $vnT->dir_images . '/edit.gif"  alt="Edit "></a>&nbsp;';
    $output['action'] .= $display . '&nbsp;';
    $output['action'] .= '<a href="' . $link_del . '"><img src="' . $vnT->dir_images . '/delete.gif"  alt="Delete "></a>';
    return $output;
  }
  function do_Manage ($lang){
    global $vnT, $func, $DB, $conf;
    //update
    if ($vnT->input["do_action"]) {
      //xoa cache
      $func->clear_cache();
      if ($vnT->input["del_id"])
        $h_id = $vnT->input["del_id"];
      switch ($vnT->input["do_action"]) {
        case "do_edit":
          if (isset($vnT->input["txt_Order"])) $arr_order = $vnT->input["txt_Order"];
          if (isset($vnT->input["is_focus"])) $arr_focus = $vnT->input["is_focus"];
          $mess .= "- " . $vnT->lang['edit_success'] . " ID: <strong>";
          $str_mess = "";
          for ($i = 0; $i < count($h_id); $i ++) {
            $dup['d_order'] = $arr_order[$h_id[$i]];
            $dup['is_focus'] = $arr_focus[$h_id[$i]];
            $ok = $DB->do_update("dealer", $dup, "did={$h_id[$i]}");
            if ($ok) {
              $str_mess .= $h_id[$i] . ", ";
            }
          }
          $mess .= substr($str_mess, 0, - 2) . "</strong><br>";
          $err = $func->html_mess($mess);
        break;
        case "do_hidden":
          $mess .= "- " . $vnT->lang['hidden_success'] . " ID: <strong>";
          for ($i = 0; $i < count($h_id); $i ++) {
            $dup['display'] = 0;
            $ok = $DB->do_update("dealer_desc", $dup,  "did={$h_id[$i]} AND lang='$lang'");
            if ($ok) {
              $str_mess .= $h_id[$i] . ", ";
            }
          }
          $mess .= substr($str_mess, 0, - 2) . "</strong><br>";
          $err = $func->html_mess($mess);
        break;
        case "do_display":
          $mess .= "- " . $vnT->lang['display_success'] . "  ID: <strong>";
          for ($i = 0; $i < count($h_id); $i ++) {
            $dup['display'] = 1;
            $ok = $DB->do_update("dealer_desc", $dup, "did={$h_id[$i]} AND lang='$lang'");
            if ($ok) {
              $str_mess .= $h_id[$i] . ", ";
            }
          }
          $mess .= substr($str_mess, 0, - 2) . "</strong><br>";
          $err = $func->html_mess($mess);
        break;
      }
    }
		if((int)$vnT->input["do_display"]) {				
			$ok = $DB->query("Update dealer_desc SET display=1 WHERE lang='$lang' AND did=".$vnT->input["do_display"]);
			if($ok){
				$mess .= "- " . $vnT->lang['display_success'] . "  ID: <strong>".$vnT->input["do_display"] . "</strong><br>";	
				$err = $func->html_mess($mess);
			}        
			//xoa cache
      $func->clear_cache();
		}
		if((int)$vnT->input["do_hidden"]) {				
			$ok = $DB->query("Update dealer_desc SET display=0 WHERE lang='$lang' AND did=".$vnT->input["do_hidden"]);
			if($ok){
				$mess .= "- " . $vnT->lang['display_success'] . "  ID: <strong>".$vnT->input["do_hidden"] . "</strong><br>";	
				$err = $func->html_mess($mess);
			}    
			//xoa cache
      $func->clear_cache();
		}
    $p = ((int) $vnT->input['p']) ? $p = $vnT->input['p'] : 1;
    $n = ($conf['record']) ? $conf['record'] : 30;
    $cat_id = ((int) $vnT->input['cat_id']) ? $vnT->input['cat_id'] : 0;
		$city = ($vnT->input['city']) ?  $vnT->input['city'] : 0;
		$search = ($vnT->input['search']) ? $vnT->input['search'] : "did";
    $keyword = ($vnT->input['keyword']) ? $vnT->input['keyword'] : "";
    $where = " ";
    $ext = "";
    if (! empty($cat_id)) {
      $subcat = List_SubCat($cat_id);
      if (! empty($subcat)) {
        $subcat = substr($subcat, 0, - 1);
        $a_cat_id = $cat_id . "," . $subcat;
        $a_cat_id = str_replace(",", "','", $a_cat_id);
        $where .= " and cat_id in ('" . $a_cat_id . "') ";
      } else {
        $where .= " and cat_id = $cat_id ";
      }
      $ext_page .= "cat_id=$cat_id|";
      $ext = "&cat_id=$cat_id";
    }
		if($city) {
			$where .= " and city = '$city' ";
			$ext_page .= "city=$city|";
      $ext = "&city=$city";
		}
		if(!empty($keyword)){
			switch($search){
				case "did" : $where .=" and  n.did = $keyword ";   break;
				case "date_post" : $where .=" and DATE_FORMAT(FROM_UNIXTIME(date_post),'%d/%m/%Y') = '{$keyword}' "; break;
				default :$where .=" and $search like '%$keyword%' ";break;		
			}
			$ext_page.="keyword=$keyword|keyword=$keyword|";
			$ext.="&search={$search}&keyword={$keyword}";
		}
    $query = $DB->query("SELECT n.did FROM  dealer n, dealer_desc nd  WHERE n.did=nd.did AND lang='$lang' $where  ");
    $totals = intval($DB->num_rows($query));
    $num_pages = ceil($totals / $n);
    if ($p > $num_pages)     $p = $num_pages;
    if ($p < 1)    $p = 1;
    $start = ($p - 1) * $n;
    $nav = $func->paginate($totals, $n, $ext, $p);
    $table['link_action'] = $this->linkUrl . "{$ext}&p=$p";
		$ext_link = $ext."&p=$p" ;
    $table['title'] = array(
      'check_box' => "<input type=\"checkbox\" name=\"checkall\" id=\"checkall\" class=\"checkbox\" />|5%|center" , 
      'order' => $vnT->lang['order']." |10%|center" , 
      'picture' => $vnT->lang['picture']."|10%|center" , 
      'title' => $vnT->lang['title']."|25%|left" , 
      'action' => "Action|10%|center");
    $sql = "SELECT * FROM  dealer n, dealer_desc nd  
					WHERE n.did=nd.did 
	 				AND lang='$lang' 
	 				$where 
					ORDER BY  d_order ASC, date_post DESC  LIMIT $start,$n";
    //print "sql = ".$sql."<br>";
    $reuslt = $DB->query($sql);
    if ($DB->num_rows($reuslt)) {
      $row = $DB->get_array($result);
      for ($i = 0; $i < count($row); $i ++) {
				$row[$i]['ext_link'] = $ext_link ;
				$row[$i]['ext_page'] = $ext_page;
        $row_info = $this->render_row($row[$i], $lang);
        $row_field[$i] = $row_info;
        $row_field[$i]['stt'] = ($i + 1);
        $row_field[$i]['row_id'] = "row_" . $row[$i]['did'];
        $row_field[$i]['ext'] = "";
      }
      $table['row'] = $row_field;
    } else {
      $table['row'] = array();
      $table['extra'] = "<div align=center class=font_err >" . $vnT->lang['no_have_dealer'] . "</div>";
    }
    $table['button'] = '<input type="button" name="btnHidden" value=" ' . $vnT->lang['hidden'] . ' " class="button" onclick="do_submit(\'do_hidden\')">&nbsp;';
    $table['button'] .= '<input type="button" name="btnDisplay" value=" ' . $vnT->lang['display'] . ' " class="button" onclick="do_submit(\'do_display\')">&nbsp;';
    $table['button'] .= '<input type="button" name="btnEdit" value=" ' . $vnT->lang['update'] . ' " class="button" onclick="do_submit(\'do_edit\')">&nbsp;';
    $table['button'] .= '<input type="button" name="btnDel" value=" ' . $vnT->lang['delete'] . ' " class="button" onclick="del_selected(\'' . $this->linkUrl . '&sub=del&ext=' . $ext_page . '\')">';
    $table_list = $func->ShowTable($table);
    $data['table_list'] = $table_list;
    $data['totals'] = $totals;
    $data['list_cat'] = Get_Cat($cat_id, $lang, "");
		$data['list_city'] = List_City("VN", $city );
		$data['list_search'] = List_Search($search);
		$data['keyword'] = $keyword;
		
    $data['err'] = $err;
    $data['nav'] = $nav;
    /*assign the array to a template variable*/
    $this->skin->assign('data', $data);
    $this->skin->parse("manage");
    return $this->skin->text("manage");
  }
}
?>
