
<!-- BEGIN: main -->
 
<table width="100%" border="0" cellspacing="2" cellpadding="2">
  <tr>
    <td align="center">{data.wellcome}</td>
  </tr>
</table>
{data.mess_backup}

<div class="dashboard-widgets-wrap"> 
	<div class="dashboard-box">
  	
    {data.box_contact}
    {data.box_mod_widget}    
    {data.box_adminlog}
  </div>
  
  <div class="dashboard-box">
  	{data.box_system}
    
  	{data.box_statistics} 	
   
  </div>
  
  <div class="clear"></div>
</div>
<!-- END: manage -->