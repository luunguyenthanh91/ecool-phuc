<?php
/*================================================================================*\
|| 							Name code : estore.php 		 		            	  ||
||  				Copyright @2008 by Thai Son - CMS vnTRUST                     ||
\*================================================================================*/
/**
 * @version : 2.0
 * @date upgrade : 09/01/2009 by Thai Son
 **/
if (! defined('IN_vnT'))
{
  die('Hacking attempt!');
}

//load Model
include(dirname(__FILE__) . "/functions.php");

class vntModule extends Model
{
  var $output = "";
  var $skin = "";
  var $linkUrl = "";
  var $module = "news";
	var $action = "comment";
	
  /**
   * function vntModule ()
   * Khoi tao 
   **/
  function vntModule ()
  {
    global $Template, $vnT, $func, $DB, $conf;

    $this->skin = new XiTemplate(DIR_MODULE . DS . $this->module . "_ad" . DS . "html" . DS . $this->action . ".tpl");
    $this->skin->assign('LANG', $vnT->lang);
    
    $lang = ($vnT->input['lang']) ? $lang = $vnT->input['lang'] : $func->get_lang_default();
    $this->linkUrl = "?mod=".$this->module."&act=".$this->action."&lang=" . $lang;
 
    switch ($vnT->input['sub'])
    {
      
      case 'edit':
        $nd['f_title'] = "Cập nhật ý kiến / phản hồi";
        $nd['content'] = $this->do_Edit($lang);
        break;
			case 'del':
        $this->do_Del($lang);
        break;				
			case 'sub_comment':
        $this->skin->assign("DIR_IMAGE", $vnT->dir_images);
        $this->skin->assign("DIR_STYLE", $vnT->dir_style);
        $this->skin->assign("DIR_JS", $vnT->dir_js);
        flush();
        echo $this->do_subComment($lang);
        exit();
        break;
      default:
        $nd['f_title'] = "Quản lý Ý kiến , Bình luận ";
        $nd['content'] = $this->do_Manage($lang);
        break;
    }
    $nd['menu'] = $func->getToolbar_Small($this->module,$this->action, $lang);
    $nd['row_lang'] = $func->html_lang("?mod=".$this->module."&act=".$this->action, $lang);
		
    $Template->assign("data", $nd);
    $Template->parse("box_main");
    $vnT->output .= $Template->text("box_main");
  
  }
  /**
   * function do_Edit 
   * Cap nhat 
   **/
  function do_Edit ($lang)
  {
    global $vnT, $func, $DB, $conf;
    $id = (int) $vnT->input['id'];
    
    if ($vnT->input['do_submit'])
    {
      $data = $_POST;			 
			$mark = $vnT->input['mark'] ;
			$item_id =  (int)$vnT->input['item_id'];


 			$cot['name'] = $vnT->input['name'];
			$cot['email'] = $vnT->input['email'];
			$cot['content'] = $func->txt_HTML($_POST['content']);
			$cot['display'] = $vnT->input['display'];
			// more here ...
			$ok = $DB->do_update("news_comment", $cot, "cid=$id");
			if ($ok)
			{

				///admin reply
        if($_POST['content_rep']){
          $cot_d['parentid'] = $id;
          $cot_d['id'] = $item_id;
          $cot_d['name'] = $vnT->input['name_rep'];
          $cot_d['email'] = $vnT->input['email_rep'];
          $cot_d['content'] = $func->txt_HTML($_POST['content_rep']);
          $cot_d['display'] = 1;
          $cot_d['date_post'] = time();
          $cot_d['adminid'] = $vnT->admininfo['adminid'];
          $DB->do_insert("news_comment", $cot_d);
        }


				//xoa cache
				$func->clear_cache();
				//insert adminlog
				$func->insertlog("Edit", $_GET['act'], $id);

				$err = $vnT->lang["edit_success"];
				$url = $this->linkUrl . "&sub=edit&id=$id";
				$func->html_redirect($url, $err);
			} else
				$err = $func->html_err($vnT->lang["edit_failt"].$DB->debug());
     
    }
    
    $query = $DB->query("SELECT * FROM news_comment WHERE cid=$id");
    if ($data = $DB->fetch_row($query))
    {
			
			$sql_item = "SELECT n.* , nd.title
					FROM news n, news_desc nd
					WHERE n.newsid=nd.newsid 
					AND nd.lang='$lang'
					AND n.newsid=".$data['id'];
	//	echo  "sql = $sql <br>";
			$res_item = $DB->query ($sql_item);
			if ($row_item = $DB->fetch_row($res_item)){

				$data['item_pic'] = ($row_item['picture'])  ? '<img src="'.MOD_DIR_UPLOAD.'/'.$row_item['picture'].'" height=50 />' : 'No Image' ;
				$data['item_id'] = $row_item['newsid'];
				$data['item_title'] = $func->HTML($row_item['title']);
				$data['item_maso'] = $row_item['maso'];
				$data['numvote'] = $row_item['numvote'];
				$data['votes'] = $row_item['votes'];
			}
			
			$data['list_hidden_email'] =  vnT_HTML::list_yesno ("hidden_email",$data['hidden_email']) ;
			$data['list_mark'] = vnT_HTML::selectbox("mark", array( '1' => '1 Sao' , '2' => '2 Sao' , '3' => '3 Sao','4' => '4 Sao','5' => '5 Sao'
    ), $data['mark']);
			
			$data['list_display'] =  vnT_HTML::selectbox("display", array(  '0' => 'Chưa xét duyệt' , '1' => 'Đã xét duyệt'  ), $data['display']);

      $data['name_rep'] =  $vnT->admininfo['username'];
      $data['email_rep'] =  $vnT->admininfo['email'];
    }else{
      $mess = $vnT->lang['not_found'] . " ID : " . $id;
      $url = $this->linkUrl;
      $func->html_redirect($url, $mess);
    }
     
		
		
    $data['err'] = $err;
    $data['link_action'] = $this->linkUrl . "&sub=edit&id=$id";
    
    /*assign the array to a template variable*/
    $this->skin->assign('data', $data);
    
    $this->skin->parse("edit");
    return $this->skin->text("edit");
  }
  
  /**
   * function do_Del 
   * Xoa 1 ... n  gioi thieu 
   **/
  function do_Del ($lang)
  {
    global $func, $DB, $conf, $vnT;

		$id = (int) $vnT->input['id'];
		$ext = $vnT->input["ext"];
		$del = 0;
		$qr = "";
			
		if ($id != 0) {
			$ids = $id;
		}
		if (isset($vnT->input["del_id"])){
			$ids = implode(',', $vnT->input["del_id"]);
		} 		

		
		$query = 'DELETE FROM news_comment WHERE cid IN ('.$ids.')' ;
		if ($ok = $DB->query($query))
		{
			$DB->query('DELETE FROM news_comment WHERE parentid IN ('.$ids.')' ) ;
			
			$mess = $vnT->lang["del_success"] ;
		} else
			$mess = $vnT->lang["del_failt"] ;
		$ext_page = str_replace("|", "&", $ext);
		$url = $this->linkUrl . "&{$ext_page}";
		$func->html_redirect($url, $mess);
  }
  
  /**
   * function render_row 
   * list cac record
   **/
  function render_row ($row_info, $lang)
  {
    global $func, $DB, $conf, $vnT;
    $row = $row_info;
    // Xu ly tung ROW
    $id = $row['cid'];
    $row_id = "row_" . $id;
    $output['check_box'] = vnT_HTML::checkbox("del_id[]", $id, 0, " ");
    $link_edit = $this->linkUrl.'&sub=edit&id=' . $id . '&ext=' . $row['ext_page'];
    $link_del = "javascript:del_item('" . $this->linkUrl . "&sub=del&id=" . $id . "&ext=" . $row['ext_page'] . "')";
    $link_item = "?mod=news&act=news&sub=edit&id={$row['id']}&lang=$lang";
		
		
		
		$output['customer'] =  "<b>".$func->HTML($row['name'])."</b>"; 
		$output['customer'] .=  "<div style='padding:2px 0px;'>Email: ".$row['email'].'</div>'; 
		$output['customer'] .=  "<div style='padding:2px 0px;'>Lúc : ".@date("H:i, d/m/Y",$row['date_post']).'</div>'; 
		$output['item_title'] .= "<b><a href='".$link_item."' target='_blank' >".$func->HTML($row['item_title'])."</a></b>";

    $output['sub_comment']  = '<a title="Ý kiến" class="thickbox" id="option_search" href="?mod=news&act=comment&sub=sub_comment&cid=' . $id . '&lang=' . $lang . '&TB_iframe=true&width=700&height=500"  ><b class="font_err" >'.(int)$row['num_sub'].'</b> Ý kiến</a>' ; 
 		
		$output['content'] = $func->HTML($row['content']);
    $res= $vnT->DB->query("select subid from news_comment_sub where cid=".$id." AND display=0");
    if($num = $vnT->DB->num_rows($res))
    {
      $output['content'] .= "<p><b>Có <font class='font_err'>".$num."</font> ý kiến chưa xét duyệt</b></p>";

    }
		
		if ($row['display']==1){
			$display ="<img src=\"{$vnT->dir_images}/dispay.gif\" width=15  />" ;
		}else{
			$display ="<img src=\"{$vnT->dir_images}/nodispay.gif\"  width=15 />" ;
		}
		
    $output['action'] = '<input name=h_id[]" type="hidden" value="' . $id . '" />';    
		$output['action'] .="<a href=\"{$link_edit}\" ><img src=\"". $vnT->dir_images ."/edit.gif\"  /></a>&nbsp;";
		$output['action'] .= $display.'&nbsp;';
    $output['action'] .= '<a href="' . $link_del . '"><img src="' . $vnT->dir_images . '/delete.gif"  alt="Delete "></a>';
    return $output;
  }
  
  /**
   * function do_Manage() 
   * Quan ly cac gioi thieu
   **/
  function do_Manage ($lang)
  {
    global $vnT, $func, $DB, $conf;
    $vnT->html->addStyleSheet($vnT->dir_js . "/auto_suggest/autosuggest.css");
		$vnT->html->addScript($vnT->dir_js . "/auto_suggest/bsn.AutoSuggest.js");	
		
    //update
    if ($vnT->input["do_action"])
    {
      //xoa cache
      $func->clear_cache();
      if ($vnT->input["del_id"]) $h_id = $vnT->input["del_id"];

				
      switch ($vnT->input["do_action"])
      {
			 case "do_edit":
				 
				break;
        case "do_hidden":
            $mess .= "- " . $vnT->lang['hidden_success'] . " ID: <strong>";
            for ($i = 0; $i < count($h_id); $i ++)
            {
              $dup['display'] = 0;
              $ok = $DB->do_update("news_comment", $dup, "cid=" . $h_id[$i]);
              if ($ok){							
                $str_mess .= $h_id[$i] . ", ";
              }
            }
            $mess .= substr($str_mess, 0, - 2) . "</strong><br>";
            $err = $func->html_mess($mess);

          break;
        case "do_display":
            $mess .= "- " . $vnT->lang['display_success'] . "  ID: <strong>";
            for ($i = 0; $i < count($h_id); $i ++)
            {
              $dup['display'] = 1;
              $ok = $DB->do_update("news_comment", $dup, "cid=" . $h_id[$i]);
              if ($ok){
                $str_mess .= $h_id[$i] . ", ";
              }
            }
            $mess .= substr($str_mess, 0, - 2) . "</strong><br>";
            $err = $func->html_mess($mess);
          break;
      }
    }
    
		$p = ((int) $vnT->input['p']) ? $p = $vnT->input['p'] : 1;
		$n = ($conf['record']) ? $conf['record'] : 30;
    $keyword = ($vnT->input['keyword']) ?  $vnT->input['keyword'] : "";
		$display = (isset($vnT->input['display'])) ? $display = $vnT->input['display'] : "-1";

    $where ='';
    $ext_page='';
    $ext='';
		if($display!="-1"){
			$where .=" and c.display=$display ";	
			$ext_page.="display=$display|";
			$ext.="&display={$display}";
		}

    if(!empty($keyword)){
      $where .=" AND n.title like '%$keyword%' ";
      $ext_page.="keyword=$keyword|";
      $ext.="&keyword={$keyword}";
    }

		
    $query = $DB->query("SELECT  c.cid
												 FROM news_comment c LEFT JOIN news_desc  n
												 ON c.id=n.newsid  
												 WHERE n.lang='$lang'
												 and c.parentid=0
												 $where ");
    $totals = intval($DB->num_rows($query));    
		
    $num_pages = ceil($totals / $n);
    if ($p > $num_pages) $p = $num_pages;
    if ($p < 1) $p = 1;
    $start = ($p - 1) * $n;
    
    $nav = $func->paginate($totals, $n, $ext, $p);
    
    $table['link_action'] = $this->linkUrl . "&sub=manage";
    $table['title'] = array(
				 'check_box' => "<input type=\"checkbox\" name=\"checkall\" id=\"checkall\" class=\"checkbox\" />|5%|center" , 
				 'customer' => "Người đăng|20%|left", 
				 'item_title' => "Tiêu đề tin|20%|left",
				 'content' => $vnT->lang['content_comment']." |35%|left",  
				 'sub_comment' => "Ý kiến|10%|center",
				 'action' => "Action|13%|center",
				);
    
    $sql = "SELECT  c.* ,n.title as item_title  ,( SELECT COUNT(cid) FROM news_comment n WHERE n.parentid=c.cid ) AS num_sub
					 FROM news_comment c LEFT JOIN news_desc  n
					 ON c.id=n.newsid  
					 WHERE n.lang='$lang'
					 and c.parentid=0
					 $where 
					 ORDER BY c.date_post DESC  LIMIT $start,$n";
    //print "sql = ".$sql."<br>";
    $result = $DB->query($sql);
    if ($DB->num_rows($result))
    {
      $row = $DB->get_array($result);
      for ($i = 0; $i < count($row); $i ++)
      {
        $row_info = $this->render_row($row[$i],$lang);
        $row_field[$i] = $row_info;
        $row_field[$i]['stt'] = ($i + 1);
        $row_field[$i]['row_id'] = "row_" . $row[$i]['cid'];
        $row_field[$i]['ext'] = "";
      }
      $table['row'] = $row_field;
    }
    else
    {
      $table['row'] = array();
      $table['extra'] = "<div align=center class=font_err >" . $vnT->lang['no_have_comment'] ."</div>";
    }
    
		$table['button'] = '<input type="button" name="btnHidden" value=" '.$vnT->lang['hidden'].' " class="button" onclick="do_submit(\'do_hidden\')">&nbsp;';
		$table['button'] .= '<input type="button" name="btnDisplay" value=" '.$vnT->lang['display'].' " class="button" onclick="do_submit(\'do_display\')">&nbsp;';
		$table['button'] .= '<input type="button" name="btnEdit" value=" '.$vnT->lang['update'].' " class="button" onclick="do_submit(\'do_edit\')">&nbsp;';
		$table['button'] .= '<input type="button" name="btnDel" value=" '.$vnT->lang['delete'].' " class="button" onclick="del_selected(\''.$this->linkUrl.'&sub=del&ext='.$ext_page.'\')">';
    
    $table_list = $func->ShowTable($table);
    $data['table_list'] = $table_list;
    $data['totals'] = $totals;
		$data['link_fsearch'] = $this->linkUrl ;
		$data['keyword'] = $keyword;
		$data['list_display'] =  vnT_HTML::selectbox("display", array( '-1' => "-- Tất cả --" ,'0' => 'Chưa xét duyệt' , '1' => 'Đã xét duyệt'  ), $display);
    
		
    $data['err'] = $err;
    $data['nav'] = $nav;
    
    /*assign the array to a template variable*/
    $this->skin->assign('data', $data);
    
    $this->skin->parse("manage");
    return $this->skin->text("manage");
  }
	
	

  
  /**
   * function render_row 
   * list cac record
   **/
  function render_row_sub ($row_info, $lang)
  {
    global $func, $DB, $conf, $vnT;
    $row = $row_info;
    // Xu ly tung ROW
    $id = $row['cid'];
    $row_id = "row_" . $id;
    $output['check_box'] = vnT_HTML::checkbox("del_id[]", $id, 0, " ");    
    $link_del = "javascript:del_item('" . $this->linkUrl . "&sub=sub_comment&do=del&id=" . $id . "&cid=" . $row['cid'] . "')";
    
     
    $output['customer'] =  "<b>".$func->HTML($row['name'])."</b>";  
		$output['customer'] .=  "<div style='padding:2px 0px;'>Lúc : ".@date("H:i, d/m/Y",$row['date_post']).'</div>'; 
   		
		$output['content'] = $func->HTML($row['content']); 
		
		if ($row['display']==1){
			$display ="<img src=\"{$vnT->dir_images}/dispay.gif\" width=15  />" ;
		}else{
			$display ="<img src=\"{$vnT->dir_images}/nodispay.gif\"  width=15 />" ;
		}
		
    $output['action'] = '<input name=h_id[]" type="hidden" value="' . $id . '" />';     
		$output['action'] .= $display.'&nbsp;';
		
    return $output;
  }
	/**
   * function do_subComment() 
   * 
   **/
  function do_subComment ($lang)
  {
    global $vnT, $func, $DB, $conf;
    
    $cid = (int) $vnT->input['cid'];    
    //check
    $res = $DB->query("SELECT * FROM news_comment WHERE cid=" . $cid);
    if ($r = $DB->fetch_row($res))
    {
      $data['comment_name'] = $func->HTML($r['content']);
    }
    else  {
      die("Comment này không tồn tại");
    }
		
     //update
    if ($vnT->input["do_action"])
    {
      //xoa cache
      $func->clear_cache();
      if ($vnT->input["del_id"]) $h_id = $vnT->input["del_id"];  
				
      switch ($vnT->input["do_action"])
      {
			 case "do_del":
				 	$mess .= "- Xóa thành công ID: <strong>";
            for ($i = 0; $i < count($h_id); $i ++)
            {             
              $ok = $DB->query("DELETE FROM news_comment WHERE cid=" . $h_id[$i]);
              if ($ok){							
                $str_mess .= $h_id[$i] . ", ";
              }
            }
            $mess .= substr($str_mess, 0, - 2) . "</strong><br>";
            $err = $func->html_mess($mess);
				break;
        case "do_hidden":
            $mess .= "- " . $vnT->lang['hidden_success'] . " ID: <strong>";
            for ($i = 0; $i < count($h_id); $i ++)
            {
              $dup['display'] = 0;
              $ok = $DB->do_update("news_comment", $dup, "cid=" . $h_id[$i]);
              if ($ok){							
                $str_mess .= $h_id[$i] . ", ";
              }
            }
            $mess .= substr($str_mess, 0, - 2) . "</strong><br>";
            $err = $func->html_mess($mess);

          break;
        case "do_display":
            $mess .= "- " . $vnT->lang['display_success'] . "  ID: <strong>";
            for ($i = 0; $i < count($h_id); $i ++)
            {
              $dup['display'] = 1;
              $ok = $DB->do_update("news_comment", $dup, "cid=" . $h_id[$i]);
              if ($ok){
                $str_mess .= $h_id[$i] . ", ";
              }
            }
            $mess .= substr($str_mess, 0, - 2) . "</strong><br>";
            $err = $func->html_mess($mess);
          break;
      }
    }
    
    
    $table['link_action'] = $this->linkUrl . "&sub=sub_comment&cid=" . $cid;
    $table['title'] = array(      
      'check_box' => "<input type=\"checkbox\" name=\"checkall\" id=\"checkall\" class=\"checkbox\" />|5%|center" , 
      'customer' => "Người đăng|20%|left" , 
      'content' => "Nội dung |60%|left",
			'action' => "Action |10%|center",
    );
    
    $sql = "SELECT * FROM news_comment WHERE parentid=" . $cid . "   ORDER BY  date_post  DESC ";
     
    $result = $DB->query($sql);
    if ($totals = $DB->num_rows($result))
    {
      $row = $DB->get_array($result);
      for ($i = 0; $i < count($row); $i ++)
      {
        $row_info = $this->render_row_sub($row[$i], $lang);
        $row_field[$i] = $row_info;
        $row_field[$i]['stt'] = ($i + 1);
        $row_field[$i]['row_id'] = "row_" . $row[$i]['cid'];
        $row_field[$i]['ext'] = "";
      }
      $table['row'] = $row_field;
    }
    else
    {
      $table['row'] = array();
      $table['extra'] = "<div align=center class=font_err >Chưa có ý kiến nào</div>";
    }
    
    $table['button'] = '<input type="button" name="btnHidden" value=" '.$vnT->lang['hidden'].' " class="button" onclick="do_submit(\'do_hidden\')">&nbsp;';
		$table['button'] .= '<input type="button" name="btnDisplay" value=" '.$vnT->lang['display'].' " class="button" onclick="do_submit(\'do_display\')">&nbsp;';

		$table['button'] .= '<input type="button" name="btnDel" value=" '.$vnT->lang['delete'].' " class="button"  onclick="do_submit(\'do_del\')" >';
    
    $table_list = $func->ShowTable($table);
    $data['table_list'] = $table_list;
    $data['totals'] = $totals;
    $data['err'] = $err;
    $data['link_action'] = $this->linkUrl . "&sub=sub_comment&cid=" . $cid;
    
    /*assign the array to a template variable*/
    $this->skin->assign('data', $data);
    
    $this->skin->parse("sub_comment");
    return $this->skin->text("sub_comment");
  }
	
  // end class
}
$vntModule = new vntModule();
?>