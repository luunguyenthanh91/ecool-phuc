<?php
/*================================================================================*\
|| 							Name code : news.php 		 		            	  ||
||  				Copyright @2008 by Thai Son - CMS vnTRUST                     ||
\*================================================================================*/
/**
 * @version : 2.0
 * @date upgrade : 09/01/2009 by Thai Son
 **/
if (!defined('IN_vnT')) {
  die('Hacking attempt!');
}
//load Model
include(dirname(__FILE__) . "/functions.php");

class vntModule extends Model
{
  var $output = "";
  var $skin = "";
  var $linkUrl = "";
  var $module = "news";
  var $action = "news";
  var $setting = array();

  function vntModule(){
    global $Template, $vnT, $func, $DB, $conf;
    $this->skin = new XiTemplate(DIR_MODULE.DS.$this->module."_ad" . DS . "html" . DS . $this->action . ".tpl");
    $this->skin->assign('CONF', $vnT->conf);
    $this->skin->assign('LANG', $vnT->lang);
    $this->skin->assign("DIR_JS", $vnT->dir_js);
    $this->skin->assign("DIR_IMAGE", $vnT->dir_images);
    $lang = ($vnT->input['lang']) ? $lang = $vnT->input['lang'] : $func->get_lang_default();
    $this->linkUrl = "?mod=" . $this->module . "&act=" . $this->action . "&lang=" . $lang;
    $vnT->html->addStyleSheet("modules/" . $this->module . "_ad/css/" . $this->module . ".css");
    $vnT->html->addScript("modules/" . $this->module . "_ad" . "/js/" . $this->module . ".js");
    $vnT->html->addStyleSheet($vnT->dir_js . "/jquery-ui/jquery-ui.min.css");
    $vnT->html->addScript($vnT->dir_js . "/jquery-ui/jquery-ui.min.js");
    $vnT->html->addScriptDeclaration("
	 		$(function() {
				$('.dates').datepicker({
					showOn: 'button',
					buttonImage: '" . $vnT->dir_images . "/calendar.gif',
					buttonImageOnly: true,
					changeMonth: true,
					changeYear: true
				});
			});		
		");
    $nd['menu'] = $func->getToolbar($this->module, $this->action, $lang);
    $nd['row_lang'] = $func->html_lang("?mod=" . $this->module . "&act=" . $this->action, $lang);
    switch ($vnT->input['sub']) {
      case 'add':
        $nd['f_title'] = $vnT->lang['add_news'];
        $nd['content'] = $this->do_Add($lang);
        break;
      case 'edit':
        $nd['f_title'] = $vnT->lang['edit_news'];
        $nd['content'] = $this->do_Edit($lang);
        break;
      case 'duplicate':
        $this->do_Duplicate($lang);
        break;
      case 'del':
        $this->do_Del($lang);
        break;
      default:
        $nd['f_title'] = $vnT->lang['manage_news'];
        $nd['content'] = $this->do_Manage($lang);
        break;
    }
    $Template->assign("data", $nd);
    $Template->parse("box_main");
    $vnT->output .= $Template->text("box_main");
  }
  function do_Add($lang){
    global $vnT, $func, $DB, $conf;
    $vnT->html->addStyleSheet($vnT->dir_js . "/autoSuggestv14/autoSuggest.css");
    $vnT->html->addScript($vnT->dir_js . "/autoSuggestv14/jquery.autoSuggest.js");

    $w = ($vnT->setting['img_width']) ? $vnT->setting['img_width'] : 500;
    $w_thum = ($vnT->setting['imgthumb_width']) ? $vnT->setting['imgthumb_width'] : 100;
    $err = "";
    $data['display'] = 1;
    $data['gio'] = date("H:i");
    $data['ngay'] = date("d/m/Y");
    $data['poster'] = $vnT->admininfo['username'];
    $data['ref'] = 1;
    $dir = $vnT->func->get_dir_upload_module($this->module, $vnT->setting['folder_upload']);

    if ($vnT->input['do_submit'] == 1) {
      $data = $_POST;
      $cat_id = $vnT->input['cat_id'];
      $cat_list = $this->get_cat_list($cat_id);
      $title = $vnT->input['title'];
      $gio = explode(":", $vnT->input['gio']);
      $ngay = explode("/", $vnT->input['ngay']);
      $date_post = mktime($gio[0], $gio[1], 0, $ngay[1], $ngay[0], $ngay[2]);
      $picture = $vnT->input['picture'];
      $info_tag = $vnT->func->get_input_tags_js($this->module, $_POST["as_values_htag"]);
      $content = $_POST['content'];
      // lay hinh
      if ($vnT->setting['get_image']) {
        $tmp_content = $this->replace_img($data['content'], $dir);
        $content = $tmp_content['result'];
        if (empty($picture)) {
          if ($tmp_content['url'])
            $picture = $tmp_content['url'];
        }
      }
      // insert CSDL
      if (empty($err)) {
        $cot = array(
          'cat_id' => $cat_id,
          'cat_list' => $cat_list,
          'picture' => $picture,
          'pic_social_network' => $vnT->input['pic_social_network'],
          'ref' => $vnT->input['ref'],
          'source' => $vnT->input['source'],
          'keyref_id' => $vnT->input['keyref_id'],
          'poster' => $vnT->input['poster'],
          'date_post' => $date_post,
          'date_update' => time(),
          'permesion_view' => $vnT->input['permesion_view'],
          'type_show' => 2
        );
        $cot['adminid'] = $vnT->admininfo['adminid'];
        $cot['tags'] = $info_tag['list_id'];
        $cot['tags_name'] = $info_tag['list_name'];
        $cot['type'] = ($vnT->input['type']) ? $vnT->input['type'] : 'article';
        $cot['video_type'] = $vnT->input['video_type'];
        $cot['video_url'] = $func->txt_HTML($_POST['video_url']);
        $kq = $DB->do_insert("news", $cot);
        if ($kq) {
          $newsid = $DB->insertid();
          $DB->query("UPDATE news SET display_order=" . $newsid . " WHERE newsid=" . $newsid);
          //desc
          $cot_d['newsid'] = $newsid;
          $cot_d['title'] = $title;
          $cot_d['short'] = $DB->mySQLSafe($_POST['short']);
          $cot_d['content'] = $DB->mySQLSafe($content);
          $cot_d['note'] = $vnT->input['note'];
          $cot_d['picturedes'] = $vnT->input['picturedes'];
          //SEO
          $cot_d['friendly_url'] = (trim($vnT->input['friendly_url'])) ? $func->make_url($vnT->input['friendly_url']) : $func->make_url($_POST['title']);
          $cot_d['friendly_title'] = (trim($vnT->input['friendly_title'])) ? trim($vnT->input['friendly_title']) : $title;
          $cot_d['metakey'] = (trim($vnT->input['metakey'])) ? $vnT->input['metakey'] : $title;
          $cot_d['metadesc'] = (trim($vnT->input['metadesc'])) ? $vnT->input['metadesc'] : $func->txt_HTML($func->cut_string($func->check_html($_POST['short'], 'nohtml'), 200, 1));
          $cot_d['display'] = $vnT->input['display'];
          $query_lang = $DB->query("select name from language ");
          while ($row_lang = $DB->fetch_row($query_lang)) {
            $cot_d['lang'] = $row_lang['name'];
            $DB->do_insert("news_desc", $cot_d);
          }
          //insert hinh
          $arr_picture = $this->get_pic_input(0, $dir, $w, $w_thum);
          if (is_array($arr_picture)) {
            foreach ($arr_picture as $key => $val) {
              $cot_pic = array();
              $cot_pic['item_id'] = $newsid;
              $cot_pic['picture'] = $val['picture'];
              $cot_pic['pic_name'] = $val['pic_name'];
              $cot_pic['pic_order'] = $val['pic_order'];
              $vnT->DB->do_insert("news_picture", $cot_pic);
            }
          }
          //build seo_url
          $seo['sub'] = 'add';
          $seo['modules'] = $this->module;
          $seo['action'] = "detail";
          $seo['name_id'] = "itemID";
          $seo['item_id'] = $newsid;
          $seo['friendly_url'] = $cot_d['friendly_url'];
          $seo['lang'] = $lang;
          $seo['query_string'] = "mod:" . $seo['modules'] . "|act:" . $seo['action'] . "|" . $seo['name_id'] . ":" . $newsid;
          $res_seo = $func->build_seo_url($seo);
          if ($res_seo['existed'] == 1) {
            $DB->query("UPDATE news_desc SET friendly_url='" . $res_seo['friendly_url'] . "' WHERE newsid=" . $newsid);
          }
          //xoa cache
          $func->clear_cache();
          //insert adminlog
          $func->insertlog("Add", $_GET['act'], $newsid);
          $mess = $vnT->lang['add_success'];
          if (isset($_POST['btn_preview'])) {
            $url = $this->linkUrl . "&sub=edit&id=$newsid&preview=1";
            $DB->query("Update news_desc SET display=0 WHERE newsid=$newsid ");
          } else {
            $url = $this->linkUrl . "&sub=add";
          }
          $func->html_redirect($url, $mess);
        } else {
          $err = $func->html_err($vnT->lang['add_failt'] . $DB->debug());
        }
      }
    }
    $data['listcat'] = $this->Get_Cat($vnT->input['cat_id'], $lang);
    $data['list_display'] = vnT_HTML::list_yesno("display", $data['display']);
    $data['list_keyref'] = $this->List_Keyref($vnT->input['keyref_id']);
    $data['list_source'] = $this->List_Source($vnT->input['source'], $lang);
    $data['list_ref'] = vnT_HTML::selectbox("ref", array('0' => 'nofollow', '1' => 'follow'), $data['ref']);
    // $data['list_permesion_view'] = vnT_HTML::selectbox("permesion_view", array(
    //                                                   '0' => $vnT->lang['all'],
    //                                                   '1' => $vnT->lang['only_member']), $data['permesion_view']);
    // $data['list_type_show'] = vnT_HTML::selectbox("type_show", array(
    //                                               '0' => $vnT->lang['type_show_0'],
    //                                               '1' => $vnT->lang['type_show_1'],
    //                                               '2' => $vnT->lang['type_show_2'],
    //                                               '3' => $vnT->lang['type_show_3']), $data['type_show']);
    $data['list_type'] = vnT_HTML::selectbox("type", $vnT->setting['arr_type'], $data['type']);
    $data['video_type'] = vnT_HTML::selectbox("video_type", $vnT->setting['arr_video_type'], $data['video_type']);
    $data["html_short"] = $vnT->editor->doDisplay('short', $vnT->input['short'], '100%', '200', "Normal", $this->module, $dir);
    $data["html_content"] = $vnT->editor->doDisplay('content', $vnT->input['content'], '100%', '500', "Default", $this->module, $dir);
    $vnT->func->get_tags_js($this->module, "tags", "htag", $data['tags']);
    $data['link_upload'] = '?mod=media&act=popup_media&module=' . $this->module . '&folder=' . $this->module . '/' . $dir . '&obj=picture&type=image&TB_iframe=true&width=900&height=474';
    $folder_upload = $conf['rootpath'] . "vnt_upload/news/" . $dir;
    $data['folder_upload'] = '../vnt_upload/news/';
    $data['dir_upload'] = $dir;
    $data['link_show_file'] = ROOT_URL . 'vnt_upload/news';
    $data['folder_upload'] = $folder_upload;
    $data['folderpath'] = $conf['rootpath'] . "vnt_upload/news";
    $data['folder'] = $dir;
    $data['max_upload'] = ini_get('upload_max_filesize');
    $data['w_thumb'] = $w_thum;
    $data['w'] = $w;
    $data['folder_browse'] = ($dir) ? $this->module . "/" . $dir : $this->module;
    $data['link_seo'] = $conf['rooturl'] . "xxx.html";
    if ($data['friendly_url'])
      $data['link_mxh'] = $conf['rooturl'] . $data['friendly_url'] . ".html";
    $data['err'] = $err;
    $data['link_action'] = $this->linkUrl . "&sub=add";
    /*assign the array to a template variable*/
    $this->skin->assign('data', $data);
    $this->skin->parse("edit");
    return $this->skin->text("edit");
  }
  function do_Edit($lang){
    global $vnT, $func, $DB, $conf;
    $vnT->html->addStyleSheet($vnT->dir_js . "/autoSuggestv14/autoSuggest.css");
    $vnT->html->addScript($vnT->dir_js . "/autoSuggestv14/jquery.autoSuggest.js");

    $w = ($vnT->setting['img_width']) ? $vnT->setting['img_width'] : 500;
    $w_thum = ($vnT->setting['imgthumb_width']) ? $vnT->setting['imgthumb_width'] : 100;
    $id = (int)$vnT->input['id'];
    $dir = $vnT->func->get_dir_upload_module($this->module, $vnT->setting['folder_upload']);
    if ($vnT->input['do_submit']) {
      $data = $_POST;
      $cat_id = $vnT->input['cat_id'];
      $cat_list = $this->get_cat_list($cat_id);
      $title = $vnT->input['title'];
      $gio = @explode(":", $vnT->input['gio']);
      $ngay = @explode("/", $vnT->input['ngay']);
      $date_post = @mktime($gio[0], $gio[1], 0, $ngay[1], $ngay[0], $ngay[2]);
      $picture = $vnT->input['picture'];
      $info_tag = $vnT->func->get_input_tags_js($this->module, $_POST["as_values_htag"]);
      $content = $_POST['content'];
      // lay hinh
      if ($vnT->setting['get_image']) {
        $tmp_content = $this->replace_img($data['content'], $dir);
        $content = $tmp_content['result'];
        if (empty($picture)) {
          if ($tmp_content['url'])
            $picture = $tmp_content['url'];
        }
      }
      if (empty($err)) {
        $cot = array(
          'cat_id' => $cat_id,
          'cat_list' => $cat_list,
          'picture' => $picture,
          'pic_social_network' => $vnT->input['pic_social_network'],
          'ref' => $vnT->input['ref'],
          'source' => $vnT->input['source'],
          'keyref_id' => $vnT->input['keyref_id'],
          'poster' => $vnT->input['poster'],
          'date_post' => $date_post,
          'date_update' => time(),
          'permesion_view' => $vnT->input['permesion_view'],
          'type_show' => 2
        );
        $cot['tags'] = $info_tag['list_id'];
        $cot['tags_name'] = $info_tag['list_name'];
        $cot['type'] = ($vnT->input['type']) ? $vnT->input['type'] : 'article';
        $cot['video_type'] = $vnT->input['video_type'];
        $cot['video_url'] = $func->txt_HTML($_POST['video_url']);
        $kq = $DB->do_update("news", $cot, "newsid=$id");
        if ($kq) {
          $cot_d['title'] = $title;
          $cot_d['short'] = $DB->mySQLSafe($_POST['short']);
          $cot_d['content'] = $DB->mySQLSafe($content);
          $cot_d['note'] = $vnT->input['note'];
          $cot_d['picturedes'] = $vnT->input['picturedes'];
          //SEO
          $cot_d['friendly_url'] = (trim($vnT->input['friendly_url'])) ? $func->make_url($vnT->input['friendly_url']) : $func->make_url($_POST['title']);
          $cot_d['friendly_title'] = (trim($vnT->input['friendly_title'])) ? trim($vnT->input['friendly_title']) : $title;
          $cot_d['metakey'] = (trim($vnT->input['metakey'])) ? $vnT->input['metakey'] : $title;
          $cot_d['metadesc'] = (trim($vnT->input['metadesc'])) ? $vnT->input['metadesc'] : $func->txt_HTML($func->cut_string($func->check_html($_POST['short'], 'nohtml'), 200, 1));

          $cot_d['display'] = $vnT->input['display'];
          $DB->do_update("news_desc", $cot_d, "newsid=$id and lang='{$lang}'");
          //update hinh
          $arr_picture = $this->get_pic_input($id, $dir, $w, $w_thum);
          //build seo_url
          $seo['sub'] = 'edit';
          $seo['modules'] = $this->module;
          $seo['action'] = "detail";
          $seo['name_id'] = "itemID";
          $seo['item_id'] = $id;
          $seo['friendly_url'] = $cot_d['friendly_url'];
          $seo['lang'] = $lang;
          $seo['query_string'] = "mod:" . $seo['modules'] . "|act:" . $seo['action'] . "|" . $seo['name_id'] . ":" . $id;
          $res_seo = $func->build_seo_url($seo);
          if ($res_seo['existed'] == 1) {
            $DB->query("UPDATE news_desc SET friendly_url='" . $res_seo['friendly_url'] . "' WHERE lang='" . $lang . "' AND newsid=" . $id);
          }
          //xoa cache
          $func->clear_cache();
          //insert adminlog
          $func->insertlog("Edit", $_GET['act'], $id);
          $url = $this->linkUrl;
          $err = $vnT->lang["edit_success"];
          $func->html_redirect($url, $err);
        } else {
          $err = $func->html_err($vnT->lang["edit_failt"] . $DB->debug());
        }
      } // if err
    } // if summit
    $query = $DB->query("SELECT * FROM news n, news_desc nd  WHERE n.newsid=nd.newsid AND lang='$lang' AND n.newsid=$id");
    if ($data = $DB->fetch_row($query)) {
      if ($vnT->input['preview'] == 1) {
        $link_preview = $conf['rooturl'];
        if ($vnT->muti_lang) $link_preview .= $lang . "/";
        $link_preview .= $data['friendly_url'] . ".html/?preview=1";

        $mess_preview = str_replace(array("{title}", "{link}"), array($data['p_name'], $link_preview), $vnT->lang['mess_preview']);
        $data['js_preview'] = "tb_show('" . $mess_preview . "', '" . $link_preview . "&TB_iframe=true&width=1000&height=700',null)";
      }
      //dir
      if ($data['picture']) {
        $dir = substr($data['picture'], 0, strrpos($data['picture'], "/"));
        $data['pic'] = $this->get_picture($data['picture'], $w_thum) . "  <a href=\"javascript:del_picture('picture')\" class=\"del\">Xóa</a>";
        $data['style_upload'] = "style='display:none' ";
      } else {
        $data['pic'] = "";
      }
      $res_pic = $vnT->DB->query("SELECT * FROM news_picture WHERE item_id=" . $id . " ORDER BY pic_order ASC , id DESC");
      if ($num_pic_old = $vnT->DB->num_rows($res_pic)) {
        $pic_stt = 0;
        while ($row_pic = $vnT->DB->fetch_row($res_pic)) {
          $pic_stt++;
          $row_pic['stt'] = $pic_stt;
          $row_pic['file_src'] = $row_pic['picture'];
          $row_pic['pic'] = $this->get_picture($row_pic['picture'], $w_thum);
          $row_pic['pic_order'] = $pic_stt;
          $this->skin->assign('row', $row_pic);
          $this->skin->parse("edit.item_pic");
        }
      }

      $data['title'] = $func->txt_unHTML($data['title']);
      $data['picturedes'] = $func->txt_unHTML($data['picturedes']);
      $data['source'] = $func->txt_unHTML($data['source']);
      $data['note'] = $func->txt_unHTML($data['note']);
      $data['poster'] = $func->txt_unHTML($data['poster']);
      $data['gio'] = date("H:i", $data['date_post']);
      $data['ngay'] = date("d/m/Y", $data['date_post']);
      $data['list_display'] = vnT_HTML::list_yesno("display", $data['display']);
      $data['listcat'] = $this->Get_Cat($data['cat_id'], $lang);
      $data['list_keyref'] = $this->List_Keyref($data['keyref_id']);
      $data['list_ref'] = vnT_HTML::selectbox("ref", array('0' => 'nofollow', '1' => 'follow'), $data['ref']);
      $data['list_source'] = $this->List_Source($data['source'], $lang);
      // $data['list_permesion_view'] = vnT_HTML::selectbox("permesion_view", array(
      //   '0' => $vnT->lang['all'],
      //   '1' => $vnT->lang['only_member']), $data['permesion_view']);
      // $data['list_type_show'] = vnT_HTML::selectbox("type_show", array(
      //   '0' => $vnT->lang['type_show_0'],
      //   '1' => $vnT->lang['type_show_1'],
      //   '2' => $vnT->lang['type_show_2'],
      //   '3' => $vnT->lang['type_show_3']), $data['type_show']);

      $data['checked_pic_social_network'] = ($data['pic_social_network'] == 1) ? " checked ='checked' " : "";
      $data['list_type'] = vnT_HTML::selectbox("type", $vnT->setting['arr_type'], $data['type']);
      $data['video_type'] = vnT_HTML::selectbox("video_type", $vnT->setting['arr_video_type'], $data['video_type']);
      $data['link_seo'] = $conf['rooturl'] . "xxx.html";
      if ($data['friendly_url'])
        $data['link_mxh'] = $conf['rooturl'] . $data['friendly_url'] . ".html";

      $data['img_mxh'] = '<div class="face-img"><img src="' . $conf['rooturl'] . 'image.php?image=' . $conf['rooturl'] . ROOT_UPLOAD . '/' . $data['picture'] . '&width=120&height=120&cropratio=1:1" alt="" /></div>';

    } else {
      $mess = $vnT->lang['not_found'] . " ID : " . $id;
      $url = $this->linkUrl;
      $func->html_redirect($url, $mess);
    }

    $vnT->func->get_tags_js($this->module, "tags", "htag", $data['tags']);

    $data["html_short"] = $vnT->editor->doDisplay('short', $data['short'], '100%', '200', "Normal", $this->module, $dir);
    $data["html_content"] = $vnT->editor->doDisplay('content', $data['content'], '100%', '500', "Default", $this->module, $dir);
    $data['link_upload'] = '?mod=media&act=popup_media&type=modules&module=' . $this->module . '&folder=' . $this->module . '/' . $dir . '&obj=picture&TB_iframe=true&width=900&height=474';
    $folder_upload = $conf['rootpath'] . "vnt_upload/news/" . $dir;

    $data['folder_upload'] = '../vnt_upload/news/';
    $data['dir_upload'] = $dir;
    $data['link_show_file'] = ROOT_URL . 'vnt_upload/news';
    $data['folder_upload'] = $folder_upload;
    $data['folderpath'] = $conf['rootpath'] . "vnt_upload/news";
    $data['folder'] = $dir;
    $data['max_upload'] = ini_get('upload_max_filesize');
    $data['w_thumb'] = $w_thum;
    $data['w'] = $w;

    $data['folder_browse'] = ($dir) ? $this->module . "/" . $dir : $this->module;
    $data['err'] = $err;


    $data['link_action'] = $this->linkUrl . "&sub=edit&id=$id";
    /*assign the array to a template variable*/
    $this->skin->assign('data', $data);
    $this->skin->parse("edit");
    return $this->skin->text("edit");
  }
  function do_Duplicate($lang){
    global $func, $DB, $conf, $vnT;
    $id = (int)$vnT->input['id'];
    $ext = $vnT->input["ext"];
    $del = 0;
    $qr = "";
    if ($id != 0) {
      $ids = $id;
    }
    if (isset($vnT->input["del_id"])) {
      $ids = implode(',', $vnT->input["del_id"]);
    }

    $arr_duplicated = array();

    $res = $DB->query("SELECT * FROM news WHERE newsid IN (" . $ids . ") ");
    if ($DB->num_rows($res)) {
      while ($row = $DB->fetch_row($res)) {
        //Duplicate products
        $cot = $row;
        $cot["newsid"] = "";
        $cot["viewnum"] = 0;
        $cot["date_post"] = time();
        $cot["date_update"] = time();
        $DB->do_insert("news", $cot);
        //End Duplicate products

        $newsid = $DB->insertid();
        if ($newsid) {
          $DB->query("UPDATE news SET display_order=" . $newsid . " WHERE newsid=" . $newsid);

          //Duplicate products_desc
          $res_d = $DB->query("SELECT * FROM news_desc WHERE newsid=" . $row['newsid'] . " ");
          while ($row_d = $DB->fetch_row($res_d)) {
            $dup_d = $row_d;
            $dup_d["id"] = "";
            $dup_d["newsid"] = $newsid;

            $arr_duplicated[$row_d["lang"]][] = $dup_d['friendly_url'];
            //$dup_d["friendly_url"] .= "-".time();
            while (in_array($dup_d['friendly_url'], $arr_duplicated[$row_d["lang"]])) {
              $dup_d['friendly_url'] .= "-" . time();
            }
            $arr_duplicated[$row_d["lang"]][] = $dup_d['friendly_url'];

            $DB->do_insert("news_desc", $dup_d);
          }
          //End Duplicate products_desc

          //build seo_url
          $seo['sub'] = 'add';
          $seo['modules'] = $this->module;
          $seo['action'] = "detail";
          $seo['name_id'] = "itemID";
          $seo['item_id'] = $newsid;
          $seo['friendly_url'] = $dup_d['friendly_url'];

          $seo['lang'] = $lang;
          $seo['query_string'] = "mod:" . $seo['modules'] . "|act:" . $seo['action'] . "|" . $seo['name_id'] . ":" . $newsid;
          $res_seo = $func->build_seo_url($seo);
          if ($res_seo['existed'] == 1) {
            $DB->query("UPDATE news_desc SET friendly_url='" . $res_seo['friendly_url'] . "' WHERE newsid=" . $newsid);
          }


        }
      }
      $mess = $vnT->lang["duplicate_success"];
      //xoa cache
      $func->clear_cache();
    } else {
      $mess = $vnT->lang["duplicate_failt"];
    }

    $ext_page = str_replace("|", "&", $ext);
    $url = $this->linkUrl . "&{$ext_page}";
    $func->html_redirect($url, $mess);
  }
  function do_Del($lang){
    global $func, $DB, $conf, $vnT;
    $id = (int)$vnT->input['id'];
    $ext = $vnT->input["ext"];
    $del = 0;
    $qr = "";
    if ($id != 0) {
      $ids = $id;
    }
    if (isset($vnT->input["del_id"])) {
      $ids = implode(',', $vnT->input["del_id"]);
    }
    $res = $DB->query("SELECT newsid FROM news WHERE newsid IN (" . $ids . ") ");
    if ($DB->num_rows($res)) {
      $DB->query("UPDATE news_desc SET display=-1 WHERE newsid IN (".$ids.") AND lang='".$lang."' ");
      $rb_log['module'] = $this->module;
      $rb_log['action'] = "detail";
      $rb_log['tbl_data'] = "news_desc";
      $rb_log['name_id'] = "newsid";
      $rb_log['item_id'] = $ids;
      $rb_log['lang'] = $lang;
      // $func->insertRecycleBin($rb_log);
      //build seo_url
      $seo['sub'] = 'del';
      $seo['modules'] = $this->module;
      $seo['action'] = "detail";
      $seo['item_id'] = $ids;
      $seo['lang'] = $lang;
      $res_seo = $func->build_seo_url($seo);
      $mess = $vnT->lang["del_success"];
      $func->clear_cache();
    } else {
      $mess = $vnT->lang["del_failt"];
    }
    $ext_page = str_replace("|", "&", $ext);
    $url = $this->linkUrl . "&{$ext_page}";
    $func->html_redirect($url, $mess);
  }
  function render_row($row_info, $lang){
    global $func, $DB, $conf, $vnT;
    $row = $row_info;
    // Xu ly tung ROW
    $id = $row['newsid'];
    $row_id = "row_" . $id;
    $output['check_box'] = vnT_HTML::checkbox("del_id[]", $id, 0, " ");
    $link_edit = $this->linkUrl . "&sub=edit&id={$id}";
    $link_duplicate = $this->linkUrl . '&sub=duplicate&id=' . $id;
    $link_del = "javascript:del_item('" . $this->linkUrl . "&sub=del&id={$id}')";
    if (empty($row['picture']))
      $picture = "<i>No Image</i>";
    else {
      $src = $func->get_src_modules($this->module . "/" . $row['picture'], 50, 0, 0);
      $picture = "<img src='" . $src . "' width=50 />";
    }

    $output['order'] = $row['ext'] . "<input name=\"txt_Order[{$id}]\" type=\"text\" size=\"3\"  style=\"text-align:center\" value=\"{$row['display_order']}\"  onkeypress=\"return is_num(event,'txtOrder')\" onchange='javascript:do_check($id)' />";

    $output['picture'] = $picture;
    $output['title'] = "<strong><a href='" . $link_edit . "' >" . $func->HTML($row['title']) . "</a></strong>";
    $output['title'] .= ' <span class=font_err >(ID: <b >' . $row['newsid'] . '</b>)</span>';
    //$output['title'] .= ' <div>Friendly url: '.$row['friendly_url'].'</div>';

    $info = "<div style='padding:2px;'>" . $vnT->lang['date_post'] . " : <b>" . @date("d/m/Y", $row['date_post']) . "</b></div>";
    $info .= "<div style='padding:2px;'>" . $vnT->lang['views'] . " : <strong>" . (int)$row['viewnum'] . "</strong></div>";
    $output['info'] = $info;


    $cat_name = "<div style='padding:2px;'>D.Mục : <b>" . $vnT->setting['arr_category'][$row['cat_id']]['cat_name'] . "</b></div>";
    $cat_name .= "<div style='padding:2px;'>L.Tin : <strong>" . $vnT->setting['arr_type'][$row['type']] . "</strong></div>";

    $output['cat_name'] = $cat_name;

    $link_display = $this->linkUrl . $row['ext_link'];
    if ($row['display'] == 1) {
      $display = "<a class='i-display' href='" . $link_display . "&do_hidden=$id' data-toggle='tooltip' data-placement='top'  title='" . $vnT->lang['click_do_hidden'] . "' ><i class=\"fa fa-eye\" aria-hidden=\"true\"></i></a>";
    } else {
      $display = "<a class='i-display'  href='" . $link_display . "&do_display=$id'  data-toggle='tooltip' data-placement='top'  title='" . $vnT->lang['click_do_display'] . "' ><i class=\"fa fa-eye-slash\" aria-hidden=\"true\"></i></a>";
    }

    $output['action'] = '<div class="action-buttons"><input name=h_id[]" type="hidden" value="' . $id . '" />';
    $output['action'] .= '<a href="' . $link_duplicate . '" class="i-duplicate" ><i class="fa fa-files-o" aria-hidden="true"></i></a>';
    $output['action'] .= '<a href="' . $link_edit . '" class="i-edit" ><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>';
    $output['action'] .= $display;
    $output['action'] .= '<a href="' . $link_del . '" class="i-del"><i class="fa fa-trash-o" aria-hidden="true"></i></a>';
    $output['action'] .= '</div>';
    return $output;
  }
  function do_Manage($lang){
    global $vnT, $func, $DB, $conf;
    $vnT->html->addStyleSheet($vnT->dir_js . "/jquery_ui/themes/base/ui.all.css");
    $vnT->html->addScript($vnT->dir_js . "/jquery_ui/ui.core.js");
    $vnT->html->addScript($vnT->dir_js . "/jquery_ui/ui.datepicker.js");
    $vnT->html->addScriptDeclaration("
	 		$(function() {
				$('.dates').datepicker({
					showOn: 'button',
					buttonImage: '" . $vnT->dir_images . "/calendar.gif',
					buttonImageOnly: true,
					changeMonth: true,
					changeYear: true
				}); 
			});
		");
    if ($vnT->input["do_action"]) {
      $func->clear_cache();
      if ($vnT->input["del_id"]) $h_id = $vnT->input["del_id"];
      switch ($vnT->input["do_action"]) {
        case "do_edit":
          if (isset($vnT->input["txt_Order"])) $arr_order = $vnT->input["txt_Order"];
          $mess .= "- " . $vnT->lang['edit_success'] . " ID: <strong>";
          $str_mess = "";
          for ($i = 0; $i < count($h_id); $i++) {
            $dup['display_order'] = $arr_order[$h_id[$i]];
            $dup['date_update'] = time();
            $ok = $DB->do_update("news", $dup, "newsid=" . $h_id[$i]);
            if ($ok) {
              $str_mess .= $h_id[$i] . ", ";
            }
          }
          $mess .= substr($str_mess, 0, -2) . "</strong><br>";
          $err = $func->html_mess($mess);
          break;
        case "do_hidden":
          $mess .= "- " . $vnT->lang['hidden_success'] . " ID: <strong>";
          for ($i = 0; $i < count($h_id); $i++) {
            $dup['display'] = 0;
            $ok = $DB->do_update("news_desc", $dup, "newsid={$h_id[$i]} AND lang='$lang' ");
            if ($ok) {
              $str_mess .= $h_id[$i] . ", ";
            }
          }
          $mess .= substr($str_mess, 0, -2) . "</strong><br>";
          $err = $func->html_mess($mess);
          break;
        case "do_display":
          $mess .= "- " . $vnT->lang['display_success'] . "  ID: <strong>";
          for ($i = 0; $i < count($h_id); $i++) {
            $dup['display'] = 1;
            $ok = $DB->do_update("news_desc", $dup, "newsid={$h_id[$i]}  AND lang='$lang' ");
            if ($ok) {
              $str_mess .= $h_id[$i] . ", ";
            }
          }
          $mess .= substr($str_mess, 0, -2) . "</strong><br>";
          $err = $func->html_mess($mess);
          break;
      }
    }
    if ((int)$vnT->input["do_display"]) {
      $ok = $DB->query("UPDATE news_desc SET display=1 WHERE newsid=".$vnT->input["do_display"]." AND lang='$lang' ");
      if ($ok) {
        $mess .= "- " . $vnT->lang['display_success'] . "  ID: <strong>" . $vnT->input["do_display"] . "</strong><br>";
        $err = $func->html_mess($mess);
      }
      //xoa cache
      $func->clear_cache();
    }
    if ((int)$vnT->input["do_hidden"]) {
      $ok = $DB->query("Update news_desc SET display=0 WHERE newsid=" . $vnT->input["do_hidden"] . "  AND lang='$lang' ");
      if ($ok) {
        $mess .= "- " . $vnT->lang['display_success'] . "  ID: <strong>" . $vnT->input["do_hidden"] . "</strong><br>";
        $err = $func->html_mess($mess);
      }
      //xoa cache
      $func->clear_cache();
    }
    $p = ((int)$vnT->input['p']) ? $vnT->input['p'] : 1;
    $n = ($conf['record']) ? $conf['record'] : 30;
    $cat_id = ((int)$vnT->input['cat_id']) ? $vnT->input['cat_id'] : 0;
    $search = ($vnT->input['search']) ? $vnT->input['search'] : "newsid";
    $keyword = ($vnT->input['keyword']) ? $vnT->input['keyword'] : "";
    $adminid = ((int)$vnT->input['adminid']) ? $vnT->input['adminid'] : 0;
    $date_begin = ($vnT->input['date_begin']) ? $vnT->input['date_begin'] : "";
    $date_end = ($vnT->input['date_end']) ? $vnT->input['date_end'] : "";
    $where = " ";
    $ext = "";
    $ext_page = "";
    if (!empty($cat_id)) {
      $where .= " AND FIND_IN_SET('$cat_id',cat_list)<>0 ";
      $ext_page .= "cat_id=$cat_id|";
      $ext = "&cat_id=$cat_id";
    }
    if (!empty($adminid)) {
      $where .= " and adminid=$adminid ";
      $ext_page .= "adminid=$adminid|";
      $ext .= "&adminid={$adminid}";
    }
    if ($date_begin || $date_end) {
      $tmp1 = @explode("/", $date_begin);
      $time_begin = @mktime(0, 0, 0, $tmp1[1], $tmp1[0], $tmp1[2]);

      $tmp2 = @explode("/", $date_end);
      $time_end = @mktime(23, 59, 59, $tmp2[1], $tmp2[0], $tmp2[2]);

      $where .= " AND (date_post BETWEEN {$time_begin} AND {$time_end} ) ";
      $ext .= "&date_begin=" . $date_begin . "&date_end=" . $date_end;
      $ext_page .= "date_begin=" . $date_begin . "|date_end=" . $date_end . "|";
    }
    if (!empty($keyword)) {
      switch ($search) {
        case "newsid" :
          $where .= " and  n.newsid = $keyword ";
          break;
        case "date_post" :
          $where .= " and DATE_FORMAT(FROM_UNIXTIME(date_post),'%d/%m/%Y') = '{$keyword}' ";
          break;
        default :
          $where .= " and $search like '%$keyword%' ";
          break;
      }
      $ext_page .= "keyword=$keyword|";
      $ext .= "&search={$search}&keyword={$keyword}";
    }
    $query= $DB->query("SELECT n.newsid FROM news n, news_desc nd
												WHERE n.newsid=nd.newsid AND lang='$lang' AND display != -1 $where");
    $totals = intval($DB->num_rows($query));
    $num_pages = ceil($totals / $n);
    if ($p > $num_pages) $p = $num_pages;
    if ($p < 1) $p = 1;
    $start = ($p - 1) * $n;
    $nav = $func->paginate($totals, $n, $ext, $p);
    $table['link_action'] = $this->linkUrl . "&p=$p";
    $ext_link = $ext . "&p=$p";

    $sortField = ($vnT->input['sortField']) ? $vnT->input['sortField'] : "display_order";
    $sortOrder = ($vnT->input['sortOrder']) ? $vnT->input['sortOrder'] : "desc";
    $OrderBy = " ORDER BY $sortField $sortOrder , date_post DESC ";
    $sortLinks = array("display_order", "title", "cat_id", "date_post", "display");
    $data['SortLink'] = $func->BuildSortingLinks($sortLinks, $this->linkUrl . $ext . "&p=" . $p, $sortField, $sortOrder);

    $ext_link .= "&sortField=$sortField&sortOrder=$sortOrder";

    $table['title'] = array(
      'check_box' => "<input type=\"checkbox\" name=\"checkall\" id=\"checkall\" class=\"checkbox\" />|3%|center",
      'order' => $vnT->lang['order'] . " {$data['SortLink']['display_order']}|7%|center",
      'picture' => $vnT->lang['picture'] . "|10%|center",
      'title' => $vnT->lang['title'] . " {$data['SortLink']['title']}||left",
      'cat_name' => "Danh mục & Loại tin {$data['SortLink']['cat_id']}|20%|left",
      'info' => $vnT->lang['infomartion'] . " {$data['SortLink']['date_post']}|17%|left",
      'action' => "Action {$data['SortLink']['display']}|10%|center");
    $sql = "SELECT * 
						FROM news n, news_desc nd
						WHERE n.newsid=nd.newsid
						AND lang='$lang' 
						AND display != -1
						$where 
						$OrderBy LIMIT $start,$n";
    //print "sql = ".$sql."<br>";
    $result = $DB->query($sql);
    if ($DB->num_rows($result)) {
      $row = $DB->get_array($result);
      for ($i = 0; $i < count($row); $i++) {
        $row[$i]['ext_link'] = $ext_link;
        $row[$i]['ext_page'] = $ext_page;
        $row_info = $this->render_row($row[$i], $lang);
        $row_field[$i] = $row_info;
        $row_field[$i]['stt'] = ($i + 1);
        $row_field[$i]['row_id'] = "row_" . $row[$i]['newsid'];
        $row_field[$i]['ext'] = "";
      }
      $table['row'] = $row_field;
    } else {
      $table['row'] = array();
      $table['extra'] = "<div align=center class=font_err >" . $vnT->lang['no_have_news'] . "</div>";
    }
    $table['button'] = '<input type="button" name="btnHidden" value=" ' . $vnT->lang['hidden'] . ' " class="button" onclick="do_submit(\'do_hidden\')">&nbsp;';
    $table['button'] .= '<input type="button" name="btnDisplay" value=" ' . $vnT->lang['display'] . ' " class="button" onclick="do_submit(\'do_display\')">&nbsp;';
    $table['button'] .= '<input type="button" name="btnEdit" value=" ' . $vnT->lang['update'] . ' " class="button" onclick="do_submit(\'do_edit\')">&nbsp;';
    $table['button'] .= '<input type="button" name="btnDuplicate" value=" ' . $vnT->lang['duplicate'] . ' " class="button" onclick="action_selected(\'' . $this->linkUrl . '&sub=duplicate&ext=' . $ext_page . '\', \'' . $vnT->lang["duplicate"] . '?\')">';
    $table['button'] .= '<input type="button" name="btnDel" value=" ' . $vnT->lang['delete'] . ' " class="button" onclick="del_selected(\'' . $this->linkUrl . '&sub=del&ext=' . $ext_page . '\')">';
    $table_list = $func->ShowTable($table);

    $data['table_list'] = $table_list;
    $data['totals'] = $totals;
    $data['link_fsearch'] = $this->linkUrl;
    $data['list_cat'] = $this->Get_Cat($cat_id, $lang, " ");

    $data['date_begin'] = $date_begin;
    $data['date_end'] = $date_end;
    $data['list_search'] = $this->List_Search($search);

    $data['keyword'] = $keyword;
    $data['cat_id'] = $cat_id;
    $data['err'] = $err;
    $data['nav'] = $nav;
    /*assign the array to a template variable*/
    $this->skin->assign('data', $data);
    $this->skin->parse("manage");
    return $this->skin->text("manage");
  }
}

$vntModule = new vntModule();
?>