<?php
/*================================================================================*\
|| 						           Name code : focus_news.php 		 		            	  			||
||  				     Copyright @2008 by Thai Son - CMS vnTRUST                  			||
\*================================================================================*/
/**
 * @version : 2.0
 * @date upgrade : 09/01/2009 by Thai Son
 **/
if (! defined('IN_vnT')) {
  die('Hacking attempt!');
}
$vntModule = new vntModule();

class vntModule
{
  var $output = "";
  var $skin = "";
  var $linkUrl = "";
  var $module = "news";
  var $action = "focus_img";

  /**
   * function vntModule ()
   * Khoi tao 
   **/
  function vntModule ()
  {
    global $Template, $vnT, $func, $DB, $conf;
    require_once ("function_" . $this->module . ".php");
    $this->skin = new XiTemplate(DIR_MODULE . DS . $this->module . "_ad" . DS . "html" . DS . $this->action . ".tpl");
    $this->skin->assign('LANG', $vnT->lang);
    $lang = ($vnT->input['lang']) ? $lang = $vnT->input['lang'] : $func->get_lang_default();
    $this->linkUrl = "?mod=" . $this->module . "&act=" . $this->action . "&lang=" . $lang;
    switch ($vnT->input['sub']) {
      case 'add':
        $nd['f_title'] = 'Thêm tiêu điểm ảnh';
        $nd['content'] = $this->do_Add($lang);
      break;
      case 'edit':
        $nd['f_title'] = 'Cập nhật tiêu điểm ảnh';
        $nd['content'] = $this->do_Edit($lang);
      break;
      case 'del':
        $this->do_Del($lang);
      break;
      default:
        $nd['f_title'] = 'Quản lý tiêu điểm ảnh';
        $nd['content'] = $this->do_Manage($lang);
      break;
    }
    $nd['menu'] = getToolbar_FocusImg($lang);
    $nd['row_lang'] = $func->html_lang("?mod=" . $this->module . "&act=" . $this->action, $lang);
    $Template->assign("data", $nd);
    $Template->parse("box_main");
    $vnT->output .= $Template->text("box_main");
  }

  /**
   * function do_Add 
   * Them  moi 
   **/
  function do_Add ($lang)
  {
    global $vnT, $func, $DB, $conf;
    $err = "";
    $data['display'] = 1;
    $data['gio'] = date("H:i");
    $data['ngay'] = date("d/m/Y");
    $data['answer_by'] = $vnT->admininfo['username'];
    $data['file_attach'] = '<span class=font_err>Chức năng này vô hiệu lực trong admin</span>';
    if ($vnT->input['btnAdd']) {
      $data = $_POST;
      $title = chop($vnT->input['title']);
      $picture = chop($vnT->input['picture']);
      $link = chop($vnT->input['link']);
      if ($vnT->input['chk_upload'] == 1) {
        $up['path'] = MOD_DIR_UPLOAD;
        $up['dir'] = "news_focus";
        $up['file'] = $_FILES['image'];
        $up['type'] = "hinh";
        $up['w'] = 300;
        $result = $vnT->File->Upload($up);
        if (empty($result['err'])) {
          $picture = $result['link'];
          $type = $result['type'];
        } else {
          $err = $result['err'];
        }
      } else {
        $fext = strtolower(substr($picture, strrpos($picture, ".") + 1));
        $type = $fext;
        if (($fext == "jpg") || ($fext == "gif") || ($fext == "png") || ($fext == "bmp") || ($fext == "swf")) {
          $lastx = strrpos($picture, "/");
          $fname = MOD_DIR_UPLOAD . "news_focus/" . substr($picture, $lastx + 1);
          $fname = str_replace("%20", "_", $fname);
          $fname = str_replace(" ", "_", $fname);
          $fname = str_replace("&amp;", "_", $fname);
          $fname = str_replace("&", "_", $fname);
          $fname = str_replace(";", "_", $fname);
          if (file_exists($fname))
            $fname = ROOTUPLOAD . "MOD_DIR_UPLOAD/" . time() . "_" . substr($picture, $lastx + 1);
          $file = @fopen($fname, "w");
          if ($f = @fopen($picture, "r")) {
            while (! @feof($f)) {
              @fwrite($file, fread($f, 1024));
            }
            @fclose($f);
            @fclose($file);
            $picture = substr($fname, strrpos($fname, "/") + 1);
          } else
            $err = "Cannot Read from this Image ! Plz save to your Computer and Upload It";
        } else
          $err = "Image Type Not Support";
      }
      $picture = chop($picture);
      if (empty($picture))
        $err = "No Focus Images Image selected";
        // insert CSDL
      if (empty($err)) {
        $cot['picture'] = $picture;
        $cot['link'] = $link;
        $cot['title'] = $title;
        $cot['f_order'] = 0;
        $cot['lang'] = $lang;
        $ok = $DB->do_insert("news_focus_image", $cot);
        if ($ok) {
          //xoa cache
          $func->clear_cache();
          //insert adminlog
          $func->insertlog("Add", $_GET['act'], $fid);
          $mess = $vnT->lang['add_success'];
          $url = $this->linkUrl . "&sub=add";
          $func->html_redirect($url, $mess);
        } else {
          $err = $func->html_err($vnT->lang['add_failt'] . $DB->debug());
        }
      }
    }
    $data['err'] = $err;
    $data['link'] = $conf['rooturl'] . "?vnTRUST=act:news|newsid:ID";
    $data['link_action'] = $this->linkUrl . "&sub=add";
    /*assign the array to a template variable*/
    $this->skin->assign('data', $data);
    $this->skin->parse("edit");
    return $this->skin->text("edit");
  }

  /**
   * function do_Edit 
   * Cap nhat 
   **/
  function do_Edit ($lang)
  {
    global $vnT, $func, $DB, $conf;
    $id = (int) $vnT->input['id'];
    if ($vnT->input['btnAdd']) {
      $data = $_POST;
      $title = $vnT->input['title'];
      $picture = $vnT->input['picture'];
      $link = $vnT->input['link'];
      if ($_POST['chk_upload'] == 1 && ! empty($_FILES['image'])) {
        $up['path'] = MOD_DIR_UPLOAD;
        $up['dir'] = "news_focus";
        $up['file'] = $_FILES['image'];
        $up['type'] = "hinh";
        $up['w'] = 300;
        $result = $vnT->File->Upload($up);
        if (empty($result['err'])) {
          $picture = $result['link'];
          $type = $result['type'];
        } else {
          $err = $result['err'];
        }
      } else {
        if ($picture != "") {
          $fext = strtolower(substr($picture, strrpos($picture, ".") + 1));
          $type = $fext;
          if (($fext == "jpg") || ($fext == "gif") || ($fext == "png") || ($fext == "bmp") || ($fext == "swf")) {
            $lastx = strrpos($picture, "/");
            $fname = MOD_DIR_UPLOAD . "news_focus/" . substr($picture, $lastx + 1);
            $fname = str_replace("%20", "_", $fname);
            $fname = str_replace(" ", "_", $fname);
            $fname = str_replace("&amp;", "_", $fname);
            $fname = str_replace("&", "_", $fname);
            $fname = str_replace(";", "_", $fname);
            if (file_exists($fname))
              $fname = MOD_DIR_UPLOAD . "news_focus/" . time() . "_" . substr($picture, $lastx + 1);
            $file = @fopen($fname, "w");
            if ($f = @fopen($picture, "r")) {
              while (! @feof($f)) {
                @fwrite($file, fread($f, 1024));
              }
              @fclose($f);
              @fclose($file);
              $picture = substr($fname, strrpos($fname, "/") + 1);
            } else
              $err = "Cannot Read from this Image ! Plz save to your Computer and Upload It";
          } else
            $err = "Image Type Not Support";
        }
      }
      if (empty($err)) {
        if (! empty($picture)) {
          // Del Image
          $query = $DB->query("SELECT picture FROM news_focus_image WHERE id='{$id}'");
          while ($img = $DB->fetch_row($query)) {
            if ((file_exists(MOD_DIR_UPLOAD . "news_focus/" . $img['picture'])) && (! empty($img['picture'])))
              @unlink(MOD_DIR_UPLOAD . "news_focus/" . $img['picture']);
          }
          // end del
          $cot['picture'] = $picture;
        }
        $cot['title'] = $title;
        $cot['link'] = $link;
        $ok = $DB->do_update("news_focus_image", $cot, "id=$id");
        if ($ok) {
          //xoa cache
          $func->clear_cache();
          //insert adminlog
          $func->insertlog("Edit", $_GET['act'], $id);
          $err = $vnT->lang["edit_success"];
          $url = $this->linkUrl . "&sub=edit&id=$id";
          $func->html_redirect($url, $err);
        } else
          $err = $func->html_err($vnT->lang["edit_failt"] . $DB->debug());
      }
    }
    $query = $DB->query("SELECT * FROM news_focus_image WHERE id='{$id}'");
    if ($data = $DB->fetch_row($query)) {
      $l_oldurl = MOD_DIR_UPLOAD . "news_focus/" . $data['picture'];
      $data['html_img'] = "<img src=\"{$l_oldurl}\" />";
    } else {
      $mess = $vnT->lang['not_found'] . " ID : " . $id;
      $url = $this->linkUrl;
      $func->html_redirect($url, $mess);
    }
    $data['err'] = $err;
    $data['link_action'] = $this->linkUrl . "&sub=edit&id=$id";
    /*assign the array to a template variable*/
    $this->skin->assign('data', $data);
    $this->skin->parse("edit");
    return $this->skin->text("edit");
  }

  /**
   * function do_Del 
   * Xoa 1 ... n  gioi thieu 
   **/
  function do_Del ($lang)
  {
    global $func, $DB, $conf, $vnT;
    $id = (int) $vnT->input['id'];
    $ext = $vnT->input["ext"];
    $del = 0;
    $qr = "";
    if ($id != 0) {
      $del = 1;
      $qr = " OR id='{$id}' ";
    }
    if (isset($vnT->input["del_id"])) {
      $ids = implode(',', $vnT->input["del_id"]);
      $key = $vnT->input["del_id"];
    }
    for ($i = 0; $i < count($key); $i ++) {
      $del = 1;
      $qr .= " OR id='{$key[$i]}' ";
    }
    if (isset($vnT->input["btnOrder"])) {
      if (isset($vnT->input["h_id"]))
        $h_id = $vnT->input["h_id"];
      else
        $h_id = "";
      if (isset($vnT->input["txtOrder"]))
        $order = $vnT->input["txtOrder"];
      else
        $order = "";
      for ($i = 0; $i < count($order); $i ++) {
        $update_q = $DB->query("UPDATE news_focus_image SET f_order ='{$order[$i]}' WHERE id='{$h_id[$i]}'");
      }
    }
    if ($del) {
      // Del Image
      $query = $DB->query("SELECT picture FROM news_focus_image WHERE id=-1" . $qr);
      while ($img = $DB->fetch_row($query)) {
        if ((file_exists(MOD_DIR_UPLOAD . "news_focus/" . $img['picture'])) && (! empty($img['picture'])))
          @unlink(MOD_DIR_UPLOAD . "news_focus/" . $img['picture']);
      }
      // End del image
      $query = "DELETE FROM news_focus_image WHERE id=-1" . $qr;
      if ($ok = $DB->query($query)) {
        //xoa cache
        $func->clear_cache();
        $mess = $vnT->lang["del_success"];
      } else
        $mess = $vnT->lang["del_failt"];
    }
    $ext_page = str_replace("|", "&", $ext);
    $url = $this->linkUrl . "&{$ext_page}";
    $func->html_redirect($url, $mess);
  }

  /**
   * function render_row 
   * list cac record
   **/
  function render_row ($row_info, $lang)
  {
    global $func, $DB, $conf, $vnT;
    $row = $row_info;
    // Xu ly tung ROW
    $id = $row['id'];
    $row_id = "row_" . $id;
    $output['check_box'] = "<input type=\"checkbox\" name=\"del_id[]\" value=\"{$id}\" class=\"checkbox\" onclick=\"select_row('{$row_id}')\">";
    $link_edit = $this->linkUrl . "&sub=edit&id={$id}";
    $link_del = "javascript:del_item('" . $this->linkUrl . "&sub=del&id={$id}')";
    $output['f_order'] = "<input name=\"txt_Order[{$id}]\" type=\"text\" size=\"2\" maxlength=\"2\" style=\"text-align:center\" value=\"{$row['f_order']}\"  onkeypress=\"return is_num(event,'txtOrder')\"  onchange='javascript:do_check($id)' />";
    $output['picture'] = $func->HTML($row['title']) . "<br>";
    $output['picture'] .= "<img src=\"" . MOD_DIR_UPLOAD . "news_focus/{$row['picture']}\">";
    $output['picture'] .= "<br>" . $row['link'];
    $output['action'] = '<input name=h_id[]" type="hidden" value="' . $id . '" />';
    $output['action'] .= '<a href="' . $link_edit . '"><img src="' . $vnT->dir_images . '/edit.gif"  alt="Edit "></a>&nbsp;';
    $output['action'] .= '<a href="' . $link_del . '"><img src="' . $vnT->dir_images . '/delete.gif"  alt="Delete "></a>';
    return $output;
  }

  /**
   * function do_Manage() 
   * Quan ly cac gioi thieu
   **/
  function do_Manage ($lang)
  {
    global $vnT, $func, $DB, $conf;
    //update
    if ($vnT->input["do_action"]) {
      //xoa cache
      $func->clear_cache();
      if ($vnT->input["del_id"])
        $h_id = $vnT->input["del_id"];
      switch ($vnT->input["do_action"]) {
        case "do_edit":
          if (isset($vnT->input["txt_Order"]))
            $arr_order = $vnT->input["txt_Order"];
          $mess .= "- " . $vnT->lang['edit_success'] . " ID: <strong>";
          $str_mess = "";
          for ($i = 0; $i < count($h_id); $i ++) {
            $dup['f_order'] = $arr_order[$h_id[$i]];
            $ok = $DB->do_update("news_focus_image", $dup, "id={$h_id[$i]}");
            if ($ok) {
              $str_mess .= $h_id[$i] . ", ";
            }
          }
          $mess .= substr($str_mess, 0, - 2) . "</strong><br>";
          $err = $func->html_mess($mess);
        break;
        case "do_hidden":
          {}
          ;
        break;
        case "do_display":
          {}
          ;
        break;
      }
    }
    $p = ((int) $vnT->input['p']) ? $p = $vnT->input['p'] : 1;
    $n = ($conf['record']) ? $conf['record'] : 30;
    $query = $DB->query("SELECT * FROM news_focus_image where lang='{$lang}'");
    $totals = intval($DB->num_rows($query));
    $num_pages = ceil($totals / $n);
    if ($p > $num_pages)
      $p = $num_pages;
    if ($p < 1)
      $p = 1;
    $start = ($p - 1) * $n;
    $nav = $func->paginate($totals, $n, $ext, $p);
    $table['link_action'] = $this->linkUrl . "&sub=manage";
    $table['title'] = array(
      'check_box' => "<input type=\"checkbox\" name=\"all\" class=\"checkbox\" onclick=\"javascript:checkall();\" />|5%|center" , 
      'f_order' => "Th&#7913; t&#7921;|10%|center" , 
      'picture' => "H&#236;nh |70%|left" , 
      'action' => "Action|10%|center");
    $sql = "SELECT * FROM news_focus_image where lang='{$lang}' ORDER BY f_order ASC LIMIT $start,$n";
    // print "sql = ".$sql."<br>";
    $result = $DB->query($sql);
    if ($DB->num_rows($result)) {
      $row = $DB->get_array($result);
      for ($i = 0; $i < count($row); $i ++) {
        $row_info = $this->render_row($row[$i], $lang);
        $row_field[$i] = $row_info;
        $row_field[$i]['stt'] = ($i + 1);
        $row_field[$i]['row_id'] = "row_" . $row[$i]['id'];
        $row_field[$i]['ext'] = "";
      }
      $table['row'] = $row_field;
    } else {
      $table['row'] = array();
      $table['extra'] = "<div align=center class=font_err >" . $vnT->lang['no_have_news'] . "</div>";
    }
    $table['button'] .= '<input type="button" name="btnEdit" value=" ' . $vnT->lang['update'] . ' " class="button" onclick="do_submit(\'do_edit\')">&nbsp;';
    $table['button'] .= '<input type="button" name="btnDel" value=" ' . $vnT->lang['delete'] . ' " class="button" onclick="del_selected(\'' . $this->linkUrl . '&sub=del&ext=' . $ext_page . '\')">';
    $table_list = $func->ShowTable($table);
    $data['table_list'] = $table_list;
    $data['totals'] = $totals;
    $data['err'] = $err;
    $data['nav'] = $nav;
    /*assign the array to a template variable*/
    $this->skin->assign('data', $data);
    $this->skin->parse("manage");
    return $this->skin->text("manage");
  }
  // end class
}
