<?php
/*================================================================================*\
|| 							Name code : key_referent.php 		 		            	  ||
||  				Copyright @2008 by Thai Son - CMS vnTRUST                     ||
\*================================================================================*/
/**
 * @version : 2.0
 * @date upgrade : 09/01/2009 by Thai Son
 **/
if (! defined('IN_vnT')) {
  die('Hacking attempt!');
}

//load Model
include(dirname(__FILE__) . "/functions.php");

class vntModule extends Model
{
  var $output = "";
  var $skin = "";
  var $linkUrl = "";
  var $module = "news";
  var $action = "key_referent";

  /**
   * function vntModule ()
   * Khoi tao 
   **/
  function vntModule ()
  {
    global $Template, $vnT, $func, $DB, $conf;
    
    $this->skin = new XiTemplate(DIR_MODULE . DS . $this->module . "_ad" . DS . "html" . DS . $this->action . ".tpl");
    $this->skin->assign('LANG', $vnT->lang);
    $lang = ($vnT->input['lang']) ? $lang = $vnT->input['lang'] : $func->get_lang_default();
    $this->linkUrl = "?mod=" . $this->module . "&act=" . $this->action . "&lang=" . $lang;
    switch ($vnT->input['sub']) {
      case 'add':
        $nd['f_title'] = $vnT->lang['add_key_referent'];
        $nd['content'] = $this->do_Add($lang);
      break;
      case 'edit':
        $nd['f_title'] = $vnT->lang['edit_key_referent'];
        $nd['content'] = $this->do_Edit($lang);
      break;
      case 'view':
        $nd['f_title'] = $vnT->lang['view_key_referent'];
        $nd['content'] = $this->do_View($lang);
      break;
      case 'set_news':
        $this->do_SetNews($lang);
      case 'remove_news':
        $this->do_Remove($lang);
      case 'del':
        $this->do_Del($lang);
      break;
      default:
        $nd['f_title'] = $vnT->lang['manage_key_referent'];
        $nd['content'] = $this->do_Manage($lang);
      break;
    }
    $nd['menu'] = $func->getToolbar($this->module, $this->action, $lang);
    $nd['row_lang'] = $func->html_lang("?mod=" . $this->module . "&act=" . $this->action, $lang);
    $Template->assign("data", $nd);
    $Template->parse("box_main");
    $vnT->output .= $Template->text("box_main");
  }

  /**
   * function do_Add 
   * Them  moi 
   **/
  function do_Add ($lang)
  {
    global $vnT, $func, $DB, $conf;
    $err = "";
    if ($vnT->input['do_submit'] == 1) {
      $data = $_POST;
      // insert CSDL
      if (empty($err)) {
        $cot['title'] = $func->txt_HTML($_POST['title']);
        $ok = $DB->do_insert("news_key_referent", $cot);
        if ($ok) {
          //xoa cache
          $func->clear_cache();
          //insert adminlog
          $func->insertlog("Add", $_GET['act'], $fid);
          $mess = $vnT->lang['add_success'];
          $url = $this->linkUrl . "&sub=add";
          $func->html_redirect($url, $mess);
        } else {
          $err = $func->html_err($vnT->lang['add_failt'] . $DB->debug());
        }
      }
    }
    $data['err'] = $err;
    $data['link_action'] = $this->linkUrl . "&sub=add";
    /*assign the array to a template variable*/
    $this->skin->assign('data', $data);
    $this->skin->parse("edit");
    return $this->skin->text("edit");
  }

  /**
   * function do_Edit 
   * Cap nhat 
   **/
  function do_Edit ($lang)
  {
    global $vnT, $func, $DB, $conf;
    $id = (int) $vnT->input['id'];
    if ($vnT->input['do_submit']) {
      $data = $_POST;
      if (empty($err)) {
        $cot['title'] = $vnT->input['title'];
        $ok = $DB->do_update("news_key_referent", $cot, "keyref_id=$id");
        if ($ok) {
          //xoa cache
          $func->clear_cache();
          //insert adminlog
          $func->insertlog("Edit", $_GET['act'], $id);
          $err = $vnT->lang["edit_success"];
          $url = $this->linkUrl . "&sub=edit&id=$id";
          $func->html_redirect($url, $err);
        } else
          $err = $func->html_err($vnT->lang["edit_failt"] . $DB->debug());
      }
    }
    $query = $DB->query("SELECT * FROM news_key_referent WHERE keyref_id=$id");
    if ($data = $DB->fetch_row($query)) {
    } else {
      $mess = $vnT->lang['not_found'] . " ID : " . $id;
      $url = $this->linkUrl;
      $func->html_redirect($url, $mess);
    }
    $data['err'] = $err;
    $data['link_action'] = $this->linkUrl . "&sub=edit&id=$id";
    /*assign the array to a template variable*/
    $this->skin->assign('data', $data);
    $this->skin->parse("edit");
    return $this->skin->text("edit");
  }

  /**
   * function do_Del 
   * Xoa 1 ... n  gioi thieu 
   **/
  function do_Del ($lang)
  {
    global $func, $DB, $conf, $vnT;
    $id = (int) $vnT->input['id'];
    $ext = $vnT->input["ext"];
    $del = 0;
    $qr = "";
    if ($id != 0) {
      $ids = $id;
    }
    if (isset($vnT->input["del_id"])) {
      $ids = implode(',', $vnT->input["del_id"]);
    }
    $query = 'DELETE FROM news_key_referent WHERE keyref_id IN (' . $ids . ')';
    if ($ok = $DB->query($query)) {
      $mess = $vnT->lang["del_success"];
    } else
      $mess = $vnT->lang["del_failt"];
    $ext_page = str_replace("|", "&", $ext);
    $url = $this->linkUrl . "&{$ext_page}";
    $func->html_redirect($url, $mess);
  }

  /**
   * function render_row 
   * list cac record
   **/
  function render_row ($row_info, $lang)
  {
    global $func, $DB, $conf, $vnT;
    $row = $row_info;
    // Xu ly tung ROW
    $id = $row['keyref_id'];
    $row_id = "row_" . $id;
    $output['check_box'] = vnT_HTML::checkbox("del_id[]", $id, 0, " ");
    $link_edit = $this->linkUrl . "&sub=edit&id={$id}";
    $link_del = "javascript:del_item('" . $this->linkUrl . "&sub=del&id={$id}')";
    $link_view = $this->linkUrl . "&sub=view&id={$id}";
    $text_edit = "news_key_referent|title|keyref_id=" . $id;
    $output['title'] = "<strong>" . $func->HTML($row['title']) . "</strong>";
    if ($row['display'] == 1) {
      $display = "<img src=\"" . $vnT->dir_images . "/dispay.gif\" width=15  />";
    } else {
      $display = "<img src=\"" . $vnT->dir_images . "/nodispay.gif\"  width=15 />";
    }
    $output['order'] = $row['ext'] . "<input name=\"txt_Order[{$id}]\" type=\"text\" size=\"2\" maxlength=\"2\" style=\"text-align:center\" value=\"{$row['keyref_order']}\"  onkeypress=\"return is_num(event,'txtOrder')\" onchange='javascript:do_check($id)' />";
    $output['view'] = "<a href=\"{$link_view}\"><img src=\"" . $vnT->dir_images . "/but_view.gif\" width=22  align=absmiddle /></a>&nbsp;<span class=font_err>(<strong>" . get_num_referent($id) . "</strong> tin)</span>";
    $output['action'] = '<input name=h_id[]" type="hidden" value="' . $id . '" />';
    $output['action'] .= '<a href="' . $link_edit . '"><img src="' . $vnT->dir_images . '/edit.gif"  alt="Edit "></a>&nbsp;';
    $output['action'] .= $display . '&nbsp;';
    $output['action'] .= '<a href="' . $link_del . '"><img src="' . $vnT->dir_images . '/delete.gif"  alt="Delete "></a>';
    return $output;
  }

  /**
   * function do_Manage() 
   * Quan ly cac gioi thieu
   **/
  function do_Manage ($lang)
  {
    global $vnT, $func, $DB, $conf;
    //update
    if ($vnT->input["do_action"]) {
      //xoa cache
      $func->clear_cache();
      if ($vnT->input["del_id"])
        $h_id = $vnT->input["del_id"];
      switch ($vnT->input["do_action"]) {
        case "do_edit":
          if (isset($vnT->input["txt_Order"]))
            $arr_order = $vnT->input["txt_Order"];
          $mess .= "- " . $vnT->lang['edit_success'] . " ID: <strong>";
          $str_mess = "";
          for ($i = 0; $i < count($h_id); $i ++) {
            $dup['keyref_order'] = $arr_order[$h_id[$i]];
            $ok = $DB->do_update("news_key_referent", $dup, "keyref_id=" . $h_id[$i]);
            if ($ok) {
              $str_mess .= $h_id[$i] . ", ";
            }
          }
          $mess .= substr($str_mess, 0, - 2) . "</strong><br>";
          $err = $func->html_mess($mess);
        break;
        case "do_hidden":
          $mess .= "- " . $vnT->lang['hidden_success'] . " ID: <strong>";
          for ($i = 0; $i < count($h_id); $i ++) {
            $dup['display'] = 0;
            $ok = $DB->do_update("news_key_referent", $dup, "keyref_id=" . $h_id[$i]);
            if ($ok) {
              $str_mess .= $h_id[$i] . ", ";
            }
          }
          $mess .= substr($str_mess, 0, - 2) . "</strong><br>";
          $err = $func->html_mess($mess);
        break;
        case "do_display":
          $mess .= "- " . $vnT->lang['display_success'] . "  ID: <strong>";
          for ($i = 0; $i < count($h_id); $i ++) {
            $dup['display'] = 1;
            $ok = $DB->do_update("news_key_referent", $dup, "keyref_id=" . $h_id[$i]);
            if ($ok) {
              $str_mess .= $h_id[$i] . ", ";
            }
          }
          $mess .= substr($str_mess, 0, - 2) . "</strong><br>";
          $err = $func->html_mess($mess);
        break;
      }
    }
    $p = ((int) $vnT->input['p']) ? $p = $vnT->input['p'] : 1;
    $n = ($conf['record']) ? $conf['record'] : 30;
    $query = $DB->query("SELECT keyref_id FROM news_key_referent  ");
    $totals = intval($DB->num_rows($query));
    $num_pages = ceil($totals / $n);
    if ($p > $num_pages)
      $p = $num_pages;
    if ($p < 1)
      $p = 1;
    $start = ($p - 1) * $n;
    $nav = $func->paginate($totals, $n, $ext, $p);
    $table['link_action'] = $this->linkUrl . "&sub=manage";
    $table['title'] = array(
      'check_box' => "<input type=\"checkbox\" name=\"checkall\" id=\"checkall\" class=\"checkbox\" />|5%|center" , 
      'order' => $vnT->lang['order'] . "|10%|center" , 
      'title' => $vnT->lang['title'] . "|40%|left" , 
      'view' => $vnT->lang['view_news'] . "|10%|center" , 
      'action' => "Action|15%|center");
    $sql = "SELECT * FROM news_key_referent  ORDER BY  keyref_order LIMIT $start,$n";
    // print "sql = ".$sql."<br>";
    $result = $DB->query($sql);
    if ($DB->num_rows($result)) {
      $row = $DB->get_array($result);
      for ($i = 0; $i < count($row); $i ++) {
        $row_info = $this->render_row($row[$i], $lang);
        $row_field[$i] = $row_info;
        $row_field[$i]['stt'] = ($i + 1);
        $row_field[$i]['row_id'] = "row_" . $row[$i]['keyref_id'];
        $row_field[$i]['ext'] = "";
      }
      $table['row'] = $row_field;
    } else {
      $table['row'] = array();
      $table['extra'] = "<div align=center class=font_err >" . $vnT->lang['no_have_news_key_referent'] . "</div>";
    }
    $table['button'] = '<input type="button" name="btnHidden" value=" ' . $vnT->lang['hidden'] . ' " class="button" onclick="do_submit(\'do_hidden\')">&nbsp;';
    $table['button'] .= '<input type="button" name="btnDisplay" value=" ' . $vnT->lang['display'] . ' " class="button" onclick="do_submit(\'do_display\')">&nbsp;';
    $table['button'] .= '<input type="button" name="btnEdit" value=" ' . $vnT->lang['update'] . ' " class="button" onclick="do_submit(\'do_edit\')">&nbsp;';
    $table['button'] .= '<input type="button" name="btnDel" value=" ' . $vnT->lang['delete'] . ' " class="button" onclick="del_selected(\'' . $this->linkUrl . '&sub=del&ext=' . $ext_page . '\')">';
    $table_list = $func->ShowTable($table);
    $data['table_list'] = $table_list;
    $data['totals'] = $totals;
    $data['err'] = $err;
    $data['nav'] = $nav;
    /*assign the array to a template variable*/
    $this->skin->assign('data', $data);
    $this->skin->parse("manage");
    return $this->skin->text("manage");
  }

  /**
   * function do_View 
   * Cap nhat 
   **/
  function do_View ($lang)
  {
    global $vnT, $func, $DB, $conf;
    if ((isset($_GET['id'])) && (is_numeric($_GET['id'])))
      $id = $_GET['id'];
    else
      $id = 0;
    $query = $DB->query("SELECT * FROM news_key_referent WHERE keyref_id=$id");
    if ($row = $DB->fetch_row($query)) {
      $row['title'] = $func->HTML($row['title']);
    } else {
      $mess = $vnT->lang['not_found'] . " ID : " . $id;
      $url = $this->linkUrl;
      $func->html_redirect($url, $mess);
    }
    $data['list_referent'] = $this->list_referent($row, $lang);
    $data['list_news'] = $this->list_news($row, $lang);
    $data['err'] = $err;
    $this->skin->assign('data', $data);
    $this->skin->parse("view");
    return $this->skin->text("view");
  }

  /**
   * function render_row_referent 
   *  
   **/
  function render_row_referent ($row_info, $lang)
  {
    global $func, $DB, $conf, $vnT, $a_unit;
    $row = $row_info;
    // Xu ly tung ROW
    $id = $row['newsid'];
    $row_id = "rowkey_" . $id;
    $output['row_id'] = $row_id;
    $output['check_box'] = vnT_HTML::checkbox("del_key[]", $id, 0, " onclick=\"select_row('" . $row_id . "')\" ");
    $link_edit = "?mod=news&act=news&sub=edit&id={$id}&lang=$lang";
    if ($row['cid'])
      $order = (int) $row['focus_cat_order'];
    else
      $order = (int) $row['focus_order'];
    $output['order'] = "<input name=\"txtOrder[{$id}]\" type=\"text\" size=\"2\" maxlength=\"2\" style=\"text-align:center\" value=\"{$order}\"  onkeypress=\"return is_num(event,'txtOrder')\" onchange='javascript:do_check_focus($id)' />";
    if (empty($row['picture']))
      $picture = "<i>No Image</i>";
    else {
      $picture = $this->get_picture($row['picture'], 50);
    }
    $output['title'] = "<a href=\"{$link_edit}\">" . $func->HTML($row['title']) . "</a>";
    $output['cat_name'] = get_cat_name($row['cat_id'], $lang);
    $output['picture'] = $picture;
    return $output;
  }

  /**
   * function list_referent 
   *  
   **/
  function list_referent ($info, $lang)
  {
    global $func, $DB, $conf, $vnT;
    $p = ((int) $vnT->input['p']) ? $p = $vnT->input['p'] : 1;
    $id = $info['keyref_id'];
    $where = "and keyref_id=" . $id;
    $query = $DB->query("SELECT n.newsid
											FROM news n,  news_desc nd
											WHERE n.newsid=nd.newsid
											AND display=1
											AND lang='$lang' 
											$where");
    $totals = $DB->num_rows($query);
    $n = 10;
    $num_pages = ceil($totals / $n);
    if ($p1 > $num_pages)
      $p = $num_pages;
    if ($p1 < 1)
      $p1 = 1;
    $start = ($p1 - 1) * $n;
    $nav = "<div align=left>&nbsp;<span class=\"pagelink\">{$num_pages} Page(s) :</span>&nbsp;";
    for ($i = 1; $i < $num_pages + 1; $i ++) {
      if ($i == $p1)
        $nav .= "<span class=\"pagecur\">{$i}</span>&nbsp";
      else
        $nav .= "<a href='" . $this->linkUrl . "&lang={$lang}&p1={$i}'><span class=\"pagelink\">{$i}</span></a>&nbsp ";
    }
    $nav .= "</div>";
    $html_row = "";
    $sql .= " SELECT *
							FROM news n,  news_desc nd
							WHERE n.newsid=nd.newsid
							AND display=1
							AND lang='$lang' 
							$where
							order by date_post DESC
							limit $start,$n";
    //print "sql = ".$sql."<br>";
    $result = $DB->query($sql);
    if ($DB->num_rows($result)) {
      while ($row = $DB->fetch_row($result)) {
        $row['ext'] = "";
        $row_info = $this->render_row_referent($row, $lang);
        /*assign the array to a template variable*/
        $this->skin->assign('row', $row_info);
        $this->skin->parse("list_referent.row_referent");
      }
    } else {
      $this->skin->assign('mess', $vnT->lang['no_have_news_referent']);
      $this->skin->parse("list_referent.row_referent_no");
    }
    $data['html_row'] = $html_row;
    $data['totals'] = $totals;
    $data['nav'] = $nav;
    $data['err'] = $err;
    $data['link_del'] = $this->linkUrl . "&sub=remove_news&id=$id";
    $data['link_action'] = $this->linkUrl . "&sub=view&id=$id";
    $data['f_title'] = $vnT->lang['list_referent'] . "&nbsp; <b class=font_err>" . $info['title'] . "</b>";
    /*assign the array to a template variable*/
    $this->skin->assign('data', $data);
    $this->skin->parse("list_referent");
    return $this->skin->text("list_referent");
  }

  /**
   * function render_row_news 
   *  
   **/
  function render_row_news ($row_info, $lang)
  {
    global $func, $DB, $conf, $vnT;
    $row = $row_info;
    // Xu ly tung ROW
    $id = $row['newsid'];
    $row_id = "row_" . $id;
    $output['check_box'] = "<input type=\"checkbox\" name=\"key_id[]\" value=\"{$id}\" class=\"checkbox\" onclick=\"select_row('{$row_id}')\">";
    $link_edit = "?mod=news&act=news&sub=edit&id={$id}&lang=$lang";
    if (empty($row['picture']))
      $picture = "<i>No Image</i>";
    else {
      $picture = $this->get_picture($row['picture'], 50);
    }
    $output['picture'] = $picture;
    $output['title'] = "<a href=\"{$link_edit}\">" . $func->HTML($row['title']) . "</a>";
    $output['cat_name'] = get_cat_name($row['cat_id'], $lang);
    return $output;
  }

  /**
   * function list_news 
   *  
   **/
  function list_news ($info, $lang)
  {
    global $func, $DB, $conf, $vnT;
    $id = $info['keyref_id'];
    if ((isset($vnT->input['p'])) && (is_numeric($vnT->input['p'])))
      $p = $vnT->input['p'];
    else
      $p = 1;
    if (isset($vnT->input['cat_id']))
      $cat_id = intval($vnT->input['cat_id']);
    if (isset($vnT->input['cat_id']))
      $cat_id = intval($vnT->input['cat_id']);
    $search = "title";
    if (isset($vnT->input['search']))
      $search = $vnT->input['search'];
    if (isset($vnT->input['search']))
      $search = $vnT->input['search'];
    if (isset($vnT->input['keyword']))
      $keyword = $vnT->input['keyword'];
    if (isset($vnT->input['keyword']))
      $keyword = $vnT->input['keyword'];
    $where = " and keyref_id=0 ";
    $ext = "";
    if (! empty($cat_id)) {
      $subcat = List_SubCat($cat_id);
      if (! empty($subcat)) {
        $subcat = substr($subcat, 0, - 1);
        $a_cat_id = $cat_id . "," . $subcat;
        $a_cat_id = str_replace(",", "','", $a_cat_id);
        $where .= " and cat_id in ('" . $a_cat_id . "') ";
      } else {
        $where .= " and cat_id = $cat_id ";
      }
      $ext_page .= "cat_id=$cat_id|";
      $ext = "&cat_id=$cat_id";
    }
    if (! empty($keyword)) {
      if ($search == "newsid") {
        $where .= " and n.newsid = $keyword ";
      } elseif ($search == "date_post") {
        $where .= " and DATE_FORMAT(FROM_UNIXTIME(date_post),'%d/%m/%Y') = '{$keyword}' ";
      } else {
        $where .= " and $search like '%$keyword%' ";
      }
      $ext_page .= "keyword=$keyword|";
      $ext .= "&search={$search}&keyword={$keyword}";
    }
    $query = $DB->query("SELECT n.newsid
						FROM news n,  news_desc nd
						WHERE n.newsid=nd.newsid
						AND display=1
						AND lang='$lang' 
						$where ");
    $totals = $DB->num_rows($query);
    $n = 20;
    $num_pages = ceil($totals / $n);
    if ($p > $num_pages)
      $p = $num_pages;
    if ($p < 1)
      $p = 1;
    $start = ($p - 1) * $n;
    $nav = $func->paginate($totals, $n, $ext, $p);
    $table['link_action'] = $this->linkUrl . "&sub=set_news&id=$id&lang={$lang}";
    $table['title'] = array(
      'check_box' => "<input type=\"checkbox\" name=\"checkall\" id=\"checkall\" class=\"checkbox\" />|5%|center" , 
      'picture' => $vnT->lang['picture'] . "|10%|center" , 
      'title' => $vnT->lang['title'] . "|40%|left" , 
      'cat_name' => $vnT->lang['category'] . "|30%|center");
    $sql = "SELECT *
						FROM news n,  news_desc nd
						WHERE n.newsid=nd.newsid
						AND display=1
						AND lang='$lang' 
						$where
						ORDER BY date_post DESC LIMIT $start,$n";
    //print "sql = ".$sql."<br>";
    $result = $DB->query($sql);
    if ($DB->num_rows($result)) {
      $row = $DB->get_array($result);
      for ($i = 0; $i < count($row); $i ++) {
        $row_info = $this->render_row_news($row[$i], $lang);
        $row_field[$i] = $row_info;
        $row_field[$i]['stt'] = ($i + 1);
        $row_field[$i]['row_id'] = "row_" . $row[$i]['newsid'];
        $row_field[$i]['ext'] = "";
      }
      $table['row'] = $row_field;
    } else {
      $table['row'] = array();
      $table['extra'] = "<div align=center class=font_err >" . $vnT->lang['no_have_news'] . "</div>";
    }
    $table['button'] = "&nbsp;&nbsp;<input type=\"button\" name=\"btnEdit\" value=\"" . $vnT->lang['do_set_referent'] . "\" class=\"button\" onclick=\"javascript:do_submit('do_edit')\">";
    $data['table_list'] = $func->ShowTable($table);
    $data['listcat'] = Get_Cat($cat_id, $lang);
    $data['list_search'] = List_Search($search);
    $data['keyword'] = $keyword;
    $data['link_fsearch'] = $this->linkUrl . "&sub=view&id=$id";
    $data['link_action'] = $this->linkUrl . "&sub=view&id=$id";
    $data['nav'] = $nav;
    $data['f_title'] = 'DANH SÁCH CÁC TIN TỨC';
    /*assign the array to a template variable*/
    $this->skin->assign('data', $data);
    $this->skin->parse("list_news");
    return $this->skin->text("list_news");
  }

  /**
   * function do_SetNews 
   *  
   **/
  function do_SetNews ($lang)
  {
    global $func, $DB, $conf, $vnT;
    $keyid = (int) $vnT->input['id'];
    if (isset($vnT->input["key_id"])) {
      $ids = implode(',', $vnT->input["key_id"]);
    }
    $query = 'UPDATE news SET keyref_id=' . $keyid . ' WHERE newsid IN (' . $ids . ')';
    if ($ok = $DB->query($query)) {
      //xoa cache
      $func->clear_cache();
      //insert adminlog
      $func->insertlog("Set News", $_GET['act'], $ids);
      $mess = $vnT->lang['set_referent_success'];
    } else
      $mess = $vnT->lang["del_failt"];
    $ext_page = str_replace("|", "&", $ext);
    $url = $url = $this->linkUrl . "&sub=view&id=$keyid&{$ext_page}";
    $func->html_redirect($url, $mess);
  }

  /**
   * function do_Remove 
   *  
   **/
  function do_Remove ($lang)
  {
    global $func, $DB, $conf, $vnT;
    $keyid = (int) $vnT->input['id'];
    $ext = $vnT->input["ext"];
    $del = 0;
    $qr = "";
    if (isset($vnT->input["del_key"])) {
      $ids = implode(',', $vnT->input["del_key"]);
    }
    $query = 'UPDATE news SET keyref_id=0 WHERE newsid IN (' . $ids . ')';
    if ($ok = $DB->query($query)) {
      $mess = $vnT->lang["del_success"];
    } else
      $mess = $vnT->lang["del_failt"];
    $ext_page = str_replace("|", "&", $ext);
    $url = $url = $this->linkUrl . "&sub=view&id=$keyid&{$ext_page}";
    $func->html_redirect($url, $mess);
  }
  // end class
}
$vntModule = new vntModule();
?>
