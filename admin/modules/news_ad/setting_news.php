<?php
/*================================================================================*\
|| 							Name code : cat_service.php								 		 		            	  ||
||  				Copyright @2009 by Thai Son - CMS vnTRUST               				      ||
\*================================================================================*/
/**
 * @version : 2.0
 * @date upgrade : 09/01/2009 by Thai Son
 **/
if (! defined('IN_vnT')) {
  die('Hacking attempt!');
}



//load Model
include(dirname(__FILE__) . "/functions.php");

class vntModule extends Model
{
  var $output = "";
  var $skin = "";
  var $linkUrl = "";
  var $module = "news";
  var $action = "setting_news";

  /**
   * function vntModule ()
   * Khoi tao 
   **/
  function vntModule ()
  {
    global $Template, $vnT, $func, $DB, $conf;
    
    $this->skin = new XiTemplate(DIR_MODULE . DS . $this->module . "_ad" . DS . "html" . DS . $this->action . ".tpl");
    $this->skin->assign('LANG', $vnT->lang);
    $lang = ($vnT->input['lang']) ? $lang = $vnT->input['lang'] : $func->get_lang_default();
    $this->linkUrl = "?mod=" . $this->module . "&act=" . $this->action . "&lang=" . $lang;
    switch ($vnT->input['sub']) {
			case 'rebuild':
	   		 $nd['content']=$this->do_Rebuild($lang);
      break;
      case 'edit':
        $nd['content'] = $this->do_Edit($lang);
      break;
      default:
        $nd['f_title'] = $vnT->lang['manage_setting_news'];
        $nd['content'] = $this->do_Manage($lang);
      break;
    }
    $nd['menu'] = $func->getToolbar_Small($this->module, $this->action, $lang);
		$nd['row_lang'] = $func->html_lang("?mod=" . $this->module . "&act=" . $this->action, $lang);
    $Template->assign("data", $nd);
    $Template->parse("box_main");
    $vnT->output .= $Template->text("box_main");
  }
	
	/**
   * function do_Rebuild  
   **/
  function do_Rebuild ($lang)
  {
    global $vnT, $func, $DB, $conf;
 
		
		//news_category_desc
		$result = $DB->query("SELECT * FROM news_category_desc "); 
		while($row = $DB->fetch_row($result))
		{
			$arr_seo[$stt]['table'] = "news_category_desc";
			$arr_seo[$stt]['table_id'] = "cat_id";
			
			$arr_seo[$stt]['modules'] = "news";
			$arr_seo[$stt]['action'] = "category";
			$arr_seo[$stt]['name_id'] = "catID";
			
			$arr_seo[$stt]['item_id'] = $row['cat_id'];
			$arr_seo[$stt]['lang'] = $row['lang'];
			$arr_seo[$stt]['friendly_url'] = $row['friendly_url'];	
			$arr_seo[$stt]['query_string'] = "mod:news|act:category|catID:".$row['cat_id'];	 
			$stt++;			
		} 
		
		//news_desc
		$result = $DB->query("SELECT * FROM news_desc "); 
		while($row = $DB->fetch_row($result))
		{
			
			$arr_seo[$stt]['table'] = "news_desc";
			$arr_seo[$stt]['table_id'] = "newsid";
			
			$arr_seo[$stt]['modules'] = "news";
			$arr_seo[$stt]['action'] = "detail";
			$arr_seo[$stt]['name_id'] = "itemID";
			
			$arr_seo[$stt]['item_id'] = $row['newsid'];
			$arr_seo[$stt]['lang'] = $row['lang'];
			$arr_seo[$stt]['friendly_url'] = $row['friendly_url'];	
			$arr_seo[$stt]['query_string'] = "mod:news|act:detail|itemID:".$row['newsid'];	 			
			$stt++;				 
						
		} 
		
		foreach ($arr_seo as $seo)
		{
			$friendly_url = $seo['friendly_url'] ;			 
			
			//check 			
			$res = $DB->query("SELECT id,name FROM seo_url WHERE modules='".$seo['modules']."' AND action='".$seo['action']."' AND name_id='".$seo['name_id']."' AND item_id=".$seo['item_id']." AND lang='".$seo['lang']."'");
			if($row = $DB->fetch_row($res))
			{// update
				
				if($friendly_url != $row['name'])
				{ 
					$res_ck = $DB->query("SELECT id FROM seo_url WHERE name='".$seo['friendly_url']."' AND lang='".$seo['lang']."'  AND id<>".$row['id'] )	 ;			
					if($row_ck = $DB->fetch_row($res_ck))
					{  
						$friendly_url = $seo['friendly_url']."-".time(); 			
						$DB->query("UPDATE ".$seo['table']." SET friendly_url='".$friendly_url."' WHERE  lang='".$seo['lang']."' AND ".$seo['table_id']."=".$seo['item_id']." ")	;			
					}
						 
					$cot['name'] = $friendly_url; 
					$cot['date_post'] = time();				
					$DB->do_update("seo_url", $cot,"modules='".$seo['modules']."' AND action='".$seo['action']."' AND name_id='".$seo['name_id']."' AND item_id=".$seo['item_id']." AND lang='".$seo['lang']."'");	
				}
			}else{//insert
				
				$res_ck = $DB->query("SELECT * FROM seo_url WHERE name='".$seo['friendly_url']."' AND lang='".$seo['lang']."' " )	 ;			
				if($row_ck = $DB->fetch_row($res_ck))
				{  
					$friendly_url = $seo['friendly_url']."-".time(); 			
					$DB->query("UPDATE ".$seo['table']." SET friendly_url='".$friendly_url."' WHERE  lang='".$seo['lang']."' AND ".$seo['table_id']."=".$seo['item_id']." ")	;			
				} 
					
				$cot['modules'] = $seo['modules'];
				$cot['action'] = $seo['action'];
				$cot['name_id'] = $seo['name_id'];
				$cot['item_id'] = $seo['item_id'];		
				$cot['lang'] = $seo['lang'];
				$cot['name'] = $friendly_url;
				$cot['query_string'] = $seo['query_string'];
				$cot['date_post'] = time();
				
				$DB->do_insert("seo_url", $cot);	
			}
		}
		//xoa cache
		$func->clear_cache();
		//insert adminlog
		$func->insertlog("Rebuild", $_GET['act'], $id);
		$err = "Rebuild Link Success";
		$url = $this->linkUrl;
		$func->html_redirect($url, $err);
		 
  }
	
	
  /**
   * function do_Edit 
   * Cap nhat gioi thieu 
   **/
  function do_Edit ($lang)
  {
    global $vnT, $func, $DB, $conf;
    if ($vnT->input['do_submit']) {
      $data = $_POST;
      $res_check = $DB->query("select * from news_setting where id=1 ");
      if($row = $DB->fetch_row($res_check))
			{
				foreach ($row as $key => $value) 
				{
					if ($key != 'id' && isset($vnT->input[$key])) {
						$dup[$key] = $vnT->input[$key];
					}
				}
				$res_lang = $DB->query("SELECT id FROM news_setting WHERE lang='$lang'");
				if($DB->num_rows($res_lang))
				{
					$ok = $DB->do_update("news_setting", $dup, "lang='$lang'");	
				}else{
					$dup['lang'] = $lang ;
					$ok = $DB->do_insert("news_setting", $dup );	
				}				
			}
			
      //xoa cache
      $func->clear_cache();
      //insert adminlog
      $func->insertlog("Setting", $_GET['act'], $id);
      $err = $vnT->lang["edit_setting_news_success"];
      $url = $this->linkUrl;
      $func->html_redirect($url, $err);
    }
  }
	
  /**
   * function do_Manage() 
   * Quan ly 
   **/
  function do_Manage ($lang)
  {
    global $vnT, $func, $DB, $conf;
    $result = $DB->query("select * from news_setting where lang='$lang' ");
    $data = $DB->fetch_row($result);
    $data['comment'] =vnT_HTML::list_yesno("comment", $data['comment']);
    $data['comment_display'] = vnT_HTML::selectbox("comment_display", array(
														     	0 => 'Có', 
														      1 => 'Không'), $data['comment_display']);
    $data['feedback'] = vnT_HTML::list_yesno("feedback", $data['feedback']);
    $data['print'] = vnT_HTML::list_yesno("print", $data['print']);
    $data['sendmail'] = vnT_HTML::list_yesno("sendmail", $data['sendmail']);
    $data['get_image'] = vnT_HTML::list_yesno("get_image", $data['get_image']);
    $data['toolsearch'] = vnT_HTML::list_yesno("toolsearch", $data['toolsearch']);
    $data['nophoto'] = vnT_HTML::list_yesno("nophoto", $data['nophoto']);
		
		if ($data['pic_nophoto']) { 	
			$data['image_nophoto'] = '<img src="'.MOD_DIR_UPLOAD.$data['pic_nophoto'].'" width="100" />' . "  <a href=\"javascript:del_picture('pic_nophoto')\" class=\"del\">Xóa</a>";
			$data['style_upload'] = "style='display:none' ";
		} else {
			$data['image_nophoto'] = "";
		}
    $data['list_rss'] = vnT_HTML::list_yesno("rss", $data['rss']);
    $data['thum_size'] = vnT_HTML::list_yesno("thum_size", $data['thum_size']);
    $data['imgthumb_align'] = vnT_HTML::selectbox("imgthumb_align", array(
      'left' => $vnT->lang['align_left'] , 
      'right' => $vnT->lang['align_right']), $data['imgthumb_align']);
    $data['img_align'] = vnT_HTML::selectbox("img_align", array(
      'left' => $vnT->lang['align_left'] , 
      'right' => $vnT->lang['align_right']), $data['img_align']);
  	$data['folder_upload'] = vnT_HTML::selectbox("folder_upload", array(
																											0 => $vnT->lang['folder_upload0'],
																											1 => $vnT->lang['folder_upload1'],
																											2 => $vnT->lang['folder_upload2'],
																											3 => $vnT->lang['folder_upload3'],
																											4 => $vnT->lang['folder_upload4']
																										), $data['folder_upload'])  ;
    $data['link_upload'] = '?mod=media&act=popup_media&module='.$this->module.'&folder='.$this->module.'&obj=pic_nophoto&type=image&TB_iframe=true&width=900&height=474';
		$data['link_action'] = $this->linkUrl . "&sub=edit";
		$data['link_rebuild'] = $this->linkUrl."&sub=rebuild";	
		
    /*assign the array to a template variable*/
    $this->skin->assign('data', $data);
    $this->skin->parse("manage");
    return $this->skin->text("manage");
  }
  // end class
}

$vntModule = new vntModule();
?>