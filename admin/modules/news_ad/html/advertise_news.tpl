<!-- BEGIN: edit -->
<script language="javascript" >
$(document).ready(function() {
	$('#myForm').validate({
		rules: {					
			cat_id: {
					required: true
				},	
				title: {
					required: true,
					minlength: 3
				},
				link: {
					required: true
				}
	    },
	    messages: {	    	
				cat_id: {
						required: "{LANG.err_select_required}" 
				},
				title: {
						required: "{LANG.err_text_required}",
						minlength: "{LANG.err_length} 3 {LANG.char}" 
				},
				link: {
						required: "{LANG.err_text_required}"
				} 
		}
	});
});
</script>
{data.err}
 <form action="{data.link_action}" method="post" enctype="multipart/form-data" name="myForm" id="myForm" class="validate">
<table width="100%"  border="0" cellspacing="2" cellpadding="2" align=center class="admintable">
	
	<tr class="form-required" >
		<td  align="right" class="row1">{LANG.category}: </td>
		<td  align="left" class="row0">{data.list_cat}</td>
	</tr>
	<tr class="form-required">
		<td width="20%" align="right" class="row1">Title : </td>
		<td width="80%" align="left" class="row0"><input name="title" type="text" id="title" size="50" maxlength="250" value="{data.title}"></td>
	</tr>
	<tr class="form-required">
		<td  align="right" class="row1"> Link : </td>
		<td align="left" class="row0"><input name="link" id="link" type="text"  size="70" maxlength="250" value="{data.link}" /></td>
	</tr>
	<tr>
		<td align="right" class="row1"> Image :  </td>
		<td align="left" class="row0">{data.pic}
    <div class="ext_upload">
<input name="chk_upload" type="radio" value="0" checked > 
Images URL &nbsp; <input name="picture" type="text" size="50" maxlength="250" onchange="do_ChoseUpload('ext_upload',0);"> <br>
<input name="chk_upload" type="radio" value="1" >Upload Picture &nbsp;&nbsp;&nbsp;
<input name="image" type="file" id="image" size="30" maxlength="250" onchange="do_ChoseUpload('ext_upload',1);">
</div>
<br /></td>
	</tr>
	<tr>
		  	<td align="right" class="row1">{LANG.width}:&nbsp;</td>
			<td align="left" class="row0"><input name="width" type="text" id="width" size="3" maxlength="3" value="{data.width}">&nbsp;pixcel</td>
		  </tr>	 	
		 <tr>
		  	<td align="right" class="row1">{LANG.height}:&nbsp;</td>
			<td align="left" class="row0"><input name="height" type="text" id="height" size="3" maxlength="3" value="{data.height}">&nbsp;pixcel</td>
		  </tr>	
	
	<tr align="center">
  	<td  align="right" class="row1">&nbsp;  </td>
    
		<td class="row0">
			<input type="hidden" name="do_submit" value="1">
			<input type="submit" name="btnAdd" value="Submit" class="button">
			<input type="reset" name="Submit2" value="Reset" class="button">            
		</td>
	</tr>
</table>
</form>
	<br />
<!-- END: edit -->
<!-- BEGIN: manage -->
<!--[if lt IE 7]>
<link rel="stylesheet" type="text/css" href="{DIR_JS}/highslide/highslide-ie6.css" />
<![endif]-->
<br>
<form action="{data.link_fsearch}" method="post" name="myform">
<table width="100%" border="0" cellspacing="2" cellpadding="2" align="center" class="tableborder">

  <tr>
    <td align="left" width="15%">{LANG.category}: </td>
    <td align="left">{data.list_cat}</td>
  </tr>
  <tr>
  </table>
</form>
{data.err}
<br />
{data.table_list}
<br />
<!-- END: manage -->

