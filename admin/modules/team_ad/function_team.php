<?php
/*================================================================================*\
|| 							Name code : funtion_team.php	 		 			          	     		  # ||
||  				Copyright © 2007 by Thai Son - CMS vnTRUST                          # ||
\*================================================================================*/
/**
* @version : 1.0
* @date upgrade : 11/12/2007 by Thai Son
**/

if (!defined('IN_vnT')){
     die('Hacking attempt!');
}
define('ROOT_UPLOAD', 'vnt_upload/team/');
define('MOD_DIR_UPLOAD','../vnt_upload/team/');
define('MOD_PATH_UPLOAD', $conf['rootpath'].'vnt_upload/team');
define('MOD_ROOT_URL',$conf['rooturl'].'modules/team/');

 
/*-------------- loadSetting --------------------*/
function loadSetting ($lang="vn"){
	global $vnT,$func,$DB,$conf;
	$setting = array();
	$result = $DB->query("select * from team_setting WHERE lang='$lang' ");
	// $setting = $DB->fetch_row($result);
	// foreach ($setting as $k => $v)	{
	// 	$vnT->setting[$k] = stripslashes($v);
	// }
	// unset($setting);
}

//------get_dir_upload
function get_dir_upload() {
global $vnT,$func,$DB,$conf;
	
	$dir="";
	//check parentid
	$res_ck = $vnT->DB->query("SELECT folder_id FROM media_folders WHERE folder_name='team'");
	if($row_ck = $DB->fetch_row($res_ck))
	{
		$parentid  = (int)$row_ck['folder_id'];
	}else{
		$cot['parentid']	=0;
		$cot['folder_path']	= "team";
		$cot['folder_name']	=	"team";
		$cot['date_create']	=	time();
		$cot['folder_type'] = "module";
		$vnT->DB->do_insert("media_folders",$cot);
		$parentid  = $vnT->DB->insertid();
	}
	
	// tao thu muc  
	switch ($vnT->setting['folder_upload'])
	{
		case 1 : 
		$dir = date("m_Y");
		$res_dir = $func->vnt_mkdir(MOD_PATH_UPLOAD , $dir,1);
		if($res_dir[0]==1)
		{
			$cot['parentid'] = $parentid;
			$cot['folder_path'] = "team/".$dir;
			$cot['folder_name'] = $dir;
			$cot['date_create'] = time();
			$DB->do_insert("media_folders",$cot);	
		}
		
		
		break ;
		case 2 : 
			$dir = date("d_m_Y");
			$res_dir = $func->vnt_mkdir(MOD_PATH_UPLOAD , $dir,1);
			if($res_dir[0]==1)
			{	
				$cot['parentid'] = $parentid;
				$cot['folder_path'] = "team/".$dir;
				$cot['folder_name'] = $dir;
				$cot['date_create'] = time();
				$DB->do_insert("media_folders",$cot);
			}
		break ;
		case 3 : 	
			$dir_thang = date("m_Y");
			$res_dir = $func->vnt_mkdir(MOD_PATH_UPLOAD , $dir_thang);
			if($res_dir[0]==1)
			{	
				$cot['parentid'] = $parentid;
				$cot['folder_path'] = "team/".$dir_thang;
				$cot['folder_name'] = $dir_thang;
				$cot['date_create'] = time();
				$DB->do_insert("media_folders",$cot);
				$parent_thang = $DB->insertid();	
				
				$dir_day = date("d") ;
				$res_dir1 = $func->vnt_mkdir(MOD_PATH_UPLOAD."/".$dir_thang , $dir_day,1);
				if($res_dir1[0]==1)
				{
					$cot['parentid'] = $parent_thang;
					$cot['folder_path'] = "team/".$dir_thang."/".$dir_day;
					$cot['folder_name'] = $dir_day;
					$cot['date_create'] = time();
					$DB->do_insert("media_folders",$cot);	
				}
			}
				
			$dir = $dir_thang."/".$dir_day;				
		 
		break ;
	} // end switch
	
	return $dir;
}


//------create_maso
function create_maso($cat_id,$p_id) {
global $vnT,$func,$DB,$conf;
	$text="";
	$sql ="select name from team_category where cat_id='{$cat_id}'  ";
	$result =$DB->query ($sql);
	if ($row = $DB->fetch_row($result)){
		$text =  ($row['name']) ? $row['name']."-".$p_id : $p_id ;
	}else{
		$text = $p_id  ;
	}
 	
	return $text;
}


//-----------------  get_cat_name
function get_cat_name($cat_id,$lang)
{
	global $vnT,$func,$DB,$conf;
	$text="";
	$sql ="select cat_name from team_category_desc where cat_id =$cat_id and lang='{$lang}' ";
	$result =$DB->query ($sql);
	if ($row = $DB->fetch_row($result)){
		$text = $func->HTML($row['cat_name']);
	}
	return $text;
}

/***** Ham get_cat_list *****/
function get_cat_list ($cat_id){
	global $vnT,$func,$DB,$conf;
	$arr_cat = array();
	$res = $DB->query("SELECT cat_id,cat_code FROM team_category where cat_id in (".$cat_id.") ");
	while ($row=$DB->fetch_row($res)) 
	{
		$tmp = @explode("_",$row['cat_code']);
		$arr_cat = array_merge($arr_cat,$tmp) ;
	}
	$arr_cat = array_unique($arr_cat);
	$out = @implode(",",$arr_cat);
	
	return $out;	
}

/***** Ham get_cat_list *****/
function get_p_list ($cat_id){
	global $vnT,$func,$DB,$conf;
	$arr_cat = array();
	$res = $DB->query("SELECT p_id,p_list FROM  teams_player where p_id in (".$cat_id.") ");
	while ($row=$DB->fetch_row($res)) 
	{
		$tmp = @explode("_",$row['cat_code']);
		$arr_cat = array_merge($arr_cat,$tmp) ;
	}
	$arr_cat = array_unique($arr_cat);
	$out = @implode(",",$arr_cat);
	
	return $out;	
}

/***** Ham rebuild_cat_list *****/
function rebuild_cat_list ($cat_id){
	global $vnT,$func,$DB,$conf;
	$arr_cat = array();
	$res_pro = $DB->query("SELECT p_id,cat_id FROM teams WHERE  FIND_IN_SET('$cat_id',cat_list)<>0 ");
	while($row_pro = $DB->fetch_row($res_pro))
	{
		$DB->query("UPDATE teams SET cat_list='".get_cat_list($row_pro['cat_id'])."' WHERE p_id=".$row_pro['p_id'])	;
	}	
	return false;	
}

/***** Ham rebuild_cat_code *****/
function rebuild_cat_code ($parentid,$cat_id){
	global $vnT,$func,$DB,$conf;
	
	$cat_code = get_catCode($parentid,$cat_id) ;
	$DB->query("UPDATE team_category SET cat_code='".$cat_code."' WHERE cat_id=".$cat_id)	;
 	
	$res_chid = $DB->query("SELECT cat_id FROM team_category WHERE  parentid=".$cat_id);
	while($row_chid = $DB->fetch_row($res_chid))
	{		
		rebuild_cat_code($cat_id,$row_chid['cat_id']);
	} 
	
	return false;	
}


/***** Ham get_parentid *****/
function get_parentid ($cat_id){
global $func,$DB,$conf;
	$cid = list_parentid ($cat_id);
	$cid = substr ($cid,strrpos($cid,",")+1);
	return $cid;	
}

function list_parentid ($cat_id){
global $func,$DB,$conf;
	$res = $DB->query("SELECT parentid,cat_id FROM team_category where cat_id in (".$cat_id.") and parentid>0  ");
	while ($cat=$DB->fetch_row($res)) {
		$output .=",".$cat["parentid"];
		$output .=list_parentid($cat['parentid']);
	}
	
	return $output;	
}

//-----------------  get_catCode
function get_catCode($parentid,$cat_id) {
	global $vnT,$func,$DB,$conf;
	$text="";
	$sql ="select cat_id,cat_code from team_category where cat_id =$parentid ";
	$result =$DB->query ($sql);
	if ($row = $DB->fetch_row($result)){
		$text = $row['cat_code']."_".$cat_id;
	}else $text = $cat_id;
	
	return $text;
}


//-----------------  get_cat_name
function get_p_name($p_id,$lang)
{
	global $vnT,$func,$DB,$conf;
	$text="";
	$sql ="select p_name from teams where p_id =$p_id  ";
	$result =$DB->query ($sql);
	if ($row = $DB->fetch_row($result)){
		$text = $func->HTML($row['p_name']);
	}
	
	return $text;
}

//-----------------  get_pic_team
function get_pic_team ($picture,$w=150){
	global $vnT,$func,$DB,$conf;
	$out=""; $ext="";
	$w_thumb = ($vnT->setting['img_width_grid']) ? $vnT->setting['img_width_grid'] : 100 ;
		
	if($picture) 
	{
		$linkhinh = "../vnt_upload/team/".$picture;
 		$linkhinh = str_replace("//","/",$linkhinh);
		$dir = substr($linkhinh,0,strrpos($linkhinh,"/"));
		$pic_name = substr($linkhinh,strrpos($linkhinh,"/")+1) ;
		$src = $dir."/thumbs/".$pic_name;		 
	}
  
	if($w<$w_thumb) $ext = " width='$w' ";
	
	$out = "<img  src=\"{$src}\" {$ext} >";
	
	return $out ;
}

//-----------------  get_pic_team
function get_pic_player ($picture,$w=150){
	global $vnT,$func,$DB,$conf;
	$out=""; $ext="";
	$w_thumb = ($vnT->setting['img_width_grid']) ? $vnT->setting['img_width_grid'] : 100 ;
		
	if($picture) 
	{
		$linkhinh = "../vnt_upload/team/".$picture;
 		$linkhinh = str_replace("//","/",$linkhinh);
		$dir = substr($linkhinh,0,strrpos($linkhinh,"/"));
		$pic_name = substr($linkhinh,strrpos($linkhinh,"/")+1) ;
		$src = $dir."/thumbs/".$pic_name;		 
	}
  
	if($w<$w_thumb) $ext = " width='$w' ";
	
	$out = "<img  src=\"{$src}\" {$ext} >";
	
	return $out ;
}
  
 
//====== get_price_pro
function get_price_pro ($price,$default="Call"){
	global $func,$DB,$conf,$vnT;
	if ($price){
		$price = $func->format_number($price)." VNĐ" ;		
	}else{
		$price = $default;
	}
	return $price;
}

//=============================List_Cat
function List_Cat($did="",$lang="vn"){
	global $func,$DB,$conf,$vnT;
	if ($did)
		$arr_selected = explode(",",$did);
	else{
		$arr_selected = array(
		  '0',
		);
	}
	
	$text= "<select name=\"list_cat[]\" id=\"list_cat\"  multiple >";
	$sql="SELECT n.*,nd.cat_name FROM team_category n, team_category_desc nd
				where n.cat_id=nd.cat_id
				AND nd.lang='$lang' 
				AND n.parentid=0 
				order by cat_order ";
	$result = $DB->query ($sql);
	while ($row = $DB->fetch_row($result)){
		$cat_name = $func->HTML($row['cat_name']);
		$selected="";
		if (in_array($row['cat_id'],$arr_selected)){
			$selected = "selected";
		}
		
		$text .= "<option value=\"{$row['cat_id']}\" {$selected} >".$cat_name."</option>";
		$n=1;
		$text.=List_Sub($row['cat_id'],$n,$did,$lang);
	}
	
	$text.="</select>";
	return $text;
}


//=============================List_Cat
function List_Cat_Team($did="",$lang="vn"){
	global $func,$DB,$conf,$vnT;
	if ($did)
		$arr_selected = explode(",",$did);
	else{
		$arr_selected = array(
		  '0',
		);
	}
	
	$text= "<select name=\"list_cat[]\" id=\"list_cat\"  multiple >";
	$sql="SELECT n.*,nd.p_name FROM  teams n, teams_desc nd
				where n.p_id=nd.p_id
				AND nd.lang='$lang' 				
				";
				//echo $sql;die;
	$result = $DB->query ($sql);
	while ($row = $DB->fetch_row($result)){
		$cat_name = $func->HTML($row['p_name']);
		$selected="";
		if (in_array($row['p_id'],$arr_selected)){
			$selected = "selected";
		}
		
		$text .= "<option value=\"{$row['p_id']}\" {$selected} >".$cat_name."</option>";
		$n=1;
		
	}
	
	$text.="</select>";
	return $text;
}

//---------------Get_Sub
function List_Sub($cid,$n,$did="",$lang){
global $func,$DB,$conf,$vnT;
	if ($did)
		$arr_selected = explode(",",$did);
	else{
		$arr_selected = array(
		  '0',
		);
	}
	$output="";
	$k=$n;
	$query = $DB->query("SELECT n.*,nd.cat_name FROM team_category n, team_category_desc nd
				where n.cat_id=nd.cat_id
				AND nd.lang='$lang'
				AND parentid={$cid} 
				order by cat_order");
	while ($cat=$DB->fetch_row($query)) 
	{
		$cat_name = $func->HTML($cat['cat_name']);
		$selected=""; 
		if (in_array($cat['cat_id'],$arr_selected)){
			$selected = "selected";
		}
		
		$output.="<option value=\"{$cat['cat_id']}\" {$selected} >";
		for ($i=0;$i<$k;$i++) $output.= "|-- ";
		$output.="{$cat_name}</option>";
		
		$n=$k+1;
		$output.=List_Sub($cat['cat_id'],$n,$did,$lang);
	}
	return $output;
}

//----------------- List_Cat_Chose
function List_Cat_Chose($did="",$lang){
global $func,$DB,$conf,$vnT;
	if ($did)
		$arr_selected = explode(",",$did);
	else{
		$arr_selected = array(
		  '0',
		);
	}
	$text= "<select name=\"cat_chose[]\" id=\"cat_chose\"  multiple >";
	$sql="SELECT n.*,nd.cat_name FROM team_category n, team_category_desc nd
				where n.cat_id=nd.cat_id
				AND nd.lang='$lang' 
				AND n.parentid=0 
				order by cat_order ";
	$result = $DB->query ($sql);
	while ($row = $DB->fetch_row($result)){
		$cat_name = $func->HTML($row['cat_name']);
		if (in_array($row['cat_id'],$arr_selected)){
			$text .= "<option value=\"{$row['cat_id']}\">".$cat_name."</option>";
		}
		$n=1;
		$text.=Get_Sub_Chose($row['cat_id'],$n,$did,$lang);
	}
	
	$text.="</select>";
	return $text;
}
//---------------Get_Sub_Chose
function Get_Sub_Chose($cid,$n,$did="",$lang){
global $func,$DB,$conf,$vnT;
	if ($did)
		$arr_selected = explode(",",$did);
	else{
		$arr_selected = array(
		  '0',
		);
	}
	$output="";
	$k=$n;
	$query = $DB->query("SELECT n.*,nd.cat_name FROM team_category n, team_category_desc nd
				where n.cat_id=nd.cat_id
				AND nd.lang='$lang'
				AND parentid={$cid} 
				order by cat_order");
	while ($cat=$DB->fetch_row($query)) {
		$cat_name = $func->HTML($cat['cat_name']);
		if (in_array($cat['cat_id'],$arr_selected)){
			$output.="<option value=\"{$cat['cat_id']}\" >";
			for ($i=0;$i<$k;$i++) $output.= "|-- ";
			$output.="{$cat_name}</option>";
		}
		$n=$k+1;
		$output.=Get_Sub_Chose($cat['cat_id'],$n,$did,$lang);
	}
	return $output;
}

/***** Ham List_SubCat *****/
function List_SubCat ($cat_id){
global $func,$DB,$conf,$vnT;
	$output="";
	$query = $DB->query("SELECT * FROM team_category WHERE parentid={$cat_id}");
	while ($cat=$DB->fetch_row($query)) {
		$output.=$cat["cat_id"].",";
		$output.=List_SubCat($cat['cat_id']);
	}
	return $output;
}
 
 
/*** Ham Get_Cat ****/
function Get_Cat($did=-1,$lang,$ext=""){
global $func,$DB,$conf;
	
	$text= "<select size=1 id=\"cat_id\" name=\"cat_id\" {$ext} >";
	$text.="<option value=\"0\">-- Root --</option>";
	$query = $DB->query("SELECT n.*,nd.cat_name FROM team_category n, team_category_desc nd
				where n.cat_id=nd.cat_id
				AND nd.lang='$lang' 
				AND n.parentid=0 
				ORDER BY n.cat_order DESC, n.cat_id DESC ");

	while ($cat=$DB->fetch_row($query)) {
		$cat_name = $func->HTML($cat['cat_name']);
		if ($cat['cat_id']==$did)
			$text.="<option value=\"{$cat['cat_id']}\" selected style='font-weight:bold;'>{$cat_name}</option>";
		else
			$text.="<option value=\"{$cat['cat_id']}\" style='font-weight:bold;' >{$cat_name}</option>";
		$n=1;
		$text.=Get_Sub($did,$cat['cat_id'],$n,$lang);
	}
	$text.="</select>";
	return $text;
}

/*** Ham Get_Cat ****/
function Get_Cat_Player($did=-1,$lang,$ext=""){
global $func,$DB,$conf;
	
	$text= "<select size=1 id=\"cat_id\" name=\"cat_id\" {$ext} >";
	$text.="<option value=\"0\">-- Root --</option>";
	$query = $DB->query("SELECT n.*,nd.p_name FROM teams n, teams_desc nd
				where n.p_id=nd.p_id
				AND nd.lang='$lang' 
				
				ORDER BY n.p_order DESC, n.p_id DESC ");

	while ($cat=$DB->fetch_row($query)) {
		$p_name = $func->HTML($cat['p_name']);
		 // echo "<pre/>";print_r($cat);die;

		if ($cat['p_id']==$did)
			$text.="<option value=\"{$cat['p_id']}\" selected style='font-weight:bold;'>{$p_name}</option>";
		else
			$text.="<option value=\"{$cat['p_id']}\" style='font-weight:bold;' >{$p_name}</option>";
		$n=1;
		
	}
	$text.="</select>";
	return $text;
}
/*** Ham Get_Sub   */
function Get_Sub($did,$cid,$n,$lang){
global $func,$DB,$conf;

	$output="";
	$k=$n;
	$query = $DB->query("SELECT n.*,nd.cat_name FROM team_category n, team_category_desc nd
				where n.cat_id=nd.cat_id
				AND nd.lang='$lang' 
				AND n.parentid=$cid
				ORDER BY n.cat_order DESC, n.cat_id DESC");
	while ($cat=$DB->fetch_row($query)) {
		$cat_name = $func->HTML($cat['cat_name']);
		
		if ($cat['cat_id']==$did)	{
			$output.="<option value=\"{$cat['cat_id']}\" selected>";
			for ($i=0;$i<$k;$i++) $output.= "|-- ";
			$output.="{$cat_name}</option>";
		}else{	
			$output.="<option value=\"{$cat['cat_id']}\" >";
			for ($i=0;$i<$k;$i++) $output.= "|-- ";
			$output.="{$cat_name}</option>";
		}
		$n=$k+1;
		$output.=Get_Sub($did,$cat['cat_id'],$n,$lang);
	}
	return $output;
}




//=============================List_Cat
function List_Cat_Root($did="",$ext){
global $func,$DB,$conf;
	
	$text= "<select name=\"cat_id\" id=\"cat_id\" {$ext}   >";
	$text.="<option value=\"\" selected> Chọn danh mục  </option>";
	$sql="SELECT n.*,nd.cat_name FROM team_category n, team_category_desc nd
				where n.cat_id=nd.cat_id
				AND nd.lang='$lang' 
				AND n.parentid=0 
				ORDER BY n.cat_order DESC, n.cat_id DESC ";
	$result = $DB->query ($sql);
	while ($row = $DB->fetch_row($result)){
		$cat_name = $func->HTML($row['cat_name'],$lang);
		if ($row['cat_id']==$did){
			$text .= "<option value=\"{$row['cat_id']}\" selected>".$cat_name."</option>";
		} else{
			$text .= "<option value=\"{$row['cat_id']}\">".$cat_name."</option>";
		}
	}
	
	$text.="</select>";
	return $text;
}

/*** Ham Get_Category ****/
function Get_Category ($selname, $did = -1, $lang, $ext = "")
{
  global $func, $DB, $conf;
  
  $text = "<select size=1 id=\"{$selname}\" name=\"{$selname}\" {$ext} class='select'>";
  
  $text .= "<option value=\"0\">-- Root --</option>";
  $query = $DB->query("SELECT n.*,nd.cat_name FROM team_category n, team_category_desc nd
				where n.cat_id=nd.cat_id
				AND nd.lang='$lang' 
				AND n.parentid=0 
				order by cat_order DESC , n.cat_id DESC");
  while ($cat = $DB->fetch_row($query))
  {
    $cat_name = $func->HTML($cat['cat_name']);
    if ($cat['cat_id'] == $did)
      $text .= "<option value=\"{$cat['cat_id']}\" selected>{$cat_name}</option>";
    else
      $text .= "<option value=\"{$cat['cat_id']}\" >{$cat_name}</option>";
    $n = 1;
    $text .= Get_Sub_Category($selname, $did, $cat['cat_id'], $n, $lang);
  }
  $text .= "</select>";
  return $text;
}
/*** Ham Get_Sub   */
function Get_Sub_Category ($selname, $did, $cid, $n, $lang)
{
  global $func, $DB, $conf;
  
  $output = "";
  $k = $n;
  $query = $DB->query("SELECT n.*,nd.cat_name FROM team_category n, team_category_desc nd
				where n.cat_id=nd.cat_id
				AND nd.lang='$lang' 
				AND n.parentid=$cid 
				order by cat_order DESC , n.cat_id DESC");
  while ($cat = $DB->fetch_row($query))
  {
    $cat_name = $func->HTML($cat['cat_name']);
    if ($cat['cat_id'] == $did)
    {
      $output .= "<option value=\"{$cat['cat_id']}\" selected>";
      for ($i = 0; $i < $k; $i ++)
        $output .= "|-- ";
      $output .= "{$cat_name}</option>";
    }
    else
    {
      $output .= "<option value=\"{$cat['cat_id']}\" >";
      for ($i = 0; $i < $k; $i ++)
        $output .= "|-- ";
      $output .= "{$cat_name}</option>";
    }
    $n = $k + 1;
    $output .= Get_Sub_Category($selname, $did, $cat['cat_id'], $n, $lang);
  }
  return $output;
} 

//get_datetime
function  get_datetime ($gio,$ngay){
	$out="";
	$gio = explode(":",$gio);
	$ngay = explode("/",$ngay);
	$out  = mktime($gio[0],$gio[1], 0, $ngay[1] , $ngay[0], $ngay[2]);
	return $out;
}


// List_Status_Pro 
function List_Status_Pro ($selname,$did,$lang="vn",$ext=""){
	global $func,$DB,$conf;
	$text= "<select name=\"{$selname}\"  {$ext}   >";
	$text.="<option value=\"0\" selected>-- Chọn trạng thái --</option>";
	$sql="SELECT n.*, nd.title FROM team_status n, team_status_desc nd 
				where n.status_id=nd.status_id
				AND nd.lang='$lang'
				AND n.display=1 
				order by n.s_order ";
	$result = $DB->query ($sql);
	while ($row = $DB->fetch_row($result)){
		if ($row['status_id']==$did){
			$text .= "<option value=\"{$row['status_id']}\" selected>".$func->HTML($row['title'])."</option>";
		} else{
			$text .= "<option value=\"{$row['status_id']}\">".$func->HTML($row['title'])."</option>";
		}
	}
	$text.="</select>";
	return $text;
}

/*------ List_Status_Muti ---------*/
function List_Status_Muti($did="",$lang="vn",$ext=""){
global $func,$DB,$conf;
	if ($did)
		$arr_selected = explode(",",$did);
	else{
		$arr_selected = array();
	}

	$sql="SELECT n.*, nd.title FROM team_status n, team_status_desc nd 
				where n.status_id=nd.status_id
				AND nd.lang='$lang'
				AND n.display=1 
				order by n.s_order ASC  ";
	$result = $DB->query ($sql);
	$text='<table width="100%" border="0" cellspacing="2" cellpadding="2" align="center"><tr>';
	$i=0;$ngang=0;
	while ($row = $DB->fetch_row($result))
	{
		if (in_array($row['status_id'],$arr_selected)){
			$checked ="checked";	
		}else{
			$checked ="";	
		}
		if($row['picture'])
			$icon = "<img src=\"../modules/team/uploads/status/{$row['picture']}\" align='absmiddle' />&nbsp;";
		$name = $func->HTML($row['title']);
		if($i%4==0){
			$text.='</tr><tr><td width="25%" >'.$icon.'&nbsp;<input name="status[]" id="status" type="checkbox" value="'.$row['status_id'].'" '.$checked.'  />&nbsp;'.$name.'';
			$ngang=0;
		}else{
			
			$text.='<td width="25%">'.$icon.'&nbsp;<input name="status[]" id="status" type="checkbox" value="'.$row['status_id'].'" '.$checked.' />&nbsp;'.$name.'';	
		}
		$i++;$ngang++;
			
	}
	
	if ($ngang<4) $text.='<td>&nbsp;</td>';
	$text.='</tr></table>';
	
	return $text;
}

//------get_status_name
function get_status_name ($sid,$lang)
{
	global $func,$vnT,$conf,$DB,$func;
	$text="";
	$result = $DB->query("SELECT title FROM team_status_desc  where status_id in ($sid) and lang='$lang' ");
	while($row = $DB->fetch_row($result))
	{
		$text .= $func->HTML($row['title'])."<br>";
	}
	
//	$text = substr($text,0,-2);
	return $text;
}

// List_Status_Promotion 
function List_Status_Promotion ( $did, $lang = "vn", $ext = "")
{
  global $func, $DB, $conf;
  $text = "<select name=\"status_id\" class='select'  {$ext}   >";
  $text .= "<option value=\"0\" selected>-- Chọn trạng thái --</option>";
  $sql = "SELECT n.*, nd.title FROM team_status n, team_status_desc nd 
				where n.status_id=nd.status_id
				AND nd.lang='$lang'
				AND n.display=1 
				AND  edit_price=1
				order by n.s_order ASC ";
  $result = $DB->query($sql);
  while ($row = $DB->fetch_row($result))
  {
    if ($row['status_id'] == $did)
    {
      $text .= "<option value=\"{$row['status_id']}\" selected>" . $func->HTML($row['title']) . "</option>";
    }
    else
    {
      $text .= "<option value=\"{$row['status_id']}\">" . $func->HTML($row['title']) . "</option>";
    }
  }
  $text .= "</select>";
  return $text;
}

//------------------------------ get_where_brand ---------------------
function get_where_brand ($brand_id)
{
  global $input, $conf, $vnT;
  $text = "";
  $resultS = $vnT->DB->query("select brand_id from team_brand where parentid=$brand_id  ");
  if ($vnT->DB->num_rows($resultS)) {
    while ($rowS = $vnT->DB->fetch_row($resultS)) {
      $subid .= $rowS["brand_id"] . ",";
    }
    $subid = substr($subid, 0, - 1);
    $subid = $brand_id . "," . $subid;
    $text .= " and brand_id in (" . $subid . ") ";
  } else {
    $text .= " and brand_id=$brand_id";
  }
  return $text;
}
// List_Brand 
function List_Brand ( $did, $lang = "vn", $ext = "")
{
  global $func, $DB, $conf ,$vnT;
  $text = "<select name=\"brand_id\"  id=\"brand_id\" class='select'  {$ext}   >";
  $text .= "<option value=\"0\" selected>--- Chọn nhà sản xuất --</option>";
  $sql = "SELECT n.*, nd.title FROM team_brand n, team_brand_desc nd 
				where n.brand_id=nd.brand_id
				AND nd.lang='$lang'
				AND n.parentid=0
				order by n.display_order ASC ";
				
  $result = $DB->query($sql);
  while ($row = $DB->fetch_row($result))
  {
		$selected = ($did==$row['brand_id']) ? " selected " : "";
    $text .= "<option value=\"{$row['brand_id']}\" {$selected} style='font-weight:bold;' >" . $func->HTML($row['title']) . "</option>";
		
		$n=1;
		$text.= Get_Sub_Brand($did,$row['brand_id'],$n,$lang);
		
   }
  $text .= "</select>";
	
  return $text;
}

/*** Ham Get_Sub_Brand   */
function Get_Sub_Brand($did,$cid,$n,$lang){
global $func,$DB,$conf;

	$output="";
	$k=$n;
	$query = $DB->query("SELECT n.*, nd.title FROM team_brand n, team_brand_desc nd 
				where n.brand_id=nd.brand_id
				AND nd.lang='$lang'
				AND n.parentid=$cid
				order by n.display_order ASC , brand_id ASC ");
	while ($row=$DB->fetch_row($query)) {
		$title = $func->HTML($row['title']);
		
		if ($row['brand_id']==$did)	{
			$output.="<option value=\"{$row['brand_id']}\" selected>";
			for ($i=0;$i<$k;$i++) $output.= "|-- ";
			$output.="{$title}</option>";
		}else{	
			$output.="<option value=\"{$row['brand_id']}\" >";
			for ($i=0;$i<$k;$i++) $output.= "|-- ";
			$output.="{$title}</option>";
		}
		$n=$k+1;
		$output.=Get_Sub_Brand($did,$row['brand_id'],$n,$lang);
	}
	return $output;
}


//=============================List_Brand_Root
function List_Brand_Root($did="",$lang="vn",$ext=""){
global $func,$DB,$conf;
	
	$text = "<select name=\"brand_id\"  id=\"brand_id\" class='select'  {$ext}   >";
  $text .= "<option value=\"0\" selected>--- ROOT --</option>";
  $sql = "SELECT n.*, nd.title FROM team_brand n, team_brand_desc nd 
				where n.brand_id=nd.brand_id
				AND nd.lang='$lang'
				AND n.parentid=0
				order by  n.display_order  ASC , n.brand_id ASC ";
  $result = $DB->query($sql);
  while ($row = $DB->fetch_row($result))
  {
		$selected = ($did==$row['brand_id']) ? " selected " : "";
    $text .= "<option value=\"{$row['brand_id']}\" }{$selected} >" . $func->HTML($row['title']) . "</option>"; 
  }
  $text .= "</select>";
	return $text;
}


//------get_brand_name
function get_brand_name ($id,$lang="vn")
{
	global $func,$vnT,$conf,$DB,$func;
	$text="";
	$result = $DB->query("SELECT title FROM team_brand_desc  where brand_id=($id) AND lang='$lang' ");
	if($row = $DB->fetch_row($result))
	{
		$text .= $func->HTML($row['title']) ;
	}
	
//	$text = substr($text,0,-2);
	return $text;
}

//------do_DeleteSub
function do_DeleteSub($ids,$lang="vn")
{
	global $func, $DB, $conf, $vnT;
	$res = $DB->query("SELECT cat_id,parentid FROM team_category WHERE parentid in (".$ids.")");
	while($row = $DB->fetch_row($res))
	{
		do_DeleteSub($row['cat_id'],$lang);
		$DB->query("UPDATE team_category_desc SET display=-1display=-1 WHERE  cat_id = ".$row['cat_id']." AND lang='".$lang."' ");

		//insert RecycleBin
		$rb_log['module'] = "team";
		$rb_log['action'] = "category";
		$rb_log['tbl_data'] = "team_category_desc";
		$rb_log['name_id'] = "cat_id";
		$rb_log['item_id'] = $row['cat_id'];
		$rb_log['lang'] = $lang;
		$func->insertRecycleBin($rb_log);
	}
	
}

/*KEY SEARCH*/
/*--------------------------- get_name_cat ---------------------*/
function get_key_catname ($cat_id,$lang="vn"){
global $vnT,$func,$DB,$conf;
	$cat_name="";

	$res = $DB->query("SELECT cat_id,cat_code FROM team_category WHERE cat_id=$cat_id");
	if ($cat=$DB->fetch_row($res)) {
		$cat_code = $cat['cat_code'];
	}
	
	$tmp = explode("_",$cat_code);
	for ($i=0;$i<count($tmp);$i++)
	{
		$res= $DB->query("select cat_name from team_category where cat_id=".$tmp[$i]." and lang='{$lang}' ");
		if ($r = $DB->fetch_row($res))
		{	
			$cat_name.=$func->HTML($r['cat_name']).' , ' ;
		}
	}
	
	$cat_name = substr($cat_name,0,-2);
	
	return $cat_name;
}

//------get_key_search
function get_key_search ($cot_key)
{
		global $func,$vnT,$conf,$DB,$func;
		$text="";
		$text .= $cot_key['maso']." | ";
		$text .= $cot_key['p_name']." | ";
		$text .= $cot_key['price']." | ";				
		$text .= $cot_key['cat_name']." | ";
		$text .= $cot_key['status'] ; 	
		return $text;				
		
}


//======================= List_Search =======================
function List_Search ($did,$ext="")
{
  global $func, $DB, $conf, $vnT;
	$arr_where = array('p_id'=>  $vnT->lang['id'] ,'maso'=>  $vnT->lang['maso'],'p_name'=> $vnT->lang['team_name'],'price'=> $vnT->lang['price'], 'date_post' => $vnT->lang['date_post']." (d/m/Y)",'description'=> $vnT->lang['description'] );
	
	$text= "<select size=1 name=\"search\" id='search' class='select' {$ext} >";
	foreach ($arr_where as $key => $value)
	{
		$selected = ($did==$key) ? "selected"	: "";
		$text.="<option value=\"{$key}\" {$selected} > {$value} </option>";
	}	
	$text.="</select>";	
	 
  return $text;
}
 
function List_Direction ($did)
{
	global $func,$DB,$conf,$vnT;
	$text= "<select size=1 name=\"direction\">";
	if ($did =="ASC")
		$text.="<option value=\"ASC\" selected>{$vnT->lang['asc']}</option>";
	else
		$text.="<option value=\"ASC\" >{$vnT->lang['asc']}</option>";
		
	if ($did =="DESC")	
		$text.="<option value=\"DESC\" selected>{$vnT->lang['desc']}</option>";
	else
		$text.="<option value=\"DESC\">{$vnT->lang['desc']}</option>";
		
	$text.="</select>";
	return $text;
}

//------list_admin
function list_admin ($did,$ext=""){
	global $func,$DB,$conf,$vnT;

	$text= "<select name=\"adminid\" id=\"adminid\"   class='select' {$ext} >";
	$text .= "<option value=\"\" selected>-- ".$vnT->lang['select_admin_post']." --</option>";
	$sql="SELECT adminid,username	FROM admin order by  username ASC ";
	
	$result = $DB->query ($sql);
	$i=0;
	while ($row = $DB->fetch_row($result))
	{
		$i++;
		$username = $func->HTML($row['username']);
		$selected = ($did==$row['adminid']) ? " selected " : "";
		$text .= "<option value=\"{$row['adminid']}\" ".$selected." >  ".$username."  </option>";
	}
	
	$text.="</select>";
	return $text;
}

//------get_admin
function get_admin($adminid) {
global $vnT,$func,$DB,$conf;
	$text = "Admin";
	$sql ="select username from admin where adminid =$adminid  ";
	$result =$DB->query ($sql);
	if ($row = $DB->fetch_row($result)){
		$text = $func->HTML($row['username']);
	}
	return $text;
}


/*------------ ORDER ---------*/
function  get_method_name ($table,$name,$lang="vn"){
	global	$DB,$input,$func,$vnT,$conf;
	$text="";
	$result = $DB->query ("select title from {$table} where name='{$name}' ") ;
	if ($row = $DB->fetch_row($result)){
		$text = $func->fetch_content ($row['title'],$lang);
	}
	return $text ;	
}

function list_status_order($did,$ext=""){
global $func,$DB,$conf,$vnT;
	
	$arr_status = array('0'=>  $vnT->lang['status_order0'] ,'1'=>  $vnT->lang['status_order1'],'2'=> $vnT->lang['status_order2'], '3' => $vnT->lang['status_order3'] );
	
	$text= "<select size=1 name=\"status\" id='status' class='select' {$ext} >";
	foreach ($arr_status as $key => $value)
	{
		$selected = ($did==$key) ? "selected"	: "";
		$text.="<option value=\"{$key}\" {$selected} > {$value} </option>";
	}	
	
	$text.="</select>";
	return $text;
}

//----- get_info_address
function  get_info_address ($data,$type=""){
	global	$DB,$input,$func,$vnT,$conf;
	
	if($type=="shipping")
	{
		$text = "Họ tên : <b>".$data['c_name']."</b>";
		$text .= "<br>Địa chỉ : ".$data['c_address'];
		if($data['c_city'])
			$text .= "<br>Tỉnh thành : ".get_city_name($data['c_city']);
 		if($data['c_phone'])
			$text .= "<br>Điện thoại : ".$data['c_phone'];
		if($data['c_mobile'])
			$text .= "<br>Mobile : ".$data['c_mobile'];
				
	}else{
		$type_mem = ($data['mem_id']==0) ? "Khách vãng lai" : "Thành viên" ;
		
		$text = "Họ tên : <b>".$data['d_name']."</b> <span class=font_err>(".$type_mem.")</span>";
		if($data['d_company'])
			$text .= "<br>Công ty : ".$data['d_company'];
			
		$text .= "<br>Địa chỉ : ".$data['d_address'];
		if($data['d_city'])
			$text .= ", ".get_city_name($data['d_city']);
 

		$text .= "<br>Điện thoại : ".$data['d_phone'];
		if($data['d_mobile'])
			$text .= "<br>Mobile : ".$data['d_mobile'];
		if($data['d_fax'])
			$text .= "<br>Fax : ".$data['d_fax'];
		if($data['d_email'])
			$text .= "<br>E-mail : ".$data['d_email'];
	}
	

	return $text ;	
}


//----------- get_city_name
function get_city_name($code)
{
	global $vnT,$func,$DB,$conf;
	$text=$code;
	$sql ="select name from iso_cities where code='$code' ";
	$result =$DB->query ($sql);
	if ($row = $DB->fetch_row($result)){
		$text = $func->HTML($row['name']);
	}
	return $text;
}

//----------- get_country_name
function get_country_name($code)
{
	global $vnT,$func,$DB,$conf;
	$text="";
	$sql ="select name from iso_countries where iso='$code' ";
	$result =$DB->query ($sql);
	if ($row = $DB->fetch_row($result)){
		$text = $func->HTML($row['name']);
	}

//	$text = $vnT->arr_order['country'][$code];
	return $text;
}

/*** Ham get_maso_next   */
function get_maso_next ()
{
  global $func, $DB, $conf;
  $output = "";
	
  $query = $DB->query("SELECT maso FROM teams ORDER BY maso DESC LIMIT 0,1");
  if ($row = $DB->fetch_row($query)) {
		
		$maso = $row['maso'];
		$maso++;
		$output = $maso;
  }
  return $output;
}

/*================================COMMENT=======================================*/
function List_Display ($did = 0, $default ="", $ext = "")
{
  global $func, $vnT, $conf;
  
  $text = "<select size=1 name=\"display\" id=\"display\" {$ext}'>";
	if($default)
		 $text .= "<option value=\"-1\" selected> " . $default. " </option>";
		 
  if ($did == "0")
    $text .= "<option value=\"0\" selected> " . $vnT->lang['display_no'] . " </option>";
  else
    $text .= "<option value=\"0\" > " . $vnT->lang['display_no'] . " </option>";
  
  if ($did == "1")
    $text .= "<option value=\"1\" selected> " . $vnT->lang['display_yes'] . " </option>";
  else
    $text .= "<option value=\"1\"> " . $vnT->lang['display_yes'] . " </option>";
  
  $text .= "</select>";
  return $text;
}


function List_Mark($did){
global $func,$DB,$conf;
	$text= "<select size=1 name=\"mark\">";
	for ($i=1;$i<=5;$i++){
		if ($did ==$i)
			$text.="<option value=\"$i\" selected> $i </option>";
		else
			$text.="<option value=\"$i\" > $i </option>";
	}

	$text.="</select>";
	return $text;
}


/*================================END COMMENT=======================================*/


?>