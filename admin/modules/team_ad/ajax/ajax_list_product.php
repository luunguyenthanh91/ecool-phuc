<?php
	define('IN_vnT',1);
	
	require_once("../../../../_config.php"); 
	include($conf['rootpath']."includes/class_db.php"); 
	$DB = new DB;	
	//Functions
	include ($conf['rootpath'] . 'includes/class_functions.php');
	include($conf['rootpath'] . 'includes/admin.class.php');
	$func  = new Func_Admin;
	$conf = $func->fetchDbConfig($conf);

	$cat_id = $_GET['cat_id'];
	$p_id = (int) $_GET['p_id'];
	$lang	= ($_GET['lang']) ? $_GET['lang'] : "en" ;
	/***** Ham List_SubCatPro *****/
	function List_SubCatPro ($cat_id){
	global $func,$DB,$conf;
		$output="";
		$query = $DB->query("SELECT * FROM product_category WHERE parentid={$cat_id}");
		while ($cat=$DB->fetch_row($query)) {
			$output.=$cat["cat_id"].",";
			$output.=List_SubCatPro($cat['cat_id']);
		}
		return $output;
	}
	
	$where = "  ";
	if(!empty($cat_id)){
		$a_cat_id = List_SubCatPro($cat_id);
		$a_cat_id  = substr($a_cat_id,0,-1);
		if (empty($a_cat_id))
			$where .=" and FIND_IN_SET('$cat_id',cat_id)<>0 ";
		else{
			$tmp = explode(",",$a_cat_id);
			$str_= " FIND_IN_SET('$cat_id',cat_id)<>0 ";	
			for ($i=0;$i<count($tmp);$i++){
				$str_ .=" or FIND_IN_SET('$tmp[$i]',cat_id)<>0 ";
			}
			$where .=" and (".$str_.") ";
		} 
	}else  $where =" and cat_id=0 "; 

	$jsout= "<select  id=\"product\" name=\"product\" size=1 class='select' >";
	$jsout.="<option value=\"0\" selected> Chọn sản phẩm  </option>";
	$query = $DB->query("SELECT *
					FROM products p, products_desc pd
					WHERE p.p_id=pd.p_id 
					AND pd.lang='$lang'
					$where 
					ORDER by p.p_id DESC ");
	while ($row=$DB->fetch_row($query)) 
	{
		if ($row['p_id']==$p_id)
			$jsout.="<option value=\"{$row['p_id']}\" selected>".$func->HTML($row['p_name'])."</option>";
		else
			$jsout.="<option value=\"{$row['p_id']}\" >".$func->HTML($row['p_name'])."</option>";
	}
	$jsout.="</select>";
	
	
flush();
echo $jsout;
exit();
?>
