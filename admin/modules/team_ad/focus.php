<?php
/*================================================================================*\
|| 						           Name code : focus_product.php 		 		            	  			||
||  				     Copyright @2008 by Thai Son - CMS vnTRUST                  			||
\*================================================================================*/
/**
 * @version : 2.0
 * @date upgrade : 09/01/2009 by Thai Son
 **/
if (! defined('IN_vnT'))
{
  die('Hacking attempt!');
}

$vntModule = new vntModule();
class vntModule
{
  var $output = "";
  var $skin = "";
  var $linkUrl = "";
  var $module = "product";
	var $action = "focus";
	
  /**
   * function vntModule ()
   * Khoi tao 
   **/
  function vntModule ()
  {
    global $Template, $vnT, $func, $DB, $conf;
    require_once ("function_".$this->module.".php");
    $this->skin = new XiTemplate(DIR_MODULE . DS . $this->module . "_ad" . DS . "html" . DS . $this->action . ".tpl");
    $this->skin->assign('LANG', $vnT->lang);
    
    $lang = ($vnT->input['lang']) ? $lang = $vnT->input['lang'] : $func->get_lang_default();
		
		$cat_id = ((int) $vnT->input['cat_id']) ? $cat_id = $vnT->input['cat_id'] : 0;
		
    $this->linkUrl = "?mod=".$this->module."&act=".$this->action."&cat_id=$cat_id&lang=" . $lang;
		
    switch ($vnT->input['sub'])
    {
      case 'edit' :	$this->do_Edit($lang); break;				
			case 'del' : $this->do_Del($lang); break;
			default :{
				$nd['f_title']= $vnT->lang['manage_focus_product'];
				$nd['content']=$this->do_Manage($lang);
			}; break;
    }
    $nd['menu'] = $func->getToolbar_Small($this->module, $this->action."&cat_id=$cat_id", $lang);
    $nd['row_lang'] = $func->html_lang("?mod=".$this->module."&act=".$this->action."&cat_id=$cat_id", $lang);		
    $Template->assign("data", $nd);
    $Template->parse("box_main");
    $vnT->output .= $Template->text("box_main");
  
  }
 
  /**
   * function do_Edit 
   * Cap nhat 
   **/
  function do_Edit ($lang)
  {
   	global $vnT, $func, $DB, $conf;
    $id = (int) $vnT->input['id'];    
	 	$cat_id = (int) $vnT->input['cat_id'];
			
	
		if (isset($vnT->input["del_id"])){
			$ids = implode(',', $vnT->input["del_id"]);
		} 
		
		// list 

		$sql = 'UPDATE products SET focus=1 WHERE p_id IN ('.$ids.')' ; 
		
		$url = $this->linkUrl;
		$ok = $DB->query ($sql);
		if ($ok)	
		{
			//xoa cache
			$func->clear_cache();
			
			//insert adminlog
			$func->insertlog("Set focus",$_GET['act'],$ids);
				
			$mess = $vnT->lang['set_focus_success'];
					
		 }else{
				$mess = $vnT->lang['edit_failt'];
		}
		
		$func->html_redirect($url,$mess);
	}

  
  /**
   * function do_Del 
   * Xoa 1 ... n  gioi thieu 
   **/
  function do_Del ($lang)
  {
    global $func, $DB, $conf, $vnT;
		
		$cat_id = (int) $vnT->input['cat_id'];
		
		if (isset($vnT->input["del_focus"])){
			$ids = implode(',', $vnT->input["del_focus"]);
		} 		
		
		$sql = 'UPDATE products SET focus=0 WHERE p_id IN ('.$ids.')' ; 
		$url = $this->linkUrl;

		if ($ok = $DB->query($sql))
		{			
			$mess = $vnT->lang["del_success"] ;
		} else
			$mess = $vnT->lang["del_failt"] ;
		$func->html_redirect($url, $mess);		
		
	}
  
	
	/**
   * function do_Manage() 
   * Quan ly cac gioi thieu
   **/
  function do_Manage ($lang)
	{
    global $vnT, $func, $DB, $conf;    
		
		$data['table_list_focus']  = $this->list_focus($lang);
		$data['table_list']  = $this->list_product($lang);		
		
    /*assign the array to a template variable*/
    $this->skin->assign('data', $data);    
    $this->skin->parse("manage");
    return $this->skin->text("manage");
  }
	
	
  /**
   * function render_row 
   * list cac record
   **/
  function render_row ($row_info, $lang)
  {
    global $func, $DB, $conf, $vnT;
    $row = $row_info;
		
		 // Xu ly tung ROW
    $id = $row['p_id'];
    $row_id = "row_" . $id;
    $output['row_id'] = $row_id;
		$output['check_box'] = vnT_HTML::checkbox("del_id[]", $id, 0, " ");
    $link_edit = $this->linkUrl . '&sub=edit&id=' . $id . '&ext=' . $row['ext_page'];
		$link_pro = "?mod=product&act=product&sub=edit&id={$id}&lang=$lang";
		
			
		if ($row['picture']){
			$output['picture']= get_pic_product($row['picture'],50);
		}else $output['picture'] ="No image";
		
		
		$output['p_name'] = "Mã số : <b class=font_err>".$row['maso']."</b><br><a href=\"{$link_pro}\">".$func->HTML($row['p_name'])."</a>";
			
		$output['cat_name'] = get_cat_name($row['cat_id'],$lang);
		
		if ($row['display']==1){
			$display ="<img src=\"{$vnT->dir_images}/dispay.gif\" width=15  />" ;
		}else{
			$display ="<img src=\"{$vnT->dir_images}/nodispay.gif\"  width=15 />" ;
		}
		
    $output['action'] = '<input name=h_id[]" type="hidden" value="' . $id . '" />';    
    $output['action'] .= '<a href="' . $link_edit . '"><img src="' . $vnT->dir_images . '/edit.gif"  alt="Edit "></a>&nbsp;';
		$output['action'] .= $display.'&nbsp;';

		return $output;
  }
  
  
	
	/**
   * function list_product 
   *  
   **/
	function list_product ($lang)
	{
		global $func,$DB,$conf,$vnT;
	  $p = ((int) $vnT->input['p']) ? $p = $vnT->input['p'] : 1;
		$n = ($conf['record']) ? $conf['record'] : 30;
		
		$cat_id = ((int) $vnT->input['cat_id']) ? $cat_id = $vnT->input['cat_id'] : 0;
		$search = ($vnT->input['search']) ? $search = $vnT->input['search'] : "p_id";
		$keyword = ($vnT->input['keyword']) ? $keyword = $vnT->input['keyword'] : "";
		$direction = ($vnT->input['direction']) ? $direction = $vnT->input['direction'] : "DESC";
		
	
	  $where ="";
	
		if(!empty($cat_id)){
			$a_cat_id = List_SubCat($cat_id);
			$a_cat_id  = substr($a_cat_id,0,-1);
			if (empty($a_cat_id))
				$where .=" and FIND_IN_SET('$cat_id',cat_id)<>0 ";
			else{
				$tmp = explode(",",$a_cat_id);
				$str_= " FIND_IN_SET('$cat_id',cat_id)<>0 ";	
				for ($i=0;$i<count($tmp);$i++){
					$str_ .=" or FIND_IN_SET('$tmp[$i]',cat_id)<>0 ";
				}
				$where .=" and (".$str_.") ";
			} 
			$ext_page .="cat_id=$cat_id|";	
			$ext.="&cat_id=$cat_id";
		}
		
		if(!empty($search)){
			$ext_page.="search=$search|";
			$ext.="&search={$search}";
		}
		
		if(!empty($keyword)){
			switch($search){
				case "p_id" : $where .=" and p.p_id = $keyword "; $search="p.p_id"; break;
				case "date_post" : $where .=" and DATE_FORMAT(FROM_UNIXTIME(date_post),'%d/%m/%Y') = '{$keyword}' "; break;
				default :$where .=" and $search like '%$keyword%' ";break;		
			}
			
			$ext_page.="keyword=$keyword|";
			$ext.="&keyword={$keyword}";
		}
		$search = ($search=="p_id") ? $search="p.p_id" : $search ; 
		$order_by = "order by $search $direction ";
		$ext_page=$ext_page."direction=$direction|p=$p";
		$ext.="&direction=$direction";
		
		
		$query = $DB->query("SELECT n.*, nd.p_name, nd.lang 
							FROM products n, products_desc nd 
							WHERE n.p_id=nd.p_id 
							AND nd.lang='$lang'
							AND n.focus=0
							$where ");
		$totals = $DB->num_rows($query);
		$n=20;
		$num_pages = ceil($totals/$n) ;
		if ($p > $num_pages) $p=$num_pages;
		if ($p < 1 ) $p=1;
		$start = ($p-1) * $n ; 
		
		$nav = $func->paginate($totals, $n, $ext, $p) ;
		
		$table['link_action'] = $this->linkUrl."&sub=edit";
		$table['title'] = array (
				 'check_box' => "<input type=\"checkbox\" name=\"checkall\" id=\"checkall\" class=\"checkbox\" />|5%|center" , 
				 'picture' => $vnT->lang['picture']."|10%|center",
				 'p_name' => $vnT->lang['product_name']."|40%|left",
				 'cat_name' => $vnT->lang['category']."|30%|center",	 
				 );
			 
			$sql = "SELECT n.*, nd.p_name, nd.lang 
							FROM products n, products_desc nd 
							WHERE n.p_id=nd.p_id 
							AND nd.lang='$lang'
							AND n.focus=0				
							$where ORDER BY date_post DESC, n.p_id DESC LIMIT $start,$n";
			//print "sql = ".$sql."<br>";
			$result = $DB->query($sql);
				if ($DB->num_rows ($result)){
				 $row = $DB->get_array($result);
				 for ($i=0;$i<count($row);$i++) {
					$row_info = $this->render_row($row[$i],$lang);
					$row_field[$i]=$row_info ;
					$row_field[$i]['stt'] = ($i+1);
					$row_field[$i]['row_id'] = "row_".$row[$i]['p_id'];
					$row_field[$i]['ext'] ="";
				}
				$table['row'] = $row_field;
		
				}else{
				$table['row']=array();
				$table['extra']="<div align=center class=font_err >".$vnT->lang['no_have_product']."</div>";
				}
		
		$table['button'] = "&nbsp;&nbsp;<input type=\"button\" name=\"btnEdit\" value=\"".$vnT->lang['set_product_focus']."\" class=\"button\" onclick=\"javascript:do_submit('do_edit')\">";
			
		$data['table_list'] = $func->ShowTable($table);
		$data['listcat']=Get_Cat($cat_id,$lang,"onChange='submit()'");
		$data['list_search']=List_Search($search);
		$data['list_direction']=List_Direction($direction);
		$data['keyword']=$keyword;	
		
		$data['link_fsearch'] = "";
		$data['nav']=$nav;	
		
	
		$data['f_title'] = 'DANH SÁCH CÁC SẢN PHẨM';
		/*assign the array to a template variable*/
		$this->skin->assign('data', $data);				
		$this->skin->parse("list_product");		
		return $this->skin->text("list_product");
	
	}
	
	/**
   * function render_row_focus 
   *  
   **/
	function render_row_focus($row_info,$lang) 
	{
		global $func,$DB,$conf,$vnT;
		$row=$row_info;
	// Xu ly tung ROW
    $id = $row['p_id'];
    $row_id = "rowfocus_" . $id;
    $output['row_id'] = $row_id;
		$output['check_box'] = vnT_HTML::checkbox("del_id[]", $id, 0, " ");
    $link_edit = $this->linkUrl . '&sub=edit&id=' . $id . '&ext=' . $row['ext_page'];
		$link_pro = "?mod=product&act=product&sub=edit&id={$id}&lang=$lang";
			
		$output['check_box'] = "<input type=\"checkbox\" name=\"del_focus[]\" value=\"{$id}\" class=\"checkbox\" onclick=\"select_row('{$row_id}')\">";
		
		$output['order'] = "<input name=\"txtOrder[{$id}]\" type=\"text\" size=\"2\" maxlength=\"2\" style=\"text-align:center\" value=\"{$row['focus_order']}\"  onkeypress=\"return is_num(event,'txtOrder')\" onchange='javascript:do_check_focus($id)' />";
		
		
		if ($row['picture']){
			$output['picture']= get_pic_product($row['picture'],50);
		}else $output['picture'] ="No image";
		
		
		$output['p_name'] = "Mã số : <b class=font_err>".$row['maso']."</b><br><a href=\"{$link_pro}\">".$func->HTML($row['p_name'])."</a>";
			
		$output['cat_name'] = get_cat_name($row['cat_id'],$lang);

		
		return $output;
	}
	
	/**
   * function list_focus 
   *  
   **/
	function list_focus($lang) 
	{
		global $func,$DB,$conf,$vnT;		
	
		//update
		if (isset ($vnT->input["btnEdit"])){
			//xoa cache
			$func->clear_cache();
			if (isset($vnT->input["del_focus"])) 
			$h_id=$vnT->input["del_focus"] ;
			$mess.="- ".$vnT->lang['edit_success']." ID: <strong>";
			$str_mess="";
			if (isset($vnT->input["txtOrder"])) $arr_order = $vnT->input["txtOrder"] ; 
			for ($i=0;$i<count($h_id);$i++)	
			{
				$dup['focus_order'] = $arr_order[$h_id[$i]];
				$ok=$DB->do_update("products",$dup,"p_id={$h_id[$i]}");
				if ($ok){
					$str_mess.=$h_id[$i].", ";
				}
			}
			$mess.=substr($str_mess,0,-2)."</strong><br>";
			$err = $func->html_mess($mess);
			//insert adminlog
			$func->insertlog("Update Order",$_GET['act'],$str_mess);
	}
		
		
	if ((isset($_GET['p1'])) && (is_numeric($_GET['p1']))) $p1=$_GET['p1']; else $p1=1;
	$cat_id = ((int) $vnT->input['cat_id']) ? $cat_id = $vnT->input['cat_id'] : 0;
	
	$where = "";
	
	if($cat_id){
		$a_cat_id = List_SubCat($cat_id);
		$a_cat_id  = substr($a_cat_id,0,-1);
		if (empty($a_cat_id))
			$where .=" and FIND_IN_SET('$cat_id',cat_id)<>0 ";
		else{
			$tmp = explode(",",$a_cat_id);
			$str_= " FIND_IN_SET('$cat_id',cat_id)<>0 ";	
			for ($i=0;$i<count($tmp);$i++){
				$str_ .=" or FIND_IN_SET('$tmp[$i]',cat_id)<>0 ";
			}
			$where .=" and (".$str_.") ";
		} 
		$ext_page .="cat_id=$cat_id|";	
		$ext.="&cat_id=$cat_id";
		
		$f_title = $vnT->lang['list_focus_cat']." <b class='font_err'>".get_cat_name($cat_id,$lang)."</b>";
	}else{
		$f_title = $vnT->lang['list_all_focus'];
	}
	
	$query = $DB->query("SELECT n.*, nd.p_name, nd.lang 
						FROM products n, products_desc nd 
						WHERE n.p_id=nd.p_id 
						AND nd.lang='$lang'
						AND n.focus=1 
						$where ");
	$totals = $DB->num_rows($query);
	$n=9;
	$num_pages = ceil($totals/$n) ;
	if ($p1 > $num_pages) $p=$num_pages;
	if ($p1 < 1 ) $p1=1;
	$start = ($p1-1) * $n ; 
	
	$nav = "<div align=left>&nbsp;<span class=\"pagelink\">{$num_pages} Page(s) :</span>&nbsp;";
	for ($i=1; $i<$num_pages+1; $i++ ) {
		if ($i==$p1) $nav.="<span class=\"pagecur\">{$i}</span>&nbsp";
		else $nav.="<a href='".$this->linkUrl."&p1={$i}'><span class=\"pagelink\">{$i}</span></a>&nbsp ";
	} 
	$nav .= "</div>";
	
	$html_row = "";
	
	$sql .= " SELECT n.*, nd.p_name, nd.lang 
						FROM products n, products_desc nd 
						WHERE n.p_id=nd.p_id 
						AND nd.lang='$lang' 
						AND n.focus=1 
						$where 
						ORDER BY focus_order , n.p_id DESC
						limit $start,$n";
	//print "sql = ".$sql."<br>";
	$result = $DB->query($sql);	
	if ($DB->num_rows($result)){
		while ($row=$DB->fetch_row($result)) 
		{
			$row['ext'] = "";
			$row['cid'] = $cat_id;
			$row_info = $this->render_row_focus($row,$lang);
		 	/*assign the array to a template variable*/
			$this->skin->assign('row', $row_info);			
			$this->skin->parse("list_focus.row_focus");
		}
	}else{
		$this->skin->assign('mess', $vnT->lang['no_have_focus']);
		$this->skin->parse("list_focus.row_focus_no");
	}
	$data['html_row'] = $html_row ;	
	$data['totals'] = $totals;
	$data['nav'] = $nav;
	$data['err'] = $err;
	$data['link_del']=$this->linkUrl."&sub=del";
	$data['link_action'] = $this->linkUrl;
	$data['f_title'] = $f_title;
	//$text_out .= $this->skin->html_list_focus($data);
	/*assign the array to a template variable*/
	$this->skin->assign('data', $data);			
	$this->skin->parse("list_focus");
	$text_out .= $this->skin->text("list_focus");		
	return $text_out;
	}
	
  // end class
}

?>
