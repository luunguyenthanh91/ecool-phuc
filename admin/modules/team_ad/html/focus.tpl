<!-- BEGIN: list_product -->
<br />
<form action="{data.link_fsearch}" method="post" name="myform">
<table width="100%" border="0" cellspacing="2" cellpadding="2" align="center" class="tableborder">
    <tr>
    <td align="left" width="15%"><strong>{LANG.category}:</strong> &nbsp;</td>
    <td align="left">{data.listcat}&nbsp; </td>
  </tr>

  <tr>
    <td  colspan="2"><strong>{LANG.search} :</strong> {data.list_search} <strong> {LANG.keyword} :</strong>  <input name="keyword" value="{data.keyword}" size="20" type="text" /> <strong>{LANG.order}</strong> {data.list_direction}&nbsp;<input type="submit" name="btnGo" value=" Search " class="button"></td>
  </tr>
  
  </table>
</form>

<br />
<table width="100%" border="0" cellspacing="0" cellpadding="0" style="margin-bottom:5PX;">
  <tr>
  <td height="30" style="border-bottom: 2px solid rgb(184, 65, 32);" class="font_title">{data.f_title}</td>
  </tr>
</table>
{data.table_list}
<br />
<table width="100%"  border="0" align="center" cellspacing="1" cellpadding="1" class="bg_tab">
  <tr>
    <td  height="25">{data.nav}</td>
  </tr>
</table>
<!-- END: list_product -->


<!-- BEGIN: manage -->
 <br />
{data.table_list_focus}
<br />

{data.table_list}
<br />
<!-- END: manage -->

<!-- BEGIN: list_focus -->
<script language="javascript">
function check_all_focus()	{
	for ( i=0;i < document.f_focus.elements.length ; i++ ){
		if (document.f_focus.elements[i].type=="checkbox" && document.f_focus.elements[i].name!="all"){
			row_id = 'rowfocus_'+document.f_focus.elements[i].value;
		}else{
			row_id="";
		}
		
		if ( document.f_focus.all.checked==true ){
			document.f_focus.elements[i].checked = true;
			if (row_id!="" ){
				getobj(row_id).className = "row_select";
			}
		}
		else{
			document.f_focus.elements[i].checked  = false;
			if (row_id!="" ){
				if (i%2==0)
					getobj(row_id).className = "row1";
				else
					getobj(row_id).className = "row";
			}
		}
		
	}
}

function do_check_focus (id){
	
	for ( i=0;i < document.f_focus.elements.length ; i++ ){
		if (document.f_focus.elements[i].type=="checkbox" && document.f_focus.elements[i].name!="all" && document.f_focus.elements[i].value == id){
			document.f_focus.elements[i].checked=true;
			getobj("rowfocus_"+id).className = "row_select";
			
			break;
		}	
	}
	
}
function selected_item_focus(){
		var name_count = document.f_focus.length;

		for (i=0;i<name_count;i++){
			if (document.f_focus.elements[i].checked){
				return true;
			}
		}
		alert('Hãy chọn ít nhất 1 record');
		return false;
	}
	
function del_focus_selected(action) {
		if (selected_item_focus()){
			question = confirm('Bạn có chắc muốn XÓA không ?')
			if (question != "0"){
				document.f_focus.action=action;
				document.f_focus.submit();
			}else{
			  alert ('Phew~');
		    }
		}
	
}

</script>
<table width="100%" border="0" cellspacing="2" cellpadding="2" align="center" class="tableborder">
  <tr>
    <td align="left" width="20%">{LANG.totals} &nbsp; : </td>
    <td align="left" width="80%" class="font_err"><strong>{data.totals}</strong></td>
  </tr>
 </table>
 <br />
<table width="100%" border="0" cellspacing="0" cellpadding="0" style="margin-bottom:5PX;">
  <tr>
  <td height="30" style="border-bottom: 2px solid rgb(184, 65, 32);" class="font_title">{data.f_title}</td>
  </tr>
</table>
{data.err}
<form action="{data.link_action}" method="post" name="f_focus" id="f_focus">
<table width="100%"  border="0" cellspacing="0" cellpadding="0" class="bg_tbl">
	<tr>
		<td >
		<table width="100%" cellspacing="0" cellpadding="0" border="0" class="adminlist">
    <thead>
		  <tr>
			  <th width="5%" align="center"><input type="checkbox" name="all" onclick="javascript:check_all_focus();"></th>
				<th width="10%">{LANG.order}</th>
			  <th width="15%" align="center">{LANG.picture}</th>
			  <th width="45%" align="center">{LANG.product_name}</th>
			  <th width="30%" align="center">{LANG.category} </th>
		  </tr>
      </thead>
      <tbody>
		   <!-- BEGIN: row_focus -->
      <tr class="{row.class}" id="{row.row_id}"> 
        <td align="center">{row.check_box}&nbsp;</td>
        <td align="center">{row.order}&nbsp;</td>
        <td align="center">{row.picture}&nbsp;</td>
        <td align="left">{row.p_name}&nbsp;</td>
        <td align="center">{row.cat_name}&nbsp;</td>
      </tr>
      <!-- END: row_focus -->
      
      <!-- BEGIN: row_focus_no -->
         <tr class="row0" >
           <td  colspan="5" align="center" class="font_err" >{mess}</td>
         </tr>
         <!-- END: row_focus_no -->
         
      </tbody>
		</table>
		</td>
	</tr>
	<tr>
		<td  style="padding:5px;">
		<input type="submit" name="btnEdit" value="{LANG.edit_order}" class="button"  >
		<input type="button" name="btnDel" value="{LANG.del_focus}" class="button" onclick="javascript:del_focus_selected('{data.link_del}');" ></td>
	</tr>
</table>
</form>    
<br>
<table width="100%"  border="0" cellspacing="1" cellpadding="1" class="bg_tab" align=center>
      <tr  height=25>
        <td align="left" >{data.nav}</td>
      </tr>
</table>
<!-- END: list_focus -->




