<!-- BEGIN: edit -->
<link href="{DIR_JS}/metabox/seo-metabox.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="{DIR_JS}/metabox/seo-metabox.js"></script>
<script language="javascript" >
var wpseo_lang = 'en';
var wpseo_meta_desc_length = '155';
var wpseo_title = 'p_name';
var wpseo_content = 'description';
var wpseo_title_template = '%%title%%';
var wpseo_metadesc_template = '';
var wpseo_permalink_template = '{CONF.rooturl}%postname%.html';
var wpseo_keyword_suggest_nonce = 'a7c4d81c79'; 
$(document).ready(function() {
 
	
	jQuery.validator.addMethod("category", function( value, element ) {
		var result = multipleSelectOnSubmit();		 
		return result;
	}, "Vui long chon danh muc");


	$('#myForm').validate({

		rules: {			
				cat : "category" ,
				p_name: {
					required: true,
					minlength: 3
				}
				
	    },
	    messages: {
	    	
				p_name: {
						required: "{LANG.err_text_required}",
						minlength: "{LANG.err_length} 3 {LANG.char}" 
				} 
		}
		
	});
	
	$('#list_cat').multiSelect({selectableHeader: "<div class='custom-header'>{LANG.list_cat}</div>",  selectionHeader: "<div class='custom-header'>{LANG.list_cat_chose}</div>",});
	
	{data.js_preview} 
}); 
 
	
</script>  

{data.err}
<form action="{data.link_action}" method="post" enctype="multipart/form-data" name="myForm" id="myForm" class="validate">
<div class="boxAdminForm">
    <div class="block-left">

        <table width="100%"  border="0"  cellspacing="1" cellpadding="1" class="admintable">

            <tr>
                <td class="row1" >Danh mục đội bóng</td>
                <td class="row0" ><div style="min-width:600px;">{data.list_cat}</div></td>
            </tr>


            <tr class="form-required">
                <td class="row1" >Tên cầu thủ: </td>
                <td class="row0"><input name="p_name" id="p_name" type="text" size="60" maxlength="250" value="{data.p_name}"  onkeyup="vnTMXH.setTitle(this.value)" ></td>
            </tr>

            <tr class="form-required">
                <td class="row1" >Vị trí: </td>
                <td class="row0"><input name="vitri" id="vitri" type="text" size="60" maxlength="250" value="{data.vitri}"  onkeyup="vnTMXH.setTitle(this.value)" ></td>
            </tr>

            <tr class="form-required">
                <td class="row1" >Số áo: </td>
                <td class="row0"><input name="soao" id="soao" type="text" size="60" maxlength="250" value="{data.soao}"  onkeyup="vnTMXH.setTitle(this.value)" ></td>
            </tr>

            <tr class="form-required">
                <td class="row1" >Ngày sinh: </td>
                <td class="row0"><input name="namsinh" id="namsinh" type="text" size="60" maxlength="250" value="{data.namsinh}"  onkeyup="vnTMXH.setTitle(this.value)" ></td>
            </tr>

            <tr >
                <td class="row1" width="20%">{LANG.picture} cầu thủ: </td>
                <td class="row0">
                    <div id="ext_picture" class="picture" >{data.pic}</div>
                    <input type="hidden" name="picture"	 id="picture" value="{data.picture}" />
                    <div id="btnU_picture" class="div_upload" {data.style_upload} ><div class="button2"><div class="image"><a title="Add an Image" class="thickbox" id="add_image" href="{data.link_upload}" >Chọn hình</a></div></div></div>
                </td>
            </tr>

            
            <tr>
                        <td class="row1" width="20%">{LANG.display}</td>
                        <td class="row0">{data.list_display}</td>
                    </tr>
           
        </table>
    </div>
    
 </div>
<table width="100%" border="0" cellspacing="0" cellpadding="0">

  <tr >
    <td class="row0"   align="center" height="50" >
      <input type="hidden" name="do_submit"	 value="1" />
      <input type="submit" name="btnAdd" value="{LANG.btn_submit}" class="button">&nbsp;
      <input type="reset" name="btnReset" value="{LANG.btn_reset}" class="button">&nbsp;
    </td>
  </tr>
</table>  
</form>


<br>
<!-- END: edit -->

<!-- BEGIN: manage -->
<form action="{data.link_fsearch}" method="post" name="fSearch">
<table width="100%" border="0" cellspacing="2" cellpadding="2" align="center" class="tableborder">
  
  <tr>
	   <td ><strong>Danh mục đội bóng : </strong>  </td>
    <td > {data.listcat}  &nbsp;&nbsp; &nbsp;&nbsp;</td>
 
  </tr>
    
  
  <tr>
     <td ><strong>Tên cầu thủ  : </strong>  </td>
    <td > <input name="keyword" value="{data.keyword}" size="20" type="text" />  &nbsp;<input type="submit" name="btnGo" value=" Search " class="button"></td>
 
  </tr>


  <tr>
    <td width="15%" align="left"><strong>{LANG.totals}:</strong> &nbsp;</td>
    <td width="85%" align="left"><b class="font_err">{data.totals}</b></td>
  </tr>
</table>
</form>
{data.err}
<br />
{data.table_list}
<br />
<table width="100%"  border="0" align="center" cellspacing="1" cellpadding="1" class="bg_tab">
  <tr>
    <td  height="25">{data.nav}</td>
  </tr>
</table>
<br />
<!-- END: manage -->