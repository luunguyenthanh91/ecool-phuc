<?php
define('IN_vnT', 1);
define('PATH_ADMIN', dirname(__FILE__));
define('DS', DIRECTORY_SEPARATOR);
require_once ("../_config.php");
require_once ("../includes/admin.inc.php");

$Template = new XiTemplate(DIR_SKIN . DS . 'global.tpl');
$Template->assign("DIR_IMAGE", $vnT->dir_images);
$Template->assign("DIR_STYLE", $vnT->dir_style);
$Template->assign("DIR_JS", $vnT->dir_js);
$Template->assign("LANG", $vnT->lang);
$Template->assign("CONF", $vnT->conf);
$Template->assign("admininfo", $vnT->admininfo);
 
$Template->assign("marquee_hotline",$vnT->lang['marquee_help']);

$vnT->output = "";

$_GET['mod'] = str_replace("'", "&#039;", $_GET['mod']);
$_GET['block'] = str_replace("'", "&#039;", $_GET['block']);
if (! empty($_GET['block']))
{
  $folder = "blocks";
  $option = trim($_GET['block']);
} elseif (! empty($_GET['mod']))
{
  $folder = "modules";
  $option = trim($_GET['mod']);
} else
{
  $folder = "modules";
  $option = "main";
}
if(empty($_SESSION['admin_session']))
	$_SESSION['admin_session'] = $vnT->admininfo['adminid']  ;
	
$_GET['act'] = str_replace("\\'", "&#039;", $_GET['act']);
$act = (empty($_GET['act'])) ? "main" : $_GET['act'];
$sub = (empty($_GET['sub'])) ? "manage" : $_GET['sub']; 
  
// header
 
//left
$box_left = $vnT->menu->box_left();

//check  permission
/*$vnT->permission_act = array_unique(array_merge($vnT->permission_act, $vnT->act_allow));
if (! empty($_GET['act']) && ! in_array($_GET['act'], $vnT->permission_act))
{
	flush();
	$linkref = "?mod=main";
	@header("Location: " . $linkref);
	echo "<meta http-equiv='refresh' content='0; url=" . $linkref . "' />";
	echo $vnT->lang['not_permission'];
	exit();
	
}*/	

if (! empty($vnT->admininfo['permission']))
{
  if (strstr($vnT->myPre_sub[$act], $sub) || in_array($act, $vnT->act_allow))
  {
  
  } else
  {
    flush();
    $mess = $vnT->lang['not_permission'];
    $url = "?mod=main";
    echo $func->html_redirect($url, $mess);
    exit();
  }
}

// main
switch ($act)
{
  case "login":  include "login.php"; break;
  case "lostpass": include "lostpass.php";   break;
  case "logout":  include "logout.php"; 	break;
  default:
    $file_action = $folder . "/" . $option . "_ad/" . $act . ".php";
    if (file_exists($file_action)) include ($file_action);
    else
      include ("modules/main_ad/main.php");
    break;
}

$vnT->html->addScriptDeclaration('
		var lang_js = new Array(); 
		lang_js["please_chose_item"]   	= "' . $vnT->lang['please_chose_item'] . '";
		lang_js["are_you_sure_del"]   	= "' . $vnT->lang['are_you_sure_del'] . '";
	');
$Template->assign("EXT_HEAD", $vnT->html->fetchHead());
$Template->assign("BOX_LEFT", $box_left);
$Template->assign("PAGE_CONTENT", $vnT->output);
$Template->assign("select_skins", $func->list_skin_admin());

$Template->parse("body");
$Template->out("body");

$DB->close();
?>