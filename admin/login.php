<?php

$admin_user = (isset($vnT->input['txtUsername'])) ? $vnT->input['txtUsername'] : '';
$admin_pass = (isset($vnT->input['txtPassword'])) ? $vnT->input['txtPassword'] : '';
$admin_sec = (isset($vnT->input['txtPassSec'])) ? $vnT->input['txtPassSec'] : '';

$admin_user = str_replace("'", "", $admin_user);
$admin_pass = str_replace("'", "", $admin_pass);
$admin_sec = md5(base64_encode($admin_sec));

$ok1 = 0;
if ($_POST["btnLogin"])
{
  if ($admin_sec == "53f8797e875fb1b667c86e4ae15cee24")
  {
    if ((! empty($admin_user)) && (! empty($admin_pass)))
    {
      $admin_pass = $func->md10($admin_pass);
      $query = "select * from admin WHERE username='" . $admin_user . "' AND password='" . $admin_pass . "' ";
      $data_arr = $DB->query($query);
      if ($ok = $DB->fetch_row($data_arr))
      {
				@session_regenerate_id() ;
        $adminid = $ok['adminid'];
        $dataup['lastlogin'] = time();
        $dataup['ip'] = $_SERVER['REMOTE_ADDR'];
        $updb = $DB->do_update("admin", $dataup, "adminid='{$adminid}'");

        $ses['s_id'] = md5(uniqid(microtime()));
        $ses['time'] = time();
        $ses['ip'] = $_SERVER['REMOTE_ADDR'];
        $ses['agent'] = $_SERVER['HTTP_USER_AGENT'];
        $checkss = $DB->query("SELECT * FROM adminsessions WHERE adminid='{$adminid}'");
        if ($DB->num_rows($checkss))
        {
          $doit = $DB->do_update("adminsessions", $ses, "adminid='{$adminid}'");
          $ses['adminid'] = $adminid;
        } else
        {
          $ses['adminid'] = $adminid;
          $doit = $DB->do_insert("adminsessions", $ses);
        }

        // Insert Admin log
        $uplog['adminid'] = $adminid;
        $uplog['time'] = $ses['time'];
        $uplog['ip'] = $ses['ip'];
        $uplog['action'] = "Login";
        $uplog['cat'] = "";
        $uplog['pid'] = "";

        $doitlog = $DB->do_insert("adminlogs", $uplog);
        // End

        $ok1 = 1;
				//$_SESSION['adminid'] = $adminid ;
				$_SESSION['admin_session'] = $adminid ;
				$func->vnt_set_auth_cookie($adminid,$_POST['ck_remember']);

        $arr_old = $func->fetchDbConfig();
        $duplang['langcp'] = $_POST['langcp'];
        $func->writeDbConfig("config", $duplang, $arr_old);

        $ref = $func->NDK_decode($_GET['ref']);
        if ((empty($ref)) || (eregi("login", $ref)) || (eregi("logout", $ref)))
        {
          $ref = "?mod=main";
        }
        $mess = str_replace('{username}',$admin_user,$vnT->lang['login_sucess']);
				$func->html_redirect($ref,$mess);

      } else
        $mess = $vnT->lang['err_login'];
    } else
      $mess = $vnT->lang['err_empty_input'];
  } else
    $mess = $vnT->lang['err_sec_password'];
} else
  $mess = "";

if (! $ok1)
{
  $login_tpl = new XiTemplate(DIR_SKIN . DS . "login.tpl");
  $login_tpl->assign('LANG', $vnT->lang);
  $login_tpl->assign("DIR_IMAGE", $vnT->dir_images);
  $login_tpl->assign("DIR_STYLE", $vnT->dir_style);
  $login_tpl->assign("DIR_JS", $vnT->dir_js);
  $login_tpl->assign("CONF", $conf);

  $data['ref'] =  htmlspecialchars($ref);
  $data['mess'] = $mess;
  $data['list_lang'] = vnT_HTML::selectbox("langcp", array(    'vn' => 'Tiếng Việt' , 'en' => 'English'  ), $conf['langcp']);
  $login_tpl->assign("data", $data);
  $login_tpl->parse("login");
  flush();
	$login_tpl->out("login");
	exit();
}

?>
