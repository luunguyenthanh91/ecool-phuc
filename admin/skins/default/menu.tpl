<!-- BEGIN: html_box_menu -->
<link href="{DIR_STYLE}/fonts/fontawesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
<LINK href="{DIR_STYLE}/ext.css" rel="stylesheet" type="text/css">
<div class="menu-control"><a   href="#Collapse" id="menu-expand"><img align="absmiddle" width="12" src="{DIR_IMAGE}/but_cong.gif">&nbsp;Collapse All</a> &nbsp; <a href="#Expand" id="menu-collapse"><img align="absmiddle" width="12" src="{DIR_IMAGE}/but_tru.gif">&nbsp;Expand All</a></div>
<ul id="admin-menu">
    <li class="menu-item newMenu active">
        <div class="menu-top">
            <div class="menu-title waves-effect" onclick="window.location.href='?mod=config&act=skin&lang=vn';"><h2>Quản lý giao diện admin</h2></div>
        </div>
        <div class="clear"></div>
    </li>
{data.box_menu}
</ul> 
 
<!-- END: html_box_menu -->

 
<!-- BEGIN: html_menu_item -->
<li class="menu-item">
  <div class="menu-top">          	
    <div class="menu-title"><h2>{data.f_title}</h2></div>
    <div class="menu-toggle"><img src="{data.img}" width="18" height="18" id="img_{data.group}" name="img_{data.group}"  /></div>
  </div>
  <div class="clear"></div>
  <div class="sub-menu" id="{data.group}" style="{data.style}" >         
  <ul >
    {data.list_menu}
  </ul>
  </div>
</li>
<!-- END: html_menu_item -->

<!-- BEGIN: html_news -->
<div class="box-menu">
	<p align="center"><img src="{DIR_IMAGE}/thiet-ke-web-logo.png" width="160" height="66" /></p>
  {data.list_news}
</div>

<!-- END: html_news -->
 