<!-- BEGIN: login -->
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>.: LOGIN - ADMIN :.</title>
<link href="{DIR_STYLE}/style.css" rel="stylesheet" type="text/css" />
</head>
<body  style="background-color:#EFEFE9">
<table width="100%" border="0" cellspacing="0" cellpadding="0"   >
  <tr>
    <td style="padding-top:200px;" ><table width="670" border="0" cellspacing="0" cellpadding="0" align="center">
      <tr>
        <td width="208"><img src="{DIR_IMAGE}/key.jpg" width="208" height="159"></td>
        <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="10"><img src="{DIR_IMAGE}/l_t.jpg" width="10" height="10"></td>
            <td background="{DIR_IMAGE}/t.jpg"><img src="{DIR_IMAGE}/t.jpg" width="3" height="10"></td>
            <td width="10"><img src="{DIR_IMAGE}/r_t.jpg" width="10" height="10"></td>
          </tr>
          <tr>
            <td background="{DIR_IMAGE}/l.jpg"><img src="{DIR_IMAGE}/l.jpg" width="10" height="4"></td>
            <td  style="background:url({DIR_IMAGE}/ngontay.jpg) #FFFFFF left top no-repeat;" >
            <form action="?act=login&ref={data.ref}" name="f_login" method="post">
            <table width="100%" border="0" cellspacing="2" cellpadding="2">
              <tr>
                <td width="42%">&nbsp;</td>
                <td width="58%"><img src="{DIR_IMAGE}/logo_vntrust.gif" ></td>
              </tr>
              <tr>
                <td colspan="2" class="font_err" style="padding-left:100px;" align="center"><strong>{data.mess}</strong></td>
                </tr>
              <tr>
                <td align="right">{LANG.username} :</td>
                <td><input type="text" name="txtUsername" id="txtUsername" class="textfield" style="width:162px;" ></td>
              </tr>
              <tr>
                <td align="right">{LANG.password} :</td>
                <td><input type="password" name="txtPassword"  id="txtPassword" class="textfield" style="width:162px;" ></td>
              </tr>
              <tr>
                <td align="right">{LANG.sec_password} :</td>
                <td><input type="password" name="txtPassSec"  class="textfield" style="width:162px;" ></td>
              </tr>
              <tr>
                <td align="right">{LANG.language} :</td>
                <td>{data.list_lang} &nbsp; <input name="ck_remember" type="checkbox" value="1" align="absmiddle" /> {LANG.remember_pass}</td>
              </tr> 
              <tr>
                <td class="font_err" align="right"><a href="?act=lostpass">* {LANG.lostpass}</a></td>
                <td >
                <input type="submit" name="btnLogin" value="{LANG.login}" class="button"></td>
                
              </tr>
            </table>
            </form>
            </td>
            <td background="{DIR_IMAGE}/r.jpg"><img src="{DIR_IMAGE}/r.jpg" width="10" height="3"></td>
          </tr>
          <tr>
            <td><img src="{DIR_IMAGE}/l_b.jpg" width="10" height="10"></td>
            <td background="{DIR_IMAGE}/b.jpg"><img src="{DIR_IMAGE}/b.jpg" width="3" height="10"></td>
            <td><img src="{DIR_IMAGE}/r_b.jpg" width="10" height="10"></td>
          </tr>
        </table></td>
      </tr>
    </table></td>
  </tr>
</table>

<script type="text/javascript">
	try{document.getElementById('txtUsername').focus();}catch(e){}
</script>
</body>
</html>
<!-- END: login -->